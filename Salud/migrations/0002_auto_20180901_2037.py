# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Salud', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campania',
            name='Observacion',
            field=models.TextField(max_length=250, verbose_name=b'Observaci\xc3\xb3n'),
        ),
    ]
