# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Salud', '0002_auto_20180901_2037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campania',
            name='Observacion',
            field=models.TextField(max_length=250, null=True, verbose_name=b'Observaci\xc3\xb3n', blank=True),
        ),
    ]
