# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Matriculas', '0001_initial'),
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campania',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=120, verbose_name=b'Nombre')),
                ('Fecha', models.DateField(verbose_name=b'Fecha')),
                ('Observacion', models.CharField(max_length=250, verbose_name=b'Observaci\xc3\xb3n')),
                ('Periodo', models.ForeignKey(to='Periodos.Periodo')),
            ],
            options={
                'verbose_name': 'Campa\xf1a M\xe9dica',
                'verbose_name_plural': 'Campa\xf1as m\xe9dicas',
            },
        ),
        migrations.CreateModel(
            name='Detallecampania',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Campania', models.ForeignKey(to='Salud.Campania')),
            ],
            options={
                'verbose_name': 'Detalle campa\xf1a',
                'verbose_name_plural': 'Detalle de campa\xf1a',
            },
        ),
        migrations.CreateModel(
            name='Especialidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=150, verbose_name=b'Nombre')),
                ('Especialidad', models.ForeignKey(blank=True, to='Salud.Especialidad', null=True)),
            ],
            options={
                'verbose_name': 'Especialidad',
                'verbose_name_plural': 'Especialidades',
            },
        ),
        migrations.CreateModel(
            name='Itemevaluacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=120, verbose_name=b'Nombre')),
                ('Detallecampania', models.ForeignKey(to='Salud.Detallecampania')),
            ],
            options={
                'verbose_name': '\xcdtem de evaluaci\xf3n',
                'verbose_name_plural': '\xcdtems de evaluaci\xf3n',
            },
        ),
        migrations.CreateModel(
            name='Paciente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Campania', models.ForeignKey(to='Salud.Campania')),
                ('MatriculaCiclo', models.ForeignKey(to='Matriculas.MatriculaCiclo')),
            ],
            options={
                'verbose_name': 'Paciente',
                'verbose_name_plural': 'Pacientes',
            },
        ),
        migrations.CreateModel(
            name='Resultado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Valor', models.CharField(max_length=50, verbose_name=b'Valor')),
                ('Itemevaluacion', models.ForeignKey(to='Salud.Itemevaluacion')),
                ('Paciente', models.ForeignKey(to='Salud.Paciente')),
            ],
            options={
                'verbose_name': 'Resultado',
                'verbose_name_plural': 'Resultados',
            },
        ),
        migrations.AddField(
            model_name='detallecampania',
            name='Especialidad',
            field=models.ForeignKey(to='Salud.Especialidad'),
        ),
    ]
