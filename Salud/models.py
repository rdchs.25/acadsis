# -*- coding: utf-8 -*-
from django.db import models

from Matriculas.models import MatriculaCiclo
from Periodos.models import Periodo


class Campania(models.Model):
    Nombre = models.CharField("Nombre", max_length=120)
    Fecha = models.DateField("Fecha")
    Periodo = models.ForeignKey(Periodo)
    Observacion = models.TextField("Observación", max_length=250, null=True, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.Nombre, self.Periodo)

    def __str__(self):
        return u'%s %s' % (self.Nombre, self.Periodo)

    class Meta:
        verbose_name = "Campaña Médica"
        verbose_name_plural = "Campañas médicas"


class Especialidad(models.Model):
    Nombre = models.CharField("Nombre", max_length=150)
    Especialidad = models.ForeignKey("self", null=True, blank=True)

    class Meta:
        verbose_name = "Especialidad"
        verbose_name_plural = "Especialidades"

    def __str__(self):
        return "%s" % self.Nombre

    def __unicode__(self):
        return "%s" % self.Nombre


class Detallecampania(models.Model):
    Especialidad = models.ForeignKey(Especialidad)
    Campania = models.ForeignKey(Campania)

    class Meta:
        verbose_name = "Detalle campaña"
        verbose_name_plural = "Detalle de campaña"

    def __str__(self):
        return "%s - %s" % (self.Especialidad, self.Campania)

    def __unicode__(self):
        return "%s - %s" % (self.Especialidad, self.Campania)


class Itemevaluacion(models.Model):
    Nombre = models.CharField("Nombre", max_length=120)
    Detallecampania = models.ForeignKey(Detallecampania)

    class Meta:
        verbose_name = "Ítem de evaluación"
        verbose_name_plural = "Ítems de evaluación"

    def __str__(self):
        return "%s" % self.Nombre

    def __unicode__(self):
        return "%s" % self.Nombre


class Paciente(models.Model):
    Campania = models.ForeignKey(Campania)
    MatriculaCiclo = models.ForeignKey(MatriculaCiclo)

    class Meta:
        verbose_name = "Paciente"
        verbose_name_plural = "Pacientes"

    def __str__(self):
        return "%s - %s" % (self.Campania, self.MatriculaCiclo)

    def __unicode__(self):
        return "%s - %s" % (self.Campania, self.MatriculaCiclo)


class Resultado(models.Model):
    Paciente = models.ForeignKey(Paciente)
    Itemevaluacion = models.ForeignKey(Itemevaluacion)
    Valor = models.CharField("Valor", max_length=50)

    class Meta:
        verbose_name = "Resultado"
        verbose_name_plural = "Resultados"

    def __str__(self):
        return "%s" % self.Valor

    def __unicode__(self):
        return "%s" % self.Valor
