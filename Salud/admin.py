# -*- coding: utf-8 -*-
from django.contrib import admin

from Salud.models import Campania, Especialidad, Detallecampania, Itemevaluacion, Paciente, Resultado

admin.site.register(Campania)
admin.site.register(Especialidad)
admin.site.register(Detallecampania)
admin.site.register(Itemevaluacion)
admin.site.register(Paciente)
admin.site.register(Resultado)
