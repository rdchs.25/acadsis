# -*- coding: latin-1 -*-
from UDL import settings


def formato_boleta_notas1(c, type):
    w = 420
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    # --------------------------
    # Numero de guia
    # --------------------------
    # while i < 1734:
    #   c.drawString(10,i, str(i))
    #   i=i+4
    # j=0
    # while j < 1734:
    #   c.drawString(j,830, str(j))
    #   j=j+10

    # --------------------------
    # Rellenos
    # --------------------------
    # k=120
    # interlineado=20
    # c.setStrokeColorRGB(0.75,0.75,0.75)
    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # while k < 300:
    ##c.drawString(10,k, str(k))
    # c.line(margenLeft,k,anchoA4 - margenRight,k) #Lineas Formulario ...
    # l = k
    # if l%40==0:
    # while l < k+interlineado:
    # c.line(margenLeft,l,anchoA4 - margenRight,l) #Lineas Formulario ...
    # l = l + 1

    # k=k+interlineado

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(205, 385 + w, "EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365 + w, "Boleta de Notas")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335 + w, "Estudiante :")
    c.drawString(40, 315 + w, "Carrera :")
    c.drawString(435, 335 + w, "Código :")
    c.drawString(435, 315 + w, "Semestre :")

    c.drawString(50, 285 + w, "Nº")
    c.drawString(74, 285 + w, "Código")
    c.drawString(120, 285 + w, "G.H.")
    c.drawString(265, 285 + w, "Asignatura")
    c.drawString(446, 285 + w, "Créditos")
    c.drawString(513, 285 + w, "Nota")

    c.drawString(350, 70 + w, "Chiclayo, ")
    c.line(210, 35 + w, 390, 35 + w)
    c.drawString(215, 20 + w, "Responsable de la Carrera Profesional")

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # 1era Linea ...
    c.line(margenLeft, 300 + w, anchoA4 - margenRight, 300 + w)
    # 2da Linea ...
    c.line(margenLeft, 280 + w, anchoA4 - margenRight, 280 + w)
    # 3da Linea ...
    c.line(margenLeft, 120 + w, anchoA4 - margenRight, 120 + w)

    # Linea Vertical Izquirda...
    c.line(margenLeft, 300 + w, margenLeft, 120 + w)
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300 + w, anchoA4 - margenRight, 120 + w)

    # Linea Vertical 2...
    c.line(margenLeft + 30, 300 + w, margenLeft + 30, 120 + w)
    # Linea Vertical 3...
    c.line(margenLeft + 70, 300 + w, margenLeft + 70, 120 + w)
    # Linea Vertical 4...
    c.line(margenLeft + 110, 300 + w, margenLeft + 110, 120 + w)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 60, 300 + w,
           anchoA4 - margenRight - 60, 120 + w)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 120, 300 + w,
           anchoA4 - margenRight - 120, 120 + w)

    # resumen de creditos
    if type == "ponderado":
        c.line(margenLeft, 110 + w, margenLeft + 160, 110 + w)  # 1era Linea horiz
        c.drawString(43, 95 + w, "Créditos aprobados")
        c.line(margenLeft, 90 + w, margenLeft + 160, 90 + w)  # 2da Linea horiz
        c.drawString(43, 75 + w, "Créditos desaprobados")
        c.line(margenLeft, 70 + w, margenLeft + 160, 70 + w)  # 3da Linea horiz

        c.line(margenLeft, 110 + w, margenLeft, 70 + w)  # 1era Linea vertical
        c.line(margenLeft + 110, 110 + w, margenLeft +
               110, 70 + w)  # 2da Linea vertical
        c.line(margenLeft + 160, 110 + w, margenLeft +
               160, 70 + w)  # 3da Linea vertical

    # promedio ponderado
    c.line(anchoA4 - margenRight - 180, 110 + w, anchoA4 -
           margenRight, 110 + w)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 180, 90 + w, anchoA4 -
           margenRight, 90 + w)  # 1era Linea horiz

    c.line(anchoA4 - margenRight - 180, 110 + w, anchoA4 -
           margenRight - 180, 90 + w)  # 1era Linea vertical

    if type == "ponderado":
        c.drawString(anchoA4 - margenRight - 175, 95 + w, "Promedio Ponderado")
    else:
        c.drawString(anchoA4 - margenRight - 175, 95 + w, "Promedio")
    c.line(anchoA4 - margenRight - 60, 110 + w, anchoA4 -
           margenRight - 60, 90 + w)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110 + w, anchoA4 -
           margenRight, 90 + w)  # 3da Linea vertical

    c.setDash(6, 3)
    c.line(0, 421, anchoA4, 421)
    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logo.png'
    c.drawImage(logo, 30, 355 + w, width=130, height=62)
    return c


def formato_boleta_notas(c, type):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    # --------------------------
    # Numero de guia
    # --------------------------
    # while i < 1734:
    #   c.drawString(10,i, str(i))
    #   i=i+4
    # j=0
    # while j < 1734:
    #   c.drawString(j,830, str(j))
    #   j=j+10

    # --------------------------
    # Rellenos
    # --------------------------
    # k=120
    # interlineado=20
    # c.setStrokeColorRGB(0.75,0.75,0.75)
    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # while k < 300:
    ##c.drawString(10,k, str(k))
    # c.line(margenLeft,k,anchoA4 - margenRight,k) #Lineas Formulario ...
    # l = k
    # if l%40==0:
    # while l < k+interlineado:
    # c.line(margenLeft,l,anchoA4 - margenRight,l) #Lineas Formulario ...
    # l = l + 1

    # k=k+interlineado

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(205, 385, "EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365, "Boleta de Notas")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335, "Estudiante :")
    c.drawString(40, 315, "Carrera :")
    c.drawString(435, 335, "Código :")
    c.drawString(435, 315, "Semestre :")

    c.drawString(50, 285, "Nº")
    c.drawString(74, 285, "Código")
    c.drawString(120, 285, "G.H.")
    c.drawString(265, 285, "Asignatura")
    c.drawString(446, 285, "Créditos")
    c.drawString(513, 285, "Nota")

    c.drawString(350, 70, "Chiclayo, ")
    c.line(210, 35, 390, 35)
    c.drawString(215, 20, "Responsable de la Carrera Profesional")

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(margenLeft, 300, anchoA4 - margenRight, 300)  # 1era Linea ...
    c.line(margenLeft, 280, anchoA4 - margenRight, 280)  # 2da Linea ...
    c.line(margenLeft, 120, anchoA4 - margenRight, 120)  # 3da Linea ...

    c.line(margenLeft, 300, margenLeft, 120)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300, anchoA4 - margenRight, 120)

    c.line(margenLeft + 30, 300, margenLeft + 30, 120)  # Linea Vertical 2...
    c.line(margenLeft + 70, 300, margenLeft + 70, 120)  # Linea Vertical 3...
    c.line(margenLeft + 110, 300, margenLeft + 110, 120)  # Linea Vertical 4...
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 60, 300, anchoA4 - margenRight - 60, 120)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 120, 300, anchoA4 - margenRight - 120, 120)

    if type == "ponderado":
        # resumen de creditos
        c.line(margenLeft, 110, margenLeft + 160, 110)  # 1era Linea horiz
        c.drawString(43, 95, "Créditos aprobados")
        c.line(margenLeft, 90, margenLeft + 160, 90)  # 2da Linea horiz
        c.drawString(43, 75, "Créditos desaprobados")
        c.line(margenLeft, 70, margenLeft + 160, 70)  # 3da Linea horiz

        c.line(margenLeft, 110, margenLeft, 70)  # 1era Linea vertical
        c.line(margenLeft + 110, 110, margenLeft + 110, 70)  # 2da Linea vertical
        c.line(margenLeft + 160, 110, margenLeft + 160, 70)  # 3da Linea vertical

    # promedio ponderado
    c.line(anchoA4 - margenRight - 180, 110, anchoA4 -
           margenRight, 110)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 180, 90, anchoA4 -
           margenRight, 90)  # 1era Linea horiz

    c.line(anchoA4 - margenRight - 180, 110, anchoA4 -
           margenRight - 180, 90)  # 1era Linea vertical

    if type == "ponderado":
        c.drawString(anchoA4 - margenRight - 175, 95, "Promedio Ponderado")
    else:
        c.drawString(anchoA4 - margenRight - 175, 95, "Promedio")

    c.line(anchoA4 - margenRight - 60, 110, anchoA4 -
           margenRight - 60, 90)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110, anchoA4 -
           margenRight, 90)  # 3da Linea vertical

    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logo.png'
    c.drawImage(logo, 30, 355, width=130, height=62)
    return c


def ficha_matricula(c):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    k = 120
    interlineado = 20

    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(180, 385, "UNIDAD DE REGISTROS ACADEMICOS")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365, "Ficha de Matrícula")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335, "Estudiante :")
    c.drawString(40, 315, "Carrera :")
    c.drawString(435, 335, "Código :")
    c.drawString(435, 315, "Semestre :")

    c.drawString(60, 285, "CÓDIGO CURSO")
    c.drawString(180, 285, "G.H.")
    c.drawString(310, 285, "NOMBRE DEL CURSO")
    c.drawString(520, 285, "CRED")

    c.line(93, 35, 205, 35)
    c.drawString(100, 20, "Firma del estudiante")
    c.line(390, 35, 502, 35)
    c.drawString(400, 20, "Firma del responsable")
    # c.line(margenLeft,630,anchoA4 - margenRight,630)

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(margenLeft, 300, anchoA4 - margenRight - 335, 300)  # 1era Linea ...
    c.line(margenLeft, 280, anchoA4 - margenRight - 335, 280)  # 2da Linea ...
    c.line(margenLeft, 120, anchoA4 - margenRight - 335, 120)  # 3da Linea ...

    c.line(margenLeft, 300, margenLeft, 120)  # Linea Vertical Izquirda...
    c.line(margenLeft + 120, 300, margenLeft + 120, 120)  # Linea Vertical 7...
    # Linea Vertical 10...
    c.line(margenLeft + 180, 300, margenLeft + 180, 120)

    # 1era Linea ...
    c.line(margenLeft + 185, 300, anchoA4 - margenRight - 45, 300)
    # 2da Linea ...
    c.line(margenLeft + 185, 280, anchoA4 - margenRight - 45, 280)
    # 3da Linea ...
    c.line(margenLeft + 185, 120, anchoA4 - margenRight - 45, 120)

    # Linea Vertical 11...
    c.line(margenLeft + 185, 300, margenLeft + 185, 120)
    # Linea Vertical 12...
    c.line(margenLeft + 470, 300, margenLeft + 470, 120)

    c.line(margenLeft + 475, 300, anchoA4 - margenRight, 300)  # 1era Linea ...
    c.line(margenLeft + 475, 280, anchoA4 - margenRight, 280)  # 2da Linea ...
    c.line(margenLeft + 475, 120, anchoA4 - margenRight, 120)  # 3da Linea ...

    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300, anchoA4 - margenRight, 120)
    # Linea Vertical Derecha 2...
    c.line(anchoA4 - margenRight - 40, 300, anchoA4 - margenRight - 40, 120)

    # fecha
    c.drawString(43, 95, "Fecha:")
    c.line(margenLeft + 50, 90, margenLeft + 140, 90)  # 2da Linea horiz
    c.line(margenLeft + 80, 93, margenLeft + 85, 103)  # slash1
    c.line(margenLeft + 110, 93, margenLeft + 115, 103)  # slash2

    # total de creditos
    c.line(anchoA4 - margenRight - 40, 110, anchoA4 -
           margenRight, 110)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 40, 90, anchoA4 -
           margenRight, 90)  # 1era Linea horiz

    c.drawString(anchoA4 - margenRight - 125, 95, "Total de Créditos")
    c.line(anchoA4 - margenRight - 40, 110, anchoA4 -
           margenRight - 40, 90)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110, anchoA4 -
           margenRight, 90)  # 3da Linea vertical

    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logo.png'
    c.drawImage(logo, 30, 355, width=130, height=62)
    return c


# esta ficha es para generar 2 fichas en una A4, esta es la ficha superior.


def ficha_matricula2(c):
    w = 420
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    k = 120
    interlineado = 20

    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(180, 385 + w, "UNIDAD DE REGISTROS ACADEMICOS")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365 + w, "Ficha de Matrícula")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335 + w, "Estudiante :")
    c.drawString(40, 315 + w, "Carrera :")
    c.drawString(435, 335 + w, "Código :")
    c.drawString(435, 315 + w, "Semestre :")

    c.drawString(60, 285 + w, "CÓDIGO CURSO")
    c.drawString(180, 285 + w, "G.H.")
    c.drawString(310, 285 + w, "NOMBRE DEL CURSO")
    c.drawString(520, 285 + w, "CRED")

    c.line(93, 35 + w, 205, 35 + w)
    c.drawString(100, 20 + w, "Firma del estudiante")
    c.line(390, 35 + w, 502, 35 + w)
    c.drawString(400, 20 + w, "Firma del responsable")
    # c.line(margenLeft,630,anchoA4 - margenRight,630)

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # 1era Linea ...
    c.line(margenLeft, 300 + w, anchoA4 - margenRight - 335, 300 + w)
    # 2da Linea ...
    c.line(margenLeft, 280 + w, anchoA4 - margenRight - 335, 280 + w)
    # 3da Linea ...
    c.line(margenLeft, 120 + w, anchoA4 - margenRight - 335, 120 + w)

    # Linea Vertical Izquirda...
    c.line(margenLeft, 300 + w, margenLeft, 120 + w)
    # Linea Vertical 7...
    c.line(margenLeft + 120, 300 + w, margenLeft + 120, 120 + w)
    # Linea Vertical 10...
    c.line(margenLeft + 180, 300 + w, margenLeft + 180, 120 + w)

    # 1era Linea ...
    c.line(margenLeft + 185, 300 + w, anchoA4 - margenRight - 45, 300 + w)
    # 2da Linea ...
    c.line(margenLeft + 185, 280 + w, anchoA4 - margenRight - 45, 280 + w)
    # 3da Linea ...
    c.line(margenLeft + 185, 120 + w, anchoA4 - margenRight - 45, 120 + w)

    # Linea Vertical 11...
    c.line(margenLeft + 185, 300 + w, margenLeft + 185, 120 + w)
    # Linea Vertical 12...
    c.line(margenLeft + 470, 300 + w, margenLeft + 470, 120 + w)

    # 1era Linea ...
    c.line(margenLeft + 475, 300 + w, anchoA4 - margenRight, 300 + w)
    # 2da Linea ...
    c.line(margenLeft + 475, 280 + w, anchoA4 - margenRight, 280 + w)
    # 3da Linea ...
    c.line(margenLeft + 475, 120 + w, anchoA4 - margenRight, 120 + w)

    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300 + w, anchoA4 - margenRight, 120 + w)
    # Linea Vertical Derecha 2...
    c.line(anchoA4 - margenRight - 40, 300 + w,
           anchoA4 - margenRight - 40, 120 + w)

    # fecha
    c.drawString(43, 95 + w, "Fecha:")
    c.line(margenLeft + 50, 90 + w, margenLeft +
           140, 90 + w)  # 2da Linea horiz
    c.line(margenLeft + 80, 93 + w, margenLeft + 85, 103 + w)  # slash1
    c.line(margenLeft + 110, 93 + w, margenLeft + 115, 103 + w)  # slash2

    # total de creditos
    c.line(anchoA4 - margenRight - 40, 110 + w, anchoA4 -
           margenRight, 110 + w)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 40, 90 + w, anchoA4 -
           margenRight, 90 + w)  # 1era Linea horiz

    c.drawString(anchoA4 - margenRight - 125, 95 + w, "Total de Créditos")
    c.line(anchoA4 - margenRight - 40, 110 + w, anchoA4 -
           margenRight - 40, 90 + w)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110 + w, anchoA4 -
           margenRight, 90 + w)  # 3da Linea vertical

    c.setDash(6, 3)
    c.line(0, 421, anchoA4, 421)
    # --------------------------
    # Logo
    # --------------------------
    c.drawImage(settings.MEDIA_ROOT + 'logo.png',
                30, 355 + w, width=130, height=62)
    return c
