# Create your views here.
# -*- coding: utf-8 -*-

import hashlib
from decimal import Decimal

from django import forms
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.forms.util import ErrorList
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
from django.views.decorators.csrf import csrf_protect

from UDL.Alumnos.models import Alumno
from UDL.Asistencias.models import Asistencia
from UDL.Campus.models import MoodleUser
from UDL.Cursos.models import PeriodoCurso
# imports tree cursos.
from UDL.Matriculas.models import MatriculaCursos, MatriculaCiclo
from UDL.Notas.models import NotasAlumno, Nota
from UDL.Pagos.models import PagoAlumno, DetallePago


class LoginForm(forms.Form):
    Usuario = forms.CharField(max_length=7, min_length=7, label="Usuario", required=True)
    Password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")

        if user and passwd:
            try:
                validar_usuario = MoodleUser.objects.using('moodle').get(username=user)
                usuario_udl = User.objects.get(username=user)

                try:
                    # usuario = MoodleUser.objects.using('moodle').get(username=user, password=hashlib.md5(
                    #     passwd + ' tvI!vpvr?,z4.7G{ +UUJWgW0NLD').hexdigest())
                    usuario = MoodleUser.objects.using('moodle').get(username=user,
                                                                     password=hashlib.md5(passwd).hexdigest())
                    usuario_udl.set_password(passwd)
                    usuario_udl.save()
                except MoodleUser.DoesNotExist:
                    msg = u'Usuario Incorrecto'
                    msg1 = u'Contraseña Incorrecta'
                    self._errors["Usuario"] = ErrorList([msg])
                    self._errors["Password"] = ErrorList([msg1])
                    del cleaned_data["Usuario"]
                    del cleaned_data["Password"]

            except MoodleUser.DoesNotExist, User.DoesNotExist:
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]

        return cleaned_data


@csrf_protect
def login_campus(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['Usuario']
            passwd = form.cleaned_data['Password']
            user = authenticate(username=usuario, password=passwd)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('profile2/')
    else:
        form = LoginForm()

    return render_to_response("login_campus.html", {"form": form}, context_instance=RequestContext(request))


def profile_usuario(request):
    if request.user.is_authenticated():
        return render_to_response("Campus/profile_usuario.html")
    else:
        return HttpResponseRedirect('../')


def ver_tree_cursos(request):
    if request.user.is_authenticated():
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        cursos_matriculados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno__user_ptr=request.user.id).order_by(
            '-MatriculaCiclo__Periodo')
        semestres = cursos_matriculados.values('MatriculaCiclo__Periodo__Anio',
                                               'MatriculaCiclo__Periodo__Semestre').distinct()

        n_cursos = cursos_matriculados.count()
        paginator = Paginator(cursos_matriculados, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        j = 1
        k = 0

        for sem in semestres:
            cursos = cursos_matriculados.filter(PeriodoCurso__Periodo__Anio=sem['MatriculaCiclo__Periodo__Anio'],
                                                PeriodoCurso__Periodo__Semestre=
                                                sem['MatriculaCiclo__Periodo__Semestre'])
            cant_cursos = cursos.count()
            k += (cant_cursos + 3) * 6 + 2
            fila = {"id": sem['MatriculaCiclo__Periodo__Anio'] + sem['MatriculaCiclo__Periodo__Semestre'], "cell": [i,
                                                                                                                    sem[
                                                                                                                        'MatriculaCiclo__Periodo__Anio'] + ' - ' +
                                                                                                                    sem[
                                                                                                                        'MatriculaCiclo__Periodo__Semestre'],
                                                                                                                    '',
                                                                                                                    0,
                                                                                                                    j,
                                                                                                                    k,
                                                                                                                    'false',
                                                                                                                    'false']}
            filas.append(fila)
            a = i + 1
            b = j + 1
            for curso in cursos:
                fila = {"id": curso.PeriodoCurso.Curso.id,
                        "cell": [a, curso.PeriodoCurso.Curso.Nombre, '', 1, b, b + 5, 'false', 'false']}
                fila1 = {"id": 'A' + str(curso.PeriodoCurso.id),
                         "cell": [a + 1, 'Asistencias', 'asistencias/%s/' % curso.PeriodoCurso.id, 2, b + 1, b +
                                  2, 'true', 'true']}
                fila2 = {"id": 'N' + str(curso.PeriodoCurso.id),
                         "cell": [a + 2, 'Notas', 'notas/%s/' % curso.PeriodoCurso.id, 2, b + 3, b +
                                  4, 'true', 'true']}
                filas.append(fila)
                filas.append(fila1)
                filas.append(fila2)
                a += 3
                b += 6
            filas.append(
                {"id": 'FP' + str(sem['MatriculaCiclo__Periodo__Anio'] + sem['MatriculaCiclo__Periodo__Semestre']),
                 "cell": [a, 'Cuenta Corriente', 'cuenta_corriente/%s/%s/' % (
                     sem['MatriculaCiclo__Periodo__Anio'], sem['MatriculaCiclo__Periodo__Semestre']), 1, b, b +
                          1, 'true', 'true']})
            filas.append(
                {"id": 'FM' + str(sem['MatriculaCiclo__Periodo__Anio'] + sem['MatriculaCiclo__Periodo__Semestre']),
                 "cell": [a + 1, 'Ficha de Matrícula', 'ficha_matricula/%s/%s/' % (
                     sem['MatriculaCiclo__Periodo__Anio'], sem['MatriculaCiclo__Periodo__Semestre']), 1, b + 2, b +
                          3, 'true', 'true']})
            filas.append(
                {"id": 'FN' + str(sem['MatriculaCiclo__Periodo__Anio'] + sem['MatriculaCiclo__Periodo__Semestre']),
                 "cell": [a + 2, 'Ficha de Notas', 'ficha_notas/%s/%s/' % (
                     sem['MatriculaCiclo__Periodo__Anio'], sem['MatriculaCiclo__Periodo__Semestre']), 1, b + 4, b +
                          5, 'true', 'true']})
            i += (cant_cursos + 3) * 3 + 1
            j += (cant_cursos + 3) * 6 + 2

        results = {"page": page, "total": paginator.num_pages, "records": n_cursos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return HttpResponseRedirect('')


def datos_personales(request):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(id=request.user.id)
            return render_to_response("Campus/datos_personales.html", {"estudiante": estudiante})
        except:
            return HttpResponse('')
    else:
        return render_to_response("Campus/mensaje.html")


class ChangePasswordForm(forms.Form):
    OldPassword = forms.CharField(widget=forms.PasswordInput, label="Contraseña Antigua", required=True)
    NewPassword = forms.CharField(widget=forms.PasswordInput, label="Nueva Contraseña", required=True)
    RepeatPassword = forms.CharField(widget=forms.PasswordInput, label="Repetir Contraseña", required=True)
    Usuario = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    def clean(self):
        cleaned_data = self.cleaned_data
        oldpasswd = cleaned_data.get("OldPassword")
        newpasswd = cleaned_data.get("NewPassword")
        repeatpasswd = cleaned_data.get("RepeatPassword")
        usuario = cleaned_data.get("Usuario")

        if oldpasswd and newpasswd and repeatpasswd and usuario:
            try:
                usuario_udl = User.objects.get(username=usuario)
                usuario_moodle = MoodleUser.objects.using('moodle').get(username=usuario)
                if usuario_udl.check_password(oldpasswd) is True:
                    if newpasswd == repeatpasswd:
                        usuario_udl.set_password(newpasswd)
                        # usuario_moodle.password = hashlib.md5(newpasswd + ' tvI!vpvr?,z4.7G{ +UUJWgW0NLD').hexdigest()
                        usuario_moodle.password = hashlib.md5(newpasswd).hexdigest()
                        usuario_udl.save()
                        usuario_moodle.save()
                    else:
                        msg = u'Las contraseñas no coinciden'
                        self._errors["RepeatPassword"] = ErrorList([msg])
                        del cleaned_data["RepeatPassword"]
                else:
                    msg = u'Contraseña Incorrecta'
                    self._errors["OldPassword"] = ErrorList([msg])
                    del cleaned_data["OldPassword"]
            except MoodleUser.DoesNotExist, User.DoesNotExist:
                msg = u'Usuario Incorrecto, intento de acceso no autorizado'
                self._errors["OldPassword"] = ErrorList([msg])
                del cleaned_data["OldPassword"]
        return cleaned_data


def aviso_change_password(request):
    if request.user.is_authenticated():
        return render_to_response("Campus/aviso_change_password.html", {"user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return render_to_response("Campus/mensaje.html")


@csrf_protect
def change_password(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = ChangePasswordForm(request.POST)
            if form.is_valid():
                return render_to_response("Campus/change_password_successful.html")
        else:
            form = ChangePasswordForm(initial={'Usuario': request.user.username})

        return render_to_response("Campus/change_password.html", {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return render_to_response("Campus/mensaje.html")


@csrf_protect
def cerrar_sesion(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            logout(request)
            return HttpResponseRedirect('../../')
        return render_to_response("Campus/cerrar_sesion.html", {"user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return render_to_response("Campus/mensaje.html")


def asistencias_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            curso = PeriodoCurso.objects.get(id=id_periodocurso)
            asistencias = Asistencia.objects.filter(MatriculaCursos__MatriculaCiclo__Alumno__id=request.user.id,
                                                    MatriculaCursos__PeriodoCurso=curso).order_by('-Fecha')
            asistio = 0
            falto = 0
            tardanza = 0
            for asi in asistencias:
                if asi.Estado == 'Asistio':
                    asistio += 1
                elif asi.Estado == 'Falto':
                    falto += 1
                elif asi.Estado == 'Tardanza':
                    tardanza += 1
            if asistencias.count() != 0:
                asiste = (asistio * 100) / asistencias.count()
                falta = (falto * 100) / asistencias.count()
                tarde = (tardanza * 100) / asistencias.count()
            else:
                asiste = 0
                falta = 0
                tarde = 0
            return render_to_response("Campus/asistencias_curso.html",
                                      {"asistencias": asistencias, "curso": curso, "asistio": asistio, "falto": falto,
                                       "tardanza": tardanza, "asiste": asiste, "falta": falta, "tarde": tarde})
        except PeriodoCurso.DoesNotExist:
            return HttpResponse('')
    else:
        return render_to_response("Campus/mensaje.html")


def notas_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            return HttpResponse('')

        # deudas = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__id=request.user.id,
        #                                    MatriculaCiclo__Periodo__Anio=periodocurso.Periodo.Anio,
        #                                    MatriculaCiclo__Periodo__Semestre=periodocurso.Periodo.Semestre,
        #                                    ConceptoPago__Descripcion__icontains='Pension')
        # Verificar si debe la quinta pensión
        deudas = PagoAlumno.objects.filter(MatriculaCiclo__Alumno__id=request.user.id,
                                           MatriculaCiclo__Periodo__Anio=periodocurso.Periodo.Anio,
                                           MatriculaCiclo__Periodo__Semestre=periodocurso.Periodo.Semestre,
                                           ConceptoPago__Descripcion__icontains='1ra Pensión')
        if deudas.count() != 0:
            if str(deudas[0].DeudaMonto()) != "0.00":
                activado = False
            else:
                activado = True
        else:
            activado = True

        # notas = Nota.objects.filter(PeriodoCurso = periodocurso).order_by('id')
        # if notas.filter(Nivel = '0').count() != 0:
        #    nivel_cero = True
        # else:
        #    nivel_cero = False

        # titulos = "['N'"
        # celdas = "[{name:'id1',index:'id1', align:'center', width:55},"
        # ident = []
        # formulas = []

        # if notas.count() != 0:
        #    for cal1 in notas:
        #        if cal1 in ident:
        #            pass
        #        else:
        #            if cal1.Nivel == "0":
        #                ultima_nota = cal1
        #                subnotas1 = Nota.objects.filter(NotaPadre = cal1)
        #                formula = "%s = (" % cal1.Identificador
        #                suma_pesos = 0
        #                i = 1
        #                if subnotas1.count() != 0:
        #                    for nota in subnotas1:
        #                        if i == 1:
        #                            formula += "%s*%s" % (nota.Identificador,nota.Peso)
        #                        else:
        #                            formula += " + %s*%s" % (nota.Identificador,nota.Peso)
        #                        suma_pesos += nota.Peso
        #                        i += 1
        #                    formula += ")/%s" % suma_pesos
        #                    formulas.append(formula)
        #                else:
        #                    formula = "%s = %s" % (cal1.Identificador,cal1.Identificador)
        #                    formulas.append(formula)
        #            else:
        #                subnotas2 = Nota.objects.filter(NotaPadre = cal1)
        #                if subnotas2.count() == 0:
        #                    ident.append(cal1)
        #                    titulos += ",'%s'" % cal1.Identificador
        #                    celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:false}," % (cal1.id,cal1.Identificador)
        #                else:
        #                    formula1 = "%s = (" % cal1.Identificador
        #                    suma_pesos1 = 0
        #                    i = 1
        #                    for cal2 in subnotas2:
        #                        if i == 1:
        #                            formula1 += "%s*%s" % (cal2.Identificador,cal2.Peso)
        #                        else:
        #                            formula1 += " + %s*%s" % (cal2.Identificador,cal2.Peso)
        #                        suma_pesos1 += cal2.Peso
        #                        i += 1

        #                        subnotas3 = Nota.objects.filter(NotaPadre = cal2)
        #                        if subnotas3.count() == 0:
        #                            ident.append(cal2)
        #                            titulos += ",'%s'" % cal2.Identificador
        #                            celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:false}," % (cal2.id,cal2.Identificador)
        #                        else:
        #                            formula2 = "%s = (" % cal2.Identificador
        #                            suma_pesos2 = 0
        #                            i = 1
        #                            for cal3 in subnotas3:
        #                                if i == 1:
        #                                    formula2 += "%s*%s" % (cal3.Identificador,cal3.Peso)
        #                                else:
        #                                    formula2 += " + %s*%s" % (cal3.Identificador,cal3.Peso)
        #                                suma_pesos2 += cal3.Peso
        #                                i += 1

        #                                ident.append(cal3)
        #                                titulos += ",'%s'" % cal3.Identificador
        #                                celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:false}," % (cal3.id,cal3.Identificador)
        #                            formula2 += ")/%s" % suma_pesos2
        #                            formulas.append(formula2)
        #                            ident.append(cal2)
        #                    formula1 += ")/%s" % suma_pesos1
        #                    formulas.append(formula1)
        #                    ident.append(cal1)
        #    ident.append(ultima_nota)
        #    titulos += ",'%s'" % ultima_nota.Identificador
        #    celdas += "{name:'%s',index:'%s', width:80, align:'center',editable:false}," % (ultima_nota.id,ultima_nota.Identificador)
        # titulos += "]"
        # celdas += "]"

        registros = []
        notas1 = Nota.objects.filter(PeriodoCurso=periodocurso, Nivel='1').order_by('Orden')

        nots1 = []
        nots2 = []
        nots3 = []

        mat = MatriculaCursos.objects.get(PeriodoCurso=periodocurso, MatriculaCiclo__Alumno__id=request.user.id)

        for nota1 in notas1:
            nots_2 = Nota.objects.filter(PeriodoCurso=periodocurso, NotaPadre=nota1, Nivel='2').order_by('Orden')
            for nota2 in nots_2:
                nots_3 = Nota.objects.filter(PeriodoCurso=periodocurso, NotaPadre=nota2, Nivel='3').order_by('Orden')
                for nota3 in nots_3:
                    valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                    if valor3.count() != 0:
                        valor3 = valor3[0].Valor
                        nots3.append([nota3, int(str(valor3).split('.')[0])])
                    else:
                        valor3 = mat.PromedioParcial(nota3)
                        nots3.append([nota3, valor3])
                valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                if valor2.count() != 0:
                    valor2 = valor2[0].Valor
                    nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                else:
                    valor2 = mat.PromedioParcial(nota2)
                    nots2.append([nota2, nots3, valor2])
                nots3 = []
            valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
            if valor1.count() != 0:
                valor1 = valor1[0].Valor
                nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
            else:
                valor1 = mat.PromedioParcial(nota1)
                nots1.append([nota1, nots2, valor1])
            nots2 = []
        registros.append([mat, nots1])
        nots1 = []

        ident = []
        formulas = []
        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')

        for nota in notas:
            ident.append(nota)

        for nota in notas1:
            subnotas = Nota.objects.filter(NotaPadre=nota)
            k = 1
            formula = ''
            suma = Decimal('0.00')
            for subnota in subnotas:
                if k == 1:
                    formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
                else:
                    formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
                k += 1
                suma += Decimal(nota.Peso)
            formula += ')/%s' % str(suma)
            formulas.append(formula)

        # return render_to_response("Campus/notas_curso.html",
        #                           {"periodocurso": periodocurso, "titulos": mark_safe(titulos),
        #                            "celdas": mark_safe(celdas), "nivel_cero": nivel_cero, "notas": ident,
        #                            "formulas": formulas, "activado": activado, "user": request.user},
        #                           context_instance=RequestContext(request))
        return render_to_response("Campus/notas_curso.html",
                                  {"periodocurso": periodocurso, "notas": ident, "formulas": formulas,
                                   "registros": registros, "activado": activado, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return render_to_response("Campus/mensaje.html")


def ver_notas_periodocurso(request):
    if request.user.is_authenticated():
        periodocurso_id = request.GET.get('p', '')
        estudiante_id = request.GET.get('u', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        notas = Nota.objects.filter(PeriodoCurso__id=periodocurso_id).order_by('id')

        alumnos = MatriculaCursos.objects.filter(PeriodoCurso__id=periodocurso_id,
                                                 MatriculaCiclo__Alumno__id=estudiante_id)
        n_alumnos = alumnos.count()
        paginator = Paginator(alumnos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for alumno in alumnos:
            ident = [i]
            ident1 = []
            if notas.count() != 0:
                for cal1 in notas:
                    if cal1.Nota in ident1:
                        pass
                    else:
                        if cal1.Nivel == "0":
                            ultima_nota = cal1.Nota
                            subnotas1 = Nota.objects.filter(NotaPadre=cal1)
                            try:
                                nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=alumno)
                                nota_primer_nivel = nota.Valor
                            except NotasAlumno.DoesNotExist:
                                nota = NotasAlumno(Nota=cal1, MatriculaCursos=alumno, Valor=Decimal(alumno.Promedio()))
                                nota.save()
                                nota_primer_nivel = nota.Valor
                        else:
                            subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                            if subnotas2.count() == 0:
                                try:
                                    nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=alumno)
                                    ident.append(str(nota.Valor))
                                except NotasAlumno.DoesNotExist:
                                    nota = NotasAlumno(Nota=cal1, MatriculaCursos=alumno, Valor=Decimal("0.00"))
                                    nota.save()
                                    ident.append(str(nota.Valor))
                                ident1.append(cal1.Nota)
                            else:
                                for cal2 in subnotas2:
                                    subnotas3 = Nota.objects.filter(NotaPadre=cal2)
                                    if subnotas3.count() == 0:
                                        try:
                                            nota = NotasAlumno.objects.get(Nota=cal2, MatriculaCursos=alumno)
                                            ident.append(str(nota.Valor))
                                        except NotasAlumno.DoesNotExist:
                                            nota = NotasAlumno(Nota=cal2, MatriculaCursos=alumno, Valor=Decimal("0.00"))
                                            nota.save()
                                            ident.append(str(nota.Valor))
                                        ident1.append(cal2.Nota)
                                    else:
                                        for cal3 in subnotas3:
                                            try:
                                                nota = NotasAlumno.objects.get(Nota=cal3, MatriculaCursos=alumno)
                                                ident.append(str(nota.Valor))
                                            except NotasAlumno.DoesNotExist:
                                                nota = NotasAlumno(Nota=cal3, MatriculaCursos=alumno,
                                                                   Valor=Decimal("0.00"))
                                                nota.save()
                                                ident.append(str(nota.Valor))
                                            ident1.append(cal3.Nota)
                                        ident1.append(cal2.Nota)
                                ident1.append(cal1.Nota)
                ident1.append(ultima_nota)
                ident.append(str(nota_primer_nivel))
            fila = {"id": alumno.id, "cell": ident}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_alumnos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ficha_matricula(request, anio, semestre):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')
        cursos = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno__id=request.user.id,
                                                PeriodoCurso__Periodo__Anio=anio,
                                                PeriodoCurso__Periodo__Semestre=semestre)
        creditos = 0
        for curso in cursos:
            creditos += curso.PeriodoCurso.Curso.Creditos

        return render_to_response("Campus/ficha_matricula.html",
                                  {"cursos": cursos, "estudiante": estudiante, "anio": anio, "semestre": semestre,
                                   "creditos": creditos})
    else:
        return render_to_response("Campus/mensaje.html")


def ficha_notas(request, anio, semestre):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')
        cursos = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno__id=request.user.id,
                                                PeriodoCurso__Periodo__Anio=anio,
                                                PeriodoCurso__Periodo__Semestre=semestre)
        # deudas = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__id=request.user.id,
        #                                    MatriculaCiclo__Periodo__Anio=anio,
        #                                    MatriculaCiclo__Periodo__Semestre=semestre,
        #                                    ConceptoPago__Descripcion__icontains='Pension')
        # Verificar si debe la quinta pensión
        deudas = PagoAlumno.objects.filter(MatriculaCiclo__Alumno__id=request.user.id,
                                           MatriculaCiclo__Periodo__Anio=anio,
                                           MatriculaCiclo__Periodo__Semestre=semestre,
                                           ConceptoPago__Descripcion__icontains='5ta Pension')

        if deudas.count() != 0:
            if str(deudas[0].DeudaMonto()) != "0.00":
                activado = False
            else:
                activado = True
        else:
            activado = True

        creditos = 0
        cursos_aprobados = 0
        cursos_desaprobados = 0
        creditos_aprobados = 0
        creditos_desaprobados = 0
        suma_productos = 0
        ponderado = Decimal('0.00')
        for curso in cursos:
            creditos += curso.PeriodoCurso.Curso.Creditos
            if curso.aprobado() is True:
                cursos_aprobados += 1
                creditos_aprobados += curso.PeriodoCurso.Curso.Creditos
            else:
                cursos_desaprobados += 1
                creditos_desaprobados += curso.PeriodoCurso.Curso.Creditos
            suma_productos += curso.PeriodoCurso.Curso.Creditos * curso.Promedio()
        division = suma_productos / creditos
        ponderado = Decimal("%.2f" % division)

        return render_to_response("Campus/ficha_notas.html",
                                  {"cursos": cursos, "estudiante": estudiante, "anio": anio, "semestre": semestre,
                                   "creditos": creditos, "ponderado": ponderado,
                                   "creditos_aprobados": creditos_aprobados,
                                   "creditos_desaprobados": creditos_desaprobados, "cursos_aprobados": cursos_aprobados,
                                   "cursos_desaprobados": cursos_desaprobados, "activado": activado})
    else:
        return render_to_response("Campus/mensaje.html")


def cuenta_corriente(request, anio, semestre):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
            matricula = MatriculaCiclo.objects.get(Periodo__Anio=anio, Periodo__Semestre=semestre, Alumno=estudiante)
        except Alumno.DoesNotExist, MatriculaCiclo.DoesNotExist:
            return HttpResponse('')
        return render_to_response("Campus/cuenta_corriente.html",
                                  {"matricula": matricula, "estudiante": estudiante, "anio": anio,
                                   "semestre": semestre})
    else:
        return render_to_response("Campus/mensaje.html")


def ver_pagos(request):
    if request.user.is_authenticated():
        anio = request.GET.get('p', '')
        semestre = request.GET.get('s', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__Anio=anio,
                                          MatriculaCiclo__Periodo__Semestre=semestre,
                                          MatriculaCiclo__Alumno__id=request.user.id, Estado=True)

        n_pagos = pagos.count()
        paginator = Paginator(pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.ConceptoPago.PagoBanco is True:
                lugar_pago = "Banco"
            else:
                lugar_pago = "Caja"
            fila = {"id": r.id, "cell": [i, str(r.ConceptoPago), r.Monto(), 'S./ %s' % str(r.DeudaMonto()), r.Mora(),
                                         'S./ %s' % r.DeudaMora(), str(r.ConceptoPago.FechaVencimiento), lugar_pago]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ver_detalle_pagos(request):
    if request.user.is_authenticated():
        id_pago = request.GET.get('id', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        detalle_pagos = DetallePago.objects.filter(PagoAlumno__id=id_pago).order_by('-FechaPago')

        n_detalle_pagos = detalle_pagos.count()
        paginator = Paginator(detalle_pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.Mora is True:
                mora = "Si"
            else:
                mora = "No"
            fila = {"id": r.id,
                    "cell": [i, str(r.Comprobante), 'S./ %s' % str(r.MontoDeuda), 'S./ %s' % r.MontoCancelado,
                             'S./ %s' % r.PagoRestante, str(r.FechaPago), mora]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_detalle_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ver_deudas(request):
    if request.user.is_authenticated():
        anio = request.GET.get('p', '')
        semestre = request.GET.get('s', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__Anio=anio,
                                          MatriculaCiclo__Periodo__Semestre=semestre,
                                          MatriculaCiclo__Alumno__id=request.user.id, Estado=False)

        n_pagos = pagos.count()
        paginator = Paginator(pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.ConceptoPago.PagoBanco is True:
                lugar_pago = "Banco"
            else:
                lugar_pago = "Caja"
            fila = {"id": r.id, "cell": [i, str(r.ConceptoPago), r.Monto(), 'S./ %s' % str(r.DeudaMonto()), r.Mora(),
                                         'S./ %s' % r.DeudaMora(), str(r.ConceptoPago.FechaVencimiento), lugar_pago]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")
