# -*- coding: utf-8 -*-

import hashlib
from decimal import Decimal

import simplejson
from django import forms
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.forms.utils import ErrorList
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect
from reportlab.lib.units import cm
# import reportlab
from reportlab.pdfgen import canvas

from Alumnos.models import Alumno
from Asistencias.models import Asistencia
from Campus.formatos import formato_boleta_notas, formato_boleta_notas1, ficha_matricula, ficha_matricula2
from Campus.models import MoodleUser
from Cursos.models import MaterialCurso
from Cursos.models import PeriodoCurso, SilaboCurso
from Horarios.models import Horario
# imports tree cursos.
from Matriculas.models import MatriculaCursos, MatriculaCiclo
from Mensajes.models import Destinatarios
from Notas.models import NotasAlumno, Nota
from Pagos.models import PagoAlumno, DetallePago
# nuevos imports new campus
from Periodos.models import Periodo
from UDL import settings

ID_PERIODO = 23

# excluidos de encuesta
cursos_excluidos = []

CODIGOS_CHIRIACO = ["140003c", "140015a", "140017d", "140023d", "140029b", "140030k", "140047k", "140054g", "140055c",
                    "140084c", "140090c", "140104d", "145501a", "145502h", "145503d", "145504k",
                    "146501e", "146502a", "147070h", "147071d", "147072k", "147073g", "149006f", "131003d", "131006c",
                    "131029c", "131037f", "131038b", "131045i", "131053a", "131061d", "131063g",
                    "131068i", "131072f", "131085k", "147059d", "147060b", "147062e", "147064h", "147065d", "147066k",
                    "147068c", "130001h", "130002d", "130004g", "130007f", "130009i", "130010g",
                    "130013f", "130019d", "130024h", "130025d", "130032k", "130048d", "130050i", "130054d", "130058j",
                    "130063c", "130064j", "130089b", "130091g", "130098a", "130105h", "130106d",
                    "130109c", "130120g", "137537k", "137538g", "137539c", "137540a", "137541h", "admin", "140056a"]


def estadoalumno(usuario):
    deudas = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__user_ptr=usuario.id,
                                       MatriculaCiclo__Periodo=ID_PERIODO, Prorroga=0,
                                       ConceptoPago__Descripcion__icontains='4ta Pensión')
    if deudas.count() != 0:
        if str(deudas[0].DeudaMonto()) != "0.00":
            activado = False
        else:
            activado = True
    else:
        activado = True
    return activado


class LoginForm2(forms.Form):
    Usuario = forms.CharField(max_length=7, min_length=7, label="Usuario", required=True)
    Password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")
        if user.lower() in CODIGOS_CHIRIACO:
            return cleaned_data

        if user and passwd:
            try:
                validar_usuario = MoodleUser.objects.using('moodle').get(username=user)
                usuario_udl = User.objects.get(username=user)

                try:
                    usuario = MoodleUser.objects.using('moodle').get(username=user,
                                                                     password=hashlib.md5(passwd).hexdigest())
                    usuario_udl.set_password(passwd)
                    usuario_udl.save()
                except MoodleUser.DoesNotExist:
                    msg = u'Usuario Incorrecto'
                    msg1 = u'Contraseña Incorrecta'
                    self._errors["Usuario"] = ErrorList([msg])
                    self._errors["Password"] = ErrorList([msg1])
                    del cleaned_data["Usuario"]
                    del cleaned_data["Password"]
            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]

        return cleaned_data


@csrf_protect
def login_campus2(request):
    if request.method == 'POST':
        form = LoginForm2(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['Usuario']
            passwd = form.cleaned_data['Password']
            if usuario.lower() in CODIGOS_CHIRIACO:
                return render_to_response('asdf.html', {"usuario": usuario.lower(), "clave": passwd},
                                          context_instance=RequestContext(request))
            user = authenticate(username=usuario, password=passwd)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('profile2/')
    else:
        form = LoginForm2()

    return render_to_response("NewCampus/login.html", {"form": form}, context_instance=RequestContext(request))


def profile_usuario2(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')
        periodo = Periodo.objects.get(id=ID_PERIODO)

        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
                                                MatriculaCiclo__Alumno__user_ptr=request.user.id, Estado=True,
                                                FechaRetiro=None)

        saludos = {'1': 'Que tal', '2': 'Bienvenido(a)', '3': 'Como le va', '4': 'saludo'}
        import random
        numero = random.choice('1234')
        if int(numero) == 4:
            import datetime
            now = datetime.datetime.now()
            doce = now.replace(hour=12, minute=0, second=0, microsecond=0)
            siete = now.replace(hour=19, minute=0, second=0, microsecond=0)
            media_noche = now.replace(hour=00, minute=0, second=0, microsecond=0)
            if doce > now > media_noche:
                saludo = 'Buenos días'
            elif siete > now > doce:
                saludo = 'Buenas tardes'
            elif siete < now < media_noche:
                saludo = 'Buenas noches'
            else:
                saludo = 'Buenas noches'
        else:
            saludo = saludos[numero]

        # Para bloqueo a morosos
        periodo = Periodo.objects.get(id=ID_PERIODO)
        deudas = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__id=request.user.id,
                                           MatriculaCiclo__Periodo=periodo, Prorroga=0,
                                           ConceptoPago__Descripcion__icontains='4ta Pensión')

        # Anio de ingreso
        anioingreso = int(estudiante.AnioIngreso)
        codigocarrera = str(estudiante.Carrera.Codigo)
        urlacadsis = settings.URL_ACADSIS

        if deudas.count() != 0:
            if str(deudas[0].DeudaMonto()) != "0.00":
                activado = False
            else:
                activado = True
        else:
            activado = True

        # Encuestas en línea
        # cursos_mat = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
        #                                             MatriculaCiclo__Alumno__user_ptr=request.user.id, Encuesta=False,
        #                                             FechaRetiro=None).exclude(PeriodoCurso__id__in=cursos_excluidos)
        #
        # if cursos_mat.count() > 0:
        #     return HttpResponseRedirect(settings.URL_ACADSIS + "/encuesta_enlinea/")

        if activado:
            return render_to_response("NewCampus/profile_usuario.html",
                                      {"saludo": saludo, "results": cursos, "estudiante": estudiante,
                                       "periodo": periodo, "var": var, "user": request.user,
                                       "anioingreso": anioingreso, "codigocarrera": codigocarrera,
                                       "urlacadsis": urlacadsis},
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("NewCampus/profile_usuario_moroso.html",
                                      {"saludo": saludo, "results": cursos, "estudiante": estudiante,
                                       "periodo": periodo, "var": var, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../')


def cursos_semestre(request):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        var = request.GET.get('q', 'l')
        periodo = Periodo.objects.get(id=ID_PERIODO)

        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
                                                MatriculaCiclo__Alumno__user_ptr=request.user.id, FechaRetiro=None)

        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')

        info_cursos = []
        for curso in cursos:
            horarios = Horario.objects.filter(PeriodoCurso=curso.PeriodoCurso)
            info_cursos.append([curso, horarios])

        return render_to_response("NewCampus/cursos_disponibles.html",
                                  {"results": info_cursos, "var": var, "periodo": periodo, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def cursos_historial_semestre(request):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')

        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        var = request.GET.get('q', 'hs')
        var_ciclo = request.GET.get('ciclo', '')
        if var_ciclo != '':
            periodo = Periodo.objects.get(id=var_ciclo)
            periodo_actual = Periodo.objects.get(id=ID_PERIODO)
        else:
            periodo = Periodo.objects.get(id=ID_PERIODO)
            periodo_actual = periodo
        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
                                                MatriculaCiclo__Alumno__user_ptr=request.user.id, FechaRetiro=None)

        info_cursos = []
        for curso in cursos:
            horarios = Horario.objects.filter(PeriodoCurso=curso.PeriodoCurso)
            info_cursos.append([curso, horarios])

        ciclos = MatriculaCiclo.objects.order_by('id').filter(Alumno=estudiante, Estado='Matriculado')
        return render_to_response("NewCampus/cursos_disponibles_historial.html",
                                  {"results": info_cursos,
                                   "var": var,
                                   "periodo": periodo_actual,
                                   "periodo_select": periodo,
                                   "user": request.user,
                                   "ciclos": ciclos}, context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('../../')


# Descargar asistencia
def cur_his_sem_dow_asistencia(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponse('')
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=reporte_asistencias.pdf'
        # p = canvas.Canvas(response, pagesize = A4)
        # horizontal
        p = canvas.Canvas(response, pagesize=(29.7 * cm, 21 * cm))
        # Por defecto se trabaja en 72dpi 72 ppp: 595x842
        altoA4 = 595
        anchoA4 = 842
        margenLeft = 20
        margenRight = 60

        periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        id_matriculaciclo = MatriculaCiclo.objects.get(Periodo_id=periodocurso.Periodo_id, Alumno_id=estudiante.id)
        cur = MatriculaCursos.objects.get(PeriodoCurso_id=id_periodocurso, MatriculaCiclo_id=id_matriculaciclo)

        # aqui se construye todo el header y footer
        p.setFont("Helvetica", 12)
        p.drawString(330, 540, "EVALUACIÓN Y REGISTRO")
        p.drawString(345, 520, "Reporte de Asistencias")

        p.setFont("Helvetica-Bold", 9)
        p.drawString(40, 500, "Código :")
        p.drawString(40, 480, "Estudiante :")
        p.drawString(370, 500, "Carrera :")
        p.drawString(370, 480, "Semestre :")

        p.setFont("Helvetica", 9)

        codigo = estudiante.Codigo
        alumno = estudiante.Nombres + " " + estudiante.ApellidoPaterno + " " + estudiante.ApellidoMaterno
        carrera = str(estudiante.Carrera)
        periodo = str(periodocurso.Periodo.Anio + "-" + periodocurso.Periodo.Semestre)

        p.drawString(100, 500, codigo)
        p.drawString(100, 480, estudiante.Nombres + " " + estudiante.ApellidoPaterno + " " + estudiante.ApellidoMaterno)
        p.drawString(430, 500, carrera)
        p.drawString(430, 480, str(periodo))

        p.setStrokeColorRGB(0.70588, 0.01176, 0.05098)
        p.line(70, 40, 235, 40)
        p.drawString(75, 25, "Responsable de la Carrera Profesional")

        # lineas de fecha
        p.line(380, 40, 470, 40)
        p.line(410, 43, 415, 53)  # slash 1
        p.line(440, 43, 445, 53)  # slash 2
        # --------------------------
        # Logo
        # --------------------------
        logo = settings.MEDIA_ROOT + 'logo.png'
        # logo = '/home/proyectos_django/UDL/media/Imagenes/logoHD.png'
        p.drawImage(logo, 30, 520, width=140, height=70)
        # c.drawImage(logo,30,520,width=140,height=62)

        i = 420  # altura a la que empezamos a escribir el encabezado de cada curso
        # j = 400

        if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
            # escribir el encabezado de cada curso
            p.setFont("Helvetica-Bold", 8)
            p.drawString(50, i + 40, "Curso :")
            p.drawString(280, i + 40, "Créditos :")
            p.drawString(380, i + 40, "Horario :")
            p.drawString(50, i + 30, "Docente :")
            p.drawString(280, i + 30, "Ciclo :")

            p.setFont("Helvetica", 8)
            p.drawString(110, i + 40, cur.PeriodoCurso.Curso.Nombre)
            p.drawString(340, i + 40, str(cur.PeriodoCurso.Curso.Creditos))
            p.drawString(110, i + 30, str(cur.PeriodoCurso.Docente))
            p.drawString(340, i + 30, u'%s º' % cur.PeriodoCurso.Curso.Ciclo)

            # recorremos e imprimimos los horarios del curso
            horarios = cur.PeriodoCurso.horario_set.all()
            h = 0
            alumno = cur
            total_asi = 0

            for hor in horarios:
                p.drawString(420, i + 40 - h, hor.Etiqueta())
                h += 10

                # buscamos el alumno que registra mas asistencias en el curso para tomarlo como referencia
                grupos = Horario.objects.filter(PeriodoCurso__Periodo=hor.PeriodoCurso.Periodo, Dia=hor.Dia,
                                                HoraInicio=hor.HoraInicio, HoraFin=hor.HoraFin,
                                                Seccion=hor.Seccion).values('PeriodoCurso__id').distinct()

                for g in grupos:
                    matriculados = MatriculaCursos.objects.filter(PeriodoCurso__id=g['PeriodoCurso__id'])
                    for mat1 in matriculados:
                        if total_asi > mat1.asistencia_set.all().count():
                            total_asi = mat1.asistencia_set.all().count()
                            alumno = mat1

            # obtenemos todas las fechas en las que se ha tomado asistencia en el curso
            fechas = Asistencia.objects.filter(MatriculaCursos=alumno).order_by('Fecha');

            # imprimirmos el % de asistencias, faltas y tardanzas del alumno
            p.setFont("Helvetica-Bold", 8)
            p.drawString(50, i + 20, "Asistencias :")
            p.drawString(280, i + 20, "Faltas :")
            p.drawString(380, i + 20, "Tardanzas :")

            p.setFont("Helvetica", 8)

            if len(fechas) != 0:
                # p.drawString(110,i + 20,str((cur.NroAsistencias() * 100) / len(fechas)) + ' %')
                # p.drawString(340,i + 20,str((cur.NroFaltas() * 100) / len(fechas)) + ' %')
                # p.drawString(440,i + 20,str((cur.NroTardanzas() * 100) / len(fechas)) + ' %')
                if cur.PorcentajeAsistencias() > 100:
                    p.drawString(110, i + 20, '100' + ' %')
                else:
                    p.drawString(110, i + 20, str(cur.PorcentajeAsistencias()) + ' %')
                p.drawString(340, i + 20, str(cur.PorcentajeFaltas()) + ' %')
                p.drawString(440, i + 20, str((cur.NroTardanzas() * 100) / len(fechas)) + ' %')
            else:
                t = cur.NroAsistencias() + cur.NroFaltas() + cur.NroTardanzas()
                p.drawString(110, i + 20, str(len(fechas)) + ' %')
                p.drawString(340, i + 20, str(len(fechas)) + ' %')
                p.drawString(440, i + 20, str(len(fechas)) + ' %')

            p.line(40, i + 10, anchoA4 - margenRight, i + 10)  # linea horizontal superior
            p.line(40, i - 45, anchoA4 - margenRight, i - 45)  # linea horizontal media
            p.line(40, i - 60, anchoA4 - margenRight, i - 60)  # linea horizontal inferior

            p.line(40, i + 10, 40, i - 60)  # linea vertical izquierda
            p.line(anchoA4 - margenRight, i + 10, anchoA4 - margenRight, i - 60)  # linea vertical derecha

            # imprimimos las fechas y el estado de las asistencias del alumno
            o = 0
            q = -50
            for fecha in fechas:
                p.saveState()
                p.rotate(90)
                p.drawString(i - 40, q - o, str(fecha.Fecha))
                p.restoreState()
                try:
                    asistencia = Asistencia.objects.get(MatriculaCursos=cur, Fecha=fecha.Fecha,
                                                        Horario__id=fecha.Horario_id)
                    p.drawString(-q + o - 5, i - 55, asistencia.Estado[0])
                except ValueError:
                    p.drawString(-q + o - 5, i - 55, '*')
                o += 15
            q += 100

            i = i - 130

        p.showPage()
        p.save()
        return response


def print_ficha_matricula(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponse('')
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=fichas_matricula.pdf'
        # p = canvas.Canvas(response, pagesize = (21*cm, 29.7*cm*.5))
        p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))

        # dibujar ficha inferior
        p = ficha_matricula(p)
        p.setFont("Helvetica-Bold", 10)

        periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        id_matriculaciclo = MatriculaCiclo.objects.get(Periodo_id=id_periodocurso, Alumno_id=estudiante.id)
        cursos = MatriculaCursos.objects.filter(MatriculaCiclo__id=id_matriculaciclo.id)

        codigo = estudiante.Codigo
        alumno = estudiante.Nombres + " " + estudiante.ApellidoPaterno + " " + estudiante.ApellidoMaterno
        carrera = str(estudiante.Carrera)
        varperiodo = Periodo.objects.get(id=id_periodocurso)
        periodo = str(varperiodo.Anio + "-" + varperiodo.Semestre)

        p.drawString(500, 335, codigo)
        p.drawString(500, 315, periodo)
        p.drawString(100, 315, carrera)
        p.drawString(100, 335, estudiante.Nombres + " " + estudiante.ApellidoPaterno + " " + estudiante.ApellidoMaterno)
        i = 265
        creditos = 0

        for cur in cursos:
            if cur.FechaRetiro is not None:
                continue
            if cur.Convalidado is False and cur.Estado is True:
                # codigo del curso
                p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                # grupo horario del curso
                p.drawString(181, i, cur.PeriodoCurso.Grupo)
                # nombre del curso, creditos y total de creditos
                p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                if cur.PeriodoCurso.Curso.Extracurricular is True:
                    p.drawString(529, i, str('---'))
                else:
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                # i = i - 18
                i = i - 20
        p.drawString(529, 95, str(creditos))

        # dibujar ficha superior
        w = 420
        p = ficha_matricula2(p)
        p.setFont("Helvetica-Bold", 10)

        p.drawString(500, 335 + w, codigo)
        p.drawString(500, 315 + w, periodo)
        p.drawString(100, 315 + w, carrera)
        p.drawString(100, 335 + w, alumno)

        i = 265 + w
        creditos = 0
        for cur in cursos:
            if cur.FechaRetiro is not None:
                continue
            if cur.Convalidado is False and cur.Estado is True:
                # codigo del curso
                p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                # grupo horario del curso
                p.drawString(181, i, cur.PeriodoCurso.Grupo)
                # nombre del curso, creditos y total de creditos
                p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                if cur.PeriodoCurso.Curso.Extracurricular is True:
                    p.drawString(529, i, str('---'))
                else:
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                # i = i - 18
                i = i - 20
        p.drawString(529, 95 + w, str(creditos))

        p.showPage()
        p.save()
        return response

    return HttpResponseRedirect('../../')


# Descargar historial de notas
# Posibles valores de type: unique, ponderado
def cur_his_sem_dow_notas(request, type, id_periodocurso):
    if type == "unique" or type == "ponderado":
        if request.user.is_authenticated():
            # verificar estado de alumno
            estado = estadoalumno(request.user)
            if estado is False:
                return HttpResponse('')
            try:
                estudiante = Alumno.objects.get(user_ptr=request.user.id)
            except Alumno.DoesNotExist:
                return HttpResponse('')

            # Generar pdf con notas
            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=ficha_notas.pdf'
            # p = canvas.Canvas(response, pagesize = (21*cm, 29.7*cm*.5))
            p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))

            cursos = []

            if type == "unique":
                periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
                id_matriculaciclo = MatriculaCiclo.objects.get(Periodo_id=periodocurso.Periodo_id,
                                                               Alumno_id=estudiante.id)
                cursos = MatriculaCursos.objects.filter(PeriodoCurso_id=id_periodocurso,
                                                        MatriculaCiclo_id=id_matriculaciclo)
            else:
                # la variable id_periodocurso para a ser id_periodo
                id_matriculaciclo = MatriculaCiclo.objects.get(Periodo_id=id_periodocurso, Alumno_id=estudiante.id)
                cursos = MatriculaCursos.objects.filter(MatriculaCiclo__id=id_matriculaciclo.id)

            try:
                # dibujar ficha inferior
                p = formato_boleta_notas(p, type)
                p.setFont("Helvetica-Bold", 10)

                codigo = estudiante.Codigo

                alumno = estudiante.Nombres + " " + estudiante.ApellidoPaterno + " " + estudiante.ApellidoMaterno
                carrera = str(estudiante.Carrera)
                if type == "unique":
                    periodo = str(periodocurso.Periodo.Anio + "-" + periodocurso.Periodo.Semestre)
                else:
                    # la variable id_periodocurso para a ser id_periodo
                    varperiodo = Periodo.objects.get(id=id_periodocurso)
                    periodo = str(varperiodo.Anio + "-" + varperiodo.Semestre)

                p.drawString(500, 335, codigo)
                p.drawString(500, 315, periodo)
                p.drawString(100, 315, carrera)
                p.drawString(100, 335, alumno)

                i = 265
                j = 1
                creditos = 0
                suma_producto = 0
                creditos_aprobados = 0

                for cur in cursos:
                    if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
                        # numero
                        if j < 10:
                            p.drawString(50, i, '0' + str(j))
                        else:
                            p.drawString(50, i, str(j))
                        # codigo del curso
                        p.drawString(72, i, cur.PeriodoCurso.Curso.Codigo)
                        p.drawString(122, i, cur.PeriodoCurso.Grupo)
                        # nombre del curso, creditos, promedio de curso

                        p.drawString(180, i, cur.PeriodoCurso.Curso.Nombre)

                        p.drawString(462, i, str(cur.PeriodoCurso.Curso.Creditos))

                        p.drawString(514, i, str(cur.Promedio()))
                        # total de creditos
                        creditos += cur.PeriodoCurso.Curso.Creditos
                        # creditos aprobados
                        if cur.aprobado() is True:
                            creditos_aprobados += int(cur.PeriodoCurso.Curso.Creditos)
                        suma_producto += int(cur.PeriodoCurso.Curso.Creditos) * cur.Promedio()
                        i = i - 20
                        j += 1

                # ponderado, total de creditos y creditos aprobados

                ponderado = suma_producto / creditos
                creditos_desaprobados = int(creditos) - int(creditos_aprobados)
                if type == "ponderado":
                    p.drawString(170, 95, str(creditos_aprobados))
                    p.drawString(170, 75, str(creditos_desaprobados))
                p.drawString(514, 95, "%.2f" % ponderado)

                # dibujar ficha superior
                w = 420
                p = formato_boleta_notas1(p, type)
                p.setFont("Helvetica-Bold", 10)

                codigo = estudiante.Codigo
                alumno = estudiante.Nombres + " " + estudiante.ApellidoPaterno + " " + estudiante.ApellidoMaterno
                carrera = str(estudiante.Carrera)
                if type == "unique":
                    periodo = str(periodocurso.Periodo.Anio + "-" + periodocurso.Periodo.Semestre)
                else:
                    # la variable id_periodocurso para a ser id_periodo
                    varperiodo = Periodo.objects.get(id=id_periodocurso)
                    periodo = str(varperiodo.Anio + "-" + varperiodo.Semestre)

                p.drawString(500, 335 + w, codigo)
                p.drawString(500, 315 + w, periodo)
                p.drawString(100, 315 + w, carrera)
                p.drawString(100, 335 + w, alumno)

                i = 265 + w
                j = 1
                creditos = 0
                suma_producto = 0
                creditos_aprobados = 0

                for cur in cursos:
                    if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
                        # numero
                        if j < 10:
                            p.drawString(50, i, '0' + str(j))
                        else:
                            p.drawString(50, i, str(j))
                        # codigo del curso
                        p.drawString(72, i, cur.PeriodoCurso.Curso.Codigo)
                        p.drawString(122, i, cur.PeriodoCurso.Grupo)
                        # nombre del curso, creditos, promedio de curso

                        p.drawString(180, i, cur.PeriodoCurso.Curso.Nombre)

                        p.drawString(462, i, str(cur.PeriodoCurso.Curso.Creditos))

                        p.drawString(514, i, str(cur.Promedio()))
                        # total de creditos
                        creditos += cur.PeriodoCurso.Curso.Creditos
                        # creditos aprobados
                        if cur.aprobado() is True:
                            creditos_aprobados += int(cur.PeriodoCurso.Curso.Creditos)
                        suma_producto += int(cur.PeriodoCurso.Curso.Creditos) * cur.Promedio()
                        i = i - 20
                        j += 1

                # ponderado, total de creditos y creditos aprobados
                ponderado = suma_producto / creditos
                creditos_desaprobados = int(creditos) - int(creditos_aprobados)
                if type == "ponderado":
                    p.drawString(170, 95 + w, str(creditos_aprobados))
                    p.drawString(170, 75 + w, str(creditos_desaprobados))
                p.drawString(514, 95 + w, "%.2f" % ponderado)

                p.showPage()
                p.save()

            except ValueError:
                p.showPage()
                p.save()

            return response

    return HttpResponseRedirect('../../')


def categoria_actual(request):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        var = request.GET.get('q', 'ca')
        periodo = Periodo.objects.get(id=ID_PERIODO)

        # verificar estado de alumno
        estado = estadoalumno(request.user)
        template_base = "NewCampus/profile_usuario.html"
        if estado is False:
            template_base = "NewCampus/profile_usuario_moroso.html"

        try:
            matricula = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "Usted no tiene asignada una categoria, verifique por favor"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("NewCampus/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("NewCampus/categoria_actual.html",
                                  {"var": var, "periodo": periodo, "matricula": matricula, "user": request.user,
                                   "template_base": template_base},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def index_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')
        var = request.GET.get('q', 'm')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("NewCampus/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        return render_to_response("NewCampus/index_curso.html",
                                  {"var": var, "periodo": periodocurso.Periodo, "periodocurso": periodocurso,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


def cuenta_corriente2(request):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        template_base = "NewCampus/profile_usuario.html"
        if estado is False:
            template_base = "NewCampus/profile_usuario_moroso.html"
        var = request.GET.get('q', 'f')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
            matricula = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except (Alumno.DoesNotExist, MatriculaCiclo.DoesNotExist):
            matricula = []
        return render_to_response("NewCampus/cuenta_corriente.html",
                                  {"var": var, "matricula": matricula, "estudiante": estudiante, "periodo": periodo,
                                   "template_base": template_base},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def ver_pagos2(request):
    if request.user.is_authenticated():
        anio = request.GET.get('p', '')
        semestre = request.GET.get('s', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__Anio=anio,
                                          MatriculaCiclo__Periodo__Semestre=semestre,
                                          MatriculaCiclo__Alumno__id=request.user.id, Estado=True)

        n_pagos = pagos.count()
        paginator = Paginator(pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.ConceptoPago.PagoBanco is True:
                lugar_pago = "Banco"
            else:
                lugar_pago = "Caja"
            fila = {"id": r.id, "cell": [i, str(r.ConceptoPago), r.Monto(), 'S./ %s' % str(r.DeudaMonto()), r.Mora(),
                                         'S./ %s' % r.DeudaMora(), str(r.ConceptoPago.FechaVencimiento), lugar_pago]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ver_detalle_pagos2(request):
    if request.user.is_authenticated():
        id_pago = request.GET.get('id', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        detalle_pagos = DetallePago.objects.filter(PagoAlumno__id=id_pago).order_by('-FechaPago')

        n_detalle_pagos = detalle_pagos.count()
        paginator = Paginator(detalle_pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.Mora is True:
                mora = "Si"
            else:
                mora = "No"
            fila = {"id": r.id,
                    "cell": [i, str(r.Comprobante), 'S./ %s' % str(r.MontoDeuda), 'S./ %s' % r.MontoCancelado,
                             'S./ %s' % r.PagoRestante, str(r.FechaPago), mora]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_detalle_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ver_deudas2(request):
    if request.user.is_authenticated():
        anio = request.GET.get('p', '')
        semestre = request.GET.get('s', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        # pagos = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__Anio=anio,
        #                                   MatriculaCiclo__Periodo__Semestre=semestre,
        #                                   MatriculaCiclo__Alumno__id=request.user.id, Estado=False)
        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Alumno__id=request.user.id, Estado=False)

        n_pagos = pagos.count()
        paginator = Paginator(pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.ConceptoPago.PagoBanco is True:
                lugar_pago = "Banco"
            else:
                lugar_pago = "Caja"
            fila = {"id": r.id, "cell": [i, str(r.ConceptoPago), r.Monto(), 'S./ %s' % str(r.DeudaMonto()), r.Mora(),
                                         'S./ %s' % r.DeudaMora(), str(r.ConceptoPago.FechaVencimiento), lugar_pago]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), mimetype='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def boleta_notas(request):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        var = request.GET.get('q', 'n')
        var_ciclo = request.GET.get('ciclo', '')
        if var_ciclo != '':
            print(var_ciclo)
            periodo = Periodo.objects.get(id=var_ciclo)
        else:
            periodo = Periodo.objects.get(id=ID_PERIODO)

        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Curso__Extracurricular=False,
                                                MatriculaCiclo__Alumno__id=request.user.id,
                                                PeriodoCurso__Periodo=periodo, Estado=True)

        # Verificar si debe la quinta pensión
        deudas = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__id=request.user.id,
                                           MatriculaCiclo__Periodo=periodo,
                                           ConceptoPago__Descripcion__icontains='4ta Pensión')

        if deudas.count() != 0:
            if str(deudas[0].DeudaMonto()) != "0.00":
                # activado = False
                activado = True
            else:
                activado = True
        else:
            activado = True

        # if estudiante.Codigo == '117533J' or estudiante.Codigo == '150098G' \
        #         or estudiante.Codigo == '102001G' or estudiante.Codigo == '117525G':
        #     activado = True

        creditos = 0
        cursos_aprobados = 0
        cursos_desaprobados = 0
        creditos_aprobados = 0
        creditos_desaprobados = 0
        suma_productos = 0
        ponderado = Decimal('0.00')

        if cursos.count() != 0:
            matriculado = True
            for curso in cursos:
                creditos += curso.PeriodoCurso.Curso.Creditos
                if curso.aprobado() is True:
                    cursos_aprobados += 1
                    creditos_aprobados += curso.PeriodoCurso.Curso.Creditos
                else:
                    cursos_desaprobados += 1
                    creditos_desaprobados += curso.PeriodoCurso.Curso.Creditos
                suma_productos += curso.PeriodoCurso.Curso.Creditos * curso.Promedio()
            division = suma_productos / creditos
            ponderado = Decimal("%.2f" % division)
        else:
            matriculado = False

        # Ciclos que ha estudiado el alumno
        ciclos = MatriculaCiclo.objects.order_by('id').filter(Alumno=estudiante, Estado='Matriculado')

        return render_to_response("NewCampus/boleta_notas.html",
                                  {"var": var,
                                   "cursos": cursos,
                                   "estudiante": estudiante,
                                   "matriculado": matriculado,
                                   "periodo": periodo,
                                   "creditos": creditos,
                                   "ponderado": ponderado,
                                   "creditos_aprobados": creditos_aprobados,
                                   "creditos_desaprobados": creditos_desaprobados,
                                   "cursos_aprobados": cursos_aprobados,
                                   "cursos_desaprobados": cursos_desaprobados,
                                   "activado": activado,
                                   "user": request.user,
                                   "ciclos": ciclos})
    else:
        return HttpResponseRedirect('../../')


def datos_personales2(request):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        template_base = "NewCampus/profile_usuario.html"
        if estado is False:
            template_base = "NewCampus/profile_usuario_moroso.html"
        var = request.GET.get('q', 'd')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        try:
            estudiante = Alumno.objects.get(id=request.user.id)
            return render_to_response("NewCampus/datos_personales.html",
                                      {"var": var, "template_base": template_base, "estudiante": estudiante,
                                       "periodo": periodo, "user": request.user})
        except:
            return HttpResponseRedirect('../../')
    else:
        return HttpResponseRedirect('../../')


class ChangePasswordForm2(forms.Form):
    OldPassword = forms.CharField(widget=forms.PasswordInput, label="Contraseña Antigua", required=True)
    NewPassword = forms.CharField(widget=forms.PasswordInput, label="Nueva Contraseña", required=True)
    RepeatPassword = forms.CharField(widget=forms.PasswordInput, label="Repetir Contraseña", required=True)
    Usuario = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    def clean(self):
        cleaned_data = self.cleaned_data
        oldpasswd = cleaned_data.get("OldPassword")
        newpasswd = cleaned_data.get("NewPassword")
        repeatpasswd = cleaned_data.get("RepeatPassword")
        usuario = cleaned_data.get("Usuario")

        if oldpasswd and newpasswd and repeatpasswd and usuario:
            try:
                usuario_udl = User.objects.get(username=usuario)
                usuario_moodle = MoodleUser.objects.using('moodle').get(username=usuario)
                if usuario_udl.check_password(oldpasswd) is True:
                    if newpasswd == repeatpasswd:
                        usuario_udl.set_password(newpasswd)
                        # usuario_moodle.password = hashlib.md5(newpasswd + ' tvI!vpvr?,z4.7G{ +UUJWgW0NLD').hexdigest()
                        usuario_moodle.password = hashlib.md5(newpasswd).hexdigest()
                        usuario_udl.save()
                        usuario_moodle.save()
                    else:
                        msg = u'Las contraseñas no coinciden'
                        self._errors["RepeatPassword"] = ErrorList([msg])
                        del cleaned_data["RepeatPassword"]
                else:
                    msg = u'Contraseña Incorrecta'
                    self._errors["OldPassword"] = ErrorList([msg])
                    del cleaned_data["OldPassword"]
            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto, intento de acceso no autorizado'
                self._errors["OldPassword"] = ErrorList([msg])
                del cleaned_data["OldPassword"]
        return cleaned_data


def aviso_change_password2(request):
    if request.user.is_authenticated():
        return render_to_response("Campus/aviso_change_password.html", {"user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return render_to_response("NewCampus/mensaje.html")


@csrf_protect
def change_password2(request):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        template_base = "NewCampus/profile_usuario.html"
        if estado is False:
            template_base = "NewCampus/profile_usuario_moroso.html"
        var = request.GET.get('q', 'p')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        if request.method == 'POST':
            form = ChangePasswordForm2(request.POST)
            if form.is_valid():
                return render_to_response("NewCampus/cambiar_passwd_successful.html")
        else:
            form = ChangePasswordForm2(initial={'Usuario': request.user.username})

        return render_to_response("NewCampus/cambiar_passwd.html",
                                  {"var": var, "periodo": periodo, "template_base": template_base, "form": form,
                                   "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/campus')


@csrf_protect
def cerrar_sesion2(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('/campus')
    else:
        return HttpResponseRedirect('/campus')


def asistencias_curso2(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'a')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        try:
            curso = PeriodoCurso.objects.get(id=id_periodocurso)
            asistencias = Asistencia.objects.filter(MatriculaCursos__MatriculaCiclo__Alumno__id=request.user.id,
                                                    MatriculaCursos__PeriodoCurso=curso).order_by('-Fecha')
            asistio = 0
            falto = 0
            tardanza = 0
            for asi in asistencias:
                if asi.Estado == 'Asistio':
                    asistio += 1
                elif asi.Estado == 'Falto':
                    falto += 1
                elif asi.Estado == 'Tardanza':
                    tardanza += 1
            if asistencias.count() != 0:
                matriculado = asistencias[0].MatriculaCursos
                asiste = (asistio * 100) / asistencias.count()
                falta = (falto * 100) / asistencias.count()
                tarde = (tardanza * 100) / asistencias.count()
            else:
                matriculado = None
                asiste = 0
                falta = 0
                tarde = 0
            return render_to_response("NewCampus/asistencias_curso.html",
                                      {"var": var, "var1": var1, "periodocurso": curso, "asistencias": asistencias,
                                       "curso": curso, "asistio": asistio, "falto": falto, "tardanza": tardanza,
                                       "asiste": asiste, "falta": falta, "tarde": tarde, "matriculado": matriculado,
                                       "user": request.user})
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("NewCampus/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    else:
        return HttpResponseRedirect('../../../')


def notas_curso2(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')
            # return HttpResponse('')
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'g')

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("NewCampus/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        # Verificar si debe la quinta pensión
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        deudas = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__id=request.user.id,
                                           MatriculaCiclo__Periodo__Anio=periodocurso.Periodo.Anio,
                                           MatriculaCiclo__Periodo__Semestre=periodocurso.Periodo.Semestre,
                                           ConceptoPago__Descripcion__icontains='4ta Pensión')
        if deudas.count() != 0:
            if str(deudas[0].DeudaMonto()) != "0.00":
                activado = False
                # activado = True
            else:
                activado = True
        else:
            activado = True

        registros = []
        if activado is True:
            notas1 = Nota.objects.filter(PeriodoCurso=periodocurso, Nivel='1').order_by('Orden')

            nots1 = []
            nots2 = []
            nots3 = []

            mat = MatriculaCursos.objects.get(PeriodoCurso=periodocurso, MatriculaCiclo__Alumno__id=request.user.id,
                                              Estado=True, FechaRetiro=None)
            notas_creadas = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota__PeriodoCurso=periodocurso)

            if notas_creadas.count() != 0:
                cals = True
            else:
                cals = False

            if cals is True:

                for nota1 in notas1:
                    nots_2 = Nota.objects.filter(PeriodoCurso=periodocurso, NotaPadre=nota1, Nivel='2').order_by(
                        'Orden')
                    for nota2 in nots_2:
                        nots_3 = Nota.objects.filter(PeriodoCurso=periodocurso, NotaPadre=nota2, Nivel='3').order_by(
                            'Orden')
                        for nota3 in nots_3:
                            valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                            if valor3.count() != 0:
                                valor3 = valor3[0].Valor
                                nots3.append([nota3, int(str(valor3).split('.')[0])])
                            else:
                                valor3 = mat.PromedioParcial(nota3)
                                nots3.append([nota3, valor3])
                        valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                        if valor2.count() != 0:
                            valor2 = valor2[0].Valor
                            nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                        else:
                            valor2 = mat.PromedioParcial(nota2)
                            nots2.append([nota2, nots3, valor2])
                        nots3 = []
                    valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
                    if valor1.count() != 0:
                        valor1 = valor1[0].Valor
                        nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
                    else:
                        valor1 = mat.PromedioParcial(nota1)
                        nots1.append([nota1, nots2, valor1])
                    nots2 = []
                registros.append([mat, nots1])
                nots1 = []
        else:
            cals = False

        # cals = False
        ident = []
        formulas = []
        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')

        for nota in notas:
            ident.append(nota)

        for nota in notas1:
            subnotas = Nota.objects.filter(NotaPadre=nota)
            k = 1
            formula = ''
            suma = Decimal('0.00')
            for subnota in subnotas:
                if k == 1:
                    formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
                else:
                    formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
                k += 1
                suma += Decimal(nota.Peso)
            formula += ')/%s' % str(suma)
            formulas.append(formula)

        return render_to_response("NewCampus/notas_curso.html",
                                  {"var": var, "var1": var1, "cals": cals, "periodocurso": periodocurso, "notas": ident,
                                   "formulas": formulas, "registros": registros, "activado": activado,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def silabo_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')
            # return HttpResponse('')
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 's')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("NewCampus/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        try:
            silabo = SilaboCurso.objects.get(PeriodoCurso=periodocurso)
        except:
            silabo = None

        return render_to_response('NewCampus/silabo.html',
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "silabo": silabo,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def material_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        if estado is False:
            return HttpResponseRedirect('/profile2')
            # return HttpResponse('')
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'r')

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("NewCampus/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        results = MaterialCurso.objects.filter(PeriodoCurso=periodocurso).order_by('-Fecha_creacion')

        paginator = Paginator(results, 100)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)

        return render_to_response('NewCampus/material_curso.html',
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "results": results,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def mensajes_recibidos(request):
    if request.user.is_authenticated():
        # verificar estado de alumno
        estado = estadoalumno(request.user)
        template_base = "NewCampus/profile_usuario.html"
        if estado is False:
            template_base = "NewCampus/profile_usuario_moroso.html"

        var = request.GET.get('me', 'me')
        query = request.GET.get('query', '')
        mensajes_recibidos = []

        import datetime
        now = datetime.datetime.today()
        dia = now.strftime('%d')

        if dia == '04' or dia == '05' or dia == '10' or dia == '11' or dia == '18' or dia == '19' or dia == '24' or dia == '25' or dia == '29' or dia == '30':
            msg = True
        else:
            msg = False

        periodo = Periodo.objects.get(id=ID_PERIODO)
        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
            matricula = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except (Alumno.DoesNotExist, MatriculaCiclo.DoesNotExist):
            matricula = []

        if matricula != []:
            deudas = PagoAlumno.objects.filter(MatriculaCiclo=matricula, Estado=False)
            nro_deudas = deudas.count()
        else:
            deudas = []
            nro_deudas = 0

        if query:
            qset = (
                    Q(Mensaje__Asunto__icontains=query) |
                    Q(Mensaje__Contenido__icontains=query)
            )
            recibidos = Destinatarios.objects.filter(Receptor=request.user).filter(qset).order_by('-FechaCreacion')
        else:
            recibidos = Destinatarios.objects.filter(Receptor=request.user).order_by('-FechaCreacion')

        n_mensajes = recibidos.count()
        paginator = Paginator(recibidos, 10)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)
        return render_to_response("NewCampus/mensajes_recibidos.html",
                                  {"var": var, "results": results, "template_base": template_base,
                                   "paginator": paginator, "n_mensajes": n_mensajes,
                                   "deudas": deudas, "nro_deudas": nro_deudas, "estudiante": estudiante, "msg": msg,
                                   "query": query, "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')
