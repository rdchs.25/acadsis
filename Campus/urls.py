# -*- coding: utf-8 -*-
from django.conf.urls import url

from Campus.newcampus_views import *

app_name = 'Campus'

urlpatterns = [
    url(r'^$', login_campus2),
    url(r'^profile2/$', profile_usuario2),
    url(r'^profile2/cursos_semestre/$', cursos_semestre),
    url(r'^profile2/cursos_historial_semestre/$', cursos_historial_semestre),
    url(r'^profile2/cursos_historial_semestre/fichamatricula/(\d+)$', print_ficha_matricula),
    url(r'^profile2/cursos_historial_semestre/notas/(.*)/(\d+)$', cur_his_sem_dow_notas),
    url(r'^profile2/cursos_historial_semestre/asistencia/(\d+)$', cur_his_sem_dow_asistencia),
    url(r'^profile2/categoria_actual/$', categoria_actual),
    url(r'^profile2/cursos_semestre/curso/(\d+)/$', index_curso),
    url(r'^profile2/datos_personales/$', datos_personales2),
    url(r'^profile2/change_password2/$', change_password2),
    url(r'^profile2/cerrar_sesion/$', cerrar_sesion2),
    url(r'^profile2/asistencias/(\d+)/$', asistencias_curso2),
    url(r'^profile2/notas/(\d+)/$', notas_curso2),
    url(r'^profile2/silabo/(\d+)/$', silabo_curso),
    url(r'^profile2/material/(\d+)/$', material_curso),
    url(r'^profile2/boleta_notas/$', boleta_notas),
    url(r'^profile2/cuenta_corriente/$', cuenta_corriente2),
    url(r'^profile2/cuenta_corriente/pagos/$', ver_pagos2),
    url(r'^profile2/cuenta_corriente/detalle_pagos/$', ver_detalle_pagos2),
    url(r'^profile2/cuenta_corriente/deudas_pendientes/$', ver_deudas2),
    url(r'^profile2/mensajes/$', mensajes_recibidos),
]
