# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models


class MoodleUser(models.Model):
    auth = models.CharField(max_length=60)
    confirmed = models.IntegerField()
    policyagreed = models.IntegerField()
    deleted = models.IntegerField()
    mnethostid = models.BigIntegerField(unique=True)
    username = models.CharField(unique=True, max_length=254)
    password = models.CharField(max_length=96)
    idnumber = models.CharField(max_length=765)
    firstname = models.CharField(max_length=300)
    lastname = models.CharField(max_length=300)
    email = models.CharField(max_length=300)
    emailstop = models.IntegerField()
    icq = models.CharField(max_length=45)
    skype = models.CharField(max_length=150)
    yahoo = models.CharField(max_length=150)
    aim = models.CharField(max_length=150)
    msn = models.CharField(max_length=150)
    phone1 = models.CharField(max_length=60)
    phone2 = models.CharField(max_length=60)
    institution = models.CharField(max_length=120)
    department = models.CharField(max_length=90)
    address = models.CharField(max_length=210)
    city = models.CharField(max_length=60)
    country = models.CharField(max_length=6)
    lang = models.CharField(max_length=90)
    theme = models.CharField(max_length=150)
    timezone = models.CharField(max_length=300)
    firstaccess = models.BigIntegerField()
    lastaccess = models.BigIntegerField()
    lastlogin = models.BigIntegerField()
    currentlogin = models.BigIntegerField()
    lastip = models.CharField(max_length=45)
    secret = models.CharField(max_length=45)
    picture = models.IntegerField()
    url = models.CharField(max_length=765)
    description = models.TextField(blank=True)
    mailformat = models.IntegerField()
    maildigest = models.IntegerField()
    maildisplay = models.IntegerField()
    htmleditor = models.IntegerField()
    ajax = models.IntegerField()
    autosubscribe = models.IntegerField()
    trackforums = models.IntegerField()
    timemodified = models.BigIntegerField()
    trustbitmask = models.BigIntegerField()
    imagealt = models.CharField(max_length=765, blank=True)
    screenreader = models.IntegerField()

    class Meta:
        db_table = u'mdl_user'
