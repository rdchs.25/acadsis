# -*- coding: utf-8 -*-
import json
from urllib import urlencode, urlopen

from json_settings import get_settings

settings = get_settings()

URL_MOODLE = settings['URL_MOODLE']

wstoken = settings['TOKEN_MOODLE']


def moodle_webservices(wsfunction, datos=''):
    try:
        parametros = urlencode(datos)
        url = URL_MOODLE + "/webservice/rest/server.php?wstoken=" + wstoken + "&wsfunction=" + wsfunction + "&moodlewsrestformat=json"
        result = urlopen(url, parametros)
        respuesta = json.loads(result.read())
        return respuesta
    except Exception as ex:
        print (str(ex))
        return ''
