# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

from Planestudio.views import obtener_detalles_plan
from Ubicacion.views import provincia, ubigeo
from Alumnos.generar_datacarne_sunedu import generar_datacarne_sunedu
from Cursos.views import prerequisito, prerequisito2, \
    obtener_cursos
from Matriculas.views import obtener_categorias
from Matriculas.matricula_online import login_matricula_online, registrar_matricula_online, enviar_ficha_email, \
    descargar_ficha_online, logout_matricula_online
from Encuesta.encuesta_online import login_encuesta_online, logout_encuesta_online, registrar_encuesta_online
from Encuesta.autoevaluacion_online import autoevaluacion_online, logout_autoevaluacion_online, \
    registrar_autoevaluacion_online
from Pagos.views2 import concepto, obtener_conceptos
from Periodos.views import index
from Docentes.Autenticacion import logout_intranet_docentes, cambiar_passwd
from Alumnos.campus_parientes import login_campus_padres, profile_padres, profile_hijos, hijos_disponibles, \
    datos_personales_padre, notas_curso, asistencias_curso, cuenta_corriente, change_password, logout_padres, \
    editar_datos, ver_pagos, ver_detalle_pagos, mensajes_recibidos_padres, cursos_semestre, index_curso, ver_deudas
from funciones import useracadsis
# Actualizacion de datos
from Alumnos.actualizacion_datos import login_actualizar_datos, logout_actualizar_datos, registrar_actualizar_datos
# Labor docente
from Labordocente.views import obtener_labores

urlpatterns = patterns('',
                       # urls de intranet docente
                       (r'^$', index),
                       (r'logout/$', logout_intranet_docentes),
                       (r'salirsistema/$', logout_intranet_docentes),
                       (r'cambiar_passwd/$', cambiar_passwd),
                       url(r'^docente/', include('Docentes.urls')),
                       # urls para aplicacion Periodos
                       url(r'^admin/', include('Periodos.urls')),
                       # urls para aplicacion Asistencias
                       url(r'^admin/', include('Asistencias.urls')),
                       # urls para aplicacion Notas
                       url(r'^admin/', include('Notas.urls')),
                       # urls para aplicacion Labordocente
                       url(r'^admin/', include('Labordocente.urls')),
                       # urls para aplicacion Matriculas
                       url(r'^admin/', include('Matriculas.urls')),
                       # urls para aplicacion Cursos
                       url(r'^admin/', include('Cursos.urls')),
                       # urls para aplicacion Pagos
                       url(r'^admin/', include('Pagos.urls')),
                       # urls para aplicacion Categorias
                       url(r'^admin/', include('Categorias.urls')),
                       # urls para aplicacion Encuesta
                       url(r'^admin/', include('Encuesta.urls')),
                       # urls para aplicacion Alumnos
                       url(r'^admin/', include('Alumnos.urls')),
                       # urls para aplicacion Mensajes
                       url(r'^admin/', include('Mensajes.urls')),
                       # urls para campus virtual
                       url(r'^campus/', include('Campus.urls')),
                       # urls para Tramite Documentario
                       url(r'^admin/', include('Tramitedocumento.urls')),
                       # urls para Marcacion docente
                       url(r'^admin/', include('Marcacion.urls')),
                       # url para generar información para carné universitario
                       (r'^generar_informacion_sunedu/$', generar_datacarne_sunedu),
                       # urls aplicacion Alumnos
                       url(r'provincias/(?P<departamento_id>\d*)/$', provincia, name='provincia'),
                       url(r'ubigeos/(?P<provincia_id>\d*)/$', ubigeo, name='ubigeo'),
                       # urls aplicacion Cursos
                       url(r'prerequisitos/(?P<carrera_id>\d*)/$', prerequisito, name='prerequisito'),
                       url(r'prerequisitos2/(?P<carrera_id>\d*)/(?P<periodo_id>\d*)/$', prerequisito2,
                           name='prerequisito2'),
                       # url para obtener categorias
                       url(r'obtener_categorias/(?P<periodo_id>\d*)/$', obtener_categorias, name='obtener_categorias'),
                       # urls para matricula en linea
                       (r'^matricula_enlinea/$', login_matricula_online),
                       (r'^matricula_enlinea/registrar/$', registrar_matricula_online),
                       (r'^matricula_enlinea/enviar_ficha/$', enviar_ficha_email),
                       (r'^matricula_enlinea/descargar_ficha/$', descargar_ficha_online),
                       (r'^matricula_enlinea/logout/$', logout_matricula_online),
                       # urls para encuesta en linea
                       (r'^encuesta_enlinea/$', login_encuesta_online),
                       (r'^encuesta_enlinea/registrar/(\d+)/$', registrar_encuesta_online),
                       (r'^encuesta_enlinea/logout/$', logout_encuesta_online),
                       # urls para autoevalacion docente en linea
                       (r'^autoevaluacion_enlinea/$', autoevaluacion_online),
                       (r'^autoevaluacion_enlinea/registrar/(\d+)/$', registrar_autoevaluacion_online),
                       (r'^autoevaluacion_enlinea/logout/$', logout_autoevaluacion_online),
                       # urls para actualizacion de datos
                       (r'^actualizacion_datos/$', login_actualizar_datos),
                       (r'^actualizacion_datos/registrar/$', registrar_actualizar_datos),
                       (r'^actualizacion_datos/logout/$', logout_actualizar_datos),

                       url(r'obtenerconceptos/(?P<periodo_id>\d*)/(?P<pagobanco>\d*)/$', concepto, name='concepto'),
                       url(r'obtener_conceptos/(?P<periodo_id>\d*)/$', obtener_conceptos, name='concepto'),
                       url(r'obtenercursos/(?P<carrera_id>\d*)/(?P<ciclo_id>\d*)/(?P<periodo_id>\d*)/$', obtener_cursos,
                           name='cursos'),
                       url(r'obtenerdetallesplan/(?P<plan_id>\d*)/$', obtener_detalles_plan, name='detalles'),
                       # horarios de labor docente
                       url(r'obtener_labores/(?P<tipolabor_id>\d*)/(?P<periodo_id>\d*)/$', obtener_labores,
                           name='labor'),
                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),

                       # Urls de autenticacion a acadsis y escuela pre
                       (r'^useracadsis/$', useracadsis),

                       # urls para campus padres de familia
                       (r'^padres/login/$', login_campus_padres),
                       (r'cerrar_sesion/$', logout_padres),
                       (r'^padres/profile/$', profile_padres),
                       (r'^padres/profile/hijos_disponibles/$', hijos_disponibles),
                       (r'^padres/profile/cursos_disponibles/(\d+)/$', cursos_semestre),
                       (r'^padres/profile/cursos_disponibles/(\d+)/curso/(\d+)/$', index_curso),
                       (r'^padres/profile/datos_personales/$', datos_personales_padre),
                       (r'^padres/profile/datos_personales/editar/$', editar_datos),
                       (r'^padres/profile/change_password/$', change_password),
                       (r'^hijos/profile/(\d+)/$', profile_hijos),
                       (r'^cuenta_corriente/(\d+)/$', cuenta_corriente),
                       (r'^cuenta_corriente/(\d+)/pagos/$', ver_pagos),
                       (r'^cuenta_corriente/(\d+)/deudas_pendientes/$', ver_deudas),
                       (r'^cuenta_corriente/(\d+)/detalle_pagos/$', ver_detalle_pagos),
                       (r'^mensajes/(\d+)/$', mensajes_recibidos_padres),
                       (r'^notas/(\d+)/(\d+)/$', notas_curso),
                       (r'^asistencia/(\d+)/(\d+)/$', asistencias_curso),
                       )
