# -*- coding: utf-8 -*-
import json

from urllib2 import Request, urlopen

from json_settings import get_settings

settings = get_settings()

URL_FACTURACION = settings['URL_FACTURACION']

wstoken = settings['TOKEN_FACTURACION']


def facturacion_webservices(wsfunction, datos=''):
    try:
        print ("Conectado con servicios ERP, función " + wsfunction)
        print ("Datos a enviar")
        print json.dumps(datos)
        url = URL_FACTURACION + "/" + wsfunction
        req = Request(url)
        req.add_header('Token', wstoken)
        req.add_header("Content-Type", "application/json")
        req.add_data(json.dumps(datos))
        rep = urlopen(req)
        if rep.getcode() == 200:
            return json.loads(rep.read())
        else:
            return False
    except Exception as error:
        print (str(error))
        return False
