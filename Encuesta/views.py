from django.http import HttpResponseRedirect

from Encuesta.models import Encuesta, CategoriaItem, Item, Opcion
from Periodos.models import Periodo


# Create your views here.

def exportar_encuesta(request):
    if request.user.is_authenticated():
        squeryset = request.POST['queryset']
        queryset = squeryset.split('-')
        for id in queryset:
            obj = Encuesta.objects.get(pk=id)
            periodo = Periodo.objects.get(id=request.POST['Periodo'])
            name_encuesta = "Encuesta de Evaluacion Docente " + str(periodo.Anio) + "-" + str(periodo.Semestre)
            # create encuesta
            encuesta_new = Encuesta.objects.create(Nombre=name_encuesta, Periodo_id=periodo.id)
            categoria_items = CategoriaItem.objects.filter(Encuesta_id=obj.id)
            # create categoria
            for categoria_item in categoria_items:
                categoria_item_new = CategoriaItem.objects.create(Descripcion=categoria_item.Descripcion,
                                                                  Orden=categoria_item.Orden,
                                                                  Encuesta_id=encuesta_new.id)
                encuesta_items = Item.objects.filter(CategoriaItem_id=categoria_item.id)
                # create encuesta item
                for encuesta_item in encuesta_items:
                    encuesta_item_new = Item.objects.create(Descripcion=encuesta_item.Descripcion,
                                                            Orden=encuesta_item.Orden,
                                                            CategoriaItem_id=categoria_item_new.id)
                    encuesta_opciones = Opcion.objects.filter(Item_id=encuesta_item.id)
                    for encuesta_opcion in encuesta_opciones:
                        Opcion.objects.create(Descripcion=encuesta_opcion.Descripcion, Valor=encuesta_opcion.Valor,
                                              Item_id=encuesta_item_new.id)

    return HttpResponseRedirect('/admin/Encuesta/encuesta/')
