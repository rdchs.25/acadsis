# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.shortcuts import render_to_response
from django.template import RequestContext

from Encuesta.models import Encuesta, CategoriaItem, Item, Opcion
from Periodos.models import Periodo


class CategoriaItemInline(admin.TabularInline):
    model = CategoriaItem
    extra = 3


class ItemInline(admin.TabularInline):
    model = Item
    extra = 3


class OpcionInline(admin.TabularInline):
    model = Opcion
    extra = 3


class IndexCursoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class EncuestaAdmin(admin.ModelAdmin):
    inlines = (CategoriaItemInline,)
    actions = ['exportar']

    def exportar(modeladmin, request, querysets):
        lista_queryset = []
        for queryset in querysets:
            lista_queryset.append(str(queryset.id))

        squeryset = '-'.join(lista_queryset)

        form = IndexCursoForm(request.POST)
        return render_to_response('Cursos/Encuesta/index.html',
                                  {"form": form, "user": request.user, "queryset": squeryset},
                                  context_instance=RequestContext(request))

    exportar.short_description = "Exportar"


class CategoriaItemAdmin(admin.ModelAdmin):
    inlines = (ItemInline,)
    list_filter = ('Encuesta',)


class ItemAdmin(admin.ModelAdmin):
    inlines = (OpcionInline,)
    list_filter = ('CategoriaItem__Encuesta',)


admin.site.register(Opcion)
admin.site.register(Item, ItemAdmin)
admin.site.register(CategoriaItem, CategoriaItemAdmin)
admin.site.register(Encuesta, EncuestaAdmin)
