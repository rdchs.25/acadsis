# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0008_auto_20181102_0808'),
        ('Encuesta', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Respuestadocente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Valor', models.IntegerField(verbose_name=b'Valor')),
                ('Item', models.ForeignKey(verbose_name=b'Item', to='Encuesta.Item')),
                ('Opcion', models.ForeignKey(verbose_name=b'Opcion', to='Encuesta.Opcion')),
                ('Periodocurso', models.ForeignKey(verbose_name=b'Grupo horario', to='Cursos.PeriodoCurso')),
            ],
        ),
    ]
