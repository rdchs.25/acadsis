# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Matriculas', '0001_initial'),
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoriaItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Descripcion', models.CharField(max_length=250, verbose_name=b'Descripci\xc3\xb3n')),
                ('Orden', models.IntegerField(verbose_name=b'Orden')),
            ],
            options={
                'verbose_name': 'CategoriaItem',
                'verbose_name_plural': 'CategoriaItems',
            },
        ),
        migrations.CreateModel(
            name='Encuesta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=50, verbose_name=b'Nombre')),
                ('Periodo', models.ForeignKey(verbose_name=b'Periodo', to='Periodos.Periodo')),
            ],
            options={
                'verbose_name': 'Encuesta',
                'verbose_name_plural': 'Encuestas',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Descripcion', models.CharField(max_length=250, verbose_name=b'Descripci\xc3\xb3n')),
                ('Orden', models.IntegerField(verbose_name=b'Orden')),
                ('CategoriaItem', models.ForeignKey(verbose_name=b'Categoria de Item', to='Encuesta.CategoriaItem')),
            ],
            options={
                'verbose_name': 'Item',
                'verbose_name_plural': 'Items',
            },
        ),
        migrations.CreateModel(
            name='Opcion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Descripcion', models.CharField(max_length=150, verbose_name=b'Descripci\xc3\xb3n')),
                ('Valor', models.IntegerField(verbose_name=b'Valor')),
                ('Item', models.ForeignKey(verbose_name=b'Item', to='Encuesta.Item')),
            ],
            options={
                'verbose_name': 'Opcion',
                'verbose_name_plural': 'Opciones',
            },
        ),
        migrations.CreateModel(
            name='Respuesta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Valor', models.IntegerField(verbose_name=b'Valor')),
                ('Item', models.ForeignKey(verbose_name=b'Item', to='Encuesta.Item')),
                ('MatriculaCursos', models.ForeignKey(verbose_name=b'Curso matriculado', to='Matriculas.MatriculaCursos')),
                ('Opcion', models.ForeignKey(verbose_name=b'Opcion', to='Encuesta.Opcion')),
            ],
        ),
        migrations.AddField(
            model_name='categoriaitem',
            name='Encuesta',
            field=models.ForeignKey(verbose_name=b'Encuesta', to='Encuesta.Encuesta'),
        ),
    ]
