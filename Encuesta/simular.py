# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Cursos.models import PeriodoCurso
from Encuesta.models import Respuestadocente

# escogemos 4 encuestas que tengan diferentes resultados
resultado1 = Respuestadocente.objects.filter(Periodocurso=5088)
resultado2 = Respuestadocente.objects.filter(Periodocurso=5055)
resultado3 = Respuestadocente.objects.filter(Periodocurso=4814)
resultado4 = Respuestadocente.objects.filter(Periodocurso=4831)
resultado5 = Respuestadocente.objects.filter(Periodocurso=4984)

cursosfaltantes = PeriodoCurso.objects.filter(Encuesta=False, Periodo__id=25)

contador = 1
for periodocurso in cursosfaltantes:
    plantilla = None
    print contador
    if contador == 1:
        plantilla = resultado1
        contador = contador + 1
    elif contador == 2:
        plantilla = resultado2
        contador = contador + 1
    elif contador == 3:
        plantilla = resultado3
        contador = contador + 1
    elif contador == 4:
        plantilla = resultado4
        contador = contador + 1
    else:
        plantilla = resultado5
        contador = 1
    for resultado in plantilla:
        respuesta = Respuestadocente(Item=resultado.Item,
                                     Opcion=resultado.Opcion,
                                     Valor=resultado.Valor,
                                     Periodocurso=periodocurso)
        respuesta.save()
    periodocurso.Encuesta = True
    periodocurso.save()
