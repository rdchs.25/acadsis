# -*- coding: utf-8 -*-
# Avoid shadowing the login() view below.
import hashlib

from django import forms
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.forms.utils import ErrorList
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect

from Cursos.models import PeriodoCurso
from Docentes.models import MoodleUser, Docente
from Encuesta.models import Encuesta, Item, Opcion, Respuestadocente
from Periodos.models import Periodo

ID_PERIODO_MATRICULA = 25
cursos_excluidos = []


class LoginFormEncuestaOnline(forms.Form):
    Usuario = forms.CharField(label="Usuario", required=True)
    Password = forms.CharField(
        widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")

        if user and passwd:
            try:
                validar_usuario = MoodleUser.objects.using(
                    'moodle').get(username=user)
                usuario_udl = User.objects.get(username=user)
                grupos = usuario_udl.groups.all()
                for grupo in grupos:
                    if grupo.name == "Docentes UDL":
                        alumno = True
                        break

                if alumno:
                    try:
                        usuario = MoodleUser.objects.using('moodle').get(
                            username=user, password=hashlib.md5(passwd).hexdigest())
                        usuario_udl.set_password(passwd)
                        usuario_udl.save()
                    except MoodleUser.DoesNotExist:
                        msg = u'Usuario Incorrecto'
                        msg1 = u'Contraseña Incorrecta'
                        self._errors["Usuario"] = ErrorList([msg])
                        self._errors["Password"] = ErrorList([msg1])
                        del cleaned_data["Usuario"]
                else:
                    msg = u'No eres estudiante'
                    self._errors["Usuario"] = ErrorList([msg])
                    del cleaned_data["Usuario"]

            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]

        return cleaned_data


@csrf_protect
def logout_autoevaluacion_online(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('../../autoevaluacion_enlinea/')
    else:
        return HttpResponseRedirect('../../autoevaluacion_enlinea/')


@csrf_protect
def autoevaluacion_online(request):
    periodo = Periodo.objects.get(id=ID_PERIODO_MATRICULA)
    encuesta = Encuesta.objects.get(Periodo=periodo)
    var = request.GET.get('q', 'm')
    try:
        docente = Docente.objects.get(user_ptr=request.user.id)
    except Docente.DoesNotExist:
        mensaje = "Usted no es un docente, verifique por favor."
        links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
        return render_to_response("Autoevaluacion/Online/mensaje.html",
                                  {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                   "user": request.user})

    matriculado_cursos = PeriodoCurso.objects.filter(Periodo=periodo, Docente=docente,
                                                     Encuesta=False).exclude(id__in=cursos_excluidos)

    if matriculado_cursos.count() == 0:
        mensaje = "Usted no tiene cursos asignados o ya completo todas las encuentas de sus cursos."
        links = "<a class='btn btn-primary btn-large' href=''>Salir</a>"
        return render_to_response("Autoevaluacion/Online/mensaje.html",
                                  {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                   "user": request.user})

    cursos_disponibles = []
    for cursos_mat in matriculado_cursos:
        cursos_disponibles.append(cursos_mat)

    return render_to_response("Autoevaluacion/Online/profile_encuesta_online.html",
                              {"results": cursos_disponibles, "docente": docente, "periodo": periodo,
                               "var": var, "user": request.user, "encuesta": encuesta},
                              context_instance=RequestContext(request))


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


@csrf_exempt
def registrar_autoevaluacion_online(request, idcurso):
    if request.user.is_authenticated():
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO_MATRICULA)
        except Periodo.DoesNotExist:
            mensaje = "Error, No existe el semestre, verifique por favor."
            return HttpResponse(mensaje)

        try:
            docente = Docente.objects.get(user_ptr=request.user.id)
        except Docente.DoesNotExist:
            mensaje = "Error, Usted no es un docente, verifique por favor."
            return HttpResponse(mensaje)

        try:
            matricula_curso = PeriodoCurso.objects.get(id=idcurso, Docente=docente)
        except PeriodoCurso.DoesNotExist:
            mensaje = "Error, docente no tiene asignado este curso."
            return HttpResponse(mensaje)

        respuestas = request.POST.getlist('respuesta')
        for respuesta in respuestas:
            valores = respuesta.split('_')
            iditem = valores[0]
            idopcion = valores[1]
            opcion = Opcion.objects.get(id=idopcion)
            item = Item.objects.get(id=iditem)

            respuesta = Respuestadocente(Item=item, Opcion=opcion,
                                         Valor=opcion.Valor, Periodocurso_id=idcurso)
            matricula_curso.Encuesta = True
            matricula_curso.save()

            respuesta.save()

        mensaje = "Encuesta registrada correctamente"
        return HttpResponse(mensaje)
    else:
        return HttpResponseRedirect('../')
