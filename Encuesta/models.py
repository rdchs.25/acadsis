# -*- coding: utf-8 -*-
from django.db import models

from Cursos.models import PeriodoCurso
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from Periodos.models import Periodo


class Encuesta(models.Model):
    Nombre = models.CharField("Nombre", max_length=50)
    Periodo = models.ForeignKey(Periodo, verbose_name='Periodo')

    class Meta:
        verbose_name = "Encuesta"
        verbose_name_plural = "Encuestas"

    def __unicode__(self):
        return u'%s - %s' % (self.Nombre, self.Periodo)


class CategoriaItem(models.Model):
    Descripcion = models.CharField("Descripción", max_length=250)
    Orden = models.IntegerField("Orden")
    Encuesta = models.ForeignKey(Encuesta, verbose_name='Encuesta')

    class Meta:
        verbose_name = "CategoriaItem"
        verbose_name_plural = "CategoriaItems"

    def __unicode__(self):
        return u'%s - %s' % (self.Orden, self.Descripcion)


class Item(models.Model):
    Descripcion = models.CharField("Descripción", max_length=250)
    Orden = models.IntegerField("Orden")
    CategoriaItem = models.ForeignKey(
        CategoriaItem, verbose_name='Categoria de Item')

    class Meta:
        verbose_name = "Item"
        verbose_name_plural = "Items"

    def __unicode__(self):
        return u'%s - %s' % (self.Orden, self.Descripcion)


class Opcion(models.Model):
    Descripcion = models.CharField("Descripción", max_length=150)
    Valor = models.IntegerField("Valor")
    Item = models.ForeignKey(Item, verbose_name='Item')

    class Meta:
        verbose_name = "Opcion"
        verbose_name_plural = "Opciones"

    def __unicode__(self):
        return u'%s - %s' % (self.Descripcion, self.Valor)


class Respuesta(models.Model):
    Item = models.ForeignKey(Item, verbose_name='Item')
    Opcion = models.ForeignKey(Opcion, verbose_name='Opcion')
    Valor = models.IntegerField("Valor")
    MatriculaCursos = models.ForeignKey(
        MatriculaCursos, verbose_name='Curso matriculado')


class Respuestadocente(models.Model):
    Item = models.ForeignKey(Item, verbose_name='Item')
    Opcion = models.ForeignKey(Opcion, verbose_name='Opcion')
    Valor = models.IntegerField("Valor")
    Periodocurso = models.ForeignKey(PeriodoCurso, verbose_name="Grupo horario")
