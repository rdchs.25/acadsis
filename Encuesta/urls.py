# -*- coding: utf-8 -*-
from django.conf.urls import url

from Encuesta.views import exportar_encuesta

app_name = 'Encuesta'

urlpatterns = [
    url(r'^encuesta/exportar/$', exportar_encuesta),
]
