# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin

from Colegios.models import Colegio
from Ubicacion.models import Departamento


class ColegioAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ColegioAdminForm, self).__init__(*args, **kwargs)
        try:
            self.fields['selected_dep'].initial = self.instance.Provincia.Departamento.id
        except:
            pass

    Departamento = forms.ModelChoiceField(queryset=Departamento.objects.all().order_by('Departamento'),
                                          widget=forms.Select(), required=False)
    selected_dep = forms.CharField(widget=forms.HiddenInput, required=False, label="")

    class Meta:
        model = Colegio
        fields = '__all__'

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Colegios/frmcolegio.js",
              )


class ColegioAdmin(admin.ModelAdmin):
    form = ColegioAdminForm
    fieldsets = (
        (None, {
            'fields': (
                'Cod_Modular', 'Cod_Local', 'Colegio', 'Departamento', 'Provincia', 'Distrito', 'Tipo', 'selected_dep')
        }),
    )
    list_display = ('Cod_Modular', 'Cod_Local', 'Colegio', 'Distrito', 'Provincia', 'Departamento', 'Tipo')
    search_fields = ('Colegio',)
    list_filter = ('Tipo',)


admin.site.register(Colegio, ColegioAdmin)
