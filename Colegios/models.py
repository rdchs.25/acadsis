# -*- coding: utf-8 -*-
from django.db import models

from Ubicacion.models import Provincia

TIPO_COLEGIO_CHOICES = (
    ('IEN', 'Nacional'),
    ('IEP', 'Particular'),
)


class Colegio(models.Model):
    Cod_Local = models.CharField("Cod. Local", max_length=10)
    Provincia = models.ForeignKey(Provincia)
    Distrito = models.CharField(max_length=50)
    Colegio = models.CharField("I.E.", max_length=100)
    Cod_Modular = models.CharField("Cod. Modular", max_length=10)
    Tipo = models.CharField(max_length=4, choices=TIPO_COLEGIO_CHOICES)

    def __unicode__(self):
        return self.Colegio

    def Departamento(self):
        return self.Provincia.Departamento

    def save(self):
        super(Colegio, self).save()
        return super(Colegio, self).save(using='escuelapre')

    class Meta:
        verbose_name = "Colegio"
        verbose_name_plural = "Colegios"
