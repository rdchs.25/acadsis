# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Ubicacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Colegio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Cod_Local', models.CharField(max_length=10, verbose_name=b'Cod. Local')),
                ('Distrito', models.CharField(max_length=50)),
                ('Colegio', models.CharField(max_length=100, verbose_name=b'I.E.')),
                ('Cod_Modular', models.CharField(max_length=10, verbose_name=b'Cod. Modular')),
                ('Tipo', models.CharField(max_length=4, choices=[(b'IEN', b'Nacional'), (b'IEP', b'Particular')])),
                ('Provincia', models.ForeignKey(to='Ubicacion.Provincia')),
            ],
            options={
                'verbose_name': 'Colegio',
                'verbose_name_plural': 'Colegios',
            },
        ),
    ]
