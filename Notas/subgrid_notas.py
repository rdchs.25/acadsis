# -*- coding: utf-8 -*-

from decimal import Decimal

import simplejson
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from Horarios.views import horarios_notas_curso
from Notas.models import Nota, NotasAlumno


def ver_subgrid_notas(request):
    if request.user.is_authenticated():
        periodocurso_id = request.GET.get('p', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        notas = Nota.objects.filter(PeriodoCurso__id=periodocurso_id, Nivel='0').order_by(str(sord) + str(sidx))

        n_notas = notas.count()
        paginator = Paginator(notas, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.SubNotas == True:
                estado = "Si"
            else:
                estado = "No"
            fila = {"id": r.id, "cell": [i, r.Nota, r.Identificador, estado]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_notas, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return HttpResponseRedirect('../../../../')


def ver_subgrid_notas1(request):
    if request.user.is_authenticated():
        periodocurso_id = request.GET.get('p', '')
        page = request.GET.get('page', '')
        fila = request.GET.get('fila', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        notas = Nota.objects.filter(PeriodoCurso__id=periodocurso_id, Nivel='1', NotaPadre__id=fila).order_by(
            str(sord) + str(sidx))

        n_notas = notas.count()
        paginator = Paginator(notas, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.SubNotas == True:
                estado = "Si"
            else:
                estado = "No"
            fila = {"id": r.id, "cell": [i, r.Nota, r.Identificador, str(r.Peso), r.Orden, estado]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_notas, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return HttpResponseRedirect('../../../../')


def ver_subgrid_notas2(request):
    if request.user.is_authenticated():
        periodocurso_id = request.GET.get('p', '')
        page = request.GET.get('page', '')
        fila = request.GET.get('fila', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        notas = Nota.objects.filter(PeriodoCurso__id=periodocurso_id, Nivel='2', NotaPadre__id=fila).order_by(
            str(sord) + str(sidx))

        n_notas = notas.count()
        paginator = Paginator(notas, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.SubNotas == True:
                estado = "Si"
            else:
                estado = "No"
            fila = {"id": r.id, "cell": [i, r.Nota, r.Identificador, str(r.Peso), r.Orden, estado]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_notas, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return HttpResponseRedirect('../../../../')


def ver_subgrid_notas3(request):
    if request.user.is_authenticated():
        periodocurso_id = request.GET.get('p', '')
        page = request.GET.get('page', '')
        fila = request.GET.get('fila', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        notas = Nota.objects.filter(PeriodoCurso__id=periodocurso_id, Nivel='3', NotaPadre__id=fila).order_by(
            str(sord) + str(sidx))

        n_notas = notas.count()
        paginator = Paginator(notas, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.SubNotas == True:
                estado = "Si"
            else:
                estado = "No"
            fila = {"id": r.id, "cell": [i, r.Nota, r.Identificador, str(r.Peso), r.Orden, estado]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_notas, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return HttpResponseRedirect('../../../../')


@csrf_exempt
def master_subgrid_notas(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']

        if operacion == "add":
            guardar_nota = Nota(PeriodoCurso_id=periodocurso_id, Nota=nota, Identificador=identificador,
                                Peso=Decimal('1.00'), Nivel='0', Orden='1', NotaPadre=None, SubNotas=False)
            guardar_nota.save()
        elif operacion == "edit":
            obj_nota = Nota.objects.get(id=nota_id)
            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.NotaPadre = None
            obj_nota.save()
        elif operacion == "del":
            obj_nota = Nota.objects.get(id=nota_id)
            obj_nota.delete()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../')


@csrf_exempt
def master_subgrid_notas1(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']
        fila_id = request.GET.get('fila', '')

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']
            orden = request.POST['Orden']
            peso = request.POST['Peso']

        if operacion == "add":
            guardar_nota = Nota(PeriodoCurso_id=periodocurso_id, Nota=nota, Identificador=identificador, Peso=peso,
                                Nivel='1', Orden=orden, NotaPadre_id=fila_id)
            guardar_nota.save()
            nota_padre = Nota.objects.get(id=fila_id)
            nota_padre.SubNotas = True
            nota_padre.save()
            notas_alumnos = NotasAlumno.objects.filter(Nota=nota_padre)
            notas_alumnos.delete()
        elif operacion == "edit":
            obj_nota = Nota.objects.get(id=nota_id)
            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.Orden = orden
            obj_nota.Peso = peso
            obj_nota.save()
            nota_padre = Nota.objects.get(id=obj_nota.NotaPadre.id)
            nota_padre.SubNotas = True
            nota_padre.save()
        elif operacion == "del":
            obj_nota = Nota.objects.get(id=nota_id)
            nota_padre_id = obj_nota.NotaPadre.id
            obj_nota.delete()
            cantidad = Nota.objects.filter(NotaPadre__id=nota_padre_id).count()
            if cantidad == 0:
                nota_padre = Nota.objects.get(id=nota_padre_id)
                nota_padre.SubNotas = False
                nota_padre.save()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../')


@csrf_exempt
def master_subgrid_notas2(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']
        fila_id = request.GET.get('fila', '')

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']
            orden = request.POST['Orden']
            peso = request.POST['Peso']

        if operacion == "add":
            guardar_nota = Nota(PeriodoCurso_id=periodocurso_id, Nota=nota, Identificador=identificador, Peso=peso,
                                Nivel='2', Orden=orden, NotaPadre_id=fila_id)
            guardar_nota.save()
            nota_padre = Nota.objects.get(id=fila_id)
            nota_padre.SubNotas = True
            nota_padre.save()
            notas_alumnos = NotasAlumno.objects.filter(Nota=nota_padre)
            notas_alumnos.delete()
        elif operacion == "edit":
            obj_nota = Nota.objects.get(id=nota_id)
            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.Orden = orden
            obj_nota.Peso = peso
            obj_nota.save()
            nota_padre = Nota.objects.get(id=obj_nota.NotaPadre.id)
            nota_padre.SubNotas = True
            nota_padre.save()
        elif operacion == "del":
            obj_nota = Nota.objects.get(id=nota_id)
            nota_padre_id = obj_nota.NotaPadre.id
            obj_nota.delete()
            cantidad = Nota.objects.filter(NotaPadre__id=nota_padre_id).count()
            if cantidad == 0:
                nota_padre = Nota.objects.get(id=nota_padre_id)
                nota_padre.SubNotas = False
                nota_padre.save()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../')


@csrf_exempt
def master_subgrid_notas3(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']
        fila_id = request.GET.get('fila', '')

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']
            orden = request.POST['Orden']
            peso = request.POST['Peso']

        if operacion == "add":
            guardar_nota = Nota(PeriodoCurso_id=periodocurso_id, Nota=nota, Identificador=identificador, Peso=peso,
                                Nivel='3', Orden=orden, NotaPadre_id=fila_id)
            guardar_nota.save()
            nota_padre = Nota.objects.get(id=fila_id)
            nota_padre.SubNotas = True
            nota_padre.save()
            notas_alumnos = NotasAlumno.objects.filter(Nota=nota_padre)
            notas_alumnos.delete()
        elif operacion == "edit":
            obj_nota = Nota.objects.get(id=nota_id)
            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.Orden = orden
            obj_nota.Peso = peso
            obj_nota.save()
            nota_padre = Nota.objects.get(id=obj_nota.NotaPadre.id)
            nota_padre.SubNotas = True
            nota_padre.save()
        elif operacion == "del":
            obj_nota = Nota.objects.get(id=nota_id)
            nota_padre_id = obj_nota.NotaPadre.id
            obj_nota.delete()
            cantidad = Nota.objects.filter(NotaPadre__id=nota_padre_id).count()
            if cantidad == 0:
                nota_padre = Nota.objects.get(id=nota_padre_id)
                nota_padre.SubNotas = False
                nota_padre.save()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../')
