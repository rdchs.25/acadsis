# -*- coding: utf-8 -*-

from decimal import Decimal

import simplejson
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt

from Asistencias.views import view_notas
from Cursos.models import PeriodoCurso
from Horarios.models import Horario
from Matriculas.models import MatriculaCursos
from Notas.models import Nota, NotasAlumno


def ver_editgrid_notas(request):
    if request.user.is_authenticated():
        periodocurso_id = request.GET.get('p', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        notas = Nota.objects.filter(PeriodoCurso__id=periodocurso_id).order_by('id')

        alumnos = MatriculaCursos.objects.filter(PeriodoCurso__id=periodocurso_id, Estado=True).order_by(
            'MatriculaCiclo__Alumno__ApellidoPaterno')

        n_alumnos = alumnos.count()
        paginator = Paginator(alumnos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for alumno in alumnos:
            ident = [i, "%s %s %s" % (
                alumno.MatriculaCiclo.Alumno.ApellidoPaterno, alumno.MatriculaCiclo.Alumno.ApellidoMaterno,
                alumno.MatriculaCiclo.Alumno.Nombres)]
            ident1 = []
            if notas.count() != 0:
                for cal1 in notas:
                    if cal1.Nota in ident1:
                        pass
                    else:
                        if cal1.Nivel == "0":
                            ultima_nota = cal1.Nota
                            subnotas1 = Nota.objects.filter(NotaPadre=cal1)
                            try:
                                nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=alumno)
                                nota_primer_nivel = nota.Valor
                            except NotasAlumno.DoesNotExist:
                                nota = NotasAlumno(Nota=cal1, MatriculaCursos=alumno, Valor=Decimal(alumno.Promedio()))
                                nota.save()
                                nota_primer_nivel = nota.Valor

                        else:
                            subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                            if subnotas2.count() == 0:
                                try:
                                    nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=alumno)
                                    ident.append(str(nota.Valor))
                                except NotasAlumno.DoesNotExist:
                                    nota = NotasAlumno(Nota=cal1, MatriculaCursos=alumno, Valor=Decimal("0.00"))
                                    nota.save()
                                    ident.append(str(nota.Valor))
                                ident1.append(cal1.Nota)
                            else:
                                for cal2 in subnotas2:
                                    subnotas3 = Nota.objects.filter(NotaPadre=cal2)
                                    if subnotas3.count() == 0:
                                        try:
                                            nota = NotasAlumno.objects.get(Nota=cal2, MatriculaCursos=alumno)
                                            ident.append(str(nota.Valor))
                                        except NotasAlumno.DoesNotExist:
                                            nota = NotasAlumno(Nota=cal2, MatriculaCursos=alumno, Valor=Decimal("0.00"))
                                            nota.save()
                                            ident.append(str(nota.Valor))
                                        ident1.append(cal2.Nota)
                                    else:
                                        for cal3 in subnotas3:
                                            try:
                                                nota = NotasAlumno.objects.get(Nota=cal3, MatriculaCursos=alumno)
                                                ident.append(str(nota.Valor))
                                            except NotasAlumno.DoesNotExist:
                                                nota = NotasAlumno(Nota=cal3, MatriculaCursos=alumno,
                                                                   Valor=Decimal("0.00"))
                                                nota.save()
                                                ident.append(str(nota.Valor))
                                            ident1.append(cal3.Nota)
                                        ident1.append(cal2.Nota)
                                ident1.append(cal1.Nota)
                ident1.append(ultima_nota)
                ident.append(str(nota_primer_nivel))

            fila = {"id": alumno.id, "cell": ident}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_alumnos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return HttpResponseRedirect('../../../')


@csrf_exempt
def master_editgrid_notas(request):
    if request.user.is_authenticated():
        matriculacurso_id = request.POST['id']
        matriculado = MatriculaCursos.objects.get(id=matriculacurso_id)
        for nota in matriculado.notasalumno_set.all():
            if nota.Nota.Nivel != '0':
                if nota.Valor != Decimal(request.POST[str(nota.Nota.id)]):
                    nota.Valor = Decimal(request.POST[str(nota.Nota.id)])
                    nota.save()
                    nota_promedio = NotasAlumno.objects.get(MatriculaCursos=nota.MatriculaCursos, Nota__Nivel='0')
                    nota_promedio.Valor = nota_promedio.MatriculaCursos.Promedio1()
                    nota_promedio.save()
            else:
                if request.user.is_superuser == True or request.user.has_perm('Notas.change_notasalumno'):
                    if nota.Valor != Decimal(request.POST[str(nota.Nota.id)]):
                        nota.Valor = Decimal(request.POST[str(nota.Nota.id)])
                        nota.save()
                else:
                    pass
        return view_notas(request, matriculado.PeriodoCurso.id)
    else:
        return HttpResponseRedirect('../../../')


def exportar_notas_excel1(request, idperiodocurso):
    if request.user.is_authenticated():
        import StringIO
        output = StringIO.StringIO()

        from xlwt import Workbook, XFStyle, Borders, Pattern, Font
        book = Workbook()
        sheet1 = book.add_sheet('Notas')

        # estilos de celda titulo
        fnt_titulo = Font()
        fnt_titulo.name = 'Arial'
        fnt_titulo.bold = True

        style_titulo = XFStyle()
        style_titulo.font = fnt_titulo

        # estilos de celda etiqueta resumen
        fnt_etiqueta_resumen = Font()
        fnt_etiqueta_resumen.name = 'Arial'
        fnt_etiqueta_resumen.bold = True

        style_etiqueta_resumen = XFStyle()
        style_etiqueta_resumen.font = fnt_etiqueta_resumen

        # estilos de celda datos resumen
        fnt_dato_resumen = Font()
        fnt_dato_resumen.name = 'Arial'

        style_dato_resumen = XFStyle()
        style_dato_resumen.font = fnt_dato_resumen

        # estilos de celda heads
        fnt_heads = Font()
        fnt_heads.name = 'Arial'
        fnt_heads.bold = True
        borders_heads = Borders()
        borders_heads.left = Borders.THIN
        borders_heads.right = Borders.THIN
        borders_heads.top = Borders.THIN
        borders_heads.bottom = Borders.THIN
        pattern_heads = Pattern()
        pattern_heads.pattern = Pattern.SOLID_PATTERN
        pattern_heads.pattern_fore_colour = 0x9ACD32

        style_heads = XFStyle()
        style_heads.font = fnt_heads
        style_heads.borders = borders_heads
        style_heads.pattern = pattern_heads

        # estilos de celda registros

        # registros del estudiante
        fnt_registros = Font()
        fnt_registros.name = 'Arial'

        # notas menores a 11
        fnt_menor_trece = Font()
        fnt_menor_trece.name = 'Arial'
        fnt_menor_trece.colour_index = 0xa

        # notas mayores o iguales a 11
        fnt_mayor_trece = Font()
        fnt_mayor_trece.name = 'Arial'
        fnt_mayor_trece.colour_index = 0xc

        # fondo notas 0
        pattern_notas0 = Pattern()
        pattern_notas0.pattern = Pattern.SOLID_PATTERN
        pattern_notas0.pattern_fore_colour = 0x31

        # fondo notas 1
        pattern_notas1 = Pattern()
        pattern_notas1.pattern = Pattern.SOLID_PATTERN
        pattern_notas1.pattern_fore_colour = 0x1A

        # fondo notas 2
        pattern_notas2 = Pattern()
        pattern_notas2.pattern = Pattern.SOLID_PATTERN
        pattern_notas2.pattern_fore_colour = 0x16

        borders_registros = Borders()
        borders_registros.left = Borders.THIN
        borders_registros.right = Borders.THIN
        borders_registros.top = Borders.THIN
        borders_registros.bottom = Borders.THIN

        style_registros = XFStyle()
        style_registros.font = fnt_registros
        style_registros.borders = borders_registros

        # style notas 0 >= 11
        style_notas0_mayor = XFStyle()
        style_notas0_mayor.font = fnt_mayor_trece
        style_notas0_mayor.borders = borders_registros
        style_notas0_mayor.pattern = pattern_notas0

        # style notas 0 < 11
        style_notas0_menor = XFStyle()
        style_notas0_menor.font = fnt_menor_trece
        style_notas0_menor.borders = borders_registros
        style_notas0_menor.pattern = pattern_notas0

        # style notas 1 >= 11
        style_notas1_mayor = XFStyle()
        style_notas1_mayor.font = fnt_mayor_trece
        style_notas1_mayor.borders = borders_registros
        style_notas1_mayor.pattern = pattern_notas1

        # style notas 1 < 11
        style_notas1_menor = XFStyle()
        style_notas1_menor.font = fnt_menor_trece
        style_notas1_menor.borders = borders_registros
        style_notas1_menor.pattern = pattern_notas1

        # style notas 2 >= 11
        style_notas2_mayor = XFStyle()
        style_notas2_mayor.font = fnt_mayor_trece
        style_notas2_mayor.borders = borders_registros
        style_notas2_mayor.pattern = pattern_notas2

        # style notas 2 < 11
        style_notas2_menor = XFStyle()
        style_notas2_menor.font = fnt_menor_trece
        style_notas2_menor.borders = borders_registros
        style_notas2_menor.pattern = pattern_notas2

        # style notas 3 >= 11
        style_notas3_mayor = XFStyle()
        style_notas3_mayor.font = fnt_mayor_trece
        style_notas3_mayor.borders = borders_registros

        # style notas 3 < 11
        style_notas3_menor = XFStyle()
        style_notas3_menor.font = fnt_menor_trece
        style_notas3_menor.borders = borders_registros

        try:
            # periodocurso = PeriodoCurso.objects.get(id = idperiodocurso, Docente__id = request.user.id)
            periodocurso = PeriodoCurso.objects.get(id=idperiodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a class='btn btn-primary btn-large' href='../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if not hor.PeriodoCurso in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        registros = []
        for per in periodo_cursos:
            mats = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False).order_by(
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')
            for mat in mats:
                notas1 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, Nivel='1').order_by('Orden')

                nots1 = []
                nots2 = []
                nots3 = []

                for nota1 in notas1:
                    nots_2 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota1, Nivel='2').order_by(
                        'Orden')
                    for nota2 in nots_2:
                        nots_3 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota2,
                                                     Nivel='3').order_by('Orden')
                        for nota3 in nots_3:
                            valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                            if valor3.count() != 0:
                                valor3 = valor3[0].Valor
                                nots3.append([nota3, int(str(valor3).split('.')[0])])
                            else:
                                valor3 = mat.PromedioParcial(nota3)
                                nots3.append([nota3, valor3])
                        valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                        if valor2.count() != 0:
                            valor2 = valor2[0].Valor
                            nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                        else:
                            valor2 = mat.PromedioParcial(nota2)
                            nots2.append([nota2, nots3, valor2])
                        nots3 = []
                    valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
                    if valor1.count() != 0:
                        valor1 = valor1[0].Valor
                        nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
                    else:
                        valor1 = mat.PromedioParcial(nota1)
                        nots1.append([nota1, nots2, valor1])
                    nots2 = []
                registros.append([mat, nots1])
                nots1 = []

        sheet1.write(2, 0, 'CURSO:', style_titulo)
        sheet1.write(2, 1, periodocurso.Curso.Nombre, style_titulo)
        sheet1.write(4, 0, 'DOCENTE:', style_titulo)
        sheet1.write(4, 1, periodocurso.Docente.__unicode__(), style_titulo)

        # escribir el titulo
        sheet1.write(6, 0, 'CALIFICACIONES', style_titulo)

        sheet1.write(8, 0, 'Nro', style_heads)
        sheet1.write(8, 1, 'Codigo', style_heads)
        sheet1.write(8, 2, 'Estudiante', style_heads)
        sheet1.write(8, 3, 'Carrera', style_heads)

        k = 4

        for reg1 in registros[0][1]:
            if reg1:
                for reg2 in reg1[1]:
                    if reg2:
                        for reg3 in reg2[1]:
                            if reg3:
                                sheet1.write(8, k, reg3[0].Identificador, style_heads)
                                k += 1
                        sheet1.write(8, k, reg2[0].Identificador, style_heads)
                        k += 1
                sheet1.write(8, k, reg1[0].Identificador, style_heads)
                k += 1

        sheet1.write(8, k, 'PF', style_heads)

        j = 4
        i = 9

        for reg in registros:
            aprobado = int(str(reg[0].PeriodoCurso.Aprobacion).split('.')[0])
            sheet1.write(i, 0, i - 8, style_registros)
            sheet1.write(i, 1, reg[0].MatriculaCiclo.Alumno.Codigo, style_registros)
            sheet1.write(i, 2, reg[0].MatriculaCiclo.Alumno.__unicode__(), style_registros)
            sheet1.write(i, 3, reg[0].MatriculaCiclo.Alumno.Carrera.Carrera, style_registros)
            for reg1 in reg[1]:
                if reg1:
                    for reg2 in reg1[1]:
                        if reg2:
                            for reg3 in reg2[1]:
                                if reg3:
                                    if reg3[2] >= aprobado:
                                        sheet1.write(i, j, reg3[2], style_notas3_mayor)
                                    else:
                                        sheet1.write(i, j, reg3[2], style_notas3_menor)
                                    j += 1
                            if reg2[2] >= aprobado:
                                sheet1.write(i, j, reg2[2], style_notas2_mayor)
                            else:
                                sheet1.write(i, j, reg2[2], style_notas2_menor)
                            j += 1
                    if reg1[2] >= aprobado:
                        sheet1.write(i, j, reg1[2], style_notas1_mayor)
                    else:
                        sheet1.write(i, j, reg1[2], style_notas1_menor)
                    j += 1
            if reg[0].Promedio() >= aprobado:
                sheet1.write(i, j, reg[0].Promedio(), style_notas0_mayor)
            else:
                sheet1.write(i, j, reg[0].Promedio(), style_notas0_menor)
            i += 1
            j = 4

        l = i

        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')

        for nota in notas:
            sheet1.write(i + 5, 1, '%s:' % nota.Identificador)
            sheet1.write(i + 5, 2, '%s' % nota.Nota)
            i += 1

        for nota in notas1:
            subnotas = Nota.objects.filter(NotaPadre=nota)
            k = 1
            formula = ''
            suma = Decimal('0.00')
            for subnota in subnotas:
                if k == 1:
                    formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
                else:
                    formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
                k += 1
                suma += Decimal(nota.Peso)
            formula += ')/%s' % str(suma)
            sheet1.write(l + 5, 4, formula)
            l += 1

        book.save(output)
        output.seek(0)
        response = HttpResponse(content=output.getvalue(), content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=notas_excel.xls'
        return response
    else:
        return HttpResponseRedirect('/docente/login/')
