# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Matriculas', '0001_initial'),
        ('Cursos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nota',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nota', models.CharField(max_length=50, verbose_name=b'Descripci\xc3\xb3n')),
                ('Identificador', models.CharField(max_length=4, null=True, verbose_name=b'Identificador', blank=True)),
                ('Peso', models.DecimalField(verbose_name=b'Peso', max_digits=6, decimal_places=2)),
                ('Nivel', models.CharField(default=b'0', max_length=2, verbose_name=b'Nivel', choices=[(b'0', b'0'), (b'1', b'1'), (b'2', b'2'), (b'3', b'3')])),
                ('Orden', models.CharField(max_length=2, verbose_name=b'Orden', choices=[(b'1', b'1'), (b'2', b'2'), (b'3', b'3'), (b'4', b'4'), (b'5', b'5'), (b'6', b'6'), (b'7', b'7'), (b'8', b'8'), (b'9', b'9'), (b'10', b'10'), (b'11', b'11'), (b'12', b'12'), (b'13', b'13'), (b'14', b'14'), (b'15', b'15'), (b'16', b'16'), (b'17', b'17'), (b'18', b'18'), (b'19', b'19'), (b'20', b'20')])),
                ('SubNotas', models.BooleanField(default=False, verbose_name=b'\xc2\xbfTiene Sub Notas ?')),
                ('NotaPadre', models.ForeignKey(blank=True, to='Notas.Nota', null=True)),
                ('PeriodoCurso', models.ForeignKey(verbose_name=b'Curso', to='Cursos.PeriodoCurso')),
            ],
            options={
                'verbose_name': 'Nota',
                'verbose_name_plural': 'Notas',
                'permissions': (('read_nota', 'Observar Configuracion de Notas'),),
            },
        ),
        migrations.CreateModel(
            name='NotasAlumno',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Valor', models.DecimalField(verbose_name=b'Valor', max_digits=4, decimal_places=2)),
                ('MatriculaCursos', models.ForeignKey(verbose_name=b'Curso', to='Matriculas.MatriculaCursos')),
                ('Nota', models.ForeignKey(verbose_name=b'Curso', to='Notas.Nota')),
            ],
            options={
                'verbose_name': 'Nota de Alumnos',
                'verbose_name_plural': 'Notas de Alumnos',
                'permissions': (('read_notasalumno', 'Observar Notas de Alumnos'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='nota',
            unique_together=set([('PeriodoCurso', 'Nivel', 'Orden', 'NotaPadre')]),
        ),
    ]
