# -*- coding: utf-8 -*-
# from django.conf import settings
import datetime
from decimal import Decimal

from Autoridades.models import Fecha
from UDL import settings


def acta_notas(c):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 60
    margenRight = 20
    i = 0

    # --------------------------
    # Numero de guia
    # --------------------------
    # while i < 1734:
    #   c.drawString(10,i, str(i))
    #   i=i+4
    # j=0
    # while j < 1734:
    #   c.drawString(j,830, str(j))
    #   j=j+10

    # --------------------------
    # Rellenos
    # --------------------------
    k = 160
    interlineado = 20
    # c.setStrokeColorRGB(0.75,0.75,0.75)
    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # while k < 590:
    # c.drawString(10,k, str(k))
    #        c.line(margenLeft,k,anchoA4 - margenRight,k) #Lineas Formulario ...
    #        l = k
    #        if l%40==0:
    #                while l < k+interlineado:
    #                        c.line(margenLeft,l,anchoA4 - margenRight,l) #Lineas Formulario ...
    #                        l = l + 1

    #        k=k+interlineado

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    # c.drawString(225,800,"EVALUACIÃN Y REGISTRO")
    c.drawString(190, 730, "ACTA DE EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    # c.drawString(270,780,"Acta de Notas")
    # c.drawString(270,780,"Y REGISTRO")

    c.setFont("Helvetica", 10)
    c.drawString(60, 710, "Asignatura :")
    c.drawString(60, 690, "Créditos :")
    c.drawString(60, 670, "Escuela Profesional :")
    c.drawString(60, 650, "Docente :")
    c.drawString(380, 710, "Código :")
    c.drawString(380, 690, "G.H. :")
    c.drawString(380, 670, "Semestre :")
    c.drawString(380, 650, "Tipo de Acta :")

    c.drawString(70, 610, "N°")
    c.drawString(95, 610, "Código")
    c.drawString(240, 610, "Apellidos y Nombres")
    c.drawString(440, 616, "Nota")
    c.drawString(430, 600, "Números")

    c.drawString(515, 616, "Nota")
    c.drawString(510, 600, "Letras")

    c.drawString(80, 130, "")
    # c.drawString(370,130,"Chiclayo, 30 Julio de 2012")
    # c.drawString(370, 130, "Chiclayo, 2 de Diciembre de 2016")
    c.line(60, 75, 270, 75)
    c.setFont("Helvetica-Bold", 10)

    autori = Fecha.objects.filter(Tipo=2).filter(Activo=1)
    nom_autoridad = str(autori[0].Autoridad)

    # nom_autoridad es el nombre del Jefe de la Oficina de Asuntos Académicos
    c.drawString(77, 60, nom_autoridad)
    # c.drawString(77, 60, "Ing. Segundo José Castillo Zumarán")
    # c.drawString(75,60,"Mgtr. Eduardo Julio Tejada Sánchez")
    c.setFont("Helvetica", 10)
    c.drawString(81, 43, "Director de Escuela Profesional de")
    c.drawString(110, 28, autori[0].Tipo.Nombre)
    # c.drawString(85,40,"Director de Asuntos Académicos")
    # c.drawString(145,40,"Director")

    c.line(margenLeft, 630, anchoA4 - margenRight, 630)
    c.line(370, 75, 550, 75)
    c.drawString(408, 40, "Asuntos Académicos")

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # 1ra Linea del Formulario
    c.line(margenLeft, 630, anchoA4 - margenRight, 630)
    c.line(margenLeft, 590, anchoA4 - margenRight, 590)  # 2da Linea ...
    c.line(margenLeft, 580, anchoA4 - margenRight, 580)  # 3da Linea ...
    c.line(margenLeft, 160, anchoA4 - margenRight, 160)  # 4da Linea ...
    c.line(margenLeft, 590, margenLeft, 630)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 590, anchoA4 - margenRight, 630)
    c.line(margenLeft, 580, margenLeft, 160)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 580, anchoA4 - margenRight, 160)

    c.line(margenLeft + 30, 580, margenLeft + 30, 160)  # Linea Vertical 2...
    c.line(margenLeft + 80, 580, margenLeft + 80, 160)  # Linea Vertical 3...
    # Linea Vertical 4...
    c.line(anchoA4 - margenRight - 100, 580, anchoA4 - margenRight - 100, 160)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 150, 580, anchoA4 - margenRight - 150, 160)

    c.line(margenLeft + 30, 590, margenLeft + 30, 630)  # Linea Vertical 2...
    c.line(margenLeft + 80, 590, margenLeft + 80, 630)  # Linea Vertical 3...
    # Linea Vertical 4...
    c.line(anchoA4 - margenRight - 100, 590, anchoA4 - margenRight - 100, 630)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 150, 590, anchoA4 - margenRight - 150, 630)

    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logoHD.jpg'
    # c.drawImage(logo,30,740,width=180,height=102)
    c.drawImage(logo, 40, 750, width=142, height=80)
    return c


def certificado_estudios(c):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(205, 800, "EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    c.drawString(220, 780, "Certificado de Estudios")

    c.setFont("Helvetica", 10)
    c.drawString(
        40, 710, "El Jefe de la Oficina de Registros Académicos Certifica :")
    c.drawString(40, 690, "que:")
    c.drawString(40, 670, "estudiante de la escuela profesional de :")
    c.drawString(40, 650, "Registra las siguientes asignaturas :")
    c.drawString(450, 690, "Código :")
    c.roundRect(465, 730, 80, 80, 0, stroke=1, fill=0)

    c.setFont("Helvetica", 8)
    c.drawString(46, 610, "Nº")
    # c.drawString(44,610,"999")
    c.drawString(67, 610, "CODIGO")
    c.drawString(170, 610, "ASIGNATURAS")
    # c.drawString(105,610,"Introducción a la ingenieria de sistemas.")
    c.drawString(277, 610, "PERIODO")
    c.drawString(320, 610, "CRED.")

    c.drawString(350, 616, "NOTA")
    c.drawString(350, 600, "NROS")

    c.drawString(390, 616, "NOTA")
    # c.drawString(360,600,"diecinueve")
    c.drawString(385, 600, "LETRAS")

    # c.drawString(480,610,"OBSERVACIONES")
    c.drawString(455, 610, "OBSERVACIONES")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # cuadrado de foto
    c.roundRect(465, 730, 80, 80, 0, stroke=1, fill=0)
    c.drawString(495, 770, "Foto")

    # lineas de los campos del encabezado estudiante, codigo, carrera.
    c.line(70, 685, 448, 685)  # estudiante
    c.line(490, 685, 555, 685)  # codigo
    c.line(220, 665, 555, 665)  # carrera

    c.drawString(
        40, 150, "Asi consta en los libros de Actas y/o Resoluciones a las que me remito en caso necesario.")
    c.drawString(350, 120, "Chiclayo, ")
    c.line(395, 120, 410, 120)
    c.drawString(415, 120, "de")
    c.line(430, 120, 500, 120)
    c.drawString(505, 120, "de")
    c.line(520, 120, 555, 120)
    c.setFont("Helvetica", 9)
    ###
    # firma = 82
    firma = 75
    c.line(70, firma, 270, firma)
    c.drawString(100, firma - 12, "SECRETARIO ACADÉMICO")
    # c.drawString(70, firma - 22, "FACULTAD DE CIENCIAS DE LA INGENIERÃA")

    c.line(322, firma, 500, firma)
    c.drawString(350, firma - 12, "JEFE DE LA UNIDAD DE")
    c.drawString(345, firma - 22, "REGISTROS ACADÉMICOS")
    ###

    # c.drawString(40,40,"Nota :")
    # c.drawString(100,30,"1) Toda enmendadura o borrón invalida el certificado.")
    # c.drawString(100,20,"2) El presente certificado carece de valor sin la impresión del sello de agua en la fotografía.")

    # 1ra Linea del Formulario
    c.line(margenLeft, 630, anchoA4 - margenRight, 630)
    c.line(margenLeft, 590, anchoA4 - margenRight, 590)  # 2da Linea ...
    c.line(margenLeft, 580, anchoA4 - margenRight, 580)  # 3da Linea ...
    c.line(margenLeft, 160, anchoA4 - margenRight, 160)  # 4da Linea ...

    # footer
    # c.line(margenLeft,50,anchoA4 - margenRight,50) #5ta Linea ...

    c.line(margenLeft, 590, margenLeft, 630)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 590, anchoA4 - margenRight, 630)

    c.line(margenLeft, 580, margenLeft, 160)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 580, anchoA4 - margenRight, 160)

    c.line(margenLeft + 20, 580, margenLeft + 20, 160)  # Linea Vertical 2...
    c.line(margenLeft + 67, 580, margenLeft + 67, 160)  # Linea Vertical 3...

    # Linea Vertical 4...
    c.line(anchoA4 - margenRight - 280, 580, anchoA4 - margenRight - 280, 160)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 237, 580, anchoA4 - margenRight - 237, 160)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 209, 580, anchoA4 - margenRight - 209, 160)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 179, 580, anchoA4 - margenRight - 179, 160)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 130, 580, anchoA4 - margenRight - 130, 160)

    c.line(margenLeft + 20, 590, margenLeft + 20, 630)  # Linea Vertical 2...
    c.line(margenLeft + 67, 590, margenLeft + 67, 630)  # Linea Vertical 3...

    # Linea Vertical 4...
    c.line(anchoA4 - margenRight - 280, 590, anchoA4 - margenRight - 280, 630)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 237, 590, anchoA4 - margenRight - 237, 630)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 209, 590, anchoA4 - margenRight - 209, 630)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 179, 590, anchoA4 - margenRight - 179, 630)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 130, 590, anchoA4 - margenRight - 130, 630)

    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logoHD.jpg'
    c.drawImage(logo, 10, 740, width=180, height=102)
    return c


def certificado_estudios_egresado(c, alumno, carrera, codigo, datos_totales):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    nombre_alumno = alumno.ApellidoPaterno + " " + \
                    alumno.ApellidoMaterno + ", " + alumno.Nombres
    c.setFont("Helvetica", 12)
    c.drawString(500, 670, codigo)
    c.drawString(70, 670, nombre_alumno.upper())
    # --------------------------
    # Lineas Formulario
    # --------------------------
    cadena_facultad = ''
    if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
        cadena_facultad = ' FACULTAD DE CIENCIAS DE INGENIERÍA'
        facu = 1
    elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
        cadena_facultad = 'FACULTAD DE CIENCIAS SOCIALES, COMERCIALES Y DERECHO'
        facu = 2
        # cadena_facultad = 'FACULTAD DE CIENCIAS DE ADMINISTRACIÓN'
    c.setFont("Times-Bold", 16)
    c.drawString(195, 800, "UNIVERSIDAD DE LAMBAYEQUE")
    c.setFont("Times-Bold", 14)
    if facu == 1:
        c.drawString(180, 780, cadena_facultad)
    if facu == 2:
        c.drawString(131, 780, cadena_facultad)
    c.setFont("Times-Bold", 14)
    c.drawString(140, 760, "ESCUELA PROFESIONAL DE " + carrera.upper())
    c.setFont("Helvetica-BoldOblique", 18)
    c.drawString(210, 730, "Certificado de Estudios")

    c.setFont("Helvetica", 12)
    c.drawString(40, 710, "El jefe de la oficina de registros Académicos")
    c.setFont("Helvetica-Bold", 14)
    c.drawString(40, 690, "CERTIFICA  :")

    c.setFont("Helvetica", 12)
    c.drawString(40, 670, "Que, ")
    c.drawString(430, 670, "Código N°, ")

    c.setFont("Helvetica", 12)
    c.drawString(40, 650, "ha obtenido las siguientes calificaciones finales:")

    # CABECERA DE TABLA
    c.setFont("Times-Bold", 12)
    c.drawString(46, 610, "ASIGNATURAS")
    c.drawString(350, 610, "CALIFICATIVO")
    c.drawString(450, 610, "CRED.")
    c.drawString(500, 610, "FECHAS")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(40, 630, 560, 630)
    c.line(40, 600, 560, 600)

    dict_semestre = {'1': 'PRIMER SEMESTRE', '2': 'SEGUNDO SEMESTRE', '3': 'TERCER SEMESTRE', '4': 'CUARTO SEMESTRE',
                     '5': 'QUINTO SEMESTRE',
                     '6': 'SEXTO SEMESTRE', '7': 'SÉTIMO SEMESTRE', '8': 'OCTAVO SEMESTRE', '9': 'NOVENO SEMESTRE',
                     '10': 'DÉCIMO SEMESTRE', '11': 'ONCEAVO SEMESTRE', '12': 'DOCEAVO SEMESTRE',
                     '13': 'TRECEAVO SEMESTRE'}
    CREDITOS_CARRERA = {'AT': 201, 'AM': 205, 'IA': 203, 'IC': 205, 'IS': 202}
    total_creditos_carrera = str(
        CREDITOS_CARRERA['%s' % alumno.Carrera.Codigo])

    # EMPEZAMOS CONTENIDO DE TABLA
    nro_semestres = len(datos_totales)
    inicio = 590
    terminado = False
    promedio_ponderado_acumulado_final = Decimal("0.000")
    total_creditos_aprobados = 0
    hoja_dos_generada = False

    for y in xrange(0, nro_semestres):
        # VALIDACIÃN SALTO DE PÃGINA
        if inicio < 40:
            hoja_dos_generada = True
            auxiliar = 30
            c.line(40, 630, 40, auxiliar)
            c.line(560, 630, 560, auxiliar)
            c.line(40, auxiliar, 560, auxiliar)

            # lineas de los campos del encabezado estudiante, codigo, carrera.
            c.line(70, 665, 410, 665)  # estudiante
            c.line(490, 665, 555, 665)  # codigo
            logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
            if facu == 1:
                c.drawImage(logo, 50, 730, width=90, height=100)
            if facu == 2:
                c.drawImage(logo, 40, 730, width=90, height=100)
            c.showPage()
            inicio = 780

        lista_cursos = datos_totales[y][1]
        nro_cursos = len(lista_cursos)

        if nro_cursos == 0:
            continue

        # Nombre del semestre
        c.setFont("Helvetica-Bold", 10)
        aux = datos_totales[y][0]
        c.drawString(46, inicio, dict_semestre[str(aux)])

        # DATOS
        inicio = inicio - 10
        y1 = inicio
        for x in xrange(0, nro_cursos):
            # VALIDACIÃN SALTO DE PÃGINA
            if inicio < 40:
                hoja_dos_generada = True
                auxiliar = 30
                c.line(40, 630, 40, auxiliar)
                c.line(560, 630, 560, auxiliar)
                c.line(40, auxiliar, 560, auxiliar)

                # lineas de los campos del encabezado estudiante, codigo, carrera.
                c.line(70, 665, 410, 665)  # estudiante
                c.line(490, 665, 555, 665)  # codigo
                logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
                # c.drawImage(logo, 50, 730, width=90, height=100)
                if facu == 1:
                    c.drawImage(logo, 50, 730, width=90, height=100)
                if facu == 2:
                    c.drawImage(logo, 40, 730, width=90, height=100)
                c.showPage()
                inicio = 780
                y1 = inicio

            codigo_curso = lista_cursos[x][0]
            nombre_curso = lista_cursos[x][1].upper()
            nota_letra = lista_cursos[x][5]
            resolucion = lista_cursos[x][6]
            ciclo_curso = lista_cursos[x][7]
            nota_numero = "(" + lista_cursos[x][4] + ")"
            creditos = lista_cursos[x][3]
            semestre_curso = lista_cursos[x][2]

            c.setFont("Helvetica", 8)
            c.drawString(46, y1, codigo_curso)
            c.drawString(90, y1, nombre_curso)
            if resolucion != '':
                c.setFont("Helvetica", 8)
                c.drawString(370, y1, nota_letra)
                c.drawString(410, y1, nota_numero)
                c.drawString(460, y1, creditos)
                c.drawString(470, y1, resolucion)
            else:
                c.setFont("Helvetica", 8)
                c.drawString(370, y1, nota_letra)
                c.drawString(410, y1, nota_numero)
                c.drawString(460, y1, creditos)
                c.drawString(520, y1, semestre_curso)

            y1 = y1 - 10
            inicio = inicio - 10

        # VALIDACIÃN SALTO DE PÃGINA
        if inicio < 40:
            hoja_dos_generada = True
            auxiliar = 30
            c.line(40, 630, 40, auxiliar)
            c.line(560, 630, 560, auxiliar)
            c.line(40, auxiliar, 560, auxiliar)

            # lineas de los campos del encabezado estudiante, codigo, carrera.
            c.line(70, 665, 410, 665)  # estudiante
            c.line(490, 665, 555, 665)  # codigo
            logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
            # c.drawImage(logo, 50, 730, width=90, height=100)
            if facu == 1:
                c.drawImage(logo, 50, 730, width=90, height=100)
            if facu == 2:
                c.drawImage(logo, 40, 730, width=90, height=100)
            c.showPage()
            inicio = 780
            y1 = inicio

        # PROMEDIO PONDERADO
        promedio_ponderado_semestral = str(datos_totales[y][2])
        promedio_ponderado_acumulado = str(datos_totales[y][3])
        total_creditos_aprobados = str(datos_totales[y][4])
        promedio_ponderado_acumulado_final = promedio_ponderado_acumulado
        c.setFont("Helvetica-Bold", 8)
        c.drawString(46, y1, "PPS = " + str(promedio_ponderado_semestral))
        c.drawString(370, y1, "PPA = " + str(promedio_ponderado_acumulado))
        inicio = inicio - 20

    # VALIDACIÃN SALTO DE PÃGINA
    if inicio < 40:
        hoja_dos_generada = True
        auxiliar = 30
        c.line(40, 630, 40, auxiliar)
        c.line(560, 630, 560, auxiliar)
        c.line(40, auxiliar, 560, auxiliar)

        # lineas de los campos del encabezado estudiante, codigo, carrera.
        c.line(70, 665, 410, 665)  # estudiante
        c.line(490, 665, 555, 665)  # codigo
        logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
        # c.drawImage(logo, 50, 730, width=90, height=100)
        if facu == 1:
            c.drawImage(logo, 50, 730, width=90, height=100)
        if facu == 2:
            c.drawImage(logo, 40, 730, width=90, height=100)

        c.showPage()
        inicio = 780

    # Promedio acumulados finales y total de creditos aprobados
    c.setFont("Helvetica-Bold", 9)
    inicio = inicio - 10
    c.drawString(46, inicio, "PROMEDIO PONDERADO ACUMULADO: " + str(promedio_ponderado_acumulado_final))
    c.drawString(350, inicio, "TOTAL DE CRÉDITOS DEL PROGRAMA : " + str(total_creditos_carrera))
    inicio = inicio - 10
    c.drawString(350, inicio, "TOTAL DE CRÉDITOS APROBADOS       : " + str(total_creditos_aprobados))

    if hoja_dos_generada == False:
        auxiliar = 30
        c.line(40, 630, 40, auxiliar)
        c.line(560, 630, 560, auxiliar)
        c.line(40, auxiliar, 560, auxiliar)

        # lineas de los campos del encabezado estudiante, codigo, carrera.
        c.line(70, 665, 410, 665)  # estudiante
        c.line(490, 665, 555, 665)  # codigo
        logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
        # c.drawImage(logo, 50, 730, width=90, height=100)
        if facu == 1:
            c.drawImage(logo, 50, 730, width=90, height=100)
        if facu == 2:
            c.drawImage(logo, 40, 730, width=90, height=100)
        c.showPage()
        inicio = 780

    # SEGUNDA HOJA
    # CABECERA DE TABLA
    c.setFont("Times-Bold", 12)
    c.drawString(46, 800, "ASIGNATURAS")
    c.drawString(350, 800, "CALIFICATIVO")
    c.drawString(450, 800, "CRED.")
    c.drawString(500, 800, "FECHAS")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # Lineas del cuadro segunda hoja
    c.line(40, 820, 560, 820)
    c.line(40, 790, 560, 790)

    inicio = 180
    c.line(40, 820, 40, inicio)
    c.line(560, 820, 560, inicio)
    c.line(40, inicio, 560, inicio)

    inicio = 170
    # inicio = inicio - 10

    c.drawString(
        46, inicio, "Así consta en las actas de exámenes finales, a las que remito en caso necesario.")

    inicio = inicio - 10
    # cuadrado de foto
    c.roundRect(46, inicio - 80, 80, 80, 0, stroke=1, fill=0)
    c.drawString(76, inicio - 40, "Foto")

    mydate = datetime.datetime.now()

    mn = {'01': 'Enero', '02': 'Febrero', '03': 'Marzo', '04': 'Abril', '05': 'Mayo',
          '06': 'Junio', '07': 'Julio', '08': 'Agosto', '09': 'Setiembre',
          '10': 'Octubre', '11': 'Noviembre', '12': 'Diciembre'}

    c.drawString(350, 140, "Chiclayo,   " + str(mydate.strftime("%d")))
    # c.line(395, 140, 410, 140)
    c.drawString(415, 140, "de   " + str(mn[mydate.strftime("%m")]))
    # c.line(430, 140, 500, 140)
    c.drawString(505, 140, "del  " + str(mydate.strftime("%Y")))
    # c.line(520, 140, 555, 140)
    c.setFont("Helvetica", 8)

    # ESCALA DE CALIFICATIVOS
    iniciox = 130
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "ESCALA DE CALIFICATIVOS")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "00 - 10 Desaprobado")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "11 - 20 Aprobado")

    # ESCALA DE CALIFICATIVOS
    inicio = inicio - 15
    c.drawString(iniciox, inicio, "ABREVIATURAS")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "PPS Promedio Ponderado Semestral")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "PPA Promedio Ponderado Acumulativo")

    # ###
    firma = 60
    if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
        c.drawString(190, firma - 22, "DECANO")
        c.drawString(140, firma - 30, cadena_facultad)
    elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
        c.drawString(250, firma - 22, "DECANO")
        c.drawString(140, firma - 30, cadena_facultad)

    # c.line(382, firma, 560, firma)
    # c.drawString(407, firma-12, "Juan Pablo Moreno Muro Dr. C. Ed.")
    c.drawString(450, firma - 22, "UNIDAD DE REGISTROS")
    c.drawString(470, firma - 30, "ACADÉMICOS")

    return c


def certificado_estudios_egresado_2(c, alumno, carrera, codigo, datos_totales):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    nombre_alumno = alumno.ApellidoPaterno + " " + \
                    alumno.ApellidoMaterno + ", " + alumno.Nombres
    c.setFont("Helvetica", 12)
    c.drawString(500, 670, codigo)
    c.drawString(70, 670, nombre_alumno.upper())
    # --------------------------
    # Lineas Formulario
    # --------------------------
    cadena_facultad = ''
    if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
        cadena_facultad = ' FACULTAD DE CIENCIAS DE INGENIERÍA'
    elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
        cadena_facultad = 'FACULTAD DE CIENCIAS SOCIALES, COMERCIALES Y DERECHO'
    c.setFont("Times-Bold", 16)
    c.drawString(195, 800, "UNIVERSIDAD DE LAMBAYEQUE")
    c.setFont("Times-Bold", 14)
    c.drawString(180, 780, cadena_facultad)
    c.setFont("Times-Bold", 14)
    c.drawString(140, 760, "ESCUELA PROFESIONAL DE " + carrera.upper())
    c.setFont("Helvetica-BoldOblique", 18)
    c.drawString(210, 730, "Certificado de Estudios")

    c.setFont("Helvetica", 12)
    c.drawString(40, 710, "El jefe de la oficina de registros Académicos")
    c.setFont("Helvetica-Bold", 14)
    c.drawString(40, 690, "CERTIFICA  :")

    c.setFont("Helvetica", 12)
    c.drawString(40, 670, "Que, ")
    c.drawString(430, 670, "Código NÂ°, ")

    c.setFont("Helvetica", 12)
    c.drawString(40, 650, "ha obtenido las siguientes calificaciones finales:")

    # CABECERA DE TABLA
    c.setFont("Times-Bold", 12)
    c.drawString(46, 610, "ASIGNATURAS")
    c.drawString(350, 610, "CALIFICATIVO")
    c.drawString(450, 610, "CRED.")
    c.drawString(500, 610, "FECHAS")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(40, 630, 560, 630)
    c.line(40, 600, 560, 600)

    dict_semestre = {'1': 'PRIMER SEMESTRE', '2': 'SEGUNDO SEMESTRE', '3': 'TERCER SEMESTRE', '4': 'CUARTO SEMESTRE',
                     '5': 'QUINTO SEMESTRE',
                     '6': 'SEXTO SEMESTRE', '7': 'SÉTIMO SEMESTRE', '8': 'OCTAVO SEMESTRE', '9': 'NOVENO SEMESTRE',
                     '10': 'DÉCIMO SEMESTRE', '11': 'ONCEAVO SEMESTRE', '12': 'DOCEAVO SEMESTRE',
                     '13': 'TRECEAVO SEMESTRE'}
    CREDITOS_CARRERA = {'AT': 201, 'AM': 205, 'IA': 203, 'IC': 205, 'IS': 202}
    total_creditos_carrera = str(
        CREDITOS_CARRERA['%s' % alumno.Carrera.Codigo])

    # EMPEZAMOS CONTENIDO DE TABLA
    nro_semestres = len(datos_totales)
    inicio = 590
    terminado = False
    promedio_ponderado_acumulado_final = Decimal("0.000")
    total_creditos_aprobados = 0
    hoja_dos_generada = False

    for y in xrange(0, nro_semestres):
        # VALIDACIÃN SALTO DE PÃGINA
        if inicio < 40:
            hoja_dos_generada = True
            auxiliar = 30
            c.line(40, 630, 40, auxiliar)
            c.line(560, 630, 560, auxiliar)
            c.line(40, auxiliar, 560, auxiliar)

            # lineas de los campos del encabezado estudiante, codigo, carrera.
            c.line(70, 665, 410, 665)  # estudiante
            c.line(490, 665, 555, 665)  # codigo
            logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
            c.drawImage(logo, 50, 730, width=90, height=100)
            c.showPage()
            inicio = 780

        lista_cursos = datos_totales[y][1]
        nro_cursos = len(lista_cursos)

        if nro_cursos == 0:
            continue

        # Nombre del semestre
        c.setFont("Helvetica-Bold", 10)
        aux = datos_totales[y][0]
        c.drawString(46, inicio, dict_semestre[str(aux)])

        # DATOS
        inicio = inicio - 10
        y1 = inicio
        for x in xrange(0, nro_cursos):
            # VALIDACIÃN SALTO DE PÃGINA
            if inicio < 40:
                hoja_dos_generada = True
                auxiliar = 30
                c.line(40, 630, 40, auxiliar)
                c.line(560, 630, 560, auxiliar)
                c.line(40, auxiliar, 560, auxiliar)

                # lineas de los campos del encabezado estudiante, codigo, carrera.
                c.line(70, 665, 410, 665)  # estudiante
                c.line(490, 665, 555, 665)  # codigo
                logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
                c.drawImage(logo, 50, 730, width=90, height=100)
                c.showPage()
                inicio = 780
                y1 = inicio

            codigo_curso = lista_cursos[x][0]
            nombre_curso = lista_cursos[x][1].upper()
            nota_letra = lista_cursos[x][5]
            resolucion = lista_cursos[x][6]
            nota_numero = "(" + lista_cursos[x][4] + ")"
            creditos = lista_cursos[x][3]
            semestre_curso = lista_cursos[x][2]

            c.setFont("Helvetica", 8)
            c.drawString(46, y1, codigo_curso)
            c.drawString(90, y1, nombre_curso + resolucion)
            c.drawString(370, y1, nota_letra)
            c.drawString(410, y1, nota_numero)
            c.drawString(460, y1, creditos)
            c.drawString(520, y1, semestre_curso)
            y1 = y1 - 10
            inicio = inicio - 10

        # VALIDACIÃN SALTO DE PÃGINA
        if inicio < 40:
            hoja_dos_generada = True
            auxiliar = 30
            c.line(40, 630, 40, auxiliar)
            c.line(560, 630, 560, auxiliar)
            c.line(40, auxiliar, 560, auxiliar)

            # lineas de los campos del encabezado estudiante, codigo, carrera.
            c.line(70, 665, 410, 665)  # estudiante
            c.line(490, 665, 555, 665)  # codigo
            logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
            c.drawImage(logo, 50, 730, width=90, height=100)
            c.showPage()
            inicio = 780
            y1 = inicio

        # PROMEDIO PONDERADO
        promedio_ponderado_semestral = str(datos_totales[y][2])
        promedio_ponderado_acumulado = str(datos_totales[y][3])
        total_creditos_aprobados = str(datos_totales[y][4])
        promedio_ponderado_acumulado_final = promedio_ponderado_acumulado
        c.setFont("Helvetica-Bold", 8)
        c.drawString(46, y1, "PPS = " + str(promedio_ponderado_semestral))
        c.drawString(370, y1, "PPA = " + str(promedio_ponderado_acumulado))
        inicio = inicio - 20

    # VALIDACIÃN SALTO DE PÃGINA
    if inicio < 40:
        hoja_dos_generada = True
        auxiliar = 30
        c.line(40, 630, 40, auxiliar)
        c.line(560, 630, 560, auxiliar)
        c.line(40, auxiliar, 560, auxiliar)

        # lineas de los campos del encabezado estudiante, codigo, carrera.
        c.line(70, 665, 410, 665)  # estudiante
        c.line(490, 665, 555, 665)  # codigo
        logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
        c.drawImage(logo, 50, 730, width=90, height=100)
        c.showPage()
        inicio = 780

    # Promedio acumulados finales y total de creditos aprobados
    c.setFont("Helvetica-Bold", 9)
    inicio = inicio - 10
    c.drawString(46, inicio, "PROMEDIO PONDERADO ACUMULADO: " + str(promedio_ponderado_acumulado_final))
    c.drawString(350, inicio, "TOTAL DE CREDITOS DEL PROGRAMA : " + str(total_creditos_carrera))
    inicio = inicio - 10
    c.drawString(350, inicio, "TOTAL DE CREDITOS APROBADOS       : " + str(total_creditos_aprobados))

    if hoja_dos_generada == False:
        auxiliar = 30
        c.line(40, 630, 40, auxiliar)
        c.line(560, 630, 560, auxiliar)
        c.line(40, auxiliar, 560, auxiliar)

        # lineas de los campos del encabezado estudiante, codigo, carrera.
        c.line(70, 665, 410, 665)  # estudiante
        c.line(490, 665, 555, 665)  # codigo
        logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
        c.drawImage(logo, 50, 730, width=90, height=100)
        c.showPage()
        inicio = 780

    # SEGUNDA HOJA
    # CABECERA DE TABLA
    c.setFont("Times-Bold", 12)
    c.drawString(46, 800, "ASIGNATURAS")
    c.drawString(350, 800, "CALIFICATIVO")
    c.drawString(450, 800, "CRED.")
    c.drawString(500, 800, "FECHAS")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # Lineas del cuadro segunda hoja
    c.line(40, 820, 560, 820)
    c.line(40, 790, 560, 790)

    inicio = 180
    c.line(40, 820, 40, inicio)
    c.line(560, 820, 560, inicio)
    c.line(40, inicio, 560, inicio)

    inicio = 170
    # inicio = inicio - 10

    c.drawString(
        46, inicio, "Así consta en las actas de exámenes finales, a las que remito en caso necesario.")

    inicio = inicio - 10
    # cuadrado de foto
    c.roundRect(46, inicio - 80, 80, 80, 0, stroke=1, fill=0)
    c.drawString(76, inicio - 40, "Foto")

    mydate = datetime.datetime.now()

    mn = {'01': 'Enero', '02': 'Febrero', '03': 'Marzo', '04': 'Abril', '05': 'Mayo',
          '06': 'Junio', '07': 'Julio', '08': 'Agosto', '09': 'Setiembre',
          '10': 'Octubre', '11': 'Noviembre', '12': 'Diciembre'}

    c.drawString(350, 140, "Chiclayo,   " + str(mydate.strftime("%d")))
    # c.line(395, 140, 410, 140)
    c.drawString(415, 140, "de   " + str(mn[mydate.strftime("%m")]))
    # c.line(430, 140, 500, 140)
    c.drawString(505, 140, "del  " + str(mydate.strftime("%Y")))
    # c.line(520, 140, 555, 140)
    c.setFont("Helvetica", 8)

    # ESCALA DE CALIFICATIVOS
    iniciox = 130
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "ESCALA DE CALIFICATIVOS")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "00 - 10 Desaprobado")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "11 - 20 Aprobado")

    # ESCALA DE CALIFICATIVOS
    inicio = inicio - 15
    c.drawString(iniciox, inicio, "ABREVIATURAS")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "PPS Promedio Ponderado Semestral")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "PPA Promedio Ponderado Acumulativo")

    # ###
    firma = 60
    if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
        c.drawString(180, firma - 22, "DECANO")
        c.drawString(140, firma - 30, cadena_facultad)
    elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
        c.drawString(250, firma - 22, "DECANO")
        c.drawString(140, firma - 30, cadena_facultad)

    c.drawString(450, firma - 22, "UNIDAD DE REGISTROS")
    c.drawString(470, firma - 30, "ACADÉMICOS")

    return c


def certificado_estudios_simple(c, alumno, carrera, codigo, datos_totales):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    nombre_alumno = alumno.ApellidoPaterno + " " + \
                    alumno.ApellidoMaterno + ", " + alumno.Nombres
    c.setFont("Helvetica", 12)
    c.drawString(500, 690, codigo + "  ,")
    c.drawString(70, 690, nombre_alumno.upper())
    # --------------------------
    # Lineas Formulario
    # --------------------------
    cadena_facultad = ''
    if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
        cadena_facultad = ' FACULTAD DE CIENCIAS DE INGENIERÍA'
        facu = 1
    elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
        cadena_facultad = 'FACULTAD DE CIENCIAS SOCIALES, COMERCIALES Y DERECHO'
        facu = 2
    c.setFont("Times-Bold", 16)
    c.drawString(195, 800, "UNIVERSIDAD DE LAMBAYEQUE")
    c.setFont("Times-Bold", 14)
    if facu == 1:
        c.drawString(180, 780, cadena_facultad)
    if facu == 2:
        c.drawString(131, 780, cadena_facultad)
    c.setFont("Times-Bold", 14)
    c.drawString(140, 760, "ESCUELA PROFESIONAL DE " + carrera.upper())
    c.setFont("Helvetica-BoldOblique", 18)
    c.drawString(210, 730, "Certificado de Estudios")

    c.setFont("Helvetica", 12)
    c.drawString(40, 710, "El Jefe de la Oficina de Registros Académicos certifica:")
    # c.setFont("Helvetica-Bold", 14)
    # c.drawString(40, 690, "CERTIFICA  :")

    c.setFont("Helvetica", 12)
    c.drawString(40, 690, "Que ")
    c.drawString(430, 690, ", con código ")

    c.setFont("Helvetica", 12)
    c.drawString(40, 670, "estudiante de la escuela profesional de " + carrera.upper() + ", registra las siguientes")
    c.drawString(40, 650, "asignaturas:")

    # CABECERA DE TABLA
    c.setFont("Times-Bold", 12)
    c.drawString(46, 610, "ASIGNATURAS")
    c.drawString(350, 610, "CALIFICATIVO")
    c.drawString(450, 610, "CRED.")
    c.drawString(500, 610, "FECHAS")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(40, 630, 560, 630)
    c.line(40, 600, 560, 600)

    dict_semestre = {'1': 'PRIMER SEMESTRE', '2': 'SEGUNDO SEMESTRE', '3': 'TERCER SEMESTRE', '4': 'CUARTO SEMESTRE',
                     '5': 'QUINTO SEMESTRE',
                     '6': 'SEXTO SEMESTRE', '7': 'SÉTIMO SEMESTRE', '8': 'OCTAVO SEMESTRE', '9': 'NOVENO SEMESTRE',
                     '10': 'DÉCIMO SEMESTRE', '11': 'ONCEAVO SEMESTRE'}
    CREDITOS_CARRERA = {'AT': 201, 'AM': 205, 'IA': 203, 'IC': 205, 'IS': 202}
    total_creditos_carrera = str(
        CREDITOS_CARRERA['%s' % alumno.Carrera.Codigo])

    # EMPEZAMOS CONTENIDO DE TABLA
    nro_semestres = len(datos_totales)
    inicio = 590
    terminado = False
    promedio_ponderado_acumulado_final = Decimal("0.000")
    total_creditos_aprobados = 0
    hoja_dos_generada = False

    for y in xrange(0, nro_semestres):
        # VALIDACIÃN SALTO DE PÃGINA
        if inicio < 40:
            hoja_dos_generada = True
            auxiliar = 30
            c.line(40, 630, 40, auxiliar)
            c.line(560, 630, 560, auxiliar)
            c.line(40, auxiliar, 560, auxiliar)

            # lineas de los campos del encabezado estudiante, codigo, carrera.
            c.line(70, 685, 410, 685)  # estudiante
            c.line(490, 685, 555, 685)  # codigo
            c.line(250, 665, 445, 665)  # carrera
            logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
            if facu == 1:
                c.drawImage(logo, 50, 730, width=90, height=100)
            if facu == 2:
                c.drawImage(logo, 40, 730, width=90, height=100)
            c.showPage()
            inicio = 780

        lista_cursos = datos_totales[y][1]
        nro_cursos = len(lista_cursos)

        if nro_cursos == 0:
            continue

        # Nombre del semestre
        c.setFont("Helvetica-Bold", 10)
        c.drawString(46, inicio, "Periodo " + str(datos_totales[y][5]) + ":")

        # DATOS
        inicio = inicio - 10
        y1 = inicio
        for x in xrange(0, nro_cursos):
            # VALIDACIÃN SALTO DE PÃGINA
            if inicio < 40:
                hoja_dos_generada = True
                auxiliar = 30
                c.line(40, 630, 40, auxiliar)
                c.line(560, 630, 560, auxiliar)
                c.line(40, auxiliar, 560, auxiliar)

                # lineas de los campos del encabezado estudiante, codigo, carrera.
                c.line(70, 685, 410, 685)  # estudiante
                c.line(490, 685, 555, 685)  # codigo
                c.line(250, 665, 445, 665)  # carrera
                logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
                if facu == 1:
                    c.drawImage(logo, 50, 730, width=90, height=100)
                if facu == 2:
                    c.drawImage(logo, 40, 730, width=90, height=100)
                c.showPage()
                inicio = 780
                y1 = inicio

            codigo_curso = lista_cursos[x][0]
            nombre_curso = lista_cursos[x][1].upper()
            nota_letra = lista_cursos[x][5]
            resolucion = lista_cursos[x][6]
            nota_numero = "(" + lista_cursos[x][4] + ")"
            creditos = lista_cursos[x][3]
            semestre_curso = lista_cursos[x][2]

            c.setFont("Helvetica", 8)
            c.drawString(46, y1, codigo_curso)
            c.drawString(90, y1, nombre_curso + resolucion)
            c.drawString(370, y1, nota_letra)
            c.drawString(410, y1, nota_numero)
            c.drawString(460, y1, creditos)
            c.drawString(520, y1, semestre_curso)
            y1 = y1 - 10
            inicio = inicio - 10

        # VALIDACIÃN SALTO DE PÃGINA
        if inicio < 40:
            hoja_dos_generada = True
            auxiliar = 30
            c.line(40, 630, 40, auxiliar)
            c.line(560, 630, 560, auxiliar)
            c.line(40, auxiliar, 560, auxiliar)

            # lineas de los campos del encabezado estudiante, codigo, carrera.
            c.line(70, 685, 410, 685)  # estudiante
            c.line(490, 685, 555, 685)  # codigo
            c.line(250, 665, 445, 665)  # carrera
            logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
            if facu == 1:
                c.drawImage(logo, 50, 730, width=90, height=100)
            if facu == 2:
                c.drawImage(logo, 40, 730, width=90, height=100)
            c.showPage()
            inicio = 780
            y1 = inicio

        # PROMEDIO PONDERADO
        promedio_ponderado_semestral = str(datos_totales[y][2])
        promedio_ponderado_acumulado = str(datos_totales[y][3])
        total_creditos_aprobados = str(datos_totales[y][4])
        promedio_ponderado_acumulado_final = promedio_ponderado_acumulado
        c.setFont("Helvetica-Bold", 8)
        c.drawString(46, y1, "PPS = " + str(promedio_ponderado_semestral))
        c.drawString(370, y1, "PPA = " + str(promedio_ponderado_acumulado))
        inicio = inicio - 20

    # VALIDACIÃN SALTO DE PÃGINA
    if inicio < 40:
        hoja_dos_generada = True
        auxiliar = 30
        c.line(40, 630, 40, auxiliar)
        c.line(560, 630, 560, auxiliar)
        c.line(40, auxiliar, 560, auxiliar)

        # lineas de los campos del encabezado estudiante, codigo, carrera.
        c.line(70, 685, 410, 685)  # estudiante
        c.line(490, 685, 555, 685)  # codigo
        c.line(250, 665, 445, 665)  # carrera
        logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
        if facu == 1:
            c.drawImage(logo, 50, 730, width=90, height=100)
        if facu == 2:
            c.drawImage(logo, 40, 730, width=90, height=100)
        c.showPage()
        inicio = 780

    # Promedio acumulados finales y total de creditos aprobados
    c.setFont("Helvetica-Bold", 9)
    inicio = inicio - 10
    c.drawString(46, inicio, "PROMEDIO PONDERADO ACUMULADO: " + str(promedio_ponderado_acumulado_final))
    c.drawString(350, inicio, "TOTAL DE CRÉDITOS DEL PROGRAMA : " + str(total_creditos_carrera))
    inicio = inicio - 10
    c.drawString(350, inicio, "TOTAL DE CRÉDITOS APROBADOS       : " + str(total_creditos_aprobados))

    if hoja_dos_generada == False:
        auxiliar = 30
        c.line(40, 630, 40, auxiliar)
        c.line(560, 630, 560, auxiliar)
        c.line(40, auxiliar, 560, auxiliar)

        # lineas de los campos del encabezado estudiante, codigo, carrera.
        c.line(70, 685, 410, 685)  # estudiante
        c.line(490, 685, 555, 685)  # codigo
        c.line(250, 665, 445, 665)  # carrera
        logo = settings.MEDIA_ROOT + 'logoHD2.jpg'
        if facu == 1:
            c.drawImage(logo, 50, 730, width=90, height=100)
        if facu == 2:
            c.drawImage(logo, 40, 730, width=90, height=100)
        c.showPage()
        inicio = 780

    # SEGUNDA HOJA
    # CABECERA DE TABLA
    c.setFont("Times-Bold", 12)
    c.drawString(46, 800, "ASIGNATURAS")
    c.drawString(350, 800, "CALIFICATIVO")
    c.drawString(450, 800, "CRED.")
    c.drawString(500, 800, "FECHAS")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(40, 820, 560, 820)
    c.line(40, 790, 560, 790)

    inicio = 200
    c.line(40, 820, 40, inicio)
    c.line(560, 820, 560, inicio)
    c.line(40, inicio, 560, inicio)

    inicio = 190
    # inicio = inicio - 10

    c.drawString(
        46, inicio, "Asi consta en los libros de Actas y/o Resoluciones a las que me remito en caso necesario.")

    inicio = inicio - 10

    c.drawString(
        46, inicio, "Se expide el presente certificado a solicitud del interesado.")

    inicio = inicio - 10

    c.drawString(
        46, inicio, "Nota: De acuerdo con el Reglamento de Estudios vigente la calificación es vigesimal.")

    inicio = inicio - 10
    # cuadrado de foto
    c.roundRect(46, inicio - 80, 80, 80, 0, stroke=1, fill=0)
    c.drawString(76, inicio - 40, "Foto")

    mydate = datetime.datetime.now()

    mn = {'01': 'Enero', '02': 'Febrero', '03': 'Marzo', '04': 'Abril', '05': 'Mayo',
          '06': 'Junio', '07': 'Julio', '08': 'Agosto', '09': 'Setiembre',
          '10': 'Octubre', '11': 'Noviembre', '12': 'Diciembre'}

    c.drawString(350, 140, "Chiclayo,   " + str(mydate.strftime("%d")))
    # c.line(395, 140, 410, 140)
    c.drawString(415, 140, "de   " + str(mn[mydate.strftime("%m")]))
    # c.line(430, 140, 500, 140)
    c.drawString(505, 140, "del  " + str(mydate.strftime("%Y")))
    # c.line(520, 140, 555, 140)
    c.setFont("Helvetica", 8)

    # ESCALA DE CALIFICATIVOS
    iniciox = 130
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "ESCALA DE CALIFICATIVOS")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "00 - 10 Desaprobado")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "11 - 20 Aprobado")

    # ESCALA DE CALIFICATIVOS
    inicio = inicio - 15
    c.drawString(iniciox, inicio, "ABREVIATURAS")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "PPS Promedio Ponderado Semestral")
    inicio = inicio - 10
    c.drawString(iniciox, inicio, "PPA Promedio Ponderado Acumulativo")

    firma = 60
    if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
        c.drawString(180, firma - 22, "DECANO")
        c.drawString(140, firma - 30, cadena_facultad)
    elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
        c.drawString(250, firma - 22, "DECANO")
        c.drawString(140, firma - 30, cadena_facultad)

    c.drawString(450, firma - 22, "UNIDAD DE REGISTROS")
    c.drawString(470, firma - 30, "ACADÉMICOS")

    return c


def historial_academico(c):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(205, 800, "EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    c.drawString(220, 780, "Historial Académico")

    c.setFont("Helvetica", 10)
    c.drawString(
        40, 710, "El jefe de la Oficina de Registros Académicos Certifica :")
    c.drawString(40, 690, "que:")
    c.drawString(40, 670, "estudiante de la carrera profesional de :")
    c.drawString(40, 650, "Registra las siguientes asignaturas :")
    c.drawString(450, 690, "Código :")
    c.roundRect(465, 730, 80, 80, 0, stroke=1, fill=0)

    c.setFont("Helvetica", 8)
    c.drawString(46, 610, "Nº")
    # c.drawString(44,610,"999")
    c.drawString(67, 610, "CODIGO")
    c.drawString(170, 610, "ASIGNATURAS")
    # c.drawString(105,610,"Introducción a la ingenieria de sistemas.")
    c.drawString(277, 610, "PERIODO")
    c.drawString(320, 610, "CRED.")

    c.drawString(350, 616, "NOTA")
    c.drawString(350, 600, "NROS")

    c.drawString(390, 616, "NOTA")
    # c.drawString(360,600,"diecinueve")
    c.drawString(385, 600, "LETRAS")

    # c.drawString(480,610,"OBSERVACIONES")
    c.drawString(455, 610, "OBSERVACIONES")

    c.setFont("Helvetica", 10)
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # cuadrado de foto
    c.roundRect(465, 730, 80, 80, 0, stroke=1, fill=0)
    c.drawString(495, 770, "Foto")

    # lineas de los campos del encabezado estudiante, codigo, carrera.
    c.line(70, 685, 448, 685)  # estudiante
    c.line(490, 685, 555, 685)  # codigo
    c.line(220, 665, 555, 665)  # carrera

    c.drawString(
        40, 150, "Asi consta en los libros de Actas y/o Resoluciones a las que me remito en caso necesario.")
    c.drawString(350, 120, "Chiclayo, ")
    c.line(395, 120, 410, 120)
    c.drawString(415, 120, "de")
    c.line(430, 120, 500, 120)
    c.drawString(505, 120, "de")
    c.line(520, 120, 555, 120)

    # c.drawString(40, 40, "Nota :")
    # c.drawString(
    #     100, 30, "1) Se considera nota desaprobada de '0' a '12' y nota aprobada de '13' a '20'")
    # c.drawString(
    #     100, 20, "2) Toda enmendadura o borrón invalida el documento.")
    # c.drawString(
    #     100, 10, "3) el presente documento carece de valor sin la impresión del sello de agua en la fotografía.")

    # 1ra Linea del Formulario
    c.line(margenLeft, 630, anchoA4 - margenRight, 630)
    c.line(margenLeft, 590, anchoA4 - margenRight, 590)  # 2da Linea ...
    c.line(margenLeft, 580, anchoA4 - margenRight, 580)  # 3da Linea ...
    c.line(margenLeft, 160, anchoA4 - margenRight, 160)  # 4da Linea ...

    # footer
    c.line(margenLeft, 50, anchoA4 - margenRight, 50)  # 5ta Linea ...

    c.line(margenLeft, 590, margenLeft, 630)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 590, anchoA4 - margenRight, 630)

    c.line(margenLeft, 580, margenLeft, 160)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 580, anchoA4 - margenRight, 160)

    c.line(margenLeft + 20, 580, margenLeft + 20, 160)  # Linea Vertical 2...
    c.line(margenLeft + 67, 580, margenLeft + 67, 160)  # Linea Vertical 3...

    # Linea Vertical 4...
    c.line(anchoA4 - margenRight - 280, 580, anchoA4 - margenRight - 280, 160)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 237, 580, anchoA4 - margenRight - 237, 160)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 209, 580, anchoA4 - margenRight - 209, 160)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 179, 580, anchoA4 - margenRight - 179, 160)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 130, 580, anchoA4 - margenRight - 130, 160)

    c.line(margenLeft + 20, 590, margenLeft + 20, 630)  # Linea Vertical 2...
    c.line(margenLeft + 67, 590, margenLeft + 67, 630)  # Linea Vertical 3...

    # Linea Vertical 4...
    c.line(anchoA4 - margenRight - 280, 590, anchoA4 - margenRight - 280, 630)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 237, 590, anchoA4 - margenRight - 237, 630)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 209, 590, anchoA4 - margenRight - 209, 630)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 179, 590, anchoA4 - margenRight - 179, 630)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 130, 590, anchoA4 - margenRight - 130, 630)

    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logoHD.jpg'
    c.drawImage(logo, 10, 740, width=180, height=102)
    return c


def boleta_notas(c):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    # --------------------------
    # Numero de guia
    # --------------------------
    # while i < 1734:
    #   c.drawString(10,i, str(i))
    #   i=i+4
    # j=0
    # while j < 1734:
    #   c.drawString(j,830, str(j))
    #   j=j+10

    # --------------------------
    # Rellenos
    # --------------------------
    # k=120
    # interlineado=20
    # c.setStrokeColorRGB(0.75,0.75,0.75)
    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # while k < 300:
    ##c.drawString(10,k, str(k))
    # c.line(margenLeft,k,anchoA4 - margenRight,k) #Lineas Formulario ...
    # l = k
    # if l%40==0:
    # while l < k+interlineado:
    # c.line(margenLeft,l,anchoA4 - margenRight,l) #Lineas Formulario ...
    # l = l + 1

    # k=k+interlineado

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(205, 385, "EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365, "Boleta de Notas")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335, "Estudiante :")
    c.drawString(40, 315, "Carrera :")
    c.drawString(435, 335, "Código :")
    c.drawString(435, 315, "Semestre :")

    c.drawString(50, 285, "NÂº")
    c.drawString(74, 285, "Código")
    c.drawString(120, 285, "G.H.")
    c.drawString(265, 285, "Asignatura")
    c.drawString(446, 285, "Créditos")
    c.drawString(513, 285, "Nota")

    c.drawString(350, 70, "Chiclayo, ")
    c.line(210, 35, 390, 35)
    c.drawString(215, 20, "Responsable de la Carrera Profesional")

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(margenLeft, 300, anchoA4 - margenRight, 300)  # 1era Linea ...
    c.line(margenLeft, 280, anchoA4 - margenRight, 280)  # 2da Linea ...
    c.line(margenLeft, 120, anchoA4 - margenRight, 120)  # 3da Linea ...

    c.line(margenLeft, 300, margenLeft, 120)  # Linea Vertical Izquirda...
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300, anchoA4 - margenRight, 120)

    c.line(margenLeft + 30, 300, margenLeft + 30, 120)  # Linea Vertical 2...
    c.line(margenLeft + 70, 300, margenLeft + 70, 120)  # Linea Vertical 3...
    c.line(margenLeft + 110, 300, margenLeft + 110, 120)  # Linea Vertical 4...
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 60, 300, anchoA4 - margenRight - 60, 120)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 120, 300, anchoA4 - margenRight - 120, 120)

    # resumen de creditos
    c.line(margenLeft, 110, margenLeft + 160, 110)  # 1era Linea horiz
    c.drawString(43, 95, "Créditos aprobados")
    c.line(margenLeft, 90, margenLeft + 160, 90)  # 2da Linea horiz
    c.drawString(43, 75, "Créditos desaprobados")
    c.line(margenLeft, 70, margenLeft + 160, 70)  # 3da Linea horiz

    c.line(margenLeft, 110, margenLeft, 70)  # 1era Linea vertical
    c.line(margenLeft + 110, 110, margenLeft + 110, 70)  # 2da Linea vertical
    c.line(margenLeft + 160, 110, margenLeft + 160, 70)  # 3da Linea vertical

    # promedio ponderado
    c.line(anchoA4 - margenRight - 180, 110, anchoA4 -
           margenRight, 110)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 180, 90, anchoA4 -
           margenRight, 90)  # 1era Linea horiz

    c.line(anchoA4 - margenRight - 180, 110, anchoA4 -
           margenRight - 180, 90)  # 1era Linea vertical
    c.drawString(anchoA4 - margenRight - 175, 95, "Promedio Ponderado")
    c.line(anchoA4 - margenRight - 60, 110, anchoA4 -
           margenRight - 60, 90)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110, anchoA4 -
           margenRight, 90)  # 3da Linea vertical

    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logoHD.jpg'
    c.drawImage(logo, 30, 355, width=130, height=62)
    return c


def boleta_notas1(c):
    w = 420
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    # --------------------------
    # Numero de guia
    # --------------------------
    # while i < 1734:
    #   c.drawString(10,i, str(i))
    #   i=i+4
    # j=0
    # while j < 1734:
    #   c.drawString(j,830, str(j))
    #   j=j+10

    # --------------------------
    # Rellenos
    # --------------------------
    # k=120
    # interlineado=20
    # c.setStrokeColorRGB(0.75,0.75,0.75)
    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # while k < 300:
    ##c.drawString(10,k, str(k))
    # c.line(margenLeft,k,anchoA4 - margenRight,k) #Lineas Formulario ...
    # l = k
    # if l%40==0:
    # while l < k+interlineado:
    # c.line(margenLeft,l,anchoA4 - margenRight,l) #Lineas Formulario ...
    # l = l + 1

    # k=k+interlineado

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(205, 385 + w, "EVALUACIÓN Y REGISTRO")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365 + w, "Boleta de Notas")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335 + w, "Estudiante :")
    c.drawString(40, 315 + w, "Carrera :")
    c.drawString(435, 335 + w, "Código :")
    c.drawString(435, 315 + w, "Semestre :")

    c.drawString(50, 285 + w, "NÂº")
    c.drawString(74, 285 + w, "Código")
    c.drawString(120, 285 + w, "G.H.")
    c.drawString(265, 285 + w, "Asignatura")
    c.drawString(446, 285 + w, "Créditos")
    c.drawString(513, 285 + w, "Nota")

    c.drawString(350, 70 + w, "Chiclayo, ")
    c.line(210, 35 + w, 390, 35 + w)
    c.drawString(215, 20 + w, "Responsable de la Carrera Profesional")

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # 1era Linea ...
    c.line(margenLeft, 300 + w, anchoA4 - margenRight, 300 + w)
    # 2da Linea ...
    c.line(margenLeft, 280 + w, anchoA4 - margenRight, 280 + w)
    # 3da Linea ...
    c.line(margenLeft, 120 + w, anchoA4 - margenRight, 120 + w)

    # Linea Vertical Izquirda...
    c.line(margenLeft, 300 + w, margenLeft, 120 + w)
    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300 + w, anchoA4 - margenRight, 120 + w)

    # Linea Vertical 2...
    c.line(margenLeft + 30, 300 + w, margenLeft + 30, 120 + w)
    # Linea Vertical 3...
    c.line(margenLeft + 70, 300 + w, margenLeft + 70, 120 + w)
    # Linea Vertical 4...
    c.line(margenLeft + 110, 300 + w, margenLeft + 110, 120 + w)
    # Linea Vertical 5...
    c.line(anchoA4 - margenRight - 60, 300 + w,
           anchoA4 - margenRight - 60, 120 + w)
    # Linea Vertical 6...
    c.line(anchoA4 - margenRight - 120, 300 + w,
           anchoA4 - margenRight - 120, 120 + w)

    # resumen de creditos
    c.line(margenLeft, 110 + w, margenLeft + 160, 110 + w)  # 1era Linea horiz
    c.drawString(43, 95 + w, "Créditos aprobados")
    c.line(margenLeft, 90 + w, margenLeft + 160, 90 + w)  # 2da Linea horiz
    c.drawString(43, 75 + w, "Créditos desaprobados")
    c.line(margenLeft, 70 + w, margenLeft + 160, 70 + w)  # 3da Linea horiz

    c.line(margenLeft, 110 + w, margenLeft, 70 + w)  # 1era Linea vertical
    c.line(margenLeft + 110, 110 + w, margenLeft +
           110, 70 + w)  # 2da Linea vertical
    c.line(margenLeft + 160, 110 + w, margenLeft +
           160, 70 + w)  # 3da Linea vertical

    # promedio ponderado
    c.line(anchoA4 - margenRight - 180, 110 + w, anchoA4 -
           margenRight, 110 + w)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 180, 90 + w, anchoA4 -
           margenRight, 90 + w)  # 1era Linea horiz

    c.line(anchoA4 - margenRight - 180, 110 + w, anchoA4 -
           margenRight - 180, 90 + w)  # 1era Linea vertical
    c.drawString(anchoA4 - margenRight - 175, 95 + w, "Promedio Ponderado")
    c.line(anchoA4 - margenRight - 60, 110 + w, anchoA4 -
           margenRight - 60, 90 + w)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110 + w, anchoA4 -
           margenRight, 90 + w)  # 3da Linea vertical

    c.setDash(6, 3)
    c.line(0, 421, anchoA4, 421)
    # --------------------------
    # Logo
    # --------------------------
    logo = settings.MEDIA_ROOT + 'logoHD.jpg'
    c.drawImage(logo, 30, 355 + w, width=130, height=62)
    return c


def ficha_matricula(c):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    k = 120
    interlineado = 20

    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(180, 385, "UNIDAD DE REGISTROS ACADÉMICOS")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365, "Ficha de Matrícula")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335, "Estudiante :")
    c.drawString(40, 315, "Carrera :")
    c.drawString(435, 335, "Código :")
    c.drawString(435, 315, "Semestre :")

    c.drawString(60, 285, "CÃDIGO CURSO")
    c.drawString(180, 285, "G.H.")
    c.drawString(310, 285, "NOMBRE DEL CURSO")
    c.drawString(520, 285, "CRED")

    c.line(93, 35, 205, 35)
    c.drawString(100, 20, "Firma del estudiante")
    c.line(390, 35, 502, 35)
    c.drawString(400, 20, "Firma del responsable")
    # c.line(margenLeft,630,anchoA4 - margenRight,630)

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    c.line(margenLeft, 300, anchoA4 - margenRight - 335, 300)  # 1era Linea ...
    c.line(margenLeft, 280, anchoA4 - margenRight - 335, 280)  # 2da Linea ...
    c.line(margenLeft, 120, anchoA4 - margenRight - 335, 120)  # 3da Linea ...

    c.line(margenLeft, 300, margenLeft, 120)  # Linea Vertical Izquirda...
    c.line(margenLeft + 120, 300, margenLeft + 120, 120)  # Linea Vertical 7...
    # Linea Vertical 10...
    c.line(margenLeft + 180, 300, margenLeft + 180, 120)

    # 1era Linea ...
    c.line(margenLeft + 185, 300, anchoA4 - margenRight - 45, 300)
    # 2da Linea ...
    c.line(margenLeft + 185, 280, anchoA4 - margenRight - 45, 280)
    # 3da Linea ...
    c.line(margenLeft + 185, 120, anchoA4 - margenRight - 45, 120)

    # Linea Vertical 11...
    c.line(margenLeft + 185, 300, margenLeft + 185, 120)
    # Linea Vertical 12...
    c.line(margenLeft + 470, 300, margenLeft + 470, 120)

    c.line(margenLeft + 475, 300, anchoA4 - margenRight, 300)  # 1era Linea ...
    c.line(margenLeft + 475, 280, anchoA4 - margenRight, 280)  # 2da Linea ...
    c.line(margenLeft + 475, 120, anchoA4 - margenRight, 120)  # 3da Linea ...

    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300, anchoA4 - margenRight, 120)
    # Linea Vertical Derecha 2...
    c.line(anchoA4 - margenRight - 40, 300, anchoA4 - margenRight - 40, 120)

    # fecha
    c.drawString(43, 95, "Fecha:")
    c.line(margenLeft + 50, 90, margenLeft + 140, 90)  # 2da Linea horiz
    c.line(margenLeft + 80, 93, margenLeft + 85, 103)  # slash1
    c.line(margenLeft + 110, 93, margenLeft + 115, 103)  # slash2

    # total de creditos
    c.line(anchoA4 - margenRight - 40, 110, anchoA4 -
           margenRight, 110)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 40, 90, anchoA4 -
           margenRight, 90)  # 1era Linea horiz

    c.drawString(anchoA4 - margenRight - 125, 95, "Total de Créditos")
    c.line(anchoA4 - margenRight - 40, 110, anchoA4 -
           margenRight - 40, 90)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110, anchoA4 -
           margenRight, 90)  # 3da Linea vertical

    # --------------------------
    # Logo
    # --------------------------
    c.drawImage(settings.MEDIA_ROOT + 'logoHD.jpg',
                30, 355, width=130, height=62)
    return c


# esta ficha es para generar 2 fichas en una A4, esta es la ficha superior.


def ficha_matricula2(c):
    w = 420
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    k = 120
    interlineado = 20

    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(180, 385 + w, "UNIDAD DE REGISTROS ACADÉMICOS")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365 + w, "Ficha de Matrícula")

    c.setFont("Helvetica", 10)
    c.drawString(40, 335 + w, "Estudiante :")
    c.drawString(40, 315 + w, "Carrera :")
    c.drawString(435, 335 + w, "Código :")
    c.drawString(435, 315 + w, "Semestre :")

    c.drawString(60, 285 + w, "CÃDIGO CURSO")
    c.drawString(180, 285 + w, "G.H.")
    c.drawString(310, 285 + w, "NOMBRE DEL CURSO")
    c.drawString(520, 285 + w, "CRED")

    c.line(93, 35 + w, 205, 35 + w)
    c.drawString(100, 20 + w, "Firma del estudiante")
    c.line(390, 35 + w, 502, 35 + w)
    c.drawString(400, 20 + w, "Firma del responsable")
    # c.line(margenLeft,630,anchoA4 - margenRight,630)

    # aaa=HexColor('#f9fadf')
    c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

    # 1era Linea ...
    c.line(margenLeft, 300 + w, anchoA4 - margenRight - 335, 300 + w)
    # 2da Linea ...
    c.line(margenLeft, 280 + w, anchoA4 - margenRight - 335, 280 + w)
    # 3da Linea ...
    c.line(margenLeft, 120 + w, anchoA4 - margenRight - 335, 120 + w)

    # Linea Vertical Izquirda...
    c.line(margenLeft, 300 + w, margenLeft, 120 + w)
    # Linea Vertical 7...
    c.line(margenLeft + 120, 300 + w, margenLeft + 120, 120 + w)
    # Linea Vertical 10...
    c.line(margenLeft + 180, 300 + w, margenLeft + 180, 120 + w)

    # 1era Linea ...
    c.line(margenLeft + 185, 300 + w, anchoA4 - margenRight - 45, 300 + w)
    # 2da Linea ...
    c.line(margenLeft + 185, 280 + w, anchoA4 - margenRight - 45, 280 + w)
    # 3da Linea ...
    c.line(margenLeft + 185, 120 + w, anchoA4 - margenRight - 45, 120 + w)

    # Linea Vertical 11...
    c.line(margenLeft + 185, 300 + w, margenLeft + 185, 120 + w)
    # Linea Vertical 12...
    c.line(margenLeft + 470, 300 + w, margenLeft + 470, 120 + w)

    # 1era Linea ...
    c.line(margenLeft + 475, 300 + w, anchoA4 - margenRight, 300 + w)
    # 2da Linea ...
    c.line(margenLeft + 475, 280 + w, anchoA4 - margenRight, 280 + w)
    # 3da Linea ...
    c.line(margenLeft + 475, 120 + w, anchoA4 - margenRight, 120 + w)

    # Linea Vertical Derecha...
    c.line(anchoA4 - margenRight, 300 + w, anchoA4 - margenRight, 120 + w)
    # Linea Vertical Derecha 2...
    c.line(anchoA4 - margenRight - 40, 300 + w,
           anchoA4 - margenRight - 40, 120 + w)

    # fecha
    c.drawString(43, 95 + w, "Fecha:")
    c.line(margenLeft + 50, 90 + w, margenLeft +
           140, 90 + w)  # 2da Linea horiz
    c.line(margenLeft + 80, 93 + w, margenLeft + 85, 103 + w)  # slash1
    c.line(margenLeft + 110, 93 + w, margenLeft + 115, 103 + w)  # slash2

    # total de creditos
    c.line(anchoA4 - margenRight - 40, 110 + w, anchoA4 -
           margenRight, 110 + w)  # 1era Linea horiz
    c.line(anchoA4 - margenRight - 40, 90 + w, anchoA4 -
           margenRight, 90 + w)  # 1era Linea horiz

    c.drawString(anchoA4 - margenRight - 125, 95 + w, "Total de Créditos")
    c.line(anchoA4 - margenRight - 40, 110 + w, anchoA4 -
           margenRight - 40, 90 + w)  # 2da Linea vertical
    c.line(anchoA4 - margenRight, 110 + w, anchoA4 -
           margenRight, 90 + w)  # 3da Linea vertical

    c.setDash(6, 3)
    c.line(0, 421, anchoA4, 421)
    # --------------------------
    # Logo
    # --------------------------
    c.drawImage(settings.MEDIA_ROOT + 'logoHD.jpg',
                30, 355 + w, width=130, height=62)
    return c


def constancia_matricula(c, w=0, primero=False):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40
    i = 0

    k = 120
    interlineado = 20

    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    # --------------------------
    # Lineas Formulario
    # --------------------------
    c.setFont("Helvetica", 14)
    c.drawString(180, 385 + w, "UNIDAD DE REGISTROS ACADÉMICOS")
    c.setFont("Helvetica", 14)
    c.drawString(250, 365 + w, "Constancia de Matrícula")

    c.setFont("Helvetica", 12)
    c.drawString(40, 300 + w, "El Jefe de la Unidad de Registros Académicos")
    c.drawString(40, 285 + w, "Hace constar:")
    c.drawString(40, 240 + w, "Que,")
    c.drawString(40, 225 + w, "código")
    c.drawString(285, 225 + w, "en  la  Universidad de Lambayeque en la Escuela")
    c.drawString(40, 210 + w, "Profesional de ")
    c.drawString(350, 210 + w, "en el semestre")
    c.drawString(40, 195 + w, "en un total de")
    c.drawString(150, 195 + w, "créditos.")

    c.line(390, 80 + w, 530, 80 + w)
    c.drawString(400, 65 + w, "Firma del responsable")

    if primero == True:
        c.setDash(6, 3)
        c.line(0, 421, anchoA4, 421)

    c.drawImage(settings.MEDIA_ROOT + 'logoHD.jpg', 30, 355 + w, width=130, height=62)
    return c
