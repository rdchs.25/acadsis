# -*- coding: utf-8 -*-
from django.conf.urls import url

from Notas.views import ver_editgrid_notas, master_editgrid_notas, exportar_notas_excel1

app_name = 'Notas'

urlpatterns = [
    url(r'^notas_asistencia/cursos/Notas/ver_editgrid_notas/$', ver_editgrid_notas),
    url(r'^notas_asistencia/cursos/Notas/master_editgrid_notas/$', master_editgrid_notas),
    url(r'^notas_asistencia/cursos/notas/exportar_excel/(\d+)/$', exportar_notas_excel1),
]
