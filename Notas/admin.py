# -*- coding: utf-8 -*-
from django import template
from django.contrib import admin
# imports para eliminar periodocurso
from django.contrib.admin.utils import unquote, get_deleted_objects
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.shortcuts import render_to_response
from django.utils.decorators import method_decorator
from django.utils.encoding import force_unicode
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_protect

from Notas.models import Nota

csrf_protect_m = method_decorator(csrf_protect)


class NotaAdmin(admin.ModelAdmin):
    list_display = ('PeriodoCurso', 'Nota', 'Peso', 'Orden',)

    @csrf_protect_m
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, perms_needed) = get_deleted_objects((obj,), opts, request.user, self.admin_site)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            obj.delete()

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Nota Eliminada Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        context = {
            "title": _("Are you sure?"),
            "object_name": force_unicode(opts.verbose_name),
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "opts": opts,
            "root_path": self.admin_site.root_path,
            "app_label": app_label,
        }
        context.update(extra_context or {})
        context_instance = template.RequestContext(request, current_app=self.admin_site.name)
        return render_to_response(self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, context_instance=context_instance)


admin.site.register(Nota, NotaAdmin)
