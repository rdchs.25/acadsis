# -*- coding: utf-8 -*-
from decimal import Decimal

from django.db import models

from Cursos.models import PeriodoCurso
from Matriculas.models import MatriculaCursos

# Create your models here.
ORDEN_CHOICES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
    ('7', '7'),
    ('8', '8'),
    ('9', '9'),
    ('10', '10'),
    ('11', '11'),
    ('12', '12'),
    ('13', '13'),
    ('14', '14'),
    ('15', '15'),
    ('16', '16'),
    ('17', '17'),
    ('18', '18'),
    ('19', '19'),
    ('20', '20'),
)

NIVEL_CHOICES = (
    ('0', '0'),
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
)


class Nota(models.Model):
    PeriodoCurso = models.ForeignKey(PeriodoCurso, verbose_name="Curso")
    Nota = models.CharField("Descripción", max_length=50)
    Identificador = models.CharField("Identificador", max_length=4, null=True, blank=True)
    Peso = models.DecimalField("Peso", max_digits=6, decimal_places=2)
    Nivel = models.CharField("Nivel", max_length=2, choices=NIVEL_CHOICES, default="0")
    Orden = models.CharField("Orden", max_length=2, choices=ORDEN_CHOICES)
    NotaPadre = models.ForeignKey('self', null=True, blank=True)
    SubNotas = models.BooleanField("¿Tiene Sub Notas ?", default=False)

    def __unicode__(self):
        return u'%s - %s - %s' % (self.Nota, self.Peso, self.PeriodoCurso)

    def total_peso_subnotas(self):
        total = Decimal("0.00")
        subnotas = Nota.objects.filter(NotaPadre=self)
        if subnotas.count() != 0:
            for nota in subnotas:
                total += nota.Peso
        else:
            total = Decimal("1.00")
        return total

    class Meta:
        verbose_name = "Nota"
        verbose_name_plural = "Notas"
        unique_together = (("PeriodoCurso", "Nivel", "Orden", "NotaPadre"),)
        permissions = (("read_nota", "Observar Configuracion de Notas"),)


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def guardarlog(linea):
    from django.utils.encoding import smart_str
    rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
           'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
           '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' '}
    from django.conf import settings
    f = open(settings.MEDIA_ROOT + 'lognotas.csv', 'a+')
    linea = replace_all(smart_str(linea), rep)
    f.write(unicode(linea))
    f.close()


class NotasAlumno(models.Model):
    MatriculaCursos = models.ForeignKey(MatriculaCursos, verbose_name="Curso")
    Nota = models.ForeignKey(Nota, verbose_name="Curso")
    Valor = models.DecimalField("Valor", max_digits=4, decimal_places=2)

    def save(self, **kwargs):
        if self.id is not None:
            try:
                notaanterior = NotasAlumno.objects.get(id=self.id).Valor
                if notaanterior != Decimal(self.Valor):
                    if kwargs is not None and kwargs['usuario'] is not None:
                        usuario = kwargs['usuario']
                        hora = kwargs['hora']
                        notaidentificador = self.Nota.Identificador
                        curso = self.MatriculaCursos.PeriodoCurso.__unicode__()
                        alumno = self.MatriculaCursos.MatriculaCiclo.Alumno.Codigo
                        nota = self.Valor
                        logstring = "%s,%s,%s,%s,%s,%s \n" % (usuario, curso, notaidentificador, nota, hora, alumno)
                        guardarlog(logstring)
            except:
                pass
        return super(NotasAlumno, self).save()

    def __unicode__(self):
        return u'%s - %s(%s %s)' % (
            self.MatriculaCursos.MatriculaCiclo.Alumno, self.MatriculaCursos.PeriodoCurso.Curso, self.Nota.Nota,
            self.Valor)

    class Meta:
        verbose_name = "Nota de Alumnos"
        verbose_name_plural = "Notas de Alumnos"
        permissions = (("read_notasalumno", "Observar Notas de Alumnos"),)
