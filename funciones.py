# -*- coding: utf-8 -*-
from datetime import datetime, date
from decimal import Decimal

import xlwt
from xlwt import Workbook, easyxf

from Pagos.models import PagoAlumno, DetallePago


def response_excel(titulo='REPORTE EXCEL', label_resumen=[], datos_resumen=[], heads=[], color=0x9ACD32, registros=[],
                   nombre_archivo='descargar'):
    book = Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 01')

    style_titulo = easyxf(
        'font: name Arial;''borders: left thick, right thick, top thick, bottom thick;''pattern: pattern solid, fore_colour red;',
        num_format_str='YYYY-MM-DD')
    style_titulo = easyxf('font: name Arial,bold True;')
    style_etiqueta_resumen = easyxf('font: name Arial,bold True;')
    style_dato_resumen = easyxf('font: name Arial;')
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid;')
    style_heads.pattern.pattern_fore_colour = color
    style_registros = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')
    style_registros_date = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;',
                                  num_format_str='YYYY-MM-DD')
    style_registros_datetime = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;',
                                      num_format_str='YYYY-MM-DD hh:mm')

    # escribir el titulo
    sheet.write(6, 0, titulo, style_titulo)

    row = 8
    col = 0
    # escribir las etiquetas del resumen
    for etiqueta in label_resumen:
        sheet.write(row, col, etiqueta, style_etiqueta_resumen)
        row += 1

    row = 8
    col = 1
    # escribir los datos del resumen
    for dato in datos_resumen:
        sheet.write(row, col, dato, style_dato_resumen)
        row += 1

    row += 1
    col = 1
    sheet.write(row, 0, 'Num', style_heads)
    # escribimos los encabezados
    for head in heads:
        sheet.write(row, col, head, style_heads)
        col += 1

    row += 1
    col = 1
    n = 1
    # recorremos la lista y escribimos los datos
    for fila in registros:
        sheet.write(row, 0, n, style_registros)
        for dato in fila:
            if isinstance(dato, datetime):
                sheet.write(row, col, dato, style_registros_datetime)
            elif isinstance(dato, date):
                sheet.write(row, col, dato, style_registros_date)
            else:
                sheet.write(row, col, dato, style_registros)
            col += 1
        col = 1
        row += 1
        n += 1

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % nombre_archivo
    book.save(response)
    return response


def response_excel2(titulo='REPORTE EXCEL', label_resumen=[], datos_resumen=[], heads=[], color=0x9ACD32, registros=[],
                    nombre_archivo='descargar', bookorigen=Workbook(encoding='utf8')):
    book = bookorigen
    sheet = book.add_sheet('Idiomas')

    style_titulo = easyxf(
        'font: name Arial;''borders: left thick, right thick, top thick, bottom thick;''pattern: pattern solid, fore_colour red;',
        num_format_str='YYYY-MM-DD')
    style_titulo = easyxf('font: name Arial,bold True;')
    style_etiqueta_resumen = easyxf('font: name Arial,bold True;')
    style_dato_resumen = easyxf('font: name Arial;')
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid;')
    style_heads.pattern.pattern_fore_colour = color
    style_registros = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')
    style_registros_date = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;',
                                  num_format_str='YYYY-MM-DD')
    style_registros_datetime = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;',
                                      num_format_str='YYYY-MM-DD hh:mm')

    # escribir el titulo
    sheet.write(6, 0, titulo, style_titulo)

    row = 8
    col = 0
    # escribir las etiquetas del resumen
    for etiqueta in label_resumen:
        sheet.write(row, col, etiqueta, style_etiqueta_resumen)
        row += 1

    row = 8
    col = 1
    # escribir los datos del resumen
    for dato in datos_resumen:
        sheet.write(row, col, dato, style_dato_resumen)
        row += 1

    row += 1
    col = 1
    sheet.write(row, 0, 'Num', style_heads)
    # escribimos los encabezados
    for head in heads:
        sheet.write(row, col, head, style_heads)
        col += 1

    row += 1
    col = 1
    n = 1
    # recorremos la lista y escribimos los datos
    for fila in registros:
        sheet.write(row, 0, n, style_registros)
        for dato in fila:
            if isinstance(dato, datetime):
                sheet.write(row, col, dato, style_registros_datetime)
            elif isinstance(dato, date):
                sheet.write(row, col, dato, style_registros_date)
            else:
                sheet.write(row, col, dato, style_registros)
            col += 1
        col = 1
        row += 1
        n += 1
    return book


def response_excelpago2(data, headers=None, titulo='REPORTE EXCEL', extra_data=None, nombre_archivo='descargar',
                        alumnopar=None):
    book = Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 01')
    headers = ["Código", "Nombres", "Carrera", "Matrículas", "Categoría"]

    style = xlwt.XFStyle()
    styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
              'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
              'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
              'default': xlwt.easyxf("borders: top thin, bottom thin, left thin,right thin;"),
              'break': xlwt.easyxf(""),
              'headers': xlwt.easyxf(
                  'pattern: pattern solid, fore_colour yellow;' "borders: top thin, bottom thin, left thin,right thin;"),
              'extra_data': xlwt.Style.default_style}

    borders = xlwt.Borders()
    borders.left = xlwt.Borders.THIN
    borders.right = xlwt.Borders.THIN
    borders.top = xlwt.Borders.THIN
    borders.bottom = xlwt.Borders.THIN

    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = 0xB4030D

    i = 0
    j = 10
    if extra_data != None:
        for x, extra in enumerate(extra_data):
            sheet.write(x + 7, 0, extra, style=styles['extra_data'])
            i = x + 8

    if headers != None:
        style.borders = borders
        style.pattern = pattern
        for x, head in enumerate(headers):
            sheet.write(i, x, head, style=styles['headers'])
            if x == 1:
                sheet.col(x).width = 8300
        i += 1

    for rowx, row in enumerate(data):
        cell_style = styles['default']
        if type(row) is unicode or type(row) is str:
            sheet.write(i, rowx, row, style=cell_style)
            # sheet.write_merge(i, i+3, rowx, rowx, row, style=cell_style)
        elif str(type(row)) == "<class 'django.db.models.query.ValuesListQuerySet'>":
            for rowy, value in enumerate(row):
                periodo = value[0] + "-" + value[1]
                sheet.write(i + rowy, rowx, periodo, style=cell_style)
                sheet.write(i + rowy, rowx + 1, value[2], style=cell_style)
            i += rowy
            j = i + 3
        elif type(row) is list:
            i = j
            for p, periodos in enumerate(row):
                if len(periodos) == 0:
                    continue
                if p == 0:
                    cell_style = styles['headers']
                    sheet.write(i, 0, "Periodo", style=cell_style)
                    sheet.write(i, 1, "Concepto", style=cell_style)
                    sheet.write(i, 2, "Monto (S/.)", style=cell_style)
                    sheet.write(i, 3, "Deuda (S/.)", style=cell_style)
                    cell_style = styles['default']
                    i += 1
                for rowy, value in enumerate(row[p]):
                    periodo = value.ConceptoPago.Periodo.__unicode__()
                    concepto = value.ConceptoPago.Descripcion
                    monto = value.Monto()
                    deuda = value.DeudaMonto()
                    sheet.write(i + rowy, 0, periodo, style=cell_style)
                    sheet.write(i + rowy, 1, concepto, style=cell_style)
                    sheet.write(i + rowy, 2, monto, style=cell_style)
                    sheet.write(i + rowy, 3, deuda, style=cell_style)
                i += rowy + 1
                sheet.write(i, 0, " ", style=styles["break"])
                i += 1

        else:
            continue

    # response = HttpResponse(content_type='application/vnd.ms-excel')
    # response['Content-Disposition'] = 'attachment; filename=%s.xls' % nombre_archivo
    # book.save(response)
    # return response

    listado = []
    pagos = DetallePago.objects.using('idiomas').filter(PagoAlumno__MatriculaCiclo__Alumno__Codigo=alumnopar).order_by(
        '-PagoAlumno__ConceptoPago__Periodo', 'PagoAlumno__Estado', '-FechaPago')
    if pagos.count() != 0:
        alumno = pagos[0].PagoAlumno.MatriculaCiclo.Alumno
        for pago in pagos:
            lista = []
            lista.append(alumno.Codigo)
            lista.append(alumno.ApellidoPaterno + ' ' + alumno.ApellidoMaterno)
            lista.append(alumno.Nombres)
            lista.append(alumno.Carrera.Carrera)
            lista.append(alumno.AnioIngreso + ' - ' + alumno.Semestre)
            lista.append(pago.PagoAlumno.ConceptoPago.Periodo.__unicode__())
            if pago.Mora is True:
                lista.append('Mora ' + pago.PagoAlumno.ConceptoPago.Descripcion)
            else:
                lista.append(pago.PagoAlumno.ConceptoPago.Descripcion)
            lista.append(pago.MontoDeuda)
            lista.append(pago.MontoCancelado)
            lista.append(pago.PagoRestante)
            lista.append(pago.Comprobante.TipoComprobante.Serie.Numero + '-' + pago.Comprobante.Correlativo)
            lista.append(pago.FechaPago)
            lista.append(pago.Comprobante.LugarPago)
            if pago.PagoAlumno.Estado == True or pago.PagoRestante == Decimal('0.00'):
                lista.append('Cancelado')
            else:
                lista.append('Pendiente')
            listado.append(lista)

    pagoalumno = PagoAlumno.objects.using('idiomas').filter(MatriculaCiclo__Alumno__Codigo=alumnopar,
                                                            Estado=False).order_by('-ConceptoPago__Periodo')
    if pagoalumno.count() != 0:
        alumno1 = pagoalumno[0].MatriculaCiclo.Alumno
        for pagoidioma in pagoalumno:
            if pagoidioma.detallepago_set.count() == 0:
                lista = []
                lista.append(alumno1.Codigo)
                lista.append(alumno1.ApellidoPaterno + ' ' + alumno1.ApellidoMaterno)
                lista.append(alumno1.Nombres)
                lista.append(alumno1.Carrera.Carrera)
                lista.append(alumno1.AnioIngreso + ' - ' + alumno1.Semestre)
                lista.append(pagoidioma.ConceptoPago.Periodo.__unicode__())
                lista.append(pagoidioma.ConceptoPago.Descripcion)
                lista.append(pagoidioma.DeudaMonto())
                lista.append(Decimal('0.00'))
                lista.append(pagoidioma.DeudaMonto())
                lista.append('')
                lista.append('')
                lista.append('')
                lista.append('Pendiente')
                listado.append(lista)

    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Ingreso', 'Semestre', 'Concepto', 'Monto de Pago',
               'Monto Pagado', 'Pago Restante', 'Comprobante', 'Fecha', 'Lugar', 'Estado']
    titulo = 'RESUMEN DE PAGOS Y DEUDAS'
    book = response_excel2(titulo=titulo, heads=headers, registros=listado, nombre_archivo='resumen_cobranza',
                           bookorigen=book)
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % nombre_archivo
    book.save(response)
    return response


def response_excelpago(data, headers=None, titulo='REPORTE EXCEL', extra_data=None, nombre_archivo='descargar'):
    book = Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 01')
    headers = ["Código", "Nombres", "Carrera", "Matrículas", "Categoría"]

    style = xlwt.XFStyle()
    styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
              'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
              'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
              'default': xlwt.easyxf("borders: top thin, bottom thin, left thin,right thin;"),
              'break': xlwt.easyxf(""),
              'headers': xlwt.easyxf(
                  'pattern: pattern solid, fore_colour yellow;' "borders: top thin, bottom thin, left thin,right thin;"),
              'extra_data': xlwt.Style.default_style}

    borders = xlwt.Borders()
    borders.left = xlwt.Borders.THIN
    borders.right = xlwt.Borders.THIN
    borders.top = xlwt.Borders.THIN
    borders.bottom = xlwt.Borders.THIN

    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = 0xB4030D

    i = 0
    j = 10
    if extra_data != None:
        for x, extra in enumerate(extra_data):
            sheet.write(x + 7, 0, extra, style=styles['extra_data'])
            i = x + 8

    if headers != None:
        style.borders = borders
        style.pattern = pattern
        for x, head in enumerate(headers):
            sheet.write(i, x, head, style=styles['headers'])
            if x == 1:
                sheet.col(x).width = 8300
        i += 1

    for rowx, row in enumerate(data):
        cell_style = styles['default']
        if type(row) is unicode or type(row) is str:
            sheet.write(i, rowx, row, style=cell_style)
            # sheet.write_merge(i, i+3, rowx, rowx, row, style=cell_style)
        elif str(type(row)) == "<class 'django.db.models.query.ValuesListQuerySet'>":
            for rowy, value in enumerate(row):
                periodo = value[0] + "-" + value[1]
                sheet.write(i + rowy, rowx, periodo, style=cell_style)
                sheet.write(i + rowy, rowx + 1, value[2], style=cell_style)
            i += rowy
            j = i + 3
        elif type(row) is list:
            i = j
            for p, periodos in enumerate(row):
                if len(periodos) == 0:
                    continue
                if p == 0:
                    cell_style = styles['headers']
                    sheet.write(i, 0, "Periodo", style=cell_style)
                    sheet.write(i, 1, "Concepto", style=cell_style)
                    sheet.write(i, 2, "Monto (S/.)", style=cell_style)
                    sheet.write(i, 3, "Deuda (S/.)", style=cell_style)
                    cell_style = styles['default']
                    i += 1
                for rowy, value in enumerate(row[p]):
                    periodo = value.ConceptoPago.Periodo.__unicode__()
                    concepto = value.ConceptoPago.Descripcion
                    monto = value.Monto()
                    deuda = value.DeudaMonto()
                    sheet.write(i + rowy, 0, periodo, style=cell_style)
                    sheet.write(i + rowy, 1, concepto, style=cell_style)
                    sheet.write(i + rowy, 2, monto, style=cell_style)
                    sheet.write(i + rowy, 3, deuda, style=cell_style)
                i += rowy + 1
                sheet.write(i, 0, " ", style=styles["break"])
                i += 1

        else:
            continue

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % nombre_archivo
    book.save(response)
    return response


def enviar_mail(emisor, passwd, receptor, asunto, html, archivos=[], path_archivos=""):
    # imports para enviar correos
    import smtplib
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEText import MIMEText
    from email.MIMEImage import MIMEImage
    from email.MIMEBase import MIMEBase
    from email import encoders
    # Establecemos conexion con el servidor smtp de gmail
    mailServer = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    mailServer.ehlo()
    mailServer.login(emisor, passwd)

    # Construimos un mensaje Multipart, con un texto y datos adjuntos
    mensaje = MIMEMultipart()
    mensaje['From'] = emisor
    for email in receptor:
        To = email
        mensaje['To'] = email
    mensaje['Subject'] = asunto

    # Adjuntamos el texto
    mensaje.attach(MIMEText(html, 'plain', _charset='utf-8'))

    # Adjuntamos la imagenes en caso se encuentre alguna
    for archivo in archivos:
        try:
            f = open(path_archivos + archivo, "rb")
            contenido = MIMEImage(file.read())
            contenido.add_header('Content-Disposition', 'attachment', filename=archivo)
            mensaje.attach(contenido)
        except:
            pass

    ## Adjuntamos los archivos en caso se encuentre alguno
    for archivo in archivos:
        try:
            f = open(path_archivos + archivo, 'rb')
            # contenido = MIMEBase('multipart', 'encrypted')
            contenido = MIMEBase('application', "octet-stream")
            contenido.set_payload(f.read())
            f.close()
            encoders.encode_base64(contenido)
            contenido.add_header('Content-Disposition', 'attachment', filename=archivo)
            mensaje.attach(contenido)
        except:
            pass

    # Enviamos el correo, con los campos emisor, receptor y mensaje
    confirmacion = mailServer.sendmail(emisor, receptor, mensaje.as_string())
    # Cierre de la conexion
    mailServer.close()
    if confirmacion == {}:
        return True
    else:
        return False


from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt


@csrf_protect
@csrf_exempt
def useracadsis(request):
    # if not request.is_ajax():
    username = ""
    if request.method == "POST":
        username = request.POST['username']
    # password = request.POST['password']
    return HttpResponse("[Access: OK]" + username)
    # else:
    #    raise Http404
    # else:
    #    raise Http404


def color_excel(titulo='REPORTE EXCEL', label_resumen=[], datos_resumen=[], heads=[], color=0x9ACD32, registros=[],
                nombre_archivo='descargar'):
    book = Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 01')

    style_titulo = easyxf(
        'font: name Arial;''borders: left thick, right thick, top thick, bottom thick;''pattern: pattern solid, fore_colour red;',
        num_format_str='YYYY-MM-DD')
    style_titulo = easyxf('font: name Arial,bold True;')
    style_etiqueta_resumen = easyxf('font: name Arial,bold True;')
    style_dato_resumen = easyxf('font: name Arial;')
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid;')
    style_heads.pattern.pattern_fore_colour = color
    style_registros_nocolor = easyxf('font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')
    style_registros_date = easyxf(
        'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid;',
        num_format_str='YYYY-MM-DD')
    style_registros_datetime = easyxf(
        'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid;',
        num_format_str='YYYY-MM-DD hh:mm')

    # escribir el titulo
    sheet.write(6, 0, titulo, style_titulo)

    row = 8
    col = 0
    # escribir las etiquetas del resumen
    for etiqueta in label_resumen:
        sheet.write(row, col, etiqueta, style_etiqueta_resumen)
        row += 1

    row = 8
    col = 1
    # escribir los datos del resumen
    for dato in datos_resumen:
        sheet.write(row, col, dato, style_dato_resumen)
        row += 1

    row += 1
    col = 1
    sheet.write(row, 0, 'Num', style_heads)
    # escribimos los encabezados
    for head in heads:
        sheet.write(row, col, head, style_heads)
        col += 1

    row += 1
    col = 1
    n = 0
    num = []
    blanco = True
    # recorremos la lista y escribimos los datos
    for fila in registros:
        num.append(n)
        if fila[0] != " ":
            n += 1
            if (n != num[-1]):
                if blanco:
                    blanco = False
                else:
                    blanco = True

            sheet.write(row, 0, n, style_registros_nocolor)

        if not blanco:
            style_registros_datetime = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour white;',
                num_format_str='YYYY-MM-DD hh:mm')
            style_registros_date = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour white;',
                num_format_str='YYYY-MM-DD')
            style_registros = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour white;')
            style_registros_nocolor = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour pale_blue;')
        else:
            style_registros_datetime = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour pale_blue;',
                num_format_str='YYYY-MM-DD hh:mm')
            style_registros_date = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour pale_blue;',
                num_format_str='YYYY-MM-DD')
            style_registros = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour pale_blue;')
            style_registros_nocolor = easyxf(
                'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid, fore_colour white;')

        for dato in fila:
            if isinstance(dato, datetime):
                sheet.write(row, col, dato, style_registros_datetime)
            elif isinstance(dato, date):
                sheet.write(row, col, dato, style_registros_date)
            else:
                sheet.write(row, col, dato, style_registros)
            col += 1
        col = 1
        row += 1

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % nombre_archivo
    book.save(response)
    return response
