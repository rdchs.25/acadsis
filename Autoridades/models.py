# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
from Carreras.models import Carrera


class Tipo(models.Model):
    Carrera = models.ForeignKey(Carrera, null=True, default=None)
    Nombre = models.CharField(max_length=200)

    # def __str__(self):
    #    return self.nombre
    def __unicode__(self):
        return self.Nombre

    class Meta:
        verbose_name = "Tipo de Autoridad"
        verbose_name_plural = "Tipos de Autoridad"


class Autoridad(models.Model):
    Apellidos = models.CharField(max_length=200)
    Nombres = models.CharField(max_length=200)
    Direccion = models.CharField(max_length=200)
    Celular = models.CharField("Celular", max_length=20, null=True, blank=True)
    Fijo = models.CharField("Fijo", max_length=12, null=True, blank=True)
    Abreviatura = models.CharField(max_length=10)

    # def __str__(self):
    #       return '%s %s'%(self.abreviatura, self.nombres, self.apellidos)
    def __unicode__(self):
        return '%s %s %s' % (self.Abreviatura, self.Nombres, self.Apellidos)

    class Meta:
        verbose_name = "Autoridad"
        verbose_name_plural = "Autoridades"


class Fecha(models.Model):
    Tipo = models.ForeignKey(Tipo)
    Autoridad = models.ForeignKey(Autoridad)
    Fechainicio = models.DateField("Fecha de Inicio")
    Fechafin = models.DateField("Fecha de Finalización", null=True, blank=True)
    Activo = models.BooleanField("Activo")

    class Meta:
        unique_together = ("Tipo", "Activo")
        verbose_name = "Autoridad por Fecha"
        verbose_name_plural = "Autoridades por Fecha"
