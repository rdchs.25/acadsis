# -*- coding: utf-8 -*-
from django.contrib import admin

from Autoridades.models import Tipo, Autoridad, Fecha


# Register your models here.
class AutoridadAdmin(admin.ModelAdmin):
    list_display = ('Apellidos', 'Nombres', 'Abreviatura')
    search_fields = ('Apellidos',)


class TipoAdmin(admin.ModelAdmin):
    list_display = ('Nombre',)
    search_fields = ('Nombre',)


class FechaAdmin(admin.ModelAdmin):
    list_display = ('Tipo', 'Autoridad', 'Fechainicio', 'Fechafin', 'Activo')


admin.site.register(Autoridad, AutoridadAdmin)
admin.site.register(Tipo, TipoAdmin)
admin.site.register(Fecha, FechaAdmin)
