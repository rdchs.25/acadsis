# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Autoridad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Apellidos', models.CharField(max_length=200)),
                ('Nombres', models.CharField(max_length=200)),
                ('Direccion', models.CharField(max_length=200)),
                ('Celular', models.CharField(max_length=20, null=True, verbose_name=b'Celular', blank=True)),
                ('Fijo', models.CharField(max_length=12, null=True, verbose_name=b'Fijo', blank=True)),
                ('Abreviatura', models.CharField(max_length=10)),
            ],
            options={
                'verbose_name': 'Autoridad',
                'verbose_name_plural': 'Autoridades',
            },
        ),
        migrations.CreateModel(
            name='Fecha',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Fechainicio', models.DateField(verbose_name=b'Fecha de Inicio')),
                ('Fechafin', models.DateField(null=True, verbose_name=b'Fecha de Finalizaci\xc3\xb3n', blank=True)),
                ('Activo', models.BooleanField(verbose_name=b'Activo')),
                ('Autoridad', models.ForeignKey(to='Autoridades.Autoridad')),
            ],
            options={
                'verbose_name': 'Autoridad por Fecha',
                'verbose_name_plural': 'Autoridades por Fecha',
            },
        ),
        migrations.CreateModel(
            name='Tipo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': 'Tipo de Autoridad',
                'verbose_name_plural': 'Tipos de Autoridad',
            },
        ),
        migrations.AddField(
            model_name='fecha',
            name='Tipo',
            field=models.ForeignKey(to='Autoridades.Tipo'),
        ),
        migrations.AlterUniqueTogether(
            name='fecha',
            unique_together=set([('Tipo', 'Activo')]),
        ),
    ]
