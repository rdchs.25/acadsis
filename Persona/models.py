# -*- coding: utf-8 -*-
from django.db import models

GENDER_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)
CIVIL_CHOICES = (
    ('Soltero', 'Soltero'),
    ('Casado', 'Casado'),
    ('Viudo', 'Viudo'),
    ('Divorciado', 'Divorciado'),
)


def obtenerpath_foto_persona(instance, filename):
    ext = filename.split('.')[-1]
    return u'docentes/%s/foto-%s.%s' % (instance.username, instance.username, ext)


class Persona(models.Model):
    Nombres = models.CharField(max_length=50)
    ApellidoPaterno = models.CharField("Ape. Pat.", max_length=50)
    ApellidoMaterno = models.CharField("Ape. Mat.", max_length=50)
    Email = models.EmailField("E-mail", null=True, blank=True)
    Nacimiento = models.DateField("Nacimiento", null=True, blank=True)
    Sexo = models.CharField(max_length=1, choices=GENDER_CHOICES, default='M')
    Dni = models.CharField("DNI", max_length=8, null=True, blank=True, unique=True)
    Direccion = models.CharField("Dirección", max_length=100, null=True, blank=True)
    Departamento = models.CharField("Departamento", max_length=50, null=True, blank=True)
    Provincia = models.CharField("Provincia", max_length=50, null=True, blank=True)
    Distrito = models.CharField("Distrito", max_length=50, null=True, blank=True)
    EstadoCivil = models.CharField("Estado Civil", max_length=20, choices=CIVIL_CHOICES, default='Soltero')
    Email2 = models.EmailField("E-mail 2", null=True, blank=True)
    Telefono = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    Celular = models.CharField("Celular", max_length=20, null=True, blank=True)
    Estado = models.BooleanField("Activado", default=True)
    Foto = models.ImageField(upload_to=obtenerpath_foto_persona, blank=True, null=True, verbose_name="Foto")
    Huella = models.BinaryField(null=True)
    Esdocente = models.BooleanField("¿Es docente?", default=False)
    Esadministrativo = models.BooleanField("¿Es administrativo?", default=False)

    def __unicode__(self):
        return u'%s %s %s' % (self.ApellidoPaterno, self.ApellidoMaterno, self.Nombres)

    class Meta:
        verbose_name = "Personas UDL"
        verbose_name_plural = "Personas UDL"
        ordering = ['ApellidoPaterno', 'ApellidoMaterno', 'Nombres']
