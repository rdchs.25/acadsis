#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Persona.models import Persona
from Docentes.models import Docente
# imports para traer variable de entorno django
import codecs


def script_migrar_administrativos():
    file1 = codecs.open("extras/admin.csv", 'r', 'utf-8')
    for line in file1:
        line = line.strip('\n')
        linea = line.split(',')
        dni = linea[0]
        apellidopaterno = linea[1].encode('utf-8')
        apellidomaterno = linea[2].encode('utf-8')
        nombres = linea[3].encode('utf-8')
        try:
            persona = Persona.objects.get(Dni=dni)
            persona.Esadministrativo = True
            persona.save()
        except Persona.DoesNotExist:
            persona = Persona(Dni=dni, ApellidoPaterno=apellidopaterno, ApellidoMaterno=apellidomaterno,
                              Nombres=nombres, Sexo="M", Esadministrativo=True)
            persona.save()


def script_migrar_docentes():
    docentes = Docente.objects.filter(Estado=True)
    for docente in docentes:
        try:
            persona = Persona.objects.get(Dni=docente.Dni)
            persona.Esdocente = True
            persona.save()
        except Persona.DoesNotExist:
            persona = Persona(Dni=docente.Dni, ApellidoPaterno=docente.ApellidoPaterno,
                              ApellidoMaterno=docente.ApellidoMaterno,
                              Nombres=docente.Nombres, Sexo=docente.Sexo, Esdocente=True, Huella=docente.Huella)
            persona.save()


script_migrar_administrativos()
script_migrar_docentes()
