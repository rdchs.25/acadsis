# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import Persona.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombres', models.CharField(max_length=50)),
                ('ApellidoPaterno', models.CharField(max_length=50, verbose_name=b'Ape. Pat.')),
                ('ApellidoMaterno', models.CharField(max_length=50, verbose_name=b'Ape. Mat.')),
                ('Email', models.EmailField(max_length=254, null=True, verbose_name=b'E-mail', blank=True)),
                ('Nacimiento', models.DateField(null=True, verbose_name=b'Nacimiento', blank=True)),
                ('Sexo', models.CharField(default=b'M', max_length=1, choices=[(b'M', b'Masculino'), (b'F', b'Femenino')])),
                ('Dni', models.CharField(max_length=20, null=True, verbose_name=b'DNI', blank=True)),
                ('Direccion', models.CharField(max_length=100, null=True, verbose_name=b'Direcci\xc3\xb3n', blank=True)),
                ('Departamento', models.CharField(max_length=50, null=True, verbose_name=b'Departamento', blank=True)),
                ('Provincia', models.CharField(max_length=50, null=True, verbose_name=b'Provincia', blank=True)),
                ('Distrito', models.CharField(max_length=50, null=True, verbose_name=b'Distrito', blank=True)),
                ('EstadoCivil', models.CharField(default=b'Soltero', max_length=20, verbose_name=b'Estado Civil', choices=[(b'Soltero', b'Soltero'), (b'Casado', b'Casado'), (b'Viudo', b'Viudo'), (b'Divorciado', b'Divorciado')])),
                ('Email2', models.EmailField(max_length=254, null=True, verbose_name=b'E-mail 2', blank=True)),
                ('Telefono', models.CharField(max_length=20, null=True, verbose_name=b'Tel\xc3\xa9fono', blank=True)),
                ('Celular', models.CharField(max_length=20, null=True, verbose_name=b'Celular', blank=True)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activado')),
                ('Foto', models.ImageField(upload_to=Persona.models.obtenerpath_foto_persona, null=True, verbose_name=b'Foto', blank=True)),
                ('Huella', models.BinaryField(null=True)),
            ],
            options={
                'ordering': ['ApellidoPaterno', 'ApellidoMaterno', 'Nombres'],
                'verbose_name': 'Personas UDL',
                'verbose_name_plural': 'Personas UDL',
            },
        ),
    ]
