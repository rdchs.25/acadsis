# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Persona', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Personatipo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'Tipo persona',
                'verbose_name_plural': 'Tipos por persona',
            },
        ),
        migrations.CreateModel(
            name='Tipo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Descripcion', models.CharField(max_length=120, verbose_name=b'Descripci\xc3\xb3n')),
            ],
            options={
                'verbose_name': 'Tipo de persona',
                'verbose_name_plural': 'Tipos de persona',
            },
        ),
        migrations.AlterField(
            model_name='persona',
            name='Dni',
            field=models.CharField(max_length=20, unique=True, null=True, verbose_name=b'DNI', blank=True),
        ),
        migrations.AddField(
            model_name='personatipo',
            name='Persona',
            field=models.ForeignKey(verbose_name=b'Persona', to='Persona.Persona'),
        ),
        migrations.AddField(
            model_name='personatipo',
            name='Tipo',
            field=models.ForeignKey(verbose_name=b'Tipo', to='Persona.Tipo'),
        ),
        migrations.AlterUniqueTogether(
            name='personatipo',
            unique_together=set([('Persona', 'Tipo')]),
        ),
    ]
