# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Persona', '0003_auto_20181129_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='persona',
            name='Esadministrativo',
            field=models.BooleanField(default=False, verbose_name=b'\xc2\xbfEs administrativo?'),
        ),
        migrations.AddField(
            model_name='persona',
            name='Esdocente',
            field=models.BooleanField(default=False, verbose_name=b'\xc2\xbfEs docente?'),
        ),
    ]
