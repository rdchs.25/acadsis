# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Persona', '0004_auto_20181129_1252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='Dni',
            field=models.CharField(max_length=8, unique=True, null=True, verbose_name=b'DNI', blank=True),
        ),
    ]
