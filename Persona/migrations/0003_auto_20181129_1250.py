# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Persona', '0002_auto_20181129_1233'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='personatipo',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='personatipo',
            name='Persona',
        ),
        migrations.RemoveField(
            model_name='personatipo',
            name='Tipo',
        ),
        migrations.DeleteModel(
            name='Personatipo',
        ),
        migrations.DeleteModel(
            name='Tipo',
        ),
    ]
