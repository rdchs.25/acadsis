# -*- coding: utf-8 -*-
from django.contrib import admin

from Persona.models import Persona


class PersonaAdmin(admin.ModelAdmin):
    list_display = ('ApellidoPaterno', 'ApellidoMaterno', 'Nombres', 'Dni', 'Estado')
    search_fields = ('ApellidoPaterno', 'ApellidoMaterno', 'Nombres', 'Dni',)
    fieldsets = (
        ('Datos personales', {
            'fields': (('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'), ('Dni', 'Email', 'Sexo',),),
        }),
        ('Datos laborales', {
            'fields': (('Esdocente', 'Esadministrativo'),),
        }),
    )


admin.site.register(Persona, PersonaAdmin)
