# -*- coding: utf-8 -*-
from django.contrib import admin

from Mensajes.models import Mensaje


class MensajeAdmin(admin.ModelAdmin):
    search_fields = ('Emisor', 'Asunto', 'Mensaje')
    list_filter = ('FechaCreacion',)
    raw_id_fields = ('Emisor',)
    list_display = ('Emisor', 'Asunto', 'Contenido', 'FechaCreacion', 'ver_destinatarios')

    def queryset(self, request):
        """
        Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view.
        """
        qs = self.model._default_manager.get_query_set()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or ()  # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)

        if request.user.is_superuser:
            return qs
        return qs.filter(Emisor=request.user)


admin.site.register(Mensaje, MensajeAdmin)
