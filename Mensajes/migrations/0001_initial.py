# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import Mensajes.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Destinatarios',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Estado')),
                ('FechaCreacion', models.DateTimeField(auto_now_add=True)),
                ('FechaEdicion', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Mensaje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Asunto', models.CharField(max_length=100, verbose_name=b'Asunto')),
                ('Contenido', models.TextField(help_text=b'Redacta el mensaje', verbose_name=b'Mensaje')),
                ('FechaCreacion', models.DateTimeField(auto_now_add=True)),
                ('Adjunto', models.FileField(upload_to=Mensajes.models.obtenerpath_mensaje, null=True, verbose_name=b'Adjunto', blank=True)),
                ('Emisor', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='destinatarios',
            name='Mensaje',
            field=models.ForeignKey(to='Mensajes.Mensaje'),
        ),
        migrations.AddField(
            model_name='destinatarios',
            name='Receptor',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
