# -*- coding: utf-8 -*-
from django.conf.urls import url

from Mensajes.views import nuevo_mensaje, ver_destinatarios

app_name = 'Mensajes'

urlpatterns = [
    url(r'^Mensajes/mensaje/nuevo/$', nuevo_mensaje),
    url(r'^Mensajes/mensaje/ver_destinatarios/(\d+)/$', ver_destinatarios),
]
