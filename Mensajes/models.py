# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models


# Create your models here.

def obtenerpath_mensaje(instance, filename):
    ext = filename.split('.')[-1]
    fecha_creacion = instance.FechaCreacion.strftime("%d_%m_%Y_%H_%M_%S")
    return u'adjuntos_mensajes/%s/%s.%s' % (instance.Emisor, fecha_creacion, ext)


class Mensaje(models.Model):
    Emisor = models.ForeignKey(User)
    Asunto = models.CharField("Asunto", max_length=100)
    Contenido = models.TextField(verbose_name='Mensaje', help_text='Redacta el mensaje')
    FechaCreacion = models.DateTimeField(auto_now_add=True)
    Adjunto = models.FileField(upload_to=obtenerpath_mensaje, blank=True, null=True, verbose_name="Adjunto")

    def __unicode__(self):
        return u'%s - %s' % (self.Emisor.username, self.Asunto)

    def ver_destinatarios(self):
        return '<a href="ver_destinatarios/%s/">Ver</a>' % self.id

    ver_destinatarios.allow_tags = True
    ver_destinatarios.short_description = 'Destinatarios'

    def save(self):
        return super(Mensaje, self).save(using='default')
        return super(Mensaje, self).save()


class Destinatarios(models.Model):
    Mensaje = models.ForeignKey(Mensaje)
    Receptor = models.ForeignKey(User)
    Estado = models.BooleanField("Estado", default=True)
    FechaCreacion = models.DateTimeField(auto_now_add=True)
    FechaEdicion = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.Receptor

    def save(self):
        return super(Destinatarios, self).save(using='default')
        return super(Destinatarios, self).save()
