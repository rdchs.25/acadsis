# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from Alumnos.models import Alumno
from Carreras.models import Carrera
from Cursos.models import PeriodoCurso
from Matriculas.models import MatriculaCiclo
from Mensajes.models import Mensaje, Destinatarios


class MensajeForm(ModelForm):
    Adjunto = forms.FileField(required=False)

    class Meta:
        model = Mensaje
        exclude = ['Emisor']


def nuevo_mensaje(request):
    if request.user.is_authenticated() and request.user.has_perm('Mensajes.add_mensaje'):
        if request.method == "POST":
            form = MensajeForm(request.POST, request.FILES)
            destinatarios = request.POST.getlist('firstSelectms2side__dx')

            if form.is_valid():
                asunto = form.cleaned_data['Asunto']
                contenido = form.cleaned_data['Contenido']
                adjunto = form.cleaned_data['Adjunto']
                nuevo_mensaje = Mensaje(Emisor=request.user, Asunto=asunto, Contenido=contenido, Adjunto=adjunto)
                nuevo_mensaje.save()
                for d in destinatarios:
                    nuevo_destinatario = Destinatarios(Mensaje=nuevo_mensaje, Receptor_id=d)
                    nuevo_destinatario.save()
                return HttpResponseRedirect('../')
        else:
            form = MensajeForm()

        carreras = Carrera.objects.filter(Estado=True)
        docentes = PeriodoCurso.objects.filter(Periodo__id=1).order_by('Docente__ApellidoPaterno',
                                                                       'Docente__ApellidoMaterno').values('Docente__id',
                                                                                                          'Docente__ApellidoPaterno',
                                                                                                          'Docente__ApellidoMaterno',
                                                                                                          'Docente__Nombres').distinct()
        matriculados = MatriculaCiclo.objects.filter(Periodo__id=25, Estado='Matriculado').order_by(
            'Alumno__ApellidoPaterno', 'Alumno__ApellidoMaterno', 'Alumno__Nombres')
        ctx = {"matriculados": matriculados, "carreras": carreras, "docentes": docentes, "form": form,
               "user": request.user}
        return render_to_response('Mensajes/nuevo_mensaje.html', ctx, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def ver_destinatarios(request, mensaje_id):
    if request.user.is_authenticated():
        try:
            mensaje = Mensaje.objects.get(id=mensaje_id)
            destinatarios = Destinatarios.objects.filter(Mensaje=mensaje)
            alumnos = []
            for d in destinatarios:
                alumno = Alumno.objects.get(user_ptr=d.Receptor.id)
                alumnos.append(alumno)
            ctx = {"mensaje": mensaje, "alumnos": alumnos, "user": request.user}
            return render_to_response('Mensajes/ver_destinatarios.html', ctx, context_instance=RequestContext(request))
        except:
            return HttpResponseRedirect('../../../../')
    else:
        return HttpResponseRedirect('../../../../')
