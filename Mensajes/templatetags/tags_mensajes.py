import datetime

from django.template import Library

from Carreras.models import Carrera
from Matriculas.models import MatriculaCiclo
from Mensajes.models import Destinatarios
from Periodos.models import Periodo

register = Library()


def alumnos_carrera(context, carrera_id=None):
    periodo = Periodo.objects.get(id=10)
    carrera = Carrera.objects.get(id=carrera_id)
    matriculados = MatriculaCiclo.objects.filter(Periodo__id=periodo.id, Estado='Matriculado',
                                                 Alumno__Carrera=carrera).order_by('Alumno__ApellidoPaterno',
                                                                                   'Alumno__ApellidoMaterno',
                                                                                   'Alumno__Nombres')
    return {
        'matriculados': matriculados,
        'carrera': carrera,
    }


register.inclusion_tag('Mensajes/opt_alumnos_carrera.html', takes_context=True)(alumnos_carrera)


def n_mensajes_hoy(context, user=None):
    fecha = datetime.date.today() - datetime.timedelta(days=1)
    n_mensajes_hoy = Destinatarios.objects.filter(Receptor=user, FechaCreacion__gte=fecha).order_by(
        '-FechaCreacion').count()
    return {
        'n_mensajes_hoy': n_mensajes_hoy,
    }


register.inclusion_tag('Mensajes/n_mensajes_hoy.html', takes_context=True)(n_mensajes_hoy)


def render_paginator(context, first_last_amount=2, before_after_amount=4):
    query = context['query']
    n_mensajes = context['n_mensajes']
    page_obj = context['results']
    paginator = context['paginator']
    page_numbers = []

    # Pages before current page
    if page_obj.number > first_last_amount + before_after_amount:
        for i in range(1, first_last_amount + 1):
            page_numbers.append(i)

        page_numbers.append(None)

        for i in range(page_obj.number - before_after_amount, page_obj.number):
            page_numbers.append(i)

    else:
        for i in range(1, page_obj.number):
            page_numbers.append(i)

    # Current page and pages after current page
    if page_obj.number + first_last_amount + before_after_amount < paginator.num_pages:
        for i in range(page_obj.number, page_obj.number + before_after_amount + 1):
            page_numbers.append(i)

        page_numbers.append(None)

        for i in range(paginator.num_pages - first_last_amount + 1, paginator.num_pages + 1):
            page_numbers.append(i)

    else:
        for i in range(page_obj.number, paginator.num_pages + 1):
            page_numbers.append(i)

    return {
        'paginator': paginator,
        'page_obj': page_obj,
        'page_numbers': page_numbers,
        'query': query,
        'n_mensajes': n_mensajes
    }


register.inclusion_tag('Mensajes/pagination.html', takes_context=True)(render_paginator)
