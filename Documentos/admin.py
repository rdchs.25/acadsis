# -*- coding: utf-8 -*-
from django.contrib import admin

from Documentos.models import Documento


class DocumentoAdmin(admin.ModelAdmin):
    list_display = ('Documento', 'Modalidad')
    search_fields = ('Documento',)

    fieldsets = (
        ('Datos del Documento', {
            'fields': ('Codigo', 'Documento', 'Modalidad', 'Estado'),
        }),
    )


admin.site.register(Documento, DocumentoAdmin)
