# -*- coding: utf-8 -*-
from django.db import models


class Documento(models.Model):
    MODALIDAD_CHOICES = (
        ('Examen Ordinario', 'Examen Ordinario'),
        ('Graduados Titulados', 'Graduados Titulados'),
        ('Traslado Externo', 'Traslado Externo'),
        ('Primeros Puestos', 'Primeros Puestos'),
        ('Deportistas Calificados', 'Deportistas Calificados'),
        ('Personas Discapacitadas', 'Personas Discapacitadas'),
        ('Escuela Pre UDL', 'Escuela Pre UDL'),
    )
    Codigo = models.CharField("Código", max_length=5, unique=True)
    Documento = models.CharField(max_length=50, unique=True)
    Estado = models.BooleanField("Activado", default=True)
    Modalidad = models.CharField("Modalidad", max_length=50, choices=MODALIDAD_CHOICES, null=True, blank=True)

    def __unicode__(self):
        return self.Documento

    def save(self):
        super(Documento, self).save()
        return super(Documento, self).save(using='escuelapre')

    class Meta:
        verbose_name = "Documentos a Pedir"
        verbose_name_plural = "Documentos a Pedir"
