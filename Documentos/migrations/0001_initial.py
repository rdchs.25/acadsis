# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Documento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(unique=True, max_length=5, verbose_name=b'C\xc3\xb3digo')),
                ('Documento', models.CharField(unique=True, max_length=50)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activado')),
                ('Modalidad', models.CharField(blank=True, max_length=50, null=True, verbose_name=b'Modalidad', choices=[(b'Examen Ordinario', b'Examen Ordinario'), (b'Graduados Titulados', b'Graduados Titulados'), (b'Traslado Externo', b'Traslado Externo'), (b'Primeros Puestos', b'Primeros Puestos'), (b'Deportistas Calificados', b'Deportistas Calificados'), (b'Personas Discapacitadas', b'Personas Discapacitadas'), (b'Escuela Pre UDL', b'Escuela Pre UDL')])),
            ],
            options={
                'verbose_name': 'Documentos a Pedir',
                'verbose_name_plural': 'Documentos a Pedir',
            },
        ),
    ]
