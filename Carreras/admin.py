# -*- coding: utf-8 -*-
from django.contrib import admin

from Carreras.models import Carrera, Facultad


class CarreraAdmin(admin.ModelAdmin):
    list_display = ('Carrera', 'Codigo', 'Facultad')
    search_fields = ('Carrera', 'Codigo', 'Facultad')


class FacultadAdmin(admin.ModelAdmin):
    list_display = ('Facultad', 'Codigo')
    search_fields = ('Facultad', 'Codigo')


admin.site.register(Carrera, CarreraAdmin)
admin.site.register(Facultad, FacultadAdmin)
