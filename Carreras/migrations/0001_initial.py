# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Carrera',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(unique=True, max_length=20, verbose_name=b'C\xc3\xb3digo')),
                ('Carrera', models.CharField(max_length=50)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activada')),
            ],
            options={
                'verbose_name': 'Carrera Profesional',
                'verbose_name_plural': 'Carreras Profesionales',
            },
        ),
    ]
