# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Carreras', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Facultad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(unique=True, max_length=20, verbose_name=b'C\xc3\xb3digo')),
                ('Facultad', models.CharField(max_length=200)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activada')),
            ],
            options={
                'verbose_name': 'Facultad',
                'verbose_name_plural': 'Facultades',
            },
        ),
        migrations.AddField(
            model_name='carrera',
            name='Facultad',
            field=models.ForeignKey(default=None, blank=True, to='Carreras.Facultad', null=True),
        ),
    ]
