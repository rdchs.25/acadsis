# -*- coding: utf-8 -*-
from django.db import models


class Facultad(models.Model):
    Codigo = models.CharField("Código", max_length=20, unique=True)
    Facultad = models.CharField(max_length=200)
    Estado = models.BooleanField("Activada", default=True)

    def __unicode__(self):
        return self.Facultad

    class Meta:
        verbose_name = "Facultad"
        verbose_name_plural = "Facultades"


class Carrera(models.Model):
    Codigo = models.CharField("Código", max_length=20, unique=True)
    Carrera = models.CharField(max_length=50)
    Estado = models.BooleanField("Activada", default=True)
    Facultad = models.ForeignKey(Facultad, default=None, null=True, blank=True)

    def __unicode__(self):
        return self.Carrera

    # def save(self):
    #     super(Carrera, self).save()
    #     return super(Carrera, self).save(using='escuelapre')

    class Meta:
        verbose_name = "Carrera Profesional"
        verbose_name_plural = "Carreras Profesionales"
