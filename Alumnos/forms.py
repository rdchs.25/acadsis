# -*- coding: utf-8 -*-
import hashlib

from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.forms.utils import ErrorList

from Alumnos.models import Alumno, RESPONSABLE_CHOICES, Pariente
from Alumnos.widgets import DateTimeWidget
from Docentes.models import MoodleUser
from Ubicacion.models import Departamento


class ArchivoForm(forms.Form):
    Archivo = forms.FileField(help_text='Cargar archivo en formato csv, con los codigos de los estudiantes')

    def clean_Archivo(self):
        archivo = self.cleaned_data['Archivo']
        if archivo.content_type != 'text/csv':
            raise forms.ValidationError("Debe ingresar un archivo csv")
        else:
            for line in archivo:
                line = line.strip('\n')
                try:
                    alumno = Alumno.objects.get(Codigo=line)
                except Alumno.DoesNotExist:
                    raise forms.ValidationError(
                        "El codigo %s, no pertenece a ningun estudiante, verificar por favor" % line)
        return archivo


class LoginFormActualizarDatos(forms.Form):
    Usuario = forms.CharField(label="Usuario", required=True)
    Password = forms.CharField(
        widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")

        if user and passwd:
            try:
                validar_usuario = MoodleUser.objects.using(
                    'moodle').get(username=user)
                usuario_udl = User.objects.get(username=user)

                grupos = usuario_udl.groups.all()
                alumno = False
                for grupo in grupos:
                    if grupo.name == "Alumnos UDL":
                        alumno = True
                        break

                if alumno:
                    try:
                        usuario = MoodleUser.objects.using('moodle').get(
                            username=user, password=hashlib.md5(passwd).hexdigest())
                        usuario_udl.set_password(passwd)
                        usuario_udl.save()
                    except MoodleUser.DoesNotExist:
                        msg = u'Usuario Incorrecto'
                        msg1 = u'Contraseña Incorrecta'
                        self._errors["Usuario"] = ErrorList([msg])
                        self._errors["Password"] = ErrorList([msg1])
                        del cleaned_data["Usuario"]
                else:
                    msg = u'No eres estudiante'
                    self._errors["Usuario"] = ErrorList([msg])
                    del cleaned_data["Usuario"]

            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]

        return cleaned_data


class FormActualizarDatos(forms.Form):
    ApellidoPaterno = forms.CharField(label='Apellido Paterno', required=True)
    ApellidoMaterno = forms.CharField(label='Apellido Materno', required=True)
    Nombres = forms.CharField(label='Nombres', required=True)
    Nacimiento = forms.DateField(widget=DateTimeWidget, required=True, label='Fecha de Nacimiento')
    LugarNacimiento = forms.CharField(label='Lugar de Nacimiento', required=True)
    NroDocumento = forms.CharField(label='DNI', required=True, validators=[
        RegexValidator(regex='^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$', message='DNI incorrecto',
                       code='invalid_username'), ])
    Email = forms.CharField(label='Email', required=True)
    Telefono = forms.CharField(label='Telefono', required=True)
    Celular = forms.CharField(label='Celular', required=True)
    Direccion = forms.CharField(label='Dirección', required=True)
    # Datos del padre
    ApePatPadre = forms.CharField(label='Apellido Paterno', required=True)
    ApeMatPadre = forms.CharField(label='Apellido Materno', required=True)
    NombresPadre = forms.CharField(label='Nombres', required=True)
    DNIPadre = forms.CharField(label='DNI', required=False, validators=[
        RegexValidator(regex='^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$', message='DNI incorrecto',
                       code='invalid_username'), ])
    VivePadre = forms.BooleanField(label='Vive', required=False)
    DirPadre = forms.CharField(label='Dirección', required=False)
    TelfPadre = forms.CharField(label='Telefono', required=False)
    CelPadre = forms.CharField(label='Celular', required=False)
    # Datos de la madre
    ApePatMadre = forms.CharField(label='Apellido Paterno', required=True)
    ApeMatMadre = forms.CharField(label='Apellido Materno', required=True)
    NombresMadre = forms.CharField(label='Nombres', required=True)
    DNIMadre = forms.CharField(label='DNI', required=False)
    ViveMadre = forms.BooleanField(label='Vive', required=False)
    DirMadre = forms.CharField(label='Dirección', required=False)
    TelfMadre = forms.CharField(label='Telefono', required=False)
    CelMadre = forms.CharField(label='Celular', required=False)
    # Datos del apoderado o familiar
    Responsable = forms.ChoiceField(label='Responsable', required=True, choices=RESPONSABLE_CHOICES)
    Parentesco = forms.CharField(label='Parentesco', required=True)
    ApePatTutor = forms.CharField(label='Apellido Paterno', required=True)
    ApeMatTutor = forms.CharField(label='Apellido Materno', required=True)
    NombresTutor = forms.CharField(label='Nombres', required=True)
    DNITutor = forms.CharField(label='DNI', required=False, validators=[RegexValidator(
        regex='^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$', message='DNI incorrecto', code='invalid_username'), ])
    DirTutor = forms.CharField(label='Dirección', required=False)
    TelfTutor = forms.CharField(label='Teléfono', required=False)
    CelTutor = forms.CharField(label='Celular', required=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data


class ParienteAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ParienteAdminForm, self).__init__(*args, **kwargs)
        dep = ''
        prov = ''
        dist = ''
        if self.instance.Departamento:
            dep = self.instance.Departamento
        if self.instance.Provincia:
            prov = self.instance.Provincia
        if self.instance.Distrito:
            dist = self.instance.Distrito
        self.fields['selected_lugar'].initial = '%s|%s|%s' % (dep, prov, dist)

    Nacimiento = forms.DateField(widget=DateTimeWidget, required=False)
    Departamento = forms.CharField(widget=forms.Select, required=False)
    Provincia = forms.CharField(widget=forms.Select, required=False)
    Distrito = forms.CharField(widget=forms.Select, required=False)
    selected_lugar = forms.CharField(widget=forms.HiddenInput, required=False, label="")

    class Meta:
        model = Pariente
        fields = '__all__'

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Parientes/frmpariente.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class AlumnoAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AlumnoAdminForm, self).__init__(*args, **kwargs)
        try:
            # Try and set the selected_cat field from instance if it exists
            if self.instance.Distrito is None:
                pass
                # self.fields['selected_dep'].initial = self.instance.Provincia.Departamento.id
                # self.fields['selected_prov'].initial = self.instance.Provincia.id
            else:
                self.fields['selected_dep'].initial = self.instance.Distrito.Provincia.Departamento.id
                self.fields['selected_prov'].initial = self.instance.Distrito.Provincia.id
                self.fields['selected_dist'].initial = self.instance.Distrito.id
        except ValueError:
            pass

    Nacimiento = forms.DateField(widget=DateTimeWidget)
    Codigo = forms.CharField(min_length=7, max_length=7, required=True, label="Código")
    Expediente = forms.CharField(min_length=7, max_length=7, required=True, label="Expediente")
    Departamento = forms.ModelChoiceField(queryset=Departamento.objects.all().order_by('Departamento'),
                                          widget=forms.Select(), required=False)
    selected_dep = forms.CharField(widget=forms.HiddenInput, required=False, label="")
    selected_prov = forms.CharField(widget=forms.HiddenInput, required=False, label="")
    selected_dist = forms.CharField(widget=forms.HiddenInput, required=False, label="")

    class Meta:
        model = Alumno
        fields = '__all__'

    def clean_emailinstitucional(self):
        return self.cleaned_data['EmailInstitucional'] or None

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Alumnos/frmalumno.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class LoginForm(forms.Form):
    Usuario = forms.CharField(label="Usuario", required=True)
    Password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")

        if user and passwd:
            try:
                usuario_udl = Pariente.objects.get(Email=user)
                usuario_udl.save()
            except ValueError:
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]
        return cleaned_data


class ChangePasswordForm(forms.Form):
    OldPassword = forms.CharField(widget=forms.PasswordInput, label="Contraseña Antigua", required=True)
    NewPassword = forms.CharField(widget=forms.PasswordInput, label="Nueva Contraseña", required=True)
    RepeatPassword = forms.CharField(widget=forms.PasswordInput, label="Repetir Contraseña", required=True)
    Usuario = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    def clean(self):
        cleaned_data = self.cleaned_data
        oldpasswd = cleaned_data.get("OldPassword")
        newpasswd = cleaned_data.get("NewPassword")
        repeatpasswd = cleaned_data.get("RepeatPassword")
        usuario = cleaned_data.get("Usuario")

        if oldpasswd and newpasswd and repeatpasswd and usuario:
            try:
                usuario_udl = User.objects.get(username=usuario)
                if usuario_udl.check_password(oldpasswd) is True:
                    if newpasswd == repeatpasswd:
                        usuario_udl.set_password(newpasswd)
                        usuario_udl.save()
                    else:
                        msg = u'Las contraseñas no coinciden'
                        self._errors["RepeatPassword"] = ErrorList([msg])
                        del cleaned_data["RepeatPassword"]
                else:
                    msg = u'Contraseña Incorrecta'
                    self._errors["OldPassword"] = ErrorList([msg])
                    del cleaned_data["OldPassword"]
            except User.DoesNotExist:
                msg = u'Usuario Incorrecto, intento de acceso no autorizado'
                self._errors["OldPassword"] = ErrorList([msg])
                del cleaned_data["OldPassword"]
        return cleaned_data


class PadreForm(forms.ModelForm):
    Departamento = forms.CharField(widget=forms.Select)
    Provincia = forms.CharField(widget=forms.Select)
    Distrito = forms.CharField(widget=forms.Select)

    class Meta:
        model = Pariente
        exclude = ('username', 'password', 'last_login', 'date_joined', 'Email', 'Estado', 'Parentesco', 'Alumno')
