# -*- coding: utf-8 -*-
from django.conf.urls import url

from Alumnos.read_only import ver_alumno
from Alumnos.views import mostrarfoto, generar_carnet

app_name = 'Alumnos'

urlpatterns = [
    url(r'^Alumnos/alumno/foto/(\d+)/$', mostrarfoto),
    url(r'^Alumnos/alumno/generar_carnet/$', generar_carnet),
    url(r'^Alumnos/alumno/ver/$', ver_alumno),
]
