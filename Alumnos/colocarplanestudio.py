#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

# imports para traer variable de entorno django
from Alumnos.models import Alumno
from Planestudio.models import Plan
from Cursos.models import Curso


def registrarplanes():
    alumnos = Alumno.objects.filter(Carrera__Estado=True, Plan=None)
    for alumno in alumnos:
        anio = alumno.AnioIngreso
        if int(anio) >= 2015:
            plan = Plan.objects.get(Carrera__id=alumno.Carrera_id, Nombre__icontains='2015')
            alumno.Plan_id = plan.id
            alumno.save()
        else:
            plan = Plan.objects.get(Carrera__id=alumno.Carrera_id, Nombre__icontains='2010')
            alumno.Plan_id = plan.id
            alumno.save()

    # cursos = Curso.objects.filter(Plan=None)
    # for curso in cursos:
    #     ciclo = curso.Ciclo
    #     periodo = curso.Periodo
    #     anio = periodo.Anio
    #     semestre = periodo.Semestre
    #     if int(anio) <= 2014:
    #         plan = Plan.objects.get(Carrera__id=curso.Carrera_id, Nombre__icontains='2010')
    #         curso.Plan_id = plan.id
    #         curso.save()
    #     else:
    #         if int(anio) <= 2015 and semestre == 'I':
    #             if (ciclo == '01' or ciclo == '02' or ciclo == '03' or ciclo == '04' or ciclo == '05'
    #                     or ciclo == '06' or ciclo == '07' or ciclo == '08'):
    #                 plan = Plan.objects.get(Carrera__id=curso.Carrera_id, Nombre__icontains='2015')
    #                 curso.Plan_id = plan.id
    #                 curso.save()
    #             else:
    #                 plan = Plan.objects.get(Carrera__id=curso.Carrera_id, Nombre__icontains='2010')
    #                 curso.Plan_id = plan.id
    #                 curso.save()


registrarplanes()
