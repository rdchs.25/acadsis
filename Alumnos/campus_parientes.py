# -*- coding: utf-8 -*-
from decimal import Decimal

import simplejson
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Alumnos.forms import LoginForm, ChangePasswordForm, PadreForm
from Alumnos.models import Alumno
from Alumnos.models import Pariente
from Asistencias.models import Asistencia
from Cursos.models import PeriodoCurso
from Horarios.models import Horario
from Matriculas.models import MatriculaCursos, MatriculaCiclo
from Mensajes.models import Destinatarios
from Notas.models import NotasAlumno, Nota
from Pagos.models import PagoAlumno, DetallePago
from Periodos.models import Periodo


def login_campus_padres(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['Usuario']
            passwd = form.cleaned_data['Password']
            user = authenticate(username=usuario, password=passwd)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('../profile/')
    else:
        form = LoginForm()
    return render_to_response("Alumnos/CampusParientes/login.html", {"form": form},
                              context_instance=RequestContext(request))


ID_PERIODO = 10


def profile_padres(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')
        periodo = Periodo.objects.get(id=ID_PERIODO)

        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
                                                MatriculaCiclo__Alumno__user_ptr=request.user.id)
        curs = Pariente.objects.get(Email=request.user)
        alumno = curs.Alumno.all().order_by('ApellidoPaterno')
        saludos = {'1': 'Que tal', '2': 'Bienvenido(a)', '3': 'Como le va', '4': 'saludo'}
        import random
        numero = random.choice('1234')
        if int(numero) == 4:
            import datetime
            now = datetime.datetime.now()
            doce = now.replace(hour=12, minute=0, second=0, microsecond=0)
            siete = now.replace(hour=19, minute=0, second=0, microsecond=0)
            media_noche = now.replace(hour=00, minute=0, second=0, microsecond=0)
            if doce > now > media_noche:
                saludo = 'Buenos días'
            elif siete > now > doce:
                saludo = 'Buenas tardes'
            elif siete < now < media_noche:
                saludo = 'Buenas noches'
            else:
                saludo = 'Buenas noches'
        else:
            saludo = saludos[numero]

        return render_to_response("Alumnos/CampusParientes/profile_padre.html",
                                  {"saludo": saludo, "results": cursos, "periodo": periodo, "var": var,
                                   "user": request.user, "alumno": alumno}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../')


def hijos_disponibles(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        cursos = Pariente.objects.get(Email=request.user)
        alumnos = cursos.Alumno.all()
        n_alumnos = alumnos.count()
        ctx = {'cursos': cursos, 'alumnos': alumnos, 'n_alumnos': n_alumnos, "user": request.user, "var": var, }
        return render_to_response("Alumnos/CampusParientes/hijos_disponibles.html", ctx,
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def profile_hijos(request, id_alumno):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')
        periodo = Periodo.objects.get(id=ID_PERIODO)

        try:
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
                                                MatriculaCiclo__Alumno__user_ptr=id_alumno)

        saludos = {'1': 'Que tal', '2': 'Bienvenido(a)', '3': 'Como le va', '4': 'saludo'}
        import random
        numero = random.choice('1234')
        if int(numero) == 4:
            import datetime
            now = datetime.datetime.now()
            doce = now.replace(hour=12, minute=0, second=0, microsecond=0)
            siete = now.replace(hour=19, minute=0, second=0, microsecond=0)
            media_noche = now.replace(hour=00, minute=0, second=0, microsecond=0)
            if doce > now > media_noche:
                saludo = 'Buenos dias'
            elif siete > now > doce:
                saludo = 'Buenas tardes'
            elif siete < now < media_noche:
                saludo = 'Buenas noches'
            else:
                saludo = 'Buenas noches'
        else:
            saludo = saludos[numero]

        return render_to_response("Alumnos/CampusParientes/profile_estudiante.html",
                                  {"saludo": saludo, "results": cursos, "estudiante": estudiante, "periodo": periodo,
                                   "var": var}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../')


def notas_curso(request, id_alumno, id_periodocurso):
    if request.user.is_authenticated():

        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'g')

        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Alumnos/CampusParientes/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        registros = []
        notas1 = Nota.objects.filter(PeriodoCurso=periodocurso, Nivel='1').order_by('Orden')

        nots1 = []
        nots2 = []
        nots3 = []

        mat = MatriculaCursos.objects.get(PeriodoCurso=periodocurso, MatriculaCiclo__Alumno__id=id_alumno)
        notas_creadas = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota__PeriodoCurso=periodocurso)

        if notas_creadas.count() != 0:
            cals = True
        else:
            cals = False

        if cals is True:

            for nota1 in notas1:
                nots_2 = Nota.objects.filter(PeriodoCurso=periodocurso, NotaPadre=nota1, Nivel='2').order_by('Orden')
                for nota2 in nots_2:
                    nots_3 = Nota.objects.filter(PeriodoCurso=periodocurso, NotaPadre=nota2, Nivel='3').order_by(
                        'Orden')
                    for nota3 in nots_3:
                        valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                        if valor3.count() != 0:
                            valor3 = valor3[0].Valor
                            nots3.append([nota3, int(str(valor3).split('.')[0])])
                        else:
                            valor3 = mat.PromedioParcial(nota3)
                            nots3.append([nota3, valor3])
                    valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                    if valor2.count() != 0:
                        valor2 = valor2[0].Valor
                        nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                    else:
                        valor2 = mat.PromedioParcial(nota2)
                        nots2.append([nota2, nots3, valor2])
                    nots3 = []
                valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
                if valor1.count() != 0:
                    valor1 = valor1[0].Valor
                    nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
                else:
                    valor1 = mat.PromedioParcial(nota1)
                    nots1.append([nota1, nots2, valor1])
                nots2 = []
            registros.append([mat, nots1])
            nots1 = []
        # cals = False
        ident = []
        formulas = []
        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')

        for nota in notas:
            ident.append(nota)

        for nota in notas1:
            subnotas = Nota.objects.filter(NotaPadre=nota)
            k = 1
            formula = ''
            suma = Decimal('0.00')
            for subnota in subnotas:
                if k == 1:
                    formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
                else:
                    formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
                k += 1
                suma += Decimal(nota.Peso)
            formula += ')/%s' % str(suma)
            formulas.append(formula)

        return render_to_response("Alumnos/CampusParientes/notas_curso.html",
                                  {"var": var, "var1": var1, "cals": cals, "periodocurso": periodocurso, "notas": ident,
                                   "formulas": formulas, "registros": registros, "user": request.user,
                                   "estudiante": estudiante, "periodo": periodo},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def asistencias_curso(request, id_alumno, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'a')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        try:
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
            curso = PeriodoCurso.objects.get(id=id_periodocurso)
            horarios = Horario.objects.filter(PeriodoCurso=id_periodocurso)
            asistencias = Asistencia.objects.filter(MatriculaCursos__MatriculaCiclo__Alumno__id=id_alumno,
                                                    MatriculaCursos__PeriodoCurso=curso).order_by('-Fecha')
            asistio = 0
            falto = 0
            tardanza = 0
            for asi in asistencias:
                if asi.Estado == 'Asistio':
                    asistio += 1
                elif asi.Estado == 'Falto':
                    falto += 1
                elif asi.Estado == 'Tardanza':
                    tardanza += 1
            if asistencias.count() != 0:
                matriculado = asistencias[0].MatriculaCursos
                asiste = (asistio * 100) / asistencias.count()
                falta = (falto * 100) / asistencias.count()
                tarde = (tardanza * 100) / asistencias.count()
            else:
                matriculado = None
                asiste = 0
                falta = 0
                tarde = 0
            return render_to_response("Alumnos/CampusParientes/asistencias_curso.html",
                                      {"var": var, "var1": var1, "periodocurso": curso, "asistencias": asistencias,
                                       "curso": curso, "asistio": asistio, "falto": falto, "tardanza": tardanza,
                                       "asiste": asiste, "falta": falta, "tarde": tarde, "matriculado": matriculado,
                                       "horarios": horarios, "user": request.user, "estudiante": estudiante,
                                       "periodo": periodo})
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Alumnos/CampusParientes/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    else:
        return HttpResponseRedirect('../../../')


def cuenta_corriente(request, id_alumno):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'f')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        try:
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
            matricula = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except (Alumno.DoesNotExist, MatriculaCiclo.DoesNotExist):
            matricula = []
        return render_to_response("Alumnos/CampusParientes/cuenta_corriente.html",
                                  {"var": var, "matricula": matricula, "estudiante": estudiante, "periodo": periodo},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def ver_pagos(request, id_alumno):
    if request.user.is_authenticated():
        anio = request.GET.get('p', '')
        semestre = request.GET.get('s', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__Anio=anio,
                                          MatriculaCiclo__Periodo__Semestre=semestre,
                                          MatriculaCiclo__Alumno__id=id_alumno, Estado=True)

        n_pagos = pagos.count()
        paginator = Paginator(pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.ConceptoPago.PagoBanco is True:
                lugar_pago = "Banco"
            else:
                lugar_pago = "Caja"
            fila = {"id": r.id,
                    "cell": [i, str(r.ConceptoPago), str(r.Monto()), 'S./ %s' % r.DeudaMonto(), str(r.Mora()),
                             'S./ %s' % r.DeudaMora(), str(r.ConceptoPago.FechaVencimiento), lugar_pago]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ver_detalle_pagos(request, id_alumno):
    if request.user.is_authenticated():
        id_pago = request.GET.get('id', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        detalle_pagos = DetallePago.objects.filter(PagoAlumno__id=id_pago).order_by('-FechaPago')

        n_detalle_pagos = detalle_pagos.count()
        paginator = Paginator(detalle_pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.Mora is True:
                mora = "Si"
            else:
                mora = "No"
            fila = {"id": r.id,
                    "cell": [i, str(r.Comprobante), 'S./ %s' % str(r.MontoDeuda), 'S./ %s' % r.MontoCancelado,
                             'S./ %s' % r.PagoRestante, str(r.FechaPago), mora]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_detalle_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


def ver_deudas(request, id_alumno):
    if request.user.is_authenticated():
        anio = request.GET.get('p', '')
        semestre = request.GET.get('s', '')
        page = request.GET.get('page', '')
        limit = request.GET.get('rows', '')
        sidx = request.GET.get('sidx', '')
        sord = request.GET.get('sord', '')

        if sord == 'asc':
            sord = '-'
        elif sord == 'desc':
            sord = ''

        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Alumno__id=id_alumno, Estado=False)

        n_pagos = pagos.count()
        paginator = Paginator(pagos, int(limit))
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = request.GET.get('page', '1')
        except ValueError:
            page = 1
            # If page request (9999) is out of range, deliver last page of results.
        try:
            resultados = paginator.page(page)
        except (EmptyPage, InvalidPage):
            resultados = paginator.page(paginator.num_pages)

        filas = []
        i = 1
        for r in resultados.object_list:
            if r.ConceptoPago.PagoBanco is True:
                lugar_pago = "Banco"
            else:
                lugar_pago = "Caja"
            fila = {"id": r.id,
                    "cell": [i, str(r.ConceptoPago), str(r.Monto()), 'S./ %s' % str(r.DeudaMonto()), str(r.Mora()),
                             'S./ %s' % r.DeudaMora(), str(r.ConceptoPago.FechaVencimiento), lugar_pago]}
            filas.append(fila)
            i += 1
        results = {"page": page, "total": paginator.num_pages, "records": n_pagos, "rows": filas}
        return HttpResponse(simplejson.dumps(results, indent=4), content_type='application/json')
    else:
        return render_to_response("Campus/mensaje.html")


@csrf_protect
def logout_padres(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('/padres/login/')
    else:
        return HttpResponseRedirect('/padres/login/')


@csrf_protect
def change_password(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', '')
        curs = Pariente.objects.get(Email=request.user)
        alumno = curs.Alumno.all()
        if request.method == 'POST':
            form = ChangePasswordForm(request.POST)
            if form.is_valid():
                return render_to_response("Alumnos/CampusParientes/change_passwd_success.html")
        else:
            form = ChangePasswordForm(initial={'Usuario': request.user.username})

        return render_to_response("Alumnos/CampusParientes/change_password.html",
                                  {"form": form, "var": var, "user": request.user, "alumno": alumno},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/padres/login/')


def datos_personales_padre(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'd')
        periodo = Periodo.objects.get(id=ID_PERIODO)
        curs = Pariente.objects.get(Email=request.user)
        alumno = curs.Alumno.all()
        try:
            padre = Pariente.objects.get(Email=request.user)
            return render_to_response("Alumnos/CampusParientes/datos_personales_padre.html",
                                      {"var": var, "padre": padre, "periodo": periodo, "user": request.user,
                                       "alumno": alumno}, context_instance=RequestContext(request))
        except:
            return HttpResponseRedirect('../../login/')
    else:
        return HttpResponseRedirect('../../login/')


def editar_datos(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'd')
        verifica_post = False
        periodo = Periodo.objects.get(id=ID_PERIODO)
        curs = Pariente.objects.get(Email=request.user)
        alumno = curs.Alumno.all()
        try:
            padre = Pariente.objects.get(Email=request.user)
        except:
            return HttpResponseRedirect('../../../login/')

        if request.method == "POST":
            verifica_post = True
            form = PadreForm(request.POST, request.FILES)
            if form.is_valid():
                padre.ApellidoPaterno = form.cleaned_data['ApellidoPaterno']
                padre.ApellidoMaterno = form.cleaned_data['ApellidoMaterno']
                padre.Nombres = form.cleaned_data['Nombres']
                padre.Nacimiento = form.cleaned_data['Nacimiento']
                padre.Sexo = form.cleaned_data['Sexo']
                padre.Dni = form.cleaned_data['Dni']
                padre.EstadoCivil = form.cleaned_data['EstadoCivil']
                padre.Direccion = form.cleaned_data['Direccion']
                padre.Departamento = form.cleaned_data['Departamento']
                padre.Provincia = form.cleaned_data['Provincia']
                padre.Distrito = form.cleaned_data['Distrito']
                padre.Telefono = form.cleaned_data['Telefono']
                padre.Celular = form.cleaned_data['Celular']
                padre.save()
                return HttpResponseRedirect('../?q=d')
        else:
            form = PadreForm(initial={"EstadoCivil": padre.EstadoCivil, "Nacimiento": padre.Nacimiento})

        return render_to_response("Alumnos/CampusParientes/editar_datos.html",
                                  {"var": var, "verifica_post": verifica_post, "form": form, "padre": padre,
                                   "periodo": periodo, "user": request.user, "alumno": alumno},
                                  context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('../../../login/')


def mensajes_recibidos_padres(request, id_alumno):
    if request.user.is_authenticated():
        var = request.GET.get('me', 'me')
        query = request.GET.get('query', '')
        mensajes_recibidos = []

        import datetime
        now = datetime.datetime.today()
        dia = now.strftime('%d')

        if dia == '04' or dia == '05' or dia == '10' or dia == '11' or dia == '18' or dia == '19' or dia == '24' or \
                dia == '25' or dia == '29' or dia == '30':
            msg = True
        else:
            msg = False

        periodo = Periodo.objects.get(id=ID_PERIODO)
        estudiante = None
        try:
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
            matricula = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except (Alumno.DoesNotExist, MatriculaCiclo.DoesNotExist):
            matricula = []

        if matricula != []:
            deudas = PagoAlumno.objects.filter(MatriculaCiclo=matricula, Estado=False)
            nro_deudas = deudas.count()
        else:
            deudas = []
            nro_deudas = 0

        if query:
            qset = (
                    Q(Mensaje__Asunto__icontains=query) |
                    Q(Mensaje__Contenido__icontains=query)
            )
            recibidos = Destinatarios.objects.filter(Receptor=id_alumno).filter(qset).order_by('-FechaCreacion')
        else:
            recibidos = Destinatarios.objects.filter(Receptor=id_alumno).order_by('-FechaCreacion')

        n_mensajes = recibidos.count()
        paginator = Paginator(recibidos, 10)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)
        return render_to_response("Alumnos/CampusParientes/mensajes_recibidos.html",
                                  {"var": var, "results": results, "paginator": paginator, "n_mensajes": n_mensajes,
                                   "deudas": deudas, "nro_deudas": nro_deudas, "estudiante": estudiante, "msg": msg,
                                   "query": query, "user": request.user, "periodo": periodo},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def cursos_semestre(request, id_alumno):
    if request.user.is_authenticated():
        try:
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
        except Alumno.DoesNotExist:
            return HttpResponse('')

        var = request.GET.get('q', 'l')
        periodo = Periodo.objects.get(id=ID_PERIODO)

        cursos = MatriculaCursos.objects.filter(PeriodoCurso__Periodo=periodo,
                                                MatriculaCiclo__Alumno__user_ptr=id_alumno)

        info_cursos = []
        for curso in cursos:
            horarios = Horario.objects.filter(PeriodoCurso=curso.PeriodoCurso)
            info_cursos.append([curso, horarios])

        return render_to_response("Alumnos/CampusParientes/cursos_disponibles.html",
                                  {"results": info_cursos, "var": var, "periodo": periodo, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def index_curso(request, id_alumno, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')

        try:
            estudiante = Alumno.objects.get(user_ptr=id_alumno)
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Alumnos/CampusParientes/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        return render_to_response("Alumnos/CampusParientes/index_cursos.html",
                                  {"var": var, "periodo": periodocurso.Periodo, "periodocurso": periodocurso,
                                   "user": request.user, "estudiante": estudiante},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')
