# -*- coding: utf-8 -*-
import random
import string

from django.contrib.auth.models import User

from Alumnos.models import Alumno
from funciones import response_excel

rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u',
       'Ñ': 'N', 'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U',
       'ä': 'a', 'ë': 'e', 'ï': 'i', 'ö': 'o', 'ü': 'u',
       'Ä': 'A', 'Ë': 'E', 'Ï': 'I', 'Ö': 'O', 'Ü': 'U',
       '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ', '!': ' ', '¿': ' ',
       '?': ' ', '^': ' ', '"': ' ', '-': ' ', '_': ' ', ' ': ''}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generatecorreo(apellidopat, apellitomat, nombre, intento):
    correo = ""
    if intento == 1:
        correo = apellidopat[:1] + apellitomat + nombre[:1] + "@udl.edu.pe"
        alumnos = Alumno.objects.filter(EmailInstitucional=correo)
        if alumnos.count() > 0:
            generatecorreo(apellidopat, apellitomat, nombre, 2)
        else:
            usuarios = User.objects.filter(email=correo)
            if usuarios.count() > 0:
                generatecorreo(apellidopat, apellitomat, nombre, 2)
    elif intento == 2:
        correo = apellidopat[:1] + apellitomat + nombre[:2] + "@udl.edu.pe"
        alumnos = Alumno.objects.filter(EmailInstitucional=correo)
        if alumnos.count() > 0:
            generatecorreo(apellidopat, apellitomat, nombre, 3)
        else:
            usuarios = User.objects.filter(email=correo)
            if usuarios.count() > 0:
                generatecorreo(apellidopat, apellitomat, nombre, 3)
    elif intento == 3:
        correo = apellidopat[:1] + apellitomat + nombre[:3] + "@udl.edu.pe"
        alumnos = Alumno.objects.filter(EmailInstitucional=correo)
        if alumnos.count() > 0:
            correo = "intentar de nuevo"
        else:
            usuarios = User.objects.filter(email=correo)
            if usuarios.count() > 0:
                correo = "intentar de nuevo"
    correo = replace_all(correo.encode('utf8'), rep)
    return correo.lower()


def generar_cuentas_correo(matriculados):
    listado = []
    for obj in matriculados:
        codigo = obj.Alumno.Codigo
        apellidopat = obj.Alumno.ApellidoPaterno.title()
        apellidomat = obj.Alumno.ApellidoMaterno.title()
        nombres = obj.Alumno.Nombres.title()
        apellidos = apellidopat + " " + apellidomat
        carrera = obj.Alumno.Carrera.__unicode__()
        carrera_id = obj.Alumno.Carrera.id
        correo = obj.Alumno.EmailInstitucional
        if correo is None or correo == "":
            try:
                correo = generatecorreo(apellidopat, apellidomat, nombres, 1)
                alumno = obj.Alumno
                alumno.EmailInstitucional = correo
                alumno.save()
            except:
                correo = "error"
        clave = "udl" + codigo.lower()

        listado.append(
            [codigo, apellidos, nombres, carrera, correo, clave, carrera_id])
    headers = ['codigo', 'Apellidos', 'Nombres', 'Carrera', 'Correo', 'Clave', 'Carrera ID']
    titulo = 'REPORTE DE CORREOS INSTITUCIONALES'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='correos_institucionales')
