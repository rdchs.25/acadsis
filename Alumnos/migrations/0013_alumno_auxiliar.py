# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0012_alumno_plan'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='auxiliar',
            field=models.CharField(default=None, editable=False, max_length=100, blank=True, null=True, verbose_name=b'auxiliar'),
        ),
    ]
