# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0001_initial'),
        ('Alumnos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='Planestudio',
            field=models.ForeignKey(to='Planestudio.Plan', null=True),
        ),
    ]
