# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Ubicacion', '0002_distrito'),
        ('Alumnos', '0007_auto_20180622_1240'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='Ubigeo',
            field=models.ForeignKey(default=None, to='Ubicacion.Distrito', null=True),
        ),
    ]
