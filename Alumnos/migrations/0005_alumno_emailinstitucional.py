# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0004_auto_20180403_1031'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='EmailInstitucional',
            field=models.EmailField(default=None, max_length=254, unique=True, null=True, verbose_name=b'E-mail Institucional'),
        ),
    ]
