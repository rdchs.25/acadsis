# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0005_alumno_emailinstitucional'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='EmailInstitucional',
            field=models.EmailField(null=True, default=None, max_length=254, blank=True, unique=True, verbose_name=b'E-mail Institucional'),
        ),
    ]
