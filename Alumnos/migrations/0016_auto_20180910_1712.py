# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0015_alumno_moodleid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='Moodleid',
            field=models.PositiveIntegerField(default=None, verbose_name=b'Moodle id', null=True, editable=False),
        ),
    ]
