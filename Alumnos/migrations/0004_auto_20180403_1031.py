# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0003_remove_alumno_planestudio'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='AnioEgreso',
            field=models.CharField(max_length=4, verbose_name=b'A\xc3\xb1o Egreso Colegio', choices=[(b'2018', b'2018'), (b'2017', b'2017'), (b'2016', b'2016'), (b'2015', b'2015'), (b'2014', b'2014'), (b'2013', b'2013'), (b'2012', b'2012'), (b'2011', b'2011'), (b'2010', b'2010'), (b'2009', b'2009'), (b'2008', b'2008'), (b'2007', b'2007'), (b'2006', b'2006'), (b'2005', b'2005'), (b'2004', b'2004'), (b'2003', b'2003'), (b'2002', b'2002'), (b'2001', b'2001'), (b'2000', b'2000'), (b'1999', b'1999'), (b'1998', b'1998'), (b'1997', b'1997'), (b'1996', b'1996'), (b'1995', b'1995'), (b'1994', b'1994'), (b'1993', b'1993'), (b'1992', b'1992'), (b'1991', b'1991'), (b'1990', b'1990'), (b'1989', b'1989'), (b'1988', b'1988'), (b'1987', b'1987'), (b'1986', b'1986'), (b'1985', b'1985'), (b'1984', b'1984'), (b'1983', b'1983'), (b'1982', b'1982'), (b'1981', b'1981'), (b'1980', b'1980'), (b'1979', b'1979'), (b'1978', b'1978'), (b'1977', b'1977'), (b'1976', b'1976'), (b'1975', b'1975'), (b'1974', b'1974'), (b'1973', b'1973'), (b'1972', b'1972'), (b'1971', b'1971'), (b'1970', b'1970'), (b'1969', b'1969')]),
        ),
        migrations.AlterField(
            model_name='alumno',
            name='SemestreEgresado',
            field=models.CharField(blank=True, max_length=3, null=True, verbose_name=b'Per\xc3\xadodo Egreso', choices=[(b'I', b'I'), (b'II', b'II'), (b'III', b'III')]),
        ),
    ]
