# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0005_detalleplan_detalleelectivo'),
        ('Alumnos', '0011_auto_20180713_1058'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='Plan',
            field=models.ForeignKey(default=None, blank=True, to='Planestudio.Plan', null=True, verbose_name=b'Plan de estudio'),
        ),
    ]
