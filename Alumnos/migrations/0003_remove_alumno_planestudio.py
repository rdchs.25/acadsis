# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0002_alumno_planestudio'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alumno',
            name='Planestudio',
        ),
    ]
