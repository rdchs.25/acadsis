# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0016_auto_20180910_1712'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='WbServiceId',
            field=models.CharField(max_length=50, null=True, verbose_name=b'WbServideId'),
        ),
    ]
