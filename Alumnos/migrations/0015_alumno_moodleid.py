# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0014_auto_20180826_1229'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='Moodleid',
            field=models.PositiveIntegerField(verbose_name=b'Moodle id', null=True, editable=False),
        ),
    ]
