# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0009_remove_alumno_distrito'),
    ]

    operations = [
        migrations.RenameField(
            model_name='alumno',
            old_name='Ubigeo',
            new_name='Distrito',
        ),
    ]
