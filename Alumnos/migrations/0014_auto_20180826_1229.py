# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0013_alumno_auxiliar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='Distrito',
            field=models.ForeignKey(to='Ubicacion.Distrito', null=True),
        ),
    ]
