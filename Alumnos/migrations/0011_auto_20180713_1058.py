# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0010_auto_20180713_1030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='Distrito',
            field=models.ForeignKey(default=99999, to='Ubicacion.Distrito'),
            preserve_default=False,
        ),
    ]
