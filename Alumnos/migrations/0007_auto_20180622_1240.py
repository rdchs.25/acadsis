# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import Alumnos.models


class Migration(migrations.Migration):

    dependencies = [
        ('Alumnos', '0006_auto_20180611_0835'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='EmailInstitucional',
            field=Alumnos.models.NullableEmailField(null=True, default=None, max_length=254, blank=True, unique=True, verbose_name=b'E-mail Institucional'),
        ),
    ]
