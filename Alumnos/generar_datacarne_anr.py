# -*- coding: utf-8 -*-

import os

from django.utils.encoding import smart_str

from Alumnos.models import Alumno
from Matriculas.models import MatriculaCiclo
from Pagos.models import PagoAlumno
from funciones import response_excel

# QUITA TILDES
rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
       'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
       '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' ', '-': ' ', '_': ' '}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


# CHOICES ESCUELAS, SEGUN FORMATO
ESCUELAS_PROFESIONALES = {
    'AT': 'ADM. TURISTICA',
    'AM': 'ADM. Y MARKETING',
    'IA': 'ING. AMBIENTAL',
    'IC': 'ING. COMERCIAL',
    'IS': 'ING. DE SISTEMAS'
}


def generar_datacarne_anr(request):
    anio = '2013'
    semestre = 'II'
    periodo = 10
    codigo_uni = '107'
    filial = 1
    path_dir_carne = '/home/CARNE_ANR/%s-%s/' % (anio, semestre)

    alumnos = Alumno.objects.filter(AnioIngreso=anio, Semestre=semestre)
    listado = []
    for alumno in alumnos:
        lista = []
        lista.append(codigo_uni)
        lista.append("000" + alumno.Codigo)
        lista.append(replace_all(smart_str(alumno.ApellidoPaterno), rep).upper().strip(" "))
        lista.append(replace_all(smart_str(alumno.ApellidoMaterno), rep).upper().strip(" "))
        lista.append(replace_all(smart_str(alumno.Nombres), rep).upper().strip(" "))
        lista.append('-')
        lista.append(ESCUELAS_PROFESIONALES[str(alumno.Carrera.Codigo)])
        lista.append(filial)
        pagos = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__id=periodo, MatriculaCiclo__Alumno=alumno,
                                          ConceptoPago__Descripcion__icontains='MATRICULA', Estado=True)
        if pagos.count() > 0:
            lista.append('V')
        else:
            lista.append('F')
        if alumno.DocIdentidad == 'DNI':
            lista.append(alumno.NroDocumento)
        else:
            lista.append('')

        if alumno.Foto != '':
            lista.append('Si')
        else:
            lista.append('No')

        if alumno.Foto != '':
            if os.path.isfile(alumno.Foto.path) == True:
                lista.append('Si')
            else:
                lista.append('No')
        else:
            lista.append('Vacio')

        matricula = MatriculaCiclo.objects.filter(Periodo__id=periodo, Alumno=alumno)
        if matricula.count() != 0:
            lista.append(matricula[0].Categoria.Categoria)
        else:
            lista.append('No matriculado')

        lista.append(alumno.Email)
        lista.append(alumno.Telefono)
        lista.append(alumno.Celular)
        listado.append(lista)
        if alumno.Foto != '' and pagos.count() > 0:
            path_foto = alumno.Foto.name
            archivo_foto = path_foto.split('/')[2]
            extension_archivo = archivo_foto.split('.')[1]
            nuevo_archivo = '%s000%s' % (codigo_uni, alumno.Codigo)
            os.system('cp %s %s%s.%s' % (alumno.Foto.path, path_dir_carne, nuevo_archivo, extension_archivo))

    headers = ["Cu", "Ca", "Ap", "Am", "No", "Fa", "Esc", "Fil", "Sol", "Dni", "Foto", "Existe Foto", "Categoria",
               "Email", "Telefono", "Celular"]
    titulo = 'REPORTE DE CARNES ANR'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='carnes_ANR')
