#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

# imports para traer variable de entorno django
import codecs
from Alumnos.models import Alumno


def registrar_correo():
    file1 = codecs.open("extras/migracion_acadsis.csv", 'r', 'utf-8')
    for line in file1:
        line = line.strip('\n')
        linea = line.split(',')
        codigo = linea[0]
        correo = linea[1]
        try:
            alumno = Alumno.objects.get(Codigo=codigo)
            alumno.EmailInstitucional = correo
            alumno.save()
        except Exception as error:
            print error
            print "ERROR, no existe alumno con codigo %s" % codigo
            continue

    file1.close()


registrar_correo()
