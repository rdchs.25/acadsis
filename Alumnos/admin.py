# -*- coding: utf-8 -*-
from django.contrib import admin

from Alumnos.forms import AlumnoAdminForm, ParienteAdminForm
from Alumnos.models import Alumno, AlumnoDocumento, Parentesco, Pariente
from funciones import response_excel


class AlumnoDocumentoInline(admin.TabularInline):
    model = AlumnoDocumento
    extra = 3


class AlumnoAdmin(admin.ModelAdmin):
    inlines = (AlumnoDocumentoInline,)
    form = AlumnoAdminForm
    actions = ['listado_excel']
    list_display = (
        'Codigo', 'Expediente', '__unicode__', 'carrera', 'documentos_entregados', 'documentos_entregados',
        'Observaciones',
        'Estado', 'Egreso', 'tiene_foto')

    fieldsets = (
        ('Datos del Alumno', {
            'fields': ('Codigo', 'Expediente', ('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'),
                       ('Nacimiento', 'LugarNacimiento', 'Sexo'), ('DocIdentidad', 'NroDocumento', 'OtroDocumento'),
                       'Foto', ('Departamento', 'Provincia', 'Distrito'), ('Direccion', 'Urbanizacion'),
                       ('Email', 'EmailInstitucional'),
                       ('Telefono', 'Celular', 'EstadoCivil'), 'Carrera',
                       ('AnioIngreso', 'Semestre', 'Modalidad'), ('Colegio', 'AnioEgreso'), 'Parentesco',
                       'Observaciones', 'Estado', 'Egresoidiomas', 'Egreso',
                       ('AnioEgresado', 'SemestreEgresado', 'selected_dep', 'selected_prov', 'selected_dist',),
                       ),
        }),
    )
    search_fields = ('Nombres', 'ApellidoPaterno', 'ApellidoMaterno', 'Codigo', 'Expediente')
    radio_fields = {"Sexo": admin.HORIZONTAL}
    # raw_id_fields = ('Colegio','Parentesco',)
    list_filter = (
        'Carrera', 'AnioIngreso', 'Semestre', 'Modalidad', 'Estado', 'Egreso', 'AnioEgresado', 'SemestreEgresado')

    def listado_excel(self, request, queryset):
        from Alumnos.views import alumnos_excel
        return alumnos_excel(queryset)

    listado_excel.short_description = "Listado de ingresantes seleccionados en Excel"


admin.site.register(Alumno, AlumnoAdmin)


class ParentescoAdmin(admin.ModelAdmin):
    list_display = ('Apoderado', 'Madre', 'Padre',)
    search_fields = ('ApePatTutor', 'ApeMatTutor', 'NombresTutor',)
    fieldsets = (
        ('Datos del Padre', {
            'fields': (
                ('ApePatPadre', 'ApeMatPadre', 'NombresPadre'), ('DNIPadre', 'VivePadre'), ('DirPadre', 'UrbPadre'),
                ('DepPadre', 'ProvPadre', 'DistPadre'), ('TelfPadre', 'CelPadre'),),
        }),
        ('Ocupación del Padre', {
            'classes': ('collapse',),
            'fields': ('OcupPadre', 'CtroTrabajoPadre', 'DirOcupPadre', 'TelfOcupPadre', 'IngresoPadre',),
        }),
        ('Datos de la Madre', {
            'fields': (
                ('ApePatMadre', 'ApeMatMadre', 'NombresMadre'), ('DNIMadre', 'ViveMadre'), ('DirMadre', 'UrbMadre'),
                ('DepMadre', 'ProvMadre', 'DistMadre'), ('TelfMadre', 'CelMadre'),),
        }),
        ('Ocupación de la Madre', {
            'classes': ('collapse',),
            'fields': ('OcupMadre', 'CtroTrabajoMadre', 'DirOcupMadre', 'TelfOcupMadre', 'IngresoMadre',),
        }),
        ('Datos del Apoderado', {
            'fields': ('Responsable', 'Parentesco', ('ApePatTutor', 'ApeMatTutor', 'NombresTutor'), 'DNITutor',
                       ('DirTutor', 'UrbTutor'), ('DepTutor', 'ProvTutor', 'DistTutor'), ('TelfTutor', 'CelTutor'),),
        }),
        ('Ocupación del Apoderado', {
            'classes': ('collapse',),
            'fields': ('OcupTutor', 'CtroTrabajoTutor', 'DirOcupTutor', 'TelfOcupTutor', 'IngresoTutor',),
        }),
    )

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Alumnos/frmparentesco.js"
              )


admin.site.register(Parentesco, ParentescoAdmin)


class ParienteAdmin(admin.ModelAdmin):
    form = ParienteAdminForm
    fieldsets = (
        ("Datos del Pariente", {'fields': (
            'Nombres', 'ApellidoPaterno', 'ApellidoMaterno', 'Email', 'Foto', 'Nacimiento', 'Sexo', 'Dni', 'Direccion',
            'Departamento', 'Provincia', 'Distrito', 'EstadoCivil', 'Telefono', 'Celular', 'Parentesco', 'Alumno',
            'Estado',
            'selected_lugar')}),
    )
    list_display = ('Email', '__unicode__', 'Estado')
    search_fields = ('Nombres', 'ApellidoPaterno', 'ApellidoMaterno',)
    list_filter = ('Estado',)
    radio_fields = {"Sexo": admin.HORIZONTAL}
    filter_horizontal = ('Alumno',)
    actions = ['listado_excel']

    def listado_excel(self, request, queryset):
        listado = []
        for obj in queryset:
            apellido_paterno = obj.ApellidoPaterno
            apellido_materno = obj.ApellidoMaterno
            nombres = obj.Nombres
            sexo = obj.Sexo
            nacimiento = obj.Nacimiento
            doc_identidad = obj.Dni
            departamento = obj.Departamento
            provincia = obj.Provincia
            distrito = obj.Distrito.Distrito
            direccion = obj.Direccion
            estado_civil = obj.EstadoCivil
            email = obj.Email
            telefono = obj.Telefono
            celular = obj.Celular
            parentesco = obj.Parentesco
            if obj.Estado is True:
                estado = 'Si'
            else:
                estado = 'No'
            fecha_creacion = obj.Fecha_creacion.strftime('%d-%m-%Y')
            ultima_edicion = obj.Ultima_edicion.strftime('%d-%m-%Y')
            listado.append(
                [apellido_paterno, apellido_materno, nombres, sexo, nacimiento, doc_identidad, departamento, provincia,
                 distrito, direccion, email, telefono, celular, estado_civil, parentesco, estado, fecha_creacion,
                 ultima_edicion])
        headers = ['Apellido Paterno', 'Apellido Materno', 'Nombres', 'Sexo', 'Nacimiento', 'DNI', 'Departamento',
                   'Provincia', 'Distrito', 'Direccion', 'Email', 'Telefono', 'Celular', 'Estado Civil', 'Parentesco',
                   'Estado', 'Creacion', 'Ultima Edicion']
        titulo = 'Listado de Participantes Curso'
        return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='participantes')

    listado_excel.short_description = "Listado de participantes seleccionados en Excel"


admin.site.register(Pariente, ParienteAdmin)
