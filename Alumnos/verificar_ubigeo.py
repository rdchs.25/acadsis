#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Alumnos.models import Alumno
from Ubicacion.models import Distrito


def script_verificar_ubigeo():
    alumnosudl = Alumno.objects.filter(Distrito_id=None)
    contador = 0
    for alumno in alumnosudl:
        distritoudl = alumno.auxiliar
        provinciaudl = alumno.Provincia
        try:
            distrito = Distrito.objects.get(Distrito=distritoudl, Provincia=provinciaudl)
            alumno.Distrito_id = distrito.id
            alumno.save()
        except Distrito.DoesNotExist:
            print ("distrito del alumno: " + alumno.Codigo + " no existe, referencia: " + distritoudl)
            contador = contador + 1
    print ("Cantidad de errores: " + str(contador))


script_verificar_ubigeo()
