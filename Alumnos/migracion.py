#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Pagos.models import DetallePago, Comprobante

from UDL.settings import MEDIA_ROOT

# imports para traer variable de entorno django
import codecs


def script_matricular():
    file1 = codecs.open("extras/migracion.csv", 'r', 'utf-8')
    i = 1
    nombre_archivo = "migracion" + ".csv"
    ruta_archivo = str(MEDIA_ROOT)
    destination = csv.writer(open('%s/%s' % (ruta_archivo, nombre_archivo), 'wb+'), delimiter='|')
    for line in file1:
        line = line.strip('\n')
        linea = line.split('|')
        if i == 1:
            i = i + 1
            continue
        serie = linea[1]
        numero = linea[2]
        alumno = linea[3].encode('utf-8')
        concepto = linea[4].encode('utf-8')

        # para serie de pregrado
        if serie == "1":
            correlativo = quitarceros(numero)
            try:
                DetallePago.objects.get(Comprobante__Correlativo__icontains=correlativo)
                destination.writerow([serie, numero, "existe", "pregrado", alumno, concepto])
            except DetallePago.DoesNotExist:
                try:
                    Comprobante.objects.get(Correlativo__icontains=correlativo)
                    destination.writerow(
                        [serie, numero, "existe - no relacionado con alumno alguno", "pregrado", alumno, concepto])
                except Comprobante.DoesNotExist:
                    destination.writerow([serie, numero, "no existe", "pregrado", alumno, concepto])
                except Comprobante.MultipleObjectsReturned:
                    comprobantes = Comprobante.objects.filter(Correlativo__icontains=correlativo)
                    for comprobante in comprobantes:
                        if quitarceros(comprobante.Correlativo) == correlativo:
                            destination.writerow([serie, numero, "existe", "idiomas", alumno, concepto])

            except DetallePago.MultipleObjectsReturned:
                detallespago = DetallePago.objects.filter(Comprobante__Correlativo__icontains=correlativo)
                for detalle in detallespago:
                    if quitarceros(detalle.Comprobante.Correlativo) == correlativo:
                        destination.writerow([serie, numero, "existe", "pregrado", alumno, concepto])

        # para serie de idiomas
        if serie == "3":
            correlativo = quitarceros(numero)
            try:
                DetallePago.objects.using("idiomas").get(Comprobante__Correlativo__icontains=correlativo)
                destination.writerow([serie, numero, "existe", "idiomas", alumno, concepto])
            except DetallePago.DoesNotExist:
                try:
                    Comprobante.objects.using("idiomas").get(Correlativo__icontains=correlativo)
                    destination.writerow(
                        [serie, numero, "existe - no relacionado con alumno alguno", "idiomas", alumno, concepto])
                except Comprobante.DoesNotExist:
                    destination.writerow([serie, numero, "no existe", "idiomas", alumno, concepto])
                except Comprobante.MultipleObjectsReturned:
                    comprobantes = Comprobante.objects.using("idiomas").filter(Correlativo__icontains=correlativo)
                    for comprobante in comprobantes:
                        if quitarceros(comprobante.Correlativo) == correlativo:
                            destination.writerow([serie, numero, "existe", "idiomas", alumno, concepto])
            except DetallePago.MultipleObjectsReturned:
                detallespago = DetallePago.objects.using("idiomas").filter(
                    Comprobante__Correlativo__icontains=correlativo)
                for detalle in detallespago:
                    if quitarceros(detalle.Comprobante.Correlativo) == correlativo:
                        destination.writerow([serie, numero, "existe", "idiomas", alumno, concepto])


def quitarceros(numero):
    primercaracter = numero[:2]
    if primercaracter == "0":
        return quitarceros(primercaracter[1:])
    else:
        return numero


script_matricular()
