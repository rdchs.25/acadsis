# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib.auth.models import User
from django.db import models

from django.conf import settings

from Carreras.models import Carrera
from Colegios.models import Colegio
from Documentos.models import Documento
from Periodos.models import ANIO_CHOICES, SEMESTRE_CHOICES
from Planestudio.models import Plan
from UDL.facturacion_webservice import facturacion_webservices
from UDL.moodle_webservice import moodle_webservices
from Ubicacion.models import Provincia, Distrito

GENDER_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

MODALIDAD_CHOICES = (
    ('Examen Ordinario', 'Examen Ordinario'),
    ('Graduados Titulados', 'Graduados Titulados'),
    ('Traslado Externo', 'Traslado Externo'),
    ('Primeros Puestos', 'Primeros Puestos'),
    ('Deportistas Calificados', 'Deportistas Calificados'),
    ('Personas Discapacitadas', 'Personas Discapacitadas'),
    ('Escuela Pre UDL', 'Escuela Pre UDL'),
)

CONDICION_CHOICES = (
    ('Postulante', 'Postulante'),
    ('Retirado', 'Retirado'),
)

ASISTENCIA_CHOICES = (
    ('A', 'Asistio'),
    ('F', 'Falto'),
)

DOCUMENTO_CHOICES = (
    ('DNI', 'DNI'),
    ('Libreta Militar', 'Libreta Militar'),
    ('Pasaporte', 'Pasaporte'),
    ('Carnet Ext.', 'Carnet Ext.'),
    ('Otro', 'Otro'),
    ('Ninguno', 'Ninguno'),
)

CIVIL_CHOICES = (
    ('Soltero', 'Soltero'),
    ('Casado', 'Casado'),
    ('Viudo', 'Viudo'),
    ('Divorciado', 'Divorciado'),
)

RESPONSABLE_CHOICES = (
    ('Padre', 'Padre'),
    ('Madre', 'Madre'),
    ('Apoderado', 'Apoderado'),
)

EGRESO_CHOICES = (
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('2014', '2014'),
    ('2013', '2013'),
    ('2012', '2012'),
    ('2011', '2011'),
    ('2010', '2010'),
    ('2009', '2009'),
    ('2008', '2008'),
    ('2007', '2007'),
    ('2006', '2006'),
    ('2005', '2005'),
    ('2004', '2004'),
    ('2003', '2003'),
    ('2002', '2002'),
    ('2001', '2001'),
    ('2000', '2000'),
    ('1999', '1999'),
    ('1998', '1998'),
    ('1997', '1997'),
    ('1996', '1996'),
    ('1995', '1995'),
    ('1994', '1994'),
    ('1993', '1993'),
    ('1992', '1992'),
    ('1991', '1991'),
    ('1990', '1990'),
    ('1989', '1989'),
    ('1988', '1988'),
    ('1987', '1987'),
    ('1986', '1986'),
    ('1985', '1985'),
    ('1984', '1984'),
    ('1983', '1983'),
    ('1982', '1982'),
    ('1981', '1981'),
    ('1980', '1980'),
    ('1979', '1979'),
    ('1978', '1978'),
    ('1977', '1977'),
    ('1976', '1976'),
    ('1975', '1975'),
    ('1974', '1974'),
    ('1973', '1973'),
    ('1972', '1972'),
    ('1971', '1971'),
    ('1970', '1970'),
    ('1969', '1969'),
)


class Parentesco(models.Model):
    # Informacion del padre
    ApePatPadre = models.CharField("Ape. Pat.", max_length=50, null=True, blank=True)
    ApeMatPadre = models.CharField("Ape. Mat.", max_length=50, null=True, blank=True)
    NombresPadre = models.CharField("Nombres", max_length=50, null=True, blank=True)
    DNIPadre = models.PositiveIntegerField("DNI", null=True, blank=True)
    VivePadre = models.BooleanField("¿Vive?", default=True)
    DirPadre = models.CharField("Dirección", max_length=100, null=True, blank=True)
    UrbPadre = models.CharField("Urbanización", max_length=100, null=True, blank=True)
    DepPadre = models.CharField("Departamento", max_length=50, null=True, blank=True)
    ProvPadre = models.CharField("Provincia", max_length=50, null=True, blank=True)
    DistPadre = models.CharField("Distrito", max_length=50, null=True, blank=True)
    TelfPadre = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    CelPadre = models.CharField("Celular", max_length=20, null=True, blank=True)
    OcupPadre = models.CharField("Ocupación", max_length=50, null=True, blank=True)
    CtroTrabajoPadre = models.CharField("Ctro. Trabajo", max_length=50, null=True, blank=True)
    TelfOcupPadre = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    DirOcupPadre = models.CharField("Dirección", max_length=100, null=True, blank=True)
    IngresoPadre = models.PositiveIntegerField("Ingreso S./", null=True, blank=True)
    # Informacion de la madre
    ApePatMadre = models.CharField("Ape. Pat.", max_length=50, null=True, blank=True)
    ApeMatMadre = models.CharField("Ape. Mat.", max_length=50, null=True, blank=True)
    NombresMadre = models.CharField("Nombres", max_length=50, null=True, blank=True)
    DNIMadre = models.PositiveIntegerField("DNI", null=True, blank=True)
    ViveMadre = models.BooleanField("¿Vive?", default=True)
    DirMadre = models.CharField("Dirección", max_length=100, null=True, blank=True)
    UrbMadre = models.CharField("Urbanización", max_length=100, null=True, blank=True)
    DepMadre = models.CharField("Departamento", max_length=50, null=True, blank=True)
    ProvMadre = models.CharField("Provincia", max_length=50, null=True, blank=True)
    DistMadre = models.CharField("Distrito", max_length=50, null=True, blank=True)
    TelfMadre = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    CelMadre = models.CharField("Celular", max_length=20, null=True, blank=True)
    OcupMadre = models.CharField("Ocupación", max_length=50, null=True, blank=True)
    CtroTrabajoMadre = models.CharField("Ctro. Trabajo", max_length=50, null=True, blank=True)
    TelfOcupMadre = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    DirOcupMadre = models.CharField("Dirección", max_length=100, null=True, blank=True)
    IngresoMadre = models.PositiveIntegerField("Ingreso S./", null=True, blank=True)
    # Informacion del tutor
    Responsable = models.CharField("Responsable", max_length=50, choices=RESPONSABLE_CHOICES)
    Parentesco = models.CharField("Parentesco", max_length=50)
    ApePatTutor = models.CharField("Ape. Pat.", max_length=50)
    ApeMatTutor = models.CharField("Ape. Mat.", max_length=50)
    NombresTutor = models.CharField("Nombres", max_length=50)
    DNITutor = models.PositiveIntegerField("DNI", null=True, blank=True)
    DirTutor = models.CharField("Dirección", max_length=100, null=True, blank=True)
    UrbTutor = models.CharField("Urbanización", max_length=100, null=True, blank=True)
    DepTutor = models.CharField("Dep.", max_length=50, null=True, blank=True)
    ProvTutor = models.CharField("Prov.", max_length=50, null=True, blank=True)
    DistTutor = models.CharField("Dist.", max_length=50, null=True, blank=True)
    TelfTutor = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    CelTutor = models.CharField("Celular", max_length=20, null=True, blank=True)
    OcupTutor = models.CharField("Ocupación", max_length=50, null=True, blank=True)
    CtroTrabajoTutor = models.CharField("Ctro. Trabajo", max_length=50, null=True, blank=True)
    TelfOcupTutor = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    DirOcupTutor = models.CharField("Dirección", max_length=100, null=True, blank=True)
    IngresoTutor = models.PositiveIntegerField("Ingreso S./", null=True, blank=True)

    def __unicode__(self):
        Persona = ""
        if self.Responsable == "Padre":
            Persona = self.ApePatPadre + " " + self.ApeMatPadre + " " + self.NombresPadre
        elif self.Responsable == "Madre":
            Persona = self.ApePatMadre + " " + self.ApeMatMadre + " " + self.NombresMadre
        elif self.Responsable == "Apoderado":
            Persona = self.ApePatTutor + " " + self.ApeMatTutor + " " + self.NombresTutor

        return u' %s - %s' % (self.Responsable, Persona)

    def Padre(self):
        Persona = self.ApePatPadre + " " + self.ApeMatPadre + " " + self.NombresPadre
        return u'%s' % (Persona)

    def Madre(self):
        Persona = self.ApePatMadre + " " + self.ApeMatMadre + " " + self.NombresMadre
        return u'%s' % (Persona)

    def Apoderado(self):
        Persona = self.ApePatTutor + " " + self.ApeMatTutor + " " + self.NombresTutor
        return u'%s' % (Persona)

    class Meta:
        verbose_name = "Familiares del Alumno"
        verbose_name_plural = "Familiares de los Alumnos"


def obtenerpath(instance, filename):
    return u'alumnos/%s/%s' % (instance.Codigo, filename)


class NullableEmailField(models.EmailField):
    description = "EmailField that stores NULL but returns ''"
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        if isinstance(value, models.EmailField):
            return value
        return value or ''

    def get_prep_value(self, value):
        return value or None


class Alumno(User):
    WbServiceId = models.CharField("WbServideId", max_length=50, null=True)
    Codigo = models.CharField("Código", max_length=7, unique=True)
    Expediente = models.CharField("Expediente", max_length=7, unique=True)
    Nombres = models.CharField(max_length=50)
    ApellidoPaterno = models.CharField("Ape. Pat.", max_length=50)
    ApellidoMaterno = models.CharField("Ape. Mat.", max_length=50)
    Nacimiento = models.DateField()
    LugarNacimiento = models.CharField("Lug. Nac.", max_length=50, null=True, blank=True)
    Sexo = models.CharField(max_length=1, choices=GENDER_CHOICES)
    DocIdentidad = models.CharField("Doc. Identidad", max_length=20, choices=DOCUMENTO_CHOICES)
    OtroDocumento = models.CharField("Otro Doc.", max_length=20, null=True, blank=True)
    NroDocumento = models.PositiveIntegerField("Nº Doc.", null=True, blank=True)
    Provincia = models.ForeignKey(Provincia)
    Distrito = models.ForeignKey(Distrito, null=True)
    Direccion = models.CharField("Dirección", max_length=100)
    Urbanizacion = models.CharField("Urbanización", max_length=100, null=True, blank=True)
    Email = models.EmailField("E-mail", null=True, blank=True)
    EstadoCivil = models.CharField("Estado Civil", max_length=20, choices=CIVIL_CHOICES, default='Soltero')
    Telefono = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    Celular = models.CharField("Celular", max_length=20, null=True, blank=True)
    Carrera = models.ForeignKey(Carrera)
    AnioIngreso = models.CharField("Año Ingreso", max_length=4, choices=ANIO_CHOICES)
    Semestre = models.CharField("Período Ingreso", max_length=2, choices=SEMESTRE_CHOICES)
    Modalidad = models.CharField("Modalidad", max_length=50, choices=MODALIDAD_CHOICES)
    Documentos = models.ManyToManyField(Documento, through='AlumnoDocumento')
    Colegio = models.ForeignKey(Colegio, verbose_name="Colegio")
    Parentesco = models.ForeignKey(Parentesco, verbose_name="Parentescos")
    AnioEgreso = models.CharField("Año Egreso Colegio", max_length=4, choices=EGRESO_CHOICES)
    Observaciones = models.TextField(blank=True, null=True)
    Condicion = models.CharField("Condición", max_length=50, choices=CONDICION_CHOICES, default='Postulante',
                                 editable=False)
    Asistencia = models.CharField("Asistencia", max_length=50, choices=ASISTENCIA_CHOICES, default='A')
    Puntaje = models.DecimalField("Puntaje", null=True, blank=True, max_digits=6, decimal_places=3, default='0.00')
    Estado = models.BooleanField("Ingresó", default=True)
    Actualizodatos = models.BooleanField("¿Actualizó datos?", default=False, editable=False)
    Egreso = models.BooleanField("¿Egresó?", default=False)
    AnioEgresado = models.CharField("Año Egreso", max_length=4, choices=ANIO_CHOICES, null=True, blank=True)
    SemestreEgresado = models.CharField("Período Egreso", max_length=3, choices=SEMESTRE_CHOICES, null=True, blank=True)
    GeneroCarne = models.BooleanField("¿Generó carné?", default=False, editable=False)
    Foto = models.ImageField(upload_to=obtenerpath, blank=True, null=True, verbose_name="Foto")
    Turno = models.CharField("Turno", max_length=2)
    Egresoidiomas = models.BooleanField("¿Egresó IDIOMAS?", default=False)
    EmailInstitucional = NullableEmailField("E-mail Institucional", blank=True, null=True, default=None, unique=True)
    Plan = models.ForeignKey(Plan, default=None, blank=True, null=True, verbose_name="Plan de estudio")
    auxiliar = models.CharField("auxiliar", default=None, blank=True, null=True, editable=False, max_length=100)
    Moodleid = models.PositiveIntegerField("Moodle id", editable=False, null=True, default=None)

    def __unicode__(self):
        return u'%s %s %s' % (self.ApellidoPaterno, self.ApellidoMaterno, self.Nombres)

    def carrera(self):
        return u'%s' % self.Carrera.Carrera

    def tiene_foto(self):
        if self.Foto == '' or self.Foto is None:
            tiene_foto = 'No'
            return 'No'
        else:
            return '<a href="foto/%s" target="_blank" >Si</a>' % self.id

    tiene_foto.allow_tags = True

    def periodoingreso(self):
        return u'%s-%s' % (self.AnioIngreso, self.Semestre)

    def documentos_entregados(self):
        cadena = ""
        for campo in self.Documentos.all():
            cadena += str(campo) + ', '
        return cadena

    def documentos_no_entregados(self):
        modalidad = self.Modalidad
        documentos_entregados = self.Documentos.all()
        documentos_total = Documento.objects.filter(Modalidad=modalidad)
        lista = []
        for doc in documentos_total:
            if doc not in documentos_entregados:
                lista.append(doc.Documento)
        return ", ".join(lista)

    def Departamento(self):
        return self.Provincia.Departamento

    def add_grupo_alumnos(self):
        from django.contrib.auth.models import Group
        grupo_alumnos = Group.objects.filter(name='Alumnos UDL').count()
        if grupo_alumnos == 0:
            crear_grupo = Group(name='Alumnos UDL')
            crear_grupo.save()
            ultimo_grupo_creado = Group.objects.order_by('-id')[0]
            self.groups.add(ultimo_grupo_creado.id)
        else:
            grupo_alumnos = Group.objects.get(name='Alumnos UDL')
            self.groups.add(grupo_alumnos.id)

    def delete(self):
        json_persona = {
            "cPerCodigo": self.WbServiceId
        }
        try:
            rpta = facturacion_webservices('EliminarPersona?cPerCodigo=' + self.WbServiceId, json_persona)
            print('DELETE')
            print(rpta)
        except:
            print('error')
        super(Alumno, self).delete()

    def facturacion_json_solicitud(self, id):

        # convertir estado civil
        switchermodalidades = {
            'Examen Ordinario': 1,
            'Graduados Titulados': 2,
            'Traslado Externo': 3,
            'Primeros Puestos': 4,
            'Deportistas Calificados': 5,
            'Personas Discapacitadas': 6,
            'Escuela Pre UDL': 7
        }
        modalidad = switchermodalidades.get(str(self.Modalidad))

        datos_solictud = {
            "nAdmModCodigo": modalidad,  # pide un integer y tenemos un string
            "nAdmDocSolCodigo": 0,  # autogenerado
            "dAdmModLastDate": str(datetime.now()),
            "nAdmDocSolPeriodo": 1,  # periodo
            "cPerJurCodigo": "1000003456",
            "dAdmDocSolFecha": str(datetime.now()),
            "nAdmDocSolEstado": 1,
            "nOpeCodigo": 0,
            "nAdmDocSolAprobo": 1,
            "cAdmDocSolKey": "",
            "nEstablecimiento": 1,
            "cAdmDocSolCode": "",
            "cAdmDocSolPerLast": "",
            "nAcdProgCodigo": 1,
            "nAdmDocSolGenero": 0,
            "cPerCodigo": id,
            "nAdmDocSolAccion": 1,
            "nAdmDocSolMovMotivo": 1,
            "cAdmDocSolMovObs": "",
            "nUniOrgCodigo": 0,
            "nFilCodigo": 0,
            "nSedCodigo": 0,
            "nProyCodigo": 0,
            "nSucCodigo": 0,
            "nConsCodigo": 0,
            "nBieCodigo": 0,
            "nActCodigo": 0,
            "nPrdActivacion": "1",
            "nPrdTermino": 0,
            "nAcdPlaEstCodigo": 1,  # alumno
            "nAcdPlaPayCodigo": 0,
            "nAcdProNivel": 0
        }
        return datos_solictud

    def facturacion_json_persona(self):
        # convertir orientación sexual
        if self.Sexo == 'M':
            sexo = 1
        else:
            sexo = 2

        # convertir estado civil
        switcher = {
            'Casado': 1,
            'Soltero': 2,
            'Viudo': 3,
            'Divorciado': 4
        }
        estadoCivil = switcher.get(str(self.EstadoCivil))

        # registrar persona, nuevo estudiante o editar información
        if self.WbServiceId is None:
            idService = ""
        else:
            idService = self.WbServiceId

        # json
        datos_for_facturacion = {
            "cPerCodigo": idService,
            "cPerApellPaterno": self.ApellidoPaterno,
            "cPerApellMaterno": self.ApellidoMaterno,
            "cPerPriNombre": self.Nombres,
            "cPerSegNombre": "",
            "cPerTerNombre": "",
            "dPerNacimiento": str(datetime.strptime(str(self.Nacimiento), "%Y-%m-%d")),
            "nPerTipo": 1,
            "nPerEstado": 1,
            "cUbiGeoCodigo": self.Distrito.Codigoubigeo,
            "nUbiGeoCodigo": 0,
            "dPerDate": str(datetime.now()),
            "dPerNameFecEfectiva": str(datetime.now()),
            "PerMail": [
                {
                    "cPerCodigo": "",
                    "nPerMaiTipo": 2,
                    "cPerMaiNombre": self.EmailInstitucional,
                    "nPerEstado": 1
                }
            ],
            "PerDomicilio": [
                {
                    "cPerCodigo": "",
                    "nPerDomTipo": 2,  # PREGUNTAR
                    "cPerDomDireccion": self.Direccion,
                    "cPerDomNumero": "",
                    "cPerDomManzana": "",
                    "cPerDomLote": "",
                    "nPerDomSubTipo": 2,
                    "cPerDomSubTipoNumero": "403",
                    "nPerDomResTipo": 9,
                    "cPerDomResNombre": "",
                    "nPerDomSecTipo": 2,
                    "cPerDomSecNombre": "",
                    "cUbiGeoCodigo": self.Distrito.Codigoubigeo,
                    "nPerDomCarta": 0,
                    "nPerDomActual": 6,
                    "nPerDomStatus": 99,
                    "nUbiCodigo": self.Distrito.Codigoubigeo,
                    "nUbiGeoCodigo": 0,
                    "cPerDomFullDireccion": self.Direccion,
                    "dEffDate": "2018-09-27T10:43:00.9146161-05:00",
                    "cPerDepartamento": self.Provincia.Departamento.Departamento.encode('utf-8'),
                    "cPerProvincia": self.Provincia.Provincia.encode('utf-8'),
                    "cPerDistrito": self.Distrito.Distrito.encode('utf-8')
                },
            ],
            "nPerIdeTipo": 25,
            "cPerIdeNumero": str(self.NroDocumento),
            "nPerNatSexo": sexo,
            "nPerNatEstCivil": estadoCivil,
            "nPerNatTipResidencia": 1,
            "nPerNatSitLaboral": 0,
            "nPerNatOcupacion": 0,
            "nPerNatCondicion": 1,
            "nPerTratamiento": 0,
            "dPerNatFecEfectiva": str(datetime.now()),
            "cPerJurCodigo": "1000003456",
            "nPerRelTipo": 3,
            "nPerRelEstado": 1
        }
        return datos_for_facturacion

    def save(self, **kwargs):
        iderp = None
        # Registrar en webservices
        if settings.ACTIVAR_SERVICIOS_FACTURACION == 1:
            if self.DocIdentidad == 'DNI':
                try:
                    json_persona = self.facturacion_json_persona()
                    iderp = facturacion_webservices("RegistraPersona", json_persona)
                    if iderp is not False:
                        json_doc_sol_admision = self.facturacion_json_solicitud(iderp)
                        facturacion_webservices("RegistraDocSolAdmision", json_doc_sol_admision)
                except:
                    print('Error al registrar en ERP')

        # Registrar en udl
        if not self.user_ptr_id:
            self.username = self.Codigo
            self.first_name = self.Nombres
            self.last_name = self.ApellidoPaterno + " " + self.ApellidoMaterno
            self.email = self.Email
            contrasena = self.Codigo
            self.WbServiceId = iderp
            self.set_password(contrasena)
            super(Alumno, self).save()
            return self.add_grupo_alumnos()
        else:
            if settings.ACTIVAR_SERVICIOS_MOODLE == 1:
                if self.EmailInstitucional is not None and self.EmailInstitucional != "":
                    if self.Moodleid is None:
                        datos_usuario = {
                            'users[0][username]': self.Codigo.lower(),
                            'users[0][password]': "udl" + self.Codigo,
                            'users[0][firstname]': self.Nombres.encode('utf-8'),
                            'users[0][lastname]': (self.ApellidoPaterno + " " + self.ApellidoMaterno).encode('utf-8'),
                            'users[0][email]': self.EmailInstitucional,
                            'users[0][auth]': 'manual',
                            'users[0][country]': 'PE',
                            'users[0][city]': self.Distrito.Distrito.encode('utf-8')

                        }
                        rest_datos_usuario = moodle_webservices("core_user_create_users", datos_usuario)
                        self.Moodleid = rest_datos_usuario[0]['id']
                    else:
                        datos_usuario = {
                            'users[0][username]': self.Codigo.lower(),
                            'users[0][password]': "udl" + self.Codigo,
                            'users[0][firstname]': self.Nombres.encode('utf-8'),
                            'users[0][lastname]': (self.ApellidoPaterno + " " + self.ApellidoMaterno).encode('utf-8'),
                            'users[0][email]': self.EmailInstitucional,
                            'users[0][auth]': 'manual',
                            'users[0][country]': 'PE',
                            'users[0][id]': self.Moodleid,
                            'users[0][city]': self.Distrito.Distrito.encode('utf-8')
                        }
                        moodle_webservices("core_user_update_users", datos_usuario)
            self.username = self.Codigo
            contrasena = self.Codigo
            self.set_password(contrasena)
            self.first_name = self.Nombres
            self.last_name = self.ApellidoPaterno + " " + self.ApellidoMaterno
            self.email = self.Email
            self.WbServiceId = iderp
            return super(Alumno, self).save()

    class Meta:
        verbose_name = "Alumno UDL"
        verbose_name_plural = "Alumnos UDL"
        permissions = (("read_alumno", "Observar Alumnos UDL"),)


def obtenerpath_alumnodocumento(instance, filename):
    return u'alumnos/documentos/%s/%s' % (instance.Alumno.Codigo, filename)


class AlumnoDocumento(models.Model):
    Alumno = models.ForeignKey(Alumno)
    Documento = models.ForeignKey(Documento)
    Numero = models.CharField("Número", max_length=20, null=True, blank=True)
    Imagen = models.ImageField(upload_to=obtenerpath_alumnodocumento, blank=True, null=True, verbose_name="Imágen")

    def __unicode__(self):
        return u'%s %s' % (self.Alumno, self.Documento)

    class Meta:
        verbose_name = "Documento de Alumno"
        verbose_name_plural = "Documentos de Alumnos"
        unique_together = (("Alumno", "Documento"))


PARENTESCO_CHOICES = (
    ('Padre', 'Padre-Madre'),
    ('Abuelo', 'Abuelo(a)'),
    ('Tio', 'Tío(a)'),
    ('Primo', 'Primo(a)'),
    ('Hermano', 'Hermano(a)'),
)


def obtenerpath_foto_pariente(instance, filename):
    ext = filename.split('.')[-1]
    return u'parientes/%s/foto-%s.%s' % (instance.username, instance.username, ext)


class Pariente(User):
    Nombres = models.CharField(max_length=50)
    ApellidoPaterno = models.CharField("Ape. Pat.", max_length=50)
    ApellidoMaterno = models.CharField("Ape. Mat.", max_length=50)
    Email = models.EmailField("E-mail", unique=True)
    Nacimiento = models.DateField("Nacimiento", null=True, blank=True)
    Sexo = models.CharField(max_length=1, choices=GENDER_CHOICES, default='M')
    Dni = models.CharField("DNI", max_length=20, null=True, blank=True)
    Direccion = models.CharField("Dirección", max_length=100, null=True, blank=True)
    Departamento = models.CharField("Departamento", max_length=50, null=True, blank=True)
    Provincia = models.CharField("Provincia", max_length=50, null=True, blank=True)
    Distrito = models.CharField("Distrito", max_length=50, null=True, blank=True)
    EstadoCivil = models.CharField("Estado Civil", max_length=20, choices=CIVIL_CHOICES, default='Soltero')
    Telefono = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    Celular = models.CharField("Celular", max_length=20, null=True, blank=True)
    Parentesco = models.CharField("Parentesco", max_length=20, choices=PARENTESCO_CHOICES, default='Padre')
    Estado = models.BooleanField("Activado", default=True)
    Fecha_creacion = models.DateTimeField('Fecha creación', auto_now_add=True, editable=False)
    Ultima_edicion = models.DateTimeField('Ultima edición', auto_now=True, editable=False)
    Alumno = models.ManyToManyField(Alumno, verbose_name="Alumno(s)")
    Foto = models.ImageField(upload_to=obtenerpath_foto_pariente, blank=True, null=True, verbose_name="Foto")

    def __unicode__(self):
        return u'%s %s %s' % (self.ApellidoPaterno, self.ApellidoMaterno, self.Nombres)

    def save(self):
        if not self.user_ptr_id:
            nombre = self.Nombres
            apellido = self.ApellidoPaterno
            contrasena = self.Email.lower()
            self.username = self.Email.lower()
            self.set_password(contrasena)
            self.first_name = self.Nombres
            self.last_name = self.ApellidoPaterno + " " + self.ApellidoMaterno
            self.email = self.Email
            return super(Pariente, self).save()

        else:
            self.first_name = self.Nombres
            self.last_name = self.ApellidoPaterno + " " + self.ApellidoMaterno
            self.email = self.Email
            self.username = self.Email
            return super(Pariente, self).save()

    class Meta:
        verbose_name = "Pariente Estudiante UDL"
        verbose_name_plural = "Parientes Estudiantes UDL"
        permissions = (("read_pariente", "Observar Parientes UDL"),)
