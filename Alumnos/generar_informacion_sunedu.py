# -*- coding: utf-8 -*-

from django.utils.encoding import smart_str

from Matriculas.models import MatriculaCiclo
from funciones import response_excel

# QUITA TILDES
rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
       'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
       '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' ', '-': ' ', '_': ' '}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


# CHOICES ESCUELAS, SEGUN FORMATO
ESCUELAS_PROFESIONALES = {
    'AT': 'ADM. TURISTICA',
    'AM': 'ADM. Y MARKETING',
    'IA': 'ING. AMBIENTAL',
    'IC': 'ING. COMERCIAL',
    'IS': 'ING. DE SISTEMAS'
}


def generar_informacion_sunedu(request):
    anio = '2016'
    semestre = 'I'
    periodo = 18
    filial = 1
    path_dir_carne = '/home/CARNE_ANR/%s-%s/' % (anio, semestre)
    codigo_universidad = '107'
    nombre_universidad = 'UNIVERSIDAD DE LAMBAYEQUE'

    matriculados = MatriculaCiclo.objects.filter(Periodo__id=periodo)
    listado = []
    for matriculado in matriculados:
        codigo = matriculado.Alumno.Codigo
        alumno = matriculado.Alumno
        carrera = str(matriculado.Alumno.Carrera)
        facultad = ''
        if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
            facultad = ' FACULTAD DE CIENCIAS DE INGENIERÍA'
        elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
            facultad = 'FACULTAD DE CIENCIAS DE ADMINISTRACIÓN'

        tipodocumento = alumno.DocIdentidad
        nrodocumento = alumno.NroDocumento
        ap_paterno = replace_all(smart_str(alumno.ApellidoPaterno), rep).upper().strip(" ")
        ap_materno = replace_all(smart_str(alumno.ApellidoMaterno), rep).upper().strip(" ")
        nombres = replace_all(smart_str(alumno.Nombres), rep).upper().strip(" ")
        anio_ingreso = alumno.AnioIngreso
        periodo_ingreso = ''
        if alumno.Semestre == 'I':
            periodo_ingreso = '01'
        elif alumno.Semestre == 'II':
            periodo_ingreso = '02'

        situacion_matricula = matriculado.Estado
        observaciones = matriculado.Observaciones

        lista = []
        lista.append(codigo_universidad)
        lista.append(nombre_universidad)
        lista.append(facultad)
        lista.append(carrera)
        lista.append('')
        lista.append('')
        lista.append(codigo)
        lista.append(tipodocumento)
        lista.append(nrodocumento)
        lista.append(ap_paterno)
        lista.append(ap_materno)
        lista.append(nombres)
        lista.append(anio_ingreso)
        lista.append(periodo_ingreso)
        lista.append(situacion_matricula)
        lista.append(observaciones)
        listado.append(lista)

    headers = ["COD. UNIV.", "UNIVERSIDAD DE LAMBAYEQUE", "FACULTAD", "CARRERA", "FILIAL", "UBIGEO FILIAR",
               "COD. ESTUDIANTE",
               "TIPO DE DOCUMENTO", "N° DE DOCUMENTO", "AP. PATERNO", "AP. MATERNO", "NOMBRES", "AÑO DE INGRESO",
               "PERIODO INGRESO", "SITUACION MATRICULA", "OBSERVACIONES MATRICULA"]
    titulo = 'REPORTE DE INFORMACIÓN DE ESTUDIANTES SUNEDU'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='informacion_SUNEDU')
