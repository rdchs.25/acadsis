# -*- coding: utf-8 -*-
from shutil import copyfile
import datetime

from Alumnos.models import Alumno
from Matriculas.models import MatriculaCiclo
from Pagos.models import PagoAlumno
from funciones import response_excel

# QUITA TILDES
rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
       'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
       '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' ', '-': ' '}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


# CHOICES ESCUELAS, SEGUN FORMATO
ESCUELAS_PROFESIONALES = {
    'AT': 'ADM. TURISTICA',
    'AM': 'ADM. Y MARKETING',
    'IA': 'ING. AMBIENTAL',
    'IC': 'ING. COMERCIAL',
    'IS': 'ING. DE SISTEMAS'
}

ESCUELAS_PROFESIONALES_ID = {
    'AT': '9463',
    'AM': '9465',
    'IA': '9464',
    'IC': '9461',
    'IS': '9462'
}

# cristopher cubas mechan
# todos

# CHOICES FACULTADES, SEGUN FORMATO
FACULTADES = {
    'AT': 'FACULTAD DE CIENCIAS ADMINISTRATIVAS',
    'AM': 'FACULTAD DE CIENCIAS ADMINISTRATIVAS',
    'IA': 'FACULTAD DE CIENCIAS DE INGENIERIA',
    'IC': 'FACULTAD DE CIENCIAS ADMINISTRATIVAS',
    'IS': 'FACULTAD DE CIENCIAS DE INGENIERIA'
}

FACULTADES_ID = {
    'AT': '1037',
    'AM': '1037',
    'IA': '1038',
    'IC': '1038',
    'IS': '1038'
}


def generar_datacarne_sunedu(request):
    anio = '2018'
    semestre = 'II'
    periodo = 25
    codigo_uni = '107'
    filial = 292
    path_dir_carne = '/home/CARNE_SUNEDU/%s-%s/' % (anio, semestre)

    alumnos = Alumno.objects.all()
    listado = []
    for alumno in alumnos:
        matricula = MatriculaCiclo.objects.filter(Periodo__id=periodo, Alumno=alumno, Estado='Matriculado')

        pagos = PagoAlumno.objects.filter(MatriculaCiclo=matricula, ConceptoPago__Descripcion__icontains="MATRICULA",
                                          Estado=True)
        if pagos.count() > 0:
            pass
        else:
            continue

        if matricula.count() == 0:
            continue

        if alumno.DocIdentidad != 'DNI':
            continue

        if alumno.Foto != '':
            pass
        else:
            continue

        lista = []
        lista.append(codigo_uni)
        codigoalumno = "020" + alumno.Codigo
        lista.append(codigoalumno)
        lista.append(alumno.ApellidoPaterno.upper())
        lista.append(alumno.ApellidoMaterno.upper())
        lista.append(alumno.Nombres.upper())
        fechanacimiento = datetime.date.strftime(alumno.Nacimiento, "%d/%m/%Y")
        lista.append(fechanacimiento)
        genero = ""
        if alumno.Sexo == 'M':
            genero = '1'
        elif alumno.Sexo == 'F':
            genero = '2'
        lista.append(genero)
        lista.append(alumno.Distrito.Codigoubigeo)
        lista.append(FACULTADES_ID[str(alumno.Carrera.Codigo)])
        lista.append("")
        lista.append(ESCUELAS_PROFESIONALES_ID[str(alumno.Carrera.Codigo)])
        lista.append("")
        lista.append("1")
        lista.append(alumno.NroDocumento)

        lista.append(alumno.AnioIngreso)
        if alumno.Semestre == "I":
            lista.append("01")
        elif alumno.Semestre == "II":
            lista.append("02")
        lista.append(filial)

        nuevo = ''
        if alumno.Foto != '':
            path_foto = alumno.Foto.name
            archivo_foto = path_foto.split('/')[2]
            extension_archivo = archivo_foto.split('.')[-1]
            nuevo_archivo = '107_%s%s' % (codigoalumno, alumno.NroDocumento)
            nuevo = alumno.Foto.path
            nuevadireccion = '%s%s.%s' % (path_dir_carne, nuevo_archivo, extension_archivo)
            try:
                copyfile(nuevo, nuevadireccion)
                # os.system('cp %s %s%s.%s' % (nuevo, path_dir_carne,nuevo_archivo,extension_archivo))
            except Exception as e:
                nuevo = 'error'

        lista.append(nuevo)
        listado.append(lista)

    headers = ["COD_UNIV", "COD_EST", "APEPAT", "APEMAT", "NOMBRE", "FAC_NOM-ESC_POS", "ABR_FAC-ABR_POS",
               "ESC_CARR-ESP_POS", "ABR_CARR-ABR_ESP_POS", "DOCU_TIP", "DOCU_NUM", "AÑO_ING", "PER_ING", "FIL", "d"]
    titulo = 'REPORTE DE CARNES SUNEDU'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='carnes_SUNEDU')


def shellquote(s):
    return "'" + s.replace("'", "'\\''") + "'"
