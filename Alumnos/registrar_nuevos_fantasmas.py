# -*- coding: utf-8 -*-

# imports reportlab

from Alumnos.models import Alumno

# imports para la paginacion

ID_PERIODO_MATRICULA = 15
codigos = ["150019L", "150016L"]
# inscritos = MatriculaCiclo.objects.filter(Periodo__id=ID_PERIODO_MATRICULA, Estado="Matriculado",
#                                           Alumno__Codigo__in=codigos)
inscritos = Alumno.objects.filter(Codigo__in=codigos)


def letra_codigo(codigo):
    """funcion para calcular la letra del codigo del estudiante"""
    suma = int(codigo[0]) * 2 + int(codigo[1]) * 3 + int(codigo[2]) * 4 + int(codigo[3]) * 5 + int(codigo[4]) * 6 + int(
        codigo[5]) * 7
    residuo = suma % 11
    if residuo == 0:
        return 'A'
    elif residuo == 1:
        return 'B'
    elif residuo == 2:
        return 'C'
    elif residuo == 3:
        return 'D'
    elif residuo == 4:
        return 'E'
    elif residuo == 5:
        return 'F'
    elif residuo == 6:
        return 'G'
    elif residuo == 7:
        return 'H'
    elif residuo == 8:
        return 'I'
    elif residuo == 9:
        return 'J'
    elif residuo == 10:
        return 'K'


def grabar_acadsis(anio, mod, alumnos, inicio=1):
    for al in alumnos:
        codigo = str(anio[2:4]) + mod + str(inicio).rjust(3).replace(" ", "0")
        codigo = codigo + letra_codigo(codigo)

        alumno_udl = Alumno.objects.get(Codigo=al.Codigo)
        print "Anterior: ", al.Codigo, "Nuevo: ", codigo
        alumno_udl.Codigo = codigo
        alumno_udl.save()

        # contrasena = al.Codigo
        # super(Alumno, alumno_udl).save(using = 'acadsis')
        # alumno_udl.set_password(contrasena)
        # add_grupo_alumnos(alumno_udl)

        # docs = AlumnoDocumento.objects.filter(Alumno = al)
        # for doc in docs:
        # doc_acadsis = AlumnoDocumento(Alumno_id=alumno_udl.id, Documento=doc.Documento, Numero=doc.Numero,
        #                               Imagen=doc.Imagen)
        # doc_acadsis.save(using = 'acadsis')

        inicio += 1


grabar_acadsis("2015", '0', inscritos, inicio=113)
