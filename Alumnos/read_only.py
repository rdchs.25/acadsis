# Create your views here.
# -*- coding: utf-8 -*-
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from Alumnos.models import Alumno
from Alumnos.models import MODALIDAD_CHOICES
from Carreras.models import Carrera
from Periodos.models import ANIO_CHOICES, SEMESTRE_CHOICES


def print_listado_excel(alumnos):
    from Alumnos.views import alumnos_excel
    return alumnos_excel(alumnos)


@csrf_protect
def ver_alumno(request):
    if request.user.is_authenticated() and request.user.has_perm('Alumnos.read_alumno'):
        if request.method == 'POST':
            action = request.POST['action']
            ingresantes = request.POST.getlist('_selected_action')
            all = request.POST['select_across']
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('m', '')
            alumnos = []

            if all == '0':
                ingresantes = request.POST.getlist('_selected_action')
            else:
                ingresantes = []
                ids_alumnos = Alumno.objects.all()
                if query1:
                    ids_alumnos1 = ids_alumnos.filter(Carrera__id=query1)
                else:
                    ids_alumnos1 = ids_alumnos

                if query2:
                    ids_alumnos2 = ids_alumnos1.filter(AnioIngreso=query2)
                else:
                    ids_alumnos2 = ids_alumnos1

                if query3:
                    ids_alumnos3 = ids_alumnos2.filter(Semestre=query3)
                else:
                    ids_alumnos3 = ids_alumnos2

                if query4:
                    ids_alumnos4 = ids_alumnos3.filter(Modalidad=query4)
                else:
                    ids_alumnos4 = ids_alumnos3

                for id_al in ids_alumnos4:
                    ingresantes.append(id_al.id)

            for al in ingresantes:
                alumno = Alumno.objects.get(id=al)
                alumnos.append(alumno)
            if action == "print_listado_excel":
                return print_listado_excel(alumnos)
            else:
                return HttpResponseRedirect('')
        else:
            query = request.GET.get('q', '')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('m', '')

            carreras = Carrera.objects.all()
            anios = []
            semestres = []
            modalidades = []

            for i in ANIO_CHOICES:
                anios.append(i[0])

            for s in SEMESTRE_CHOICES:
                semestres.append(s[0])

            for m in MODALIDAD_CHOICES:
                modalidades.append(m[0])

            results = Alumno.objects.all()

            if query1:
                results1 = results.filter(Carrera__id=query1)
            else:
                results1 = results

            if query2:
                results2 = results1.filter(AnioIngreso=query2)
            else:
                results2 = results1

            if query3:
                results3 = results2.filter(Semestre=query3)
            else:
                results3 = results2

            if query4:
                results4 = results3.filter(Modalidad=query4)
            else:
                results4 = results3

            if query:
                qset = (
                        Q(Nombres__icontains=query) |
                        Q(ApellidoPaterno__icontains=query) |
                        Q(ApellidoMaterno__icontains=query)
                )
                results5 = results4.filter(qset)
            else:
                results5 = results4

            n_alumnos = results5.count()
            paginator = Paginator(results5, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Alumnos/Lectura/ver_alumnos.html",
                                      {"results": results, "paginator": paginator, "query": query, "query1": query1,
                                       "query2": query2, "query3": query3, "query4": query4, "carreras": carreras,
                                       "anios": anios, "semestres": semestres, "modalidades": modalidades,
                                       "n_alumnos": n_alumnos, "user": request.user,
                                       "has_add_permission": request.user.has_perm('Alumnos.add_alumno')},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')
