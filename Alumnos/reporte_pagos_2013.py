# -*- coding: utf-8 -*-
import os
import sys

import xlwt

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

from django.utils.encoding import smart_str
from Pagos.models import DetallePago
from decimal import Decimal
from UDL.settings import MEDIA_ROOT
import datetime


def procesar_pagosrealizados(pagos):
    recaudado = 0
    moras = 0
    pagos_concepto = []
    i = 1
    for pago in pagos:
        codigo = pago.PagoAlumno.MatriculaCiclo.Alumno.Codigo
        apellidos_alumno = pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + \
                           pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoMaterno
        nombres_alumno = pago.PagoAlumno.MatriculaCiclo.Alumno.Nombres
        periodo_alumno = pago.PagoAlumno.MatriculaCiclo.Periodo.__unicode__()
        carrera_alumno = smart_str(pago.PagoAlumno.MatriculaCiclo.Alumno.Carrera)
        concepto = pago.PagoAlumno.ConceptoPago.Descripcion
        comprobante = pago.Comprobante.TipoComprobante.Serie.Numero + '-' + pago.Comprobante.Correlativo
        monto_pagar = pago.MontoDeuda
        monto_cancelado = pago.MontoCancelado
        fecha = pago.FechaPago.strftime("%d-%m-%Y")
        lugarpago = pago.Comprobante.LugarPago

        if pago.Mora:
            concepto = "Mora " + concepto
            moras += monto_cancelado
        else:
            recaudado += monto_cancelado

        pagos_concepto.append(
            [i, codigo, apellidos_alumno, nombres_alumno, periodo_alumno, carrera_alumno, concepto, monto_pagar,
             monto_cancelado, comprobante, fecha, lugarpago])
        i += 1

    total = recaudado + moras

    return [pagos_concepto, recaudado, moras, total]


def generar_excel(data, headers=None, extra_data=None, ):
    ruta_archivo = str(MEDIA_ROOT)
    nombre_archivo = "reporte_2013.xls"

    style = xlwt.XFStyle()
    styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
              'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
              'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
              'default': xlwt.easyxf("borders: top thin, bottom thin, left thin,right thin;"),
              'headers': xlwt.easyxf(
                  'pattern: pattern solid, fore_colour yellow;' "borders: top thin, bottom thin, left thin,right thin;"),
              'extra_data': xlwt.Style.default_style}

    book = xlwt.Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 1')

    borders = xlwt.Borders()
    borders.left = xlwt.Borders.THIN
    borders.right = xlwt.Borders.THIN
    borders.top = xlwt.Borders.THIN
    borders.bottom = xlwt.Borders.THIN

    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = 0xB4030D

    i = 7
    j = 10
    if extra_data is None:
        for x, extra in enumerate(extra_data):
            sheet.write(x + 7, 0, extra, style=styles['extra_data'])
            i = x + 8

    if headers is None:
        style.borders = borders
        style.pattern = pattern
        for x, head in enumerate(headers):
            sheet.write(i, x, head, style=styles['headers'])
        i += 1

    for rowx, row in enumerate(data):
        for colx, value in enumerate(row):
            if isinstance(value, datetime.datetime):
                cell_style = styles['datetime']
            elif isinstance(value, datetime.date):
                cell_style = styles['date']
            elif isinstance(value, datetime.time):
                cell_style = styles['time']
            else:
                cell_style = styles['default']
            sheet.write(rowx + i, colx, value, style=cell_style)
        j = rowx + i + 3

    hoy = datetime.date.today()
    fecha = 'Chiclayo, %s' % hoy.strftime("%d %b %Y")
    sheet.write(j, 0, fecha, style=styles['extra_data'])

    full_path = ruta_archivo + nombre_archivo
    book.save(full_path)


desde = "2013-I"
hasta = "2013-II"
pagos = DetallePago.objects.filter(PagoAlumno__MatriculaCiclo__Estado="Matriculado",
                                   PagoAlumno__MatriculaCiclo__Periodo__id__in=[9, 10]).exclude(
    MontoCancelado=Decimal('0.00')).order_by('FechaPago')

pagos_diarios, recaudado, moras, total = procesar_pagosrealizados(pagos)
headers = ['Nº', 'Código', 'Apellidos', 'Nombres', 'Periodo', 'Carrera', 'Concepto Pago', 'Monto Deuda', 'Monto Pagado',
           'Comprobante', 'Fecha Pago', 'Lugar']
extra_data = ['', 'Pagos diarios', 'Desde: ' + desde, 'Hasta: ' + hasta, 'Lugar de Pago: ' + "Banco y Caja",
              'Recaudado (sin moras): ' + str(recaudado), 'Recaudado (solo moras): ' + str(moras),
              'Total recaudado: ' + str(total), '']

generar_excel(pagos_diarios, headers, extra_data)
