# -*- coding: utf-8 -*-

from django.contrib.auth import login as auth_login
from django.contrib.auth import logout, authenticate
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from Alumnos.forms import LoginFormActualizarDatos, FormActualizarDatos
from Alumnos.models import Parentesco, Alumno


@csrf_protect
def logout_actualizar_datos(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('../../actualizacion_datos/')
    else:
        return HttpResponseRedirect('../../actualizacion_datos/')


@csrf_protect
@never_cache
def login_actualizar_datos(request):
    if request.method == 'POST':
        form = LoginFormActualizarDatos(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['Usuario']
            passwd = form.cleaned_data['Password']
            user = authenticate(username=usuario, password=passwd)
            if user is not None:
                auth_login(request, user)
                var = request.GET.get('q', 'm')
                try:
                    estudiante = Alumno.objects.get(user_ptr=request.user.id)
                except Alumno.DoesNotExist:
                    mensaje = "Usted no es un estudiante, verifique por favor."
                    links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                    return render_to_response("Alumnos/ActualizacionDatos/mensaje.html",
                                              {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
                return HttpResponseRedirect('/actualizacion_datos/registrar')
    else:
        form = LoginFormActualizarDatos()

    return render_to_response("Alumnos/ActualizacionDatos/login.html", {"form": form},
                              context_instance=RequestContext(request))


@csrf_exempt
def registrar_actualizar_datos(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            try:
                estudiante = Alumno.objects.get(user_ptr=request.user.id)
            except Alumno.DoesNotExist:
                mensaje = "Usted no es un estudiante, verifique por favor."
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                return render_to_response("Alumnos/ActualizacionDatos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links),
                                           "user": request.user})

            form = FormActualizarDatos(request.POST)
            if form.is_valid():
                apellido_materno = form.cleaned_data['ApellidoPaterno']
                apellido_materno = form.cleaned_data['ApellidoMaterno']
                nombres = form.cleaned_data['Nombres']
                fechanacimiento = form.cleaned_data['Nacimiento']
                lugarnacimiento = form.cleaned_data['LugarNacimiento']
                tipodocumento = 'DNI'
                nrodocumento = form.cleaned_data['NroDocumento']
                email = form.cleaned_data['Email']
                telefono = form.cleaned_data['Telefono']
                celular = form.cleaned_data['Celular']
                estudiante.ApellidoPaterno = apellido_materno
                estudiante.ApellidoMaterno = apellido_materno
                estudiante.Nombres = nombres
                estudiante.Nacimiento = fechanacimiento
                estudiante.LugarNacimiento = lugarnacimiento
                estudiante.DocIdentidad = tipodocumento
                estudiante.NroDocumento = nrodocumento
                estudiante.Email = email
                estudiante.Telefono = telefono
                estudiante.Celular = celular
                estudiante.save()

                parentesco = Parentesco.objects.get(id=estudiante.Parentesco_id)
                parentesco.ApePatPadre = form.cleaned_data['ApePatPadre']
                parentesco.ApeMatPadre = form.cleaned_data['ApeMatPadre']
                parentesco.NombresPadre = form.cleaned_data['NombresPadre']
                parentesco.DNIPadre = form.cleaned_data['DNIPadre']
                parentesco.VivePadre = form.cleaned_data['VivePadre']
                parentesco.DirPadre = form.cleaned_data['DirPadre']
                parentesco.TelfPadre = form.cleaned_data['TelfPadre']
                parentesco.CelPadre = form.cleaned_data['CelPadre']
                parentesco.ApePatMadre = form.cleaned_data['ApePatMadre']
                parentesco.ApeMatMadre = form.cleaned_data['ApeMatMadre']
                parentesco.NombresMadre = form.cleaned_data['NombresMadre']
                parentesco.DNIMadre = form.cleaned_data['DNIMadre']
                parentesco.ViveMadre = form.cleaned_data['ViveMadre']
                parentesco.DirMadre = form.cleaned_data['DirMadre']
                parentesco.TelfMadre = form.cleaned_data['TelfMadre']
                parentesco.CelMadre = form.cleaned_data['CelMadre']
                parentesco.Responsable = form.cleaned_data['Responsable']
                parentesco.Parentesco = form.cleaned_data['Parentesco']
                parentesco.ApePatTutor = form.cleaned_data['ApePatTutor']
                parentesco.ApeMatTutor = form.cleaned_data['ApeMatTutor']
                parentesco.NombresTutor = form.cleaned_data['NombresTutor']
                parentesco.DNITutor = form.cleaned_data['DNITutor']
                parentesco.DirTutor = form.cleaned_data['DirTutor']
                parentesco.TelfTutor = form.cleaned_data['TelfTutor']
                parentesco.CelTutor = form.cleaned_data['CelTutor']
                parentesco.save()

                estudiante.Actualizodatos = True
                estudiante.save()

                mensaje = "Cambios coorectos."
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                return render_to_response("Alumnos/ActualizacionDatos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = FormActualizarDatos()
            try:
                estudiante = Alumno.objects.get(user_ptr=request.user.id)
            except Alumno.DoesNotExist:
                mensaje = "Usted no es un estudiante, verifique por favor."
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                return render_to_response("Alumnos/ActualizacionDatos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links),
                                           "user": request.user})
        return render_to_response("Alumnos/ActualizacionDatos/profile_actualizar_datos.html",
                                  {"form": form, "user": request.user, "estudiante": estudiante},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/actualizacion_datos')
