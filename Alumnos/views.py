# -*- coding: utf-8 -*-
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from reportlab.lib.units import cm
# imports reportlab
from reportlab.pdfgen import canvas

from Alumnos.forms import ArchivoForm
from Alumnos.models import Alumno
from funciones import response_excel


# Metodos para imprimir Datos del Alumno

def mostrarfoto(request, id_alumno):
    if request.user.is_authenticated():
        try:
            alumno = Alumno.objects.get(id=id_alumno)
            return render_to_response('Alumnos/Foto.html', {"alumno": alumno}, context_instance=RequestContext(request))
        except ValueError:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')


@csrf_protect
def generar_carnet(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = ArchivoForm(request.POST, request.FILES)
            if form.is_valid():
                archivo = form.cleaned_data['Archivo']

                response = HttpResponse(content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename=carne_estudiante.pdf'
                p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))
                matriculados1 = []
                for mat1 in archivo:
                    line = mat1.strip('\n')
                    mat1 = Alumno.objects.get(Codigo=line)
                    matriculados1.append(mat1)
                o = len(matriculados1)
                imagen_carne = settings.MEDIA_ROOT + 'carne_estudiante.jpg'
                for m in range(0, o, 5):
                    alumnos = matriculados1[0 + m:5 + m]
                    i = 705
                    p.drawImage(imagen_carne, 0, 0, width=595, height=842)
                    for mat in alumnos:
                        p.setFont("Helvetica-Bold", 8)
                        codigo = mat.Codigo
                        apellidos = mat.ApellidoPaterno + ' ' + mat.ApellidoMaterno
                        nombres = mat.Nombres
                        carrera = mat.Carrera.Carrera
                        if mat.Foto != "":
                            imagen_alumno = settings.MEDIA_ROOT + '%s' % str(mat.Foto)
                        else:
                            imagen_alumno = settings.MEDIA_ROOT + 'avatar.jpg'
                        p.drawString(15, i, 'CÓDIGO:')
                        p.drawString(55, i, codigo.upper())
                        p.drawString(15, i - 20, apellidos.upper())
                        p.drawString(15, i - 40, nombres.upper())
                        p.drawString(15, i - 60, carrera.upper())
                        p.setStrokeColorRGB(0, 0, 0)
                        p.setFillColorRGB(1, 1, 1)
                        p.roundRect(150, i - 65, 80, 80, 10, stroke=1, fill=1)
                        p.drawImage(imagen_alumno, 155, i - 58, width=70, height=70)
                        i = i - 155
                        p.setFillColorRGB(0, 0, 0)
                    p.showPage()
                    p.save()
                return response
        else:
            form = ArchivoForm()

        return render_to_response('Alumnos/form_generar_carnet.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect("../../../")


def alumnos_excel(queryset):
    listado = []
    for obj in queryset:
        codigo = obj.Codigo
        apellido_paterno = obj.ApellidoPaterno
        apellido_materno = obj.ApellidoMaterno
        nombres = obj.Nombres
        nacimiento = obj.Nacimiento
        sexo = obj.Sexo
        doc_identidad = obj.DocIdentidad
        nro_documento = obj.NroDocumento
        departamento = obj.Provincia.Departamento.Departamento
        provincia = obj.Provincia.Provincia
        distrito = obj.Distrito.Distrito
        direccion = obj.Direccion
        urbanizacion = obj.Urbanizacion
        email = obj.Email
        telefono = obj.Telefono
        celular = obj.Celular
        carrera = obj.Carrera.__unicode__()
        anioingreso = obj.AnioIngreso
        semestre = obj.Semestre
        modalidad = obj.Modalidad
        colegio = obj.Colegio.__unicode__()
        tipo_colegio = obj.Colegio.Tipo
        provincia_colegio = obj.Colegio.Provincia.Provincia
        distrito_colegio = obj.Colegio.Distrito
        egresocolegio = obj.AnioEgreso
        tutor = obj.Parentesco.__unicode__()
        obs = obj.Observaciones
        if obj.Foto != '':
            foto = 'Si'
        else:
            foto = 'No'
        if obj.Egreso:
            egreso = 'Si'
            anioegresado = obj.AnioEgresado
            semestreegresado = obj.SemestreEgresado
        else:
            egreso = 'No'
            anioegresado = ''
            semestreegresado = ''

        listado.append(
            [codigo, apellido_paterno, apellido_materno, nombres, nacimiento, sexo, doc_identidad, nro_documento,
             departamento, provincia, distrito, direccion, urbanizacion, email, telefono, celular, carrera, anioingreso,
             semestre, modalidad, colegio, tipo_colegio, provincia_colegio, distrito_colegio, egresocolegio, tutor,
             foto, obs, egreso, anioegresado, semestreegresado])
    headers = ['Código', 'Apellido Paterno', 'Apellido Materno', 'Nombres', 'Nacimiento', 'Sexo', 'Doc. Ident',
               'Nro. Doc', 'Departamento', 'Provincia', 'Distrito', 'Direccion', 'Urb.', 'Email', 'Telefono', 'Celular',
               'Carrera', 'Anio Ingreso', 'Semestre', 'Modalidad', 'Colegio', 'Tipo Colegio', 'Prov Colegio',
               'Distrito Colegio', 'Egreso Colegio', 'Tutor', 'Foto', 'Observaciones', 'Egreso', 'Anio Egresado',
               'Semestre Egresado']
    titulo = 'Listado de Ingresantes UDL'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='ingresantes')
