# -*- coding: utf-8 -*-
from django.db import models

# from UDL.Periodos.models import PeriodoCurso
from Cursos.models import PeriodoCurso

# Create your models here.
DIA_CHOICES = (
    ('Lunes', 'Lunes'),
    ('Martes', 'Martes'),
    ('Miercoles', 'Miercoles'),
    ('Jueves', 'Jueves'),
    ('Viernes', 'Viernes'),
    ('Sabado', 'Sabado'),
    ('Domingo', 'Domingo'),
)


class Horario(models.Model):
    PeriodoCurso = models.ForeignKey(PeriodoCurso, verbose_name="Curso")
    Dia = models.CharField("Día", max_length=10, choices=DIA_CHOICES)
    HoraInicio = models.TimeField()
    HoraFin = models.TimeField()
    Seccion = models.CharField("Sección", max_length=50)

    def __unicode__(self):
        return u'%s - %s (%s-%s)' % (self.PeriodoCurso, self.Dia, self.HoraInicio, self.HoraFin)

    def Etiqueta(self):
        return u'%s (%s - %s)' % (self.Dia, self.HoraInicio, self.HoraFin)

    class Meta:
        verbose_name = "Horario"
        verbose_name_plural = "Horarios"
        unique_together = (("PeriodoCurso", "Dia", "HoraInicio", "HoraFin", "Seccion",),)
        permissions = (("read_horario", "Observar Configuracion de Horarios"),)
