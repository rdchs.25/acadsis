# -*- coding: utf-8 -*-

from django import forms
from django.conf import settings
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from Cursos.models import PeriodoCurso
from Horarios.models import Horario
from Horarios.widgets import SelectTimeWidget
from Notas.models import Nota
from Periodos.models import Periodo

# imports para la paginacion

SECCION_CHOICES = (
    ('Aula 01', 'Aula 01'),
    ('Aula 02', 'Aula 02'),
    ('Aula 03', 'Aula 03'),
    ('Aula 04', 'Aula 04'),
    ('Aula 05', 'Aula 05'),
    ('Aula 06', 'Aula 06'),
    ('Aula 07', 'Aula 07'),
    ('Aula 08', 'Aula 08'),
    ('Aula 09', 'Aula 09'),
    ('Aula 10', 'Aula 10'),
    ('Aula 11', 'Aula 11'),
    ('Aula 12', 'Aula 12'),
    ('Aula 13', 'Aula 13'),
    ('Aula 14', 'Aula 14'),
    ('Aula 15', 'Aula 15'),
    ('Aula 16', 'Aula 16'),
    ('Aula 17', 'Aula 17'),
    ('Aula 18', 'Aula 18'),
    ('Aula 19', 'Aula 19'),
    ('Aula 20', 'Aula 20'),
    ('Aula 21', 'Aula 21'),
    ('Aula 22', 'Aula 22'),
    ('Aula 23', 'Aula 23'),
    ('Aula 24', 'Aula 24'),
    ('Aula 25', 'Aula 25'),
    ('Lab. Inf. 01', 'Lab. Inf. 01'),
    ('Lab. Inf. 02', 'Lab. Inf. 02'),
    ('Lab. Inf. 03', 'Lab. Inf. 03'),
    ('Lab. Multi. 01', 'Lab. Multi. 01'),
    ('Lab. Multi. 02', 'Lab. Multi. 02'),
)


def horarios_notas_curso(request, periodocurso_id):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario') or request.user.has_perm(
            'Horarios.change_horario') or request.user.has_perm('Horarios.read_horario') or request.user.has_perm(
        'Notas.add_nota') or request.user.has_perm('Notas.change_nota') or request.user.has_perm('Notas.read_nota'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
            horarios = Horario.objects.filter(PeriodoCurso=periodocurso_id)
            notas = Nota.objects.filter(PeriodoCurso=periodocurso_id).order_by('Orden')
            if notas.filter(Nivel='0').count() != 0:
                nivel_cero = True
            else:
                nivel_cero = False
        except PeriodoCurso.DoesNotExist:
            raise Http404

        return render_to_response("Periodos/contenido_curso.html",
                                  {"periodocurso": periodocurso, "horarios": horarios, "notas": notas,
                                   "nivel_cero": nivel_cero, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


# agregar grupos horario a un curso de carrera en un periodo determinado
class HorarioForm(forms.ModelForm):
    HoraInicio = forms.TimeField(widget=SelectTimeWidget(), label='Inicio')
    HoraFin = forms.TimeField(widget=SelectTimeWidget(), label='Fin')
    PeriodoCurso1 = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    Seccion = forms.ChoiceField(choices=SECCION_CHOICES, required=True, label='Aula')

    class Meta:
        model = Horario
        fields = ('Dia', 'HoraInicio', 'HoraFin', 'Seccion',)


@csrf_protect
def agregar_horario(request, periodocurso_id):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
        except PeriodoCurso.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = HorarioForm(request.POST)
            if form.is_valid():
                dia = form.cleaned_data['Dia']
                horainicio = form.cleaned_data['HoraInicio']
                horafin = form.cleaned_data['HoraFin']
                seccion = form.cleaned_data['Seccion']

                grabar_horario = Horario(PeriodoCurso_id=periodocurso_id, Dia=dia, HoraInicio=horainicio,
                                         HoraFin=horafin, Seccion=seccion)
                grabar_horario.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Periodos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = HorarioForm(initial={"PeriodoCurso1": periodocurso_id})

        return render_to_response("Horarios/agregar_horario.html",
                                  {"form": form, "user": request.user, "periodocurso": periodocurso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


# editar grupos horario a un curso de carrera en un periodo determinado
@csrf_protect
def editar_horario(request, horario_id):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        try:
            horario = Horario.objects.get(id=horario_id)
        except Horario.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = HorarioForm(request.POST)
            if form.is_valid():
                dia = form.cleaned_data['Dia']
                horainicio = form.cleaned_data['HoraInicio']
                horafin = form.cleaned_data['HoraFin']
                seccion = form.cleaned_data['Seccion']

                horario.Dia = dia
                horario.HoraInicio = horainicio
                horario.HoraFin = horafin
                horario.Seccion = seccion
                horario.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Periodos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = HorarioForm(
                initial={"PeriodoCurso1": horario.PeriodoCurso.id, "Dia": horario.Dia, "HoraInicio": horario.HoraInicio,
                         "HoraFin": horario.HoraFin, "Seccion": horario.Seccion})

        return render_to_response("Horarios/editar_horario.html",
                                  {"form": form, "user": request.user, "horario": horario},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


class IndexHorarioForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


@csrf_protect
def index_horario(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario') or request.user.has_perm(
            'Horarios.add_horario') or request.user.has_perm('Horarios.read_horario'):
        if request.method == 'POST':
            form = IndexHorarioForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?p=" + str(periodo.id))
        else:
            form = IndexHorarioForm()
        return render_to_response('Horarios/Horario/index_horario.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


# Gestion de horarios Nueva Interfaz
def app_horarios(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        id_periodo = request.GET.get('p', '')
        try:
            periodo = Periodo.objects.get(id=id_periodo)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        return render_to_response("Horarios/interfaz_horarios.html", {"user": request.user, "periodo": periodo},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def buscar_horario_aula(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        consulta = str(request.POST['consulta'])
        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        n_aula = consulta.split('-')[0]
        carrera = consulta.split('-')[1]
        ciclo = consulta.split('-')[2]
        if carrera != '' and ciclo != '':
            horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoCurso__Curso__Carrera__Codigo=carrera,
                                              PeriodoCurso__Curso__Ciclo=ciclo)
        elif carrera != '':
            horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoCurso__Curso__Carrera__Codigo=carrera)
        else:
            horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Seccion=n_aula)

        cadena = ''
        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}

        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoCurso.Curso.Carrera.Codigo, horario.PeriodoCurso.Curso.Nombre,
                horario.PeriodoCurso.Docente.username, horario.PeriodoCurso.id, dia, inicio, fin,
                horario.PeriodoCurso.Curso.Ciclo, horario.id)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def buscar_periodocurso(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario'):
        consulta = str(request.POST['consulta'])
        carrera = consulta.split('-')[0]
        ciclo = consulta.split('-')[1]
        cadena = ''
        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        if carrera != '' and ciclo != '':
            periodocursos = PeriodoCurso.objects.filter(Periodo__id=ID_PERIODO, Curso__Carrera__Codigo=carrera,
                                                        Curso__Ciclo=ciclo)
        elif carrera == '':
            cadena += '0|Primero elija una carrera|id-nombre&&'
            return HttpResponse(cadena)
        elif ciclo == '':
            cadena += '0|Primero elija un ciclo|id-nombre&&'
            return HttpResponse(cadena)
        else:
            cadena += '0|Primero elija una carrera y ciclo|id-nombre&&'
            return HttpResponse(cadena)

        cadena += '1|'
        for percur in periodocursos:
            id_curso = percur.id
            nombre = percur.Curso.Nombre
            grupo = percur.Grupo
            cadena += '%s-%s&&' % (id_curso, grupo + ' ' + nombre)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def traer_periodocurso(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        consulta = str(request.POST['consulta'])
        idperiodocurso = consulta
        cadena = ''
        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        try:
            periodocurso = PeriodoCurso.objects.get(id=idperiodocurso)
            carrera = periodocurso.Curso.Carrera.Carrera
            ciclo = periodocurso.Curso.Ciclo
            curso = periodocurso.Grupo + ' ' + periodocurso.Curso.Nombre
            cadena += '%s-%s-%s' % (carrera, ciclo, curso)
            return HttpResponse(cadena)
        except PeriodoCurso.DoesNotExist:
            pass
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def guardar_horario(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario'):
        consulta = str(request.POST['consulta'])
        id_periodocurso = consulta.split('-')[0]
        dia = consulta.split('-')[1]
        h_inicio = consulta.split('-')[2]
        h_fin = consulta.split('-')[3]
        n_aula = consulta.split('-')[4]
        dias = {'LU': 'Lunes', 'MA': 'Martes', 'MI': 'Miercoles', 'JU': 'Jueves', 'VI': 'Viernes', 'SA': 'Sabado',
                'DO': 'Domingo'}

        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

            # inicio guardar
        from datetime import time
        hora_inicio = h_inicio.split(':')
        hora_fin = h_fin.split(':')

        inicio = time(int(hora_inicio[0]), int(hora_inicio[1]))
        fin = time(int(hora_fin[0]), int(hora_fin[1]))

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            return HttpResponse('0|El Curso no existe')

        horarios_dia = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Dia=dias[dia], Seccion=n_aula)
        for hor in horarios_dia:
            if inicio >= hor.HoraInicio and fin <= hor.HoraFin:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con ciclo %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoCurso.Curso.Ciclo), hor.PeriodoCurso.Curso.Carrera,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))
                # return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (n_aula, dias[dia], inicio.strftime('%H:%M'),fin.strftime('%H:%M')))
            elif inicio >= hor.HoraInicio and inicio < hor.HoraFin:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con ciclo %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoCurso.Curso.Ciclo), hor.PeriodoCurso.Curso.Carrera,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))
            elif fin <= hor.HoraFin and fin > hor.HoraInicio:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con ciclo %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoCurso.Curso.Ciclo), hor.PeriodoCurso.Curso.Carrera,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))
            elif inicio <= hor.HoraInicio and fin >= hor.HoraFin:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con ciclo %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoCurso.Curso.Ciclo), hor.PeriodoCurso.Curso.Carrera,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))

        crear_horario = Horario(PeriodoCurso=periodocurso, Dia=dias[dia], HoraInicio=inicio, HoraFin=fin,
                                Seccion=n_aula)
        crear_horario.save()
        # fin guardar horario

        carrera = consulta.split('-')[5]
        ciclo = consulta.split('-')[6]

        horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                          PeriodoCurso__Curso__Carrera__Codigo=carrera,
                                          PeriodoCurso__Curso__Ciclo=ciclo)

        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        cadena = ''
        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoCurso.Curso.Carrera.Codigo, horario.PeriodoCurso.Curso.Nombre,
                horario.PeriodoCurso.Docente.username, horario.PeriodoCurso.id, dia, inicio, fin,
                horario.PeriodoCurso.Curso.Ciclo, horario.id)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def modificar_horario(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        consulta = str(request.POST['consulta'])
        id_horario = consulta.split('-')[0]
        dia = consulta.split('-')[1]
        h_inicio = consulta.split('-')[2]
        h_fin = consulta.split('-')[3]
        n_aula = consulta.split('-')[4]
        dias = {'LU': 'Lunes', 'MA': 'Martes', 'MI': 'Miercoles', 'JU': 'Jueves', 'VI': 'Viernes', 'SA': 'Sabado',
                'DO': 'Domingo'}

        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        # inicio modificar
        from datetime import time
        hora_inicio = h_inicio.split(':')
        hora_fin = h_fin.split(':')

        inicio = time(int(hora_inicio[0]), int(hora_inicio[1]))
        fin = time(int(hora_fin[0]), int(hora_fin[1]))

        try:
            horario = Horario.objects.get(id=id_horario)
        except Horario.DoesNotExist:
            return HttpResponse('0|El Horario no existe')

        horarios_dia = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Dia=dias[dia], Seccion=n_aula)
        for hor in horarios_dia:
            if int(hor.id) != int(id_horario):
                if inicio >= hor.HoraInicio and fin <= hor.HoraFin:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))
                elif inicio >= hor.HoraInicio and inicio < hor.HoraFin:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))
                elif fin <= hor.HoraFin and fin > hor.HoraInicio:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))
                elif inicio <= hor.HoraInicio and fin >= hor.HoraFin:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))

        horario.Dia = dias[dia]
        horario.HoraInicio = inicio
        horario.HoraFin = fin
        horario.save()
        # fin modificar horario

        carrera = consulta.split('-')[5]
        ciclo = consulta.split('-')[6]

        horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                          PeriodoCurso__Curso__Carrera__Codigo=carrera,
                                          PeriodoCurso__Curso__Ciclo=ciclo)

        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        cadena = ''
        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoCurso.Curso.Carrera.Codigo, horario.PeriodoCurso.Curso.Nombre,
                horario.PeriodoCurso.Docente.username, horario.PeriodoCurso.id, dia, inicio, fin,
                horario.PeriodoCurso.Curso.Ciclo, horario.id)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def eliminar_horario(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.delete_horario'):
        consulta = str(request.POST['consulta'])
        id_horario = consulta.split('-')[0]
        n_aula = consulta.split('-')[1]

        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        # inicio eliminar
        try:
            horario = Horario.objects.get(id=id_horario)
        except Horario.DoesNotExist:
            return HttpResponse('0|El Horario no existe')

        horario.delete()

        # fin eliminar horario

        carrera = consulta.split('-')[2]
        ciclo = consulta.split('-')[3]

        horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                          PeriodoCurso__Curso__Carrera__Codigo=carrera,
                                          PeriodoCurso__Curso__Ciclo=ciclo)

        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        cadena = ''
        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoCurso.Curso.Carrera.Codigo, horario.PeriodoCurso.Curso.Nombre,
                horario.PeriodoCurso.Docente.username, horario.PeriodoCurso.id, dia, inicio, fin,
                horario.PeriodoCurso.Curso.Ciclo, horario.id)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


# OK
def descargar_horarios2(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        from xlrd import open_workbook
        from xlutils.copy import copy
        from xlwt import XFStyle, Borders, Font, Alignment

        # Estilos de celda registros
        borders_registros = Borders()
        borders_registros.left = Borders.THIN
        borders_registros.right = Borders.THIN
        borders_registros.top = Borders.THIN
        borders_registros.bottom = Borders.THIN

        alignment_registros = Alignment()
        alignment_registros.horz = Alignment.HORZ_CENTER
        alignment_registros.vert = Alignment.VERT_JUSTIFIED

        alignment_celda_horarios = Alignment()
        alignment_celda_horarios.horz = Alignment.HORZ_CENTER
        alignment_celda_horarios.vert = Alignment.VERT_CENTER

        font_h1 = Font()
        font_h1.bold = True
        font_h1.height = 0x0140  # 18

        font_h2 = Font()
        font_h2.bold = True
        font_h2.height = 0x00F0  # 12

        font_h6 = Font()
        font_h6.height = 0x00B4  # 9

        style_registros = XFStyle()
        style_registros.borders = borders_registros
        style_registros.alignment = alignment_registros
        style_registros.font = font_h6

        style_titulo = XFStyle()
        style_titulo.borders = borders_registros
        style_titulo.alignment = alignment_registros
        style_titulo.font = font_h1

        style_horas = XFStyle()
        style_horas.alignment = alignment_registros
        style_horas.borders = borders_registros
        style_horas.font = font_h2

        style_celda_horarios = XFStyle()
        style_celda_horarios.alignment = alignment_celda_horarios
        style_celda_horarios.borders = borders_registros
        style_celda_horarios.font = font_h2

        style_celda_curso = XFStyle()
        style_celda_curso.alignment = alignment_celda_horarios
        style_celda_curso.borders = borders_registros
        style_celda_curso.font = font_h6

        rb = open_workbook(settings.MEDIA_ROOT + u'formato_horarios.xls', formatting_info=True)
        wb = copy(rb)

        carreras = ['AM', 'AT', 'IA', 'IC', 'IS']
        carreras_disponibles = {'AM': 'Administracion y Marketing', 'IC': 'Ingenieria Comercial',
                                'IA': 'Ingenieria Ambiental', 'AT': 'Administracion Turistica',
                                'IS': 'Ingenieria de Sistemas'}
        ciclo_letras = {'01': '1er', '02': '2do', '03': '3er', '04': '4to', '05': '5to', '06': '6to', '07': '7mo',
                        '08': '8vo', '09': '9no', '10': '10mo'}
        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        dias_disponibles2 = ['LU', 'MA', 'MI', 'JU', 'VI', 'SA', 'DO']
        horarios_disponibles = ["08:00", "08:50", "09:40", "10:30", "10:50", "11:40", "12:30", "13:20", "14:10",
                                "15:00", "15:50", "16:40", "17:30", "17:50", "18:40", "19:30", "20:20", "21:10",
                                "22:00"]
        # horarios_disponibles = ["08:00","08:50","09:40","10:30","11:40","12:30","13:20","14:10","15:00","15:50","16:40","17:30","17:50","18:40","19:30","20:20", "21:10"]

        # Dimensiones GridHorarios
        NUM_CELDAS_TITULO = 3
        TAM_GRID_FILAS = 19
        TAM_GRID_COLUMNAS = 6
        OFFSET_SUP_GRID = NUM_CELDAS_TITULO + 1
        POSICION_GRID = NUM_CELDAS_TITULO + TAM_GRID_FILAS  # 8 + 3

        ID_PERIODO = request.GET.get('p', '')
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        contador_hoja = 0
        for carrera in carreras:
            wsheet = wb.get_sheet(contador_hoja)

            # Añadimos Hoja con el nombre de la carrera
            secciones = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO,
                                               PeriodoCurso__Curso__Carrera__Codigo=carrera).order_by(
                'PeriodoCurso__Curso__Ciclo').values("Seccion").distinct()

            # Dibujamos en excel
            fGrid = 1
            cGrid = 1
            lista_secciones = []

            for seccion in secciones:

                # Horarios por Periodo y Carrera
                ciclos = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO,
                                                PeriodoCurso__Curso__Carrera__Codigo=carrera,
                                                Seccion=seccion['Seccion']).order_by(
                    'PeriodoCurso__Curso__Ciclo').values("PeriodoCurso__Curso__Ciclo").distinct()

                for ciclo in ciclos:
                    elemento = [ciclo['PeriodoCurso__Curso__Ciclo'], seccion['Seccion']]
                    if not elemento in lista_secciones:
                        lista_secciones.append(elemento)
                        horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=ID_PERIODO,
                                                          PeriodoCurso__Curso__Carrera__Codigo=carrera,
                                                          Seccion=seccion['Seccion'], PeriodoCurso__Curso__Ciclo=ciclo[
                                'PeriodoCurso__Curso__Ciclo']).order_by('PeriodoCurso__Curso__Ciclo', 'Seccion')
                        # Determinamos el TURNO de cada horario para mostrar solo las filas correspondientes del mismo.
                        turno = 'M'
                        n_manana = 0
                        n_tarde = 0
                        for horario in horarios:
                            # if int(horario.PeriodoCurso.Curso.Ciclo) == int(ciclo['PeriodoCurso__Curso__Ciclo']):
                            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
                            # print inicio, " // ", horario
                            # fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
                            if horarios_disponibles.index(inicio) > 6:
                                n_tarde += 1
                            else:
                                n_manana += 1

                        if n_tarde > n_manana:
                            turno = 'T'
                        else:
                            turno = 'M'

                        # Dibujamos Titulo del Horario
                        wsheet.write(fGrid, cGrid, unicode(carreras_disponibles[carrera]) + " - " + unicode(
                            ciclo_letras[ciclo['PeriodoCurso__Curso__Ciclo']]) + " Ciclo - " + seccion['Seccion'],
                                     style_titulo)
                        # # Dibujamos esquina Hora/Día :p
                        # wsheet.write( fGrid + 2, cGrid, "Hora / Dia" )

                        # # Dibujamos Horarios por defecto
                        rangohs = range(0, 18)

                        f = fGrid + 3
                        c = cGrid
                        for hd in rangohs:
                            wsheet.write(f, c, str(horarios_disponibles[hd] + " - " + horarios_disponibles[hd + 1]),
                                         style_celda_horarios)
                            # wsheet.col(c).height = 1000
                            f += 1

                        # Colocamos los horarios en sus respectivas posiciones
                        f = fGrid + 3
                        c = cGrid + 1

                        datos = ""
                        # Generamos cadena horario
                        for horario in horarios:
                            # if int(horario.PeriodoCurso.Curso.Ciclo) == int(ciclo['PeriodoCurso__Curso__Ciclo']):
                            # Todos los horarios
                            dia = dias[horario.Dia]
                            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
                            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]

                            datos += "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&&" % (
                                carrera, horario.Seccion, horario.PeriodoCurso.Curso.Nombre,
                                horario.PeriodoCurso.Docente.username, horario.PeriodoCurso.id, dia, inicio, fin,
                                horario.PeriodoCurso.Curso.Ciclo, horario.id)

                        datos_idem = datos.split("&&")[:-1]

                        origen_horario_fil = f
                        origen_horario_col = c

                        # ========================================================================================
                        for d in datos_idem:
                            linea = d.split("|")
                            # AM|Aula 13|Matemática I|nnieves|828|LU|18:20|20:00|1|1157&&
                            print "***", datos_idem
                            indice_dia = dias_disponibles2.index(linea[5])
                            indice_hin = horarios_disponibles.index(linea[6])
                            indice_hfn = horarios_disponibles.index(linea[7])
                            num_celdas_seleccionadas = indice_hfn - indice_hin

                            for v in range(indice_hin, indice_hfn):
                                pos_f = origen_horario_fil + v
                                pos_c = origen_horario_col + indice_dia
                                debuger = str(linea[6]) + ":" + str(linea[7])
                                valor_celda = "%s\n%s\n%s\n" % (linea[2], linea[3].upper(), debuger)
                                wsheet.write(pos_f, pos_c, valor_celda, style_registros)
                                # wsheet.write( pos_f, pos_c, valor_celda + str(pos_f) + str(origen_horario_fil) + str(v) + "_" + str(indice_hin) + "," + str(indice_hfn), style_registros ) # Debuger
                        fGrid += POSICION_GRID
            contador_hoja += 1

        # Seleccionamos la primera hoja
        wb.get_sheet(0)

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=formato_horarios1.xls'
        wb.save()
        return response
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
