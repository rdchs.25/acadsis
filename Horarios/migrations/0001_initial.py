# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Horario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Dia', models.CharField(max_length=10, verbose_name=b'D\xc3\xada', choices=[(b'Lunes', b'Lunes'), (b'Martes', b'Martes'), (b'Miercoles', b'Miercoles'), (b'Jueves', b'Jueves'), (b'Viernes', b'Viernes'), (b'Sabado', b'Sabado'), (b'Domingo', b'Domingo')])),
                ('HoraInicio', models.TimeField()),
                ('HoraFin', models.TimeField()),
                ('Seccion', models.CharField(max_length=50, verbose_name=b'Secci\xc3\xb3n')),
                ('PeriodoCurso', models.ForeignKey(verbose_name=b'Curso', to='Cursos.PeriodoCurso')),
            ],
            options={
                'verbose_name': 'Horario',
                'verbose_name_plural': 'Horarios',
                'permissions': (('read_horario', 'Observar Configuracion de Horarios'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='horario',
            unique_together=set([('PeriodoCurso', 'Dia', 'HoraInicio', 'HoraFin', 'Seccion')]),
        ),
    ]
