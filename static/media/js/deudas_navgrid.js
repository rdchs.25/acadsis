jQuery("#navgrid").jqGrid({
   	url:'deudas_pendientes?p=' + jQuery("#anio").val() + '&s=' + jQuery("#semestre").val(),
	datatype: "json",
   	colNames:['No','Concepto', 'Monto', 'Deuda'],
   	colModel:[
   		{name:'id',index:'id', width:55},
   		{name:'concepto',index:'concepto', width:200},
   		{name:'monto',index:'monto', width:90, align:"right"},
   		{name:'deuda',index:'deuda', width:90, align:"right"}
   	],
   	rowNum:10,
   	//rowList:[10,20,30],
   	pager: '#pagernav',
   	sortname: 'id',
        viewrecords: false,
        pgbuttons: false,
        pginput: false,
        sortorder: "desc",
        caption:"Deudas Pendientes",
	height:'100%',
        width:'100%',
});
jQuery("#navgrid").jqGrid('navGrid','#pagernav',
{search:false,add:false,edit:false,del:false}, //options
{}, // edit options
{}, // add options
{}, // del options
{} // search options
);
