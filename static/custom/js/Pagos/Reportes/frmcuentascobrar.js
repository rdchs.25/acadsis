$(document).ready(function() {

    var periodo = $('#id_Periodo');
    var conceptos = $('#id_ConceptoPago');

    var update_conceptos = function() {

	var periodo_id = periodo.val();

        conceptos.empty();

        $.ajax({
           url: "obtener_conceptos/"+periodo_id+"/",
           dataType: "json",
           success: function (data, status){
			if (data) {
				$.each(data, function(i,item){
                                     var opcion = "<option value='" + item.pk + "'>" + item.fields.Descripcion + "</option>";
                                     conceptos.append(opcion);
				});
			}
                     },
           error: function (data, status, e){
            		   	var opcion = "<option value=''>Primero elija el Período académico</option>";
            			conceptos.append(opcion);
                  }
        });

    };

    update_conceptos();

    periodo.change(function () {
          update_conceptos();
    });

});
