$(document).ready(function() {

$('label[for="id_Descripcion"]').hide();
$('#id_Descripcion').hide();


   if ($('#id_TipoMora').val() == 'Diaria' || $('#id_TipoMora').val() == 'Semanal' || $('#id_TipoMora').val() == 'Mensual'){
      var montomora = $('#id_MontoMora').val();
      var vencemora = $('#id_FechaVencimiento').val();
   }
   
   if ($('#id_Descripcion').val()){

       var desc = $('#id_Descripcion').val();
       var separar = desc.split(" ");

       if (separar[0]=='Pension'){
           $('#id_Tipo').val(separar[0]);
           $('#id_Mes').val(separar[1]);
//            $('#id_Nivel').val(separar[2]);
       }else if (separar[0]=='Matricula'){
           $('#id_Tipo').val(separar[0]);
//            $('#id_Nivel').val(separar[1]);
//        }else if (separar[0]=='Aniversario'){
//            $('#id_Tipo').val(separar[0]);
       }else{
           $('#id_Tipo').val('Otro');
           var otro = $('#id_Descripcion').val();
       }

   }


   var update_mora = function() {

    if ($('#id_TipoMora').val()=='Ninguna'){

       $('label[for="id_MontoMora"]').hide();
       $('#id_MontoMora').hide();
       $('label[for="id_FechaVencimiento"]').hide();
       $('#id_FechaVencimiento').hide();
       $('#id_FechaVencimiento_btn').hide();
       $('.datetimeshortcuts').hide();
       

       var today       = new Date();
       var anio     = today.getFullYear();
       var mes      = today.getMonth() + 1;
       var dia        = today.getDate();

       mes = (mes < 10)? "0" + mes : mes;
       dia = (dia < 10)? "0" + dia : dia;

 
       $('#id_FechaVencimiento').val('31/12/2050');
       $('#id_MontoMora').val('0.00');

    }else if ($('#id_TipoMora').val()=='Diaria'){
 
       $('label[for="id_MontoMora"]').show();
       $('#id_MontoMora').show();
       $('#id_MontoMora').val('0.40');

       $('label[for="id_FechaVencimiento"]').show();
       $('#id_FechaVencimiento').show();
       $('#id_FechaVencimiento').val('');
       //$('#id_FechaVencimiento_btn').show();
       $('.datetimeshortcuts').show();


    }else {

       $('label[for="id_MontoMora"]').show();
       $('#id_MontoMora').show();
       $('#id_MontoMora').val('');

       $('label[for="id_FechaVencimiento"]').show();
       $('#id_FechaVencimiento').show();
       $('#id_FechaVencimiento').val('');
       //$('#id_FechaVencimiento_btn').show();
       $('.datetimeshortcuts').show();
    }

   }
   var update_descripcion = function() {

    if ($('#id_Tipo').val()=='Otro'){

        $('label[for="id_Descripcion"]').show();
        $('#id_Descripcion').show();
        $('label[for="id_Mes"]').hide();
        $('#id_Mes').hide();
        $('#id_Descripcion').val('');

    }else{

        $('label[for="id_Descripcion"]').hide();
        $('#id_Descripcion').hide();

        if ($('#id_Tipo').val()=='Pension'){
           $('label[for="id_Mes"]').show();
           $('#id_Mes').show();
           $('#id_Descripcion').val($('#id_Tipo').val() + " " + $('#id_Mes').val() + " " + $('#id_Periodo :selected').text());

        }else if ($('#id_Tipo').val()=='Matricula'){

           $('label[for="id_Mes"]').hide();
           $('#id_Mes').hide();
           $('#id_Descripcion').val($('#id_Tipo').val() +  " " + $('#id_Periodo :selected').text());

        }
     }
  }

    update_descripcion();
    update_mora();

    if (typeof(otro) != 'undefined'){
           $('#id_Descripcion').val(otro);
    }

    if (typeof(montomora) != 'undefined' && typeof(vencemora) != 'undefined'){
           $('#id_MontoMora').val(montomora);
           $('#id_FechaVencimiento').val(vencemora);
    }

    $('#id_Tipo').change(function () {
          update_descripcion();
    });

    $('#id_Mes').change(function () {
          update_descripcion();
    });

    $('#id_Periodo').change(function () {
          update_descripcion();
    });

    $('#id_TipoMora').change(function () {
          update_mora();
    });

});