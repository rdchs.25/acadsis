$(document).ready(function() {

    var periodo = $('#id_Periodo');
    var concepto = $('#id_ConceptoPago');

    var update_concepto = function() {

	var periodo_id = periodo.val();

        concepto.empty();

        $.ajax({
           url: "obtener_conceptos/"+periodo_id+"/",
           dataType: "json",
           success: function (data, status){
			if (data) {
				$.each(data, function(i,item){
                                     var opcion = "<option value='" + item.pk + "'>" + item.fields.Descripcion + "</option>";
                                     concepto.append(opcion);
				});
			}
                     },
           error: function (data, status, e){
            		   	var opcion = "<option value=''>Primero elija el Período académico</option>";
            			concepto.append(opcion);
                  }
        });

    };

    update_concepto();

    periodo.change(function () {
          update_concepto();
    });

});