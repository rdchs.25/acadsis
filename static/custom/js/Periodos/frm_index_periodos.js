$(document).ready(function() {

    var carrera = $('#id_Carrera');
    var ciclo = $('#id_Ciclo');
    var curso = $('#id_Curso');
    var periodo = $('#id_Periodo');

    var update_curso = function() {

	var carrera_id = carrera.val();
        var ciclo_id = ciclo.val();
        var periodo_id = periodo.val();

        curso.empty();

        $.ajax({
           url: "obtenercursos/"+carrera_id+"/"+ciclo_id+"/"+periodo_id+"/",
           dataType: "json",
           success: function (data, status){
			if (data) {
				$.each(data, function(i,item){
                                     var opcion = "<option value='" + item.pk + "'>" + item.extras.__unicode__ + "</option>";
                                     curso.append(opcion);
				});
			}
                     },
           error: function (data, status, e){
            		   	var opcion = "<option value=''>Primero elija un periodo, una carrera y un ciclo</option>";
            			curso.append(opcion);
                  }
        });

    };

    update_curso();

    carrera.change(function () {
          update_curso();
    });

    ciclo.change(function () {
          update_curso();
    });

    periodo.change(function () {
          update_curso();
    });

});
