$(document).ready(function() {

    var periodo = $('#id_Periodo');
    var categoria = $('#id_Categoria');

    var update_categoria = function() {

	if ($('#id_selected_cat').val()){
            var selected_cat = $('#id_selected_cat').val();
            $('#id_selected_cat').val('');
        }else{
            var selected_cat = '';
        }

        var periodo_id = periodo.val();
        categoria.empty();

        $.ajax({
           url: "../obtener_categorias/"+periodo_id+"/",
           dataType: "json",
           success: function (data, status){
			if (data) {
				$.each(data, function(i,item){
                                     if ( item.pk == selected_cat){
                                         var opcion = "<option value='" + item.pk + "' selected = 'selected'>" + item.extras.__unicode__ + "</option>";
                                     }else{
                                         var opcion = "<option value='" + item.pk + "'>" + item.extras.__unicode__ + "</option>";
                                     }
                                     categoria.append(opcion);
				});
			}
                     },
           error: function (data, status, e){
            		   	var opcion = "<option value=''>Primero elija un período</option>";
            			categoria.append(opcion);
                  }
        });

    };

    update_categoria();

    periodo.change(function () {
          update_categoria();
    });

});
