$(document).ready(function() {

    var departamento = $('#id_Departamento');
    var provincia = $('#id_Provincia');

    var update_provincia = function() {

	if ($('#id_selected_dep').val()){
            var departamento_id = $('#id_selected_dep').val();
            departamento.val($('#id_selected_dep').val());
            $('#id_selected_dep').val('');
        }else{
            var departamento_id = departamento.val();
        }

        provincia.empty();

        $.ajax({
           url: "provincias/"+departamento_id+"/",
           dataType: "json",
           success: function (data, status){
			if (data) {
				$.each(data, function(i,item){
                                     var opcion = "<option value='" + item.pk + "'>" + item.fields.Provincia + "</option>";
                                     provincia.append(opcion);
				});
			}
                     },
           error: function (data, status, e){
            		   	var opcion = "<option value=''>Primero elija un departamento</option>";
            			provincia.append(opcion);
                  }
        });

    };

    update_provincia();

    departamento.change(function () {
          update_provincia();
    });

});