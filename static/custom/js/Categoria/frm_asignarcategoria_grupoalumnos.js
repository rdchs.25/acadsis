$(document).ready(function () {

    var condicion = $('#id_Condicion');
    var periodo_origen = $('#id_PeriodoOrigen');
    var periodo_destino = $('#id_PeriodoDestino');
    var idvactualizar = $('#id_divActualizar');
    var categoria = $('#id_Categoria');
    var divperiodestino = $('#id_divPeriodoDestino');
    var divarchivo = $('#id_divArchivo');
    var divcategoria = $('#id_divCategoria');

    divarchivo.hide();
    divperiodestino.show();
    idvactualizar.hide();

    // periodo_origen.find("option[value='"+periodo_destino.val()+"']").remove();

    condicion.on("change", function () {
        var nombre = condicion.val();
        if (nombre == "Archivo") {
            divarchivo.show();
            divperiodestino.hide();
            divcategoria.hide();
            idvactualizar.show();

        } else {
            divarchivo.hide();
            divcategoria.show();
            divperiodestino.show();
            idvactualizar.hide();
        }
    });

    var update_categoria = function () {

        var periodo_id = periodo_origen.val();
        var periodo_destino_id = periodo_destino.val();

        categoria.empty();

        $.ajax({
            url: "obtener_categorias/" + periodo_destino_id + "/",
            dataType: "json",
            success: function (data, status) {
                if (data) {
                    $.each(data, function (i, item) {
                        var opcion = "<option value='" + item.pk + "'>" + item.fields.Categoria + "</option>";
                        categoria.append(opcion);
                    });
                }

            },
            error: function (data, status, e) {
                var opcion = "<option value=''>Primero elija el Período académico</option>";
                categoria.append(opcion);
            }
        });

    };

    update_categoria();

    // periodo_origen.change(function () {
    //     update_categoria();
    // });

});

