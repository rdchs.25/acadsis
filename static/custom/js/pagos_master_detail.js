jQuery("#list10").jqGrid({
        url:'pagos/?p=' + jQuery("#anio").val() + '&s=' + jQuery("#semestre").val(),
        datatype: "json",
   	colNames:['No','Concepto', 'Monto', 'Deuda','Mora','Deuda Mora','Vence','Lugar Pago'],
   	colModel:[
   		{name:'id',index:'id', width:55},
   		{name:'concepto',index:'concepto', width:200},
   		{name:'monto',index:'monto', width:90, align:"right"},
   		{name:'deuda',index:'deuda', width:90, align:"right"},
   		{name:'mora',index:'mora', width:90, align:"right"},		
   		{name:'deuda_mora',index:'deuda_mora', width:90,align:"right"},		
   		{name:'vence',index:'vence', width:150},		
   		{name:'lugar_pago',index:'lugar_pago', width:90}
   	],
   	rowNum:10,
   	pager: '#pager10',
   	sortname: 'id',
        viewrecords: false,
        pgbuttons: false,
        pginput: false,
        sortorder: "desc",
	multiselect: false,
	caption: "Pagos Realizados",
	onSelectRow: function(ids) {
		if(ids == null) {
			ids=0;
			if(jQuery("#list10_d").jqGrid('getGridParam','records') > 0 )
			{
				jQuery("#list10_d").jqGrid('setGridParam',{url:"detalle_pagos/?id="+ids,page:1});
				jQuery("#list10_d").jqGrid('setCaption',"Detalle de Pago: "+ids).trigger('reloadGrid');
			}
		} else {
			jQuery("#list10_d").jqGrid('setGridParam',{url:"detalle_pagos/?id="+ids,page:1});
			jQuery("#list10_d").jqGrid('setCaption',"Detalle de Pago: "+ids).trigger('reloadGrid');			
		}
	},
        height:'100%',
        width: '100%'
});
jQuery("#list10").jqGrid('navGrid','#pager10',{search:false,add:false,edit:false,del:false});
jQuery("#list10_d").jqGrid({
	height: '100%',
        width: '100%',
   	url:'detalle_pagos/?id=0',
	datatype: "json",
   	colNames:['No','Comprobante', 'Debe', 'Pago','Resta','Fecha Pago','¿Es mora?'],
   	colModel:[
   		{name:'num',index:'num', width:55},
   		{name:'comprobante',index:'comprobante', width:220},
   		{name:'debe',index:'debe', width:80, align:"right"},
   		{name:'pago',index:'pago', width:80, align:"right"},		
   		{name:'resta',index:'resta', width:80, align:"right"},
   		{name:'fecha',index:'fecha', width:100, align:"center"},
   		{name:'mora',index:'mora', width:80, align:"center"}
   	],
   	rowNum:10,
   	pager: '#pager10_d',
   	sortname: 'item',
        viewrecords: false,
        pgbuttons: false,
        pginput: false,
        sortorder: "asc",
	multiselect: false,
	caption:"Detalle de Pago"
});
jQuery("#list10_d").jqGrid('navGrid','#pager10_d',{search:false,add:false,edit:false,del:false});
