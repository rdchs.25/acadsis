$(document).ready(function () {

    var carrera = $('#id_Carrera');
    var prerequisito = $('#id_Prerequisito');
    var periodo = $('#id_Periodo');

    var update_prerequisito = function () {

        if ($('#id_selected_car').val()) {
            var carrera_id = $('#id_selected_car').val();
            carrera.val($('#id_selected_car').val());
            $('#id_selected_car').val('');
        } else {
            var carrera_id = carrera.val();
        }

        if ($('#id_selected_per').val()) {
            var periodo_id = $('#id_selected_per').val();
            periodo.val($('#id_selected_per').val());
            $('#id_selected_per').val('');
        } else {
            var periodo_id = periodo.val();
        }

        prerequisito.empty();

        $.ajax({
            url: "prerequisitos2/" + carrera_id + "/" + periodo_id + "/",
            dataType: "json",
            success: function (data, status) {
                if (data) {
                    $.each(data, function (i, item) {
                        var opcion = "<option value='" + item.pk + "'>" + item.extras.Ciclo + " - " + item.extras.Codigo + ", " + item.extras.Nombre + "</option>";
                        prerequisito.append(opcion);
                    });
                    var opcion = "<option value=''>Ninguno</option>";
                    prerequisito.append(opcion);

                    if ($('#id_selected_pre').val()) {
                        prerequisito.val($('#id_selected_pre').val());
                        $('#id_selected_pre').val('');
                    } else {
                        prerequisito.val('');
                    }
                }
            },
            error: function (data, status, e) {
                var opcion = "<option value=''>Primero elija una carrera</option>";
                prerequisito.append(opcion);
            }
        });

    };

    update_prerequisito();

    carrera.change(function () {
        update_prerequisito();
    });

});
