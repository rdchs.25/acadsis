$(document).ready(function() {
    var periodo = $('#id_Periodo');
    var ciclo   = $('#id_Ciclo');
    var carrera = $('#id_Carrera');
    var curso = $('#id_Curso');

    var update_curso = function() {

        var periodo_id = periodo.val();
        var carrera_id = carrera.val();
        var ciclo_id = ciclo.val();

        curso.empty();
        if (periodo_id !== '' && carrera_id !== '' && ciclo_id !== '') {
            $.ajax({
                url: "obtenercursos/" + carrera_id + "/" + ciclo_id + "/" + periodo_id + "/",
                dataType: "json",
                success: function (data, status){
                    if (data) {
                        var count = Object.keys(data).length;
                        var opcion = "<option value=''>------</option>";
                        curso.append(opcion);
                        if (count > 0) {
                            $.each(data, function(i,item){
                                var opcion = "<option value='" + item.pk + "'>" + item.fields.Nombre + "</option>";
                                curso.append(opcion);
                            }); 
                        }else{
                            var opcion = "<option value=''>------</option>";
                            curso.append(opcion);
                        }
                    }
                },
                error: function (data, status, e){
                    var opcion = "<option value=''>------</option>";
                    curso.append(opcion);
                }
            });
        }
    };
    update_curso();
    periodo.change(function () {
        update_curso();
    });
    carrera.change(function () {
        update_curso();
    });
    ciclo.change(function () {
        update_curso();
    });
});
