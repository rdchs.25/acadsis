$(document).ready(function () {
    var plan = $('#id_Plan');
    plan.change(function () {
        update_curso();
    });
});

function update_curso() {
    var plan = $('#id_Plan');
    var curso = $('#id_Prerequisitos_from');
    curso.empty();
    var plan_id = plan.val();
    curso.empty();

    if (plan_id !== '') {
        $.ajax({
            url: "obtenerdetallesplan/" + plan_id + "/",
            dataType: "json",
            success: function (data, status) {
                if (data) {
                    var count = Object.keys(data).length;
                    var opcion = "<option value=''>------</option>";
                    curso.append(opcion);
                    if (count > 0) {
                        $.each(data, function (i, item) {
                            var opcion = "<option value='" + item.pk + "'>" + item.fields.Nombre + "</option>";
                            curso.append(opcion);
                        });
                    } else {
                        var opcion = "<option value=''>------</option>";
                        curso.append(opcion);
                    }
                }
            },
            error: function (data, status, e) {
                var opcion = "<option value=''>------</option>";
                curso.append(opcion);
            }
        });
    }
}

