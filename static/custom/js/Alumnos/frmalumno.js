$(document).ready(function () {

    var departamento = $('#id_Departamento');
    var provincia = $('#id_Provincia');
    var ubigeo = $('#id_Distrito');

    var update_provincia = function () {

        if ($('#id_selected_dep').val()) {
            var departamento_id = $('#id_selected_dep').val();
            departamento.val($('#id_selected_dep').val());
            $('#id_selected_dep').val('');
        } else {
            var departamento_id = departamento.val();
        }

        if ($('#id_selected_prov').val()) {
            var selected_prov = $('#id_selected_prov').val();
            $('#id_selected_prov').val('');
        } else {
            var selected_prov = '';
        }

        provincia.empty();
        ubigeo.empty();
        if (departamento_id == ''){
            var opcion = "<option value=''>Primero elija un departamento</option>";
            provincia.append(opcion);
            update_ubigeo();
            return false;
        }
        var opcion = "<option value=''>--Seleccione--</option>";
        provincia.append(opcion);

        $.ajax({
            url: "provincias/" + departamento_id + "/",
            dataType: "json",
            success: function (data, status) {
                if (data) {
                    $.each(data, function (i, item) {
                        if (item.pk == selected_prov) {
                            var opcion = "<option value='" + item.pk + "' selected = 'selected'>" + item.fields.Provincia + "</option>";
                        } else {
                            var opcion = "<option value='" + item.pk + "'>" + item.fields.Provincia + "</option>";
                        }
                        provincia.append(opcion);
                    });
                    update_ubigeo();
                }
            },
            error: function (data, status, e) {
                var opcion = "<option value=''>Primero elija un departamento</option>";
                provincia.append(opcion);
                update_ubigeo();
            }
        });
    };

    var update_ubigeo = function () {

        if ($('#id_selected_prov').val()) {
            var provincia_id = $('#id_selected_prov').val();
            provincia.val($('#id_selected_prov').val());
            $('#id_selected_prov').val('');
        } else {
            var provincia_id = provincia.val();
        }

        if ($('#id_selected_dist').val()) {
            var selected_dist = $('#id_selected_dist').val();
            $('#id_selected_dist').val('');
        } else {
            var selected_dist = '';
        }

        ubigeo.empty();
        if (provincia_id == ''){
            var opcion = "<option value=''>Primero elija un provincia</option>";
            ubigeo.append(opcion);
            return false;
        }
        var opcion = "<option value=''>--Seleccione--</option>";
        ubigeo.append(opcion);


        $.ajax({
            url: "ubigeos/" + provincia_id + "/",
            dataType: "json",
            success: function (data, status) {
                if (data) {
                    $.each(data, function (i, item) {
                        if (item.pk == selected_dist) {
                            var opcion = "<option value='" + item.pk + "' selected = 'selected'>" + item.fields.Distrito + "</option>";
                        } else {
                            var opcion = "<option value='" + item.pk + "'>" + item.fields.Distrito + "</option>";
                        }
                        ubigeo.append(opcion);
                    });
                }
            },
            error: function (data, status, e) {
                var opcion = "<option value=''>Primero elija un provincia</option>";
                ubigeo.append(opcion);
            }
        });
    };

    update_provincia();
    departamento.change(function () {
        update_provincia();
    });
    provincia.change(function () {
        update_ubigeo();
    });

});
