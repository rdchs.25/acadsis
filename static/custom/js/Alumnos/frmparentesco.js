$(document).ready(function() {

// al elegir responsable se cargue los valores de padre o madre

   $('#id_Responsable').change(function () {
          var responsable = $('#id_Responsable').val();

          if (responsable == 'Madre'){
		
		$('#id_Parentesco').val($('#id_Responsable').val());
		$('#id_ApePatTutor').val($('#id_ApePatMadre').val());
		$('#id_ApeMatTutor').val($('#id_ApeMatMadre').val());
		$('#id_NombresTutor').val($('#id_NombresMadre').val());
 		$('#id_DNITutor').val($('#id_DNIMadre').val());
		$('#id_DirTutor').val($('#id_DirMadre').val());
		$('#id_UrbTutor').val($('#id_UrbMadre').val());
		$('#id_DepTutor').val($('#id_DepMadre').val());
		$('#id_ProvTutor').val($('#id_ProvMadre').val());
		$('#id_DistTutor').val($('#id_DistMadre').val());
		$('#id_TelfTutor').val($('#id_TelfMadre').val());
		$('#id_CelTutor').val($('#id_CelMadre').val());
		$('#id_OcupTutor').val($('#id_OcupMadre').val());
                $('#id_CtroTrabTutor').val($('#id_CtroTrabMadre').val());
		$('#id_DirOcupTutor').val($('#id_DirOcupMadre').val());
		$('#id_TelfOcupTutor').val($('#id_TelfOcupMadre').val());
		$('#id_IngresoTutor').val($('#id_IngresoMadre').val());
		
          }else if (responsable == 'Padre'){

		$('#id_Parentesco').val($('#id_Responsable').val());
		$('#id_ApePatTutor').val($('#id_ApePatPadre').val());
		$('#id_ApeMatTutor').val($('#id_ApeMatPadre').val());
		$('#id_NombresTutor').val($('#id_NombresPadre').val());
 		$('#id_DNITutor').val($('#id_DNIPadre').val());
		$('#id_DirTutor').val($('#id_DirPadre').val());
		$('#id_UrbTutor').val($('#id_UrbPadre').val());
		$('#id_DepTutor').val($('#id_DepPadre').val());
		$('#id_ProvTutor').val($('#id_ProvPadre').val());
		$('#id_DistTutor').val($('#id_DistPadre').val());
		$('#id_TelfTutor').val($('#id_TelfPadre').val());
		$('#id_CelTutor').val($('#id_CelPadre').val());
		$('#id_OcupTutor').val($('#id_OcupPadre').val());
                $('#id_CtroTrabTutor').val($('#id_CtroTrabPadre').val());
		$('#id_DirOcupTutor').val($('#id_DirOcupPadre').val());
		$('#id_TelfOcupTutor').val($('#id_TelfOcupPadre').val());
		$('#id_IngresoTutor').val($('#id_IngresoPadre').val());

          }else if (responsable == 'Apoderado'){
		
		$('#id_Parentesco').val('');
		$('#id_ApePatTutor').val('');
		$('#id_ApeMatTutor').val('');
		$('#id_NombresTutor').val('');
 		$('#id_DNITutor').val('');
		$('#id_DirTutor').val('');
		$('#id_UrbTutor').val('');
		$('#id_DepTutor').val('');
		$('#id_ProvTutor').val('');
		$('#id_DistTutor').val('');
		$('#id_TelfTutor').val('');
		$('#id_CelTutor').val('');
		$('#id_OcupTutor').val('');
                $('#id_CtroTrabTutor').val('');
		$('#id_DirOcupTutor').val('');
		$('#id_TelfOcupTutor').val('');
		$('#id_IngresoTutor').val('');

          }
    });






});
