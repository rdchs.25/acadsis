window.addEvent('domready',function() {    //You may will need to change category, id_subcategory, and id_selected_cat    //to match the names of the fields that you are working with.    var departamento = $('id_Departamento');    var provincia = $('id_Provincia');    var update_provincia = function() {        var departamento_id = $('id_selected_dep').value;        if (departamento_id) {            $('id_selected_dep').value='';            departamento.value=departamento_id;        } else {            departamento_id = departamento.getSelected()[0].value;        }        //cat_id = category.getSelected()[0].value;        var provincia_id = provincia.getSelected()[0].value;        var request = new Request.JSON({            url: "provincias/"+departamento_id+"/",            onComplete: function(provs){                provincia.empty();                if (provs) {                    provs.each(function(prov) {                        var o = new Element('option', {                            'value':prov.pk,                            'html':prov.fields.Provincia                        });                        if (prov.pk == provincia_id) {                            o.set('selected','selected');                        }                        o.inject(provincia);                    });                } else {                    var o = new Element('option', {                        'value':'',                        'html':'Primero elija un Departamento'                    });                    o.inject(provincia);                }            }        }).get();    };    update_provincia();    departamento.addEvent('change', function(e){        e.stop();        update_provincia();    });});