if (jQuery("#grid_perms_add").val() == 'True' && jQuery("#nivel_cero").val() == 'False' ){
var add_grid = true ;
}else{
var add_grid = false;
}

if (jQuery("#grid_perms_add").val() == 'True'){
var add_grid2 = true ;
}else{
var add_grid2 = false;
}


if (jQuery("#grid_perms_change").val() == 'True'){
var edit_grid = true ;
}else{
var edit_grid = false;
}

if (jQuery("#grid_perms_delete").val() == 'True'){
var del_grid = true ;
}else{
var del_grid = false;
}

jQuery("#subgrid_notas").jqGrid({
   	url:'subgrid/vernotas/?p=' + jQuery("#grid_periodocurso").val(),
	datatype: "json",
   	colNames:['No','Nota','Identificador','Subnotas'],
   	colModel:[
   	{name:'id',index:'id',width:100,align:'center',editable:false,editoptions:{readonly:true,size:10}},
        {name:'Nota',index:'Nota', width:380,align:'center',editable:true,editoptions:{size:10, maxlength: 50},editrules:{required:true}},
        {name:'Identificador',index:'Identificador',width:280,align:'center',editable:true,editoptions:{size:10, maxlength: 4},editrules:{required:true,maxValue:4}},
        {name:'SubNotas',index:'SubNotas',width:100,align:'center',editable:false,editoptions:{readonly:true,size:10}},
   	],
   	rowNum:10,
   	pager: '#pagernav',
   	sortname: 'id',
        viewrecords: true,
        sortorder: 'desc',
        caption:'Configurar Notas',
        subGrid: true,
        subGridRowExpanded: function(subgrid_id, row_id) {
            var subgrid_table_id, pager_id;
            subgrid_table_id = subgrid_id+"_t";
            pager_id = "p_"+subgrid_table_id;
            jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
            jQuery("#"+subgrid_table_id).jqGrid({
            url:'subgrid/vernotas1/?p=' + jQuery("#grid_periodocurso").val() + '&fila=' + row_id,
            datatype: "json",
            colNames: ['No','Nota','Idem', 'Peso', 'Orden','SubNotas'],
            colModel: [
                {name:'id',index:'id',width:55,editable:false,editoptions:{readonly:true,size:10}},
                {name:'Nota',index:'Nota', width:220,editable:true,editoptions:{size:10, maxlength: 50},editrules:{required:true}},
                {name:'Identificador',index:'Identificador',width:150,formoptions:{label:'Identificador'},editable:true,editoptions:{size:10, maxlength: 4},editrules:{required:true,maxValue:4}},
                {name:'Peso',index:'Peso', width:150,editable:true,editoptions:{size:10},editrules:{required:true, number:true}},
                {name:'Orden',index:'Orden',width:150,editable:true,edittype:"select",editoptions:{value: "1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10"},editrules:{required:true}},
                {name:'SubNotas',index:'SubNotas',width:100,align:'center',editable:false,editoptions:{readonly:true,size:10}},
                ],
            rowNum:10,
   	    pager: pager_id,
   	    sortname: 'id',
            viewrecords: true,
            sortorder: 'desc',
            subGrid: true,
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id, pager_id;
                subgrid_table_id = subgrid_id+"_t";
                pager_id = "p_"+subgrid_table_id;
                jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
                jQuery("#"+subgrid_table_id).jqGrid({
                url:'subgrid/vernotas2/?p=' + jQuery("#grid_periodocurso").val() + '&fila=' + row_id,
                datatype: "json",
                colNames: ['No','Nota','Idem', 'Peso', 'Orden','SubNotas'],
                colModel: [
                    {name:'id',index:'id',width:55,editable:false,editoptions:{readonly:true,size:10}},
                    {name:'Nota',index:'Nota', width:230,editable:true,editoptions:{size:10, maxlength: 50},editrules:{required:true}},
                    {name:'Identificador',index:'Identificador',width:120,formoptions:{label:'Identificador'},editable:true,editoptions:{size:10, maxlength: 4},editrules:{required:true,maxValue:4}},
                    {name:'Peso',index:'Peso', width:140,editable:true,editoptions:{size:10},editrules:{required:true, number:true}},
                    {name:'Orden',index:'Orden',width:140,editable:true,edittype:"select",editoptions:{value: "1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10"},editrules:{required:true}},
                    {name:'SubNotas',index:'SubNotas',width:100,align:'center',editable:false,editoptions:{readonly:true,size:10}},
                    ],
                rowNum:10,
                pager: pager_id,
                sortname: 'id',
                viewrecords: true,
                sortorder: 'desc',
                subGrid: true,
                subGridRowExpanded: function(subgrid_id, row_id) {
                    var subgrid_table_id, pager_id;
                    subgrid_table_id = subgrid_id+"_t";
                    pager_id = "p_"+subgrid_table_id;
                    jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
                    jQuery("#"+subgrid_table_id).jqGrid({
                    url:'subgrid/vernotas3/?p=' + jQuery("#grid_periodocurso").val() + '&fila=' + row_id,
                    datatype: "json",
                    colNames: ['No','Nota','Idem', 'Peso', 'Orden','SubNotas'],
                    colModel: [
                        {name:'id',index:'id',width:55,editable:false,editoptions:{readonly:true,size:10}},
                        {name:'Nota',index:'Nota', width:190,editable:true,editoptions:{size:10, maxlength: 50},editrules:{required:true}},
                        {name:'Identificador',index:'Identificador',width:140,formoptions:{label:'Identificador'},editable:true,editoptions:{size:10, maxlength: 4},editrules:{required:true,maxValue:4}},
                        {name:'Peso',index:'Peso', width:140,editable:true,editoptions:{size:10},editrules:{required:true, number:true}},
                        {name:'Orden',index:'Orden',width:140,editable:true,edittype:"select",editoptions:{value: "1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10"},editrules:{required:true}},
                        {name:'SubNotas',index:'SubNotas',width:100,align:'center',editable:false,editoptions:{readonly:true,size:10}},
                        ],
                    rowNum:10,
                    pager: pager_id,
                    sortname: 'id',
                    viewrecords: true,
                    sortorder: 'desc',
                    editurl:jQuery("#grid_periodocurso").val() + '/master_subgrid_notas3/?fila=' + row_id + '&add=' + jQuery("#grid_perms_add").val() + '&change=' + jQuery("#grid_perms_change").val() + '&delete=' + jQuery("#grid_perms_delete").val(),
                    height:'100%',
                    width: '100%'
                    });
                    jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,
                    {search:false,add:add_grid2,edit:edit_grid,del:del_grid}, //options
                    {width:500,height:260,reloadAfterSubmit:true,closeAfterEdit: true}, // edit options
                    {width:500,height:260,reloadAfterSubmit:true,closeAfterAdd: true}, // add options
                    {width:270,reloadAfterSubmit:true}, // del options
                    {} // search options
                    );
                },
                editurl:jQuery("#grid_periodocurso").val() + '/master_subgrid_notas2/?fila=' + row_id + '&add=' + jQuery("#grid_perms_add").val() + '&change=' + jQuery("#grid_perms_change").val() + '&delete=' + jQuery("#grid_perms_delete").val(),
                height:'100%',
                width: '100%'
                });
                jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,
                {search:false,add:add_grid2,edit:edit_grid,del:del_grid}, //options
                {width:500,height:260,reloadAfterSubmit:true,closeAfterEdit: true}, // edit options
                {width:500,height:260,reloadAfterSubmit:true,closeAfterAdd: true}, // add options
                {width:270,reloadAfterSubmit:true}, // del options
                {} // search options
                );
            },
            editurl:jQuery("#grid_periodocurso").val() + '/master_subgrid_notas1/?fila=' + row_id + '&add=' + jQuery("#grid_perms_add").val() + '&change=' + jQuery("#grid_perms_change").val() + '&delete=' + jQuery("#grid_perms_delete").val(),
            height:'100%',
            width: '100%'
            });
            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,
            {search:false,add:add_grid2,edit:edit_grid,del:del_grid}, //options
            {width:500,height:260,reloadAfterSubmit:true,closeAfterEdit: true}, // edit options
            {width:500,height:260,reloadAfterSubmit:true,closeAfterAdd: true}, // add options
            {width:270,reloadAfterSubmit:true}, // del options
            {} // search options
            );
        },
        editurl: jQuery("#grid_periodocurso").val() + '/master_subgrid_notas/?add=' + jQuery("#grid_perms_add").val() + '&change=' + jQuery("#grid_perms_change").val() + '&delete=' + jQuery("#grid_perms_delete").val(),
        height:'100%',
        width: '100%'
});

jQuery("#subgrid_notas").jqGrid('navGrid','#pagernav',
{search:false,add:add_grid,edit:edit_grid,del:del_grid}, //options
{width:500,height:220,reloadAfterSubmit:true,closeAfterEdit: true}, // edit options
{width:500,height:220,reloadAfterSubmit:true,closeAfterAdd: true}, // add options
{width:270,reloadAfterSubmit:true}, // del options
{} // search options
);
