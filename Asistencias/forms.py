# -*- coding: utf-8 -*-
import datetime

from django import forms

from Alumnos.widgets import DateTimeWidget
from Periodos.models import Periodo


class IndexNotasAsistenciasForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class AsistenciaForm(forms.Form):
    Fecha = forms.DateField(initial=datetime.date.today, widget=DateTimeWidget, label="Fecha")
    PeriodoCurso = forms.CharField(widget=forms.HiddenInput, required=True, label="Curso")

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }
