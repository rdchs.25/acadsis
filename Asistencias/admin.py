# -*- coding: utf-8 -*-
from django.contrib import admin

from Asistencias.models import AsistenciaDocente


class AsistenciaDocenteAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'docente', 'curso', 'grupo', 'carrera', 'horario', 'temaclase',)
    search_fields = (
        'Horario__PeriodoCurso__Docente__ApellidoPaterno', 'Horario__PeriodoCurso__Docente__ApellidoMaterno',
        'Horario__PeriodoCurso__Docente__Nombres', 'Horario__PeriodoCurso__Curso__Nombre')
    date_hierarchy = 'Fecha'


admin.site.register(AsistenciaDocente, AsistenciaDocenteAdmin)
