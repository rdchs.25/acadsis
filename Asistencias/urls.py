# -*- coding: utf-8 -*-
from django.conf.urls import url

from Asistencias.views import index_notas_asistencias, menu_notas_asistencias, view_asistencias, add_asistencias, \
    edit_asistencias, delete_asistencias, view_notas_nuevo, view_guardar_notas

app_name = 'Asistencias'

urlpatterns = [
    # urls para aplicacion Asistencias
    url(r'^notas_asistencia/$', index_notas_asistencias),
    url(r'^notas_asistencia/cursos/(\d+)/$', menu_notas_asistencias),
    url(r'^notas_asistencia/cursos/Asistencias/(\d+)/$', view_asistencias),
    url(r'^notas_asistencia/cursos/Asistencias/add/(\d+)/$', add_asistencias),
    url(r'^notas_asistencia/cursos/Asistencias/editar/(.*)/(.*)/(.*)/(.*)/(.*)/$', edit_asistencias),
    url(r'^notas_asistencia/cursos/Asistencias/delete/(.*)/(.*)/(.*)/(.*)/(.*)/$', delete_asistencias),
    url(r'^notas_asistencia/cursos/Notas/(\d+)/$', view_notas_nuevo),
    url(r'^notas_asistencia/cursos/Notas/(\d+)/registrar/$', view_guardar_notas),
]
