# -*- coding: utf-8 -*-

import datetime
from decimal import Decimal
from pprint import pprint

# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect

from Asistencias.forms import AsistenciaForm, IndexNotasAsistenciasForm
from Asistencias.models import Asistencia, AsistenciaDocente
from Cursos.models import PeriodoCurso
from Horarios.models import Horario
from Matriculas.models import MatriculaCursos
from Notas.models import Nota, NotasAlumno


@csrf_protect
def index_notas_asistencias(request):
    if request.user.is_authenticated() and request.user.has_perm('Notas.read_notasalumno') or request.user.has_perm(
            'Notas.add_notasalumno') or request.user.has_perm('Notas.change_notasalumno') or request.user.has_perm(
        'Asistencias.change_asistencia') or request.user.has_perm('Asistencias.add_asistencia') or \
            request.user.has_perm('Asistencias.delete_asistencia'):
        if request.method == 'POST':
            form = IndexNotasAsistenciasForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("cursos/%s/" % str(periodo.id))
        else:
            form = IndexNotasAsistenciasForm()
        return render_to_response('Asistencias/index.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../')


def menu_notas_asistencias(request, id_periodo):
    if request.user.is_authenticated() and request.user.has_perm('Notas.add_notasalumno') or request.user.has_perm(
            'Notas.change_notasalumno') or request.user.has_perm('Asistencias.change_asistencia') or \
            request.user.has_perm('Asistencias.add_asistencia') or \
            request.user.has_perm('Asistencias.delete_asistencia'):

        periodos = PeriodoCurso.objects.filter(Periodo__id=id_periodo).values('Periodo__Anio',
                                                                              'Periodo__Semestre').distinct()

        i = 1
        j = 1

        arbol = ""
        for per in periodos:
            arbol += "d.add(" + str(i) + ",0,'" + str(per['Periodo__Anio']) + "-" + str(
                per['Periodo__Semestre']) + "','');"
            i = i + 1
        j = i

        k = 1
        carrs = []
        for per in periodos:
            carreras = PeriodoCurso.objects.filter(Periodo__Anio=per['Periodo__Anio'],
                                                   Periodo__Semestre=per['Periodo__Semestre']).values(
                'Curso__Carrera__Carrera').distinct()
            for car in carreras:
                arbol += "d.add(" + str(j) + "," + str(k) + ",'" + car['Curso__Carrera__Carrera'] + "','');"
                carrs.append([j, car['Curso__Carrera__Carrera'], per['Periodo__Anio'], per['Periodo__Semestre']])
                j = j + 1
            k = k + 1
        l = j

        x = i
        cicls = []
        for car in carrs:
            ciclos = PeriodoCurso.objects.filter(Periodo__Anio=car[2], Periodo__Semestre=car[3],
                                                 Curso__Carrera__Carrera=car[1]).values('Curso__Ciclo').distinct()
            for ciclo in ciclos:
                arbol += "d.add(" + str(l) + "," + str(x) + ",'" + ciclo['Curso__Ciclo'] + " Ciclo','');"
                cicls.append([x, car[1], car[2], car[3], ciclo['Curso__Ciclo']])
                l = l + 1
            x = x + 1
        m = l

        n = j
        curs = []
        for cicl in cicls:
            cursos = PeriodoCurso.objects.filter(Periodo__Anio=cicl[2], Periodo__Semestre=cicl[3],
                                                 Curso__Carrera__Carrera=cicl[1], Curso__Ciclo=cicl[4]).values(
                'Curso__Nombre').distinct()
            for cur in cursos:
                arbol += "d.add(" + str(m) + "," + str(n) + ",'" + cur['Curso__Nombre'] + "','');"
                curs.append([x, cicl[1], cicl[2], cicl[3], cicl[4], cur['Curso__Nombre']])
                m = m + 1
            n = n + 1
        o = m

        p = l
        id_groups = []
        for cur in curs:
            grupos = PeriodoCurso.objects.filter(Periodo__Anio=cur[2], Periodo__Semestre=cur[3],
                                                 Curso__Carrera__Carrera=cur[1], Curso__Ciclo=cur[4],
                                                 Curso__Nombre=cur[5]).distinct()
            for group in grupos:
                arbol += "d.add(" + str(o) + "," + str(p) + ",'" + group.Grupo + "','');"
                id_groups.append([o, group.id])
                o = o + 1
            p = p + 1

        q = m
        for id_group in id_groups:
            group_id = PeriodoCurso.objects.get(id=id_group[1])
            capa = "var capa1 = 'capa1';"
            link = "var link" + str(o) + " = '../Asistencias/" + str(group_id.id) + "/';"
            msj = "var msj = 'espere por favor...';"
            add = capa + link + msj
            arbol += add + "d.add('%s','%s','Asistencias','javascript:muestracapa2(capa1,link%s,msj)','','');" % (
                str(o), str(id_group[0]), str(o))

            link1 = "var link" + str(o + 1) + " = '../Notas/" + str(group_id.id) + "/';"
            add = capa + link1 + msj
            arbol += add + "d.add('%s','%s','Notas','javascript:muestracapa2(capa1,link%s,msj)','','');" % (
                str(o + 1), str(id_group[0]), str(o + 1))
            o = o + 2
            q = q + 1

        return render_to_response("Asistencias/menu_notas_asistencias.html",
                                  {"arbol": mark_safe(arbol), "user": request.user})
    else:
        return HttpResponseRedirect('../../')


# Clases y metodos para manejar el modulo de asistencias

def view_asistencias(request, id_periodocurso):
    if request.user.is_authenticated() and request.user.has_perm('Asistencias.change_asistencia') or \
            request.user.has_perm('Asistencias.add_asistencia') or \
            request.user.has_perm('Asistencias.delete_asistencia'):
        query = request.GET.get('q', '')
        query1 = request.GET.get('p', '')

        try:
            query = datetime.strptime(query, '%Y/%m/%d')
        except:
            query = ''

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada."
            links = "<a href='../../../../../'>Inicio</a>"
            return render_to_response("Asistencias/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=id_periodocurso)

        if query and query1:
            results1 = Asistencia.objects.filter(Horario__id=query1)
            results2 = results1.filter(Fecha=query).order_by('-Fecha').values('Fecha', 'Horario__Dia',
                                                                              'Horario__HoraInicio',
                                                                              'Horario__HoraFin').distinct()
            results3 = []
            n_asistencias = 0
            for res in results2:
                results3.append(
                    [res['Fecha'], res['Horario__Dia'], res['Horario__HoraInicio'], res['Horario__HoraFin']])
                n_asistencias += 1
            paginator = Paginator(results3, 50)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)
        elif query:
            results2 = Asistencia.objects.filter(Fecha=query).order_by('-Fecha').values('Fecha', 'Horario__Dia',
                                                                                        'Horario__HoraInicio',
                                                                                        'Horario__HoraFin').distinct()
            results3 = []
            n_asistencias = 0
            for res in results2:
                results3.append(
                    [res['Fecha'], res['Horario__Dia'], res['Horario__HoraInicio'], res['Horario__HoraFin']])
                n_asistencias += 1
            paginator = Paginator(results3, 50)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)
        elif query1:
            results2 = Asistencia.objects.filter(Horario=query1).order_by('-Fecha').values('Fecha', 'Horario__Dia',
                                                                                           'Horario__HoraInicio',
                                                                                           'Horario__HoraFin').distinct()
            results3 = []
            n_asistencias = 0
            for res in results2:
                results3.append(
                    [res['Fecha'], res['Horario__Dia'], res['Horario__HoraInicio'], res['Horario__HoraFin']])
                n_asistencias += 1
            paginator = Paginator(results3, 50)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)
        else:
            results2 = Asistencia.objects.filter(Horario__PeriodoCurso__id=id_periodocurso).order_by('-Fecha').values(
                'Fecha', 'Horario__Dia', 'Horario__HoraInicio', 'Horario__HoraFin').distinct()
            results3 = []
            n_asistencias = 0
            for res in results2:
                try:
                    asistenciadocente = AsistenciaDocente.objects.get(FechaClase=res['Fecha'],
                                                                      Horario__PeriodoCurso__id=id_periodocurso)
                    results3.append(
                        [res['Fecha'], res['Horario__Dia'], res['Horario__HoraInicio'],
                         res['Horario__HoraFin'], asistenciadocente.Fecha])
                except (AsistenciaDocente.MultipleObjectsReturned, AsistenciaDocente.DoesNotExist):
                    results3.append(
                        [res['Fecha'], res['Horario__Dia'], res['Horario__HoraInicio'], res['Horario__HoraFin'], ''])
                    pass

                n_asistencias += 1
            paginator = Paginator(results3, 50)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)
        return render_to_response("Asistencias/PeriodoCurso/view_asistencias.html",
                                  {"results": results, "query": query, "horarios": horarios,
                                   "periodocurso": periodocurso, "query1": query1, "n_asistencias": n_asistencias,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Acceso denegado"
        links = ""
        return render_to_response("Asistencias/mensaje1.html", {"mensaje": mensaje, "links": mark_safe(links)})


@csrf_protect
def add_asistencias(request, id_periodocurso):
    if request.user.is_authenticated() and request.user.has_perm('Asistencias.add_asistencia'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Asistencias/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        alumnos = MatriculaCursos.objects.filter(PeriodoCurso__id=id_periodocurso).order_by(
            'MatriculaCiclo__Alumno__ApellidoPaterno')

        horarios = Horario.objects.filter(PeriodoCurso__id=id_periodocurso)
        existe = False

        if request.method == 'POST':
            form = AsistenciaForm(request.POST)
            if form.is_valid():
                fecha = form.cleaned_data['Fecha']
                horario = request.POST['horario']
                curso = form.cleaned_data['PeriodoCurso']

                existe_asistencia = Asistencia.objects.filter(Fecha=fecha, Horario__id=horario)
                if existe_asistencia.count() != 0:
                    existe = True
                else:
                    for alumno in alumnos:
                        if alumno.Estado == True and alumno.Convalidado == False:
                            asistencia = request.POST[str(alumno.id)]
                            grabar_asistencia = Asistencia(MatriculaCursos=alumno, Horario_id=horario,
                                                           Estado=asistencia, Fecha=fecha)
                            grabar_asistencia.save()

                    mensaje = "Datos Guardados Correctamente"
                    links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                    return render_to_response("Asistencias/mensaje.html",
                                              {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = AsistenciaForm(initial={"Fecha": datetime.date.today(), "PeriodoCurso": id_periodocurso})

        return render_to_response("Asistencias/PeriodoCurso/add_asistencias.html",
                                  {"form": form, "user": request.user, "alumnos": alumnos, "periodocurso": periodocurso,
                                   "horarios": horarios, "existe": existe}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Asistencias/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def edit_asistencias(request, id_periodocurso, fecha, dia, inicio, fin):
    if request.user.is_authenticated() and request.user.has_perm('Asistencias.change_asistencia'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Asistencias/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        asistentes = Asistencia.objects.filter(Fecha=fecha, Horario__Dia=dia, Horario__HoraInicio=inicio,
                                               Horario__HoraFin=fin,
                                               MatriculaCursos__PeriodoCurso__id=id_periodocurso).order_by(
            'MatriculaCursos__MatriculaCiclo__Alumno__ApellidoPaterno')

        if asistentes.count() != 0:
            horario_elegido = asistentes[0].Horario

        horarios = Horario.objects.filter(PeriodoCurso__id=id_periodocurso)

        if request.method == 'POST':
            form = AsistenciaForm(request.POST)
            if form.is_valid():
                fecha = form.cleaned_data['Fecha']
                horario = request.POST['horario']
                curso = form.cleaned_data['PeriodoCurso']

                for asistente in asistentes:
                    asistencia = request.POST[str(asistente.id)]
                    grabar_asistencia = Asistencia(id=asistente.id, MatriculaCursos=asistente.MatriculaCursos,
                                                   Horario_id=horario, Estado=asistencia, Fecha=fecha)
                    grabar_asistencia.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Asistencias/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
            form = AsistenciaForm(initial={"Fecha": fecha, "PeriodoCurso": id_periodocurso})

        return render_to_response("Asistencias/PeriodoCurso/edit_asistencias.html",
                                  {"form": form, "user": request.user, "alumnos": asistentes,
                                   "periodocurso": periodocurso, "horarios": horarios,
                                   "horario_elegido": horario_elegido}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Asistencias/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def delete_asistencias(request, id_periodocurso, fecha, dia, inicio, fin):
    if request.user.is_authenticated() and request.user.has_perm('Asistencias.delete_asistencia'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Asistencias/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        asistentes = Asistencia.objects.filter(Fecha=fecha, Horario__Dia=dia, Horario__HoraInicio=inicio,
                                               Horario__HoraFin=fin,
                                               MatriculaCursos__PeriodoCurso__id=id_periodocurso).order_by(
            'MatriculaCursos__MatriculaCiclo__Alumno__ApellidoPaterno')

        if asistentes.count() != 0:
            horario_elegido = asistentes[0].Horario
            fecha = asistentes[0].Fecha

        if request.method == 'POST':
            for asistente in asistentes:
                asistente.delete()

            mensaje = "Asistencia Eliminada Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Asistencias/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("Asistencias/PeriodoCurso/delete_asistencias.html",
                                  {"user": request.user, "periodocurso": periodocurso,
                                   "horario_elegido": horario_elegido, "fecha": fecha},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Asistencias/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def view_notas(request, id_periodocurso):
    if request.user.is_authenticated() and request.user.has_perm('Notas.change_notasalumno') or request.user.has_perm(
            'Notas.add_notasalumno'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a href='../../../../'>Inicio</a>"
            return render_to_response("Docentes/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('id')

        titulos = "['N','Estudiante'"
        celdas = "[{name:'id1',index:'id1', align:'center', width:55},{name:'estudiante',index:'estudiante'," \
                 " width:250},"
        ident = []
        formulas = []

        if notas.count() != 0:
            for cal1 in notas:
                if cal1 in ident:
                    pass
                else:
                    if cal1.Nivel == "0":
                        ultima_nota = cal1
                        subnotas1 = Nota.objects.filter(NotaPadre=cal1)
                        formula = "%s = (" % cal1.Identificador
                        suma_pesos = 0
                        i = 1
                        if subnotas1.count() != 0:
                            for nota in subnotas1:
                                if i == 1:
                                    formula += "%s*%s" % (nota.Identificador, nota.Peso)
                                else:
                                    formula += " + %s*%s" % (nota.Identificador, nota.Peso)
                                suma_pesos += nota.Peso
                                i += 1
                            formula += ")/%s" % suma_pesos
                            formulas.append(formula)
                        else:
                            formula = "%s = %s" % (cal1.Identificador, cal1.Identificador)
                            formulas.append(formula)
                    else:
                        subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                        if subnotas2.count() == 0:
                            ident.append(cal1)
                            titulos += ",'%s'" % cal1.Identificador
                            celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:true," \
                                      " editrules:{required:true, number:true, custom:true, " \
                                      "custom_func:evaluar_nota}}," % (
                                          cal1.id, cal1.Identificador)
                        else:
                            formula1 = "%s = (" % cal1.Identificador
                            suma_pesos1 = 0
                            i = 1
                            for cal2 in subnotas2:
                                if i == 1:
                                    formula1 += "%s*%s" % (cal2.Identificador, cal2.Peso)
                                else:
                                    formula1 += " + %s*%s" % (cal2.Identificador, cal2.Peso)
                                suma_pesos1 += cal2.Peso
                                i += 1

                                subnotas3 = Nota.objects.filter(NotaPadre=cal2)
                                if subnotas3.count() == 0:
                                    ident.append(cal2)
                                    titulos += ",'%s'" % cal2.Identificador
                                    celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center'," \
                                              "editable:true, editrules:{required:true, number:true, custom:true, " \
                                              "custom_func:evaluar_nota}}," % (
                                                  cal2.id, cal2.Identificador)
                                else:
                                    formula2 = "%s = (" % cal2.Identificador
                                    suma_pesos2 = 0
                                    i = 1
                                    for cal3 in subnotas3:
                                        if i == 1:
                                            formula2 += "%s*%s" % (cal3.Identificador, cal3.Peso)
                                        else:
                                            formula2 += " + %s*%s" % (cal3.Identificador, cal3.Peso)
                                        suma_pesos2 += cal3.Peso
                                        i += 1

                                        ident.append(cal3)
                                        titulos += ",'%s'" % cal3.Identificador
                                        celdas += "{name:'%s',index:'%s', width:80,sortable:false, " \
                                                  "align:'center',editable:true, editrules:{required:true, " \
                                                  "number:true, custom:true, custom_func:evaluar_nota}}," % (
                                                      cal3.id, cal3.Identificador)
                                    formula2 += ")/%s" % suma_pesos2
                                    formulas.append(formula2)
                                    ident.append(cal2)
                            formula1 += ")/%s" % suma_pesos1
                            formulas.append(formula1)
                            ident.append(cal1)
            ident.append(ultima_nota)
            titulos += ",'%s'" % ultima_nota.Identificador
            celdas += "{name:'%s',index:'%s', width:80, align:'center',editable:true}," % (
                ultima_nota.id, ultima_nota.Identificador)
        titulos += "]"
        celdas += "]"
        return render_to_response("Asistencias/PeriodoCurso/view_notas.html",
                                  {"periodocurso": periodocurso, "titulos": mark_safe(titulos),
                                   "celdas": mark_safe(celdas), "notas": ident, "formulas": formulas,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Acceso denegado"
        links = ""
        return render_to_response("Asistencias/mensaje1.html", {"mensaje": mensaje, "links": mark_safe(links)})


@csrf_exempt
def view_notas_nuevo(request, id_periodocurso):
    if request.user.is_authenticated() and request.user.has_perm('Notas.change_notasalumno') or request.user.has_perm(
            'Notas.add_notasalumno') or request.user.has_perm('Notas.read_notasalumno'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a href='../../../../'>Inicio</a>"
            return render_to_response("Docentes/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if not hor.PeriodoCurso in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        registros = []
        for per in periodo_cursos:
            notas = Nota.objects.filter(PeriodoCurso=per).order_by('id')
            mats = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False).order_by(
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')
            for mat in mats:
                # ----------------
                ident1 = []
                if notas.count() != 0:
                    for cal1 in notas:
                        if cal1.Nota in ident1:
                            pass
                        else:
                            if cal1.Nivel == "0":
                                ultima_nota = cal1.Nota
                                subnotas1 = Nota.objects.filter(NotaPadre=cal1)
                                try:
                                    nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=mat)
                                except NotasAlumno.DoesNotExist:
                                    nota = NotasAlumno(Nota=cal1, MatriculaCursos=mat, Valor=Decimal(mat.Promedio()))
                                    nota.save()
                            else:
                                subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                                if subnotas2.count() == 0:
                                    try:
                                        nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=mat)
                                    except NotasAlumno.DoesNotExist:
                                        nota = NotasAlumno(Nota=cal1, MatriculaCursos=mat, Valor=Decimal("0.00"))
                                        nota.save()
                                    ident1.append(cal1.Nota)
                                else:
                                    for cal2 in subnotas2:
                                        subnotas3 = Nota.objects.filter(NotaPadre=cal2)
                                        if subnotas3.count() == 0:
                                            try:
                                                nota = NotasAlumno.objects.get(Nota=cal2, MatriculaCursos=mat)
                                            except NotasAlumno.DoesNotExist:
                                                nota = NotasAlumno(Nota=cal2, MatriculaCursos=mat,
                                                                   Valor=Decimal("0.00"))
                                                nota.save()
                                            ident1.append(cal2.Nota)
                                        else:
                                            for cal3 in subnotas3:
                                                try:
                                                    nota = NotasAlumno.objects.get(Nota=cal3, MatriculaCursos=mat)
                                                except NotasAlumno.DoesNotExist:
                                                    nota = NotasAlumno(Nota=cal3, MatriculaCursos=mat,
                                                                       Valor=Decimal("0.00"))
                                                    nota.save()
                                                ident1.append(cal3.Nota)
                                            ident1.append(cal2.Nota)
                                    ident1.append(cal1.Nota)
                    ident1.append(ultima_nota)
                # ----------------
                notas0 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, Nivel='0').order_by('Orden')
                if notas0.count() == 1:
                    valor_final = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=notas0[0])
                    if valor_final.count() == 1:
                        id_nota_final = valor_final[0].Nota.id
                        valor_final = valor_final[0].Valor

                notas1 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, Nivel='1').order_by('Orden')

                nots1 = []
                nots2 = []
                nots3 = []

                for nota1 in notas1:
                    nots_2 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota1, Nivel='2').order_by(
                        'Orden')
                    for nota2 in nots_2:
                        nots_3 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota2,
                                                     Nivel='3').order_by('Orden')
                        for nota3 in nots_3:
                            valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                            if valor3.count() != 0:
                                valor3 = valor3[0].Valor
                                nots3.append([nota3, int(str(valor3).split('.')[0])])
                            else:
                                valor3 = mat.PromedioParcial(nota3)
                                nots3.append([nota3, valor3])
                        valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                        if valor2.count() != 0:
                            valor2 = valor2[0].Valor
                            nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                        else:
                            valor2 = mat.PromedioParcial(nota2)
                            nots2.append([nota2, nots3, valor2])
                        nots3 = []
                    valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
                    if valor1.count() != 0:
                        valor1 = valor1[0].Valor
                        nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
                    else:
                        valor1 = mat.PromedioParcial(nota1)
                        nots1.append([nota1, nots2, valor1])
                    nots2 = []
                registros.append([mat, nots1, id_nota_final, int(str(valor_final).split('.')[0])])
                nots1 = []
        # ----------------------------------
        ident = []
        formulas = []
        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')

        for nota in notas:
            ident.append(nota)

        for nota in notas1:
            subnotas = Nota.objects.filter(NotaPadre=nota)
            k = 1
            formula = ''
            suma = Decimal('0.00')
            for subnota in subnotas:
                if k == 1:
                    formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
                else:
                    formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
                k += 1
                suma += Decimal(nota.Peso)
            formula += ')/%s' % str(suma)
            formulas.append(formula)

        return render_to_response("Asistencias/PeriodoCurso/view_notas_nuevo.html",
                                  {"periodocurso": periodocurso, "registros": registros, "notas": ident,
                                   "formulas": formulas, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Asistencias/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def view_guardar_notas(request, id_periodocurso):
    if request.user.is_authenticated() and request.user.has_perm('Notas.change_notasalumno') or request.user.has_perm(
            'Notas.add_notasalumno') or request.user.has_perm('Notas.read_notasalumno'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a href='../../../../'>Inicio</a>"
            return render_to_response("Docentes/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        vnotas = request.POST['vnotas']
        rsp = request.POST['vnotas'].split('|')
        pps = {}
        for r in rsp:
            if r != '':
                id_matriculado = r.split('-')[0].split('_')[1]
                id_nota = r.split('-')[0].split('_')[0]
                valor = r.split('-')[1]
                notasalumno = NotasAlumno.objects.get(MatriculaCursos__id=id_matriculado, Nota__id=id_nota)
                notasalumno.Valor = valor
                hora = datetime.datetime.now()
                notasalumno.save(usuario=request.user, hora=hora)

                nota_promedio = NotasAlumno.objects.get(MatriculaCursos__id=id_matriculado, Nota__Nivel='0')
                if nota_promedio != notasalumno:
                    nota_promedio.Valor = nota_promedio.MatriculaCursos.Promedio1()
                    hora = datetime.datetime.now()
                    nota_promedio.save(usuario=request.user, hora=hora)

                    if notasalumno.Nota.NotaPadre.Nivel != '0':
                        pps['%s_%s' % (notasalumno.Nota.NotaPadre.id,
                                       notasalumno.MatriculaCursos.id)] = notasalumno.MatriculaCursos.PromedioParcial(
                            notasalumno.Nota.NotaPadre)
                    else:
                        promedio = int(str(notasalumno.MatriculaCursos.Promedio()).split('.')[0])
                        pps['%s_%s' % (notasalumno.Nota.NotaPadre.id, notasalumno.MatriculaCursos.id)] = promedio
                        pps['ps_%s_%s' % (notasalumno.Nota.NotaPadre.id, notasalumno.MatriculaCursos.id)] = promedio
                    pps[notasalumno.MatriculaCursos.id] = notasalumno.MatriculaCursos.Promedio1()
                else:
                    pps['ps_%s_%s' % (notasalumno.Nota.id, notasalumno.MatriculaCursos.id)] = notasalumno.Valor
                    # pps[notasalumno.MatriculaCursos.id] = notasalumno.MatriculaCursos.Promedio1()
        for pp in pps.items():
            vnotas += '%s-%s|' % (pp[0], pp[1])
        return HttpResponse(vnotas)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Asistencias/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
