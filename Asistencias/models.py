# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone

from Horarios.models import Horario
from Matriculas.models import MatriculaCursos

# Create your models here.
ASISTENCIA_CHOICES = (
    ('Asistio', 'Asistió'),
    ('Falto', 'Falto'),
    ('Tardanza', 'Tardanza'),
    ('Justifico', 'Justifico'),
)


class Asistencia(models.Model):
    Horario = models.ForeignKey(Horario, verbose_name="Horario")
    MatriculaCursos = models.ForeignKey(MatriculaCursos, verbose_name="Matriculado")
    Estado = models.CharField("Asistencia", max_length=10, choices=ASISTENCIA_CHOICES)
    Fecha = models.DateField("Fecha", default=timezone.now)

    def __unicode__(self):
        return u'%s - %s(%s %s %s)' % (
            self.MatriculaCursos.MatriculaCiclo.Alumno, self.MatriculaCursos.PeriodoCurso.Curso, self.Horario.Dia,
            self.Horario.HoraInicio, self.Horario.HoraFin)

    class Meta:
        verbose_name = "Asistencia"
        verbose_name_plural = "Asistencias"


class AsistenciaDocente(models.Model):
    Horario = models.ForeignKey(Horario, verbose_name="Horario")
    TemaClase = models.TextField()
    FechaClase = models.DateField("Fecha")
    Fecha = models.DateTimeField("Fecha", auto_now_add=True)
    Observaciones = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u'%s - %s - %s(%s %s %s) - %s' % (
            self.Fecha.strftime("%d %b %Y"), self.Horario.PeriodoCurso.Docente, self.Horario.PeriodoCurso.Curso,
            self.Horario.Dia, self.Horario.HoraInicio, self.Horario.HoraFin, self.TemaClase)

    def fecha(self):
        return u'%s' % self.Fecha.strftime("%d %b %Y")

    def docente(self):
        return u'%s' % self.Horario.PeriodoCurso.Docente

    def grupo(self):
        return u'%s' % self.Horario.PeriodoCurso.Grupo

    def curso(self):
        return u'%s' % self.Horario.PeriodoCurso.Curso

    def carrera(self):
        return u'%s' % self.Horario.PeriodoCurso.Curso.Carrera

    def horario(self):
        return self.Horario.Etiqueta()

    def temaclase(self):
        return u'%s' % self.TemaClase

    temaclase.short_description = "Tema de Clase"

    class Meta:
        verbose_name = "Asistencia de Docentes"
        verbose_name_plural = "Asistencias de Docentes"
