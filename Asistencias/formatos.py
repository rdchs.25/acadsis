# -*- coding: latin-1 -*-

from django.conf import settings

from Asistencias.models import Asistencia


def ficha_asistencia(c, alumnos, fechas, periodocurso, idhorarios):
    c.setFont("Helvetica", 4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    altoA4 = 595
    anchoA4 = 842
    margenLeft = 20
    margenRight = 60
    interlineado = 20
    i = len(fechas)
    c.setStrokeColorRGB(0.90588, 0.41176, 0.31098)

    b = len(alumnos)
    j = 1
    for e in range(0, b, 20):
        matriculados = alumnos[0 + e:20 + e]
        # --------------------------
        # Lineas Formulario
        # --------------------------
        c.setFont("Helvetica", 14)
        c.drawString(330, 540, "EVALUACIÓN Y REGISTRO")
        c.setFont("Helvetica", 14)
        c.drawString(360, 520, "Ficha de Asistencia")

        c.setFont("Helvetica", 9)
        c.drawString(40, 480, "Semestre :")
        c.drawString(90, 480, str(periodocurso.Periodo))

        c.drawString(40, 460, "Ciclo :")
        c.drawString(90, 460, '%s Ciclo' % periodocurso.Curso.Ciclo)

        c.drawString(200, 480, "Docente :")
        c.drawString(250, 480, str(periodocurso.Docente))

        c.drawString(200, 460, "Curso :")
        c.drawString(250, 460, periodocurso.Curso.Nombre)

        c.drawString(600, 480, "Horario :")

        f = 0
        for horario in periodocurso.horario_set.all():
            c.drawString(650, 480 - f,
                         '%s %s %s %s' % (horario.Dia, horario.HoraInicio, horario.HoraFin, horario.Seccion))
            f += 10

        c.setStrokeColorRGB(0.70588, 0.01176, 0.05098)

        # datos de alumno
        c.drawString(25, 410, "Nº")
        c.drawString(45, 410, "CÓDIGO")
        c.drawString(125, 410, "APELLIDOS Y NOMBRES")
        c.drawString(285, 410, "G.H.")
        c.saveState()
        c.rotate(90)
        c.drawString(392, -325, "CARRERA")
        c.restoreState()

        c.line(margenLeft, 440, anchoA4 - margenRight - 452, 440)  # 1era Linea ...
        c.line(margenLeft, 390, anchoA4 - margenRight - 452, 390)  # 2da Linea ...
        c.line(margenLeft, 30, anchoA4 - margenRight - 452, 30)  # 3da Linea ...
        c.drawString(margenLeft, 10,
                     "(*): Aquellos estudiantes que todavía no se han matriculado y no registran asistencias.")

        for g in range(20, 360, 18):
            c.line(margenLeft, 390 - g, anchoA4 - margenRight - 452, 390 - g)  # lineas de registros,18

        c.line(margenLeft, 440, margenLeft, 30)  # Linea Vertical 1
        c.line(margenLeft + 20, 440, margenLeft + 20, 30)  # Linea Vertical 2
        c.line(margenLeft + 65, 440, margenLeft + 65, 30)  # Linea Vertical 3
        c.line(margenLeft + 260, 440, margenLeft + 260, 30)  # Linea Vertical 4
        c.line(margenLeft + 290, 440, margenLeft + 290, 30)  # Linea Vertical 5
        c.line(margenLeft + 310, 440, margenLeft + 310, 30)  # Linea Vertical 6

        h = 0
        # j=1
        for alumno in matriculados:
            if j >= 10:
                c.drawString(25, 375 - h, str(j))
            else:
                c.drawString(28, 375 - h, str(j))
            c.drawString(45, 375 - h, str(alumno.MatriculaCiclo.Alumno.Codigo))
            c.drawString(90, 375 - h, str(alumno.MatriculaCiclo.Alumno))
            c.drawString(288, 375 - h, str(alumno.PeriodoCurso.Grupo))
            c.drawString(314, 375 - h, alumno.MatriculaCiclo.Alumno.Carrera.Codigo)

            k = 0
            asistio = 0
            falto = 0
            tardanza = 0
            for (fecha, horario) in zip(fechas, idhorarios):
                try:
                    asistencia = Asistencia.objects.filter(MatriculaCursos=alumno, Fecha=fecha['Fecha'],
                                                           Horario_id=horario['Horario_id'])[:1].get()

                    c.drawString(340 + k, 375 - h, str(asistencia.Estado[0]))
                    if str(asistencia.Estado[0]) == 'A':
                        asistio += 1
                    elif str(asistencia.Estado[0]) == 'F':
                        falto += 1
                    elif str(asistencia.Estado[0]) == 'T':
                        tardanza += 1
                except Asistencia.DoesNotExist:
                    c.drawString(340 + k, 375 - h, '*')
                k += 15

            if asistio >= 10:
                c.drawString(340 + k + 3, 375 - h, str(asistio))
            else:
                c.drawString(340 + k + 5, 375 - h, str(asistio))

            if falto >= 10:
                c.drawString(340 + k + 18, 375 - h, str(falto))
            else:
                c.drawString(340 + k + 20, 375 - h, str(falto))

            if tardanza >= 10:
                c.drawString(340 + k + 33, 375 - h, str(tardanza))
            else:
                c.drawString(340 + k + 35, 375 - h, str(tardanza))
            h += 18
            j += 1

        # datos de asistencias

        c.line(margenLeft + 315, 440, margenLeft + 315 + 15 * i, 440)
        c.line(margenLeft + 315, 390, margenLeft + 315 + 15 * i, 390)
        c.line(margenLeft + 315, 30, margenLeft + 315 + 15 * i, 30)

        for l in range(20, 360, 18):
            c.line(margenLeft + 315, 390 - l, margenLeft + 315 + 15 * i, 390 - l)  # lineas de registros,18

        c.line(margenLeft + 315, 440, margenLeft + 315, 30)

        m = 0
        for n in range(1, i + 1):
            c.line(margenLeft + 315 + 15 * n, 440, margenLeft + 315 + 15 * n, 30)
            ultimo_punto = 15 * n
            m += 15

        o = 0
        for fecha in fechas:
            c.saveState()
            c.rotate(90)
            c.drawString(392, -346 - o, str(fecha['Fecha']))
            c.restoreState()
            o += 15

        # datos resumen

        c.saveState()
        c.rotate(90)
        c.drawString(396, -346 - m - 5, 'ASISTIÓ')
        c.drawString(400, -346 - m - 20, 'FALTÓ')
        c.drawString(391, -346 - m - 35, 'TARDANZA')
        c.restoreState()

        c.line(margenLeft + 315 + 15 * i + 5, 440, margenLeft + 315 + 15 * i + 50, 440)
        c.line(margenLeft + 315 + 15 * i + 5, 390, margenLeft + 315 + 15 * i + 50, 390)
        c.line(margenLeft + 315 + 15 * i + 5, 30, margenLeft + 315 + 15 * i + 50, 30)

        for p in range(20, 360, 18):
            c.line(margenLeft + 315 + 15 * i + 5, 390 - p, margenLeft + 315 + 15 * i + 50, 390 - p)

        c.line(margenLeft + 315 + 15 * i + 5, 440, margenLeft + 315 + 15 * i + 5, 30)

        for q in range(1, 4):
            c.line(margenLeft + 315 + 15 * i + 5 + 15 * q, 440, margenLeft + 315 + 15 * i + 5 + 15 * q, 30)

        # --------------------------
        # Logo
        # --------------------------
        logo = settings.MEDIA_ROOT + 'logoHD.jpg'
        c.drawImage(logo, 30, 520, width=140, height=62)
        c.showPage()
        c.save()
