# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Asistencia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Estado', models.CharField(max_length=10, verbose_name=b'Asistencia', choices=[(b'Asistio', b'Asisti\xc3\xb3'), (b'Falto', b'Falto'), (b'Tardanza', b'Tardanza'), (b'Justifico', b'Justifico')])),
                ('Fecha', models.DateField(default=django.utils.timezone.now, verbose_name=b'Fecha')),
            ],
            options={
                'verbose_name': 'Asistencia',
                'verbose_name_plural': 'Asistencias',
            },
        ),
        migrations.CreateModel(
            name='AsistenciaDocente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('TemaClase', models.TextField()),
                ('FechaClase', models.DateField(verbose_name=b'Fecha')),
                ('Fecha', models.DateTimeField(auto_now_add=True, verbose_name=b'Fecha')),
                ('Observaciones', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Asistencia de Docentes',
                'verbose_name_plural': 'Asistencias de Docentes',
            },
        ),
    ]
