# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Asistencias', '0001_initial'),
        ('Horarios', '0001_initial'),
        ('Matriculas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='asistenciadocente',
            name='Horario',
            field=models.ForeignKey(verbose_name=b'Horario', to='Horarios.Horario'),
        ),
        migrations.AddField(
            model_name='asistencia',
            name='Horario',
            field=models.ForeignKey(verbose_name=b'Horario', to='Horarios.Horario'),
        ),
        migrations.AddField(
            model_name='asistencia',
            name='MatriculaCursos',
            field=models.ForeignKey(verbose_name=b'Matriculado', to='Matriculas.MatriculaCursos'),
        ),
    ]
