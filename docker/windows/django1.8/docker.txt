docker run -i -t \
  -p 8086:8086 -p 8087:8087 --name acadsis -v /home/rchaname/Proyectos/python:/home/rchaname \
  -e HOST_USER_NAME=$USERNAME \
  -e HOST_USER_ID=$UID \
  -e HOST_GROUP_NAME=$(id -g -n $USERNAME || echo $USERNAME) \
  -e HOST_GROUP_ID=$(id -g $USERNAME) \
  django1.8:python2.7 bash

docker run -ti --name=acadsis \
  -v /home/rchaname/Projects/python/:/projects \
  -v /etc/group:/etc/group:ro \
  -v /etc/passwd:/etc/passwd:ro -u $( id -u $USER ):$( id -g $USER ) \
  django1.8:python2.7 bash