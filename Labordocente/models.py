# -*- coding: utf-8 -*-
from django.db import models

from Carreras.models import Carrera
from Docentes.models import Docente
from Periodos.models import Periodo


class Tipolabor(models.Model):
    Nombre = models.CharField(max_length=60, verbose_name="Nombre")

    def __unicode__(self):
        return u'%s' % self.Nombre

    class Meta:
        verbose_name = "Tipo de labor"
        verbose_name_plural = "Tipos de labor"


class Labor(models.Model):
    Codigo = models.CharField(max_length=9)
    Tipolabor = models.ForeignKey(Tipolabor, verbose_name="Tipo de labor")
    Nombre = models.CharField(max_length=60)
    Periodo = models.ForeignKey(Periodo, verbose_name="Período")
    Carrera = models.ForeignKey(Carrera, null=True, blank=True)
    Estado = models.BooleanField("Activada", default=True)
    Observacion = models.TextField("Observación", blank=True, null=True)

    def __unicode__(self):
        return u'%s, %s, %s' % (self.Nombre, self.Periodo, self.Carrera)

    class Meta:
        verbose_name = "Labor de docentes"
        verbose_name_plural = "Labores de docente"


def obtenerpath_periodolabor(instance, filename):
    ext = filename.split('.')[-1]
    return u'documentos/%s-%s/%s/%s_%s.%s' % (
        instance.Periodo.Anio, instance.Periodo.Semestre, instance.Grupo, instance.Labor.Codigo,
        instance.Docente.username, ext)


class PeriodoLabor(models.Model):
    Periodo = models.ForeignKey(Periodo)
    Labor = models.ForeignKey(Labor)
    Grupo = models.CharField(max_length=5)
    Docente = models.ForeignKey(Docente)
    Observaciones = models.TextField(blank=True, null=True)
    Documento = models.FileField(upload_to=obtenerpath_periodolabor, blank=True, null=True, verbose_name="Documento",
                                 editable=False)

    def __unicode__(self):
        return u'%s %s %s' % (self.Periodo, self.Grupo, self.Labor.Nombre)

    def etiqueta(self):
        return u'%s-%s' % (self.Grupo, self.Labor.Nombre)

    def carreralabor(self):
        return u'%s' % self.Labor.Carrera.Carrera

    carreralabor.short_description = 'Carrera'

    def save(self):
        return super(PeriodoLabor, self).save(using='default')

    class Meta:
        verbose_name = "Grupo Horario de labor"
        verbose_name_plural = "Grupos Horarios de labor"
        unique_together = (("Periodo", "Labor", "Grupo", "Docente",),)
        permissions = (("read_periodolabor", "Observar Grupos Horario de labor"),)


DIA_CHOICES = (
    ('Lunes', 'Lunes'),
    ('Martes', 'Martes'),
    ('Miercoles', 'Miercoles'),
    ('Jueves', 'Jueves'),
    ('Viernes', 'Viernes'),
    ('Sabado', 'Sabado'),
    ('Domingo', 'Domingo'),
)


class Horario(models.Model):
    PeriodoLabor = models.ForeignKey(PeriodoLabor, verbose_name="Labor")
    Dia = models.CharField("Día", max_length=10, choices=DIA_CHOICES)
    HoraInicio = models.TimeField()
    HoraFin = models.TimeField()
    Seccion = models.CharField("Sección", max_length=50)

    def __unicode__(self):
        return u'%s - %s (%s-%s)' % (self.PeriodoLabor, self.Dia, self.HoraInicio, self.HoraFin)

    def etiqueta(self):
        return u'%s (%s - %s)' % (self.Dia, self.HoraInicio, self.HoraFin)

    class Meta:
        verbose_name = "Horario"
        verbose_name_plural = "Horarios"
        unique_together = (("PeriodoLabor", "Dia", "HoraInicio", "HoraFin", "Seccion",),)
        permissions = (("read_horario", "Observar Configuracion de Horarios"),)


class AsistenciaDocente(models.Model):
    Horario = models.ForeignKey(Horario, verbose_name="Horario")
    TemaClase = models.TextField()
    FechaClase = models.DateField("Fecha")
    Fecha = models.DateTimeField("Fecha", auto_now_add=True)
    Observaciones = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u'%s - %s - %s(%s %s %s) - %s' % (
            self.Fecha.strftime("%d %b %Y"), self.Horario.PeriodoLabor.Docente, self.Horario.PeriodoLabor.Labor,
            self.Horario.Dia, self.Horario.HoraInicio, self.Horario.HoraFin, self.TemaClase)

    def fecha(self):
        return u'%s' % self.Fecha.strftime("%d %b %Y")

    def docente(self):
        return u'%s' % self.Horario.PeriodoLabor.Docente

    def grupo(self):
        return u'%s' % self.Horario.PeriodoLabor.Grupo

    def labor(self):
        return u'%s' % self.Horario.PeriodoLabor.Labor

    def carrera(self):
        return u'%s' % self.Horario.PeriodoLabor.Labor.Carrera

    def horario(self):
        return self.Horario.etiqueta()

    def temaclase(self):
        return u'%s' % self.TemaClase

    temaclase.short_description = "Tema de Clase"

    class Meta:
        verbose_name = "Asistencia de Docentes"
        verbose_name_plural = "Asistencias de Docentes"
