# -*- coding: utf-8 -*-
from django.conf.urls import url

from Horarios.views import index_horario
from Labordocente.views import index_labor, index_periodolabor, ver_periodolabor, listado_periodo_labor, \
    index_periodo_labor, agregar_grupo_periodolabor, editar_grupo_periodolabor, horarios_periodolabor, \
    agregar_horario_labor, editar_horario_labor, app_horarios_labor, buscar_horario_aula_labor, buscar_periodolabor, \
    traer_periodolabor, descargar_horarios_labor, eliminar_horario_labor, modificar_horario_labor, \
    guardar_horario_labor, crear_grupo_horario_labor

app_name = 'Labordocente'

urlpatterns = [
    url(r'^Labordocente/periodolabor/$', ver_periodolabor),
    url(r'^Labordocente/periodolabor/(\d+)/$', listado_periodo_labor),
    url(r'^Labordocente/periodolabor/(\d+)/(\d+)/$', index_periodo_labor),
    url(r'^Labordocente/periodolabor/agregar/(\d+)/(\d+)/$', agregar_grupo_periodolabor),
    url(r'^Labordocente/periodolabor/editar/(\d+)/$', editar_grupo_periodolabor),
    url(r'^Labordocente/periodolabor/ver/(\d+)/$', horarios_periodolabor),
    url(r'^Labordocente/periodolabor/agregarhorario/(\d+)/$', agregar_horario_labor),
    url(r'^Labordocente/periodolabor/editarhorario/(\d+)/$', editar_horario_labor),
    url(r'^Labordocente/labor/elegirperiodo/$', index_labor),
    url(r'^Labordocente/periodolabor/elegirperiodo/$', index_periodolabor),
    url(r'^Labordocente/periodolabor/horarios/elegirperiodo/$', index_horario),
    url(r'^Labordocente/labor/creargrupohorario/$', crear_grupo_horario_labor),
    url(r'^Labordocente/periodolabor/horarios/$', app_horarios_labor),
    url(r'^Labordocente/periodolabor/horarios/buscar/$', buscar_horario_aula_labor),
    url(r'^Labordocente/periodolabor/horarios/buscar_labor/$', buscar_periodolabor),
    url(r'^Labordocente/periodolabor/horarios/traer_periodolabor/$', traer_periodolabor),
    url(r'^Labordocente/periodolabor/horarios/descargar/$', descargar_horarios_labor),
    url(r'^Labordocente/periodolabor/horarios/guardar_horario/$', guardar_horario_labor),
    url(r'^Labordocente/periodolabor/horarios/editar_horario/$', modificar_horario_labor),
    url(r'^Labordocente/periodolabor/horarios/eliminar_horario/$', eliminar_horario_labor),
]
