# -*- coding: utf-8 -*-
from django import forms

from Docentes.models import Docente
from Horarios.widgets import SelectTimeWidget
from Periodos.models import Periodo
from .models import PeriodoLabor, Horario

SECCION_CHOICES = (
    ('Aula 01', 'Aula 01'),
    ('Aula 02', 'Aula 02'),
    ('Aula 03', 'Aula 03'),
    ('Aula 04', 'Aula 04'),
    ('Aula 05', 'Aula 05'),
    ('Aula 06', 'Aula 06'),
    ('Aula 07', 'Aula 07'),
    ('Aula 08', 'Aula 08'),
    ('Aula 09', 'Aula 09'),
    ('Aula 10', 'Aula 10'),
    ('Aula 11', 'Aula 11'),
    ('Aula 12', 'Aula 12'),
    ('Aula 13', 'Aula 13'),
    ('Aula 14', 'Aula 14'),
    ('Aula 15', 'Aula 15'),
    ('Aula 16', 'Aula 16'),
    ('Aula 17', 'Aula 17'),
    ('Aula 18', 'Aula 18'),
    ('Aula 19', 'Aula 19'),
    ('Aula 20', 'Aula 20'),
    ('Aula 21', 'Aula 21'),
    ('Aula 22', 'Aula 22'),
    ('Aula 23', 'Aula 23'),
    ('Aula 24', 'Aula 24'),
    ('Aula 25', 'Aula 25'),
    ('Lab. Inf. 01', 'Lab. Inf. 01'),
    ('Lab. Inf. 02', 'Lab. Inf. 02'),
    ('Lab. Inf. 03', 'Lab. Inf. 03'),
    ('Lab. Multi. 01', 'Lab. Multi. 01'),
    ('Lab. Multi. 02', 'Lab. Multi. 02'),
)


class IndexPeriodoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class GrupoPeriodoLaborForm(forms.ModelForm):
    Grupo = forms.CharField(min_length=3, max_length=3, required=True, label="Grupo Horario")
    Docente = forms.ModelChoiceField(
        queryset=Docente.objects.filter(Estado=True).order_by('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'),
        widget=forms.Select(), required=True, label='Docente')
    Observaciones = forms.CharField(widget=forms.Textarea, required=False, label="Comentario")
    Periodo1 = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    Labor1 = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    class Meta:
        model = PeriodoLabor
        exclude = ('Periodo', 'Labor',)


# agregar grupos horario a un curso de carrera en un periodo determinado
class HorarioForm(forms.ModelForm):
    HoraInicio = forms.TimeField(widget=SelectTimeWidget(), label='Inicio')
    HoraFin = forms.TimeField(widget=SelectTimeWidget(), label='Fin')
    PeriodoLabor1 = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    Seccion = forms.ChoiceField(choices=SECCION_CHOICES, required=True, label='Aula')

    class Meta:
        model = Horario
        fields = ('Dia', 'HoraInicio', 'HoraFin', 'Seccion',)


class IndexHorarioForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class DocenteForm(forms.Form):
    Grupo = forms.CharField(min_length=3, max_length=3, required=True, label="Grupo Horario")
    Docente = forms.ModelChoiceField(
        queryset=Docente.objects.all().order_by('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'), widget=forms.Select(),
        required=True, label='Docente')
