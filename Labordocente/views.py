# -*- coding: utf-8 -*-
import simplejson
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from Carreras.models import Carrera
from Labordocente.models import PeriodoLabor, Labor, Tipolabor
from Periodos.models import Periodo
from .forms import IndexPeriodoForm, GrupoPeriodoLaborForm, HorarioForm, IndexHorarioForm, DocenteForm
from .models import Horario


@csrf_protect
def index_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Labor.change_labor') or request.user.has_perm(
            'Labor.add_labor') or request.user.has_perm('PeriodoLabor.read_periodolabor'):
        if request.method == 'POST':
            form = IndexPeriodoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?Periodo__id__exact=" + str(periodo.id))
        else:
            form = IndexPeriodoForm()
        return render_to_response('Labordocente/Labor/index_labor.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def index_periodolabor(request):
    if request.user.is_authenticated() and request.user.has_perm(
            'PeriodoLabor.change_periodolabor') or request.user.has_perm('PeriodoLabor.add_periodolabor') or \
            request.user.has_perm('PeriodoLabor.read_periodolabor'):
        if request.method == 'POST':
            form = IndexPeriodoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?p=" + str(periodo.id))
        else:
            form = IndexPeriodoForm()
        return render_to_response('Labordocente/PeriodoLabor/index_periodolabor.html',
                                  {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def index_horariolabor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horario.change_horario') or request.user.has_perm(
            'Horario.add_horario') or request.user.has_perm('PeriodoLabor.read_periodolabor'):
        if request.method == 'POST':
            form = IndexPeriodoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?Periodo__id__exact=" + str(periodo.id))
        else:
            form = IndexPeriodoForm()
        return render_to_response('Labordocente/Labor/index_labor.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def ver_periodolabor(request):
    periodolabors = []
    if request.user.is_authenticated() and request.user.has_perm('Labordocente.read_periodolabor'):
        if request.method == 'POST':
            action = request.POST['action']
            all = request.POST['select_across']

            if all == '0':
                periodolabors = request.POST.getlist('_selected_action')
                grupos = []
                for periodolabor_id in periodolabors:
                    periodolabor = PeriodoLabor.objects.get(id=periodolabor_id)
                    grupos.append(periodolabor)
            else:
                query = request.GET.get('q', '')
                query1 = request.GET.get('p', '')

                results = PeriodoLabor.objects.all()

                if query1:
                    results1 = results.filter(Periodo__id=query1)
                else:
                    results1 = results

                if query:
                    qset = (
                            Q(Labor__Codigo__icontains=query) |
                            Q(Labor__Nombre__icontains=query) |
                            Q(Labor__Carrera__Carrera__icontains=query) |
                            Q(Grupo__icontains=query) |
                            Q(Docente__ApellidoPaterno__icontains=query) |
                            Q(Docente__ApellidoMaterno__icontains=query) |
                            Q(Docente__Nombres__icontains=query)
                    )
                    results4 = results1.filter(qset).order_by(
                        'Labor__Tipolabor', 'Labor__Nombre')
                else:
                    results4 = results1.order_by(
                        'Labor__Tipolabor', 'Labor__Nombre')

                grupos = []
                for periodolabor_id in results4:
                    periodolabor = PeriodoLabor.objects.get(
                        id=periodolabor_id.id)
                    grupos.append(periodolabor)
        else:
            query = request.GET.get('q', '')
            query1 = request.GET.get('p', '')

            carreras = Carrera.objects.all()
            periodos = Periodo.objects.all()

            ciclos = []
            for i in range(1, 11):
                ciclo = str(i).rjust(2).replace(" ", "0")
                ciclos.append(ciclo)

            results = PeriodoLabor.objects.all()

            if query1:
                results1 = results.filter(Periodo__id=query1)
            else:
                results1 = results

            if query:
                qset = (
                        Q(Labor__Codigo__icontains=query) |
                        Q(Labor__Nombre__icontains=query) |
                        Q(Labor__Carrera__Carrera__icontains=query) |
                        Q(Grupo__icontains=query) |
                        Q(Docente__ApellidoPaterno__icontains=query) |
                        Q(Docente__ApellidoMaterno__icontains=query) |
                        Q(Docente__Nombres__icontains=query)
                )
                results4 = results1.filter(qset).order_by(
                    'Labor__Tipolabor', 'Labor__Nombre')
            else:
                results4 = results1.order_by(
                    'Labor__Tipolabor', 'Labor__Nombre')

            n_grupos = results4.count()
            paginator = Paginator(results4, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of
            # results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Labordocente/PeriodoLabor/ver_periodolabor.html",
                                      {"results": results, "paginator": paginator, "query": query, "query1": query1,
                                       "carreras": carreras, "periodos": periodos,
                                       "ciclos": ciclos, "n_grupos": n_grupos, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def listado_periodo_labor(request, periodolabor_id):
    if request.user.is_authenticated() and request.user.has_perm('Labordocente.read_periodolabor') or \
            request.user.has_perm('Labordocente.add_periodolabor') or \
            request.user.has_perm('Labordocente.change_periodolabor'):
        try:
            periodolabor = PeriodoLabor.objects.get(id=periodolabor_id)
        except PeriodoLabor.DoesNotExist:
            raise Http404

        periodo = periodolabor.Periodo
        labor = periodolabor.Labor
        return HttpResponseRedirect('../' + str(periodo.id) + '/' + str(labor.id) + '/')
    else:
        return HttpResponseRedirect('../../../../')


def index_periodo_labor(request, periodo_id, labor_id):
    if request.user.is_authenticated() and request.user.has_perm('Labor.read_periodolabor') or request.user.has_perm(
            'Labor.add_periodolabor') or request.user.has_perm('Labor.change_periodolabor'):
        labors = PeriodoLabor.objects.filter(Periodo__id=periodo_id, Labor__id=labor_id)
        n_grupos = labors.count()
        if n_grupos == 0:
            try:
                periodo = Periodo.objects.get(id=periodo_id)
                labor = Labor.objects.get(id=labor_id)
            except (Periodo.DoesNotExist, Labor.DoesNotExist):
                raise Http404
        else:
            existe_labor = PeriodoLabor.objects.filter(Periodo__id=periodo_id, Labor__id=labor_id)[0]
            periodo = existe_labor.Periodo
            labor = existe_labor.Labor

        paginator = Paginator(labors, 10)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)

        return render_to_response("Labordocente/PeriodoLabor/periodo_labor.html",
                                  {"user": request.user, "periodo": periodo, "labor": labor, "labores": results,
                                   "n_grupos": n_grupos}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def agregar_grupo_periodolabor(request, periodo_id, labor_id):
    if request.user.is_authenticated() and request.user.has_perm('Labor.add_periodolabor'):
        try:
            periodo = Periodo.objects.get(id=periodo_id)
            labor = Labor.objects.get(id=labor_id)
        except (Periodo.DoesNotExist, Labor.DoesNotExist):
            raise Http404
        if request.method == 'POST':
            form = GrupoPeriodoLaborForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo1']
                labor = form.cleaned_data['Labor1']
                grupo = form.cleaned_data['Grupo']
                docente = form.cleaned_data['Docente']
                observaciones = form.cleaned_data['Observaciones']

                grabar_periodolabor = PeriodoLabor(Periodo_id=periodo_id, Labor_id=labor_id, Grupo=grupo,
                                                   Docente=docente, Observaciones=observaciones)
                grabar_periodolabor.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Labordocente/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = GrupoPeriodoLaborForm(initial={"Periodo1": periodo_id, "Labor1": labor_id})

        return render_to_response("Labordocente/PeriodoLabor/agregar_grupo_periodolabor.html",
                                  {"form": form, "user": request.user, "periodo": periodo, "labor": labor},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../../../'>Inicio</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_grupo_periodolabor(request, periodo_labor_id):
    if request.user.is_authenticated() and request.user.has_perm('Labor.change_periodolabor'):
        try:
            periodo_labor = PeriodoLabor.objects.get(id=periodo_labor_id)
        except PeriodoLabor.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = GrupoPeriodoLaborForm(request.POST)
            if form.is_valid():
                grupo = form.cleaned_data['Grupo']
                docente = form.cleaned_data['Docente']
                observaciones = form.cleaned_data['Observaciones']

                periodo_labor.Grupo = grupo
                periodo_labor.Docente = docente
                periodo_labor.Observaciones = observaciones
                periodo_labor.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Labordocente/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = GrupoPeriodoLaborForm(
                initial={"Periodo1": periodo_labor.Periodo.id, "Labor1": periodo_labor.Labor.id,
                         "Grupo": periodo_labor.Grupo, "Docente": periodo_labor.Docente.id,
                         "Observaciones": periodo_labor.Observaciones})

        return render_to_response("Labordocente/PeriodoLabor/editar_grupo_periodolabor.html",
                                  {"form": form, "user": request.user, "periodo_labor": periodo_labor},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../../'>Inicio</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def horarios_periodolabor(request, periodolabor_id):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario') or request.user.has_perm(
            'Horarios.change_horario') or request.user.has_perm('Horarios.read_horario') or \
            request.user.has_perm('Notas.add_nota') or request.user.has_perm('Notas.change_nota') or \
            request.user.has_perm('Notas.read_nota'):
        try:
            periodolabor = PeriodoLabor.objects.get(id=periodolabor_id)
            horarios = Horario.objects.filter(PeriodoLabor=periodolabor_id)
        except PeriodoLabor.DoesNotExist:
            raise Http404

        return render_to_response("Labordocente/Horario/contenido_labor.html",
                                  {"periodolabor": periodolabor, "horarios": horarios, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def agregar_horario_labor(request, periodolabor_id):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario'):
        try:
            periodolabor = PeriodoLabor.objects.get(id=periodolabor_id)
        except PeriodoLabor.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = HorarioForm(request.POST)
            if form.is_valid():
                dia = form.cleaned_data['Dia']
                horainicio = form.cleaned_data['HoraInicio']
                horafin = form.cleaned_data['HoraFin']
                seccion = form.cleaned_data['Seccion']

                grabar_horario = Horario(PeriodoLabor_id=periodolabor_id, Dia=dia, HoraInicio=horainicio,
                                         HoraFin=horafin, Seccion=seccion)
                grabar_horario.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Labordocente/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = HorarioForm(initial={"PeriodoLabor1": periodolabor_id})

        return render_to_response("Labordocente/Horario/agregar_horario.html",
                                  {"form": form, "user": request.user, "periodolabor": periodolabor},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_horario_labor(request, horario_id):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        try:
            horario = Horario.objects.get(id=horario_id)
        except Horario.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = HorarioForm(request.POST)
            if form.is_valid():
                dia = form.cleaned_data['Dia']
                horainicio = form.cleaned_data['HoraInicio']
                horafin = form.cleaned_data['HoraFin']
                seccion = form.cleaned_data['Seccion']

                horario.Dia = dia
                horario.HoraInicio = horainicio
                horario.HoraFin = horafin
                horario.Seccion = seccion
                horario.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Labordocente/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = HorarioForm(
                initial={"PeriodoLabor1": horario.PeriodoLabor.id, "Dia": horario.Dia, "HoraInicio": horario.HoraInicio,
                         "HoraFin": horario.HoraFin, "Seccion": horario.Seccion})

        return render_to_response("Labordocente/Horario/editar_horario.html",
                                  {"form": form, "user": request.user, "horario": horario},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def index_horario(request):
    if request.user.is_authenticated() and request.user.has_perm('Labordocente.change_horario') or \
            request.user.has_perm('Labordocente.add_horario') or \
            request.user.has_perm('Labordocente.read_horario'):
        if request.method == 'POST':
            form = IndexHorarioForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?p=" + str(periodo.id))
        else:
            form = IndexHorarioForm()
        return render_to_response('Labordocente/Horario/index_horario.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


def app_horarios_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Labordocente.read_horario'):
        id_periodo = request.GET.get('p', '')
        try:
            periodo = Periodo.objects.get(id=id_periodo)
            tipolabores = Tipolabor.objects.all()
            labores = Labor.objects.filter(Periodo=periodo)
        except Periodo.DoesNotExist:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Labordocente/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        return render_to_response("Labordocente/Horario/interfaz_horarios.html",
                                  {"user": request.user, "periodo": periodo, "tipolabores": tipolabores,
                                   "labores": labores},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def buscar_horario_aula_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        consulta = str(request.POST['consulta'])
        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        n_aula = consulta.split('-')[0]
        carrera = consulta.split('-')[1]
        ciclo = consulta.split('-')[2]
        if carrera != '' and ciclo != '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__Tipolabor__id=carrera,
                                              PeriodoLabor__Labor__id=ciclo)
        elif carrera != '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__Tipolabor__id=carrera)
        else:
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula)

        cadena = ''
        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}

        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoLabor.Labor.Tipolabor.id, horario.PeriodoLabor.Labor.Nombre,
                horario.PeriodoLabor.Docente.username, horario.PeriodoLabor.id, dia, inicio, fin,
                horario.PeriodoLabor.Labor.id, horario.id, horario.PeriodoLabor.Grupo)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def buscar_periodolabor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario'):
        consulta = str(request.POST['consulta'])
        tipolabor = consulta.split('-')[0]
        labor = consulta.split('-')[1]
        cadena = ''
        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        if tipolabor != '' and labor != '':
            periodolabores = PeriodoLabor.objects.filter(Periodo__id=ID_PERIODO, Labor__Tipolabor__id=tipolabor,
                                                         Labor__id=labor)
        elif tipolabor == '':
            cadena += '0|Primero elija una tipolabor|id-nombre&&'
            return HttpResponse(cadena)
        elif labor == '':
            cadena += '0|Primero elija un labor|id-nombre&&'
            return HttpResponse(cadena)
        else:
            cadena += '0|Primero elija una tipolabor y labor|id-nombre&&'
            return HttpResponse(cadena)

        cadena += '1|'
        for percur in periodolabores:
            id_labor = percur.id
            nombre = percur.Labor.Nombre
            grupo = percur.Grupo
            cadena += '%s-%s&&' % (id_labor, grupo + ' ' + nombre)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def traer_periodolabor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        consulta = str(request.POST['consulta'])
        idperiodolabor = consulta
        cadena = ''
        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        try:
            periodolabor = PeriodoLabor.objects.get(id=idperiodolabor)
            carrera = periodolabor.Labor.Tipolabor.id
            ciclo = periodolabor.Labor.id
            carrera_nombre = periodolabor.Labor.Tipolabor.Nombre
            ciclo_nombre = periodolabor.Labor.Nombre
            labor = periodolabor.Grupo + ' ' + periodolabor.Labor.Nombre
            cadena += '%s-%s-%s-%s-%s' % (carrera, ciclo, labor, carrera_nombre, ciclo_nombre)
            return HttpResponse(cadena)
        except PeriodoLabor.DoesNotExist:
            pass
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def descargar_horarios_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        from xlrd import open_workbook
        from xlutils.copy import copy
        from xlwt import XFStyle, Borders, Font, Alignment

        # Estilos de celda registros
        borders_registros = Borders()
        borders_registros.left = Borders.THIN
        borders_registros.right = Borders.THIN
        borders_registros.top = Borders.THIN
        borders_registros.bottom = Borders.THIN

        alignment_registros = Alignment()
        alignment_registros.horz = Alignment.HORZ_CENTER
        alignment_registros.vert = Alignment.VERT_JUSTIFIED

        alignment_celda_horarios = Alignment()
        alignment_celda_horarios.horz = Alignment.HORZ_CENTER
        alignment_celda_horarios.vert = Alignment.VERT_CENTER

        font_h1 = Font()
        font_h1.bold = True
        font_h1.height = 0x0140  # 18

        font_h2 = Font()
        font_h2.bold = True
        font_h2.height = 0x00F0  # 12

        font_h6 = Font()
        font_h6.height = 0x00B4  # 9

        style_registros = XFStyle()
        style_registros.borders = borders_registros
        style_registros.alignment = alignment_registros
        style_registros.font = font_h6

        style_titulo = XFStyle()
        style_titulo.borders = borders_registros
        style_titulo.alignment = alignment_registros
        style_titulo.font = font_h1

        style_horas = XFStyle()
        style_horas.alignment = alignment_registros
        style_horas.borders = borders_registros
        style_horas.font = font_h2

        style_celda_horarios = XFStyle()
        style_celda_horarios.alignment = alignment_celda_horarios
        style_celda_horarios.borders = borders_registros
        style_celda_horarios.font = font_h2

        style_celda_labor = XFStyle()
        style_celda_labor.alignment = alignment_celda_horarios
        style_celda_labor.borders = borders_registros
        style_celda_labor.font = font_h6

        rb = open_workbook(settings.MEDIA_ROOT + u'formato_horarios.xls', formatting_info=True)
        wb = copy(rb)

        carreras = ['AM', 'AT', 'IA', 'IC', 'IS']
        carreras_disponibles = {'AM': 'Administracion y Marketing', 'IC': 'Ingenieria Comercial',
                                'IA': 'Ingenieria Ambiental', 'AT': 'Administracion Turistica',
                                'IS': 'Ingenieria de Sistemas'}
        ciclo_letras = {'01': '1er', '02': '2do', '03': '3er', '04': '4to', '05': '5to', '06': '6to', '07': '7mo',
                        '08': '8vo', '09': '9no', '10': '10mo'}
        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        dias_disponibles2 = ['LU', 'MA', 'MI', 'JU', 'VI', 'SA', 'DO']
        horarios_disponibles = ["08:00", "08:50", "09:40", "10:30", "10:50", "11:40", "12:30", "13:20", "14:10",
                                "15:00", "15:50", "16:40", "17:30", "17:50", "18:40", "19:30", "20:20", "21:10",
                                "22:00"]

        # Dimensiones GridHorarios
        NUM_CELDAS_TITULO = 3
        TAM_GRID_FILAS = 19
        TAM_GRID_COLUMNAS = 6
        OFFSET_SUP_GRID = NUM_CELDAS_TITULO + 1
        POSICION_GRID = NUM_CELDAS_TITULO + TAM_GRID_FILAS  # 8 + 3

        ID_PERIODO = request.GET.get('p', '')
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        contador_hoja = 0
        for carrera in carreras:
            wsheet = wb.get_sheet(contador_hoja)

            # Añadimos Hoja con el nombre de la carrera
            secciones = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO,
                                               PeriodoLabor__Labor__Tipolabor__id=carrera).order_by(
                'PeriodoLabor__Labor__id').values("Seccion").distinct()

            # Dibujamos en excel
            fGrid = 1
            cGrid = 1
            lista_secciones = []

            for seccion in secciones:

                # Horarios por Periodo y Carrera
                ciclos = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO,
                                                PeriodoLabor__Labor__Tipolabor__id=carrera,
                                                Seccion=seccion['Seccion']).order_by(
                    'PeriodoLabor__Labor__id').values("PeriodoLabor__Labor__id").distinct()

                for ciclo in ciclos:
                    elemento = [ciclo['PeriodoLabor__Labor__id'], seccion['Seccion']]
                    if not elemento in lista_secciones:
                        lista_secciones.append(elemento)
                        horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO,
                                                          PeriodoLabor__Labor__Tipolabor__id=carrera,
                                                          Seccion=seccion['Seccion'], PeriodoLabor__Labor__id=ciclo[
                                'PeriodoLabor__Labor__id']).order_by('PeriodoLabor__Labor__id', 'Seccion')
                        # Determinamos el TURNO de cada horario para mostrar solo las filas correspondientes del mismo.
                        turno = 'M'
                        n_manana = 0
                        n_tarde = 0
                        for horario in horarios:
                            # if int(horario.PeriodoLabor.Labor.Ciclo) == int(ciclo['PeriodoLabor__Labor__id']):
                            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
                            # print inicio, " // ", horario
                            # fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
                            if horarios_disponibles.index(inicio) > 6:
                                n_tarde += 1
                            else:
                                n_manana += 1

                        if n_tarde > n_manana:
                            turno = 'T'
                        else:
                            turno = 'M'

                        # Dibujamos Titulo del Horario
                        wsheet.write(fGrid, cGrid, unicode(carreras_disponibles[carrera]) + " - " + unicode(
                            ciclo_letras[ciclo['PeriodoLabor__Labor__id']]) + " Ciclo - " + seccion['Seccion'],
                                     style_titulo)
                        # # Dibujamos esquina Hora/Día :p
                        # wsheet.write( fGrid + 2, cGrid, "Hora / Dia" )

                        # # Dibujamos Horarios por defecto
                        rangohs = range(0, 18)

                        f = fGrid + 3
                        c = cGrid
                        for hd in rangohs:
                            wsheet.write(f, c, str(horarios_disponibles[hd] + " - " + horarios_disponibles[hd + 1]),
                                         style_celda_horarios)
                            # wsheet.col(c).height = 1000
                            f += 1

                        # Colocamos los horarios en sus respectivas posiciones
                        f = fGrid + 3
                        c = cGrid + 1

                        datos = ""
                        # Generamos cadena horario
                        for horario in horarios:
                            # if int(horario.PeriodoLabor.Labor.Ciclo) == int(ciclo['PeriodoLabor__Labor__id']):
                            # Todos los horarios
                            dia = dias[horario.Dia]
                            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
                            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]

                            datos += "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&&" % (
                                carrera, horario.Seccion, horario.PeriodoLabor.Labor.Nombre,
                                horario.PeriodoLabor.Docente.username, horario.PeriodoLabor.id, dia, inicio, fin,
                                horario.PeriodoLabor.Labor.Ciclo, horario.id)

                        datos_idem = datos.split("&&")[:-1]

                        origen_horario_fil = f
                        origen_horario_col = c

                        # ========================================================================================
                        for d in datos_idem:
                            linea = d.split("|")
                            # AM|Aula 13|Matemática I|nnieves|828|LU|18:20|20:00|1|1157&&
                            print "***", datos_idem
                            indice_dia = dias_disponibles2.index(linea[5])
                            indice_hin = horarios_disponibles.index(linea[6])
                            indice_hfn = horarios_disponibles.index(linea[7])
                            num_celdas_seleccionadas = indice_hfn - indice_hin

                            for v in range(indice_hin, indice_hfn):
                                pos_f = origen_horario_fil + v
                                pos_c = origen_horario_col + indice_dia
                                debuger = str(linea[6]) + ":" + str(linea[7])
                                valor_celda = "%s\n%s\n%s\n" % (linea[2], linea[3].upper(), debuger)
                                wsheet.write(pos_f, pos_c, valor_celda, style_registros)
                        fGrid += POSICION_GRID
            contador_hoja += 1

        # Seleccionamos la primera hoja
        wb.get_sheet(0)

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=formato_horarios1.xls'
        wb.save()
        return response
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def guardar_horario_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.add_horario'):
        consulta = str(request.POST['consulta'])
        id_periodolabor = consulta.split('-')[0]
        dia = consulta.split('-')[1]
        h_inicio = consulta.split('-')[2]
        h_fin = consulta.split('-')[3]
        n_aula = consulta.split('-')[4]
        dias = {'LU': 'Lunes', 'MA': 'Martes', 'MI': 'Miercoles', 'JU': 'Jueves', 'VI': 'Viernes', 'SA': 'Sabado',
                'DO': 'Domingo'}

        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        # inicio guardar
        from datetime import time
        hora_inicio = h_inicio.split(':')
        hora_fin = h_fin.split(':')

        inicio = time(int(hora_inicio[0]), int(hora_inicio[1]))
        fin = time(int(hora_fin[0]), int(hora_fin[1]))

        try:
            periodolabor = PeriodoLabor.objects.get(id=id_periodolabor)
        except PeriodoLabor.DoesNotExist:
            return HttpResponse('0|El Labor no existe')

        horarios_dia = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Dia=dias[dia], Seccion=n_aula)
        for hor in horarios_dia:
            if inicio >= hor.HoraInicio and fin <= hor.HoraFin:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con labor %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoLabor.Labor.id), hor.PeriodoLabor.Labor.Tipolabor,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))
            elif hor.HoraInicio <= inicio < hor.HoraFin:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con labor %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoLabor.Labor.id), hor.PeriodoLabor.Labor.Tipolabor,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))
            elif hor.HoraFin >= fin > hor.HoraInicio:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con labor %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoLabor.Labor.id), hor.PeriodoLabor.Labor.Tipolabor,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))
            elif inicio <= hor.HoraInicio and fin >= hor.HoraFin:
                return HttpResponse(
                    '0|No se pudo registrar horario en en el %s, %s, %s - %s \nConflicto con labor %s %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M'),
                        str(hor.PeriodoLabor.Labor.id), hor.PeriodoLabor.Labor.Tipolabor,
                        hor.HoraInicio.strftime('%H:%M'),
                        hor.HoraFin.strftime('%H:%M')))

        crear_horario = Horario(PeriodoLabor=periodolabor, Dia=dias[dia], HoraInicio=inicio, HoraFin=fin,
                                Seccion=n_aula)
        crear_horario.save()
        # fin guardar horario

        tipolabor = consulta.split('-')[5]
        labor = consulta.split('-')[6]

        horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                          PeriodoLabor__Labor__Tipolabor__id=tipolabor,
                                          PeriodoLabor__Labor__id=labor)

        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        cadena = ''
        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoLabor.Labor.Tipolabor.id, horario.PeriodoLabor.Labor.Nombre,
                horario.PeriodoLabor.Docente.username, horario.PeriodoLabor.id, dia, inicio, fin,
                horario.PeriodoLabor.Labor.id, horario.id, horario.PeriodoLabor.Grupo)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_exempt
def modificar_horario_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        consulta = str(request.POST['consulta'])
        id_horario = consulta.split('-')[0]
        dia = consulta.split('-')[1]
        h_inicio = consulta.split('-')[2]
        h_fin = consulta.split('-')[3]
        n_aula = consulta.split('-')[4]
        dias = {'LU': 'Lunes', 'MA': 'Martes', 'MI': 'Miercoles', 'JU': 'Jueves', 'VI': 'Viernes', 'SA': 'Sabado',
                'DO': 'Domingo'}

        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Labordocente/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        # inicio modificar
        from datetime import time
        hora_inicio = h_inicio.split(':')
        hora_fin = h_fin.split(':')

        inicio = time(int(hora_inicio[0]), int(hora_inicio[1]))
        fin = time(int(hora_fin[0]), int(hora_fin[1]))

        try:
            horario = Horario.objects.get(id=id_horario)
        except Horario.DoesNotExist:
            return HttpResponse('0|El Horario no existe')

        horarios_dia = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Dia=dias[dia], Seccion=n_aula)
        for hor in horarios_dia:
            if int(hor.id) != int(id_horario):
                if inicio >= hor.HoraInicio and fin <= hor.HoraFin:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))
                elif hor.HoraInicio <= inicio < hor.HoraFin:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))
                elif hor.HoraFin >= fin > hor.HoraInicio:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))
                elif inicio <= hor.HoraInicio and fin >= hor.HoraFin:
                    return HttpResponse('0|Ya existe horario en el %s, %s, %s - %s' % (
                        n_aula, dias[dia], inicio.strftime('%H:%M'), fin.strftime('%H:%M')))

        horario.Dia = dias[dia]
        horario.HoraInicio = inicio
        horario.HoraFin = fin
        horario.save()
        # fin modificar horario

        tipolabor = consulta.split('-')[5]
        labor = consulta.split('-')[6]

        horarios = None

        if tipolabor == '' and labor == '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula)
        if tipolabor != '' and labor == '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__Tipolabor__id=tipolabor)
        if tipolabor == '' and labor != '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__id=labor)

        if tipolabor != '' and labor != '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__Tipolabor__id=tipolabor,
                                              PeriodoLabor__Labor__id=labor)

        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        cadena = ''
        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoLabor.Labor.Tipolabor.id, horario.PeriodoLabor.Labor.Nombre,
                horario.PeriodoLabor.Docente.username, horario.PeriodoLabor.id, dia, inicio, fin,
                horario.PeriodoLabor.Labor.id, horario.id, horario.PeriodoLabor.Grupo)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def is_int(val):
    if type(val) == int:
        return True
    else:
        if val.is_integer():
            return True
        else:
            return False


@csrf_exempt
def eliminar_horario_labor(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.delete_horario'):
        consulta = str(request.POST['consulta'])
        id_horario = consulta.split('-')[0]
        n_aula = consulta.split('-')[1]

        ID_PERIODO = int(request.POST['periodo'])
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO)
        except ValueError:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Labordocente/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        # inicio eliminar
        try:
            horario = Horario.objects.get(id=id_horario)
        except Horario.DoesNotExist:
            return HttpResponse('0|El Horario no existe')

        horario.delete()

        # fin eliminar horario

        tipolabor = consulta.split('-')[2]
        labor = consulta.split('-')[3]

        horarios = None

        if tipolabor == '' and labor == '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula)
        if tipolabor != '' and labor == '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__Tipolabor__id=tipolabor)
        if tipolabor == '' and labor != '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__id=labor)

        if tipolabor != '' and labor != '':
            horarios = Horario.objects.filter(PeriodoLabor__Periodo__id=ID_PERIODO, Seccion=n_aula,
                                              PeriodoLabor__Labor__Tipolabor__id=tipolabor,
                                              PeriodoLabor__Labor__id=labor)

        dias = {'Lunes': 'LU', 'Martes': 'MA', 'Miercoles': 'MI', 'Jueves': 'JU', 'Viernes': 'VI', 'Sabado': 'SA',
                'Domingo': 'DO'}
        cadena = ''
        for horario in horarios:
            dia = dias[horario.Dia]
            inicio = str(horario.HoraInicio).split(':')[0] + ':' + str(horario.HoraInicio).split(':')[1]
            fin = str(horario.HoraFin).split(':')[0] + ':' + str(horario.HoraFin).split(':')[1]
            cadena += '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&&' % (
                horario.PeriodoLabor.Labor.Tipolabor.id, horario.PeriodoLabor.Labor.Nombre,
                horario.PeriodoLabor.Docente.username, horario.PeriodoLabor.id, dia, inicio, fin,
                horario.PeriodoLabor.Labor.id, horario.id, horario.PeriodoLabor.Grupo)
        return HttpResponse(cadena)
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Labordocente/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def obtener_labores(request, tipolabor_id, periodo_id):
    if tipolabor_id != "":
        consulta = Labor.objects.filter(Tipolabor__id=tipolabor_id, Periodo__id=periodo_id).order_by('Nombre')
        resultado = {}
        contador = 0
        for fila in consulta:
            response_data = {}
            response_data['id'] = fila.id
            response_data['nombre'] = fila.Nombre
            if fila.Carrera is None:
                response_data['carrera'] = ''
            else:
                response_data['carrera'] = fila.Carrera.Carrera
            resultado[contador] = response_data
            contador = contador + 1
        return HttpResponse(simplejson.dumps(resultado), content_type='application/json')


def crear_grupo_horario_labor(request):
    if request.method == 'POST':
        labores = request.POST.getlist('labores')
        form = DocenteForm(request.POST)
        if form.is_valid():
            docente = form.cleaned_data['Docente']
            grupo = form.cleaned_data['Grupo']
            for labor_id in labores:
                try:
                    labor = Labor.objects.get(id=labor_id)
                    verificar = PeriodoLabor.objects.filter(Periodo=labor.Periodo, Labor=labor)
                    if verificar.count() == 0:
                        nuevo_grupo = PeriodoLabor(Periodo=labor.Periodo, Labor=labor, Grupo=grupo,
                                                   Docente=docente)
                        nuevo_grupo.save()
                except ValueError:
                    pass
            return HttpResponseRedirect("../")
    else:
        return HttpResponseRedirect("../")
