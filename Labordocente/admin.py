# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.utils import get_deleted_objects, unquote
from django.core.exceptions import PermissionDenied
from django.db import router, transaction
from django.http import Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_unicode
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_protect

from .forms import DocenteForm
from .models import Tipolabor, Labor, PeriodoLabor, Horario, AsistenciaDocente

csrf_protect_m = method_decorator(csrf_protect)


class LaborAdmin(admin.ModelAdmin):
    list_filter = ('Periodo', 'Tipolabor')
    search_fields = ('Nombre',)
    actions = ['crear_grupo_horario']
    list_display = ('Periodo', 'Codigo', 'Nombre', 'Tipolabor', 'Carrera')

    @csrf_protect_m
    def crear_grupo_horario(modeladmin, request, queryset):
        form = DocenteForm()
        return render_to_response('Labordocente/Labor/crear_grupo_horario.html',
                                  {"form": form, "queryset": queryset, "user": request.user},
                                  context_instance=RequestContext(request))

    crear_grupo_horario.short_description = "Crear Grupo Horario"

    class Meta:
        model = Labor


class PeriodoLaborAdmin(admin.ModelAdmin):
    list_filter = ('Periodo',)

    @csrf_protect_m
    @transaction.atomic()
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        using = router.db_for_write(self.model)

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, model_count, perms_needed, protected) = get_deleted_objects(
            [obj], opts, request.user, self.admin_site, using)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            self.delete_model(request, obj)

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Grupo Horario de labor docente eliminado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        object_name = force_unicode(opts.verbose_name)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": object_name}
        else:
            title = _("Are you sure?")

        context = {
            "title": title,
            "object_name": object_name,
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": opts,
            "app_label": app_label,
        }
        context.update(extra_context or {})

        return TemplateResponse(request, self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, current_app=self.admin_site.name)

    class Meta:
        model = PeriodoLabor


class HorarioAdmin(admin.ModelAdmin):
    list_display = ('PeriodoLabor', 'Dia', 'HoraInicio', 'HoraFin', 'Seccion',)

    @csrf_protect_m
    @transaction.atomic()
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        using = router.db_for_write(self.model)

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, model_count, perms_needed, protected) = get_deleted_objects(
            [obj], opts, request.user, self.admin_site, using)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            self.delete_model(request, obj)

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Horario de labor docente Eliminado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        object_name = force_unicode(opts.verbose_name)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": object_name}
        else:
            title = _("Are you sure?")

        context = {
            "title": title,
            "object_name": object_name,
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": opts,
            "app_label": app_label,
        }
        context.update(extra_context or {})

        return TemplateResponse(request, self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, current_app=self.admin_site.name)


admin.site.register(Tipolabor)
admin.site.register(Labor, LaborAdmin)
admin.site.register(PeriodoLabor, PeriodoLaborAdmin)
admin.site.register(Horario, HorarioAdmin)
admin.site.register(AsistenciaDocente)
