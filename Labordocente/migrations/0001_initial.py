# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import Labordocente.models


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0001_initial'),
        ('Carreras', '0001_initial'),
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AsistenciaDocente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('TemaClase', models.TextField()),
                ('FechaClase', models.DateField(verbose_name=b'Fecha')),
                ('Fecha', models.DateTimeField(auto_now_add=True, verbose_name=b'Fecha')),
                ('Observaciones', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Asistencia de Docentes',
                'verbose_name_plural': 'Asistencias de Docentes',
            },
        ),
        migrations.CreateModel(
            name='Horario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Dia', models.CharField(max_length=10, verbose_name=b'D\xc3\xada', choices=[(b'Lunes', b'Lunes'), (b'Martes', b'Martes'), (b'Miercoles', b'Miercoles'), (b'Jueves', b'Jueves'), (b'Viernes', b'Viernes'), (b'Sabado', b'Sabado'), (b'Domingo', b'Domingo')])),
                ('HoraInicio', models.TimeField()),
                ('HoraFin', models.TimeField()),
                ('Seccion', models.CharField(max_length=50, verbose_name=b'Secci\xc3\xb3n')),
            ],
            options={
                'verbose_name': 'Horario',
                'verbose_name_plural': 'Horarios',
                'permissions': (('read_horario', 'Observar Configuracion de Horarios'),),
            },
        ),
        migrations.CreateModel(
            name='Labor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(max_length=9)),
                ('Nombre', models.CharField(max_length=60)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activada')),
                ('Observacion', models.TextField(null=True, verbose_name=b'Observaci\xc3\xb3n', blank=True)),
                ('Carrera', models.ForeignKey(blank=True, to='Carreras.Carrera', null=True)),
                ('Periodo', models.ForeignKey(verbose_name=b'Per\xc3\xadodo', to='Periodos.Periodo')),
            ],
            options={
                'verbose_name': 'Labor de docentes',
                'verbose_name_plural': 'Labores de docente',
            },
        ),
        migrations.CreateModel(
            name='PeriodoLabor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Grupo', models.CharField(max_length=5)),
                ('Observaciones', models.TextField(null=True, blank=True)),
                ('Documento', models.FileField(verbose_name=b'Documento', upload_to=Labordocente.models.obtenerpath_periodolabor, null=True, editable=False, blank=True)),
                ('Docente', models.ForeignKey(to='Docentes.Docente')),
                ('Labor', models.ForeignKey(to='Labordocente.Labor')),
                ('Periodo', models.ForeignKey(to='Periodos.Periodo')),
            ],
            options={
                'verbose_name': 'Grupo Horario de labor',
                'verbose_name_plural': 'Grupos Horarios de labor',
                'permissions': (('read_periodolabor', 'Observar Grupos Horario de labor'),),
            },
        ),
        migrations.CreateModel(
            name='Tipolabor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=60, verbose_name=b'Nombre')),
            ],
            options={
                'verbose_name': 'Tipo de labor',
                'verbose_name_plural': 'Tipos de labor',
            },
        ),
        migrations.AddField(
            model_name='labor',
            name='Tipolabor',
            field=models.ForeignKey(verbose_name=b'Tipo de labor', to='Labordocente.Tipolabor'),
        ),
        migrations.AddField(
            model_name='horario',
            name='PeriodoLabor',
            field=models.ForeignKey(verbose_name=b'Labor', to='Labordocente.PeriodoLabor'),
        ),
        migrations.AddField(
            model_name='asistenciadocente',
            name='Horario',
            field=models.ForeignKey(verbose_name=b'Horario', to='Labordocente.Horario'),
        ),
        migrations.AlterUniqueTogether(
            name='periodolabor',
            unique_together=set([('Periodo', 'Labor', 'Grupo', 'Docente')]),
        ),
        migrations.AlterUniqueTogether(
            name='horario',
            unique_together=set([('PeriodoLabor', 'Dia', 'HoraInicio', 'HoraFin', 'Seccion')]),
        ),
    ]
