# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.

class Serie(models.Model):
    Numero = models.CharField("Nº Serie", max_length=4, unique=True)

    def __unicode__(self):
        return u'%s' % self.Numero

    class Meta:
        verbose_name = "Número de Serie"
        verbose_name_plural = "Números de Serie"


class TipoComprobante(models.Model):
    Tipo = models.CharField("Tipo", max_length=50, unique=True)
    Serie = models.OneToOneField(Serie)
    PagoBanco = models.BooleanField("¿Pagar en Banco?", default=False)
    Estado = models.BooleanField("¿Activada?", default=True)

    def __unicode__(self):
        return u'%s %s' % (self.Tipo, self.Serie)

    class Meta:
        verbose_name = "Tipo de Comprobante"
        verbose_name_plural = "Tipos de Comprobante"
