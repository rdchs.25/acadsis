# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Serie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Numero', models.CharField(unique=True, max_length=4, verbose_name=b'N\xc2\xba Serie')),
            ],
            options={
                'verbose_name': 'N\xfamero de Serie',
                'verbose_name_plural': 'N\xfameros de Serie',
            },
        ),
        migrations.CreateModel(
            name='TipoComprobante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Tipo', models.CharField(unique=True, max_length=50, verbose_name=b'Tipo')),
                ('PagoBanco', models.BooleanField(default=False, verbose_name=b'\xc2\xbfPagar en Banco?')),
                ('Estado', models.BooleanField(default=True, verbose_name=b'\xc2\xbfActivada?')),
                ('Serie', models.OneToOneField(to='Configuraciones.Serie')),
            ],
            options={
                'verbose_name': 'Tipo de Comprobante',
                'verbose_name_plural': 'Tipos de Comprobante',
            },
        ),
    ]
