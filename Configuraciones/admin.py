# -*- coding: utf-8 -*-
from django.contrib import admin

from Configuraciones.models import TipoComprobante, Serie


class TipoComprobanteAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'PagoBanco', 'Estado')


admin.site.register(TipoComprobante, TipoComprobanteAdmin)


class SerieAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)


admin.site.register(Serie, SerieAdmin)
