# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Marcacion', '0004_auto_20181120_1536'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='marcacion',
            options={'ordering': ['-Fecha', '-Hora'], 'verbose_name': 'Marcaci\xf3n docente', 'verbose_name_plural': 'Marcaciones de docente'},
        ),
    ]
