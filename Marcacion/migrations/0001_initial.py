# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0003_docente_huella'),
    ]

    operations = [
        migrations.CreateModel(
            name='Marcacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Fechamarcacion', models.DateTimeField(auto_now=True, verbose_name=b'Fecha Marcaci\xc3\xb3n')),
                ('Docente', models.ForeignKey(verbose_name=b'Docente', to='Docentes.Docente')),
            ],
        ),
    ]
