# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Persona', '0001_initial'),
        ('Marcacion', '0005_auto_20181121_0821'),
    ]

    operations = [
        migrations.CreateModel(
            name='Marcacionpersona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Fechamarcacion', models.DateTimeField(auto_now=True, verbose_name=b'Fecha Marcaci\xc3\xb3n')),
                ('Fecha', models.DateField(editable=False)),
                ('Hora', models.TimeField(editable=False)),
                ('Persona', models.ForeignKey(verbose_name=b'Personal', to='Persona.Persona')),
            ],
            options={
                'ordering': ['-Fecha', '-Hora'],
                'verbose_name': 'Marcaci\xf3n personal',
                'verbose_name_plural': 'Marcaciones de personal',
            },
        ),
    ]
