# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0003_docente_huella'),
        ('Marcacion', '0003_auto_20181119_1720'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reglaspersona',
            name='Docente',
        ),
        migrations.RemoveField(
            model_name='reglaspersona',
            name='Reglas',
        ),
        migrations.AddField(
            model_name='reglas',
            name='Docente',
            field=models.ForeignKey(default=None, to='Docentes.Docente', null=True),
        ),
        migrations.DeleteModel(
            name='Reglaspersona',
        ),
    ]
