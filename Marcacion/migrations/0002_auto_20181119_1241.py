# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('Marcacion', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='marcacion',
            name='Fecha',
            field=models.DateField(default=datetime.datetime(2018, 11, 19, 17, 41, 30, 770000, tzinfo=utc), editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='marcacion',
            name='Hora',
            field=models.TimeField(default=datetime.datetime(2018, 11, 19, 17, 41, 49, 448000, tzinfo=utc), editable=False),
            preserve_default=False,
        ),
    ]
