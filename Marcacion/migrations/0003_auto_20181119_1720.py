# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0003_docente_huella'),
        ('Marcacion', '0002_auto_20181119_1241'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reglas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Agrupacion', models.CharField(max_length=1, choices=[(b'A', b'Administrativos'), (b'D', b'Docentes'), (b'L', b'Limpieza')])),
                ('Turno', models.CharField(max_length=1, choices=[(b'M', b'Ma\xc3\xb1ana'), (b'T', b'Tarde')])),
                ('Dias', models.CharField(max_length=30)),
                ('Entrada', models.TimeField()),
                ('Salida', models.TimeField()),
                ('Tolerancia', models.PositiveSmallIntegerField(default=10, help_text=b'En minutos')),
                ('Estado', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Reglas de Asistencia',
                'verbose_name_plural': 'Reglas de Asistencia',
            },
        ),
        migrations.CreateModel(
            name='Reglaspersona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Docente', models.ForeignKey(to='Docentes.Docente')),
                ('Reglas', models.ForeignKey(to='Marcacion.Reglas')),
            ],
            options={
                'verbose_name': 'Personal Asignado',
                'verbose_name_plural': 'Personal Asignado',
            },
        ),
        migrations.AlterModelOptions(
            name='marcacion',
            options={'ordering': ['-Fechamarcacion', 'Docente'], 'verbose_name': 'Marcaci\xf3n docente', 'verbose_name_plural': 'Marcaciones de docente'},
        ),
    ]
