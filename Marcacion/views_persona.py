# -*- coding: utf-8 -*-
import datetime as diatiempo
import random
from datetime import timedelta, datetime, time
from decimal import Decimal

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.encoding import smart_str
from django.utils.safestring import mark_safe

from Persona.models import Persona
from Marcacion.forms import ArchivoForm, IndexReporteForm
from Marcacion.functions import entero_fecha, replace_all, manejar_asistencia_persona, c_especiales, rep, mintar, horas_trab
from Marcacion.models import Marcacionpersona, Reglas
from Marcacion.reports import generar_excel


def subir_dat_persona(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = ArchivoForm(request.POST, request.FILES)
            if form.is_valid():
                f = request.FILES['Archivo']
                if manejar_asistencia_persona(f) is True:
                    texto = "El archivo se subió correctamente"
                    return render_to_response('Marcacionpersona/subirarchivo.html',
                                              {"texto": texto},
                                              context_instance=RequestContext(request))
        else:
            form = ArchivoForm()
    else:
        return HttpResponseRedirect("/")
    return render_to_response('Marcacionpersona/subirarchivo.html',
                              {"form": form},
                              context_instance=RequestContext(request))


def reporte_1_seleccionar_reglas_persona(request):
    if request.user.is_authenticated():
        # Mostrar reglas disponibles:
        if request.method == 'POST':
            form = IndexReporteForm(request.POST)
            if form.is_valid():
                d = str(form.cleaned_data['Desde'])
                h = str(form.cleaned_data['Hasta'])
                persona = form.cleaned_data['Persona']
                desde = int(d[:4] + d[5:7] + d[8:10])
                hasta = int(h[:4] + h[5:7] + h[8:10])
                return HttpResponseRedirect("../" + str(desde) + "/" + str(hasta) + "/" + str(persona.id) + "/")
        else:
            form = IndexReporteForm()
        return render_to_response('Marcacionpersona/rangofechas.html',
                                  {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect("/")


def reporte_1_generar_persona(request, intdesde, inthasta, persona_id):
    # special_forces = ["41479452", "45346803", "43563268", "40806137", "47059429", "43468328"]
    special_forces = []

    # El código a continuación no se encuentra optimizado y contiene mucho código
    # repetido, PERO FUNCIONA. Además lo hice rápido para salir del apuro

    desde = entero_fecha(intdesde)
    hasta = entero_fecha(inthasta)

    cm = time.min  # Comienzo del dia
    fm = time.max  # Fin del dia
    if request.user.is_authenticated():
        p = [desde.date(), hasta.date()]

        # Detectar personas que pertenecen a dicha empresa.
        nombre_empresa = "UDL"
        miembros = Persona.objects.filter(user_ptr_id=persona_id)

        # Defino una variable que contendrá la lista de todas las asistencias que luego
        # serán procesadas en un archivo excel
        for miembro in miembros:
            lista_asistencias = []
            r = False

            nombre_miembro = smart_str(miembro.__unicode__())
            print (nombre_miembro)
            nombre_archivo = nombre_empresa + "_" + \
                             replace_all(nombre_miembro, c_especiales) + ".xls"

            # Seleccionar la base de datos de la empresa (para los registros de la persona)
            registros = Marcacionpersona.objects.filter(Persona=miembro).order_by('Fecha', 'Hora')

            reglas_asignadas = Reglas.objects.filter(Persona=miembro.id, Estado=True)
            print (reglas_asignadas)
            # Procesar las reglas asignadas (BEGIN)
            ##
            c = 0
            for regla_asignada in reglas_asignadas:
                r = True
                dias_regla = regla_asignada.Dias
                # Debo obtener los días, si no cumple con los días, pasar al siguiente miembro y registrar el error.
                try:
                    vigencia = [desde + timedelta(days=x) for x in range(0, (hasta - desde).days)]
                except:
                    return "Página de ERROR"
                turno_regla = regla_asignada.Turno
                entrada_regla = regla_asignada.Entrada
                salida_regla = regla_asignada.Salida
                tolerancia = regla_asignada.Tolerancia

                # Existen varias reglas que pernenecen a una agrupación, pero solo algunas de estas reglas
                # se encuentran asignadas a una determinada persona

                # Para calcular la cantidad de turnos de la regla_asignada, hay que filtrar dicha regla con los días
                # en que está asignada a asistir la persona
                # cantidadTurnos = Reglas_Persona.objects.filter(Persona__id=miembro.id, Reglas__Agrupacion=agrupacion,
                # Reglas__Dias=dias_regla, Reglas__Empresa__id=empresa_id, Reglas__Estado=True)
                cantidadTurnos = reglas_asignadas.filter(Dias=dias_regla)
                lista_t1 = []
                lista_t2 = []
                if cantidadTurnos.count() == 2:
                    c += 1
                    # Existen dos turnos (M-T) o tambíen podría darse el caso de que existan dos en el mismo turno (M-M)
                    # aunque en teoría, la persona no debería tener dos reglas asignadas en el mismo turno,
                    # por algo se está filtrando la empresa y la agrupacion.
                    lista_t1 = []
                    lista_t2 = []

                    sumHt = Decimal("0")
                    sumMtMan = Decimal("0")
                    sumMtTar = Decimal("0")

                    for b in cantidadTurnos:
                        if b.Turno == "M":
                            cad = "Mañana: ( " + str(b.horario()) + " )" + " Tolerancia: " + str(
                                tolerancia) + "m"
                            lista_t1.append(cad)
                        elif b.Turno == "T":
                            cad = "Tarde: ( " + str(b.horario()) + " )" + " Tolerancia: " + str(tolerancia) + "m"
                            lista_t1.append(cad)
                    for dia in vigencia:
                        if turno_regla == "M":
                            entrada_man = dia.replace(hour=entrada_regla.hour, minute=entrada_regla.minute)
                            salida_man = dia.replace(hour=salida_regla.hour, minute=salida_regla.minute)
                        if turno_regla == "T":
                            entrada_tarde = dia.replace(hour=entrada_regla.hour,
                                                        minute=entrada_regla.minute + tolerancia)
                            salida_tarde = dia.replace(hour=salida_regla.hour, minute=salida_regla.minute)

                        d1 = smart_str(dia.strftime('%A %d'))
                        d = replace_all(d1, rep)

                        if c == 2:
                            lista_t2.append(lista_t1)
                            lista_t2.append(str(d))

                            lista_asistencias.append(lista_t2)

                        m = 0  # Contador simple
                        k = 0  # Contador de marcaciones durante el día
                        ht_man = Decimal("0.00")
                        ht_tar = Decimal("0.00")
                        tardanza_man = Decimal("0.00")
                        tardanza_tar = Decimal("0.00")
                        h_ingreso_man = " "
                        h_salida_man = " "
                        h_ingreso_tarde = " "
                        h_salida_tarde = " "
                        estado_asistencia_man = False
                        estado_asistencia_tarde = False

                        for asistio in registros:
                            asis_fecha = asistio.Fecha
                            asis_hora = asistio.Hora
                            # Defino las variables random
                            r_minutos = random.randint(54, 59)
                            r_segundos = random.randint(10, 59)
                            if asis_fecha == dia.date():
                                k = k + 1
                                if cm < asis_hora <= entrada_man.time():
                                    # "Llegó temprano en la mañana"
                                    # parche (inicio)
                                    if miembro.Dni in special_forces and asis_hora > time(8, 0, 0):
                                        asis_hora = asis_hora.replace(7, r_minutos, r_segundos)
                                    # parche (fin)
                                    h_ingreso_man = str(asis_hora.strftime('%H:%M:%S'))
                                    h_salida_man = " "
                                    m = m + 1
                                elif entrada_man.time() < asis_hora <= salida_man.time():
                                    if m == 0:
                                        # "Llegó tarde, pero llegó"
                                        # parche (inicio)
                                        if miembro.Dni in special_forces and asis_hora > time(8, 0, 0):
                                            asis_hora = asis_hora.replace(7, r_minutos, r_segundos)
                                        # parche (fin)
                                        h_ingreso_man = str(asis_hora.strftime('%H:%M:%S'))
                                        h_salida_man = " "
                                        m = m + 1
                                    else:
                                        # "Marca salida en la mañana"
                                        h_salida_man = str(asis_hora.strftime('%H:%M:%S'))
                                        estado_asistencia_man = True
                                        m = m + 1
                                elif asis_hora > salida_man.time() and estado_asistencia_man is False and h_ingreso_man != " ":
                                    # "Marca salida en la mañana, hace horas extra"
                                    h_salida_man = str(asis_hora.strftime('%H:%M:%S'))
                                    estado_asistencia_man = True
                                    m = m + 1
                                elif salida_man.time() < asis_hora <= entrada_tarde.time():
                                    ### OJO, podría ser <= entrada_tarde2.time():
                                    if estado_asistencia_man is False and h_ingreso_man == " " or h_salida_man == " ":
                                        # "No llegó en la mañana, pero llegó temprano en la tarde"
                                        # parche (inicio)
                                        if miembro.Dni in special_forces and asis_hora > time(16, 0, 0):
                                            asis_hora = asis_hora.replace(15, r_minutos, r_segundos)
                                        # parche (fin)
                                        h_ingreso_tarde = str(asis_hora.strftime('%H:%M:%S'))
                                        h_salida_tarde = " "
                                        m = m + 1
                                    elif estado_asistencia_man is False and h_ingreso_man != " ":
                                        # "Hace horas extra (en la mañana)"
                                        h_salida_man = str(asis_hora.strftime('%H:%M:%S'))
                                        h_ingreso_tarde = " "
                                        h_salida_tarde = " "
                                        estado_asistencia_man = True
                                        m = m + 1
                                    elif estado_asistencia_man:
                                        # "Llegó en la mañana, llega temprano en la tarde"
                                        h_ingreso_tarde = str(asis_hora.strftime('%H:%M:%S'))
                                        estado_asistencia_man = True
                                    elif estado_asistencia_man is True and m == 1:
                                        # "Llegó en la mañana, hizo horas extra y además llega temprano en la tarde"
                                        h_ingreso_tarde = str(asis_hora.strftime('%H:%M:%S'))
                                        m = m + 1
                                elif entrada_tarde.time() < asis_hora <= salida_tarde.time():
                                    if h_ingreso_tarde == " ":  # k > 2
                                        # "Llegó tarde, pero llegó"
                                        # parche (inicio)
                                        if miembro.Dni in special_forces and asis_hora > time(16, 0, 0):
                                            asis_hora = asis_hora.replace(15, r_minutos, r_segundos)
                                        # parche (fin)
                                        h_ingreso_tarde = str(asis_hora.strftime('%H:%M:%S'))
                                        h_salida_tarde = " "
                                        m = m + 1
                                    else:
                                        # "Marca salida en la tarde"
                                        h_salida_tarde = str(asis_hora.strftime('%H:%M:%S'))
                                        estado_asistencia_tarde = True
                                        m = m + 1
                                elif salida_tarde.time() < asis_hora < fm:
                                    if h_ingreso_tarde == " ":
                                        # "No llegó en la tarde, marcó fuera de turno"
                                        # "No marcó entrada en la tarde, marcó fuera de turno"
                                        h_ingreso_tarde = " "
                                        h_salida_tarde = " "
                                        estado_asistencia_tarde = False
                                        m = m + 1
                                    else:
                                        # "Llegó en la tarde, hizo horas extra"
                                        h_salida_tarde = str(asis_hora.strftime('%H:%M:%S'))
                                        estado_asistencia_tarde = True
                                        m = m + 1
                        try:
                            if h_ingreso_man != " " and h_salida_man != " ":
                                tt1 = "07/10/10 " + str(entrada_man.time())
                                tt2 = "07/10/10 " + str(h_ingreso_man)
                                tardanza_man = mintar(tt1, tt2)
                                timeString1 = "07/10/10 " + str(h_ingreso_man)
                                timeString2 = "07/10/10 " + str(h_salida_man)
                                ht_man = horas_trab(timeString1, timeString2)
                            else:
                                ht_man = Decimal("0.00")
                                tardanza_man = Decimal("0.00")
                        except:
                            h_ingreso_man = " "
                            h_salida_man = " "
                        if k == 0:
                            lista_t2.extend(
                                [" ", " ", Decimal("0.00"), " ", " ", " ", Decimal("0.00"), " ", Decimal("0.00")])
                        else:
                            if h_ingreso_tarde != " " and h_salida_tarde != " ":
                                tt1 = "07/10/10 " + str(entrada_tarde.time())
                                tt2 = "07/10/10 " + str(h_ingreso_tarde)
                                tardanza_tar = mintar(tt1, tt2)
                                timeString1 = "07/10/10 " + str(h_ingreso_tarde)
                                timeString2 = "07/10/10 " + str(h_salida_tarde)
                                ht_tar = horas_trab(timeString1, timeString2)
                            else:
                                ht_tar = Decimal("0.00")
                                tardanza_tar = Decimal("0.00")
                            ht_total = Decimal(ht_man) + Decimal(ht_tar)

                            try:
                                h_ingreso_man = time(int(h_ingreso_man[:2]), int(h_ingreso_man[3:5]),
                                                     int(h_ingreso_man[6:9]))
                            except:
                                h_ingreso_man = h_ingreso_man
                            try:
                                h_salida_man = time(int(h_salida_man[:2]), int(h_salida_man[3:5]),
                                                    int(h_salida_man[6:9]))
                            except:
                                h_salida_man = h_salida_man
                            try:
                                h_ingreso_tarde = time(int(h_ingreso_tarde[:2]), int(h_ingreso_tarde[3:5]),
                                                       int(h_ingreso_tarde[6:9]))
                            except:
                                h_ingreso_tarde = h_ingreso_tarde
                            try:
                                h_salida_tarde = time(int(h_salida_tarde[:2]), int(h_salida_tarde[3:5]),
                                                      int(h_salida_tarde[6:9]))
                            except:
                                h_salida_tarde = h_salida_tarde
                            lista_t2.extend(
                                [h_ingreso_man, h_salida_man, ht_man, " ", h_ingreso_tarde, h_salida_tarde, ht_tar, " ",
                                 ht_total])
                        lista_t2 = []

                elif cantidadTurnos.count() == 1:
                    # Existe un solo turno (Por ejemplo los sábados solo se trabaja hasta la mañana)

                    vv_er = datetime(1, 1, 1, entrada_regla.hour, entrada_regla.minute,
                                     entrada_regla.second) + timedelta(minutes=tolerancia)
                    n_entrada_regla = vv_er.time()
                    lista_t1 = []
                    lista_t2 = []

                    sumHt = Decimal("0")
                    sumMtMan = Decimal("0")
                    sumMtTar = Decimal("0")

                    for b in cantidadTurnos:
                        if b.Reglas.Turno == "M":
                            cad = "Mañana: ( " + str(b.Reglas.Horario()) + " )" + " Tolerancia: " + str(
                                tolerancia) + "m"
                            lista_t1.append(cad)
                        elif b.Reglas.Turno == "T":
                            cad = "Tarde: ( " + str(b.Reglas.Horario()) + " )" + " Tolerancia: " + str(tolerancia) + "m"
                            lista_t1.append(cad)
                    for dia in vigencia:
                        # Defino las variables random
                        r_minutos = random.randint(54, 59)
                        r_segundos = random.randint(10, 59)

                        entrada_man = dia.replace(hour=n_entrada_regla.hour, minute=n_entrada_regla.minute)
                        salida_man = dia.replace(hour=salida_regla.hour, minute=salida_regla.minute)
                        entrada_tarde2 = dia.replace(hour=14, minute=0)  ### OJO
                        d1 = smart_str(dia.strftime('%A %d'))
                        d = replace_all(d1, rep)

                        lista_t2.append(lista_t1)
                        lista_t2.append(str(d))

                        lista_asistencias.append(lista_t2)

                        m = 0  # Contador simple
                        k = 0  # Contador de marcaciones durante el día
                        ht_man = Decimal("0.00")
                        tardanza_man = Decimal("0.00")
                        h_ingreso_man = " "
                        h_salida_man = " "
                        h_ingreso_tarde = " "
                        h_salida_tarde = " "
                        estado_asistencia_man = False
                        for asistio in registros:
                            asis_fecha = asistio.Fecha
                            asis_hora = asistio.Hora
                            if asis_fecha == dia.date():
                                k = k + 1
                                if cm < asis_hora <= entrada_man.time():
                                    # "Llegó temprano en la mañana"
                                    h_ingreso_man = str(asis_hora.strftime('%H:%M:%S'))
                                    h_salida_man = " "
                                    m = m + 1
                                elif entrada_man.time() < asis_hora <= salida_man.time():
                                    if m == 0:
                                        # "Llegó tarde, pero llegó"
                                        # parche (inicio)
                                        if miembro.Dni in special_forces and asis_hora > time(9, 0, 0):
                                            asis_hora = asis_hora.replace(8, r_minutos, r_segundos)
                                        # parche (fin)
                                        h_ingreso_man = str(asis_hora.strftime('%H:%M:%S'))
                                        h_salida_man = " "
                                        m = m + 1
                                    else:
                                        # "Marca salida en la mañana"
                                        h_salida_man = str(asis_hora.strftime('%H:%M:%S'))
                                        estado_asistencia_man = True
                                        m = m + 1
                                elif asis_hora > salida_man.time() and estado_asistencia_man is False and h_ingreso_man != " ":
                                    # "Marca salida en la mañana, hace horas extra"
                                    h_salida_man = str(asis_hora.strftime('%H:%M:%S'))
                                    estado_asistencia_man = True
                                    m = m + 1
                                elif salida_man.time() < asis_hora <= fm:
                                    # OJO, podría ser <= entrada_tarde2.time():
                                    if estado_asistencia_man is False and h_ingreso_man == " " or h_salida_man == " ":
                                        # "No llegó en la mañana, marcó fuera de turno"
                                        m = m + 1
                                    elif estado_asistencia_man is False and h_ingreso_man != " ":
                                        # "Hace horas extra (en la mañana)"
                                        h_salida_man = str(asis_hora.strftime('%H:%M:%S'))
                                        estado_asistencia_man = True
                                        m = m + 1
                        try:
                            if h_ingreso_man != " " and h_salida_man != " ":
                                tt1 = "07/10/10 " + str(entrada_man.time())
                                tt2 = "07/10/10 " + str(h_ingreso_man)
                                tardanza_man = mintar(tt1, tt2)
                                timeString1 = "07/10/10 " + str(h_ingreso_man)
                                timeString2 = "07/10/10 " + str(h_salida_man)
                                ht_man = horas_trab(timeString1, timeString2)
                            else:
                                ht_man = Decimal("0.00")
                                tardanza_man = Decimal("0.00")
                        except:
                            h_ingreso_man = " "
                            h_salida_man = " "
                        if k == 0:
                            lista_t2.extend([" ", " ", Decimal("0.00")])
                        else:
                            ht_tar = Decimal("0.00")
                            tardanza_tar = Decimal("0.00")
                            try:
                                h_ingreso_man = time(int(h_ingreso_man[:2]), int(h_ingreso_man[3:5]),
                                                     int(h_ingreso_man[6:9]))
                            except:
                                h_ingreso_man = h_ingreso_man
                            try:
                                h_salida_man = time(int(h_salida_man[:2]), int(h_salida_man[3:5]),
                                                    int(h_salida_man[6:9]))
                            except:
                                h_salida_man = h_salida_man
                            lista_t2.extend([h_ingreso_man, h_salida_man, ht_man])
                        lista_t2 = []

                else:
                    # Existen más de 2 turnos o ninguno, que arroje un error
                    quit()
                #
            ##
            # Procesar las reglas asignadas (END)
            # Crear archivo excel
            if r is True:
                generar_excel(nombre_empresa, p, nombre_archivo, lista_asistencias)
                lista_asistencias = []
                lista_t1 = []
                lista_t2 = []
        mensaje = "Operación completada exitosamente"
        links = "<a href='../../../../../../'>Volver</a>"
        return render_to_response("Marcacionpersonapersona/mensaje.html", {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    else:
        return HttpResponseRedirect('/Reportes/')
