# -*- coding: utf-8 -*-
from django.db import models

from Docentes.models import Docente
from Persona.models import Persona

AGRUPACION_CHOICES = (
    ('A', 'Administrativos'),
    ('D', 'Docentes'),
    ('L', 'Limpieza'),
)

TURNO_CHOICES = (
    ('M', 'Mañana'),
    ('T', 'Tarde'),
)


class Marcacion(models.Model):
    Docente = models.ForeignKey(Docente, verbose_name="Docente")
    Fechamarcacion = models.DateTimeField('Fecha Marcación', auto_now=True, editable=False)
    Fecha = models.DateField(editable=False)
    Hora = models.TimeField(editable=False)

    def __unicode__(self):
        return ""

    class Meta:
        verbose_name = "Marcación docente"
        verbose_name_plural = "Marcaciones de docente"
        ordering = ['-Fecha', '-Hora']


class Reglas(models.Model):
    Agrupacion = models.CharField(max_length=1, choices=AGRUPACION_CHOICES)
    Turno = models.CharField(max_length=1, choices=TURNO_CHOICES)
    Dias = models.CharField(max_length=30)
    Entrada = models.TimeField()
    Salida = models.TimeField()
    Tolerancia = models.PositiveSmallIntegerField(help_text="En minutos", default=10)
    Estado = models.BooleanField(default=True)
    Docente = models.ForeignKey(Docente, null=True, default=None)

    def __unicode__(self):
        return u'%s' % self.Entrada

    def horario(self):
        return u'%s - %s' % (self.Entrada, self.Salida)

    class Meta:
        verbose_name = "Reglas de Asistencia"
        verbose_name_plural = "Reglas de Asistencia"


class Marcacionpersona(models.Model):
    Persona = models.ForeignKey(Persona, verbose_name="Personal")
    Fechamarcacion = models.DateTimeField('Fecha Marcación', auto_now=True, editable=False)
    Fecha = models.DateField(editable=False)
    Hora = models.TimeField(editable=False)

    def __unicode__(self):
        return ""

    class Meta:
        verbose_name = "Marcación personal"
        verbose_name_plural = "Marcaciones de personal"
        ordering = ['-Fecha', '-Hora']