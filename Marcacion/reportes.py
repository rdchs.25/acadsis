# -*- coding: utf-8 -*-
from funciones import response_excel


def docentes_excel(queryset):
    listado = []
    for obj in queryset:
        nombredocente = obj.Docente.__unicode__()
        fecha = obj.Fecha
        hora = str(obj.Hora)

        listado.append(
            [nombredocente, fecha, hora])
    headers = ['Docente', 'Fecha', 'Hora', ]
    titulo = 'Listado de marcación docente UDL'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='marcacion_docente')


def personal_excel(queryset):
    listado = []
    for obj in queryset:
        nombrepersona = obj.Persona.__unicode__()
        fecha = obj.Fecha
        hora = str(obj.Hora)

        listado.append(
            [nombrepersona, fecha, hora])
    headers = ['Personal', 'Fecha', 'Hora', ]
    titulo = 'Listado de marcación administrativos UDL'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='marcacion_personal')
