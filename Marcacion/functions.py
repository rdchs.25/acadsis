# -*- coding: utf-8 -*-
import os
from datetime import date, datetime, time
from decimal import Decimal, ROUND_UP
from time import strptime, mktime

from django.conf import settings
from django.db import transaction

from Docentes.models import Docente
from Marcacion.models import Marcacion, Marcacionpersona
from Persona.models import Persona

TWOPLACES = Decimal(10) ** -2
c_especiales = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
                'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': '_', 'º': '_', '¬': '_', '¡': '_',
                '!': '_', '¿': '_', '?': '_', '^': '_', '"': '_', ' ': '_'}

rep = {'Monday': 'Lunes', 'Tuesday': 'Martes', 'Wednesday': 'Miércoles',
       'Thursday': 'Jueves', 'Friday': 'Viernes', 'Saturday': 'Sábado',
       'Sunday': 'Domingo'}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def entero_fecha(entero):
    cadena = str(entero)
    r = datetime(int(cadena[:4]),
                 int(cadena[4:6]),
                 int(cadena[6:8]))
    return r


def manejar_asistencia(f):
    nombre_empresa = 'UDL'
    fecha = date.today()
    nombre_archivo = f.name
    ext_archivo = str(nombre_archivo).lower()[-3:]
    nombre_txt = str(fecha) + ".txt"
    ruta_archivo = settings.STATICFILES_DIRS[0] + "uploads/archivos/" + str(nombre_empresa)
    # Verifico si existe la ruta (En caso de que no exista, lo creo)
    if os.path.isdir(ruta_archivo) is False:
        os.mkdir(ruta_archivo)

    # Verifico la extensión del archivo (el reloj emite un .dat)
    if ext_archivo == "dat":
        destination = open('%s/%s' % (ruta_archivo, nombre_txt), 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()
        archivo = open('%s/%s' % (ruta_archivo, nombre_txt), 'rb')
        items = []
        for line in archivo:
            linea = str(line)
            # Verifico la estructura
            identif = int(linea[1:9])
            r1 = str(linea[10:20])
            r2 = str(linea[21:29])
            fecha_registro = date(int(r1[0:4]), int(r1[5:7]), int(r1[8:10]))
            hora_registro = time(int(r2[0:2]), int(r2[3:5]), int(r2[6:8]))
            items.extend([[identif, fecha_registro, hora_registro]])
        with transaction.atomic():
            for x in items:
                try:
                    marcacion = Marcacion()
                    docente = Docente.objects.get(Dni=str(x[0]))
                    marcacion.Docente = docente
                    marcacion.Fechamarcacion = str(x[1]) + " " + str(x[2])
                    marcacion.Fecha = str(x[1])
                    marcacion.Hora = str(x[2])
                    marcacion.save()
                except Docente.DoesNotExist:
                    pass
                # Asistencia.objects.get_or_create(Codigo=x[0], Fecha=x[1], Hora=x[2])
        return True
        # Quizás te preguntes: ¿Por qué genero una lista nueva en vez de comenzar la transacción al
        # momento de la lectura del archivo?
        # Rpta: Si la estructura del archivo es incorrecta, la transacción no se ejecuta.
    else:
        return False


def manejar_asistencia_persona(f):
    nombre_empresa = 'UDL'
    fecha = date.today()
    nombre_archivo = f.name
    ext_archivo = str(nombre_archivo).lower()[-3:]
    nombre_txt = str(fecha) + ".txt"
    ruta_archivo = settings.STATICFILES_DIRS[0] + "uploads/archivos/" + str(nombre_empresa)
    # Verifico si existe la ruta (En caso de que no exista, lo creo)
    if os.path.isdir(ruta_archivo) is False:
        os.mkdir(ruta_archivo)

    # Verifico la extensión del archivo (el reloj emite un .dat)
    if ext_archivo == "dat":
        destination = open('%s/%s' % (ruta_archivo, nombre_txt), 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()
        archivo = open('%s/%s' % (ruta_archivo, nombre_txt), 'rb')
        items = []
        for line in archivo:
            linea = str(line)
            # Verifico la estructura
            identif = int(linea[1:9])
            r1 = str(linea[10:20])
            r2 = str(linea[21:29])
            fecha_registro = date(int(r1[0:4]), int(r1[5:7]), int(r1[8:10]))
            hora_registro = time(int(r2[0:2]), int(r2[3:5]), int(r2[6:8]))
            items.extend([[identif, fecha_registro, hora_registro]])
        with transaction.atomic():
            for x in items:
                try:
                    marcacion = Marcacionpersona()
                    persona = Persona.objects.get(Dni=str(x[0]))
                    marcacion.Persona = persona
                    marcacion.Fechamarcacion = str(x[1]) + " " + str(x[2])
                    marcacion.Fecha = str(x[1])
                    marcacion.Hora = str(x[2])
                    marcacion.save()
                except Persona.DoesNotExist:
                    pass
                # Asistencia.objects.get_or_create(Codigo=x[0], Fecha=x[1], Hora=x[2])
        return True
        # Quizás te preguntes: ¿Por qué genero una lista nueva en vez de comenzar la transacción al
        # momento de la lectura del archivo?
        # Rpta: Si la estructura del archivo es incorrecta, la transacción no se ejecuta.
    else:
        return False


# x = Hora de Entrada Establecida
# y = Hora en que marcó la entrada
def mintar(x, y):
    tt1_tuple = strptime(x, "%m/%d/%y %H:%M:%S")
    tt2_tuple = strptime(y, "%m/%d/%y %H:%M:%S")
    dif_min = mktime(tt2_tuple) - mktime(tt1_tuple)
    r = dif_min / 60.0
    resultado = Decimal(str(r))
    resultado = resultado.quantize(TWOPLACES, rounding=ROUND_UP)
    return resultado


# x = Hora que marcó Ingreso
# y = Hora que marcó Salida
def horas_trab(x, y):
    timeTuple1 = strptime(x, "%m/%d/%y %H:%M:%S")
    timeTuple2 = strptime(y, "%m/%d/%y %H:%M:%S")
    diferencia = mktime(timeTuple2) - mktime(timeTuple1)
    minutos_trab = int(diferencia / 60.0)
    r = minutos_trab / 60.0
    resultado = Decimal(str(r))
    resultado = resultado.quantize(TWOPLACES, rounding=ROUND_UP)
    return resultado
