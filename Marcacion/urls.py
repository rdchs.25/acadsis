# -*- coding: utf-8 -*-
from django.conf.urls import url

from Marcacion.views import subir_dat, reporte_1_seleccionar_reglas, reporte_1_generar
from Marcacion.views_persona import subir_dat_persona, reporte_1_seleccionar_reglas_persona, reporte_1_generar_persona

app_name = 'Marcacion'

urlpatterns = [
    # Marcacion docente
    url(r'^Marcacion/subir_archivo/$', subir_dat),
    url(r'^Marcacion/subir_archivo/procesar/$', subir_dat),
    url(r'^Marcacion/Reportes/reporte_1/reglas/$', reporte_1_seleccionar_reglas),
    url(r'^Marcacion/Reportes/reporte_1/(\d+)/(\d+)/(\d+)/$', reporte_1_generar),
    # Marcacion personal
    url(r'^Marcacionpersona/subir_archivo/$', subir_dat_persona),
    url(r'^Marcacionpersona/subir_archivo/procesar/$', subir_dat_persona),
    url(r'^Marcacionpersona/Reportes/reporte_1/reglas/$', reporte_1_seleccionar_reglas_persona),
    url(r'^Marcacionpersona/Reportes/reporte_1/(\d+)/(\d+)/(\d+)/$', reporte_1_generar_persona),
]
