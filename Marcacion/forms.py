# -*- coding: utf-8 -*-
from django import forms

from Docentes.models import Docente
from Marcacion.widgets import DateTimeWidget


class ArchivoForm(forms.Form):
    Archivo = forms.FileField()


class IndexReporteForm(forms.Form):
    Desde = forms.DateField(widget=DateTimeWidget)
    Hasta = forms.DateField(widget=DateTimeWidget)
    Docente = forms.ModelChoiceField(
        queryset=Docente.objects.filter(Estado=True).order_by('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'),
        widget=forms.Select(),
        required=True, label='Docente')

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Alumnos/frmalumno.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }
