# -*- coding: utf-8 -*-
from django.contrib import admin
from rangefilter.filter import DateRangeFilter

from Marcacion.models import Marcacion, Marcacionpersona, Reglas


class MigracionModelAdmin(admin.ModelAdmin):
    list_display = ("Docente", "Fecha", "Hora")
    list_filter = (
        ('Fecha', DateRangeFilter),
    )
    search_fields = ("Docente__ApellidoPaterno", "Docente__ApellidoMaterno", "Docente__Nombres")
    actions = ['listado_excel']

    def listado_excel(self, request, queryset):
        from Marcacion.reportes import docentes_excel
        return docentes_excel(queryset)

    listado_excel.short_description = "Reporte listado de marcación"


class MigracionpersonaModelAdmin(admin.ModelAdmin):
    list_display = ("Persona", "Fecha", "Hora")
    list_filter = (
        ('Fecha', DateRangeFilter),
    )
    search_fields = ("Persona__ApellidoPaterno", "Persona__ApellidoMaterno", "Persona__Nombres")
    actions = ['listado_excel']

    def listado_excel(self, request, queryset):
        from Marcacion.reportes import personal_excel
        return personal_excel(queryset)

    listado_excel.short_description = "Reporte listado de marcación"


class ReglasModelAdmin(admin.ModelAdmin):
    list_display = ('Docente', 'Dias', 'Entrada', 'Salida', 'Turno')


admin.site.register(Marcacion, MigracionModelAdmin)
admin.site.register(Marcacionpersona, MigracionpersonaModelAdmin)
admin.site.register(Reglas, ReglasModelAdmin)
