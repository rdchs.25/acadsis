# -*- coding: utf-8 -*-
import xlwt
from django.conf import settings


# El reporte debe generarse por agrupación, lo cual en realidad generará varios reportes,
# uno por persona dentro de "media/Generados/Empresa/"

def generar_excel(nombre_empresa, periodo, nombre_archivo, datos):
    import os

    ruta_archivo = settings.STATICFILES_DIRS[0] + "/generados/" + nombre_empresa + "/"
    # Verifico si existe la ruta (En caso de que no exista, lo creo)
    if os.path.isdir(ruta_archivo) is False:
        os.mkdir(ruta_archivo)

    style = xlwt.XFStyle()
    styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
              'date': xlwt.easyxf('font: height 160;', num_format_str='dd-mm-yyyy'),
              'time': xlwt.easyxf('font: name DejaVu Sans Mono, height 160;', num_format_str='hh:mm:ss'),
              'default': xlwt.easyxf('font: height 160;', num_format_str='@'),
              'headers': xlwt.easyxf('font: height 160, bold 1;', num_format_str='@'),
              'num_data': xlwt.easyxf('font: name DejaVu Sans Mono, height 160;', num_format_str='0.00'),
              'num_data_red': xlwt.easyxf('font: name DejaVu Sans Mono, height 160, color red;', num_format_str='0.00'),
              'num_data_blue': xlwt.easyxf('font: name DejaVu Sans Mono, height 160, color blue;',
                                           num_format_str='0.00'),
              'num_data_strong': xlwt.easyxf('font: name DejaVu Sans Mono, height 160, bold 1;', num_format_str='0.00')}
    book = xlwt.Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 1')

    sheet.write(0, 1, "Desde", style=styles['default'])
    sheet.write(0, 2, periodo[0], style=styles['date'])
    sheet.write(0, 3, "hasta", style=styles['default'])
    sheet.write(0, 4, periodo[1], style=styles['date'])

    i = 2

    flag1 = 0
    flag2 = 0
    # Variables para Sumatoria
    # sum_mintar_m, sum_mintar_t, sum_ht = Decimal("0.00"), Decimal("0.00"), Decimal("0.00")
    for rowx, row in enumerate(datos):
        if len(row[0]) == 2:
            if flag2 == 0:
                cell_style = styles['headers']

                if flag1 != 0:
                    i += 2
                for h in row[0]:
                    sheet.write(rowx + i, 0, h, style=cell_style)
                    i += 1
                i += 1
                sheet.write(rowx + i, 0, "", style=cell_style)
                sheet.write(rowx + i, 1, "Mañana", style=cell_style)
                sheet.write(rowx + i, 4, "---", style=cell_style)
                sheet.write(rowx + i, 5, "Tarde", style=cell_style)
                i += 1

                encabezado = ["Dia", "Entrada", "Salida", "Horas Trabajadas", "---", "Entrada", "Salida",
                              "Horas Trabajadas", "---", "Horas Totales"]
                for c, celda in enumerate(encabezado):
                    sheet.write(rowx + i, c, celda, style=cell_style)
                i += 1
                flag2 = 1
            else:
                pass
        elif len(row[0]) == 1:
            if flag1 == 0:
                flag1 = 1
                cell_style = styles['headers']
                i += 2

                for h in row[0]:
                    sheet.write(rowx + i, 0, h, style=cell_style)
                    i += 1
                i += 1
                encabezado = ["Dia", "Entrada", "Salida", "Horas Trabajadas"]
                for c, celda in enumerate(encabezado):
                    sheet.write(rowx + i, c, celda, style=cell_style)
                i += 1
                comparar_row = row[0]
            else:
                if row[0] != comparar_row:
                    comparar_row = row[0]

                    cell_style = styles['headers']
                    i += 2

                    for h in row[0]:
                        sheet.write(rowx + i, 0, h, style=cell_style)
                        i += 1
                    i += 1
                    encabezado = ["Dia", "Entrada", "Salida", "Horas Trabajadas"]
                    for c, celda in enumerate(encabezado):
                        sheet.write(rowx + i, c, celda, style=cell_style)
                    i += 1
        # Escribo las columnas
        row = row[1:]
        for colx, value in enumerate(row):
            if colx == 0:
                cell_style = styles['default']
            elif colx == 1 or colx == 2 or colx == 5 or colx == 6:
                cell_style = styles['time']
            elif colx == 9:
                cell_style = styles['num_data_strong']
                # sum_ht = sum_ht + value
            else:
                cell_style = styles['num_data']
            sheet.write(rowx + i, colx, value, style=cell_style)
        #
        # foot = ["Sumatoria", "", "", "", sum_mintar_m, "", "", "", "", sum_mintar_t, "", sum_ht]
        # i += 1
        # for c, celda in enumerate(foot):
        # sheet.write(rowx+i, c, celda, style=cell_style)
        # sum_mintar_m, sum_mintar_t, sum_ht = Decimal("0.00"), Decimal("0.00"), Decimal("0.00")

    full_path = ruta_archivo + nombre_archivo
    book.save(full_path)
