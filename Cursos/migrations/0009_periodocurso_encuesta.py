# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0008_auto_20181102_0808'),
    ]

    operations = [
        migrations.AddField(
            model_name='periodocurso',
            name='Encuesta',
            field=models.BooleanField(default=False, verbose_name=b'Encuesta respondida'),
        ),
    ]
