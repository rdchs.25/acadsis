# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0005_curso_moodleid'),
    ]

    operations = [
        migrations.AddField(
            model_name='periodocurso',
            name='Moodleid',
            field=models.PositiveIntegerField(verbose_name=b'Moodle id', null=True, editable=False),
        ),
    ]
