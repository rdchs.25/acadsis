# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0006_periodocurso_moodleid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='periodocurso',
            name='Grupo',
            field=models.CharField(max_length=50),
        ),
    ]
