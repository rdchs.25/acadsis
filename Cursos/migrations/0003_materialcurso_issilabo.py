# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0002_auto_20180102_2013'),
    ]

    operations = [
        migrations.AddField(
            model_name='materialcurso',
            name='Issilabo',
            field=models.BooleanField(default=False),
        ),
    ]
