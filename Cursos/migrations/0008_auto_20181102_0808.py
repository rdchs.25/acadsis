# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0007_auto_20181002_1126'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='materialcurso',
            options={'verbose_name': 'Material del Curso', 'verbose_name_plural': 'Material de Cursos', 'permissions': (('read_materialcurso', 'Observar Material de curso'),)},
        ),
    ]
