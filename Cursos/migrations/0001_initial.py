# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import Cursos.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Curso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(max_length=9)),
                ('Nombre', models.CharField(max_length=60)),
                ('Creditos', models.PositiveIntegerField()),
                ('Ciclo', models.CharField(max_length=2, verbose_name=b'Ciclo', choices=[(b'01', b'1\xc2\xba'), (b'02', b'2\xc2\xba'), (b'03', b'3\xc2\xba'), (b'04', b'4\xc2\xba'), (b'05', b'5\xc2\xba'), (b'06', b'6\xc2\xba'), (b'07', b'7\xc2\xba'), (b'08', b'8\xc2\xba'), (b'09', b'9\xc2\xba'), (b'10', b'10\xc2\xba')])),
                ('Equivalencia', models.CharField(max_length=60, null=True, blank=True)),
                ('Codigo1', models.CharField(max_length=7, null=True, blank=True)),
                ('Creditos_equiv', models.PositiveIntegerField(null=True, blank=True)),
                ('Extracurricular', models.BooleanField(default=False, verbose_name=b'Extra Curricular')),
                ('Proyecto_formativo', models.BooleanField(default=False, verbose_name=b'Proyecto Formativo')),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activada')),
            ],
            options={
                'verbose_name': 'Curso de Carrera',
                'verbose_name_plural': 'Cursos de Carrera',
            },
        ),
        migrations.CreateModel(
            name='MaterialCurso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=100, verbose_name=b'Nombre')),
                ('Descripcion', models.TextField(null=True, verbose_name=b'Descripci\xc3\xb3n', blank=True)),
                ('Archivo', models.FileField(upload_to=Cursos.models.obtenerpath_archivo_materialcurso, max_length=200, verbose_name=b'Archivo')),
                ('Fecha_creacion', models.DateTimeField(auto_now_add=True, verbose_name=b'Fecha creaci\xc3\xb3n')),
                ('Ultima_edicion', models.DateTimeField(auto_now=True, verbose_name=b'Ultima edici\xc3\xb3n')),
            ],
            options={
                'verbose_name': 'Material del Curso',
                'verbose_name_plural': 'Material de Cursos',
            },
        ),
        migrations.CreateModel(
            name='PeriodoCurso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Grupo', models.CharField(max_length=5)),
                ('Aprobacion', models.DecimalField(default=b'11.00', max_digits=4, decimal_places=2)),
                ('EditarAsistencias', models.BooleanField(default=True, verbose_name=b'Editar Asistencias')),
                ('EditarFormula', models.BooleanField(default=False, verbose_name=b'Editar F\xc3\xb3rmula')),
                ('EditarNotas', models.BooleanField(default=False, verbose_name=b'Editar Notas')),
                ('EditarAplazados', models.BooleanField(default=False, verbose_name=b'Editar Aplazados')),
                ('Observaciones', models.TextField(null=True, blank=True)),
                ('Silabo', models.FileField(verbose_name=b'Silabo', upload_to=Cursos.models.obtenerpath_periodocurso, null=True, editable=False, blank=True)),
                ('Curso', models.ForeignKey(to='Cursos.Curso')),
            ],
            options={
                'verbose_name': 'Grupo Horario',
                'verbose_name_plural': 'Grupos Horarios',
                'permissions': (('read_periodocurso', 'Observar Grupos Horario'),),
            },
        ),
        migrations.CreateModel(
            name='SilaboCurso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Sumilla', models.TextField(default=b'Sumilla', verbose_name=b'Sumilla')),
                ('Comp_general', models.TextField(default=b'Competencia General', verbose_name=b'Competencia General')),
                ('Comp_especifica1', models.TextField(default=b'Competencia Espec\xc3\xadfica 01', verbose_name=b'Competencia Espec\xc3\xadfica 01')),
                ('Comp_especifica2', models.TextField(default=b'Competencia Espec\xc3\xadfica 02', verbose_name=b'Competencia Espec\xc3\xadfica 02')),
                ('Comp_especifica3', models.TextField(default=b'Competencia Espec\xc3\xadfica 03', verbose_name=b'Competencia Espec\xc3\xadfica 03')),
                ('Comp_especifica4', models.TextField(default=b'Competencia Espec\xc3\xadfica 04', verbose_name=b'Competencia Espec\xc3\xadfica 04')),
                ('Unidad_didactica1', models.TextField(default=b'Unidad Did\xc3\xa1ctica 01', verbose_name=b'Unidad Did\xc3\xa1ctica 01')),
                ('Unidad_didactica2', models.TextField(default=b'Unidad Did\xc3\xa1ctica 02', verbose_name=b'Unidad Did\xc3\xa1ctica 02')),
                ('Unidad_didactica3', models.TextField(default=b'Unidad Did\xc3\xa1ctica 03', verbose_name=b'Unidad Did\xc3\xa1ctica 03')),
                ('Unidad_didactica4', models.TextField(default=b'Unidad Did\xc3\xa1ctica 04', verbose_name=b'Unidad Did\xc3\xa1ctica 04')),
                ('Estrategia_metodologica', models.TextField(default=b'Estrategias metodol\xc3\xb3gicas', verbose_name=b'Estrategias metodol\xc3\xb3gicas')),
                ('Material_educativo', models.TextField(default=b'Material Educativo', verbose_name=b'Material Educativo')),
                ('Evaluacion_trabajo', models.TextField(default=b'Evaluaci\xc3\xb3n Trabajos', verbose_name=b'Evaluaci\xc3\xb3n Trabajos')),
                ('Evaluacion_conceptual', models.TextField(default=b'Evaluaci\xc3\xb3n Conceptual', verbose_name=b'Evaluaci\xc3\xb3n Conceptual')),
                ('Evaluacion_procedimental', models.TextField(default=b'Evaluaci\xc3\xb3n Procedimental', verbose_name=b'Evaluaci\xc3\xb3n Procedimental')),
                ('Evaluacion_actitudinal', models.TextField(default=b'Evaluaci\xc3\xb3n Actitudinal', verbose_name=b'Evaluaci\xc3\xb3n Actitudinal')),
                ('Requisito_aprobacion', models.TextField(default=b'Requisitos Aprobaci\xc3\xb3n', verbose_name=b'Requisitos Aprobaci\xc3\xb3n')),
                ('Biblio_obligatoria', models.TextField(default=b'Bibliograf\xc3\xada Obligatoria', verbose_name=b'Bibliograf\xc3\xada Obligatoria')),
                ('Biblio_consulta', models.TextField(default=b'Bibliograf\xc3\xada Consulta', verbose_name=b'Bibliograf\xc3\xada Consulta')),
                ('Fecha_creacion', models.DateTimeField(auto_now_add=True, verbose_name=b'Fecha creaci\xc3\xb3n')),
                ('Ultima_edicion', models.DateTimeField(auto_now=True, verbose_name=b'Ultima edici\xc3\xb3n')),
                ('PeriodoCurso', models.ForeignKey(to='Cursos.PeriodoCurso')),
            ],
            options={
                'verbose_name': 'Silabo del Curso',
                'verbose_name_plural': 'Silabos de Cursos',
            },
        ),
    ]
