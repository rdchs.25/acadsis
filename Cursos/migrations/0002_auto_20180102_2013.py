# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0001_initial'),
        ('Cursos', '0001_initial'),
        ('Carreras', '0001_initial'),
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='periodocurso',
            name='Docente',
            field=models.ForeignKey(to='Docentes.Docente'),
        ),
        migrations.AddField(
            model_name='periodocurso',
            name='Periodo',
            field=models.ForeignKey(to='Periodos.Periodo'),
        ),
        migrations.AddField(
            model_name='materialcurso',
            name='PeriodoCurso',
            field=models.ForeignKey(to='Cursos.PeriodoCurso'),
        ),
        migrations.AddField(
            model_name='curso',
            name='Carrera',
            field=models.ForeignKey(to='Carreras.Carrera'),
        ),
        migrations.AddField(
            model_name='curso',
            name='Equivalencias',
            field=models.ManyToManyField(related_name='inverso_equiv', to='Cursos.Curso', blank=True),
        ),
        migrations.AddField(
            model_name='curso',
            name='Periodo',
            field=models.ForeignKey(verbose_name=b'Per\xc3\xadodo', to='Periodos.Periodo'),
        ),
        migrations.AddField(
            model_name='curso',
            name='Prerequisito',
            field=models.ForeignKey(blank=True, to='Cursos.Curso', null=True),
        ),
        migrations.AddField(
            model_name='curso',
            name='Prerequisitos',
            field=models.ManyToManyField(related_name='inverso_prereq', to='Cursos.Curso', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='periodocurso',
            unique_together=set([('Periodo', 'Curso', 'Grupo', 'Docente')]),
        ),
    ]
