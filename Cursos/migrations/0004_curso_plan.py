# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0005_detalleplan_detalleelectivo'),
        ('Cursos', '0003_materialcurso_issilabo'),
    ]

    operations = [
        migrations.AddField(
            model_name='curso',
            name='Plan',
            field=models.ForeignKey(default=None, blank=True, to='Planestudio.Plan', null=True, verbose_name=b'Plan de estudio'),
        ),
    ]
