# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Cursos', '0004_curso_plan'),
    ]

    operations = [
        migrations.AddField(
            model_name='curso',
            name='Moodleid',
            field=models.PositiveIntegerField(verbose_name=b'Moodle id', null=True, editable=False),
        ),
    ]
