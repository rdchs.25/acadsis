#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Cursos.models import Curso, PeriodoCurso


def crearcursos():
    cursos = PeriodoCurso.objects.filter(Periodo_id=25)
    for curso in cursos:
        print curso
        curso.save()


crearcursos()
