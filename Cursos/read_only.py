# -*- coding: utf-8 -*-
# imports para xlwt
import datetime
from decimal import Decimal

# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.db.models.query import QuerySet, ValuesQuerySet
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from reportlab.lib.pagesizes import A4
# imports reportlab
from reportlab.pdfgen import canvas

from Asistencias.models import AsistenciaDocente
from Carreras.models import Carrera
from Cursos.models import PeriodoCurso
from Docentes.models import Docente
from Encuesta.models import Encuesta, CategoriaItem, Item, Respuesta, Respuestadocente
from Horarios.models import Horario
from Matriculas.models import MatriculaCursos, MatriculaCiclo
from Notas.formatos import acta_notas
# importar formatos reportlab creados
from Notas.models import Nota, NotasAlumno
from Periodos.models import Periodo
from funciones import response_excel


def enabled_formulas(grupos, estado):
    for grupo in grupos:
        grupo.EditarFormula = estado
        grupo.save()
    return HttpResponseRedirect('')


def enabled_aplazados(grupos, estado):
    for grupo in grupos:
        grupo.EditarAplazados = estado
        grupo.save()
    return HttpResponseRedirect('')


def enabled_notas(grupos, estado):
    for grupo in grupos:
        grupo.EditarNotas = estado
        grupo.save()
    return HttpResponseRedirect('')


def enabled_asistencias(grupos, estado):
    for grupo in grupos:
        grupo.EditarAsistencias = estado
        grupo.save()
    return HttpResponseRedirect('')


def actas_periodocurso(grupos):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=actas.pdf'
    p = canvas.Canvas(response, pagesize=A4)
    n = 201110026
    nombrearchivo = ''
    for grupo in grupos:
        m = 0
        o = grupo.matriculacursos_set.filter(MatriculaCiclo__Estado='Matriculado',
                                             Convalidado=False, Estado=True).count()
        j = 1
        n += 1
        for m in range(0, o, 21):
            matriculados = grupo.matriculacursos_set.filter(MatriculaCiclo__Estado='Matriculado',
                                                            Convalidado=False, Estado=True).order_by(
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')[0 + m:21 + m]
            p = acta_notas(p)
            # Agrego el correlativo
            p.setFont("Helvetica", 14)
            # p.drawString(450,760,"Nº " + str(n))
            p.setFont("Helvetica-Bold", 10)
            p.drawString(120, 710, grupo.Curso.Nombre)
            p.drawString(120, 690, str(grupo.Curso.Creditos))
            p.drawString(160, 670, grupo.Curso.Carrera.Carrera)
            p.drawString(120, 650, unicode(grupo.Docente.Nombres.title() + ' ' +
                                           grupo.Docente.ApellidoPaterno.title() + ' ' +
                                           grupo.Docente.ApellidoMaterno.title()))

            p.drawString(450, 710, str(grupo.Curso.Codigo))
            p.drawString(450, 690, grupo.Grupo)
            p.drawString(450, 670, str(grupo.Periodo))
            # p.drawString(450,670,'2013-II')
            mydate = grupo.Curso.Periodo.Fin
            mn = {'01': 'Enero', '02': 'Febrero', '03': 'Marzo', '04': 'Abril', '05': 'Mayo',
                  '06': 'Junio', '07': 'Julio', '08': 'Agosto', '09': 'Setiembre',
                  '10': 'Octubre', '11': 'Noviembre', '12': 'Diciembre'}
            p.drawString(370, 130, "Chiclayo,   " + str(mydate.strftime("%d")))
            p.drawString(435, 130, " de   " + str(mn[mydate.strftime("%m")]))
            p.drawString(510, 130, " del  " + str(mydate.strftime("%Y")))
            p.drawString(450, 650, 'FINAL')

            p.drawString(372, 60, "Mag. Merly del Rocio Nevado Chauca")

            dict = {'0.00': 'cero', '1.00': 'uno', '2.00': 'dos', '3.00': 'tres', '4.00': 'cuatro', '5.00': 'cinco',
                    '6.00': 'seis', '7.00': 'siete', '8.00': 'ocho', '9.00': 'nueve', '10.00': 'diez',
                    '11.00': 'once', '12.00': 'doce', '13.00': 'trece', '14.00': 'catorce', '15.00': 'quince',
                    '16.00': 'dieciseis', '17.00': 'diecisiete', '18.00': 'dieciocho', '19.00': 'diecinueve',
                    '20.00': 'veinte'}
            i = 565
            nombredocente = unicode(grupo.Docente.Nombres.title() + ' ' +
                                    grupo.Docente.ApellidoPaterno.title() + ' ' + grupo.Docente.ApellidoMaterno.title())
            nombrearchivo = grupo.Curso.Nombre + ' - ' + nombredocente + '.pdf'
            for mat in matriculados:
                if j < 10:
                    p.drawString(70, i, '0' + str(j))
                else:
                    p.drawString(70, i, str(j))
                p.drawString(93, i, mat.MatriculaCiclo.Alumno.Codigo)
                alumnomatriculado = mat.MatriculaCiclo.Alumno
                nombrealumno = alumnomatriculado.ApellidoPaterno + ' ' + \
                               alumnomatriculado.ApellidoMaterno + ' ' + alumnomatriculado.Nombres
                p.drawString(163, i, nombrealumno.title())
                if mat.Promedio() < Decimal('10.00'):
                    p.drawString(
                        443, i, '0' + str(mat.Promedio()).split('.')[0])
                else:
                    p.drawString(443, i, str(mat.Promedio()).split('.')[0])
                p.drawString(513, i, dict[str(mat.Promedio())])
                i = i - 20
                j += 1
            p.showPage()
            p.save()
    response['Content-Disposition'] = "attachment; filename=" + "'" + nombrearchivo + "'"
    return response


def matriculados_grupo_excel(request, grupos):
    output_name = "matriculados"
    encoding = 'utf8'
    import StringIO
    output = StringIO.StringIO()
    try:
        import xlwt
    except ImportError:
        # xlwt doesn't exist; fall back to csv
        pass

    book = xlwt.Workbook(encoding=encoding)
    i = 0
    for dat in grupos:
        i = i + 1
        if dat.matriculacursos_set.all().count() != 0:
            data = dat.matriculacursos_set.order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                    'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                    'MatriculaCiclo__Alumno__Nombres').values(
                'MatriculaCiclo__Alumno__Codigo', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres',
                'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Categoria__Categoria',
                'MatriculaCiclo__Categoria__Pago', 'MatriculaCiclo__Alumno__AnioIngreso',
                'MatriculaCiclo__Alumno__Semestre', 'FechaRetiro', 'MatriculaCiclo_id')
            headers = ['MatriculaCiclo__Alumno__Codigo', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                       'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres',
                       'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Categoria__Categoria',
                       'MatriculaCiclo__Categoria__Pago', 'MatriculaCiclo__Alumno__AnioIngreso',
                       'MatriculaCiclo__Alumno__Semestre', 'FechaRetiro', 'MatriculaCiclo_id']
            titulos = ['Código', 'Apellido Paterno', 'Apellido Materno', 'Nombres',
                       'Carrera', 'Categoria', 'Monto pensión', 'Año Ingreso', 'Semestre Ingreso', 'Fecha Retiro',
                       "Pago pensión 1",
                       "Pago pensión 2",
                       "Pago pensión 3", "Pago pensión 4", "Pago pensión 5"]

            # Make sure we've got the right type of data to work with
            valid_data = False
            if isinstance(data, ValuesQuerySet):
                matriculados = list(data)
            elif isinstance(data, QuerySet):
                matriculados = list(data.values())
            if hasattr(data, '__getitem__'):
                if isinstance(data[0], dict):
                    if headers is None:
                        headers = data[0].keys()
                    data = [[row[col] for col in headers] for row in data]
                    if titulos is None:
                        data.insert(0, headers)
                    else:
                        data.insert(0, titulos)
                if hasattr(data[0], '__getitem__'):
                    valid_data = True
            assert valid_data is True, "ExcelResponse requires a sequence of sequences"

            sheet = book.add_sheet('Hoja%s' % i)
            styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
                      'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
                      'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
                      'default': xlwt.Style.default_style}
            cell_style = ''
            contador = 0
            for rowx, row in enumerate(data):
                for colx, value in enumerate(row):
                    if isinstance(value, datetime.datetime):
                        cell_style = styles['datetime']
                    elif isinstance(value, datetime.date):
                        cell_style = styles['date']
                    elif isinstance(value, datetime.time):
                        cell_style = styles['time']
                    else:
                        cell_style = styles['default']
                    print (str(colx))
                    if contador == 0:
                        sheet.write(rowx + 6, colx, value, style=cell_style)
                    elif contador > 0:
                        if colx <= 9:
                            sheet.write(rowx + 6, colx, value, style=cell_style)
                        else:
                            matriculado = MatriculaCiclo.objects.get(id=value)
                            # Verificar pago pensiones
                            pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="1ra",
                                                                      Estado=True)
                            if pagos.count() > 0:
                                sheet.write(rowx + 6, colx, "SI", style=cell_style)
                            else:
                                sheet.write(rowx + 6, colx, "NO", style=cell_style)

                            pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="2da",
                                                                      Estado=True)
                            if pagos.count() > 0:
                                sheet.write(rowx + 6, colx + 1, "SI", style=cell_style)
                            else:
                                sheet.write(rowx + 6, colx + 1, "NO", style=cell_style)

                            pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="3ra",
                                                                      Estado=True)
                            if pagos.count() > 0:
                                sheet.write(rowx + 6, colx + 2, "SI", style=cell_style)
                            else:
                                sheet.write(rowx + 6, colx + 2, "NO", style=cell_style)
                            pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="4ta",
                                                                      Estado=True)
                            if pagos.count() > 0:
                                sheet.write(rowx + 6, colx + 3, "SI", style=cell_style)
                            else:
                                sheet.write(rowx + 6, colx + 3, "NO", style=cell_style)
                            pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="5ta",
                                                                      Estado=True)
                            if pagos.count() > 0:
                                sheet.write(rowx + 6, colx + 4, "SI", style=cell_style)
                            else:
                                sheet.write(rowx + 6, colx + 4, "NO", style=cell_style)
                            break

                contador = contador + 1

            sheet.write(0, 0, 'Codigo Curso:', style=cell_style)
            sheet.write(1, 0, 'Curso', style=cell_style)
            sheet.write(2, 0, 'Grupo Horario:', style=cell_style)
            sheet.write(3, 0, 'Docente:', style=cell_style)

            sheet.write(0, 1, dat.Curso.Codigo, style=cell_style)
            sheet.write(1, 1, dat.Curso.Nombre, style=cell_style)
            sheet.write(2, 1, dat.Grupo, style=cell_style)
            sheet.write(3, 1, dat.Docente.Nombres + ' ' + dat.Docente.ApellidoPaterno +
                        ' ' + dat.Docente.ApellidoMaterno, style=cell_style)

            content_type = 'application/vnd.ms-excel'
            file_ext = 'xls'
        else:
            mensaje = "El curso %s no tiene matriculados" % dat
            links = "<a href=''>Volver</a>"
            return render_to_response("Periodos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type=content_type)
    response['Content-Disposition'] = 'attachment;filename="%s.%s"' % (
        output_name.replace('"', '\"'), file_ext)
    return response


def resultado_encuesta_excel(request, grupos):
    output_name = "resultado_encuesta"
    encoding = 'utf8'
    import StringIO
    output = StringIO.StringIO()
    try:
        from xlwt import Workbook, easyxf
        import xlwt
    except ImportError:
        # xlwt doesn't exist; fall back to csv
        pass
    book = xlwt.Workbook(encoding=encoding)
    i = 0
    color = 0x9ACD32
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid; align: horiz center, vert centre')
    style_heads.pattern.pattern_fore_colour = color
    style_registros = easyxf(
        'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')

    for dat in grupos:
        i = i + 1
        periodo_id = dat.Periodo.id
        encuesta = Encuesta.objects.get(Periodo__id=periodo_id)
        data = dat.matriculacursos_set.filter(Encuesta=True)
        cantidad_encuestas = data.count()
        maximo = Decimal(cantidad_encuestas * 5)
        if cantidad_encuestas != 0:
            nombre_docente = dat.Docente.Nombres + ' ' + \
                             dat.Docente.ApellidoPaterno + ' ' + dat.Docente.ApellidoMaterno
            nombre_curso = dat.Curso.Nombre
            nombre_carrera = dat.Curso.Carrera.Carrera
            sheet = book.add_sheet('Hoja%s' % i)
            # recopilamos data
            categorias = CategoriaItem.objects.filter(
                Encuesta=encuesta).order_by('Orden')

            for categoria in categorias:
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                for item in items:
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)

            sheet.write_merge(0, 3, 0, 1, 'ITEMS DE EVALUACIÓN', style_heads)
            sheet.write(0, 2, 'Puntaje', style_heads)
            sheet.write(1, 2, nombre_carrera, style_heads)
            sheet.write(2, 2, nombre_docente, style_heads)
            sheet.write(3, 2, nombre_curso, style_heads)
            inicio_y = 4

            resumen_dimensiones = []
            promedio_global = Decimal("0.00")
            cantidad_categorias = categorias.count()
            puntaje1 = 0
            puntaje2 = 0
            puntaje3 = 0
            puntaje4 = 0
            puntaje5 = 0
            for categoria in categorias:
                categoria_resumen = []
                categoria_resumen.append(categoria.Descripcion)
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                suma_promedios = Decimal("0.00")
                cantidad_items = items.count()
                categoria_satisfaccion = False
                if categoria.Orden == 7:
                    categoria_satisfaccion = True
                for item in items:
                    # nombre de item
                    sheet.write(inicio_y, 0, item.Orden, style_registros)
                    sheet.write(inicio_y, 1, item.Descripcion, style_registros)
                    suma_valores = Decimal("0.00")
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)
                        valor_respuesta = 0
                        for respuesta in respuestas:
                            valor_respuesta = respuesta.Valor
                        # SATISFACCIÓN GENERAL
                        if categoria_satisfaccion == True:
                            if valor_respuesta == 1:
                                puntaje1 = puntaje1 + 1
                            elif valor_respuesta == 2:
                                puntaje2 = puntaje2 + 1
                            elif valor_respuesta == 3:
                                puntaje3 = puntaje3 + 1
                            elif valor_respuesta == 4:
                                puntaje4 = puntaje4 + 1
                            elif valor_respuesta == 5:
                                puntaje5 = puntaje5 + 1

                        suma_valores = suma_valores + Decimal(valor_respuesta)
                    promedio = 20 * suma_valores / maximo
                    suma_promedios = suma_promedios + promedio
                    sheet.write(inicio_y, 2, round(
                        promedio, 2), style_registros)
                    inicio_y = inicio_y + 1
                promedio_categoria = suma_promedios / Decimal(cantidad_items)
                promedio_global = promedio_global + promedio_categoria
                categoria_resumen.append(promedio_categoria)
                resumen_dimensiones.append(categoria_resumen)
                # nombre de categoria
                sheet.write_merge(inicio_y, inicio_y, 0, 1,
                                  categoria.Descripcion, style_heads)
                sheet.write(inicio_y, 2, round(
                    promedio_categoria, 2), style_heads)
                inicio_y = inicio_y + 1
            promedio_global = promedio_global / Decimal(cantidad_categorias)

            # DIMENSIONES
            sheet.write_merge(0, 3, 4, 4, 'DIMENSIONES', style_heads)
            sheet.write(0, 5, 'Puntaje', style_heads)
            sheet.write(1, 5, nombre_carrera, style_heads)
            sheet.write(2, 5, nombre_docente, style_heads)
            sheet.write(3, 5, nombre_curso, style_heads)
            inicio_y = 4

            for resumen in resumen_dimensiones:
                sheet.write(inicio_y, 4, resumen[0], style_registros)
                sheet.write(inicio_y, 5, round(resumen[1], 1), style_registros)
                inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, 'Puntaje Global', style_heads)
            sheet.write(inicio_y, 5, round(promedio_global, 1), style_heads)
            inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, 'Número de Encuestas', style_heads)
            sheet.write(inicio_y, 5, cantidad_encuestas, style_heads)
            inicio_y = inicio_y + 2

            # SATISFACCIÓN GENERAL
            sheet.write(inicio_y, 4, "SATISFACCIÓN GENERAL", style_heads)
            sheet.write(inicio_y, 5, "N°", style_heads)
            sheet.write(inicio_y, 6, "%", style_heads)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Muy Insatisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje1, style_registros)
            porcentaje1 = Decimal(puntaje1) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje1, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Insatisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje2, style_registros)
            porcentaje2 = Decimal(puntaje2) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje2, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Indiferente", style_registros)
            sheet.write(inicio_y, 5, puntaje3, style_registros)
            porcentaje3 = Decimal(puntaje3) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje3, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Satisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje4, style_registros)
            porcentaje4 = Decimal(puntaje4) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje4, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Muy satisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje5, style_registros)
            porcentaje5 = Decimal(puntaje5) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje5, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Total", style_heads)
            sheet.write(inicio_y, 5, puntaje1 + puntaje2 + puntaje3 + puntaje4 + puntaje5, style_heads)
            sheet.write(inicio_y, 6, round(porcentaje1 + porcentaje2 + porcentaje3 + porcentaje4 + porcentaje5, 2),
                        style_heads)
            inicio_y = inicio_y + 1
        else:
            mensaje = "El curso %s no tiene matriculados" % dat
            links = "<a href=''>Volver</a>"
            return render_to_response("Periodos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        # iniciamos autoevaluación docente
        i = i + 1
        encuesta = Encuesta.objects.get(Periodo__id=periodo_id)
        data = PeriodoCurso.objects.filter(id=dat.id)
        cantidad_encuestas = data.count()
        maximo = Decimal(cantidad_encuestas * 5)
        if dat.Encuesta is True:
            nombre_docente = dat.Docente.Nombres + ' ' + \
                             dat.Docente.ApellidoPaterno + ' ' + dat.Docente.ApellidoMaterno
            nombre_curso = dat.Curso.Nombre
            nombre_carrera = dat.Curso.Carrera.Carrera
            sheet = book.add_sheet('Hoja%s' % i)
            # recopilamos data
            categorias = CategoriaItem.objects.filter(
                Encuesta=encuesta).order_by('Orden')

            for categoria in categorias:
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                for item in items:
                    for grupohorario in data:
                        respuestas = Respuestadocente.objects.filter(
                            Periodocurso=grupohorario, Item=item)

            sheet.write_merge(0, 3, 0, 1, 'ITEMS DE EVALUACIÓN', style_heads)
            sheet.write(0, 2, 'Puntaje', style_heads)
            sheet.write(1, 2, nombre_carrera, style_heads)
            sheet.write(2, 2, nombre_docente, style_heads)
            sheet.write(3, 2, nombre_curso, style_heads)
            inicio_y = 4

            resumen_dimensiones = []
            promedio_global = Decimal("0.00")
            cantidad_categorias = categorias.count()
            puntaje1 = 0
            puntaje2 = 0
            puntaje3 = 0
            puntaje4 = 0
            puntaje5 = 0
            for categoria in categorias:
                categoria_resumen = []
                categoria_resumen.append(categoria.Descripcion)
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                suma_promedios = Decimal("0.00")
                cantidad_items = items.count()
                categoria_satisfaccion = False
                if categoria.Orden == 7:
                    categoria_satisfaccion = True
                for item in items:
                    # nombre de item
                    sheet.write(inicio_y, 0, item.Orden, style_registros)
                    sheet.write(inicio_y, 1, item.Descripcion, style_registros)
                    suma_valores = Decimal("0.00")
                    for grupohorario in data:
                        respuestas = Respuestadocente.objects.filter(
                            Periodocurso=grupohorario, Item=item)
                        valor_respuesta = 0
                        for respuesta in respuestas:
                            valor_respuesta = respuesta.Valor
                        # SATISFACCIÓN GENERAL
                        if categoria_satisfaccion == True:
                            if valor_respuesta == 1:
                                puntaje1 = puntaje1 + 1
                            elif valor_respuesta == 2:
                                puntaje2 = puntaje2 + 1
                            elif valor_respuesta == 3:
                                puntaje3 = puntaje3 + 1
                            elif valor_respuesta == 4:
                                puntaje4 = puntaje4 + 1
                            elif valor_respuesta == 5:
                                puntaje5 = puntaje5 + 1

                        suma_valores = suma_valores + Decimal(valor_respuesta)
                    promedio = 20 * suma_valores / maximo
                    suma_promedios = suma_promedios + promedio
                    sheet.write(inicio_y, 2, round(
                        promedio, 2), style_registros)
                    inicio_y = inicio_y + 1
                promedio_categoria = suma_promedios / Decimal(cantidad_items)
                promedio_global = promedio_global + promedio_categoria
                categoria_resumen.append(promedio_categoria)
                resumen_dimensiones.append(categoria_resumen)
                # nombre de categoria
                sheet.write_merge(inicio_y, inicio_y, 0, 1,
                                  categoria.Descripcion, style_heads)
                sheet.write(inicio_y, 2, round(
                    promedio_categoria, 2), style_heads)
                inicio_y = inicio_y + 1
            promedio_global = promedio_global / Decimal(cantidad_categorias)

            # DIMENSIONES
            sheet.write_merge(0, 3, 4, 4, 'DIMENSIONES', style_heads)
            sheet.write(0, 5, 'Puntaje', style_heads)
            sheet.write(1, 5, nombre_carrera, style_heads)
            sheet.write(2, 5, nombre_docente, style_heads)
            sheet.write(3, 5, nombre_curso, style_heads)
            inicio_y = 4

            for resumen in resumen_dimensiones:
                sheet.write(inicio_y, 4, resumen[0], style_registros)
                sheet.write(inicio_y, 5, round(resumen[1], 1), style_registros)
                inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, 'Puntaje Global', style_heads)
            sheet.write(inicio_y, 5, round(promedio_global, 1), style_heads)
            inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, 'Número de Encuestas', style_heads)
            sheet.write(inicio_y, 5, cantidad_encuestas, style_heads)
            inicio_y = inicio_y + 2

            # SATISFACCIÓN GENERAL
            sheet.write(inicio_y, 4, "SATISFACCIÓN GENERAL", style_heads)
            sheet.write(inicio_y, 5, "N°", style_heads)
            sheet.write(inicio_y, 6, "%", style_heads)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Muy Insatisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje1, style_registros)
            porcentaje1 = Decimal(puntaje1) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje1, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Insatisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje2, style_registros)
            porcentaje2 = Decimal(puntaje2) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje2, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Indiferente", style_registros)
            sheet.write(inicio_y, 5, puntaje3, style_registros)
            porcentaje3 = Decimal(puntaje3) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje3, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Satisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje4, style_registros)
            porcentaje4 = Decimal(puntaje4) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje4, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Muy satisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje5, style_registros)
            porcentaje5 = Decimal(puntaje5) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje5, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Total", style_heads)
            sheet.write(inicio_y, 5, puntaje1 + puntaje2 + puntaje3 + puntaje4 + puntaje5, style_heads)
            sheet.write(inicio_y, 6, round(porcentaje1 + porcentaje2 + porcentaje3 + porcentaje4 + porcentaje5, 2),
                        style_heads)
            inicio_y = inicio_y + 1
        content_type = 'application/vnd.ms-excel'
        file_ext = 'xls'
    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type=content_type)
    response['Content-Disposition'] = 'attachment;filename="%s.%s"' % (
        output_name.replace('"', '\"'), file_ext)
    return response


def reporte_resumen_encuesta(request, grupos):
    output_name = "resultado_encuesta"
    encoding = 'utf8'
    import StringIO
    output = StringIO.StringIO()
    try:
        from xlwt import Workbook, easyxf
        import xlwt
    except ImportError:
        # xlwt doesn't exist; fall back to csv
        pass
    book = xlwt.Workbook(encoding=encoding)
    i = 0
    color = 0x9ACD32
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid; align: horiz center, vert centre')
    style_heads.pattern.pattern_fore_colour = color
    style_registros = easyxf(
        'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')

    for dat in grupos:
        i = i + 1
        periodo_id = dat.Periodo.id
        encuesta = Encuesta.objects.get(Periodo__id=periodo_id)
        data = dat.matriculacursos_set.filter(Encuesta=True)
        cantidad_encuestas = data.count()
        maximo = Decimal(cantidad_encuestas * 5)
        if cantidad_encuestas != 0:
            nombre_docente = dat.Docente.Nombres + ' ' + \
                             dat.Docente.ApellidoPaterno + ' ' + dat.Docente.ApellidoMaterno
            nombre_curso = dat.Curso.Nombre
            nombre_carrera = dat.Curso.Carrera.Carrera
            sheet = book.add_sheet('Hoja%s' % i)
            # recopilamos data
            categorias = CategoriaItem.objects.filter(
                Encuesta=encuesta).order_by('Orden')

            for categoria in categorias:
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                for item in items:
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)

            sheet.write_merge(0, 3, 0, 1, 'ITEMS DE EVALUACIÓN', style_heads)
            sheet.write(0, 2, 'Puntaje', style_heads)
            sheet.write(1, 2, nombre_carrera, style_heads)
            sheet.write(2, 2, nombre_docente, style_heads)
            sheet.write(3, 2, nombre_curso, style_heads)
            inicio_y = 4

            resumen_dimensiones = []
            promedio_global = Decimal("0.00")
            cantidad_categorias = categorias.count()
            puntaje1 = 0
            puntaje2 = 0
            puntaje3 = 0
            puntaje4 = 0
            puntaje5 = 0
            for categoria in categorias:
                categoria_resumen = []
                categoria_resumen.append(categoria.Descripcion)
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                suma_promedios = Decimal("0.00")
                cantidad_items = items.count()
                categoria_satisfaccion = False
                if categoria.Orden == 7:
                    categoria_satisfaccion = True
                for item in items:
                    # nombre de item
                    sheet.write(inicio_y, 0, item.Orden, style_registros)
                    sheet.write(inicio_y, 1, item.Descripcion, style_registros)
                    suma_valores = Decimal("0.00")
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)
                        valor_respuesta = 0
                        for respuesta in respuestas:
                            valor_respuesta = respuesta.Valor
                        # SATISFACCIÓN GENERAL
                        if categoria_satisfaccion is True:
                            if valor_respuesta == 1:
                                puntaje1 = puntaje1 + 1
                            elif valor_respuesta == 2:
                                puntaje2 = puntaje2 + 1
                            elif valor_respuesta == 3:
                                puntaje3 = puntaje3 + 1
                            elif valor_respuesta == 4:
                                puntaje4 = puntaje4 + 1
                            elif valor_respuesta == 5:
                                puntaje5 = puntaje5 + 1

                        suma_valores = suma_valores + Decimal(valor_respuesta)
                    promedio = 20 * suma_valores / maximo
                    suma_promedios = suma_promedios + promedio
                    sheet.write(inicio_y, 2, round(
                        promedio, 2), style_registros)
                    inicio_y = inicio_y + 1
                promedio_categoria = suma_promedios / Decimal(cantidad_items)
                promedio_global = promedio_global + promedio_categoria
                categoria_resumen.append(promedio_categoria)
                resumen_dimensiones.append(categoria_resumen)
                # nombre de categoria
                sheet.write_merge(inicio_y, inicio_y, 0, 1,
                                  categoria.Descripcion, style_heads)
                sheet.write(inicio_y, 2, round(
                    promedio_categoria, 2), style_heads)
                inicio_y = inicio_y + 1
            promedio_global = promedio_global / Decimal(cantidad_categorias)

            content_type = 'application/vnd.ms-excel'
            file_ext = 'xls'
        else:
            mensaje = "El curso %s no tiene matriculados" % dat
            links = "<a href=''>Volver</a>"
            return render_to_response("Periodos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type=content_type)
    response['Content-Disposition'] = 'attachment;filename="%s.%s"' % (
        output_name.replace('"', '\"'), file_ext)
    return response


def reporte_horarios_x_grupo_excel(request, grupos):
    import xlwt

    wb = xlwt.Workbook()
    ws = wb.add_sheet('Hoja1')

    if len(grupos) != 0:
        headers = ['Grupo', 'Curso__Codigo', 'Curso__Nombre', 'Curso__Ciclo', 'Curso__Carrera__Carrera',
                   'Periodo__Anio',
                   'Periodo__Semestre', 'Docente__ApellidoPaterno', 'Docente__ApellidoMaterno', 'Docente__Nombres']
        titulos = ['Grupo', 'Codigo', 'Curso', 'Ciclo', 'Carrera',
                   'Semestre', 'Docente', 'Aula', 'Hora Inicio', 'Hora Fin', 'Silabo']

        # Cols
        ws.write(0, 0, "Grupo")
        ws.write(0, 1, "Codigo")
        ws.write(0, 2, "Curso")
        ws.write(0, 3, "Ciclo")
        ws.write(0, 4, "Carrera")
        ws.write(0, 5, "Semestre")
        ws.write(0, 6, "Docente")
        ws.write(0, 7, "Aula")
        ws.write(0, 8, "Dia")
        ws.write(0, 9, "Hora Inicio")
        ws.write(0, 10, "Hora Fin")
        ws.write(0, 11, "Silabo")
        ws.write(0, 12, "Silabo")
        ws.write(0, 13, "correo")
        ws.write(0, 14, "parcial 1")
        ws.write(0, 15, "username")
        ws.write(0, 16, "apellidos")
        ws.write(0, 17, "nombres")

        # Fil
        i = 0
        for dat in grupos:
            horarios_por_periodocurso = Horario.objects.filter(
                PeriodoCurso=dat.id).order_by('Dia')

            matriculados = MatriculaCursos.objects.filter(PeriodoCurso=dat)
            parcial = 'No'
            for mat in matriculados:
                try:
                    n_parcial = mat.notasalumno_set.get(
                        Nota__Identificador='PP1')
                    if n_parcial.Valor != Decimal('0'):
                        # if mat.Promedio() != Decimal('0'):
                        parcial = 'Si'
                        break
                except:
                    pass

            for hora in horarios_por_periodocurso:
                i = i + 1
                ws.write(i, 0, dat.Grupo)
                ws.write(i, 1, dat.Curso.Codigo)
                ws.write(i, 2, dat.Curso.Nombre)
                ws.write(i, 3, dat.Curso.Ciclo)
                ws.write(i, 4, dat.Curso.Carrera.Carrera)
                ws.write(i, 5, '%s-%s' %
                         (dat.Periodo.Anio, dat.Periodo.Semestre))
                ws.write(i, 6, '%s %s %s' % (dat.Docente.ApellidoPaterno,
                                             dat.Docente.ApellidoMaterno, dat.Docente.Nombres))
                ws.write(i, 7, hora.Seccion)
                ws.write(i, 8, hora.Dia)
                ws.write(i, 9, hora.HoraInicio, xlwt.easyxf(
                    num_format_str='hh:mm:ss'))
                ws.write(i, 10, hora.HoraFin, xlwt.easyxf(
                    num_format_str='hh:mm:ss'))
                if dat.Silabo != '':
                    ws.write(i, 11, 'Si')
                else:
                    ws.write(i, 11, 'No')
                ws.write(i, 12, dat.Silabo.name)

                nota = ''
                for d in dat.nota_set.all():
                    pass
                    # nota += str(d.Nota) + ','
                ws.write(i, 13, nota)
                ws.write(i, 14, parcial)
                ws.write(i, 15, dat.Docente.Email)
                ws.write(i, 16, '%s %s' % (
                    dat.Docente.ApellidoPaterno, dat.Docente.ApellidoMaterno))
                ws.write(i, 17, dat.Docente.Nombres)

    else:
        mensaje = "No existen grupos horarios seleccionados"
        links = "<a href=''>Volver</a>"
        return render_to_response("Periodos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

    fname = 'reporte_grupos_horarios.xls'
    response = HttpResponse(content_type="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=%s' % fname

    wb.save(response)

    return response


@csrf_protect
def ver_periodocurso(request):
    periodocursos = []
    if request.user.is_authenticated() and request.user.has_perm('Cursos.read_periodocurso'):
        if request.method == 'POST':
            action = request.POST['action']
            all = request.POST['select_across']

            if all == '0':
                periodocursos = request.POST.getlist('_selected_action')
                grupos = []
                for periodocurso_id in periodocursos:
                    periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
                    grupos.append(periodocurso)
            else:
                query = request.GET.get('q', '')
                query1 = request.GET.get('c', '')
                query2 = request.GET.get('p', '')
                query3 = request.GET.get('n', '')
                query4 = request.GET.get('e', '')

                results = PeriodoCurso.objects.all().order_by(
                        'Curso__Carrera__Carrera', 'Curso__Ciclo', 'Curso__Nombre')

                if query1:
                    results1 = results.filter(Curso__Carrera__id=query1)
                else:
                    results1 = results

                if query2:
                    results2 = results1.filter(Periodo__id=query2)
                else:
                    results2 = results1

                if query3:
                    results3 = results2.filter(Curso__Ciclo=query3)
                else:
                    results3 = results2

                if query:
                    qset = (
                            Q(Curso__Codigo__icontains=query) |
                            Q(Curso__Nombre__icontains=query) |
                            Q(Curso__Carrera__Carrera__icontains=query) |
                            Q(Grupo__icontains=query) |
                            Q(Docente__ApellidoPaterno__icontains=query) |
                            Q(Docente__ApellidoMaterno__icontains=query) |
                            Q(Docente__Nombres__icontains=query)
                    )
                    results4 = results3.filter(qset)
                else:
                    results4 = results3

                if query4:
                    results5 = results4.filter(Encuesta=query4)
                else:
                    results5 = results4

                grupos = []
                for periodocurso_id in results5:
                    periodocurso = PeriodoCurso.objects.get(
                        id=periodocurso_id.id)
                    grupos.append(periodocurso)

            if action == "print_actas_selected":
                if grupos != []:
                    return actas_periodocurso(grupos)
                else:
                    return HttpResponseRedirect('')
            elif action == "print_reporte_horarios_x_grupo_excel":
                if grupos != []:
                    return reporte_horarios_x_grupo_excel(request, grupos)
                else:
                    return HttpResponseRedirect('')
            elif action == "print_matriculados_grupo_excel":
                if grupos != []:
                    return matriculados_grupo_excel(request, grupos)
                else:
                    return HttpResponseRedirect('')
            elif action == "print_notas_grupo_excel":
                if grupos != []:
                    return notas_grupo_excel(request, grupos)
                else:
                    return HttpResponseRedirect('')
            elif action == "print_resultado_encuesta_excel":
                if grupos != []:
                    return resultado_encuesta_excel(request, grupos)
                else:
                    return HttpResponseRedirect('')
            elif action == "enabled_formulas":
                if grupos != []:
                    return enabled_formulas(grupos, True)
                else:
                    return HttpResponseRedirect('')
            elif action == "disabled_formulas":
                if grupos != []:
                    return enabled_formulas(grupos, False)
                else:
                    return HttpResponseRedirect('')
            elif action == "enabled_notas":
                if grupos != []:
                    return enabled_notas(grupos, True)
                else:
                    return HttpResponseRedirect('')
            elif action == "disabled_notas":
                if grupos != []:
                    return enabled_notas(grupos, False)
                else:
                    return HttpResponseRedirect('')
            elif action == "enabled_aplazados":
                if grupos != []:
                    return enabled_aplazados(grupos, True)
                else:
                    return HttpResponseRedirect('')
            elif action == "disabled_aplazados":
                if grupos != []:
                    return enabled_aplazados(grupos, False)
                else:
                    return HttpResponseRedirect('')
            elif action == "enabled_asistencias":
                if grupos != []:
                    return enabled_asistencias(grupos, True)
                else:
                    return HttpResponseRedirect('')
            elif action == "exportar_encuesta":
                return HttpResponseRedirect('../exportarencuesta/')

            elif action == "disabled_asistencias":
                if grupos != []:
                    return enabled_asistencias(grupos, False)
                else:
                    return HttpResponseRedirect('')
            else:
                return HttpResponseRedirect('')
        else:
            query = request.GET.get('q', '')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('n', '')
            query4 = request.GET.get('e', '')

            carreras = Carrera.objects.all()
            periodos = Periodo.objects.all()

            ciclos = []
            for i in range(1, 11):
                ciclo = str(i).rjust(2).replace(" ", "0")
                ciclos.append(ciclo)

            results = PeriodoCurso.objects.all().order_by(
                        'Curso__Carrera__Carrera', 'Curso__Ciclo', 'Curso__Nombre')

            if query1:
                results1 = results.filter(Curso__Carrera__id=query1)
            else:
                results1 = results

            if query2:
                results2 = results1.filter(Periodo__id=query2)
            else:
                results2 = results1

            if query3:
                results3 = results2.filter(Curso__Ciclo=query3)
            else:
                results3 = results2

            if query:
                qset = (
                        Q(Curso__Codigo__icontains=query) |
                        Q(Curso__Nombre__icontains=query) |
                        Q(Curso__Carrera__Carrera__icontains=query) |
                        Q(Grupo__icontains=query) |
                        Q(Docente__ApellidoPaterno__icontains=query) |
                        Q(Docente__ApellidoMaterno__icontains=query) |
                        Q(Docente__Nombres__icontains=query)
                )
                results4 = results3.filter(qset)
            else:
                results4 = results3

            if query4:
                results5 = results4.filter(Encuesta=query4)
            else:
                results5 = results4

            n_grupos = results5.count()
            paginator = Paginator(results5, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of
            # results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Cursos/Lectura/ver_periodocurso.html",
                                      {"results": results, "paginator": paginator, "query": query, "query1": query1,
                                       "query2": query2, "query3": query3, "query4": query4, "carreras": carreras,
                                       "periodos": periodos,
                                       "ciclos": ciclos, "n_grupos": n_grupos, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def ver_materialcurso(request):
    periodocursos = []
    if request.user.is_authenticated() and request.user.has_perm('Cursos.read_materialcurso'):
        if request.method == 'POST':
            action = request.POST['action']
            all = request.POST['select_across']

            if all == '0':
                periodocursos = request.POST.getlist('_selected_action')
                grupos = []
                for periodocurso_id in periodocursos:
                    periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
                    grupos.append(periodocurso)
            else:
                query = request.GET.get('q', '')
                query1 = request.GET.get('c', '')
                query2 = request.GET.get('p', '')
                query3 = request.GET.get('n', '')

                results = PeriodoCurso.objects.all()

                if query1:
                    results1 = results.filter(Curso__Carrera__id=query1)
                else:
                    results1 = results

                if query2:
                    results2 = results1.filter(Periodo__id=query2)
                else:
                    results2 = results1

                if query3:
                    results3 = results2.filter(Curso__Ciclo=query3)
                else:
                    results3 = results2

                if query:
                    qset = (
                            Q(Curso__Codigo__icontains=query) |
                            Q(Curso__Nombre__icontains=query) |
                            Q(Curso__Carrera__Carrera__icontains=query) |
                            Q(Grupo__icontains=query) |
                            Q(Docente__ApellidoPaterno__icontains=query) |
                            Q(Docente__ApellidoMaterno__icontains=query) |
                            Q(Docente__Nombres__icontains=query)
                    )
                    results4 = results3.filter(qset).order_by(
                        'Curso__Nombre', 'Curso__Carrera__Carrera')
                else:
                    results4 = results3.order_by(
                        'Curso__Nombre', 'Curso__Carrera__Carrera')

                grupos = []
                for periodocurso_id in results4:
                    periodocurso = PeriodoCurso.objects.get(
                        id=periodocurso_id.id)
                    grupos.append(periodocurso)

            return HttpResponseRedirect('')
        else:
            query = request.GET.get('q', '')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('n', '')

            carreras = Carrera.objects.all()
            periodos = Periodo.objects.all()

            ciclos = []
            for i in range(1, 11):
                ciclo = str(i).rjust(2).replace(" ", "0")
                ciclos.append(ciclo)

            results = PeriodoCurso.objects.all()

            if query1:
                results1 = results.filter(Curso__Carrera__id=query1)
            else:
                results1 = results

            if query2:
                results2 = results1.filter(Periodo__id=query2)
            else:
                results2 = results1

            if query3:
                results3 = results2.filter(Curso__Ciclo=query3)
            else:
                results3 = results2

            if query:
                qset = (
                        Q(Curso__Codigo__icontains=query) |
                        Q(Curso__Nombre__icontains=query) |
                        Q(Curso__Carrera__Carrera__icontains=query) |
                        Q(Grupo__icontains=query) |
                        Q(Docente__ApellidoPaterno__icontains=query) |
                        Q(Docente__ApellidoMaterno__icontains=query) |
                        Q(Docente__Nombres__icontains=query)
                )
                results4 = results3.filter(qset).order_by(
                    'Curso__Nombre', 'Curso__Carrera__Carrera')
            else:
                results4 = results3.order_by(
                    'Curso__Nombre', 'Curso__Carrera__Carrera')

            n_grupos = results4.count()
            paginator = Paginator(results4, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of
            # results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Cursos/MaterialCurso/ver_materialcurso.html",
                                      {"results": results, "paginator": paginator, "query": query, "query1": query1,
                                       "query2": query2, "query3": query3, "carreras": carreras, "periodos": periodos,
                                       "ciclos": ciclos, "n_grupos": n_grupos, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def ver_docentes_periodocurso(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        id_periodo = request.GET.get('p', '')
        try:
            periodo = Periodo.objects.get(id=id_periodo)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        carreras = Carrera.objects.filter(Estado=True)
        docentes = Docente.objects.filter(Estado=True).order_by(
            'ApellidoPaterno', 'ApellidoMaterno', 'Nombres')
        ciclos = []
        for i in range(1, 11):
            ciclo = str(i).rjust(2).replace(" ", "0")
            ciclos.append(ciclo)

        query = request.GET.get('q', '')
        query1 = request.GET.get('c', '')
        query2 = request.GET.get('n', '')

        results = PeriodoCurso.objects.filter(Periodo=periodo)

        if query1:
            results = results.filter(Curso__Carrera__id=query1)

        if query2:
            results = results.filter(Curso__Ciclo=query2)

        if query:
            qset = (
                    Q(Docente__Nombres__icontains=query) |
                    Q(Docente__ApellidoPaterno__icontains=query) |
                    Q(Docente__ApellidoMaterno__icontains=query) |
                    Q(Curso__Nombre__icontains=query)
            )
            results = results.filter(qset).order_by(
                'Curso__Carrera__Carrera', 'Curso__Ciclo', 'Curso__Nombre')
        else:
            results = results.order_by(
                'Curso__Carrera__Carrera', 'Curso__Ciclo', 'Curso__Nombre')

        n_cursos = results.count()

        return render_to_response("Cursos/Lectura/ver_docentes_periodocurso.html",
                                  {"results": results, "query": query, "query1": query1, "query2": query2,
                                   "carreras": carreras, "ciclos": ciclos, "periodo": periodo, "n_cursos": n_cursos,
                                   "docentes": docentes, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_exempt
def grabar_docente(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.change_horario'):
        consulta = str(request.POST['consulta'])
        id_periodocurso = consulta.split('-')[0]
        id_docente = consulta.split('-')[1]
        id_docente_antiguo = consulta.split('-')[2]
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            return HttpResponse('0|No existe el curso, verifique por favor.')

        try:
            docente = Docente.objects.get(id=id_docente)
        except Docente.DoesNotExist:
            return HttpResponse('0|No existe el docente a asignar, verifique por favor.')

        periodocurso.Docente = docente
        periodocurso.save()
        return HttpResponse('1|%s %s %s|%s|%s|%s' % (
            docente.ApellidoPaterno, docente.ApellidoMaterno, docente.Nombres, periodocurso.id, docente.id,
            id_docente_antiguo))
    else:
        return HttpResponse('0|No tiene permisos para procesar esta solicitud, verifique por favor.')


def descargar_docentes_periodocurso(request):
    if request.user.is_authenticated() and request.user.has_perm('Horarios.read_horario'):
        import xlwt

        wb = xlwt.Workbook()
        ws = wb.add_sheet('Hoja1')

        id_periodo = request.GET.get('p', '')
        try:
            periodo = Periodo.objects.get(id=id_periodo)
        except:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        results = PeriodoCurso.objects.filter(Periodo=periodo).order_by(
            'Curso__Carrera__Carrera', 'Curso__Ciclo', 'Curso__Nombre')

        # Cols
        ws.write(0, 0, "Grupo")
        ws.write(0, 1, "Codigo")
        ws.write(0, 2, "Curso")
        ws.write(0, 3, "Ciclo")
        ws.write(0, 4, "Carrera")
        ws.write(0, 5, "Apellidos Docente")
        ws.write(0, 6, "Nombres Docente")
        ws.write(0, 7, "Codigo Docente")
        ws.write(0, 8, "Email Docente")
        ws.write(0, 9, "Silabo")

        # Fil
        i = 0
        for cur in results:
            i = i + 1
            ws.write(i, 0, cur.Grupo)
            ws.write(i, 1, cur.Curso.Codigo)
            ws.write(i, 2, cur.Curso.Nombre)
            ws.write(i, 3, cur.Curso.Ciclo)
            ws.write(i, 4, cur.Curso.Carrera.Carrera)
            ws.write(i, 5, '%s %s' %
                     (cur.Docente.ApellidoPaterno, cur.Docente.ApellidoMaterno))
            ws.write(i, 6, '%s' % cur.Docente.Nombres)
            ws.write(i, 7, '%s' % cur.Docente.username)
            ws.write(i, 8, '%s' % cur.Docente.Email)
            if cur.Silabo != '':
                ws.write(i, 9, 'Si')
            else:
                ws.write(i, 9, 'No')
        fname = 'reporte_docentes_periodocurso.xls'
        response = HttpResponse(content_type="application/ms-excel")
        response['Content-Disposition'] = 'attachment; filename=%s' % fname

        wb.save(response)

        return response
    else:
        return HttpResponseRedirect('../../../')


def descargar_matriculados_excel(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if not hor.PeriodoCurso in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        matriculados = []
        i = 1
        for per in periodo_cursos:
            mats = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True).order_by(
                'MatriculaCiclo__Alumno__Carrera__Carrera',
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')
            for mat in mats:
                apellidos = mat.MatriculaCiclo.Alumno.ApellidoPaterno + \
                            ' ' + mat.MatriculaCiclo.Alumno.ApellidoMaterno
                nombres = mat.MatriculaCiclo.Alumno.Nombres
                if mat.Estado == True:
                    condicion = 'Matriculado'
                else:
                    condicion = 'Retirado'

                if mat.Convalidado == True:
                    convalidado = 'Si'
                else:
                    convalidado = 'No'
                matriculados.append([mat.MatriculaCiclo.Alumno.Codigo, apellidos, nombres,
                                     mat.MatriculaCiclo.Alumno.Carrera.Carrera, mat.PeriodoCurso.Grupo, condicion,
                                     convalidado])
                i += 1
        headers = ['Código', 'Apellidos', 'Nombres',
                   'Carrera', 'GH', 'Condición', 'Convalidado']
        titulo = 'LISTADO DE MATRICULADOS EN : %s' % periodocurso.Curso
        return response_excel(titulo=titulo, heads=headers, registros=matriculados, nombre_archivo='lista_matriculados')

    else:
        return HttpResponseRedirect('../../../')


def descargar_avance_silabico_excel(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        avance = []
        for horario in horarios:
            asistencias = AsistenciaDocente.objects.filter(Horario=horario)
            for a in asistencias:
                avance.append(a)
        listado_asistencias = sorted(
            avance, key=lambda instance: instance.FechaClase)

        avance_silabico = []
        for lista in listado_asistencias:
            dia = lista.Horario.Dia
            inicio = lista.Horario.HoraInicio.strftime('%H:%M')
            fin = lista.Horario.HoraFin.strftime('%H:%M')
            aula = lista.Horario.Seccion
            tema = lista.TemaClase
            fecha = lista.FechaClase.strftime('%d-%m-%Y')
            fecha_registro = lista.Fecha.strftime('%d-%m-%Y')
            avance_silabico.append(
                [dia, inicio, fin, aula, tema, fecha, fecha_registro])
        headers = ['Día', 'Inicio', 'Fin', 'Aula',
                   'Tema Clase', 'Fecha Clase', 'Fecha Registro']
        titulo = 'AVANCE SILABICO DEL CURSO : %s' % periodocurso.Curso
        return response_excel(titulo=titulo, heads=headers, registros=avance_silabico, nombre_archivo='avance_silabico')
    else:
        return HttpResponseRedirect('../../../')


def notas_grupo_excel(request, grupos):
    output_name = "notas_matriculados"
    encoding = 'utf8'
    import StringIO
    output = StringIO.StringIO()
    try:
        import xlwt
        from xlwt import Workbook, XFStyle, Borders, Pattern, Font, easyxf
    except ImportError:
        # xlwt doesn't exist; fall back to csv
        pass

    book = xlwt.Workbook(encoding=encoding)
    hojan = 0
    hojan = hojan + 1
    sheet1 = book.add_sheet('Hoja%s' % hojan)

    contadorfinal = 0
    for dat in grupos:
        contador = contadorfinal
        # estilos de celda titulo
        fnt_titulo = Font()
        fnt_titulo.name = 'Arial'
        fnt_titulo.bold = True

        style_titulo = XFStyle()
        style_titulo.font = fnt_titulo

        # estilos de celda etiqueta resumen
        fnt_etiqueta_resumen = Font()
        fnt_etiqueta_resumen.name = 'Arial'
        fnt_etiqueta_resumen.bold = True

        style_etiqueta_resumen = XFStyle()
        style_etiqueta_resumen.font = fnt_etiqueta_resumen

        # estilos de celda datos resumen
        fnt_dato_resumen = Font()
        fnt_dato_resumen.name = 'Arial'

        style_dato_resumen = XFStyle()
        style_dato_resumen.font = fnt_dato_resumen

        # estilos de celda heads
        fnt_heads = Font()
        fnt_heads.name = 'Arial'
        fnt_heads.bold = True
        borders_heads = Borders()
        borders_heads.left = Borders.THIN
        borders_heads.right = Borders.THIN
        borders_heads.top = Borders.THIN
        borders_heads.bottom = Borders.THIN
        pattern_heads = Pattern()
        pattern_heads.pattern = Pattern.SOLID_PATTERN
        pattern_heads.pattern_fore_colour = 0x9ACD32

        style_heads = XFStyle()
        style_heads.font = fnt_heads
        style_heads.borders = borders_heads
        style_heads.pattern = pattern_heads

        # estilos de celda registros

        # registros del estudiante
        fnt_registros = Font()
        fnt_registros.name = 'Arial'

        # notas menores a 11
        fnt_menor_trece = Font()
        fnt_menor_trece.name = 'Arial'
        fnt_menor_trece.colour_index = 0xa

        # notas mayores o iguales a 11
        fnt_mayor_trece = Font()
        fnt_mayor_trece.name = 'Arial'
        fnt_mayor_trece.colour_index = 0xc

        # fondo notas 0
        pattern_notas0 = Pattern()
        pattern_notas0.pattern = Pattern.SOLID_PATTERN
        pattern_notas0.pattern_fore_colour = 0x31

        # fondo notas 1
        pattern_notas1 = Pattern()
        pattern_notas1.pattern = Pattern.SOLID_PATTERN
        pattern_notas1.pattern_fore_colour = 0x1A

        # fondo notas 2
        pattern_notas2 = Pattern()
        pattern_notas2.pattern = Pattern.SOLID_PATTERN
        pattern_notas2.pattern_fore_colour = 0x16

        borders_registros = Borders()
        borders_registros.left = Borders.THIN
        borders_registros.right = Borders.THIN
        borders_registros.top = Borders.THIN
        borders_registros.bottom = Borders.THIN

        style_registros = XFStyle()
        style_registros.font = fnt_registros
        style_registros.borders = borders_registros

        # style notas 0 >= 11
        style_notas0_mayor = XFStyle()
        style_notas0_mayor.font = fnt_mayor_trece
        style_notas0_mayor.borders = borders_registros
        style_notas0_mayor.pattern = pattern_notas0

        # style notas 0 < 11
        style_notas0_menor = XFStyle()
        style_notas0_menor.font = fnt_menor_trece
        style_notas0_menor.borders = borders_registros
        style_notas0_menor.pattern = pattern_notas0

        # style notas 1 >= 11
        style_notas1_mayor = XFStyle()
        style_notas1_mayor.font = fnt_mayor_trece
        style_notas1_mayor.borders = borders_registros
        style_notas1_mayor.pattern = pattern_notas1

        # style notas 1 < 11
        style_notas1_menor = XFStyle()
        style_notas1_menor.font = fnt_menor_trece
        style_notas1_menor.borders = borders_registros
        style_notas1_menor.pattern = pattern_notas1

        # style notas 2 >= 11
        style_notas2_mayor = XFStyle()
        style_notas2_mayor.font = fnt_mayor_trece
        style_notas2_mayor.borders = borders_registros
        style_notas2_mayor.pattern = pattern_notas2

        # style notas 2 < 11
        style_notas2_menor = XFStyle()
        style_notas2_menor.font = fnt_menor_trece
        style_notas2_menor.borders = borders_registros
        style_notas2_menor.pattern = pattern_notas2

        # style notas 3 >= 11
        style_notas3_mayor = XFStyle()
        style_notas3_mayor.font = fnt_mayor_trece
        style_notas3_mayor.borders = borders_registros

        # style notas 3 < 11
        style_notas3_menor = XFStyle()
        style_notas3_menor.font = fnt_menor_trece
        style_notas3_menor.borders = borders_registros

        try:
            # periodocurso = PeriodoCurso.objects.get(id = idperiodocurso, Docente__id = request.user.id)
            periodocurso = PeriodoCurso.objects.get(id=dat.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a class='btn btn-primary btn-large' href='../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if not hor.PeriodoCurso in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        registros = []
        for per in periodo_cursos:
            mats = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False,
                                                  FechaRetiro=None).order_by(
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')
            for mat in mats:
                notas1 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, Nivel='1').order_by('Orden')

                nots1 = []
                nots2 = []
                nots3 = []

                for nota1 in notas1:
                    nots_2 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota1, Nivel='2').order_by(
                        'Orden')
                    for nota2 in nots_2:
                        nots_3 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota2,
                                                     Nivel='3').order_by('Orden')
                        for nota3 in nots_3:
                            valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                            if valor3.count() != 0:
                                valor3 = valor3[0].Valor
                                nots3.append([nota3, int(str(valor3).split('.')[0])])
                            else:
                                valor3 = mat.PromedioParcial(nota3)
                                nots3.append([nota3, valor3])
                        valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                        if valor2.count() != 0:
                            valor2 = valor2[0].Valor
                            nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                        else:
                            valor2 = mat.PromedioParcial(nota2)
                            nots2.append([nota2, nots3, valor2])
                        nots3 = []
                    valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
                    if valor1.count() != 0:
                        valor1 = valor1[0].Valor
                        nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
                    else:
                        valor1 = mat.PromedioParcial(nota1)
                        nots1.append([nota1, nots2, valor1])
                    nots2 = []
                registros.append([mat, nots1])
                nots1 = []

        sheet1.write(contador + 2, 0, 'CURSO:', style_titulo)
        sheet1.write(contador + 2, 1, periodocurso.Curso.Nombre, style_titulo)
        sheet1.write(contador + 3, 0, 'CICLO:', style_titulo)
        sheet1.write(contador + 3, 1, periodocurso.Curso.Ciclo, style_titulo)
        sheet1.write(contador + 4, 0, 'DOCENTE:', style_titulo)
        sheet1.write(contador + 4, 1, periodocurso.Docente.__unicode__(), style_titulo)

        # escribir el titulo
        sheet1.write(contador + 6, 0, 'CALIFICACIONES', style_titulo)

        sheet1.write(contador + 8, 0, 'Nro', style_heads)
        sheet1.write(contador + 8, 1, 'Codigo', style_heads)
        sheet1.write(contador + 8, 2, 'Estudiante', style_heads)
        sheet1.write(contador + 8, 3, 'Carrera', style_heads)

        k = 4

        for reg1 in registros[0][1]:
            if reg1:
                for reg2 in reg1[1]:
                    if reg2:
                        for reg3 in reg2[1]:
                            if reg3:
                                sheet1.write(contador + 8, k, reg3[0].Identificador, style_heads)
                                k += 1
                        sheet1.write(contador + 8, k, reg2[0].Identificador, style_heads)
                        k += 1
                sheet1.write(contador + 8, k, reg1[0].Identificador, style_heads)
                k += 1

        sheet1.write(contador + 8, k, 'PF', style_heads)

        j = 4
        i = 9

        for reg in registros:
            aprobado = int(str(reg[0].PeriodoCurso.Aprobacion).split('.')[0])
            sheet1.write(contador + i, 0, i - 8, style_registros)
            sheet1.write(contador + i, 1, reg[0].MatriculaCiclo.Alumno.Codigo, style_registros)
            sheet1.write(contador + i, 2, reg[0].MatriculaCiclo.Alumno.__unicode__(), style_registros)
            sheet1.write(contador + i, 3, reg[0].MatriculaCiclo.Alumno.Carrera.Carrera, style_registros)
            for reg1 in reg[1]:
                if reg1:
                    for reg2 in reg1[1]:
                        if reg2:
                            for reg3 in reg2[1]:
                                if reg3:
                                    if reg3[2] >= aprobado:
                                        sheet1.write(contador + i, j, reg3[2], style_notas3_mayor)
                                    else:
                                        sheet1.write(contador + i, j, reg3[2], style_notas3_menor)
                                    j += 1
                            if reg2[2] >= aprobado:
                                sheet1.write(contador + i, j, reg2[2], style_notas2_mayor)
                            else:
                                sheet1.write(contador + i, j, reg2[2], style_notas2_menor)
                            j += 1
                    if reg1[2] >= aprobado:
                        sheet1.write(contador + i, j, reg1[2], style_notas1_mayor)
                    else:
                        sheet1.write(contador + i, j, reg1[2], style_notas1_menor)
                    j += 1
            if reg[0].Promedio() >= aprobado:
                sheet1.write(contador + i, j, reg[0].Promedio(), style_notas0_mayor)
            else:
                sheet1.write(contador + i, j, reg[0].Promedio(), style_notas0_menor)
            i += 1
            j = 4

        # l = i

        # notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        # notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        #
        # for nota in notas:
        #     sheet1.write(contador + i + 5, 1, '%s:' % nota.Identificador)
        #     sheet1.write(contador + i + 5, 2, '%s' % nota.Nota)
        #     i += 1

        contadorfinal = contador + i

        # for nota in notas1:
        #     subnotas = Nota.objects.filter(NotaPadre=nota)
        #     k = 1
        #     formula = ''
        #     suma = Decimal('0.00')
        #     for subnota in subnotas:
        #         if k == 1:
        #             formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
        #         else:
        #             formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
        #         k += 1
        #         suma += Decimal(nota.Peso)
        #     formula += ')/%s' % str(suma)
        #     sheet1.write(contador + l + 5, 4, formula)
        #     l += 1

        print contadorfinal
    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=notas_excel.xls'
    return response
