# -*- coding: utf-8 -*-
from django.conf.urls import url

from Cursos.read_only import ver_periodocurso, ver_docentes_periodocurso, grabar_docente, \
    descargar_docentes_periodocurso, descargar_matriculados_excel, descargar_avance_silabico_excel, ver_materialcurso
from Cursos.views import exportar_curso, crear_grupo_horario
from Cursos.views import index_admin_curso, index_periodocurso, index_materialcurso
from Docentes.views import index_cursos_docente
from Horarios.views import horarios_notas_curso, agregar_horario, editar_horario, app_horarios, \
    buscar_horario_aula, buscar_periodocurso, traer_periodocurso, descargar_horarios2, guardar_horario, \
    modificar_horario, eliminar_horario
from Horarios.views import index_horario
from Notas.subgrid_notas import ver_subgrid_notas, ver_subgrid_notas1, ver_subgrid_notas2, ver_subgrid_notas3, \
    master_subgrid_notas, master_subgrid_notas1, master_subgrid_notas2, master_subgrid_notas3

app_name = 'Cursos'

urlpatterns = [
    url(r'^Cursos/curso/exportarcurso/$', exportar_curso),
    url(r'^Cursos/curso/creargrupohorario/$', crear_grupo_horario),
    url(r'^Cursos/periodocurso/$', ver_periodocurso),
    url(r'^Cursos/periodocurso/ver/(\d+)/$', horarios_notas_curso),
    url(r'^Cursos/periodocurso/ver/(\d+)/master_subgrid_notas/$', master_subgrid_notas),
    url(r'^Cursos/periodocurso/ver/(\d+)/master_subgrid_notas1/$', master_subgrid_notas1),
    url(r'^Cursos/periodocurso/ver/(\d+)/master_subgrid_notas2/$', master_subgrid_notas2),
    url(r'^Cursos/periodocurso/ver/(\d+)/master_subgrid_notas3/$', master_subgrid_notas3),
    url(r'^Cursos/periodocurso/subgrid/vernotas/$', ver_subgrid_notas),
    url(r'^Cursos/periodocurso/subgrid/vernotas1/$', ver_subgrid_notas1),
    url(r'^Cursos/periodocurso/subgrid/vernotas2/$', ver_subgrid_notas2),
    url(r'^Cursos/periodocurso/subgrid/vernotas3/$', ver_subgrid_notas3),
    url(r'^Cursos/periodocurso/agregarhorario/(\d+)/$', agregar_horario),
    url(r'^Cursos/periodocurso/editarhorario/(\d+)/$', editar_horario),
    url(r'^Cursos/periodocurso/docentes/$', ver_docentes_periodocurso),
    url(r'^Cursos/periodocurso/docentes/grabar_docente/$', grabar_docente),
    url(r'^Cursos/periodocurso/docentes/descargar/$', descargar_docentes_periodocurso),
    url(r'^Cursos/periodocurso/(\d+)/matriculados_excel/$', descargar_matriculados_excel),
    url(r'^Cursos/periodocurso/(\d+)/avance_silabico/$', descargar_avance_silabico_excel),
    url(r'^Cursos/periodocurso/horarios/$', app_horarios),
    url(r'^Cursos/periodocurso/horarios/buscar/$', buscar_horario_aula),
    url(r'^Cursos/periodocurso/horarios/buscar_curso/$', buscar_periodocurso),
    url(r'^Cursos/periodocurso/horarios/traer_periodocurso/$', traer_periodocurso),
    url(r'^Cursos/periodocurso/horarios/descargar/$', descargar_horarios2),
    url(r'^Cursos/periodocurso/horarios/guardar_horario/$', guardar_horario),
    url(r'^Cursos/periodocurso/horarios/editar_horario/$', modificar_horario),
    url(r'^Cursos/periodocurso/horarios/eliminar_horario/$', eliminar_horario),
    url(r'^Cursos/curso/elegirperiodo/$', index_admin_curso),
    url(r'^Cursos/periodocurso/elegirperiodo/$', index_periodocurso),
    url(r'^Cursos/materialcurso/elegirperiodo/$', index_materialcurso),
    url(r'^Cursos/materialcurso/$', ver_materialcurso),
    url(r'^Cursos/periodocurso/docentes/elegirperiodo/$', index_cursos_docente),
    url(r'^Cursos/periodocurso/horarios/elegirperiodo/$', index_horario),
]
