# Create your views here.
# -*- coding: utf-8 -*-

from decimal import Decimal

from django import forms
from django.core import serializers
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from Cursos.admin import PeriodoForm, DocenteForm
from Cursos.models import Curso, PeriodoCurso
from Notas.models import Nota
from Periodos.models import Periodo


class IndexCursoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


@csrf_protect
def index_admin_curso(request):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.change_curso') or request.user.has_perm(
            'Cursos.add_curso'):
        if request.method == 'POST':
            form = IndexCursoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?Periodo__id__exact=" + str(periodo.id))
        else:
            form = IndexCursoForm()
        return render_to_response('Cursos/Curso/index_curso.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


class IndexPeriodoCursoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


@csrf_protect
def index_periodocurso(request):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.change_periodocurso') or request.user.has_perm(
            'Cursos.add_periodocurso') or request.user.has_perm('Cursos.read_periodocurso'):
        if request.method == 'POST':
            form = IndexPeriodoCursoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?p=" + str(periodo.id))
        else:
            form = IndexPeriodoCursoForm()
        return render_to_response('Cursos/PeriodoCurso/index_periodocurso.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def index_materialcurso(request):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.read_materialcurso'):
        if request.method == 'POST':
            form = IndexPeriodoCursoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?p=" + str(periodo.id))
        else:
            form = IndexPeriodoCursoForm()
        return render_to_response('Cursos/MaterialCurso/index_materialcurso.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def obtener_cursos(request, carrera_id, ciclo_id, periodo_id):
    if carrera_id != "" and ciclo_id != "":
        consulta = Curso.objects.filter(Carrera__id=carrera_id, Ciclo=ciclo_id, Periodo__id=periodo_id).order_by(
            'Nombre')
        if consulta:
            return HttpResponse(
                serializers.serialize('json', consulta, fields=('pk', 'Nombre'), extras=('__unicode__',)))


def prerequisito(request, carrera_id):
    if carrera_id != "":
        return HttpResponse(
            serializers.serialize('json', Curso.objects.filter(Carrera__id=carrera_id).order_by('Nombre'),
                                  fields=('pk', 'Nombre'), extras=('__unicode__',)))


def prerequisito2(request, carrera_id, periodo_id):
    if carrera_id != "":
        return HttpResponse(
            serializers.serialize('json', Curso.objects.filter(Carrera__id=carrera_id, Periodo__id=periodo_id).order_by(
                'Ciclo', 'Nombre'),
                                  fields=('pk', 'Nombre'), extras=('Nombre', 'Ciclo', 'Codigo')))


def exportar_curso(request):
    if request.method == 'POST':
        cursos_exportar = request.POST.getlist('cursos')
        form = PeriodoForm(request.POST)
        if form.is_valid():
            # obtengo el nuevo periodo a aperturar el plan de estudios
            periodo = form.cleaned_data['Periodo']

            # recorro todo los cursos que deseo migrar al nuevo periodo
            for curso1 in cursos_exportar:
                # obtengo el curso a migrar
                curso = Curso.objects.get(id=curso1)
                # verifica si el curso existe en el periodo a aperturar
                verificar = Curso.objects.filter(Codigo=curso.Codigo, Nombre=curso.Nombre, Ciclo=curso.Ciclo,
                                                 Periodo=periodo, Carrera=curso.Carrera)
                if verificar.count() == 0:
                    if curso.Prerequisito is not None:
                        # obtiene el prerequisito del curso si no tiene coloca None
                        prerequisito = Curso.objects.filter(Periodo=curso.Periodo, Codigo=curso.Prerequisito.Codigo,
                                                            Nombre=curso.Prerequisito.Nombre,
                                                            Ciclo=curso.Prerequisito.Ciclo,
                                                            Carrera=curso.Prerequisito.Carrera)
                        if prerequisito.count() != 0:
                            prereq = prerequisito[0]
                        else:
                            prereq = None
                    else:
                        prereq = None
                    # creacion del curso en el nuevo periodo
                    nuevo_curso = Curso(Codigo=curso.Codigo, Nombre=curso.Nombre, Creditos=curso.Creditos,
                                        Ciclo=curso.Ciclo, Periodo=periodo, Prerequisito=prereq, Carrera=curso.Carrera,
                                        Equivalencia=curso.Equivalencia, Codigo1=curso.Codigo1,
                                        Extracurricular=curso.Extracurricular, Estado=curso.Estado)
                    nuevo_curso.save()

                    # asignacion de equivalencias al nuevo curso creado, se toman las equivalencias
                    # del curso base (ciclo anterior)
                    for e in curso.Equivalencias.all():
                        nuevo_curso.Equivalencias.add(e.id)
                    # como es nuevo el curso se agrega como equivalencia el curso base
                    nuevo_curso.Equivalencias.add(curso.id)

                    # asignacion de prerequisitos al nuevo curso creado, se toman los prerequisitos
                    # del curso base (ciclo anterior)
                    for p in curso.Prerequisitos.all():
                        nuevo_curso.Prerequisitos.add(p.id)

                    # como es nuevo el curso se agrega como prerequisito el curso base en caso
                    # tenga prerequisito
                    if prereq is not None:
                        nuevo_curso.Prerequisitos.add(prereq.id)

            # corrección de los prerequisitos (porque agreega los del ciclo anterior)
            cursos_periodo = Curso.objects.filter(Periodo=periodo)
            for curso in cursos_periodo:
                prereq = curso.Prerequisito
                if prereq:
                    prereq_cod = prereq.Codigo
                    try:
                        true_prereq = Curso.objects.get(Periodo=periodo, Codigo=prereq_cod, Carrera=curso.Carrera)
                        curso.Prerequisito = true_prereq
                        curso.save()
                        curso.Prerequisitos.add(true_prereq.id)
                    except Curso.DoesNotExist:
                        pass
        return HttpResponseRedirect("../")
    else:
        return HttpResponseRedirect("../")


def crear_grupo_horario(request):
    if request.method == 'POST':
        cursos = request.POST.getlist('cursos')
        form = DocenteForm(request.POST)
        if form.is_valid():
            docente = form.cleaned_data['Docente']
            for curso_id in cursos:
                try:
                    curso = Curso.objects.get(id=curso_id)
                    verificar = PeriodoCurso.objects.filter(Periodo=curso.Periodo, Curso=curso)
                    if verificar.count() == 0:
                        dict = {'AT': '01A', 'CC': '01B', 'IA': '01C', 'IC': '01D', 'IS': '01E', 'AM': '01F'}
                        nuevo_grupo = PeriodoCurso(Periodo=curso.Periodo, Curso=curso, Grupo=dict[curso.Carrera.Codigo],
                                                   Docente=docente, Aprobacion=Decimal('11.00'))
                        nuevo_grupo.save()

                        crear_nota = Nota(PeriodoCurso=nuevo_grupo, Nota='Promedio Final', Identificador='PF',
                                          Peso=Decimal('1.00'), Orden='1', SubNotas=True)
                        crear_nota.save()

                        crear_primer_parcial = Nota(PeriodoCurso=nuevo_grupo, Nota='Promedio Parcial 1',
                                                    Identificador='PP1', Peso=Decimal('1.00'), Nivel='1', Orden='1',
                                                    NotaPadre=crear_nota)
                        crear_primer_parcial.save()
                        crear_segundo_parcial = Nota(PeriodoCurso=nuevo_grupo, Nota='Promedio Parcial 2',
                                                     Identificador='PP2', Peso=Decimal('1.00'), Nivel='1', Orden='2',
                                                     NotaPadre=crear_nota)
                        crear_segundo_parcial.save()
                except:
                    pass
            return HttpResponseRedirect("../")
    else:
        return HttpResponseRedirect("../")
