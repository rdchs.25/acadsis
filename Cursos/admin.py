# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect

from Carreras.models import Carrera
from Cursos.models import Curso
from Docentes.models import Docente
from Periodos.models import Periodo

csrf_protect_m = method_decorator(csrf_protect)


class CursoAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CursoAdminForm, self).__init__(*args, **kwargs)
        try:
            # Try and set the selected_cat field from instance if it exists
            if self.instance.id is not None:
                # self.fields['Equivalencias'].queryset = Curso.objects.filter(Nombre__icontains=self.instance.Nombre)
                self.fields['Equivalencias'].queryset = Curso.objects.all()
                self.fields['Prerequisitos'].queryset = Curso.objects.all()
                self.fields['selected_car'].initial = self.instance.Carrera.id
                self.fields['selected_per'].initial = self.instance.Periodo.id
                if self.instance.Prerequisito is not None:
                    self.fields['selected_pre'].initial = self.instance.Prerequisito.id
            else:
                self.fields['Equivalencias'].queryset = Curso.objects.none()
                self.fields['Prerequisitos'].queryset = Curso.objects.none()
        except ValueError:
            pass

    Carrera = forms.ModelChoiceField(queryset=Carrera.objects.all(), widget=forms.Select(), required=True)
    Prerequisito = forms.ModelChoiceField(queryset=Curso.objects.filter(Periodo=Periodo.objects.get(Activo=True)),
                                          widget=forms.Select(), required=False)
    selected_car = forms.CharField(widget=forms.HiddenInput, required=False, label="")
    selected_pre = forms.CharField(widget=forms.HiddenInput, required=False, label="")
    selected_per = forms.CharField(widget=forms.HiddenInput, required=False, label="")

    class Meta:
        model = Curso
        fields = '__all__'

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Cursos/frmcurso.js",
              )


class PeriodoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Periodo')


class DocenteForm(forms.Form):
    Docente = forms.ModelChoiceField(
        queryset=Docente.objects.all().order_by('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'), widget=forms.Select(),
        required=True, label='Docente')


class CursoAdmin(admin.ModelAdmin):
    form = CursoAdminForm
    actions = ['exportar_curso', 'crear_grupo_horario']
    filter_horizontal = ['Prerequisitos', 'Equivalencias']
    list_display = (
        'Codigo', 'Nombre', 'Creditos', 'Ciclo', 'Prerequisito', 'Carrera', 'Periodo', 'Equivalencia', 'Codigo1',
        'Creditos_equiv', 'Estado', 'Extracurricular', 'Proyecto_formativo', 'Moodleid')
    list_filter = ('Carrera', 'Ciclo', 'Periodo', 'Estado', 'Extracurricular')
    search_fields = ('Codigo', 'Nombre')
    fieldsets = (
        ('Datos del Curso', {
            'fields': (
                'Codigo', 'Nombre', 'Creditos', 'Ciclo', 'Carrera', 'Periodo', 'Prerequisito', 'Equivalencias',
                'Prerequisitos', 'Equivalencia', 'Codigo1', 'Creditos_equiv', 'Extracurricular', 'Proyecto_formativo',
                'Estado', 'selected_car', 'selected_pre', 'selected_per',),
        }),
    )

    @csrf_protect_m
    def exportar_curso(modeladmin, request, queryset):
        form = PeriodoForm()
        return render_to_response('Cursos/exportar_curso.html',
                                  {"form": form, "queryset": queryset, "user": request.user},
                                  context_instance=RequestContext(request))

    exportar_curso.short_description = "Exportar Cursos a otro Semestre"

    @csrf_protect_m
    def crear_grupo_horario(modeladmin, request, queryset):
        form = DocenteForm()
        return render_to_response('Cursos/crear_grupo_horario.html',
                                  {"form": form, "queryset": queryset, "user": request.user},
                                  context_instance=RequestContext(request))

    crear_grupo_horario.short_description = "Crear Grupo Horario"

    def queryset(self, request):
        """
        Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view.
        """
        id_periodo = request.GET.get('p', '')
        qs = self.model._default_manager.get_query_set()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or ('-Ciclo', 'Codigo')  # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        if id_periodo != '':
            return qs.filter(Periodo__id=id_periodo)
        else:
            return qs


admin.site.register(Curso, CursoAdmin)
