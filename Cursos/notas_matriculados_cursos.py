# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

from Matriculas.models import MatriculaCursos
from Cursos.models import Curso
from UDL import settings
from tempfile import TemporaryFile

from xlrd import open_workbook
from xlutils.copy import copy
from xlwt import XFStyle, Borders

# estilos de celda registros
borders_registros = Borders()
borders_registros.left = Borders.THIN
borders_registros.right = Borders.THIN
borders_registros.top = Borders.THIN
borders_registros.bottom = Borders.THIN

style_registros = XFStyle()
style_registros.borders = borders_registros

cursos = Curso.objects.filter(Periodo__id=8)
for curso in cursos:
    matriculados = MatriculaCursos.objects.filter(PeriodoCurso__Curso=curso).order_by(
        'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
        'MatriculaCiclo__Alumno__Nombres')
    codigo_curso = curso.Codigo
    nombre = curso.Nombre
    carr = curso.Carrera.Carrera
    cod = curso.Carrera.Codigo
    ciclo = curso.Ciclo
    if matriculados.count() != 0:
        docente = matriculados[0].PeriodoCurso.Docente.ApellidoPaterno + ' ' + matriculados[
            0].PeriodoCurso.Docente.ApellidoMaterno + ' ' + matriculados[0].PeriodoCurso.Docente.Nombres
    rb = open_workbook(settings.MEDIA_ROOT + u'archivos_excel/notas_2012/formato_notas.xls', formatting_info=True)
    wb = copy(rb)
    ws = wb.get_sheet(0)
    # ws1 = wb.get_sheet(1)

    ws.insert_bitmap(settings.MEDIA_ROOT + 'archivos_excel/LogoExcel.bmp', 0, 0)

    ws.write(2, 4, carr)
    ws.write(4, 0, "Docente: " + docente)
    ws.write(3, 4, ciclo)
    ws.write(4, 4, nombre)

    # if matriculados.count() > 40:
    #    ws1.insert_bitmap(settings.MEDIA_ROOT + 'archivos_excel/LogoExcel.bmp', 1, 0)
    #    ws1.write(3,1,carr, style_registros)
    #    ws1.write(4,1,docente, style_registros)
    #    ws1.write(3,4,ciclo, style_registros)
    #    ws1.write(4,4,nombre, style_registros)

    i = 7
    for m in matriculados[0:40]:
        codigo = m.MatriculaCiclo.Alumno.Codigo
        alumno = m.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + m.MatriculaCiclo.Alumno.ApellidoMaterno + ' ' + m.MatriculaCiclo.Alumno.Nombres
        carrera = m.MatriculaCiclo.Alumno.Carrera.Codigo
        ws.write(i, 1, alumno, style_registros)
        ws.write(i, 2, carrera, style_registros)
        i += 1

    # if matriculados.count() > 40:
    #    j = 8
    #    for m in matriculados[21:]:
    #        codigo = m.MatriculaCiclo.Alumno.Codigo
    #        alumno = m.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + m.MatriculaCiclo.Alumno.ApellidoMaterno + ' ' + m.MatriculaCiclo.Alumno.Nombres
    #        carrera = m.MatriculaCiclo.Alumno.Carrera.Codigo
    #        ws1.write(j,1,alumno, style_registros)
    #        ws1.write(j,2,carrera, style_registros)
    #        j+=1
    wb.save()
    wb.save()
