# -*- coding: utf-8 -*-
import datetime
# Codificacion para las tildes
import time

from django.conf import settings
from django.db import models

from Carreras.models import Carrera
from Docentes.models import Docente
from Periodos.models import Periodo
from Planestudio.models import Plan
from UDL.moodle_webservice import moodle_webservices

CICLO_CHOICES = (
    ('01', '1º'),
    ('02', '2º'),
    ('03', '3º'),
    ('04', '4º'),
    ('05', '5º'),
    ('06', '6º'),
    ('07', '7º'),
    ('08', '8º'),
    ('09', '9º'),
    ('10', '10º'),
)


class Curso(models.Model):
    Codigo = models.CharField(max_length=9)
    Nombre = models.CharField(max_length=60)
    Creditos = models.PositiveIntegerField()
    Ciclo = models.CharField("Ciclo", max_length=2, choices=CICLO_CHOICES)
    Periodo = models.ForeignKey(Periodo, verbose_name="Período")
    Prerequisito = models.ForeignKey('self', null=True, blank=True)
    Carrera = models.ForeignKey(Carrera)
    Equivalencia = models.CharField(max_length=60, null=True, blank=True)
    Codigo1 = models.CharField(max_length=7, null=True, blank=True)
    Creditos_equiv = models.PositiveIntegerField(null=True, blank=True)
    Extracurricular = models.BooleanField("Extra Curricular", default=False)
    Proyecto_formativo = models.BooleanField("Proyecto Formativo", default=False)
    Estado = models.BooleanField("Activada", default=True)
    Prerequisitos = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='inverso_prereq')
    Equivalencias = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='inverso_equiv')
    Plan = models.ForeignKey(Plan, default=None, blank=True, null=True, verbose_name="Plan de estudio")
    Moodleid = models.PositiveIntegerField("Moodle id", editable=False, null=True)

    def save(self, **kwargs):
        # nombrecategoria = self.nombrecurso()
        # if self.Periodo.Moodleid is not None:
        #     if self.Moodleid is None:
        #         datos_categoria = {
        #             'categories[0][name]': nombrecategoria,
        #             'categories[0][parent]': self.Periodo.Moodleid,
        #         }
        #         rest_datos_cateogria = moodle_webservices("core_course_create_categories", datos_categoria)
        #         self.Moodleid = rest_datos_cateogria[0]['id']
        #     else:
        #         datos_categoria = {
        #             'categories[0][name]': nombrecategoria,
        #             'categories[0][id]': self.Moodleid,
        #         }
        #         moodle_webservices("core_course_update_categories", datos_categoria)

        return super(Curso, self).save()

    def __unicode__(self):
        return u'%s, %s, %s, %s' % (self.Nombre, self.Carrera.Codigo, self.Periodo, self.Ciclo)

    def nombrecurso(self):
        return (self.Ciclo + " - " + self.Nombre + ", " + self.Carrera.Codigo).encode('utf-8')

    def nuevounicode(self):
        return u'%s - %s, %s' % (self.Ciclo, self.Carrera.Codigo, self.Nombre)

    class Meta:
        verbose_name = "Curso de Carrera"
        verbose_name_plural = "Cursos de Carrera"


def obtenerpath_periodocurso(instance, filename):
    ext = filename.split('.')[-1]
    return u'silabos/%s-%s/%s/%s/%s_%s.%s' % (
        instance.Periodo.Anio, instance.Periodo.Semestre, instance.Curso.Carrera.Codigo, instance.Curso.Ciclo,
        instance.Curso.Codigo, instance.Docente.username, ext)


class PeriodoCurso(models.Model):
    Periodo = models.ForeignKey(Periodo)
    Curso = models.ForeignKey(Curso)
    Grupo = models.CharField(max_length=50)
    Docente = models.ForeignKey(Docente)
    Aprobacion = models.DecimalField(max_digits=4, decimal_places=2, default="11.00")
    EditarAsistencias = models.BooleanField("Editar Asistencias", default=True)
    EditarFormula = models.BooleanField("Editar Fórmula", default=False)
    EditarNotas = models.BooleanField("Editar Notas", default=False)
    EditarAplazados = models.BooleanField("Editar Aplazados", default=False)
    Observaciones = models.TextField(blank=True, null=True)
    Silabo = models.FileField(upload_to=obtenerpath_periodocurso, blank=True, null=True, verbose_name="Silabo",
                              editable=False)
    Moodleid = models.PositiveIntegerField("Moodle id", editable=False, null=True)
    Encuesta = models.BooleanField("Encuesta respondida", default=False)

    def __unicode__(self):
        return u'%s %s %s' % (self.Periodo, self.Grupo, self.Curso.Nombre)

    def Etiqueta(self):
        return u'%s %s-%s-%s Ciclo' % (self.Grupo, self.Curso.Codigo, self.Curso.Nombre, self.Curso.Ciclo)

    def CarreraCurso(self):
        return u'%s' % (self.Curso.Carrera.Carrera)

    CarreraCurso.short_description = 'Carrera'

    def silabomaterial(self):
        silabo = self.materialcurso_set.filter(Issilabo=True)
        if silabo.count() == 1:
            return "SI"
        else:
            return "NO"

    def nombrecursomoodle(self):
        return (
                self.Curso.Carrera.Codigo + " - " + self.Curso.Ciclo + " " + self.Curso.Nombre + " - " + self.Grupo).encode(
            'utf-8')

    def shortname(self):
        return (
                self.Curso.Carrera.Codigo + " - " + self.Curso.Ciclo + " " + self.Curso.Nombre + " - " + self.Grupo).encode(
            'utf-8')

    def delete(self, **kwargs):
        datos_curso = {
            'courseids[0]': self.Moodleid,
        }
        moodle_webservices("core_course_delete_courses", datos_curso)
        super(PeriodoCurso, self).delete()

    def save(self, **kwargs):
        fecha_inicio = int(time.mktime(datetime.datetime.strptime(str(self.Periodo.Inicio), "%Y-%m-%d").timetuple()))
        fecha_fin = int(time.mktime(datetime.datetime.strptime(str(self.Periodo.Fin), "%Y-%m-%d").timetuple()))
        fullname = self.nombrecursomoodle()
        shortname = self.shortname()
        if settings.ACTIVAR_SERVICIOS_MOODLE == 1:
            if self.Curso.Moodleid is not None and self.Periodo.Moodleid is not None:
                if self.Moodleid is None:
                    datos_curso = {
                        'courses[0][fullname]': fullname,
                        'courses[0][shortname]': shortname,
                        'courses[0][categoryid]': self.Curso.Moodleid,
                        'courses[0][summaryformat]': 1,
                        'courses[0][format]': "weeks",
                        'courses[0][showgrades]': 1,
                        'courses[0][newsitems]': 5,
                        'courses[0][startdate]': fecha_inicio,
                        'courses[0][enddate]': fecha_fin,
                        'courses[0][maxbytes]': 0,
                        'courses[0][showreports]': 0,
                        'courses[0][visible]': 1,
                        'courses[0][groupmode]': 0,
                        'courses[0][groupmodeforce]': 0,
                        'courses[0][defaultgroupingid]': 0,
                    }
                    rest_datos_curso = moodle_webservices("core_course_create_courses", datos_curso)
                    self.Moodleid = rest_datos_curso[0]['id']

                    # nos aseguramos que el docente tenga su usuario en moodle
                    docente = self.Docente
                    if docente.Moodleid is None:
                        docente.save()
                        # registramos al docente como profesor
                    if docente.Moodleid is not None:
                        datos_matriculacurso = {
                            'enrolments[0][roleid]': 3,
                            'enrolments[0][userid]': self.Docente.Moodleid,
                            'enrolments[0][courseid]': self.Moodleid,
                        }
                        moodle_webservices("enrol_manual_enrol_users", datos_matriculacurso)
                else:
                    profesoranterior = PeriodoCurso.objects.get(id=self.id).Docente
                    mismodocente = False
                    # nos aseguramos que el docente anterior tenga su usuario en moodle
                    if profesoranterior.Moodleid is None:
                        profesoranterior.save()

                    if self.Docente_id == profesoranterior.user_ptr_id:
                        mismodocente = True

                    if profesoranterior.Moodleid is not None and mismodocente is False:
                        datos_matriculacurso = {
                            'enrolments[0][roleid]': 5,
                            'enrolments[0][userid]': profesoranterior.Moodleid,
                            'enrolments[0][courseid]': self.Moodleid,
                        }
                        moodle_webservices("enrol_manual_unenrol_users", datos_matriculacurso)

                    datos_curso = {
                        'courses[0][id]': self.Moodleid,
                        'courses[0][fullname]': fullname,
                        'courses[0][shortname]': shortname,
                        'courses[0][categoryid]': self.Curso.Moodleid,
                        'courses[0][summaryformat]': 1,
                        'courses[0][format]': "weeks",
                        'courses[0][showgrades]': 1,
                        'courses[0][newsitems]': 5,
                        'courses[0][startdate]': fecha_inicio,
                        'courses[0][enddate]': fecha_fin,
                        'courses[0][maxbytes]': 0,
                        'courses[0][showreports]': 0,
                        'courses[0][visible]': 1,
                        'courses[0][groupmode]': 0,
                        'courses[0][groupmodeforce]': 0,
                        'courses[0][defaultgroupingid]': 0,
                    }
                    moodle_webservices("core_course_update_courses", datos_curso)

                    # nos aseguramos que el nuevo docente tenga su usuario en moodle
                    docente = self.Docente
                    if docente.Moodleid is None:
                        docente.save()
                    # registramos al docente como profesor
                    if docente.Moodleid is not None and mismodocente is False:
                        datos_matriculacurso = {
                            'enrolments[0][roleid]': 3,
                            'enrolments[0][userid]': self.Docente.Moodleid,
                            'enrolments[0][courseid]': self.Moodleid,
                        }
                        moodle_webservices("enrol_manual_enrol_users", datos_matriculacurso)
        return super(PeriodoCurso, self).save()

    class Meta:
        verbose_name = "Grupo Horario"
        verbose_name_plural = "Grupos Horarios"
        unique_together = (("Periodo", "Curso", "Grupo", "Docente",),)
        permissions = (("read_periodocurso", "Observar Grupos Horario"),)


rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
       'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': '', 'º': '', '¬': '', '¡': '',
       '!': '', '¿': '', '?': '', '^': '', '"': '', ' ': '_'}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def obtenerpath_archivo_materialcurso(instance, filename):
    ext = filename.split('.')[-1]
    nombre_archivo = filename.split('.')[0]
    nombre_archivo = nombre_archivo.strip()
    nombre_archivo = replace_all(nombre_archivo.encode('utf8'), rep)

    return 'material_cursos/%s-%s/%s/%s/%s/%s.%s' % (
        instance.PeriodoCurso.Periodo.Anio, instance.PeriodoCurso.Periodo.Semestre,
        instance.PeriodoCurso.Curso.Carrera.Codigo, instance.PeriodoCurso.Curso.Ciclo, instance.PeriodoCurso.id,
        nombre_archivo, ext)


class MaterialCurso(models.Model):
    PeriodoCurso = models.ForeignKey(PeriodoCurso)
    Nombre = models.CharField("Nombre", max_length=100)
    Descripcion = models.TextField("Descripción", blank=True, null=True)
    Archivo = models.FileField(upload_to=obtenerpath_archivo_materialcurso, verbose_name='Archivo', max_length=200)
    Fecha_creacion = models.DateTimeField('Fecha creación', auto_now_add=True, editable=False)
    Ultima_edicion = models.DateTimeField('Ultima edición', auto_now=True, editable=False)
    Issilabo = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % self.Nombre

    def save(self):
        super(MaterialCurso, self).save(using='default')
        return super(MaterialCurso, self).save(using='campus')

    class Meta:
        verbose_name = 'Material del Curso'
        verbose_name_plural = 'Material de Cursos'
        permissions = (("read_materialcurso", "Observar Material de curso"),)


class SilaboCurso(models.Model):
    PeriodoCurso = models.ForeignKey(PeriodoCurso)
    Sumilla = models.TextField("Sumilla", default='Sumilla')
    Comp_general = models.TextField("Competencia General", default='Competencia General')
    Comp_especifica1 = models.TextField("Competencia Específica 01", default='Competencia Específica 01')
    Comp_especifica2 = models.TextField("Competencia Específica 02", default='Competencia Específica 02')
    Comp_especifica3 = models.TextField("Competencia Específica 03", default='Competencia Específica 03')
    Comp_especifica4 = models.TextField("Competencia Específica 04", default='Competencia Específica 04')
    Unidad_didactica1 = models.TextField("Unidad Didáctica 01", default='Unidad Didáctica 01')
    Unidad_didactica2 = models.TextField("Unidad Didáctica 02", default='Unidad Didáctica 02')
    Unidad_didactica3 = models.TextField("Unidad Didáctica 03", default='Unidad Didáctica 03')
    Unidad_didactica4 = models.TextField("Unidad Didáctica 04", default='Unidad Didáctica 04')
    Estrategia_metodologica = models.TextField("Estrategias metodológicas", default='Estrategias metodológicas')
    Material_educativo = models.TextField("Material Educativo", default='Material Educativo')
    Evaluacion_trabajo = models.TextField("Evaluación Trabajos", default='Evaluación Trabajos')
    Evaluacion_conceptual = models.TextField("Evaluación Conceptual", default='Evaluación Conceptual')
    Evaluacion_procedimental = models.TextField("Evaluación Procedimental", default='Evaluación Procedimental')
    Evaluacion_actitudinal = models.TextField("Evaluación Actitudinal", default='Evaluación Actitudinal')
    Requisito_aprobacion = models.TextField("Requisitos Aprobación", default='Requisitos Aprobación')
    Biblio_obligatoria = models.TextField("Bibliografía Obligatoria", default='Bibliografía Obligatoria')
    Biblio_consulta = models.TextField("Bibliografía Consulta", default='Bibliografía Consulta')
    Fecha_creacion = models.DateTimeField('Fecha creación', auto_now_add=True, editable=False)
    Ultima_edicion = models.DateTimeField('Ultima edición', auto_now=True, editable=False)

    def __unicode__(self):
        return u'%s' % self.PeriodoCurso

    class Meta:
        verbose_name = 'Silabo del Curso'
        verbose_name_plural = 'Silabos de Cursos'
