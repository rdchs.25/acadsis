# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

from Cursos.models import PeriodoCurso

# imports reportlab
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from decimal import Decimal

# importar formatos reportlab creados
from Notas.formatos import acta_notas

grupos = PeriodoCurso.objects.filter(Periodo__id=9, Curso__Carrera__Codigo='AT').order_by('Curso__Ciclo',
                                                                                          'Curso__Nombre')

p = canvas.Canvas(response, pagesize=A4)
n = 201110000
for grupo in grupos:
    if grupo.Grupo.find('30') == -1:
        m = 0
        o = grupo.matriculacursos_set.filter(MatriculaCiclo__Estado='Matriculado', Convalidado=False,
                                             Estado=True).count()
        j = 1
        n += 1
        for m in range(0, o, 21):
            matriculados = grupo.matriculacursos_set.filter(MatriculaCiclo__Estado='Matriculado', Convalidado=False,
                                                            Estado=True).order_by(
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')[0 + m:21 + m]
            p = acta_notas(p)
            # Agrego el correlativo
            p.setFont("Helvetica", 14)
            # p.drawString(450,760,"Nº " + str(n))
            p.setFont("Helvetica-Bold", 10)
            p.drawString(120, 710, grupo.Curso.Nombre)
            p.drawString(120, 690, str(grupo.Curso.Creditos))
            p.drawString(120, 670, grupo.Curso.Carrera.Carrera)
            p.drawString(120, 650, unicode(
                grupo.Docente.Nombres + ' ' + grupo.Docente.ApellidoPaterno + ' ' + grupo.Docente.ApellidoMaterno))

            p.drawString(450, 710, str(grupo.Curso.Codigo))
            p.drawString(450, 690, grupo.Grupo)
            p.drawString(450, 670, str(grupo.Periodo))
            p.drawString(450, 650, 'FINAL')

            p.drawString(410, 60, unicode(
                grupo.Docente.Nombres + ' ' + grupo.Docente.ApellidoPaterno + ' ' + grupo.Docente.ApellidoMaterno))

            dict = {'0.00': 'cero', '1.00': 'uno', '2.00': 'dos', '3.00': 'tres', '4.00': 'cuatro', '5.00': 'cinco',
                    '6.00': 'seis', '7.00': 'siete', '8.00': 'ocho', '9.00': 'nueve', '10.00': 'diez', '11.00': 'once',
                    '12.00': 'doce', '13.00': 'trece', '14.00': 'catorce', '15.00': 'quince', '16.00': 'dieciseis',
                    '17.00': 'diecisiete', '18.00': 'dieciocho', '19.00': 'diecinueve', '20.00': 'veinte'}
            i = 565
            for mat in matriculados:
                if j < 10:
                    p.drawString(70, i, '0' + str(j))
                else:
                    p.drawString(70, i, str(j))
                p.drawString(93, i, mat.MatriculaCiclo.Alumno.Codigo)
                p.drawString(163, i, str(mat.MatriculaCiclo.Alumno))
                if mat.Promedio() < Decimal('10.00'):
                    p.drawString(443, i, '0' + str(mat.Promedio()).split('.')[0])
                else:
                    p.drawString(443, i, str(mat.Promedio()).split('.')[0])
                p.drawString(513, i, dict[str(mat.Promedio())])
                i = i - 20
                j += 1
            p.showPage()
            p.save()
