#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Cursos.models import PeriodoCurso
from Docentes.models import Docente


def crearusuarios():
    iddocentes = PeriodoCurso.objects.filter(Periodo_id=25).values_list('Docente_id').distinct()
    for iddocente in iddocentes:
        docente = Docente.objects.get(id=iddocente[0])
        if docente.Moodleid is not None:
            continue
        docente.save()
        print (docente.username)


crearusuarios()
