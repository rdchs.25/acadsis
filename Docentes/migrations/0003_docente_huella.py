# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0002_docente_moodleid'),
    ]

    operations = [
        migrations.AddField(
            model_name='docente',
            name='Huella',
            field=models.BinaryField(null=True),
        ),
    ]
