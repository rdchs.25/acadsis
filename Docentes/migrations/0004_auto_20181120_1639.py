# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Docentes', '0003_docente_huella'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='docente',
            options={'ordering': ['ApellidoPaterno', 'ApellidoMaterno', 'Nombres'], 'verbose_name': 'Docente UDL', 'verbose_name_plural': 'Docentes UDL'},
        ),
    ]
