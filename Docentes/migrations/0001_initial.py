# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.auth.models
import Docentes.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Docente',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('Nombres', models.CharField(max_length=50)),
                ('ApellidoPaterno', models.CharField(max_length=50, verbose_name=b'Ape. Pat.')),
                ('ApellidoMaterno', models.CharField(max_length=50, verbose_name=b'Ape. Mat.')),
                ('Email', models.EmailField(max_length=254, null=True, verbose_name=b'E-mail', blank=True)),
                ('Nacimiento', models.DateField(null=True, verbose_name=b'Nacimiento', blank=True)),
                ('Sexo', models.CharField(default=b'M', max_length=1, choices=[(b'M', b'Masculino'), (b'F', b'Femenino')])),
                ('Dni', models.CharField(max_length=20, null=True, verbose_name=b'DNI', blank=True)),
                ('Direccion', models.CharField(max_length=100, null=True, verbose_name=b'Direcci\xc3\xb3n', blank=True)),
                ('Departamento', models.CharField(max_length=50, null=True, verbose_name=b'Departamento', blank=True)),
                ('Provincia', models.CharField(max_length=50, null=True, verbose_name=b'Provincia', blank=True)),
                ('Distrito', models.CharField(max_length=50, null=True, verbose_name=b'Distrito', blank=True)),
                ('EstadoCivil', models.CharField(default=b'Soltero', max_length=20, verbose_name=b'Estado Civil', choices=[(b'Soltero', b'Soltero'), (b'Casado', b'Casado'), (b'Viudo', b'Viudo'), (b'Divorciado', b'Divorciado')])),
                ('Email2', models.EmailField(max_length=254, null=True, verbose_name=b'E-mail 2', blank=True)),
                ('Telefono', models.CharField(max_length=20, null=True, verbose_name=b'Tel\xc3\xa9fono', blank=True)),
                ('Celular', models.CharField(max_length=20, null=True, verbose_name=b'Celular', blank=True)),
                ('Perfil', models.TextField(null=True, verbose_name=b'Perfil profesional', blank=True)),
                ('Titulo', models.CharField(max_length=100, null=True, verbose_name=b'T\xc3\xadtulo', blank=True)),
                ('Grado', models.CharField(max_length=100, null=True, verbose_name=b'Grado M\xc3\xa1ximo', blank=True)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activado')),
                ('Foto', models.ImageField(upload_to=Docentes.models.obtenerpath_foto_docente, null=True, verbose_name=b'Foto', blank=True)),
                ('HojaVida', models.FileField(upload_to=Docentes.models.obtenerpath_hojavida_docente, null=True, verbose_name=b'Hoja de Vida', blank=True)),
            ],
            options={
                'verbose_name': 'Docente UDL',
                'verbose_name_plural': 'Docentes UDL',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='MoodleUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auth', models.CharField(max_length=60)),
                ('confirmed', models.IntegerField()),
                ('policyagreed', models.IntegerField()),
                ('deleted', models.IntegerField()),
                ('mnethostid', models.BigIntegerField(unique=True)),
                ('username', models.CharField(max_length=300)),
                ('password', models.CharField(max_length=96)),
                ('idnumber', models.CharField(max_length=765)),
                ('firstname', models.CharField(max_length=300)),
                ('lastname', models.CharField(max_length=300)),
                ('email', models.CharField(max_length=300)),
                ('emailstop', models.IntegerField()),
                ('icq', models.CharField(max_length=45)),
                ('skype', models.CharField(max_length=150)),
                ('yahoo', models.CharField(max_length=150)),
                ('aim', models.CharField(max_length=150)),
                ('msn', models.CharField(max_length=150)),
                ('phone1', models.CharField(max_length=60)),
                ('phone2', models.CharField(max_length=60)),
                ('institution', models.CharField(max_length=120)),
                ('department', models.CharField(max_length=90)),
                ('address', models.CharField(max_length=210)),
                ('city', models.CharField(max_length=60)),
                ('country', models.CharField(max_length=6)),
                ('lang', models.CharField(max_length=90)),
                ('theme', models.CharField(max_length=150)),
                ('timezone', models.CharField(max_length=300)),
                ('firstaccess', models.BigIntegerField()),
                ('lastaccess', models.BigIntegerField()),
                ('lastlogin', models.BigIntegerField()),
                ('currentlogin', models.BigIntegerField()),
                ('lastip', models.CharField(max_length=45)),
                ('secret', models.CharField(max_length=45)),
                ('picture', models.IntegerField()),
                ('url', models.CharField(max_length=765)),
                ('description', models.TextField(blank=True)),
                ('mailformat', models.IntegerField()),
                ('maildigest', models.IntegerField()),
                ('maildisplay', models.IntegerField()),
                ('htmleditor', models.IntegerField()),
                ('ajax', models.IntegerField()),
                ('autosubscribe', models.IntegerField()),
                ('trackforums', models.IntegerField()),
                ('timemodified', models.BigIntegerField()),
                ('trustbitmask', models.BigIntegerField()),
                ('imagealt', models.CharField(max_length=765, blank=True)),
                ('screenreader', models.IntegerField()),
            ],
            options={
                'db_table': 'mdl_user',
            },
        ),
    ]
