# Create your views here.
# -*- coding: utf-8 -*-

from decimal import Decimal

from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from Cursos.models import PeriodoCurso
from Encuesta.models import Encuesta, CategoriaItem, Item, Respuesta
from Periodos.models import Periodo
from funciones import response_excel


class IndexCursosDocenteForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


@csrf_protect
def index_cursos_docente(request):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.change_periodocurso') or request.user.has_perm(
            'Cursos.add_periodocurso'):
        if request.method == 'POST':
            form = IndexCursosDocenteForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?p=" + str(periodo.id))
        else:
            form = IndexCursosDocenteForm()
        return render_to_response('Docentes/Docente/index_cursos_docente.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


def docentes_excel(queryset):
    listado = []
    for obj in queryset:
        grupos = PeriodoCurso.objects.filter(Docente_id=obj.user_ptr_id, Periodo_id=23, Curso__Extracurricular=False)
        if grupos.count() == 0:
            continue
        auxiliar = 0
        for dat in grupos:
            data = dat.matriculacursos_set.filter(Encuesta=True)
            if data.count() == 0:
                continue
            else:
                auxiliar = auxiliar + 1
        if auxiliar == 0:
            continue

        codigo = obj.username
        apellido_paterno = obj.ApellidoPaterno
        apellido_materno = obj.ApellidoMaterno
        nombres = obj.Nombres
        email = obj.Email
        dni = obj.Dni

        promedioia = reporte_resumen_encuesta(obj.user_ptr_id, "IA")
        cantidad = 0
        sumapromedios = Decimal("0.00")
        if promedioia != "-":
            cantidad = cantidad + 1
            sumapromedios = sumapromedios + Decimal(promedioia)

        promediois = reporte_resumen_encuesta(obj.user_ptr_id, "IS")
        if promediois != "-":
            cantidad = cantidad + 1
            sumapromedios = sumapromedios + Decimal(promediois)

        promedioic = reporte_resumen_encuesta(obj.user_ptr_id, "IC")
        if promedioic != "-":
            cantidad = cantidad + 1
            sumapromedios = sumapromedios + Decimal(promedioic)

        promedioam = reporte_resumen_encuesta(obj.user_ptr_id, "AM")
        if promedioam != "-":
            cantidad = cantidad + 1
            sumapromedios = sumapromedios + Decimal(promedioam)

        promedioat = reporte_resumen_encuesta(obj.user_ptr_id, "AT")
        if promedioat != "-":
            cantidad = cantidad + 1
            sumapromedios = sumapromedios + Decimal(promedioat)
        promediogeneral = sumapromedios / cantidad
        listado.append(
            [codigo, apellido_paterno, apellido_materno, nombres, email, dni, promedioia, promediois, promedioic,
             promedioam, promedioat, promediogeneral])
    headers = ['Usuario', 'Apellido Paterno', 'Apellido Materno', 'Nombres', 'Email', 'DNI', 'IA', 'IS', 'IC', 'AM',
               'AT', 'General']
    titulo = 'Listado de docentes UDL'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='docentes')


def reporte_resumen_encuesta(docente_id, carrera):
    i = 0
    grupos = PeriodoCurso.objects.filter(Docente_id=docente_id, Curso__Carrera__Codigo=carrera, Periodo_id=23,
                                         Curso__Extracurricular=False)
    if grupos.count() == 0:
        return "-"
    sumapromedios = Decimal("0.00")
    cantidad = 0
    for dat in grupos:
        data = dat.matriculacursos_set.filter(Encuesta=True)
        if data.count() == 0:
            continue
        cantidad = cantidad + 1
        i = i + 1
        periodo_id = dat.Periodo.id
        encuesta = Encuesta.objects.get(Periodo__id=periodo_id)

        cantidad_encuestas = data.count()
        maximo = Decimal(cantidad_encuestas * 5)
        if cantidad_encuestas != 0:
            # recopilamos data
            categorias = CategoriaItem.objects.filter(
                Encuesta=encuesta).order_by('Orden')

            for categoria in categorias:
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                for item in items:
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)
            inicio_y = 4

            resumen_dimensiones = []
            promedio_global = Decimal("0.00")
            cantidad_categorias = categorias.count()
            puntaje1 = 0
            puntaje2 = 0
            puntaje3 = 0
            puntaje4 = 0
            puntaje5 = 0
            for categoria in categorias:
                categoria_resumen = []
                categoria_resumen.append(categoria.Descripcion)
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                suma_promedios = Decimal("0.00")
                cantidad_items = items.count()
                categoria_satisfaccion = False
                if categoria.Orden == 7:
                    categoria_satisfaccion = True
                for item in items:
                    # nombre de item
                    suma_valores = Decimal("0.00")
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)
                        valor_respuesta = 0
                        for respuesta in respuestas:
                            valor_respuesta = respuesta.Valor
                        # SATISFACCIÓN GENERAL
                        if categoria_satisfaccion is True:
                            if valor_respuesta == 1:
                                puntaje1 = puntaje1 + 1
                            elif valor_respuesta == 2:
                                puntaje2 = puntaje2 + 1
                            elif valor_respuesta == 3:
                                puntaje3 = puntaje3 + 1
                            elif valor_respuesta == 4:
                                puntaje4 = puntaje4 + 1
                            elif valor_respuesta == 5:
                                puntaje5 = puntaje5 + 1

                        suma_valores = suma_valores + Decimal(valor_respuesta)
                    promedio = 20 * suma_valores / maximo
                    suma_promedios = suma_promedios + promedio
                    inicio_y = inicio_y + 1
                promedio_categoria = suma_promedios / Decimal(cantidad_items)
                promedio_global = promedio_global + promedio_categoria
                categoria_resumen.append(promedio_categoria)
                resumen_dimensiones.append(categoria_resumen)
                inicio_y = inicio_y + 1
            promedio_global = round(Decimal(promedio_global) / Decimal(cantidad_categorias), 2)
            print promedio_global
            sumapromedios = round(Decimal(promedio_global) + Decimal(sumapromedios), 2)
        print sumapromedios
    if cantidad == 0:
        return "-"
    return round(Decimal(sumapromedios) / Decimal(cantidad), 2)
