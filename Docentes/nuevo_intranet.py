# -*- coding: utf-8 -*-

import cStringIO as StringIO
import cgi
import datetime
from decimal import Decimal

import ho.pisa as pisa
from django.conf import settings
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.forms import ModelForm
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect
from reportlab.lib.units import cm
# imports reportlab
from reportlab.pdfgen import canvas

from UDL import settings

from Asistencias.formatos import ficha_asistencia
from Asistencias.models import Asistencia, AsistenciaDocente
from Cursos.models import PeriodoCurso, MaterialCurso, Curso, SilaboCurso
from Docentes.forms import DocenteForm, AsistenciaComunForm, SilaboForm, MaterialForm, SilaboCursoForm
from Docentes.models import Docente
from Encuesta.models import Encuesta, CategoriaItem, Item, Respuesta
from Horarios.models import Horario
from Horarios.views import horarios_notas_curso
from Labordocente.models import PeriodoLabor, Horario as HorarioLabor
from Matriculas.models import MatriculaCursos
from Mensajes.models import Mensaje, Destinatarios
from Notas.models import Nota, NotasAlumno
from Periodos.models import Periodo
from funciones import response_excel

cursos_excluidos = []


def labor_docente(request):
    var = 'ld'
    pl = request.GET.get('pl', '')

    # verificar periodo activo
    try:
        periodo = Periodo.objects.filter(Activo=True)[0]
    except ValueError:
        mensaje = "Intranet de Docentes Deshabilitado."
        links = "<a class='btn btn-primary btn-large' href='/'>Intente nuevamente</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

    # combo box periodo labor
    periodo_labors = PeriodoLabor.objects.filter(Periodo_id=periodo.id, Docente_id=request.user.id)
    horario_labors = []
    periodo_labor_select = ''
    if pl != '':
        # Mostrar que periodo labor se eligio
        periodo_labor_select = PeriodoLabor.objects.get(id=pl)
        # Buscar horario
        horario_labors = HorarioLabor.objects.filter(PeriodoLabor_id=pl)

    return render_to_response("Docentes/Intranet/labor_docente.html",
                              {'var': var, 'periodo_labors': periodo_labors, 'horario_labors': horario_labors, 'pl': pl,
                               'periodo_labor_select': periodo_labor_select}, context_instance=RequestContext(request))


def evaluacion_docente_descarga(request, perido_curso):
    output_name = "resultado_encuesta"
    encoding = 'utf8'
    import StringIO
    output = StringIO.StringIO()
    try:
        from xlwt import Workbook, easyxf
        import xlwt
    except ImportError:
        # xlwt doesn't exist; fall back to csv
        pass
    book = xlwt.Workbook(encoding=encoding)
    i = 0
    color = 0x9ACD32
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid; align: horiz center, vert centre')
    style_heads.pattern.pattern_fore_colour = color
    style_registros = easyxf(
        'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')

    grupos = PeriodoCurso.objects.filter(id=perido_curso, Docente__user_ptr=request.user.id)

    for dat in grupos:
        i = i + 1
        periodo_id = dat.Periodo.id
        encuesta = Encuesta.objects.get(Periodo__id=periodo_id)
        data = dat.matriculacursos_set.filter(Encuesta=True)
        cantidad_encuestas = data.count()
        maximo = Decimal(cantidad_encuestas * 5)
        if cantidad_encuestas != 0:
            nombre_docente = dat.Docente.Nombres + ' ' + \
                             dat.Docente.ApellidoPaterno + ' ' + dat.Docente.ApellidoMaterno
            nombre_curso = dat.Curso.Nombre
            nombre_carrera = dat.Curso.Carrera.Carrera
            sheet = book.add_sheet('Hoja%s' % i)
            # recopilamos data
            categorias = CategoriaItem.objects.filter(
                Encuesta=encuesta).order_by('Orden')

            for categoria in categorias:
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                for item in items:
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)

            sheet.write_merge(0, 3, 0, 1, 'ITEMS DE EVALUACIÓN', style_heads)
            sheet.write(0, 2, 'Puntaje', style_heads)
            sheet.write(1, 2, nombre_carrera, style_heads)
            sheet.write(2, 2, nombre_docente, style_heads)
            sheet.write(3, 2, nombre_curso, style_heads)
            inicio_y = 4

            resumen_dimensiones = []
            promedio_global = Decimal("0.00")
            cantidad_categorias = categorias.count()
            puntaje1 = 0
            puntaje2 = 0
            puntaje3 = 0
            puntaje4 = 0
            puntaje5 = 0
            for categoria in categorias:
                categoria_resumen = []
                categoria_resumen.append(categoria.Descripcion)
                items = Item.objects.filter(
                    CategoriaItem=categoria).order_by('Orden')
                suma_promedios = Decimal("0.00")
                cantidad_items = items.count()
                categoria_satisfaccion = False
                if categoria.Orden == 7:
                    categoria_satisfaccion = True
                for item in items:
                    # nombre de item
                    sheet.write(inicio_y, 0, item.Orden, style_registros)
                    sheet.write(inicio_y, 1, item.Descripcion, style_registros)
                    suma_valores = Decimal("0.00")
                    for matriculacurso in data:
                        respuestas = Respuesta.objects.filter(
                            MatriculaCursos=matriculacurso, Item=item)
                        valor_respuesta = 0
                        for respuesta in respuestas:
                            valor_respuesta = respuesta.Valor
                        # SATISFACCIÓN GENERAL
                        if categoria_satisfaccion is True:
                            if valor_respuesta == 1:
                                puntaje1 = puntaje1 + 1
                            elif valor_respuesta == 2:
                                puntaje2 = puntaje2 + 1
                            elif valor_respuesta == 3:
                                puntaje3 = puntaje3 + 1
                            elif valor_respuesta == 4:
                                puntaje4 = puntaje4 + 1
                            elif valor_respuesta == 5:
                                puntaje5 = puntaje5 + 1

                        suma_valores = suma_valores + Decimal(valor_respuesta)
                    promedio = 20 * suma_valores / maximo
                    suma_promedios = suma_promedios + promedio
                    sheet.write(inicio_y, 2, round(
                        promedio, 2), style_registros)
                    inicio_y = inicio_y + 1
                promedio_categoria = suma_promedios / Decimal(cantidad_items)
                promedio_global = promedio_global + promedio_categoria
                categoria_resumen.append(promedio_categoria)
                resumen_dimensiones.append(categoria_resumen)
                # nombre de categoria
                sheet.write_merge(inicio_y, inicio_y, 0, 1,
                                  categoria.Descripcion, style_heads)
                sheet.write(inicio_y, 2, round(
                    promedio_categoria, 2), style_heads)
                inicio_y = inicio_y + 1
            promedio_global = promedio_global / Decimal(cantidad_categorias)

            # DIMENSIONES
            sheet.write_merge(0, 3, 4, 4, 'DIMENSIONES', style_heads)
            sheet.write(0, 5, 'Puntaje', style_heads)
            sheet.write(1, 5, nombre_carrera, style_heads)
            sheet.write(2, 5, nombre_docente, style_heads)
            sheet.write(3, 5, nombre_curso, style_heads)
            inicio_y = 4

            for resumen in resumen_dimensiones:
                sheet.write(inicio_y, 4, resumen[0], style_registros)
                sheet.write(inicio_y, 5, round(resumen[1], 1), style_registros)
                inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, 'Puntaje Global', style_heads)
            sheet.write(inicio_y, 5, round(promedio_global, 1), style_heads)
            inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, 'Número de Encuestas', style_heads)
            sheet.write(inicio_y, 5, cantidad_encuestas, style_heads)
            inicio_y = inicio_y + 2

            # SATISFACCIÓN GENERAL
            sheet.write(inicio_y, 4, "SATISFACCIÓN GENERAL", style_heads)
            sheet.write(inicio_y, 5, "N°", style_heads)
            sheet.write(inicio_y, 6, "%", style_heads)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Muy Insatisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje1, style_registros)
            porcentaje1 = Decimal(puntaje1) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje1, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Insatisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje2, style_registros)
            porcentaje2 = Decimal(puntaje2) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje2, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Indiferente", style_registros)
            sheet.write(inicio_y, 5, puntaje3, style_registros)
            porcentaje3 = Decimal(puntaje3) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje3, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Satisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje4, style_registros)
            porcentaje4 = Decimal(puntaje4) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje4, 2), style_registros)
            inicio_y = inicio_y + 1
            sheet.write(inicio_y, 4, "Muy satisfecho", style_registros)
            sheet.write(inicio_y, 5, puntaje5, style_registros)
            porcentaje5 = Decimal(puntaje5) / Decimal(cantidad_encuestas) * 100;
            sheet.write(inicio_y, 6, round(porcentaje5, 2), style_registros)
            inicio_y = inicio_y + 1

            sheet.write(inicio_y, 4, "Total", style_heads)
            sheet.write(inicio_y, 5, puntaje1 + puntaje2 + puntaje3 + puntaje4 + puntaje5, style_heads)
            sheet.write(inicio_y, 6, round(porcentaje1 + porcentaje2 + porcentaje3 + porcentaje4 + porcentaje5, 2),
                        style_heads)
            inicio_y = inicio_y + 1

            content_type = 'application/vnd.ms-excel'
            file_ext = 'xls'
        else:
            mensaje = "El curso %s no tiene matriculados" % dat
            links = "<a href=''>Volver</a>"
            return render_to_response("Periodos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type=content_type)
    response['Content-Disposition'] = 'attachment;filename="%s.%s"' % (
        output_name.replace('"', '\"'), file_ext)
    return response


def evaluacion_docente(request):
    var = request.GET.get('q')
    ie = request.GET.get('ie', '')

    # verificar periodo activo
    try:
        periodo = Periodo.objects.filter(Activo=True)[0]
    except ValueError:
        mensaje = "Intranet de Docentes Deshabilitado."
        links = "<a class='btn btn-primary btn-large' href='/'>Intente nuevamente</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

    # combo box de encuestas
    encuestas = Encuesta.objects.filter(Periodo_id=periodo)
    # promedios y promedio general
    i = 0
    lista_cursos = []
    promedio_general = 0
    if ie != '':

        grupos = PeriodoCurso.objects.filter(Periodo=periodo, Docente__user_ptr=request.user.id)

        for dat in grupos:
            i = i + 1
            encuesta = Encuesta.objects.get(id=ie)
            data = dat.matriculacursos_set.filter(Encuesta=True)
            cantidad_encuestas = data.count()
            maximo = Decimal(cantidad_encuestas * 5)
            if cantidad_encuestas != 0:
                nombre_docente = dat.Docente.Nombres + ' ' + \
                                 dat.Docente.ApellidoPaterno + ' ' + dat.Docente.ApellidoMaterno
                nombre_curso = dat.Curso.Nombre
                nombre_carrera = dat.Curso.Carrera.Carrera

                # recopilamos data
                categorias = CategoriaItem.objects.filter(
                    Encuesta=encuesta).order_by('Orden')

                for categoria in categorias:
                    items = Item.objects.filter(
                        CategoriaItem=categoria).order_by('Orden')
                    for item in items:
                        for matriculacurso in data:
                            respuestas = Respuesta.objects.filter(
                                MatriculaCursos=matriculacurso, Item=item)

                resumen_dimensiones = []
                promedio_global = Decimal("0.00")
                cantidad_categorias = categorias.count()
                puntaje1 = 0
                puntaje2 = 0
                puntaje3 = 0
                puntaje4 = 0
                puntaje5 = 0
                for categoria in categorias:
                    categoria_resumen = []
                    categoria_resumen.append(categoria.Descripcion)
                    items = Item.objects.filter(
                        CategoriaItem=categoria).order_by('Orden')
                    suma_promedios = Decimal("0.00")
                    cantidad_items = items.count()
                    categoria_satisfaccion = False
                    if categoria.Orden == 7:
                        categoria_satisfaccion = True
                    for item in items:
                        # nombre de item
                        suma_valores = Decimal("0.00")
                        for matriculacurso in data:
                            respuestas = Respuesta.objects.filter(
                                MatriculaCursos=matriculacurso, Item=item)
                            valor_respuesta = 0
                            for respuesta in respuestas:
                                valor_respuesta = respuesta.Valor
                            # SATISFACCIÓN GENERAL
                            if categoria_satisfaccion is True:
                                if valor_respuesta == 1:
                                    puntaje1 = puntaje1 + 1
                                elif valor_respuesta == 2:
                                    puntaje2 = puntaje2 + 1
                                elif valor_respuesta == 3:
                                    puntaje3 = puntaje3 + 1
                                elif valor_respuesta == 4:
                                    puntaje4 = puntaje4 + 1
                                elif valor_respuesta == 5:
                                    puntaje5 = puntaje5 + 1

                            suma_valores = suma_valores + Decimal(valor_respuesta)
                        promedio = 20 * suma_valores / maximo
                        suma_promedios = suma_promedios + promedio
                    promedio_categoria = suma_promedios / Decimal(cantidad_items)
                    promedio_global = promedio_global + promedio_categoria
                    categoria_resumen.append(promedio_categoria)
                    resumen_dimensiones.append(categoria_resumen)

                promedio_global = promedio_global / Decimal(cantidad_categorias)
                cursos = {}
                cursos['id'] = dat.id
                cursos['nombre'] = dat.Curso.Nombre
                cursos['codigo'] = dat.Curso.Codigo
                cursos['ciclo'] = dat.Curso.Ciclo
                cursos['carrera'] = dat.Curso.Carrera
                cursos['grupo'] = dat.Grupo
                cursos['promedio'] = round(promedio_global, 2)
                lista_cursos.append(cursos)
                promedio_general += promedio_global

        promedio_general = round(promedio_general / (i), 2)

    return render_to_response("Docentes/Intranet/evaluacion_docente.html",
                              {'var': var, 'encuestas': encuestas, 'ie': ie, 'cursos': lista_cursos,
                               'promedio_general': promedio_general}, context_instance=RequestContext(request))


def profile_docente(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')
        try:
            periodo = Periodo.objects.filter(Activo=True)[0]
        except ValueError:
            mensaje = "Intranet de Docentes Deshabilitado."
            links = "<a class='btn btn-primary btn-large' href='/'>Intente nuevamente</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso__Periodo=periodo,
                                          PeriodoCurso__Docente__user_ptr=request.user.id).values('Dia', 'HoraInicio',
                                                                                                  'HoraFin',
                                                                                                  'Seccion').distinct()
        cursos = PeriodoCurso.objects.filter(Periodo=periodo, Docente__user_ptr=request.user.id)
        # cursos = PeriodoCurso.objects.filter(Periodo = periodo)
        info_cursos = []
        info_cursos1 = []
        curso = []
        horario = []
        matriculados = 0
        carrera = []
        for hor in horarios:
            for cur in cursos:
                c = Horario.objects.filter(PeriodoCurso=cur, Dia=hor['Dia'], HoraInicio=hor['HoraInicio'],
                                           HoraFin=hor['HoraFin'], Seccion=hor['Seccion'])
                if c.count() > 0:
                    curso.append(c[0].PeriodoCurso)
                    if c[0].PeriodoCurso.Curso.Carrera not in carrera:
                        carrera.append(c[0].PeriodoCurso.Curso.Carrera)
                    matriculados += MatriculaCursos.objects.filter(PeriodoCurso=cur,
                                                                   PeriodoCurso__Docente__user_ptr=request.user.id,
                                                                   PeriodoCurso__Periodo=periodo,
                                                                   Convalidado=False).count()
                    dia = '%s %s - %s %s' % (hor['Dia'], hor['HoraInicio'], hor['HoraFin'], hor['Seccion'])
                    if dia not in horario:
                        horario.append(
                            '%s %s - %s %s' % (hor['Dia'], hor['HoraInicio'], hor['HoraFin'], hor['Seccion']))
            if [curso, carrera, matriculados] not in info_cursos1:
                info_cursos1.append([curso, carrera, matriculados])
                info_cursos.append([curso, carrera, matriculados, horario])
            else:
                i = info_cursos1.index([curso, carrera, matriculados])
                info_cursos[i][3].append(
                    '%s %s - %s %s' % (hor['Dia'], hor['HoraInicio'], hor['HoraFin'], hor['Seccion']))
            curso = []
            horario = []
            matriculados = 0
            carrera = []

        saludos = {'1': 'Que tal', '2': 'Bienvenido', '3': 'Como le va', '4': 'saludo'}
        import random
        numero = random.choice('1234')

        # Encuestas en línea
        # docente = Docente.objects.get(user_ptr=request.user.id)
        # cursos_mat = PeriodoCurso.objects.filter(Periodo=periodo, Docente=docente, Encuesta=False).exclude(
        #     id__in=cursos_excluidos)
        #
        # if cursos_mat.count() > 0:
        #     return HttpResponseRedirect(settings.URL_ACADSIS + "/autoevaluacion_enlinea/")

        if int(numero) == 4:
            import datetime
            now = datetime.datetime.now()
            doce = now.replace(hour=12, minute=0, second=0, microsecond=0)
            siete = now.replace(hour=19, minute=0, second=0, microsecond=0)
            media_noche = now.replace(hour=00, minute=0, second=0, microsecond=0)
            if doce > now > media_noche:
                saludo = 'Buenos días'
            elif siete > now > doce:
                saludo = 'Buenas tardes'
            elif siete < now < media_noche:
                saludo = 'Buenas noches'
            else:
                saludo = 'Buenas noches'
        else:
            saludo = saludos[numero]

        usuarioaula = request.user.username
        clave = "udl" + request.user.username

        return render_to_response("Docentes/Intranet/profile_docente.html",
                                  {"saludo": saludo, "results": info_cursos, "usuarioaula": usuarioaula, "clave": clave,
                                   "cursos": cursos, "periodo": periodo,
                                   "var": var, "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../login/')


def cursos_disponibles(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')
        try:
            periodo = Periodo.objects.filter(Activo=True)[0]
        except ValueError:
            mensaje = "Intranet de Docentes Deshabilitado."
            links = "<a class='btn btn-primary btn-large' href='/'>Intente nuevamente</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso__Periodo=periodo,
                                          PeriodoCurso__Docente__user_ptr=request.user.id).values('Dia', 'HoraInicio',
                                                                                                  'HoraFin',
                                                                                                  'Seccion').distinct()
        cursos = PeriodoCurso.objects.filter(Periodo=periodo, Docente__user_ptr=request.user.id)
        cursos = PeriodoCurso.objects.filter(Periodo=periodo)
        info_cursos = []
        info_cursos1 = []
        curso = []
        horario = []
        matriculados = 0
        carrera = []
        for hor in horarios:
            cant = 0
            carrera_anterior = ''
            for cur in cursos:
                c = Horario.objects.filter(PeriodoCurso=cur, Dia=hor['Dia'], HoraInicio=hor['HoraInicio'],
                                           HoraFin=hor['HoraFin'], Seccion=hor['Seccion'])
                if c.count() > 0:
                    cant_mats = MatriculaCursos.objects.filter(PeriodoCurso=cur,
                                                               PeriodoCurso__Docente__user_ptr=request.user.id,
                                                               PeriodoCurso__Periodo=periodo, Convalidado=False).count()
                    matriculados += cant_mats
                    curso.append(c[0].PeriodoCurso)
                    if c[0].PeriodoCurso.Curso.Carrera not in carrera:
                        if cant_mats > cant:
                            cant = cant_mats
                            try:
                                carrera.remove(carrera_anterior)
                            except ValueError:
                                pass
                            carrera.append(c[0].PeriodoCurso.Curso.Carrera)
                            carrera_anterior = c[0].PeriodoCurso.Curso.Carrera
                    dia = '%s %s - %s %s' % (hor['Dia'], hor['HoraInicio'], hor['HoraFin'], hor['Seccion'])
                    if dia not in horario:
                        horario.append(
                            '%s %s - %s %s' % (hor['Dia'], hor['HoraInicio'], hor['HoraFin'], hor['Seccion']))
            if [curso, carrera, matriculados] not in info_cursos1:
                info_cursos1.append([curso, carrera, matriculados])
                info_cursos.append([curso, carrera, matriculados, horario])
            else:
                i = info_cursos1.index([curso, carrera, matriculados])
                info_cursos[i][3].append(
                    '%s %s - %s %s' % (hor['Dia'], hor['HoraInicio'], hor['HoraFin'], hor['Seccion']))
            curso = []
            horario = []
            matriculados = 0
            carrera = []
        return render_to_response("Docentes/Intranet/cursos_disponibles.html",
                                  {"results": info_cursos, "var": var, "periodo": periodo, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../login/')


def datos_personales_docente(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'd')
        try:
            periodo = Periodo.objects.filter(Activo=True)[0]
        except ValueError:
            mensaje = "Intranet de Docentes Deshabilitado."
            links = "<a class='btn btn-primary btn-large' href='/'>Intente nuevamente</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        try:
            docente = Docente.objects.get(id=request.user.id)
            return render_to_response("Docentes/Intranet/datos_personales_docente.html",
                                      {"var": var, "docente": docente, "periodo": periodo, "user": request.user},
                                      context_instance=RequestContext(request))
        except ValueError:
            return HttpResponseRedirect('../../login/')
    else:
        return HttpResponseRedirect('../../login/')


def editar_datos_personales_docente(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'd')
        verifica_post = False
        try:
            periodo = Periodo.objects.filter(Activo=True)[0]
        except ValueError:
            mensaje = "Intranet de Docentes Deshabilitado."
            links = "<a class='btn btn-primary btn-large' href='/'>Intente nuevamente</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        try:
            docente = Docente.objects.get(id=request.user.id)
        except ValueError:
            return HttpResponseRedirect('../../../login/')

        if request.method == "POST":
            verifica_post = True
            form = DocenteForm(request.POST, request.FILES)
            if form.is_valid():
                docente.ApellidoPaterno = form.cleaned_data['ApellidoPaterno']
                docente.ApellidoMaterno = form.cleaned_data['ApellidoMaterno']
                docente.Nombres = form.cleaned_data['Nombres']
                docente.Nacimiento = form.cleaned_data['Nacimiento']
                docente.Sexo = form.cleaned_data['Sexo']
                docente.Dni = form.cleaned_data['Dni']
                docente.EstadoCivil = form.cleaned_data['EstadoCivil']
                docente.Direccion = form.cleaned_data['Direccion']
                docente.Departamento = form.cleaned_data['Departamento']
                docente.Provincia = form.cleaned_data['Provincia']
                docente.Distrito = form.cleaned_data['Distrito']
                docente.Telefono = form.cleaned_data['Telefono']
                docente.Celular = form.cleaned_data['Celular']
                docente.Email2 = form.cleaned_data['Email2']
                docente.Perfil = form.cleaned_data['Perfil']
                docente.Titulo = form.cleaned_data['Titulo']
                docente.Grado = form.cleaned_data['Grado']
                if form.cleaned_data['HojaVida']:
                    docente.HojaVida = form.cleaned_data['HojaVida']
                if form.cleaned_data['Foto']:
                    docente.Foto = form.cleaned_data['Foto']

                docente.save()

                return HttpResponseRedirect('../?q=d')
        else:
            form = DocenteForm(initial={"EstadoCivil": docente.EstadoCivil, "Nacimiento": docente.Nacimiento,
                                        "Perfil": docente.Perfil})

        return render_to_response("Docentes/Intranet/editar_datos_personales_docente.html",
                                  {"var": var, "verifica_post": verifica_post, "form": form, "docente": docente,
                                   "periodo": periodo, "user": request.user}, context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('../../../login/')


def index_intranet_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'm')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        carreras = []
        for per in periodo_cursos:
            if per.Curso.Carrera not in carreras:
                carreras.append(per.Curso.Carrera)
        return render_to_response("Docentes/Intranet/index_intranet_curso.html",
                                  {"var": var, "horarios": horarios, "periodocurso": periodocurso,
                                   'cursos': periodo_cursos, "carreras": carreras, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../login/')


def listado_matriculados_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'm')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        matriculados = []
        for per in periodo_cursos:
            mat = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False).order_by(
                'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')
            for m in mat:
                matriculados.append(m)
        return render_to_response("Docentes/Intranet/listado_matriculados_curso.html",
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "horarios": horarios,
                                   "matriculados": matriculados, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../login/')


def listado_matriculados_excel(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        matriculados = []
        i = 1
        for per in periodo_cursos:
            mats = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True).order_by(
                'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')
            for mat in mats:
                apellidos = mat.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + mat.MatriculaCiclo.Alumno.ApellidoMaterno
                nombres = mat.MatriculaCiclo.Alumno.Nombres
                if mat.Estado is True:
                    condicion = 'Matriculado'
                else:
                    condicion = 'Retirado'

                if mat.Convalidado is True:
                    convalidado = 'Si'
                else:
                    convalidado = 'No'
                matriculados.append(
                    [mat.MatriculaCiclo.Alumno.Codigo, apellidos, nombres, mat.MatriculaCiclo.Alumno.Carrera.Carrera,
                     mat.PeriodoCurso.Grupo, condicion, convalidado])
                i += 1
        headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'GH', 'Condición', 'Convalidado']
        titulo = 'LISTADO DE MATRICULADOS EN : %s' % periodocurso.Curso
        return response_excel(titulo=titulo, heads=headers, registros=matriculados, nombre_archivo='lista_matriculados')

    else:
        return HttpResponseRedirect('/docente/login/')


@csrf_exempt
def registrar_notas(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'n')

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__id=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a class='btn btn-primary btn-large' href='../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        registros = []
        for per in periodo_cursos:
            notas = Nota.objects.filter(PeriodoCurso=per).order_by('id')
            mats = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False).order_by(
                'MatriculaCiclo__Alumno__ApellidoPaterno', 'MatriculaCiclo__Alumno__ApellidoMaterno',
                'MatriculaCiclo__Alumno__Nombres')
            for mat in mats:
                # ----------------
                ident1 = []
                if notas.count() != 0:
                    for cal1 in notas:
                        if cal1.Nota in ident1:
                            pass
                        else:
                            if cal1.Nivel == "0":
                                ultima_nota = cal1.Nota
                                subnotas1 = Nota.objects.filter(NotaPadre=cal1)
                                try:
                                    nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=mat)
                                except NotasAlumno.DoesNotExist:
                                    nota = NotasAlumno(Nota=cal1, MatriculaCursos=mat, Valor=Decimal(mat.Promedio()))
                                    nota.save()
                            else:
                                subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                                if subnotas2.count() == 0:
                                    try:
                                        nota = NotasAlumno.objects.get(Nota=cal1, MatriculaCursos=mat)
                                    except NotasAlumno.DoesNotExist:
                                        nota = NotasAlumno(Nota=cal1, MatriculaCursos=mat, Valor=Decimal("0.00"))
                                        nota.save()
                                    ident1.append(cal1.Nota)
                                else:
                                    for cal2 in subnotas2:
                                        subnotas3 = Nota.objects.filter(NotaPadre=cal2)
                                        if subnotas3.count() == 0:
                                            try:
                                                nota = NotasAlumno.objects.get(Nota=cal2, MatriculaCursos=mat)
                                            except NotasAlumno.DoesNotExist:
                                                nota = NotasAlumno(Nota=cal2, MatriculaCursos=mat,
                                                                   Valor=Decimal("0.00"))
                                                nota.save()
                                            ident1.append(cal2.Nota)
                                        else:
                                            for cal3 in subnotas3:
                                                try:
                                                    nota = NotasAlumno.objects.get(Nota=cal3, MatriculaCursos=mat)
                                                except NotasAlumno.DoesNotExist:
                                                    nota = NotasAlumno(Nota=cal3, MatriculaCursos=mat,
                                                                       Valor=Decimal("0.00"))
                                                    nota.save()
                                                ident1.append(cal3.Nota)
                                            ident1.append(cal2.Nota)
                                    ident1.append(cal1.Nota)
                    ident1.append(ultima_nota)
                # ----------------
                notas0 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, Nivel='0').order_by('Orden')
                if notas0.count() == 1:
                    valor_final = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=notas0[0])
                    if valor_final.count() == 1:
                        id_nota_final = valor_final[0].Nota.id
                        valor_final = valor_final[0].Valor

                notas1 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, Nivel='1').order_by('Orden')

                nots1 = []
                nots2 = []
                nots3 = []

                for nota1 in notas1:
                    nots_2 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota1, Nivel='2').order_by(
                        'Orden')
                    for nota2 in nots_2:
                        nots_3 = Nota.objects.filter(PeriodoCurso=mat.PeriodoCurso, NotaPadre=nota2,
                                                     Nivel='3').order_by('Orden')
                        for nota3 in nots_3:
                            valor3 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota3)
                            if valor3.count() != 0:
                                valor3 = valor3[0].Valor
                                nots3.append([nota3, int(str(valor3).split('.')[0])])
                            else:
                                valor3 = mat.PromedioParcial(nota3)
                                nots3.append([nota3, valor3])
                        valor2 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota2)
                        if valor2.count() != 0:
                            valor2 = valor2[0].Valor
                            nots2.append([nota2, nots3, int(str(valor2).split('.')[0])])
                        else:
                            valor2 = mat.PromedioParcial(nota2)
                            nots2.append([nota2, nots3, valor2])
                        nots3 = []
                    valor1 = NotasAlumno.objects.filter(MatriculaCursos=mat, Nota=nota1)
                    if valor1.count() != 0:
                        valor1 = valor1[0].Valor
                        nots1.append([nota1, nots2, int(str(valor1).split('.')[0])])
                    else:
                        valor1 = mat.PromedioParcial(nota1)
                        nots1.append([nota1, nots2, valor1])
                    nots2 = []
                registros.append([mat, nots1, id_nota_final, int(str(valor_final).split('.')[0])])
                nots1 = []
        # ----------------------------------
        ident = []
        formulas = []
        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')
        notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso=periodocurso).order_by('Nivel', 'Orden')

        for nota in notas:
            ident.append(nota)

        for nota in notas1:
            subnotas = Nota.objects.filter(NotaPadre=nota)
            k = 1
            formula = ''
            suma = Decimal('0.00')
            for subnota in subnotas:
                if k == 1:
                    formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
                else:
                    formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
                k += 1
                suma += Decimal(nota.Peso)
            formula += ')/%s' % str(suma)
            formulas.append(formula)

        identificador = "PP2"
        return render_to_response("Docentes/Intranet/registrar_notas.html",
                                  {"identificador": identificador, "var": var, "var1": var1, "horarios": horarios,
                                   "periodocurso": periodocurso, "registros": registros, "notas": ident,
                                   "formulas": formulas, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_exempt
def guardar_notas(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__id=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a class='btn btn-primary btn-large' href='../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        vnotas = request.POST['vnotas']
        rsp = request.POST['vnotas'].split('|')
        pps = {}
        for r in rsp:
            if r != '':
                id_matriculado = r.split('-')[0].split('_')[1]
                id_nota = r.split('-')[0].split('_')[0]
                valor = r.split('-')[1]
                notasalumno = NotasAlumno.objects.get(MatriculaCursos__id=id_matriculado, Nota__id=id_nota)
                notasalumno.Valor = valor
                notasalumno.save()

                nota_promedio = NotasAlumno.objects.get(MatriculaCursos__id=id_matriculado, Nota__Nivel='0')
                if nota_promedio != notasalumno:
                    # nota_promedio.Valor = nota_promedio.MatriculaCursos.Promedio1()
                    # nota_promedio.save()

                    if notasalumno.Nota.NotaPadre.Nivel != '0':
                        pps['%s_%s' % (notasalumno.Nota.NotaPadre.id,
                                       notasalumno.MatriculaCursos.id)] = notasalumno.MatriculaCursos.PromedioParcial(
                            notasalumno.Nota.NotaPadre)
                    else:
                        promedio = int(str(notasalumno.MatriculaCursos.Promedio()).split('.')[0])
                        pps['%s_%s' % (notasalumno.Nota.NotaPadre.id, notasalumno.MatriculaCursos.id)] = promedio
                        pps['ps_%s_%s' % (notasalumno.Nota.NotaPadre.id, notasalumno.MatriculaCursos.id)] = promedio
                    pps[notasalumno.MatriculaCursos.id] = notasalumno.MatriculaCursos.Promedio1()
                else:
                    pps['ps_%s_%s' % (notasalumno.Nota.id, notasalumno.MatriculaCursos.id)] = notasalumno.Valor
                    # pps[notasalumno.MatriculaCursos.id] = notasalumno.MatriculaCursos.Promedio1()
        for pp in pps.items():
            vnotas += '%s-%s|' % (pp[0], pp[1])
        return HttpResponse(vnotas)
    else:
        return HttpResponseRedirect('../../../../login/')


@csrf_exempt
def registrar_formula(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'f')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__id=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada"
            links = "<a class='btn btn-primary btn-large' href='../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)

        notas = Nota.objects.filter(PeriodoCurso=periodocurso).order_by('id')
        if notas.filter(Nivel='0').count() != 0:
            nivel_cero = True
        else:
            nivel_cero = False

        titulos = "['N','Estudiante'"
        celdas = "[{name:'id1',index:'id1', align:'center', width:55},{name:'estudiante',index:'estudiante', width:250},"
        ident = []
        formulas = []
        editable = 'true'

        if notas.count() != 0:
            for cal1 in notas:
                if cal1 in ident:
                    pass
                else:
                    if cal1.Nivel == "0":
                        ultima_nota = cal1
                        subnotas1 = Nota.objects.filter(NotaPadre=cal1)
                        formula = "%s = (" % cal1.Identificador
                        suma_pesos = 0
                        i = 1
                        if subnotas1.count() != 0:
                            for nota in subnotas1:
                                if i == 1:
                                    formula += "%s*%s" % (nota.Identificador, nota.Peso)
                                else:
                                    formula += " + %s*%s" % (nota.Identificador, nota.Peso)
                                suma_pesos += nota.Peso
                                i += 1
                            formula += ")/%s" % suma_pesos
                            formulas.append(formula)
                        else:
                            formula = "%s = %s" % (cal1.Identificador, cal1.Identificador)
                            formulas.append(formula)
                    else:
                        subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                        if subnotas2.count() == 0:
                            ident.append(cal1)
                            if cal1.PeriodoCurso.EditarNotas is True:
                                editable = 'true'
                            else:
                                editable = 'false'
                            titulos += ",'%s'" % cal1.Identificador
                            celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:%s, editrules:{required:true, number:true, custom:true, custom_func:evaluar_nota}}," % (
                                cal1.id, cal1.Identificador, editable)
                        else:
                            formula1 = "%s = (" % cal1.Identificador
                            suma_pesos1 = 0
                            i = 1
                            for cal2 in subnotas2:
                                if i == 1:
                                    formula1 += "%s*%s" % (cal2.Identificador, cal2.Peso)
                                else:
                                    formula1 += " + %s*%s" % (cal2.Identificador, cal2.Peso)
                                suma_pesos1 += cal2.Peso
                                i += 1

                                subnotas3 = Nota.objects.filter(NotaPadre=cal2)
                                if subnotas3.count() == 0:
                                    ident.append(cal2)
                                    if cal2.PeriodoCurso.EditarNotas is True:
                                        editable = 'true'
                                    else:
                                        editable = 'false'
                                    titulos += ",'%s'" % cal2.Identificador
                                    celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:%s, editrules:{required:true, number:true, custom:true, custom_func:evaluar_nota}}," % (
                                        cal2.id, cal2.Identificador, editable)
                                else:
                                    formula2 = "%s = (" % cal2.Identificador
                                    suma_pesos2 = 0
                                    i = 1
                                    for cal3 in subnotas3:
                                        if i == 1:
                                            formula2 += "%s*%s" % (cal3.Identificador, cal3.Peso)
                                        else:
                                            formula2 += " + %s*%s" % (cal3.Identificador, cal3.Peso)
                                        suma_pesos2 += cal3.Peso
                                        i += 1

                                        ident.append(cal3)
                                        if cal3.PeriodoCurso.EditarNotas is True:
                                            editable = 'true'
                                        else:
                                            editable = 'false'
                                        titulos += ",'%s'" % cal3.Identificador
                                        celdas += "{name:'%s',index:'%s', width:80,sortable:false, align:'center',editable:%s, editrules:{required:true, number:true, custom:true, custom_func:evaluar_nota}}," % (
                                            cal3.id, cal3.Identificador, editable)
                                    formula2 += ")/%s" % suma_pesos2
                                    formulas.append(formula2)
                                    ident.append(cal2)
                            formula1 += ")/%s" % suma_pesos1
                            formulas.append(formula1)
                            ident.append(cal1)
            ident.append(ultima_nota)
            if ultima_nota.PeriodoCurso.EditarNotas is True:
                editable = 'true'
            else:
                editable = 'false'
            titulos += ",'%s'" % ultima_nota.Identificador
            celdas += "{name:'%s',index:'%s', width:80, align:'center',editable:%s}," % (
                ultima_nota.id, ultima_nota.Identificador, editable)

        titulos += "]"
        celdas += "]"
        return render_to_response("Docentes/Intranet/formula_calificacion.html",
                                  {"var": var, "var1": var1, "horarios": horarios, "periodocurso": periodocurso,
                                   "titulos": mark_safe(titulos), "celdas": mark_safe(celdas), "nivel_cero": nivel_cero,
                                   "notas": ident, "formulas": formulas, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_exempt
def master_subgrid_notas_nueva(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']

        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']

        if operacion == "add":
            for periodo_curso in periodo_cursos:
                guardar_nota = Nota(PeriodoCurso=periodo_curso, Nota=nota, Identificador=identificador,
                                    Peso=Decimal('1.00'), Nivel='0', Orden='1', NotaPadre=None, SubNotas=False)
                guardar_nota.save()
        elif operacion == "edit":
            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    obj_nota1.Nota = nota
                    obj_nota1.Identificador = identificador
                    obj_nota1.NotaPadre = None
                    obj_nota1.save()
            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.NotaPadre = None
            obj_nota.save()
        elif operacion == "del":
            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    obj_nota1.delete()
            obj_nota.delete()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_exempt
def master_subgrid_notas1_nueva(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']
        fila_id = request.GET.get('fila', '')

        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']
            orden = request.POST['Orden']
            peso = request.POST['Peso']

        if operacion == "add":
            nota_padre = Nota.objects.get(id=fila_id)
            for periodo_curso in periodo_cursos:
                nota_padre1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=nota_padre.Nota,
                                               Identificador=nota_padre.Identificador, Peso=nota_padre.Peso, Nivel='0',
                                               Orden=nota_padre.Orden)
                guardar_nota = Nota(PeriodoCurso=periodo_curso, Nota=nota, Identificador=identificador, Peso=peso,
                                    Nivel='1', Orden=orden, NotaPadre_id=nota_padre1.id)
                guardar_nota.save()

                nota_padre1.SubNotas = True
                nota_padre1.save()
                notas_alumnos = NotasAlumno.objects.filter(Nota=nota_padre1)
                notas_alumnos.delete()
        elif operacion == "edit":
            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    obj_nota1.Nota = nota
                    obj_nota1.Identificador = identificador
                    obj_nota1.Orden = orden
                    obj_nota1.Peso = peso
                    obj_nota1.save()
                    nota_padre = Nota.objects.get(id=obj_nota1.NotaPadre.id)
                    nota_padre.SubNotas = True
                    nota_padre.save()

            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.Orden = orden
            obj_nota.Peso = peso
            obj_nota.save()
            nota_padre = Nota.objects.get(id=obj_nota.NotaPadre.id)
            nota_padre.SubNotas = True
            nota_padre.save()
        elif operacion == "del":
            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    nota_padre_id1 = obj_nota1.NotaPadre.id
                    obj_nota1.delete()
                    cantidad1 = Nota.objects.filter(NotaPadre__id=nota_padre_id1).count()
                    if cantidad1 == 0:
                        nota_padre1 = Nota.objects.get(id=nota_padre_id1)
                        nota_padre1.SubNotas = False
                        nota_padre1.save()

            nota_padre_id = obj_nota.NotaPadre.id
            obj_nota.delete()
            cantidad = Nota.objects.filter(NotaPadre__id=nota_padre_id).count()
            if cantidad == 0:
                nota_padre = Nota.objects.get(id=nota_padre_id)
                nota_padre.SubNotas = False
                nota_padre.save()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_exempt
def master_subgrid_notas2_nueva(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']
        fila_id = request.GET.get('fila', '')

        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']
            orden = request.POST['Orden']
            peso = request.POST['Peso']

        if operacion == "add":
            nota_padre = Nota.objects.get(id=fila_id)
            for periodo_curso in periodo_cursos:
                nota_padre1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=nota_padre.Nota,
                                               Identificador=nota_padre.Identificador, Peso=nota_padre.Peso, Nivel='1',
                                               Orden=nota_padre.Orden)
                guardar_nota = Nota(PeriodoCurso=periodo_curso, Nota=nota, Identificador=identificador, Peso=peso,
                                    Nivel='2', Orden=orden, NotaPadre_id=nota_padre1.id)
                guardar_nota.save()

                nota_padre1.SubNotas = True
                nota_padre1.save()
                notas_alumnos = NotasAlumno.objects.filter(Nota=nota_padre1)
                notas_alumnos.delete()
        elif operacion == "edit":

            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    obj_nota1.Nota = nota
                    obj_nota1.Identificador = identificador
                    obj_nota1.Orden = orden
                    obj_nota1.Peso = peso
                    obj_nota1.save()
                    nota_padre = Nota.objects.get(id=obj_nota1.NotaPadre.id)
                    nota_padre.SubNotas = True
                    nota_padre.save()

            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.Orden = orden
            obj_nota.Peso = peso
            obj_nota.save()
            nota_padre = Nota.objects.get(id=obj_nota.NotaPadre.id)
            nota_padre.SubNotas = True
            nota_padre.save()

        elif operacion == "del":

            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    nota_padre_id1 = obj_nota1.NotaPadre.id
                    obj_nota1.delete()
                    cantidad1 = Nota.objects.filter(NotaPadre__id=nota_padre_id1).count()
                    if cantidad1 == 0:
                        nota_padre1 = Nota.objects.get(id=nota_padre_id1)
                        nota_padre1.SubNotas = False
                        nota_padre1.save()

            nota_padre_id = obj_nota.NotaPadre.id
            obj_nota.delete()
            cantidad = Nota.objects.filter(NotaPadre__id=nota_padre_id).count()
            if cantidad == 0:
                nota_padre = Nota.objects.get(id=nota_padre_id)
                nota_padre.SubNotas = False
                nota_padre.save()

        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_exempt
def master_subgrid_notas3_nueva(request, periodocurso_id):
    if request.user.is_authenticated() and request.GET.get('add', '') == 'True' or request.GET.get('change',
                                                                                                   '') == 'True' or request.GET.get(
        'delete', '') == 'True':
        operacion = request.POST['oper']
        nota_id = request.POST['id']
        fila_id = request.GET.get('fila', '')

        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        if operacion == "add" or operacion == "edit":
            nota = request.POST['Nota']
            identificador = request.POST['Identificador']
            orden = request.POST['Orden']
            peso = request.POST['Peso']

        if operacion == "add":
            nota_padre = Nota.objects.get(id=fila_id)
            for periodo_curso in periodo_cursos:
                nota_padre1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=nota_padre.Nota,
                                               Identificador=nota_padre.Identificador, Peso=nota_padre.Peso, Nivel='2',
                                               Orden=nota_padre.Orden)
                guardar_nota = Nota(PeriodoCurso=periodo_curso, Nota=nota, Identificador=identificador, Peso=peso,
                                    Nivel='3', Orden=orden, NotaPadre_id=nota_padre1.id)
                guardar_nota.save()

                nota_padre1.SubNotas = True
                nota_padre1.save()
                notas_alumnos = NotasAlumno.objects.filter(Nota=nota_padre1)
                notas_alumnos.delete()
        elif operacion == "edit":

            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    obj_nota1.Nota = nota
                    obj_nota1.Identificador = identificador
                    obj_nota1.Orden = orden
                    obj_nota1.Peso = peso
                    obj_nota1.save()
                    nota_padre = Nota.objects.get(id=obj_nota1.NotaPadre.id)
                    nota_padre.SubNotas = True
                    nota_padre.save()

            obj_nota.Nota = nota
            obj_nota.Identificador = identificador
            obj_nota.Orden = orden
            obj_nota.Peso = peso
            obj_nota.save()
            nota_padre = Nota.objects.get(id=obj_nota.NotaPadre.id)
            nota_padre.SubNotas = True
            nota_padre.save()

        elif operacion == "del":

            obj_nota = Nota.objects.get(id=nota_id)

            for periodo_curso in periodo_cursos:
                if periodo_curso != obj_nota.PeriodoCurso:
                    obj_nota1 = Nota.objects.get(PeriodoCurso=periodo_curso, Nota=obj_nota.Nota,
                                                 Identificador=obj_nota.Identificador, Peso=obj_nota.Peso,
                                                 Nivel=obj_nota.Nivel, Orden=obj_nota.Orden, SubNotas=obj_nota.SubNotas)
                    nota_padre_id1 = obj_nota1.NotaPadre.id
                    obj_nota1.delete()
                    cantidad1 = Nota.objects.filter(NotaPadre__id=nota_padre_id1).count()
                    if cantidad1 == 0:
                        nota_padre1 = Nota.objects.get(id=nota_padre_id1)
                        nota_padre1.SubNotas = False
                        nota_padre1.save()

            nota_padre_id = obj_nota.NotaPadre.id
            obj_nota.delete()
            cantidad = Nota.objects.filter(NotaPadre__id=nota_padre_id).count()
            if cantidad == 0:
                nota_padre = Nota.objects.get(id=nota_padre_id)
                nota_padre.SubNotas = False
                nota_padre.save()
        return horarios_notas_curso(request, periodocurso_id)
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_protect
def listado_asistencias(request, id_periodo, id_docente, dia, inicio, fin):
    if request.user.is_authenticated():

        periodo_curso = PeriodoCurso.objects.filter(Periodo__id=id_periodo, Docente__id=request.user.id)
        if periodo_curso.count() == 0:
            mensaje = "El curso no existe o no tiene permiso de entrada."
            links = "<a class='btn btn-primary btn-large' href='../../../../../../cursos_disponibles/?q=l'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=id_periodo, Dia=dia, HoraInicio=inicio,
                                              HoraFin=fin, PeriodoCurso__Docente__id=request.user.id)
            if horarios.count() == 0:
                mensaje = "El curso no se le a asignado horarios."
                links = "<a class='btn btn-primary btn-large' href='../../../../../../cursos_disponibles/?q=l'>Regresar</a>"
                return render_to_response("Docentes/Intranet/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
            else:
                periodocurso = horarios[0].PeriodoCurso
                horarios1 = Horario.objects.filter(PeriodoCurso=periodocurso)
                horario = horarios[0]
                carreras = horarios.values('PeriodoCurso__Curso__Carrera__Carrera').distinct()

        results2 = Asistencia.objects.filter(Horario__PeriodoCurso__Periodo__id=id_periodo,
                                             Horario__PeriodoCurso__Docente=request.user, Horario__Dia=dia,
                                             Horario__HoraInicio=inicio, Horario__HoraFin=fin).order_by(
            '-Fecha').values('Fecha', 'Horario__Dia', 'Horario__HoraInicio', 'Horario__HoraFin').distinct()
        results3 = []
        n_asistencias = 0
        for res in results2:
            results3.append([res['Fecha'], res['Horario__Dia'], res['Horario__HoraInicio'], res['Horario__HoraFin']])
            n_asistencias += 1

        paginator = Paginator(results3, 100)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)
        return render_to_response("Docentes/Intranet/listado_asistencias.html",
                                  {"results": results, "periodocurso": periodocurso, "horarios": horarios1,
                                   "horario": horario, "carreras": carreras, "n_asistencias": n_asistencias,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/docente/login/')


def pdf_asistencia_comun(request, id_periodo, dia, inicio, fin):
    if request.user.is_authenticated():
        estudiantes = MatriculaCursos.objects.filter(PeriodoCurso__Periodo__id=id_periodo,
                                                     PeriodoCurso__Docente=request.user).order_by(
            'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Alumno__ApellidoPaterno',
            'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')

        horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=id_periodo, Dia=dia, HoraInicio=inicio, HoraFin=fin,
                                          PeriodoCurso__Docente=request.user)
        alumnos = []
        periodocurso = None
        estudiante_mayor = None
        if estudiantes.count() != 0 and horarios.count() != 0:
            periodocurso = horarios[0].PeriodoCurso

            cantidad_asistencias = 0
            for estudiante in estudiantes:
                for horario in horarios:
                    if estudiante.PeriodoCurso == horario.PeriodoCurso:
                        alumnos.append(estudiante)
                        if estudiante.asistencia_set.count() > cantidad_asistencias:
                            cantidad_asistencias = estudiante.asistencia_set.count()
                            estudiante_mayor = estudiante
        fechas = estudiante_mayor.asistencia_set.filter(Horario__PeriodoCurso__Periodo__id=id_periodo,
                                                        Horario__PeriodoCurso__Docente=request.user).order_by(
            'Fecha').values('Fecha')
        idhorarios = estudiante_mayor.asistencia_set.filter(Horario__PeriodoCurso__Periodo__id=id_periodo,
                                                            Horario__PeriodoCurso__Docente=request.user).order_by(
            'Fecha').values('Horario_id')

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=ficha_asistencias.pdf'
        p = canvas.Canvas(response, pagesize=(29.7 * cm, 21 * cm))
        p = ficha_asistencia(p, alumnos, fechas, periodocurso, idhorarios)
        return response
    else:
        return HttpResponseRedirect('/docente/login/')


@csrf_protect
def agregar_asistencia1(request, id_periodo, dia, inicio, fin):
    if request.user.is_authenticated():
        estudiantes = MatriculaCursos.objects.filter(PeriodoCurso__Periodo__id=id_periodo,
                                                     PeriodoCurso__Docente=request.user, Estado=True,
                                                     Convalidado=False).order_by(
            'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Alumno__ApellidoPaterno',
            'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')

        horarios = Horario.objects.filter(PeriodoCurso__Periodo__id=id_periodo, Dia=dia, HoraInicio=inicio, HoraFin=fin,
                                          PeriodoCurso__Docente=request.user)

        if estudiantes.count() != 0 and horarios.count() != 0:
            periodocurso = estudiantes[0].PeriodoCurso
            if periodocurso.EditarAsistencias is False:
                mensaje = "Registro de asistencias desactivada"
                links = "<a class='btn btn-primary btn-large' href='javascript:window.close()'>Cerrar</a>"
                return render_to_response("Docentes/Intranet/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
            alumnos = []
            for estudiante in estudiantes:
                for horario in horarios:
                    if estudiante.PeriodoCurso == horario.PeriodoCurso:
                        alumnos.append(estudiante)
        else:
            mensaje = "El curso no tiene horarios o no tiene permiso de entrada."
            links = "<a class='btn btn-primary btn-large' href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        existe = False
        fecha = ''
        tema_clase = ''
        no_registrado = ''
        registrados = []
        if request.method == 'POST':
            form = AsistenciaComunForm(request.POST)
            if form.is_valid():
                fecha = form.cleaned_data['Fecha']
                tema_clase = form.cleaned_data['TemaClase']

                existe_asistencia = Asistencia.objects.filter(Fecha=fecha,
                                                              Horario__PeriodoCurso__Periodo__id=id_periodo,
                                                              Horario__Dia=dia, Horario__HoraInicio=inicio,
                                                              Horario__HoraFin=fin,
                                                              Horario__PeriodoCurso__Docente__id=request.user.id)
                if existe_asistencia.count() != 0:
                    existe = True
                else:
                    for alumno in alumnos:
                        if alumno.Estado is True and alumno.Convalidado is False:
                            validar_alumno = request.POST.get(str(alumno.id), False)
                            if validar_alumno is False:
                                if no_registrado == '':
                                    if alumno.Inhabilitado() is False:
                                        pass
                                        # no_registrado = alumno
                                    if alumno.Inhabilitadoporpagos() is False:
                                        pass
                                        # no_registrado = alumno
                            else:
                                registrados.append([alumno.id, validar_alumno])
                    if no_registrado == '':
                        for alumno in alumnos:
                            if alumno.Inhabilitado() is False and alumno.Inhabilitadoporpagos() is False:
                                if alumno.Estado is True and alumno.Convalidado is False:
                                    asistencia = request.POST[str(alumno.id)]
                                    horario1 = Horario.objects.get(PeriodoCurso=alumno.PeriodoCurso, Dia=dia,
                                                                   HoraInicio=inicio, HoraFin=fin)
                                    grabar_asistencia = Asistencia(MatriculaCursos=alumno, Horario=horario1,
                                                                   Estado=asistencia, Fecha=fecha)
                                    grabar_asistencia.save()

                        for horario in horarios:
                            grabar_asistencia_docente = AsistenciaDocente(Horario=horario, TemaClase=tema_clase,
                                                                          FechaClase=fecha)
                            grabar_asistencia_docente.save()

                        mensaje = "Datos Guardados Correctamente"
                        links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                        return render_to_response("Docentes/Intranet/asistencia_registrada.html",
                                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = AsistenciaComunForm(initial={"Fecha": datetime.date.today()})

        return render_to_response("Docentes/Intranet/agregar_asistencia.html",
                                  {"form": form, "user": request.user, "alumnos": alumnos, "periodocurso": periodocurso,
                                   "horario": horarios[0], "existe": existe, "fecha": fecha, "tema_clase": tema_clase,
                                   "registrados": registrados, "no_registrado": no_registrado},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def modificar_asistencia1(request, id_periodo, fecha, dia, inicio, fin):
    if request.user.is_authenticated():
        asistentes = Asistencia.objects.filter(Fecha=fecha, Horario__Dia=dia, Horario__HoraInicio=inicio,
                                               Horario__HoraFin=fin,
                                               MatriculaCursos__PeriodoCurso__Periodo__id=id_periodo,
                                               MatriculaCursos__PeriodoCurso__Docente__id=request.user.id).order_by(
            'MatriculaCursos__MatriculaCiclo__Alumno__Carrera__Carrera',
            'MatriculaCursos__MatriculaCiclo__Alumno__ApellidoPaterno',
            'MatriculaCursos__MatriculaCiclo__Alumno__ApellidoMaterno',
            'MatriculaCursos__MatriculaCiclo__Alumno__Nombres')

        asistencia_docente = AsistenciaDocente.objects.filter(FechaClase=fecha,
                                                              Horario__PeriodoCurso__Periodo__id=id_periodo,
                                                              Horario__PeriodoCurso__Docente__id=request.user.id,
                                                              Horario__Dia=dia, Horario__HoraInicio=inicio,
                                                              Horario__HoraFin=fin)

        fecha_original = fecha
        if asistencia_docente.count() != 0 and asistentes.count() != 0:
            tema_clase = asistencia_docente[0].TemaClase
            periodocurso = asistentes[0].Horario.PeriodoCurso
            if periodocurso.EditarAsistencias is False:
                mensaje = "Registro de asistencias desactivada"
                links = "<a class='btn btn-primary btn-large' href='javascript:window.close()'>Cerrar</a>"
                return render_to_response("Docentes/Intranet/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
            hor = asistentes[0].Horario
        else:
            tema_clase = ''

        existe = False
        no_registrado = ''
        registrados = []
        if request.method == 'POST':
            form = AsistenciaComunForm(request.POST)
            if form.is_valid():
                fecha = form.cleaned_data['Fecha']
                tema_clase = form.cleaned_data['TemaClase']

                existe_asistencia = Asistencia.objects.filter(Fecha=fecha,
                                                              Horario__PeriodoCurso__Periodo__id=id_periodo,
                                                              Horario__Dia=dia, Horario__HoraInicio=inicio,
                                                              Horario__HoraFin=fin,
                                                              Horario__PeriodoCurso__Docente__id=request.user.id)
                if existe_asistencia.count() != 0 and fecha.strftime("%Y-%m-%d") != fecha_original:
                    existe = True
                else:

                    for asistente in asistentes:
                        validar_alumno = request.POST.get(str(asistente.id), False)
                        if validar_alumno is False:
                            if no_registrado == '':
                                no_registrado = asistente
                        else:
                            registrados.append([asistente.id, validar_alumno])
                    if no_registrado == '':
                        for asistente in asistentes:
                            asistencia = request.POST[str(asistente.id)]
                            horario = Horario.objects.get(PeriodoCurso=asistente.Horario.PeriodoCurso, Dia=dia,
                                                          HoraInicio=inicio, HoraFin=fin)
                            grabar_asistencia = Asistencia(id=asistente.id, MatriculaCursos=asistente.MatriculaCursos,
                                                           Horario=horario, Estado=asistencia, Fecha=fecha)
                            grabar_asistencia.save()

                        for asis in asistencia_docente:
                            grabar_asistencia_docente = AsistenciaDocente(id=asis.id, Horario=asis.Horario,
                                                                          FechaClase=fecha, TemaClase=tema_clase,
                                                                          Fecha=asis.Fecha)
                            grabar_asistencia_docente.save()

                        mensaje = "Datos Guardados Correctamente"
                        links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                        return render_to_response("Docentes/Intranet/asistencia_registrada.html",
                                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = AsistenciaComunForm(initial={"Fecha": fecha, "TemaClase": tema_clase})

        return render_to_response("Docentes/Intranet/editar_asistencia.html",
                                  {"form": form, "user": request.user, "alumnos": asistentes,
                                   "periodocurso": periodocurso, "fecha": fecha, "existe": existe,
                                   "tema_clase": tema_clase, "horario": hor, "registrados": registrados,
                                   "no_registrado": no_registrado}, context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def eliminar_asistencia1(request, id_periodo, fecha, dia, inicio, fin):
    if request.user.is_authenticated():
        asistentes = Asistencia.objects.filter(Fecha=fecha, Horario__Dia=dia, Horario__HoraInicio=inicio,
                                               Horario__HoraFin=fin,
                                               MatriculaCursos__PeriodoCurso__Periodo__id=id_periodo,
                                               MatriculaCursos__PeriodoCurso__Docente__id=request.user.id).order_by(
            'MatriculaCursos__MatriculaCiclo__Alumno__Carrera__Carrera',
            'MatriculaCursos__MatriculaCiclo__Alumno__ApellidoPaterno',
            'MatriculaCursos__MatriculaCiclo__Alumno__ApellidoMaterno',
            'MatriculaCursos__MatriculaCiclo__Alumno__Nombres')

        asistencia_docente = AsistenciaDocente.objects.filter(FechaClase=fecha,
                                                              Horario__PeriodoCurso__Periodo__id=id_periodo,
                                                              Horario__PeriodoCurso__Docente__id=request.user.id,
                                                              Horario__Dia=dia, Horario__HoraInicio=inicio,
                                                              Horario__HoraFin=fin)

        if asistentes.count() != 0 and asistencia_docente.count() != 0:
            horario_elegido = asistentes[0].Horario
            fecha = asistentes[0].Fecha
            periodocurso = asistentes[0].Horario.PeriodoCurso

            if periodocurso.EditarAsistencias is False:
                mensaje = "Registro de asistencias desactivada"
                links = "<a class='btn btn-primary btn-large' href='javascript:window.close()'>Cerrar</a>"
                return render_to_response("Docentes/Intranet/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            for asistente in asistentes:
                asistente.delete()
            for asi_docente in asistencia_docente:
                asi_docente.delete()

            mensaje = "Asistencia Eliminada Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/asistencia_eliminada.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("Docentes/Intranet/eliminar_asistencia.html",
                                  {"user": request.user, "periodocurso": periodocurso,
                                   "horario_elegido": horario_elegido, "fecha": fecha},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def silabo_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 's')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        mensaje = False
        if request.method == 'POST':
            form = SilaboForm(request.POST, request.FILES)
            if form.is_valid():
                silabo = form.cleaned_data['Silabo']
                if silabo != None:
                    from django.core.files.storage import default_storage
                    for per in periodo_cursos:
                        if per.Silabo:
                            default_storage.delete(periodocurso.Silabo.path)
                        per.Silabo = silabo
                        per.save()

                        periodocurso = per
                        mensaje = True
        else:
            form = SilaboForm()

        return render_to_response('Docentes/Intranet/silabo.html',
                                  {"var": var, "var1": var1, "form": form, "periodocurso": periodocurso,
                                   "horarios": horarios, "mensaje": mensaje, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../login/')


def material_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'r')

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)

        results = MaterialCurso.objects.filter(PeriodoCurso=periodocurso).order_by('-Fecha_creacion')

        paginator = Paginator(results, 100)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)

        return render_to_response('Docentes/Intranet/material_curso.html',
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "horarios": horarios,
                                   "results": results, "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_protect
def agregar_material_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        if request.method == 'POST':
            form = MaterialForm(request.POST, request.FILES)
            if form.is_valid():
                archivo = form.cleaned_data['Archivo']
                nombre = form.cleaned_data['Nombre']
                descripcion = form.cleaned_data['Descripcion']

                from django.core.files.storage import default_storage
                for per in periodo_cursos:
                    nuevo_material = MaterialCurso(PeriodoCurso=per, Nombre=nombre, Descripcion=descripcion,
                                                   Archivo=archivo)
                    nuevo_material.save()
                mensaje = "Material Guardado Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Docentes/Intranet/asistencia_registrada.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = MaterialForm()

        return render_to_response("Docentes/Intranet/agregar_material_curso.html",
                                  {"form": form, "user": request.user, "periodocurso": periodocurso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_material_curso(request, id_materialcurso):
    if request.user.is_authenticated():
        try:
            materialcurso = MaterialCurso.objects.get(id=id_materialcurso,
                                                      PeriodoCurso__Docente__user_ptr=request.user.id)
            periodocurso = materialcurso.PeriodoCurso
        except MaterialCurso.DoesNotExist:
            mensaje = "El material no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        if request.method == 'POST':
            form = MaterialForm(request.POST, request.FILES)
            if form.is_valid():
                archivo = form.cleaned_data['Archivo']
                nombre = form.cleaned_data['Nombre']
                descripcion = form.cleaned_data['Descripcion']

                from django.core.files.storage import default_storage
                for per in periodo_cursos:
                    material = MaterialCurso.objects.get(PeriodoCurso=per, Nombre=materialcurso.Nombre,
                                                         Descripcion=materialcurso.Descripcion,
                                                         Archivo=materialcurso.Archivo.name)
                    material.Nombre = nombre
                    material.Descripcion = descripcion
                    if material.Archivo:
                        default_storage.delete(materialcurso.Archivo.path)
                        material.Archivo = archivo
                        material.save()

                mensaje = "Material Guardado Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Docentes/Intranet/asistencia_registrada.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = MaterialForm(initial={"Nombre": materialcurso.Nombre, "Descripcion": materialcurso.Descripcion})

        return render_to_response("Docentes/Intranet/editar_material_curso.html",
                                  {"form": form, "user": request.user, "periodocurso": periodocurso,
                                   "materialcurso": materialcurso}, context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def eliminar_material_curso(request, id_materialcurso):
    if request.user.is_authenticated():
        try:
            materialcurso = MaterialCurso.objects.get(id=id_materialcurso,
                                                      PeriodoCurso__Docente__user_ptr=request.user.id)
            periodocurso = materialcurso.PeriodoCurso
        except MaterialCurso.DoesNotExist:
            mensaje = "El material no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        if request.method == 'POST':
            from django.core.files.storage import default_storage
            for per in periodo_cursos:
                material = MaterialCurso.objects.get(PeriodoCurso=per, Nombre=materialcurso.Nombre,
                                                     Descripcion=materialcurso.Descripcion,
                                                     Archivo=materialcurso.Archivo.name)
                material1 = MaterialCurso.objects.using('campus').get(PeriodoCurso=per, Nombre=materialcurso.Nombre,
                                                                      Descripcion=materialcurso.Descripcion,
                                                                      Archivo=materialcurso.Archivo.name)
                if material.Archivo:
                    default_storage.delete(materialcurso.Archivo.path)
                    material.delete()
                    material1.delete()

            mensaje = "Material Eliminado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/asistencia_eliminada.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("Docentes/Intranet/eliminar_materialcurso.html",
                                  {"user": request.user, "periodocurso": periodocurso, "materialcurso": materialcurso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>" \
                "Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def marterial_curso_essilabo(request, id_materialcurso):
    if request.user.is_authenticated():
        try:
            materialcurso = MaterialCurso.objects.get(id=id_materialcurso,
                                                      PeriodoCurso__Docente__user_ptr=request.user.id)
            periodocurso = materialcurso.PeriodoCurso
        except MaterialCurso.DoesNotExist:
            mensaje = "El material no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>" \
                    "Cerrar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)

        if request.method == 'POST':
            materiales = MaterialCurso.objects.filter(PeriodoCurso=periodocurso,
                                                      PeriodoCurso__Docente__user_ptr=request.user.id).exclude(
                id=id_materialcurso)
            for mater in materiales:
                mater.Issilabo = False
                mater.save()
            materiales1 = MaterialCurso.objects.using('campus').filter(PeriodoCurso=periodocurso,
                                                                       PeriodoCurso__Docente__user_ptr=request.user.id).exclude(
                id=id_materialcurso)
            for mater in materiales1:
                mater.Issilabo = False
                mater.save()

            material = MaterialCurso.objects.get(id=id_materialcurso)
            material1 = MaterialCurso.objects.using('campus').get(id=id_materialcurso)
            material.Issilabo = True
            material1.Issilabo = True
            material.save()
            material1.save()

            mensaje = "Material actualizado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Docentes/Intranet/asistencia_eliminada.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("Docentes/Intranet/materialcurso_essilabo.html",
                                  {"user": request.user, "periodocurso": periodocurso, "materialcurso": materialcurso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Acceso Denegado"
        links = "<a class='btn btn-primary btn-large' href='javascript:opener.location.reload();window.close()'>" \
                "Cerrar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def auxiliar_asistencias(request, id_curso):
    from xlrd import open_workbook
    from xlutils.copy import copy
    from xlwt import XFStyle, Borders, Alignment

    # rotacion de texto
    alignment_registros = Alignment()
    # alignment_registros.orie = Alignment.ORIENTATION_STACKED
    # alignment_registros.rota = Alignment.ROTATION_STACKED

    # estilos de celda registros
    borders_registros = Borders()
    borders_registros.left = Borders.THIN
    borders_registros.right = Borders.THIN
    borders_registros.top = Borders.THIN
    borders_registros.bottom = Borders.THIN

    style_registros = XFStyle()
    style_registros.borders = borders_registros

    style_registros1 = XFStyle()
    style_registros1.borders = borders_registros
    style_registros1.alignment = alignment_registros

    curso = Curso.objects.get(id=id_curso)
    matriculados = MatriculaCursos.objects.filter(PeriodoCurso__Curso=curso, MatriculaCiclo__Estado='Matriculado',
                                                  Convalidado=False).order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                                              'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                                              'MatriculaCiclo__Alumno__Nombres')
    codigo_curso = curso.Codigo
    nombre = curso.Nombre
    carr = curso.Carrera.Carrera
    cod = curso.Carrera.Codigo
    ciclo = curso.Ciclo
    if matriculados.count() != 0:
        docente = matriculados[0].PeriodoCurso.Docente.ApellidoPaterno + ' ' + matriculados[
            0].PeriodoCurso.Docente.ApellidoMaterno + ' ' + matriculados[0].PeriodoCurso.Docente.Nombres
        horarios = Horario.objects.filter(PeriodoCurso=matriculados[0].PeriodoCurso)
        fechas = []
        for horario in horarios:
            asistencia = Asistencia.objects.filter(Horario=horario).order_by('Fecha').values('Fecha').distinct()
            for asis in asistencia:
                if asis['Fecha'] not in fechas:
                    fechas.append(asis['Fecha'])
    else:
        mensaje = "No hay alumnmos matriculados"
        links = "<a class='btn btn-primary btn-large' href='../'>Atrás</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    fechas.reverse()
    fechas.sort()
    rb = open_workbook(settings.MEDIA_ROOT + u'formato_asis.xls', formatting_info=True)
    wb = copy(rb)
    ws = wb.get_sheet(0)
    ws1 = wb.get_sheet(1)

    ws.insert_bitmap(settings.MEDIA_ROOT + 'LogoExcel.bmp', 1, 0)

    ws.write(2, 2, carr)
    ws.write(4, 2, docente)
    ws.write(2, 14, ciclo)
    ws.write(4, 14, nombre)

    if matriculados.count() > 20:
        ws1.insert_bitmap(settings.MEDIA_ROOT + 'LogoExcel.bmp', 1, 0)
        ws1.write(2, 2, carr)
        ws1.write(4, 2, docente)
        ws1.write(2, 14, ciclo)
        ws1.write(4, 14, nombre)

    i = 8
    for m in matriculados[0:20]:
        codigo = m.MatriculaCiclo.Alumno.Codigo
        alumno = m.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + m.MatriculaCiclo.Alumno.ApellidoMaterno + ' ' + m.MatriculaCiclo.Alumno.Nombres
        carrera = m.MatriculaCiclo.Alumno.Carrera.Codigo
        ws.write(i, 1, alumno, style_registros)
        ws.write(i, 2, carrera, style_registros)
        k = 3
        for fecha in fechas:
            asistencia = Asistencia.objects.filter(MatriculaCursos=m, Fecha=fecha)
            if asistencia.count() != 0:
                ws.write(i, k, asistencia[0].Estado[0], style_registros)
            k += 1
        i += 1

    k = 3
    for fecha in fechas:
        ws.write(7, k, fecha.strftime("%d-%m-%Y"), style_registros1)
        ws1.write(7, k, fecha.strftime("%d-%m-%Y"), style_registros1)
        k += 1

    if matriculados.count() > 20:
        j = 8
        for m in matriculados[21:]:
            codigo = m.MatriculaCiclo.Alumno.Codigo
            alumno = m.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + m.MatriculaCiclo.Alumno.ApellidoMaterno + ' ' + m.MatriculaCiclo.Alumno.Nombres
            carrera = m.MatriculaCiclo.Alumno.Carrera.Codigo
            ws1.write(j, 1, alumno, style_registros)
            ws1.write(j, 2, carrera, style_registros)
            k = 3
            for fecha in fechas:
                asistencia = Asistencia.objects.filter(MatriculaCursos=m, Fecha=fecha)
                if asistencia.count() != 0:
                    ws1.write(j, k, asistencia[0].Estado[0], style_registros)
                k += 1
            j += 1
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s_%s.xls' % (cod, codigo_curso)
    wb.save()
    return response


def auxiliar_notas(request, id_curso):
    from xlrd import open_workbook
    from xlutils.copy import copy
    from xlwt import XFStyle, Borders

    # estilos de celda registros
    borders_registros = Borders()
    borders_registros.left = Borders.THIN
    borders_registros.right = Borders.THIN
    borders_registros.top = Borders.THIN
    borders_registros.bottom = Borders.THIN

    style_registros = XFStyle()
    style_registros.borders = borders_registros

    curso = Curso.objects.get(id=id_curso)
    matriculados = MatriculaCursos.objects.filter(PeriodoCurso__Curso=curso, MatriculaCiclo__Estado='Matriculado',
                                                  Convalidado=False).order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                                              'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                                              'MatriculaCiclo__Alumno__Nombres')
    codigo_curso = curso.Codigo
    nombre = curso.Nombre
    carr = curso.Carrera.Carrera
    cod = curso.Carrera.Codigo
    ciclo = curso.Ciclo
    docente = None
    if matriculados.count() != 0:
        docente = matriculados[0].PeriodoCurso.Docente.ApellidoPaterno + ' ' + matriculados[
            0].PeriodoCurso.Docente.ApellidoMaterno + ' ' + matriculados[0].PeriodoCurso.Docente.Nombres
    rb = open_workbook(settings.MEDIA_ROOT + u'formato_notas.xls', formatting_info=True)
    wb = copy(rb)
    ws = wb.get_sheet(0)
    # ws1 = wb.get_sheet(1)

    ws.insert_bitmap(settings.MEDIA_ROOT + 'LogoExcel.bmp', 0, 0)

    ws.write(2, 4, carr)
    ws.write(4, 0, "Docente: " + docente)
    ws.write(3, 4, ciclo)
    ws.write(4, 4, nombre)

    i = 7
    for m in matriculados[0:60]:
        codigo = m.MatriculaCiclo.Alumno.Codigo
        alumno = m.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + m.MatriculaCiclo.Alumno.ApellidoMaterno + ' ' + m.MatriculaCiclo.Alumno.Nombres
        carrera = m.MatriculaCiclo.Alumno.Carrera.Codigo
        ws.write(i, 1, alumno, style_registros)
        ws.write(i, 2, carrera, style_registros)
        j = 3
        for nota in m.notasalumno_set.order_by('-Nota__Nivel', 'Nota__Orden'):
            ws.write(i, j, nota.Valor, style_registros)
            j += 1
        i += 1

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s_%s.xls' % (cod, codigo_curso)
    wb.save()
    return response


class MensajeForm(ModelForm):
    class Meta:
        model = Mensaje
        exclude = ['Emisor']


def redactar_mensaje(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'rm')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        matriculados = []
        for per in periodo_cursos:
            mat = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False).order_by(
                'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')
            for m in mat:
                matriculados.append(m)

        if request.method == "POST":
            form = MensajeForm(request.POST)
            destinatarios = request.POST.getlist('destinatarios_mensaje')

            if form.is_valid():
                asunto = form.cleaned_data['Asunto']
                contenido = form.cleaned_data['Contenido']
                nuevo_mensaje = Mensaje(Emisor=request.user, Asunto=asunto, Contenido=contenido)
                nuevo_mensaje.save()
                for d in destinatarios:
                    nuevo_destinatario = Destinatarios(Mensaje=nuevo_mensaje, Receptor_id=d)
                    nuevo_destinatario.save()
                return HttpResponseRedirect('../../enviados/%s/?q=l&a=me' % periodocurso.id)
        else:
            form = MensajeForm()

        return render_to_response("Docentes/Intranet/redactar_mensaje.html",
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "horarios": horarios,
                                   "matriculados": matriculados, "form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../login/')


def mensajes_enviados(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 'me')
        query = request.GET.get('query', '')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        horarios = Horario.objects.filter(PeriodoCurso=periodocurso)
        periodo_cursos = []
        for horario in horarios:
            horario1 = Horario.objects.filter(PeriodoCurso__Periodo=horario.PeriodoCurso.Periodo, Dia=horario.Dia,
                                              HoraInicio=horario.HoraInicio, HoraFin=horario.HoraFin,
                                              Seccion=horario.Seccion)
            for hor in horario1:
                if hor.PeriodoCurso not in periodo_cursos:
                    periodo_cursos.append(hor.PeriodoCurso)
        matriculados = []
        for per in periodo_cursos:
            mat = MatriculaCursos.objects.filter(PeriodoCurso=per, Estado=True, Convalidado=False).order_by(
                'MatriculaCiclo__Alumno__Carrera__Carrera', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')
            for m in mat:
                matriculados.append(m.MatriculaCiclo.Alumno.id)
        mensajes_enviados = []

        if query:
            qset = (
                    Q(Asunto__icontains=query) |
                    Q(Contenido__icontains=query)
            )
            enviados = Mensaje.objects.filter(Emisor=request.user).filter(qset).order_by('-FechaCreacion')
        else:
            enviados = Mensaje.objects.filter(Emisor=request.user).order_by('-FechaCreacion')

        if len(matriculados) != 0:
            for env in enviados:
                for dest in env.destinatarios_set.all():
                    if dest.Receptor.id in matriculados:
                        mensajes_enviados.append(env)
                        break

        n_mensajes = len(mensajes_enviados)
        paginator = Paginator(mensajes_enviados, 5)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)
        return render_to_response("Docentes/Intranet/mensajes_enviados.html",
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "horarios": horarios,
                                   "results": results, "paginator": paginator, "n_mensajes": n_mensajes, "query": query,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../login/')


def mensajes_recibidos(request):
    if request.user.is_authenticated():
        var = request.GET.get('re', 're')
        query = request.GET.get('query', '')
        mensajes_recibidos = []

        if query:
            qset = (
                    Q(Mensaje__Asunto__icontains=query) |
                    Q(Mensaje__Contenido__icontains=query)
            )
            recibidos = Destinatarios.objects.filter(Receptor=request.user).filter(qset).order_by('-FechaCreacion')
        else:
            recibidos = Destinatarios.objects.filter(Receptor=request.user).order_by('-FechaCreacion')

        n_mensajes = recibidos.count()
        paginator = Paginator(recibidos, 10)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)
        return render_to_response("Docentes/Intranet/mensajes_recibidos.html",
                                  {"var": var, "results": results, "paginator": paginator, "n_mensajes": n_mensajes,
                                   "query": query, "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../login/')


@csrf_protect
def ver_silabo_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 's')
        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        try:
            silabo = SilaboCurso.objects.get(PeriodoCurso=periodocurso)
        except SilaboCurso.DoesNotExist:
            silabo = None
        return render_to_response('Docentes/Intranet/silabo_curso.html',
                                  {"var": var, "var1": var1, "periodocurso": periodocurso, "silabo": silabo,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../login/')


@csrf_protect
def descargar_silabo_curso(request, id_periodocurso):
    # if request.user.is_authenticated():
    var = request.GET.get('q', 'l')
    var1 = request.GET.get('a', 's')
    try:
        # periodocurso = PeriodoCurso.objects.get(id = id_periodocurso,Docente__user_ptr = request.user.id)
        periodocurso = PeriodoCurso.objects.get(id=id_periodocurso)
    except PeriodoCurso.DoesNotExist:
        mensaje = "El curso no existe o no tiene permiso de entrada al curso"
        links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
        return render_to_response("Docentes/Intranet/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    try:
        silabo = SilaboCurso.objects.get(PeriodoCurso=periodocurso)
    except SilaboCurso.DoesNotExist:
        silabo = None

    html = render_to_string('Docentes/Intranet/template_silabo_curso.html',
                            {"var": var, "var1": var1, "periodocurso": periodocurso, "silabo": silabo,
                             "user": request.user}, context_instance=RequestContext(request))
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('Error al generar el PDF: %s' % cgi.escape(html))
    # else:
    #     return HttpResponseRedirect('../../../../login/')


def editar_silabo_curso(request, id_periodocurso):
    if request.user.is_authenticated():
        var = request.GET.get('q', 'l')
        var1 = request.GET.get('a', 's')
        verifica_post = False

        try:
            periodocurso = PeriodoCurso.objects.get(id=id_periodocurso, Docente__user_ptr=request.user.id)
        except PeriodoCurso.DoesNotExist:
            mensaje = "El curso no existe o no tiene permiso de entrada al curso"
            links = "<a class='btn btn-primary btn-large' href='../../'>Regresar</a>"
            return render_to_response("Docentes/Intranet/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        try:
            silabo = SilaboCurso.objects.get(PeriodoCurso__id=periodocurso.id)
        except SilaboCurso.DoesNotExist:
            nuevo_silabo = SilaboCurso(PeriodoCurso=periodocurso)
            nuevo_silabo.save()
            silabo = nuevo_silabo

        if request.method == "POST":
            verifica_post = True
            form = SilaboCursoForm(request.POST)
            if form.is_valid():
                sumilla = form.cleaned_data['Sumilla']
                comp_general = form.cleaned_data['Comp_general']
                comp_especifica1 = form.cleaned_data['Comp_especifica1']
                comp_especifica2 = form.cleaned_data['Comp_especifica2']
                comp_especifica3 = form.cleaned_data['Comp_especifica3']
                comp_especifica4 = form.cleaned_data['Comp_especifica4']
                unidad_didactica1 = form.cleaned_data['Unidad_didactica1']
                unidad_didactica2 = form.cleaned_data['Unidad_didactica2']
                unidad_didactica3 = form.cleaned_data['Unidad_didactica3']
                unidad_didactica4 = form.cleaned_data['Unidad_didactica4']
                estrategia_metodologica = form.cleaned_data['Estrategia_metodologica']
                material_educativo = form.cleaned_data['Material_educativo']
                evaluacion_trabajo = form.cleaned_data['Evaluacion_trabajo']
                evaluacion_conceptual = form.cleaned_data['Evaluacion_conceptual']
                evaluacion_procedimental = form.cleaned_data['Evaluacion_procedimental']
                evaluacion_actitudinal = form.cleaned_data['Evaluacion_actitudinal']
                requisito_aprobacion = form.cleaned_data['Requisito_aprobacion']
                biblio_obligatoria = form.cleaned_data['Biblio_obligatoria']
                biblio_consulta = form.cleaned_data['Biblio_consulta']
                if silabo:
                    silabo.Sumilla = sumilla
                    silabo.Comp_general = comp_general
                    silabo.Comp_especifica1 = comp_especifica1
                    silabo.Comp_especifica2 = comp_especifica2
                    silabo.Comp_especifica3 = comp_especifica3
                    silabo.Comp_especifica4 = comp_especifica4
                    silabo.Unidad_didactica1 = unidad_didactica1
                    silabo.Unidad_didactica2 = unidad_didactica2
                    silabo.Unidad_didactica3 = unidad_didactica3
                    silabo.Unidad_didactica4 = unidad_didactica4
                    silabo.Estrategia_metodologica = estrategia_metodologica
                    silabo.Material_educativo = material_educativo
                    silabo.Evaluacion_trabajo = evaluacion_trabajo
                    silabo.Evaluacion_conceptual = evaluacion_conceptual
                    silabo.Evaluacion_procedimental = evaluacion_procedimental
                    silabo.Evaluacion_actitudinal = evaluacion_actitudinal
                    silabo.Requisito_aprobacion = requisito_aprobacion
                    silabo.Biblio_obligatoria = biblio_obligatoria
                    silabo.Biblio_consulta = biblio_consulta
                    silabo.save()
                else:
                    nuevo_silabo = SilaboCurso(PeriodoCurso=periodocurso, Sumilla=sumilla, Comp_general=comp_general,
                                               Comp_especifica1=comp_especifica1, Comp_especifica2=comp_especifica2,
                                               Comp_especifica3=comp_especifica3, Comp_especifica4=comp_especifica4,
                                               Unidad_didactica1=unidad_didactica1, Unidad_didactica2=unidad_didactica2,
                                               Unidad_didactica3=unidad_didactica3, Unidad_didactica4=unidad_didactica4,
                                               Estrategia_metodologica=estrategia_metodologica,
                                               Material_educativo=material_educativo,
                                               Evaluacion_trabajo=evaluacion_trabajo,
                                               Evaluacion_conceptual=evaluacion_conceptual,
                                               Evaluacion_procedimental=evaluacion_procedimental,
                                               Evaluacion_actitudinal=evaluacion_actitudinal,
                                               Requisito_aprobacion=requisito_aprobacion,
                                               Biblio_obligatoria=biblio_obligatoria, Biblio_consulta=biblio_consulta)
                    nuevo_silabo.save()

                return HttpResponseRedirect('../?q=l&a=s')
        else:
            form = SilaboCursoForm()

        return render_to_response("Docentes/Intranet/editar_silabo_curso.html",
                                  {"var": var, "var1": var1, "verifica_post": verifica_post, "silabo": silabo,
                                   "form": form, "periodocurso": periodocurso, "user": request.user},
                                  context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('../../../login/')
