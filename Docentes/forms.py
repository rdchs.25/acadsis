# -*- coding: utf-8 -*-
import datetime
import hashlib

from django import forms
from django.contrib.auth.models import User
from django.forms.utils import ErrorList

from Alumnos.widgets import DateTimeWidget
from Cursos.models import SilaboCurso
from Docentes.models import Docente, MoodleUser


class DocenteAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DocenteAdminForm, self).__init__(*args, **kwargs)
        dep = ''
        prov = ''
        dist = ''
        if self.instance.Departamento:
            dep = self.instance.Departamento
        if self.instance.Provincia:
            prov = self.instance.Provincia
        if self.instance.Distrito:
            dist = self.instance.Distrito
        self.fields['selected_lugar'].initial = '%s|%s|%s' % (dep, prov, dist)

    Nacimiento = forms.DateField(widget=DateTimeWidget, required=False)
    Departamento = forms.CharField(widget=forms.Select)
    Provincia = forms.CharField(widget=forms.Select)
    Distrito = forms.CharField(widget=forms.Select)
    selected_lugar = forms.CharField(widget=forms.HiddenInput, required=False, label="")

    class Meta:
        model = Docente
        fields = '__all__'

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Docentes/frmdocente.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class AsistenciaComunForm(forms.Form):
    Fecha = forms.DateField(initial=datetime.date.today, widget=DateTimeWidget, label="Fecha")
    TemaClase = forms.CharField(widget=forms.Textarea, required=True, label="Tema de Clase")

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class DocenteForm(forms.ModelForm):
    Departamento = forms.CharField(widget=forms.Select)
    Provincia = forms.CharField(widget=forms.Select)
    Distrito = forms.CharField(widget=forms.Select)

    def clean_HojaVida(self):
        archivo = self.cleaned_data['HojaVida']
        if archivo:
            if archivo.content_type != 'application/pdf':
                raise forms.ValidationError("Debe ingresar un archivo en formato pdf")
        return archivo

    class Meta:
        model = Docente
        exclude = ('username', 'password', 'last_login', 'date_joined', 'Email', 'Estado')


class SilaboForm(forms.Form):
    Silabo = forms.FileField()

    def clean(self):
        cleaned_data = self.cleaned_data
        silabo = cleaned_data.get("Silabo")

        if silabo:
            if silabo.content_type != 'application/pdf':
                msg = u'Debe ingresar un archivo en formato pdf'
                self._errors["Silabo"] = ErrorList([msg])
                del cleaned_data["Silabo"]
        return cleaned_data


class MaterialForm(forms.Form):
    Nombre = forms.CharField(max_length=100, required=True, label="Nombre")
    Descripcion = forms.CharField(widget=forms.Textarea, required=True)
    Archivo = forms.FileField()

    def clean(self):
        cleaned_data = self.cleaned_data
        archivo = cleaned_data.get("Archivo")
        nombre = cleaned_data.get("Nombre")
        descripcion = cleaned_data.get("Descripcion")
        valid_conten_type = ['application/x-rar', 'application/zip', 'application/rar']
        exts = ['rar', 'zip']

        if archivo and nombre and descripcion:
            ext = archivo.name.split('.')[-1]
            if not ext in exts:
                msg = u'Tipo de archivo no permitido, %s' % ext
                self._errors["Archivo"] = ErrorList([msg])
                del cleaned_data["Archivo"]
            elif archivo.size > 7340032:
                msg = u'Debe ingresar archivos menores a 7 MB'
                self._errors["Archivo"] = ErrorList([msg])
                del cleaned_data["Archivo"]
            elif archivo.size < 0:
                msg = u'Campo obligatorio'
                self._errors["Archivo"] = ErrorList([msg])
                del cleaned_data["Archivo"]
        return cleaned_data


class SilaboCursoForm(forms.ModelForm):
    class Meta:
        model = SilaboCurso
        fields = '__all__'
        # exclude = ('username','password','last_login','date_joined','Email','Estado')


class LoginFormIntranet(forms.Form):
    Usuario = forms.CharField(label="Usuario", required=True)
    Password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")

        if user and passwd:
            try:
                validar_usuario = MoodleUser.objects.using('moodle').get(username=user)
                usuario_udl = User.objects.get(username=user)

                grupos = usuario_udl.groups.all()
                docente = False

                for grupo in grupos:
                    if grupo.name == "Docentes UDL":
                        docente = True
                        break

                if docente:
                    # if usuario_udl.check_password(passwd) == False:
                    #    msg = u'Contraseña incorrecta'
                    #    self._errors["Password"] = ErrorList([msg])
                    #    del cleaned_data["Password"]

                    try:
                        usuario = MoodleUser.objects.using('moodle').get(username=user,
                                                                         password=hashlib.md5(passwd).hexdigest())
                        usuario_udl.set_password(passwd)
                        usuario_udl.save()
                    except MoodleUser.DoesNotExist:
                        msg = u'Usuario Incorrecto'
                        msg1 = u'Contraseña Incorrecta'
                        self._errors["Usuario"] = ErrorList([msg])
                        self._errors["Password"] = ErrorList([msg1])
                        del cleaned_data["Usuario"]
                        # del cleaned_data["Password"]
                else:
                    msg = u'No eres Docente'
                    self._errors["Usuario"] = ErrorList([msg])
                    del cleaned_data["Usuario"]

            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]

        return cleaned_data


class ChangePasswordForm(forms.Form):
    OldPassword = forms.CharField(widget=forms.PasswordInput, label="Contraseña Antigua", required=True)
    NewPassword = forms.CharField(widget=forms.PasswordInput, label="Nueva Contraseña", required=True)
    RepeatPassword = forms.CharField(widget=forms.PasswordInput, label="Repetir Contraseña", required=True)
    Usuario = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    def clean(self):
        cleaned_data = self.cleaned_data
        oldpasswd = cleaned_data.get("OldPassword")
        newpasswd = cleaned_data.get("NewPassword")
        repeatpasswd = cleaned_data.get("RepeatPassword")
        usuario = cleaned_data.get("Usuario")

        if oldpasswd and newpasswd and repeatpasswd and usuario:
            try:
                usuario_udl = User.objects.get(username=usuario)
                usuario_moodle = MoodleUser.objects.using('moodle').get(username=usuario)
                if usuario_udl.check_password(oldpasswd) is True:
                    if newpasswd == repeatpasswd:
                        usuario_udl.set_password(newpasswd)
                        usuario_moodle.password = hashlib.md5(newpasswd).hexdigest()
                        usuario_udl.save()
                        usuario_moodle.save()
                    else:
                        msg = u'Las contraseñas no coinciden'
                        self._errors["RepeatPassword"] = ErrorList([msg])
                        del cleaned_data["RepeatPassword"]
                else:
                    msg = u'Contraseña Incorrecta'
                    self._errors["OldPassword"] = ErrorList([msg])
                    del cleaned_data["OldPassword"]
            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto, intento de acceso no autorizado'
                self._errors["OldPassword"] = ErrorList([msg])
                del cleaned_data["OldPassword"]
        return cleaned_data
