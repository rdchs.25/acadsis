# -*- coding: utf-8 -*-
import random
import string

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from Alumnos.models import GENDER_CHOICES, CIVIL_CHOICES
from UDL.moodle_webservice import moodle_webservices


def obtenerpath_foto_docente(instance, filename):
    ext = filename.split('.')[-1]
    return u'docentes/%s/foto-%s.%s' % (instance.username, instance.username, ext)


def obtenerpath_hojavida_docente(instance, filename):
    ext = filename.split('.')[-1]
    return u'docentes/%s/hojavida-%s.%s' % (instance.username, instance.username, ext)


class Docente(User):
    Nombres = models.CharField(max_length=50)
    ApellidoPaterno = models.CharField("Ape. Pat.", max_length=50)
    ApellidoMaterno = models.CharField("Ape. Mat.", max_length=50)
    Email = models.EmailField("E-mail", null=True, blank=True)
    Nacimiento = models.DateField("Nacimiento", null=True, blank=True)
    Sexo = models.CharField(max_length=1, choices=GENDER_CHOICES, default='M')
    Dni = models.CharField("DNI", max_length=20, null=True, blank=True)
    Direccion = models.CharField("Dirección", max_length=100, null=True, blank=True)
    Departamento = models.CharField("Departamento", max_length=50, null=True, blank=True)
    Provincia = models.CharField("Provincia", max_length=50, null=True, blank=True)
    Distrito = models.CharField("Distrito", max_length=50, null=True, blank=True)
    EstadoCivil = models.CharField("Estado Civil", max_length=20, choices=CIVIL_CHOICES, default='Soltero')
    Email2 = models.EmailField("E-mail 2", null=True, blank=True)
    Telefono = models.CharField("Teléfono", max_length=20, null=True, blank=True)
    Celular = models.CharField("Celular", max_length=20, null=True, blank=True)
    Perfil = models.TextField("Perfil profesional", blank=True, null=True)
    Titulo = models.CharField("Título", max_length=100, null=True, blank=True)
    Grado = models.CharField("Grado Máximo", max_length=100, null=True, blank=True)
    Estado = models.BooleanField("Activado", default=True)
    Foto = models.ImageField(upload_to=obtenerpath_foto_docente, blank=True, null=True, verbose_name="Foto")
    HojaVida = models.FileField(upload_to=obtenerpath_hojavida_docente, blank=True, null=True,
                                verbose_name="Hoja de Vida")
    Moodleid = models.PositiveIntegerField("Moodle id", editable=False, null=True)
    Huella = models.BinaryField(null=True)

    def __unicode__(self):
        return u'%s %s %s' % (self.ApellidoPaterno, self.ApellidoMaterno, self.Nombres)

    def usuario(self):
        return self.username

    def add_grupo_docente(self):
        from django.contrib.auth.models import Group
        grupo_docentes = Group.objects.filter(name='Docentes UDL').count()
        if grupo_docentes == 0:
            crear_grupo = Group(name='Docentes UDL')
            crear_grupo.save()
            ultimo_grupo_creado = Group.objects.order_by('-id')[0]
            self.groups.add(ultimo_grupo_creado.id)
        else:
            grupo_docentes = Group.objects.get(name='Docentes UDL')
            self.groups.add(grupo_docentes.id)

    def save(self, **kwargs):
        if not self.user_ptr_id:
            nombre = self.Nombres
            apellido = self.ApellidoPaterno
            usuario = str(nombre[0]) + apellido
            contrasena = usuario.lower()
            self.username = usuario.lower()
            self.set_password(contrasena)
            self.first_name = self.Nombres
            self.last_name = self.ApellidoPaterno + " " + self.ApellidoMaterno
            self.email = self.Email
            super(Docente, self).save()
            return self.add_grupo_docente()
        else:
            if settings.ACTIVAR_SERVICIOS_FACTURACION == 1:
                if self.Email is not None and self.Email != "":
                    if self.Moodleid is None:
                        datos_usuario = {
                            'users[0][username]': self.username.lower(),
                            'users[0][password]': "udl" + self.username,
                            'users[0][firstname]': self.Nombres.encode('utf-8'),
                            'users[0][lastname]': (self.ApellidoPaterno + " " + self.ApellidoMaterno).encode('utf-8'),
                            'users[0][email]': self.Email,
                            'users[0][auth]': 'manual',
                            'users[0][country]': 'PE',
                        }
                        rest_datos_usuario = moodle_webservices("core_user_create_users", datos_usuario)
                        self.Moodleid = rest_datos_usuario[0]['id']
                    else:
                        datos_usuario = {
                            'users[0][username]': self.username.lower(),
                            'users[0][password]': "udl" + self.username,
                            'users[0][firstname]': self.Nombres.encode('utf-8'),
                            'users[0][lastname]': (self.ApellidoPaterno + " " + self.ApellidoMaterno).encode('utf-8'),
                            'users[0][email]': self.Email,
                            'users[0][auth]': 'manual',
                            'users[0][country]': 'PE',
                            'users[0][id]': self.Moodleid,
                        }
                        moodle_webservices("core_user_update_users", datos_usuario)
            self.first_name = self.Nombres
            self.last_name = self.ApellidoPaterno + " " + self.ApellidoMaterno
            self.email = self.Email
            super(Docente, self).save()
            return self.add_grupo_docente()

    class Meta:
        verbose_name = "Docente UDL"
        verbose_name_plural = "Docentes UDL"
        ordering = ['ApellidoPaterno', 'ApellidoMaterno', 'Nombres']


class MoodleUser(models.Model):
    auth = models.CharField(max_length=60)
    confirmed = models.IntegerField()
    policyagreed = models.IntegerField()
    deleted = models.IntegerField()
    mnethostid = models.BigIntegerField(unique=True)
    username = models.CharField(max_length=300)
    password = models.CharField(max_length=96)
    idnumber = models.CharField(max_length=765)
    firstname = models.CharField(max_length=300)
    lastname = models.CharField(max_length=300)
    email = models.CharField(max_length=300)
    emailstop = models.IntegerField()
    icq = models.CharField(max_length=45)
    skype = models.CharField(max_length=150)
    yahoo = models.CharField(max_length=150)
    aim = models.CharField(max_length=150)
    msn = models.CharField(max_length=150)
    phone1 = models.CharField(max_length=60)
    phone2 = models.CharField(max_length=60)
    institution = models.CharField(max_length=120)
    department = models.CharField(max_length=90)
    address = models.CharField(max_length=210)
    city = models.CharField(max_length=60)
    country = models.CharField(max_length=6)
    lang = models.CharField(max_length=90)
    theme = models.CharField(max_length=150)
    timezone = models.CharField(max_length=300)
    firstaccess = models.BigIntegerField()
    lastaccess = models.BigIntegerField()
    lastlogin = models.BigIntegerField()
    currentlogin = models.BigIntegerField()
    lastip = models.CharField(max_length=45)
    secret = models.CharField(max_length=45)
    picture = models.IntegerField()
    url = models.CharField(max_length=765)
    description = models.TextField(blank=True)
    mailformat = models.IntegerField()
    maildigest = models.IntegerField()
    maildisplay = models.IntegerField()
    htmleditor = models.IntegerField()
    ajax = models.IntegerField()
    autosubscribe = models.IntegerField()
    trackforums = models.IntegerField()
    timemodified = models.BigIntegerField()
    trustbitmask = models.BigIntegerField()
    imagealt = models.CharField(max_length=765, blank=True)
    screenreader = models.IntegerField()

    class Meta:
        db_table = u'mdl_user'


rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u',
       'Ñ': 'N', 'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U',
       'ä': 'a', 'ë': 'e', 'ï': 'i', 'ö': 'o', 'ü': 'u',
       'Ä': 'A', 'Ë': 'E', 'Ï': 'I', 'Ö': 'O', 'Ü': 'U',
       '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ', '!': ' ', '¿': ' ',
       '?': ' ', '^': ' ', '"': ' ', '-': ' ', '_': ' ', ' ': ''}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generatecorreo(apellidopat, apellitomat, nombre, intento):
    correo = ""
    if intento == 1:
        correo = apellidopat[:1] + apellitomat + nombre[:1] + "@udl.edu.pe"
        alumnos = Docente.objects.filter(EmailInstitucional=correo)
        if alumnos.count() > 0:
            generatecorreo(apellidopat, apellitomat, nombre, 2)
        else:
            usuarios = User.objects.filter(email=correo)
            if usuarios.count() > 0:
                generatecorreo(apellidopat, apellitomat, nombre, 2)
    elif intento == 2:
        correo = apellidopat[:1] + apellitomat + nombre[:2] + "@udl.edu.pe"
        alumnos = Docente.objects.filter(email=correo)
        if alumnos.count() > 0:
            generatecorreo(apellidopat, apellitomat, nombre, 3)
        else:
            usuarios = User.objects.filter(email=correo)
            if usuarios.count() > 0:
                generatecorreo(apellidopat, apellitomat, nombre, 3)
    elif intento == 3:
        correo = apellidopat[:1] + apellitomat + nombre[:3] + "@udl.edu.pe"
        alumnos = Docente.objects.filter(email=correo)
        if alumnos.count() > 0:
            correo = "intentar de nuevo"
        else:
            usuarios = User.objects.filter(email=correo)
            if usuarios.count() > 0:
                correo = "intentar de nuevo"
    correo = replace_all(correo.encode('utf8'), rep)
    return correo.lower()
