from django.contrib import admin

from Docentes.forms import DocenteAdminForm
from Docentes.models import Docente


class DocenteAdmin(admin.ModelAdmin):
    form = DocenteAdminForm
    fieldsets = (
        ("Datos del Docente", {'fields': (
            'Nombres', 'ApellidoPaterno', 'ApellidoMaterno', 'Email', 'Foto', 'HojaVida', 'Nacimiento', 'Sexo', 'Dni',
            'Direccion', 'Departamento', 'Provincia', 'Distrito', 'EstadoCivil', 'Email2', 'Telefono', 'Celular',
            'Perfil',
            'Titulo', 'Grado', 'Estado', 'selected_lugar')}),
    )
    list_display = ('usuario', '__unicode__', 'Email', 'Dni', 'Estado')
    search_fields = ('Nombres', 'ApellidoPaterno', 'ApellidoMaterno',)
    list_filter = ('Estado',)
    radio_fields = {"Sexo": admin.HORIZONTAL}
    actions = ['reset_password', 'desactivar_docente', 'activar_docente', 'listado_excel']

    def reset_password(self, request, queryset):
        for docente in queryset:
            docente.set_password(docente.username)
            docente.save()

    reset_password.short_description = "Resetear Password"

    def desactivar_docente(self, request, queryset):
        for docente in queryset:
            docente.Estado = False
            docente.save()

    desactivar_docente.short_description = "Desactivar Docente"

    def activar_docente(self, request, queryset):
        for docente in queryset:
            docente.Estado = True
            docente.save()

    activar_docente.short_description = "Activar Docente"

    def listado_excel(self, request, queryset):
        from Docentes.views import docentes_excel
        docentes = []
        for q in queryset:
            docentes.append(q)
        return docentes_excel(docentes)

    listado_excel.short_description = "Listado de docentes seleccionados en Excel"


admin.site.register(Docente, DocenteAdmin)
