# -*- coding: utf-8 -*-
# Avoid shadowing the login() view below.

from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect

from Docentes.forms import LoginFormIntranet, ChangePasswordForm


@csrf_protect
@never_cache
def login_intranet_docentes(request):
    if request.method == 'POST':
        form = LoginFormIntranet(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['Usuario']
            passwd = form.cleaned_data['Password']
            user = authenticate(username=usuario, password=passwd)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect('../profile/')
    else:
        form = LoginFormIntranet()

    return render_to_response("Docentes/Intranet/login.html", {"form": form}, context_instance=RequestContext(request))


@csrf_protect
def logout_intranet_docentes(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('/docente/login/')
    else:
        return HttpResponseRedirect('/docente/login/')


@csrf_protect
def cambiar_passwd(request):
    if request.user.is_authenticated():
        var = request.GET.get('q', '')
        if request.method == 'POST':
            form = ChangePasswordForm(request.POST)
            if form.is_valid():
                return render_to_response("Docentes/Intranet/cambiar_passwd_successful.html")
        else:
            form = ChangePasswordForm(initial={'Usuario': request.user.username})

        return render_to_response("Docentes/Intranet/cambiar_passwd.html",
                                  {"form": form, "var": var, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/docente/login/')
