# -*- coding: utf-8 -*-
from django.conf.urls import url

from Docentes.Autenticacion import login_intranet_docentes
from Docentes.nuevo_intranet import profile_docente, datos_personales_docente, editar_datos_personales_docente, \
    cursos_disponibles, index_intranet_curso, listado_matriculados_curso, listado_matriculados_excel, registrar_notas, \
    guardar_notas, registrar_formula, master_subgrid_notas_nueva, master_subgrid_notas1_nueva, \
    master_subgrid_notas2_nueva, master_subgrid_notas3_nueva, listado_asistencias, agregar_asistencia1, \
    pdf_asistencia_comun, silabo_curso, ver_silabo_curso, editar_silabo_curso, descargar_silabo_curso, material_curso, \
    agregar_material_curso, editar_material_curso, eliminar_material_curso, marterial_curso_essilabo, \
    auxiliar_asistencias, auxiliar_notas, redactar_mensaje, mensajes_enviados, mensajes_recibidos, evaluacion_docente, \
    evaluacion_docente_descarga, labor_docente, modificar_asistencia1, eliminar_asistencia1
from Notas.subgrid_notas import ver_subgrid_notas, ver_subgrid_notas1, ver_subgrid_notas2, ver_subgrid_notas3
from Notas.views import exportar_notas_excel1

app_name = 'Docentes'

urlpatterns = [
    url(r'^login/$', login_intranet_docentes),
    url(r'^profile/$', profile_docente),
    url(r'^profile/datos_personales/$', datos_personales_docente),
    url(r'^profile/datos_personales/editar/$', editar_datos_personales_docente),
    url(r'^profile/cursos_disponibles/$', cursos_disponibles),
    url(r'^profile/cursos_disponibles/curso/(\d+)/$', index_intranet_curso),
    url(r'^profile/cursos_disponibles/matriculados/(\d+)/$', listado_matriculados_curso),
    url(r'^profile/cursos_disponibles/matriculados/(\d+)/reporte_excel/$', listado_matriculados_excel),
    url(r'^profile/Notas/(\d+)/$', registrar_notas),
    url(r'^profile/Notas/(\d+)/registrar/$', guardar_notas),
    url(r'^profile/Formula/(\d+)/$', registrar_formula),
    url(r'^profile/subgrid/vernotas/$', ver_subgrid_notas),
    url(r'^profile/subgrid/vernotas1/$', ver_subgrid_notas1),
    url(r'^profile/subgrid/vernotas2/$', ver_subgrid_notas2),
    url(r'^profile/subgrid/vernotas3/$', ver_subgrid_notas3),
    url(r'^profile/(\d+)/master_subgrid_notas/$', master_subgrid_notas_nueva),
    url(r'^profile/(\d+)/master_subgrid_notas1/$', master_subgrid_notas1_nueva),
    url(r'^profile/(\d+)/master_subgrid_notas2/$', master_subgrid_notas2_nueva),
    url(r'^profile/(\d+)/master_subgrid_notas3/$', master_subgrid_notas3_nueva),
    url(r'^profile/Asistencias/(\d+)/(\d+)/(.*)/(.*)/(.*)/$', listado_asistencias),
    url(r'^profile/Asistencias/add/(\d+)/(.*)/(.*)/(.*)/$', agregar_asistencia1),
    url(r'^profile/Asistencias/editarcomun/(\d+)/(.*)/(.*)/(.*)/(.*)/$', modificar_asistencia1),
    url(r'^profile/Asistencias/deletecomun/(\d+)/(.*)/(.*)/(.*)/(.*)/$', eliminar_asistencia1),
    url(r'^profile/Asistencias/reporte_pdf/(\d+)/(.*)/(.*)/(.*)/$', pdf_asistencia_comun),
    url(r'^profile/notas/exportar_excel/(\d+)/$', exportar_notas_excel1),
    url(r'^profile/silabo/(\d+)/$', silabo_curso),
    url(r'^profile/ver_silabo/(\d+)/$', ver_silabo_curso),
    url(r'^profile/ver_silabo/(\d+)/editar/$', editar_silabo_curso),
    url(r'^profile/ver_silabo/(\d+)/descargar/$', descargar_silabo_curso),
    url(r'^profile/material/(\d+)/$', material_curso),
    url(r'^profile/material/agregar/(\d+)/$', agregar_material_curso),
    url(r'^profile/material/editar/(\d+)/$', editar_material_curso),
    url(r'^profile/material/delete/(\d+)/$', eliminar_material_curso),
    url(r'^profile/material/issilabo/(\d+)/$', marterial_curso_essilabo),
    url(r'^profile/asistencias_aux/(\d+)/$', auxiliar_asistencias),
    url(r'^profile/notas_aux/(\d+)/$', auxiliar_notas),
    url(r'^profile/mensajes/redactar/(\d+)/$', redactar_mensaje),
    url(r'^profile/mensajes/enviados/(\d+)/$', mensajes_enviados),
    url(r'^profile/mensajes/recibidos/$', mensajes_recibidos),
    url(r'^labor/$', labor_docente),
    url(r'^evaluacion/$', evaluacion_docente),
    url(r'^evaluacion/descargar/(\d+)/$', evaluacion_docente_descarga),
]
