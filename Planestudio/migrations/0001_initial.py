# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Carreras', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Detalleplan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(max_length=9)),
                ('Nombre', models.CharField(max_length=60)),
                ('Creditos', models.PositiveIntegerField()),
                ('Extracurricular', models.BooleanField(default=False, verbose_name=b'Extra Curricular')),
                ('Tipoextracurricular', models.CharField(max_length=50, null=True, verbose_name=b'Tipo extracurricular', choices=[(b'proyecto_formativo', b'Proyecto Formativo'), (b'informatica_aplicada', b'Informatica Aplicada'), (b'03', b'3\xc2\xba')])),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activada')),
                ('Ciclo', models.CharField(max_length=2, verbose_name=b'Ciclo', choices=[(b'01', b'1\xc2\xba'), (b'02', b'2\xc2\xba'), (b'03', b'3\xc2\xba'), (b'04', b'4\xc2\xba'), (b'05', b'5\xc2\xba'), (b'06', b'6\xc2\xba'), (b'07', b'7\xc2\xba'), (b'08', b'8\xc2\xba'), (b'09', b'9\xc2\xba'), (b'10', b'10\xc2\xba')])),
                ('Equivalencias', models.ManyToManyField(related_name='inverso_equiv', to='Planestudio.Detalleplan', blank=True)),
            ],
            options={
                'verbose_name': 'Curso de Carrera',
                'verbose_name_plural': 'Cursos de Carrera',
            },
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=120, verbose_name=b'Nombre')),
                ('Carrera', models.ForeignKey(to='Carreras.Carrera')),
            ],
            options={
                'verbose_name': 'Curso de Carrera',
                'verbose_name_plural': 'Cursos de Carrera',
            },
        ),
        migrations.AddField(
            model_name='detalleplan',
            name='Plan',
            field=models.ForeignKey(to='Planestudio.Plan'),
        ),
        migrations.AddField(
            model_name='detalleplan',
            name='Prerequisitos',
            field=models.ManyToManyField(related_name='inverso_prereq', to='Planestudio.Detalleplan', blank=True),
        ),
    ]
