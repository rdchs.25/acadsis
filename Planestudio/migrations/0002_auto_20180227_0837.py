# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detalleplan',
            name='Tipoextracurricular',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name=b'Tipo extracurricular', choices=[(b'proyecto_formativo', b'Proyecto Formativo'), (b'informatica_aplicada', b'Informatica Aplicada')]),
        ),
    ]
