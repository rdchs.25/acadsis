# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0002_auto_20180227_0837'),
    ]

    operations = [
        migrations.AddField(
            model_name='detalleplan',
            name='Electivo',
            field=models.BooleanField(default=False, verbose_name=b'Activada'),
        ),
        migrations.AddField(
            model_name='detalleplan',
            name='Electivodetalle',
            field=models.ManyToManyField(related_name='inverso_eledet', to='Planestudio.Detalleplan', blank=True),
        ),
    ]
