# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0004_auto_20180227_1246'),
    ]

    operations = [
        migrations.AddField(
            model_name='detalleplan',
            name='Detalleelectivo',
            field=models.BooleanField(default=False, verbose_name=b'Detalle electivoElectivo'),
        ),
    ]
