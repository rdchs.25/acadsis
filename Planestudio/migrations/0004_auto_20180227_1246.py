# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Planestudio', '0003_auto_20180227_1212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detalleplan',
            name='Electivo',
            field=models.BooleanField(default=False, verbose_name=b'Electivo'),
        ),
    ]
