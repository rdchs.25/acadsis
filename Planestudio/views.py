from django.core import serializers
from django.http import HttpResponse

# Create your views here.
from Planestudio.models import Detalleplan, Plan


def obtener_detalles_plan(request, plan_id):
    if plan_id != "":
        try:
            plan = Plan.objects.get(id=plan_id)
            consulta = Detalleplan.objects.filter(Plan__Carrera__Codigo=plan.Carrera.Codigo).order_by(
                'Nombre')
            if consulta:
                return HttpResponse(
                    serializers.serialize('json', consulta, fields=('pk', 'Nombre'), extras=('__unicode__',)))
        except Detalleplan.DoesNotExist:
            return
