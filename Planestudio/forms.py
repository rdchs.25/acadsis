# -*- coding: utf-8 -*-
from django import forms

from Planestudio.models import Detalleplan


class DetalleplanAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DetalleplanAdminForm, self).__init__(*args, **kwargs)
        # self.fields["Prerequisitos"].queryset = Detalleplan.objects.filter(Plan__Carrera__Codigo="IA")

    # Prerequisitos

    # def __init__(self, *args, **kwargs):
    #
    #     super(DetalleplanAdminForm, self).__init__(*args, **kwargs)
    #     if self.tipo:
    #         if self.tipo == 'matricula':
    #             self.fields['ConceptoInscripcion'].required = False
    #             self.fields['FechaMatricula'].required = True
    #             self.fields['Categoria'].required = True
    #         if self.tipo == 'inscripcion':
    #             self.fields['ConceptosDisponibles'].required = False
    #             pass
    #     if self.tipo is None:
    #         self.fields['ConceptoInscripcion'].required = False
    #     if self.interesado_id:
    #         self.fields['Interesado'].queryset = Interesado.objects.filter(
    #             id=self.interesado_id)
    #     try:
    #         self.fields[
    #             'Maestria'].initial = self.instance.GrupoMaestria.Maestria.id
    #         self.fields[
    #             'GrupoMaestria'].initial = self.instance.GrupoMaestria.id
    #         self.fields[
    #             'Categoria'].initial = self.instance.Categoria.id
    #     except:
    #         pass

    # Maestria = forms.ModelChoiceField(queryset=Maestria.objects.filter(
    #     Estado=True), widget=forms.Select(), required=True, label='Maestria')
    # ConceptoInscripcion = forms.ModelChoiceField(queryset=ConceptoPago.objects.filter(
    #     Descripcion__icontains='Inscripcion'), widget=forms.Select(), required=True, label='Concepto de Pago')
    # ConceptosDisponibles = forms.ModelMultipleChoiceField(
    #     queryset=ConceptoPago.objects.exclude(Descripcion__icontains='Inscripcion'), required=False, label='Conceptos')

    class Meta:
        model = Detalleplan
        fields = "__all__"

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Planestudio/frm_obtener_cursos.js",
              )
