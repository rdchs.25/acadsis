# -*- coding: utf-8 -*-
import codecs
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Carreras.models import Carrera
from Planestudio.models import Plan, Detalleplan


def script_registrar(archivo, nombre_plan, codigo_carrera):
    try:
        carrera = Carrera.objects.get(Codigo=codigo_carrera)
        plan = Plan(Nombre=nombre_plan, Carrera=carrera)
        plan.save()
        nombre_archivo = "extras/" + archivo
        file1 = codecs.open(nombre_archivo, 'r', 'utf-8')
        cursos_electivos = False
        for line in file1:
            line = line.strip('\n')
            linea = line.split(';')
            codigo = linea[0].replace(" ", "")
            if codigo == "":
                cabecera = linea[1]
                if cabecera == "Cursos Electivos":
                    cursos_electivos = True
                print ("Cabecera: " + cabecera)
            else:
                nombre_curso = linea[1]
                requisito = linea[2]
                creditos = linea[3]
                ciclo = linea[4]
                electivo = False
                if nombre_curso == 'Electivo':
                    electivo = True
                detalle_plan = Detalleplan(Codigo=codigo, Plan=plan, Nombre=nombre_curso, Creditos=creditos,
                                           Ciclo=ciclo, Electivo=electivo)

                if requisito != 'Ning.':
                    codigos_pre = requisito.split('|')
                    for i in range(0, len(codigos_pre)):
                        try:
                            codigo_pre_limpio = codigos_pre[i].replace(" ", "")
                            detalle_pre = Detalleplan.objects.get(Codigo=codigo_pre_limpio, Plan=plan)
                            detalle_plan.save()
                            detalle_plan.Prerequisitos.add(detalle_pre.id)
                        except Detalleplan.DoesNotExist:
                            print ("Pre requisito no encontrado")
                            return
                else:
                    detalle_plan.save()
                if cursos_electivos is True:
                    codigo_padre_electivo = linea[5].replace(" ", "")
                    padre_electivo = Detalleplan.objects.get(Codigo=codigo_padre_electivo, Plan=plan)
                    padre_electivo.Electivodetalle.add(detalle_plan.id)
                    detalle_plan.Detalleelectivo = True
                    detalle_plan.save()

                print (codigo)

        file1.close()
    except Carrera.DoesNotExist:
        return


# script_registrar("at.csv", "Plan de estudio 2010 - 2015", "AT")
script_registrar("is.csv", "Plan de estudio 2010 - 2015", "IS")
# script_registrar("ia.csv", "Plan de estudio 2010 - 2015", "IA")
# script_registrar("ic.csv", "Plan de estudio 2010 - 2015", "IC")
