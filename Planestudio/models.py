# -*- coding: utf-8 -*-
from django.db import models

from Carreras.models import Carrera

CICLO_CHOICES = (
    ('01', '1º'),
    ('02', '2º'),
    ('03', '3º'),
    ('04', '4º'),
    ('05', '5º'),
    ('06', '6º'),
    ('07', '7º'),
    ('08', '8º'),
    ('09', '9º'),
    ('10', '10º'),
)

TIPOEXTRACURRICULAR_CHOICES = (
    ('proyecto_formativo', 'Proyecto Formativo'),
    ('informatica_aplicada', 'Informatica Aplicada'),
)


class Plan(models.Model):
    Nombre = models.CharField("Nombre", max_length=120)
    Carrera = models.ForeignKey(Carrera)

    def __unicode__(self):
        return '%s - %s' % (self.Nombre, self.Carrera)

    class Meta:
        verbose_name = "Curso de Carrera"
        verbose_name_plural = "Cursos de Carrera"


class Detalleplan(models.Model):
    Codigo = models.CharField(max_length=9)
    Nombre = models.CharField(max_length=60)
    Creditos = models.PositiveIntegerField()
    Extracurricular = models.BooleanField("Extra Curricular", default=False)
    Tipoextracurricular = models.CharField("Tipo extracurricular", max_length=50, null=True, blank=True,
                                           choices=TIPOEXTRACURRICULAR_CHOICES)
    Estado = models.BooleanField("Activada", default=True)
    Prerequisitos = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='inverso_prereq')
    Equivalencias = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='inverso_equiv')
    Ciclo = models.CharField("Ciclo", max_length=2, choices=CICLO_CHOICES)
    Plan = models.ForeignKey(Plan)
    Electivo = models.BooleanField("Electivo", default=False)
    Electivodetalle = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='inverso_eledet')
    Detalleelectivo = models.BooleanField("Detalle electivoElectivo", default=False)

    def __unicode__(self):
        return '%s - %s, %s' % (self.get_Ciclo_display(), self.Codigo, self.Nombre)

    class Meta:
        verbose_name = "Curso de Carrera"
        verbose_name_plural = "Cursos de Carrera"
