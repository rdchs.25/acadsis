from django.contrib import admin

from Planestudio.forms import DetalleplanAdminForm
from Planestudio.models import Plan, Detalleplan


class DetalleplanAdmin(admin.ModelAdmin):
    form = DetalleplanAdminForm
    # actions = ['exportar_curso', 'crear_grupo_horario']
    filter_horizontal = ['Prerequisitos', 'Equivalencias', 'Electivodetalle']
    list_display = (
        'Codigo', 'Nombre', 'Creditos', 'Ciclo', 'Plan', 'Estado', 'Electivo', 'Detalleelectivo', 'Extracurricular',)
    list_filter = ('Ciclo', 'Estado', 'Electivo', 'Extracurricular', 'Plan__Carrera__Carrera',)
    search_fields = ('Codigo', 'Nombre')
    fieldsets = (
        ('Datos del Curso', {
            'fields': (
                'Plan', 'Codigo', 'Nombre', 'Creditos', 'Ciclo', 'Equivalencias', 'Prerequisitos', 'Electivodetalle',
                'Extracurricular', 'Tipoextracurricular', 'Estado', 'Electivo', 'Detalleelectivo',),
        }),
    )


admin.site.register(Plan)
admin.site.register(Detalleplan, DetalleplanAdmin)
