# -*- coding: utf-8 -*-
import datetime
from decimal import Decimal

import django.utils.timezone
from django.db import models

from Alumnos.models import Alumno
from Categorias.models import Categoria
from Cursos.models import PeriodoCurso
from Periodos.models import Periodo
from UDL.moodle_webservice import moodle_webservices

CONDICION_CHOICES = (
    ('Matriculado', 'Matriculado'),
    ('Reserva de Matrícula', 'Reserva de Matrícula'),
    ('Retirado', 'Retirado'),
)


class MatriculaCiclo(models.Model):
    Periodo = models.ForeignKey(Periodo, verbose_name="Período")
    Alumno = models.ForeignKey(Alumno, verbose_name="Alumno")
    Categoria = models.ForeignKey(Categoria, verbose_name="Categoría")
    FechaMatricula = models.DateField("Fecha de Matrícula", default=django.utils.timezone.now)
    Estado = models.CharField("Condición", max_length=100, choices=CONDICION_CHOICES, default='Matriculado')
    FechaRetiro = models.DateField("Fecha de Retiro", null=True, blank=True)
    Observaciones = models.TextField(blank=True, null=True)
    TieneSeguro = models.BooleanField("¿Tiene seguro?", default=False)
    CopiaDni = models.BooleanField("Copia de DNI", default=False)
    ReciboLuzAgua = models.BooleanField("Recibo de luz o agua", default=False)
    Exoneradoidiomas = models.BooleanField("¿Exonerado idiomas?", default=False)
    MatriculaProyectoFormativo = models.BooleanField("¿Activar proyecto formativo?", default=False)

    def __unicode__(self):
        return u'%s - %s' % (self.Periodo, self.Alumno)

    def Codigo(self):
        return u'%s' % self.Alumno.Codigo

    def categoria(self):
        return u'%s' % self.Categoria.Categoria

    def Carrera(self):
        return u'%s' % self.Alumno.Carrera

    def NroAsistencias(self):
        asistencias = 0
        for curso in self.matriculacursos_set.all():
            asistencias += curso.NroAsistencias()
        return asistencias

    def NroFaltas(self):
        faltas = 0
        for curso in self.matriculacursos_set.all():
            faltas += curso.NroFaltas()
        return faltas

    def NroTardanzas(self):
        tardanzas = 0
        for curso in self.matriculacursos_set.all():
            tardanzas += curso.NroTardanzas()
        return tardanzas

    def CredAprobados(self):
        sum_cred = 0
        for curso in self.matriculacursos_set.all():
            if curso.Aprobado is True:
                sum_cred += curso.PeriodoCurso.Curso.Creditos
        return sum_cred

    def CredDesaprobados(self):
        sum_cred = 0
        for curso in self.matriculacursos_set.all():
            if curso.Aprobado is False:
                sum_cred += curso.PeriodoCurso.Curso.Creditos
        return sum_cred

    def pagaronmatricula(self):
        from Pagos.models import PagoAlumno
        pagomatricula = PagoAlumno.objects.filter(MatriculaCiclo__id=self.id,
                                                  ConceptoPago__Descripcion__icontains='MATRICULA', Estado=True)
        alumnopago = False
        if pagomatricula.count() > 0:
            alumnopago = True
        return alumnopago

    def conceptosAsignados(self):
        from Pagos.models import PagoAlumno
        q = PagoAlumno.objects.filter(MatriculaCiclo__id=self.id)
        deudaspregrado = PagoAlumno.objects.filter(Estado=False,
                                                   MatriculaCiclo__Alumno__Codigo=self.Alumno.Codigo).exclude(
            MatriculaCiclo__id=self.id)
        deudasidiomas = PagoAlumno.objects.using('idiomas').filter(Estado=False,
                                                                   MatriculaCiclo__Alumno__Codigo=self.Alumno.Codigo)

        c = ""
        if len(q) > 0:
            c = c + "<strong>Asignación de pagos del ciclo actual</strong><br>"
            for concepto in q:
                if concepto.EstadoProrroga() is True and concepto.Estado is False:
                    c = c + "- " + str(
                        concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon_clock.gif' /><br/>"
                else:
                    if concepto.Estado is True:
                        c = c + "- " + str(
                            concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon-yes.gif' /><br/>"
                    else:
                        c = c + "- " + str(
                            concepto.ConceptoPago) + " - " + "<img alt='0' src='/static/admin/img/icon-no.gif' /><br/>"
        if len(deudaspregrado) > 0:
            c = c + "<br><strong>Deudas Pregrado Ciclos Anterioress</strong><br>"
            for concepto in deudaspregrado:
                if concepto.EstadoProrroga() is True and concepto.Estado is False:
                    c = c + "- " + str(
                        concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon_clock.gif' /><br/>"
                else:
                    if concepto.Estado is True:
                        c = c + "- " + str(
                            concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon-yes.gif' /><br/>"
                    else:
                        c = c + "- " + str(
                            concepto.ConceptoPago) + " - " + "<img alt='0' src='/static/admin/img/icon-no.gif' /><br/>"

        if len(deudasidiomas) > 0:
            c = c + "<br><strong>Deudas Instituto de Idiomas</strong><br>"
            for concepto in deudasidiomas:
                if concepto.EstadoProrroga() is True and concepto.Estado is False:
                    c = c + "- " + str(concepto.ConceptoPago.Periodo.Anio) + ' ' + str(
                        concepto.ConceptoPago.Periodo.Semestre) + ' ' + str(
                        concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon_clock.gif' /><br/>"
                else:
                    if concepto.Estado is True:
                        c = c + "- " + str(concepto.ConceptoPago.Periodo.Anio) + ' ' + str(
                            concepto.ConceptoPago.Periodo.Semestre) + ' ' + str(
                            concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon-yes.gif' /><br/>"
                    else:
                        c = c + "- " + str(concepto.ConceptoPago.Periodo.Anio) + ' ' + str(
                            concepto.ConceptoPago.Periodo.Semestre) + ' ' + str(
                            concepto.ConceptoPago) + " - " + "<img alt='0' src='/static/admin/img/icon-no.gif' /><br/>"
        return c

    def tienedeudas(self):
        from Pagos.models import PagoAlumno
        q = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__Alumno__Codigo=self.Alumno.Codigo)
        deudasidiomas = PagoAlumno.objects.using('idiomas').filter(Estado=False,
                                                                   MatriculaCiclo__Alumno__Codigo=self.Alumno.Codigo)
        cantidaddeudas = len(q) + len(deudasidiomas)
        print (str(cantidaddeudas))
        if cantidaddeudas == 0:
            return False
        else:
            return True

    def conceptosAsignados2(self):
        from Pagos.models import PagoAlumno
        q = PagoAlumno.objects.filter(Estado=False, MatriculaCiclo__id=self.id)
        c = ""
        if len(q) > 0:
            for concepto in q:
                if concepto.EstadoProrroga() is True and concepto.Estado is False:
                    c = c + "- " + str(
                        concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon_clock.gif' /><br/>"
                else:
                    if concepto.Estado is True:
                        c = c + "- " + str(
                            concepto.ConceptoPago) + " - " + "<img alt='1' src='/static/admin/img/icon-yes.gif' /><br/>"
                    else:
                        c = c + "- " + str(
                            concepto.ConceptoPago) + " - " + "<img alt='0' src='/static/admin/img/icon-no.gif' /><br/>"
        return c

    def save(self):
        if self.Estado == 'Retirado' or self.Estado == u'Reserva de Matrícula':
            pagos_pendientes = self.pagoalumno_set.filter(Estado=False)
            for pago in pagos_pendientes:
                detalles = pago.detallepago_set.filter(MontoCancelado=Decimal("0.00"))
                for detalle in detalles:
                    detalle.Activado = False
                    detalle.save()
            cursos_matriculados = self.matriculacursos_set.all()
            for curso in cursos_matriculados:
                curso.Estado = False
                curso.FechaRetiro = datetime.date.today()
                curso.save()
        elif self.Estado == 'Matriculado':
            pagos_pendientes = self.pagoalumno_set.filter(Estado=False)
            for pago in pagos_pendientes:
                detalles = pago.detallepago_set.filter(MontoCancelado=Decimal("0.00"))
                for detalle in detalles:
                    detalle.Activado = True
                    detalle.save()
            cursos_matriculados = self.matriculacursos_set.all()
            for curso in cursos_matriculados:
                curso.Estado = True
                curso.FechaRetiro = None
                curso.save()
        return super(MatriculaCiclo, self).save()

    class Meta:
        unique_together = (("Alumno", "Periodo"),)
        verbose_name = "Matrícula en Ciclo Académico UDL"
        verbose_name_plural = "Matrícula en Ciclo Académico UDL"
        permissions = (("change_seguro_matriculaciclo", "Modificar seguro"),)


class MatriculaCursos(models.Model):
    MatriculaCiclo = models.ForeignKey(MatriculaCiclo, verbose_name="Alumno")
    PeriodoCurso = models.ForeignKey(PeriodoCurso, verbose_name="Curso")
    FechaMatricula = models.DateField("Fecha de Matrícula", default=django.utils.timezone.now)
    Convalidado = models.BooleanField("¿Es Convalidación?", default=False)
    Resolucion = models.CharField("Decreto", max_length=30, null=True, blank=True)
    Estado = models.BooleanField("Matrícula Activada", default=True)
    FechaRetiro = models.DateField("Fecha de Retiro", null=True, blank=True)
    FechaCreacion = models.DateField("Fecha de Creación", auto_now_add=True, editable=False)
    Aprobado = models.BooleanField("Curso Aprobado", default=False)
    Encuesta = models.BooleanField("Encuesta respondida", default=False)
    Moodleid = models.PositiveIntegerField("Moodle id", editable=False, null=True)

    def save(self, **kwargs):
        if self.MatriculaCiclo.Alumno.Moodleid is not None and self.PeriodoCurso.Moodleid is not None:
            datos_matriculacurso = {
                'enrolments[0][roleid]': 5,
                'enrolments[0][userid]': self.MatriculaCiclo.Alumno.Moodleid,
                'enrolments[0][courseid]': self.PeriodoCurso.Moodleid,
            }
            moodle_webservices("enrol_manual_enrol_users", datos_matriculacurso)
        return super(MatriculaCursos, self).save()

    def delete(self, **kwargs):
        if self.MatriculaCiclo.Alumno.Moodleid is not None and self.PeriodoCurso.Moodleid is not None:
            datos_matriculacurso = {
                'enrolments[0][roleid]': 5,
                'enrolments[0][userid]': self.MatriculaCiclo.Alumno.Moodleid,
                'enrolments[0][courseid]': self.PeriodoCurso.Moodleid,
            }
            moodle_webservices("enrol_manual_unenrol_users", datos_matriculacurso)
        super(MatriculaCursos, self).delete()

    def __unicode__(self):
        return u'%s - %s - %s' % (self.PeriodoCurso.Periodo, self.MatriculaCiclo.Alumno, self.PeriodoCurso.Curso)

    def aprobado(self):
        if self.Promedio() >= Decimal(self.PeriodoCurso.Aprobacion):
            self.Aprobado = True
            super(MatriculaCursos, self).save()
            return True
        else:
            self.Aprobado = False
            super(MatriculaCursos, self).save()
            return False

    def Calificaciones(self):
        return self.notasalumno_set.all().order_by('Nota__Orden')

    def NroAsistencias(self):
        asistencias = self.asistencia_set.filter(Estado='Asistio').count()
        return asistencias

    def NroFaltas(self):
        faltas = self.asistencia_set.filter(Estado='Falto').count()
        return faltas

    def NroTardanzas(self):
        tardanzas = self.asistencia_set.filter(Estado='Tardanza').count()
        return tardanzas

    def PorcentajeAsistencias(self):
        num_horarios = self.PeriodoCurso.horario_set.count()
        if num_horarios != 0:
            total_sesiones = Decimal(num_horarios * 16)
            faltas = Decimal(self.asistencia_set.filter(Estado='Asistio').count())
            porcentaje = faltas / total_sesiones
            valor = Decimal("%.2f" % porcentaje) * 100
            return valor
        else:
            return "-1"

    def PorcentajeFaltas(self):
        num_horarios = self.PeriodoCurso.horario_set.count()
        if num_horarios != 0:
            total_sesiones = Decimal(num_horarios * 16)
            faltas = Decimal(self.asistencia_set.filter(Estado='Falto').count())
            porcentaje = faltas / total_sesiones
            valor = Decimal("%.2f" % porcentaje) * 100
            return valor
        else:
            return "-1"

    def Inhabilitado(self):
        porcentaje = self.PorcentajeFaltas()
        if Decimal(porcentaje) >= Decimal('30.00'):
            return True
        else:
            return False

    def Inhabilitadoporpagos(self):
        return False
        """
        deudas = self.MatriculaCiclo.pagoalumno_set.filter(Prorroga=False,Estado = False,MatriculaCiclo__Alumno__id=self.MatriculaCiclo.Alumno.id, MatriculaCiclo__Periodo = 19, ConceptoPago__Descripcion__icontains='4ta Pension')
        if deudas.count() != 0:
            if str(deudas[0].DeudaMonto()) != "0.00":
                return True
            else:
                return False
        else:
            return False
        """

    def PromedioParcial(self, nota):
        notas = self.notasalumno_set.filter(Nota__NotaPadre=nota)
        suma_pesos = 0
        suma = 0
        for nota in notas:
            suma_pesos += nota.Nota.Peso
            suma += nota.Valor * nota.Nota.Peso
        if (suma_pesos == 0):
            promedio = 0
        else:
            promedio = suma / suma_pesos
        valor = Decimal("%.2f" % promedio)
        return valor

    # este es el promedio evaluando si se modifico el valor del promedio final.
    def Promedio(self):
        Promedio = Decimal("0.00")
        calificaciones = self.notasalumno_set.all()
        for cal in calificaciones:
            if cal.Nota.Nivel == '3':
                nivel3 = cal.Valor * cal.Nota.Peso
                nivel2 = cal.Nota.NotaPadre.Peso / cal.Nota.NotaPadre.total_peso_subnotas()
                nivel1 = cal.Nota.NotaPadre.NotaPadre.Peso / cal.Nota.NotaPadre.NotaPadre.total_peso_subnotas()
                nivel0 = cal.Nota.NotaPadre.NotaPadre.NotaPadre.Peso / cal.Nota.NotaPadre.NotaPadre.NotaPadre.total_peso_subnotas()
                Promedio += nivel3 * nivel2 * nivel1 * nivel0
            elif cal.Nota.Nivel == '2':
                nivel2 = cal.Valor * cal.Nota.Peso
                nivel1 = cal.Nota.NotaPadre.Peso / cal.Nota.NotaPadre.total_peso_subnotas()
                nivel0 = cal.Nota.NotaPadre.NotaPadre.Peso / cal.Nota.NotaPadre.NotaPadre.total_peso_subnotas()
                Promedio += nivel2 * nivel1 * nivel0
            elif cal.Nota.Nivel == '1':
                nivel1 = cal.Valor * cal.Nota.Peso
                nivel0 = cal.Nota.NotaPadre.Peso / cal.Nota.NotaPadre.total_peso_subnotas()
                Promedio += nivel1 * nivel0
            elif cal.Nota.Nivel == '0':
                if cal.Nota.SubNotas == False:
                    nivel0 = cal.Valor * cal.Nota.Peso
                    Promedio += nivel0
        if calificaciones.count() != 0:
            try:
                condicional = calificaciones.filter(Nota__Nivel='0')[0].Valor
                if (condicional != Promedio) and (condicional != Decimal("0.00")):
                    Promedio = calificaciones.filter(Nota__Nivel='0')[0].Valor
            except:
                Promedio = 0
        valor = Decimal("%.2f" % round(Promedio))
        return valor

    # este es el promedio normal
    def Promedio1(self):
        Promedio = Decimal("0.00")
        calificaciones = self.notasalumno_set.all()
        for cal in calificaciones:
            if cal.Nota.Nivel == '3':
                nivel3 = cal.Valor * cal.Nota.Peso
                nivel2 = cal.Nota.NotaPadre.Peso / cal.Nota.NotaPadre.total_peso_subnotas()
                nivel1 = cal.Nota.NotaPadre.NotaPadre.Peso / cal.Nota.NotaPadre.NotaPadre.total_peso_subnotas()
                nivel0 = cal.Nota.NotaPadre.NotaPadre.NotaPadre.Peso / cal.Nota.NotaPadre.NotaPadre.NotaPadre.total_peso_subnotas()
                Promedio += nivel3 * nivel2 * nivel1 * nivel0
            elif cal.Nota.Nivel == '2':
                nivel2 = cal.Valor * cal.Nota.Peso
                nivel1 = cal.Nota.NotaPadre.Peso / cal.Nota.NotaPadre.total_peso_subnotas()
                nivel0 = cal.Nota.NotaPadre.NotaPadre.Peso / cal.Nota.NotaPadre.NotaPadre.total_peso_subnotas()
                Promedio += nivel2 * nivel1 * nivel0
            elif cal.Nota.Nivel == '1':
                nivel1 = cal.Valor * cal.Nota.Peso
                nivel0 = cal.Nota.NotaPadre.Peso / cal.Nota.NotaPadre.total_peso_subnotas()
                Promedio += nivel1 * nivel0
            elif cal.Nota.Nivel == '0':
                if cal.Nota.SubNotas == False:
                    nivel0 = cal.Valor * cal.Nota.Peso
                    Promedio += nivel0
        valor = Decimal("%.2f" % round(Promedio))
        return valor

    def getMaximocreditos(self):
        return False

    class Meta:
        unique_together = (("MatriculaCiclo", "PeriodoCurso"),)
        verbose_name = "Matrícula en Cursos UDL"
        verbose_name_plural = "Matrícula en Cursos UDL"
        permissions = (("read_matriculacursos", "Observar Matricula de Cursos UDL"),)
