#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
from datetime import datetime as dt

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

# imports para traer variable de entorno django
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from Cursos.models import PeriodoCurso
from Pagos.models import PagoAlumno

# se matricula automaticamente en los cursos de 1er ciclo de su carrera
cachimbos_matriculados = MatriculaCiclo.objects.filter(Alumno__AnioIngreso="2018", Alumno__Semestre="II")


def matricular_cachimbos():
    for cachimbo in cachimbos_matriculados:
        fecha = "2018-08-27"
        # if cachimbo.FechaMatricula != fecha:
        #     cachimbo.FechaMatricula = fecha
        #     cachimbo.save()
        cursos = PeriodoCurso.objects.filter(Curso__Carrera=cachimbo.Alumno.Carrera, Curso__Ciclo='01', Periodo_id=25)
        cursos_mat = MatriculaCursos.objects.filter(MatriculaCiclo=cachimbo)
        pagomatricula = PagoAlumno.objects.filter(MatriculaCiclo=cachimbo,
                                                  ConceptoPago__Descripcion__icontains="Matrícula", Estado=True)
        if (cursos_mat.count() == 0) and pagomatricula.count() > 0:
            fechapagomatricula = pagomatricula[0].fechapagomatricula()
            a = dt.strptime(fecha, "%Y-%m-%d")
            b = dt.strptime(fechapagomatricula, "%Y-%m-%d")
            if b <= a:
                fechapagomatricula = fecha
            print ("Cachimbo Matriculado: " + cachimbo.__unicode__())
            for curso in cursos:
                nueva_mat = MatriculaCursos(MatriculaCiclo=cachimbo, PeriodoCurso=curso,
                                            FechaMatricula=fechapagomatricula)
                nueva_mat.save()


def asignar_matricula():
    inscritos = MatriculaCiclo.objects.filter(Periodo_id=25, Estado="Matriculado")
    # Verificar si han pagado matricula
    for inscrito in inscritos:
        concepto_pagos = inscrito.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="Matrícula")
        if concepto_pagos.count() > 0:
            concepto_pago = concepto_pagos[0]
        else:
            continue
        # Si ya pagó matrícula, busca si tiene asignada la primera pensión
        # En caso no tenga asignada la primera pensión, se le asigna
        if concepto_pago.Estado and inscrito.Categoria.Categoria != "BECA 0":
            try:
                pension_1 = inscrito.pagoalumno_set.filter(ConceptoPago__id=139)[0]
            except ValueError:
                print "Se le ha asignado concepto de matrícula a: ", inscrito
                PagoAlumno(ConceptoPago_id=139, MatriculaCiclo_id=inscrito.id).save()


matricular_cachimbos()
