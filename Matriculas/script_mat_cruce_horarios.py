#!/usr/bin/python
# -*- coding: utf-8 -*-
# import para enviar correos

# imports para traer variable de entorno django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

# imports para generar archivo en excel
from Matriculas.models import MatriculaCiclo
from tempfile import TemporaryFile
from xlwt import Workbook, XFStyle, Borders, Pattern, Font


def matriculados_fecha():
    book = Workbook()
    sheet1 = book.add_sheet('Hoja 1')

    # configuramos estilos de celda
    fnt = Font()
    fnt.name = 'Arial'
    borders = Borders()
    borders.left = Borders.THIN
    borders.right = Borders.THIN
    borders.top = Borders.THIN
    borders.bottom = Borders.THIN
    pattern = Pattern()
    pattern.pattern = Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = 0xB4030D

    # estilo para titulos de columnas
    style = XFStyle()
    style.font = fnt
    style.borders = borders
    style.pattern = pattern

    # estilo para resultados
    style1 = XFStyle()
    style1.font = fnt
    style1.borders = borders

    # configuramos ancho de columnas
    sheet1.col(0).width = 2000
    sheet1.col(1).width = 2500
    sheet1.col(2).width = 9000
    sheet1.col(3).width = 9000
    sheet1.col(4).width = 9000
    sheet1.col(5).width = 3000
    sheet1.col(6).width = 9000
    sheet1.col(7).width = 3000
    sheet1.col(8).width = 9000
    sheet1.col(9).width = 2000

    # empezamos a escribir el resumen
    sheet1.write(1, 0, 'Listado de Matriculados con cruce de horarios Semestre 2013-I')

    # escribimos los encabezados
    sheet1.write(14, 0, 'Nro', style)
    sheet1.write(14, 1, 'Codigo', style)
    sheet1.write(14, 2, 'Estudiante', style)
    sheet1.write(14, 3, 'Carrera', style)
    sheet1.write(14, 4, 'Curso', style)
    sheet1.write(14, 5, 'Creditos', style)
    sheet1.write(14, 6, 'Ciclo', style)
    sheet1.write(14, 7, 'Dia', style)
    sheet1.write(14, 8, 'Hora Inicio', style)
    sheet1.write(14, 9, 'Hora Fin', style)

    i = 15
    matriculados = MatriculaCiclo.objects.filter(Periodo__id=9, Estado='Matriculado').exclude(
        Alumno__AnioIngreso='2013').order_by('Alumno__ApellidoPaterno', 'Alumno__ApellidoMaterno', 'Alumno__Nombres')
    print matriculados.count()

    # recorremos la consulta y escribimos los resultados
    for mat in matriculados:
        cursos_mat = mat.matriculacursos_set.all()
        if cursos_mat.count() != 0:
            hors = []
            for c in cursos_mat:
                horarios = c.PeriodoCurso.horario_set.all()
                for h in horarios:
                    hors.append(h)
            cruces = []
            for h in hors:
                for h1 in hors:
                    if h != h1:
                        resta = int(h.PeriodoCurso.Curso.Ciclo) - int(h1.PeriodoCurso.Curso.Ciclo)
                        if resta == 2 or resta == -2 or resta == 4 or resta == -4:
                            if h.Dia == h1.Dia:
                                if h.HoraInicio >= h1.HoraInicio and h.HoraFin <= h1.HoraFin:
                                    if not h in cruces:
                                        cruces.append(h)
                                elif h.HoraInicio >= h1.HoraInicio and h.HoraInicio < h1.HoraFin:
                                    if not h in cruces:
                                        cruces.append(h)
                                elif h.HoraFin <= h1.HoraFin and h.HoraFin > h1.HoraInicio:
                                    if not h in cruces:
                                        cruces.append(h)
                                elif h.HoraInicio <= h1.HoraInicio and h.HoraFin >= h1.HoraFin:
                                    if not h in cruces:
                                        cruces.append(h)
            for c in cruces:
                sheet1.write(i, 0, i - 14, style1)
                sheet1.write(i, 1, mat.Alumno.Codigo, style1)
                sheet1.write(i, 2, mat.Alumno.__unicode__(), style1)
                sheet1.write(i, 3, mat.Alumno.Carrera.Carrera, style1)
                sheet1.write(i, 4, c.PeriodoCurso.Curso.Nombre, style1)
                sheet1.write(i, 5, c.PeriodoCurso.Curso.Creditos, style1)
                sheet1.write(i, 6, c.PeriodoCurso.Curso.Ciclo, style1)
                sheet1.write(i, 7, c.Dia, style1)
                sheet1.write(i, 8, c.HoraInicio.strftime('%H:%M'), style1)
                sheet1.write(i, 9, c.HoraFin.strftime('%H:%M'), style1)
                i += 1
    book.save('mat_cruce_horarios.xls')
    book.save(TemporaryFile())


matriculados_fecha()
