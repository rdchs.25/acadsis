# -*- coding: utf-8 -*-

from decimal import Decimal

from Categorias.models import Categoria
from Matriculas.models import MatriculaCiclo, MatriculaCursos
# importar formatos reportlab creados
from Pagos.models import PagoAlumno, DetallePago, ConceptoPago
from funciones import response_excel


def archivo_categorizacion(matriculados):
    ponderado_matriculados = []

    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Ponderado']
    # comprobamos que sus pagos hayan sido ha tiempo
    listapensiones = ['220', '221', '222', '223', '224']

    for concepto in listapensiones:
        descripcionconcepto = ConceptoPago.objects.get(id=concepto).Descripcion + '(' + str(
            ConceptoPago.objects.get(id=concepto).FechaVencimiento) + ')'
        headers.append(descripcionconcepto)
    headers.append("Categoria 2018-I")
    headers.append("Categoria 2018-II")
    headers.append("Observación")
    headers.append("¿Categorizar?")
    for matriculado in matriculados:
        try:
            listadatos = []
            mat = MatriculaCiclo.objects.get(id=matriculado)
            codigo = mat.Alumno.Codigo
            apellidos = mat.Alumno.ApellidoPaterno + ' ' + mat.Alumno.ApellidoMaterno
            nombres = mat.Alumno.Nombres
            carrera = mat.Alumno.Carrera.Carrera
            creditos_reporte = 0
            suma_producto = 0
            creditos_aprobados = 0
            cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=mat.id)
            print (codigo)
            if len(cursos_matriculado) <= 0:
                listadatos = [codigo, apellidos, nombres, carrera, 'No registra cursos']
                ponderado_matriculados.append(listadatos)
                continue
            for cur in cursos_matriculado:
                if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
                    # Antes de calcular el credito primero es necesario
                    # verificar si el curso tiene equivalencia
                    if cur.PeriodoCurso.Curso.Equivalencia:
                        c = cur.PeriodoCurso.Curso.Creditos_equiv
                    else:
                        c = cur.PeriodoCurso.Curso.Creditos
                    # total de creditos
                    creditos_reporte += c
                    # creditos aprobados
                    if cur.aprobado() is True:
                        creditos_aprobados += int(c)
                    suma_producto += int(c) * cur.Promedio()
            # ponderado, total de creditos y creditos aprobados
            ponderado = suma_producto / creditos_reporte

            # definimos criterios de categorización
            categorianueva = ''
            pagopuntual = True
            cambioporponderado = False
            motivocambiotecategoria = ''
            categoriaactual = ''
            sematricula = True
            # parámetros iniciales

            categorias2018i = {'CATEGORIA A': 350.00,
                               'CATEGORIA B': 300.00,
                               'CATEGORIA C': 270.00,
                               'CATEGORIA D': 200.00,
                               'CATEGORIA E': 150.00,
                               'CATEGORIA F': 100.00,
                               'BECA': 0.00,
                               'CATEGORIA CONVENIO': 50.00,
                               'CATEGORIA ESPECIAL': 250.00}
            categorias2018ii = {'CATEGORIA B': 350.00,
                                'CATEGORIA C': 300.00,
                                'CATEGORIA D': 270.00}
            equivalenciascategoria = {'CATEGORIA A': 'CATEGORIA B',
                                      'CATEGORIA B': 'CATEGORIA C',
                                      'CATEGORIA C': 'CATEGORIA D',
                                      'CATEGORIA D': 'CATEGORIA D',
                                      'CATEGORIA E': 'CATEGORIA D',
                                      'CATEGORIA F': 'CATEGORIA D',
                                      'BECA': 'CATEGORIA D',
                                      'CATEGORIA CONVENIO': 'CATEGORIA D',
                                      'CATEGORIA ESPECIAL': 'CATEGORIA D'}
            # buscamos el equivalente a su categoría en el 2018-II
            textocategoriaactual = ''
            for key, value in categorias2018i.items():
                if key in mat.Categoria.Categoria:
                    categoriaactual = key
                    textocategoriaactual = key + ' - ' + str(value)
            categoriequivalente = equivalenciascategoria[categoriaactual]

            textocategoriaequivalente = categoriequivalente + ' - ' + str(categorias2018ii[categoriequivalente])

            listadatos = [codigo, apellidos, nombres, carrera, Decimal("%.2f" % ponderado)]

            # -----------   verificamos pagos puntuales ------------------------------
            conpcetosnoasignados = False
            for pension in listapensiones:
                descripcion = 'detalle de pago no encontrados'
                pago = PagoAlumno.objects.filter(MatriculaCiclo__id=mat.id, ConceptoPago__id=pension)
                if len(pago) == 0:
                    conpcetosnoasignados = True
                    continue
                pago = PagoAlumno.objects.get(MatriculaCiclo__id=mat.id, ConceptoPago__id=pension)
                if pago.Estado is False:
                    pagopuntual = False
                    sematricula = False
                    descripcion = 'Debe'
                else:
                    conceptopago = pago.ConceptoPago
                    fechavencimiento = conceptopago.FechaVencimiento
                    detallepago = DetallePago.objects.filter(PagoAlumno=pago).order_by('-FechaPago')
                    if len(detallepago) > 0:
                        fechapago = detallepago[0].FechaPago
                        descripcion = str(fechapago)
                        if fechapago > fechavencimiento:
                            pagopuntual = False
                listadatos.append(descripcion)

            # ---------------   categoría correspondiente por promedio obtenido   -----------------------------
            if conpcetosnoasignados is True:
                listadatos = [codigo, apellidos, nombres, carrera, Decimal("%.2f" % ponderado),
                              'Conceptos no asignados']
                ponderado_matriculados.append(listadatos)
                continue

            categoriaporpromedio = None

            if ponderado <= 12.00:
                categoriaporpromedio = 'CATEGORIA B'
                if categoriequivalente == 'CATEGORIA B':
                    cambioporponderado = False
                    categorianueva = 'CATEGORIA B'
                if categoriequivalente == 'CATEGORIA C':
                    cambioporponderado = True
                    categorianueva = 'CATEGORIA B'
                if categoriequivalente == 'CATEGORIA D':
                    cambioporponderado = True
                    categorianueva = 'CATEGORIA C'
            elif 12.00 < ponderado <= 14.00:
                categoriaporpromedio = 'CATEGORIA C'
                if categoriequivalente == 'CATEGORIA B':
                    cambioporponderado = True
                    categorianueva = 'CATEGORIA B'
                if categoriequivalente == 'CATEGORIA C':
                    cambioporponderado = False
                    categorianueva = 'CATEGORIA C'
                if categoriequivalente == 'CATEGORIA D':
                    cambioporponderado = True
                    categorianueva = 'CATEGORIA C'
            elif 14.00 < ponderado:
                categoriaporpromedio = 'CATEGORIA D'
                if categoriequivalente == 'CATEGORIA B':
                    cambioporponderado = True
                    categorianueva = 'CATEGORIA D'
                if categoriequivalente == 'CATEGORIA C':
                    cambioporponderado = True
                    categorianueva = 'CATEGORIA D'
                if categoriequivalente == 'CATEGORIA D':
                    cambioporponderado = False
                    categorianueva = 'CATEGORIA D'
            # si no pago puntual automáticamente pasa a la inmediata superior
            if pagopuntual is False:
                if categoriequivalente == 'CATEGORIA B':
                    categorianueva = 'CATEGORIA B'
                if categoriequivalente == 'CATEGORIA C':
                    categorianueva = 'CATEGORIA B'
                if categoriequivalente == 'CATEGORIA D':
                    categorianueva = 'CATEGORIA C'

            # aquellos alumnos que tenian una pensión menor a 270, autmáticamente pasan a categoría D de 270
            saltarcriterios = False
            if (categoriaactual == 'CATEGORIA D' or categoriaactual == 'CATEGORIA E' or
                    categoriaactual == 'CATEGORIA F' or categoriaactual == 'BECA' or
                    categoriaactual == 'CATEGORIA CONVENIO' or categoriaactual == 'CATEGORIA ESPECIAL'):
                categorianueva = 'CATEGORIA D'
                saltarcriterios = True
            listadatos.append(textocategoriaactual)
            listadatos.append(categorianueva)

            # indicar como motivo de asignación automática a categoría D para aquellos que pagaban menos de 270
            if saltarcriterios is True:
                motivocambiotecategoria = 'Alumno pagaba menos de 270/'

            # indicar que alumnos cambiaría su categoria por el promedio obtenido
            if cambioporponderado is True:
                motivocambiotecategoria = motivocambiotecategoria + 'Promedio varió/'

            # indicar que alumno presenta deudas en el 2018-I
            if pagopuntual is False:
                motivocambiotecategoria = motivocambiotecategoria + 'Pagos atrasados en 2018-I/'

            # validación de deudas de idiomas y pregrado
            tienedeudas = mat.tienedeudas()

            # si tiene deudas no se matrícula en el siguiente ciclo
            if tienedeudas is True:
                sematricula = False
                motivocambiotecategoria = motivocambiotecategoria + "Registra deudas/"

            listadatos.append(motivocambiotecategoria)
            if sematricula is True:
                listadatos.append("SI")
            else:
                listadatos.append("NO")

            # Validación y registro de matrícula
            existematricula = MatriculaCiclo.objects.filter(Periodo_id=25, Alumno=mat.Alumno)
            if len(existematricula) == 0 and sematricula is True:
                categoriasdepago = Categoria.objects.filter(Periodo_id=25, Categoria__icontains=categorianueva).order_by(
                    'id')
                categoriadepago = categoriasdepago[0]
                observacion = "07/08/2018: Se categorizó con " + categorianueva + " por motivo de " \
                              + motivocambiotecategoria
                nueva_matricula = MatriculaCiclo(Periodo_id=25, Alumno=mat.Alumno, Categoria=categoriadepago,
                                                 Observaciones=observacion)
                nueva_matricula.save()

            ponderado_matriculados.append(listadatos)

        except (ValueError, PagoAlumno.DoesNotExist, DetallePago.DoesNotExist):
            print (ValueError.message)
            ponderado_matriculados.append(['', '', '', '', '', '', '', '', '', '', ''])

    titulo = 'INFORME DE CATEGORIZACIÓN'
    return response_excel(titulo=titulo, heads=headers, registros=ponderado_matriculados,
                          nombre_archivo='informe_categorizacion')
