# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Notas.models import NotasAlumno
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from decimal import Decimal

matriculas = MatriculaCiclo.objects.filter(Estado="Matriculado", Periodo__id=25)

for matriculaciclo in matriculas:
    cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=matriculaciclo.id)
    for cur in cursos_matriculado:
        try:
            nota_promedio = NotasAlumno.objects.get(MatriculaCursos__id=cur.id, Nota__Nivel='0')
        except (ValueError, NotasAlumno.DoesNotExist):
            print ("alumno: " + cur.MatriculaCiclo.Alumno.Codigo)
            print ("curso: " + cur.PeriodoCurso.Curso.Nombre)
            continue
        nota_promedio.Nota.Peso = Decimal("1.00")
        nota_promedio.Nota.save()
        aplazado = nota_promedio.Valor
        promedio_normal = nota_promedio.MatriculaCursos.Promedio1()

        if (aplazado == Decimal("0.00")) or (promedio_normal > aplazado):
            # print aplazado
            nota_promedio.Valor = promedio_normal
            nota_promedio.save()

        try:
            cur.Aprobado
        except Exception as error:
            continue
