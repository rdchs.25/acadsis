#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

# imports para traer variable de entorno django
import codecs
from Matriculas.models import MatriculaCiclo
from Alumnos.models import Alumno


def script_matricular():
    file1 = codecs.open("extras/ingresantes.csv", 'r', 'utf-8')
    for line in file1:
        line = line.strip('\n')
        linea = line.split(',')
        codigo = linea[0]

        try:
            alumno = Alumno.objects.get(Codigo=codigo)
        except Alumno.DoesNotExist:
            print ("ERROR, no existe alumno con codigo %s" % codigo)
            continue

        try:
            matriculaexiste = MatriculaCiclo.objects.get(Periodo_id=25, Alumno=alumno)
            if matriculaexiste is not None:
                print ("ERROR, alumno ya matriculado con codigo %s" % codigo)
                continue
        except MatriculaCiclo.DoesNotExist:
            nueva_matricula = MatriculaCiclo(Periodo_id=25, Alumno=alumno, Categoria_id=767)
            nueva_matricula.save()
        except ValueError:
            print ("Error %s" % alumno)
            continue

    file1.close()


script_matricular()
