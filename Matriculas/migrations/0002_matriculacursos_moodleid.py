# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Matriculas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='matriculacursos',
            name='Moodleid',
            field=models.PositiveIntegerField(verbose_name=b'Moodle id', null=True, editable=False),
        ),
    ]
