# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('Categorias', '0001_initial'),
        ('Cursos', '0001_initial'),
        ('Alumnos', '0001_initial'),
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MatriculaCiclo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('FechaMatricula', models.DateField(default=django.utils.timezone.now, verbose_name=b'Fecha de Matr\xc3\xadcula')),
                ('Estado', models.CharField(default=b'Matriculado', max_length=100, verbose_name=b'Condici\xc3\xb3n', choices=[(b'Matriculado', b'Matriculado'), (b'Reserva de Matr\xc3\xadcula', b'Reserva de Matr\xc3\xadcula'), (b'Retirado', b'Retirado')])),
                ('FechaRetiro', models.DateField(null=True, verbose_name=b'Fecha de Retiro', blank=True)),
                ('Observaciones', models.TextField(null=True, blank=True)),
                ('TieneSeguro', models.BooleanField(default=False, verbose_name=b'\xc2\xbfTiene seguro?')),
                ('CopiaDni', models.BooleanField(default=False, verbose_name=b'Copia de DNI')),
                ('ReciboLuzAgua', models.BooleanField(default=False, verbose_name=b'Recibo de luz o agua')),
                ('Exoneradoidiomas', models.BooleanField(default=False, verbose_name=b'\xc2\xbfExonerado idiomas?')),
                ('MatriculaProyectoFormativo', models.BooleanField(default=False, verbose_name=b'\xc2\xbfActivar proyecto formativo?')),
                ('Alumno', models.ForeignKey(verbose_name=b'Alumno', to='Alumnos.Alumno')),
                ('Categoria', models.ForeignKey(verbose_name=b'Categor\xc3\xada', to='Categorias.Categoria')),
                ('Periodo', models.ForeignKey(verbose_name=b'Per\xc3\xadodo', to='Periodos.Periodo')),
            ],
            options={
                'verbose_name': 'Matr\xedcula en Ciclo Acad\xe9mico UDL',
                'verbose_name_plural': 'Matr\xedcula en Ciclo Acad\xe9mico UDL',
                'permissions': (('change_seguro_matriculaciclo', 'Modificar seguro'),),
            },
        ),
        migrations.CreateModel(
            name='MatriculaCursos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('FechaMatricula', models.DateField(default=django.utils.timezone.now, verbose_name=b'Fecha de Matr\xc3\xadcula')),
                ('Convalidado', models.BooleanField(default=False, verbose_name=b'\xc2\xbfEs Convalidaci\xc3\xb3n?')),
                ('Resolucion', models.CharField(max_length=30, null=True, verbose_name=b'Resoluci\xc3\xb3n', blank=True)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Matr\xc3\xadcula Activada')),
                ('FechaRetiro', models.DateField(null=True, verbose_name=b'Fecha de Retiro', blank=True)),
                ('FechaCreacion', models.DateField(auto_now_add=True, verbose_name=b'Fecha de Creaci\xc3\xb3n')),
                ('Aprobado', models.BooleanField(default=False, verbose_name=b'Curso Aprobado')),
                ('Encuesta', models.BooleanField(default=False, verbose_name=b'Encuesta respondida')),
                ('MatriculaCiclo', models.ForeignKey(verbose_name=b'Alumno', to='Matriculas.MatriculaCiclo')),
                ('PeriodoCurso', models.ForeignKey(verbose_name=b'Curso', to='Cursos.PeriodoCurso')),
            ],
            options={
                'verbose_name': 'Matr\xedcula en Cursos UDL',
                'verbose_name_plural': 'Matr\xedcula en Cursos UDL',
                'permissions': (('read_matriculacursos', 'Observar Matricula de Cursos UDL'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='matriculacursos',
            unique_together=set([('MatriculaCiclo', 'PeriodoCurso')]),
        ),
        migrations.AlterUniqueTogether(
            name='matriculaciclo',
            unique_together=set([('Alumno', 'Periodo')]),
        ),
    ]
