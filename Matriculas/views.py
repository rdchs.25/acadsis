# Create your views here.
# -*- coding: utf-8 -*-
import datetime
import json
import urllib2
from decimal import Decimal

from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect

from Alumnos.models import Alumno
from Alumnos.reportes2 import generar_cuentas_correo
from Carreras.models import Carrera
from Categorias.models import Categoria
from Cursos.models import PeriodoCurso, Curso
from Matriculas.forms import MatriculaCicloForm, IndexMatriculaForm, MatriculaCicloSeguroForm, MatriculaCursosForm, \
    EditMatriculaCursosForm
from Matriculas.models import CONDICION_CHOICES
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from Matriculas.reportes import print_ficha_matricula, print_constancia_matricula, print_ficha_notas1, \
    print_ficha_notas, archivo_promedio_cursos, archivo_promedio_ponderado, print_reporte_asistencia, \
    print_reporte_asistencias_pdf, print_reporte_notas, print_certificado_estudios, \
    print_certificado_estudios_egresado, print_historial_academico, print_historial_categorias, avance_plan_estudios, \
    datos_matriculados_excel, listado_categorias_excel, print_certificado_estudios_simple
from Matriculas.reportes2 import archivo_categorizacion
from Notas.models import Nota, NotasAlumno
from Pagos.models import PagoAlumno
from Periodos.models import ANIO_CHOICES, SEMESTRE_CHOICES
from Periodos.models import Periodo


def obtener_categorias(request, periodo_id):
    if periodo_id != "":
        return HttpResponse(
            serializers.serialize('json', Categoria.objects.filter(Periodo__id=periodo_id), fields=('pk', 'Categoria'),
                                  extras=('__unicode__',)))


def redireccionar_matriculas(request):
    return HttpResponseRedirect('/admin/Matriculas/matriculacursos/')


@csrf_protect
def index_matriculas(request):
    if request.user.is_authenticated() and request.user.has_perm(
            'Matriculas.read_matriculacursos') or request.user.has_perm(
        'Matriculas.change_matriculacursos') or request.user.has_perm('Matriculas.add_matriculacursos'):
        if request.method == 'POST':
            form = IndexMatriculaForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("individual/periodo/" + str(periodo.id) + "/")
        else:
            form = IndexMatriculaForm()
        return render_to_response('Matriculas/index.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def buscar_alumno_matriculado(request, idperiodo):
    if request.user.is_authenticated() and request.user.has_perm(
            'Matriculas.read_matriculacursos') or request.user.has_perm(
        'Matriculas.add_matriculacursos') or request.user.has_perm('Matriculas.change_matriculacursos'):
        if request.method == 'POST':
            action = request.POST['action']
            all = request.POST['select_across']
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')

            if all == '0':
                matriculados_ciclo = request.POST.getlist('_selected_action')
            else:
                matriculados_ciclo = []
                ids_mat = MatriculaCiclo.objects.filter(Periodo__id=idperiodo).order_by('Alumno__Carrera__Carrera',
                                                                                        'Alumno__ApellidoPaterno',
                                                                                        'Alumno__ApellidoMaterno',
                                                                                        'Alumno__Nombres')
                if query1:
                    ids_mat1 = ids_mat.filter(Alumno__Carrera__id=query1)
                else:
                    ids_mat1 = ids_mat

                if query2:
                    ids_mat2 = ids_mat1.filter(Alumno__AnioIngreso=query2)
                else:
                    ids_mat2 = ids_mat1

                if query3:
                    ids_mat3 = ids_mat2.filter(Alumno__Semestre=query3)
                else:
                    ids_mat3 = ids_mat2

                if query4:
                    ids_mat4 = ids_mat3.filter(Estado=query4)
                else:
                    ids_mat4 = ids_mat3

                for id_mat in ids_mat4:
                    matriculados_ciclo.append(id_mat.id)
            alumnos_carnet = []
            matriculados = []

            for mat in matriculados_ciclo:
                alumno = MatriculaCiclo.objects.get(id=mat)
                alumnos_carnet.append(alumno)

            for mat in matriculados_ciclo:
                cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=mat)
                if cursos_matriculado.count() > 0:
                    matriculados.append(cursos_matriculado)
            if action == "print_matricula_selected":
                return print_ficha_matricula(matriculados)
            elif action == "print_constancia_matricula_selected":
                return print_constancia_matricula(matriculados)
            elif action == "print_notas_selected":
                return print_ficha_notas(matriculados)
            elif action == "print_notas1_selected":
                return print_ficha_notas1(matriculados)
            elif action == "promedios_alumnos_selected":
                return archivo_promedio_cursos(matriculados)
            elif action == "promedios_ponderados_alumnos_selected":
                return archivo_promedio_ponderado(matriculados)
            elif action == "categorizacion_alumnos_selected":
                return archivo_categorizacion(matriculados_ciclo)
            elif action == "print_asistencia_selected":
                return print_reporte_asistencia(matriculados)
            elif action == "print_reporte_asistencias_selected":
                return print_reporte_asistencias_pdf(matriculados)
            elif action == "print_reporte_notas_selected":
                return print_reporte_notas(matriculados)
            elif action == "print_certificado_estudios_selected":
                return print_certificado_estudios(matriculados_ciclo)
            elif action == "print_certificado_estudios_egresado_selected":
                return print_certificado_estudios_egresado(matriculados_ciclo)
            elif action == "print_certificado_estudios_simple_selected":
                return print_certificado_estudios_simple(matriculados_ciclo)
            elif action == "print_historial_academico_selected":
                return print_historial_academico(matriculados_ciclo)
            elif action == "print_historial_categorias_selected":
                return print_historial_categorias(matriculados_ciclo)
            elif action == "print_avance_plan_estudios_selected":
                return avance_plan_estudios(alumnos_carnet)
            elif action == "print_datos_matriculados_selected":
                return datos_matriculados_excel(alumnos_carnet)
            elif action == "print_categorias_matriculados_selected":
                return listado_categorias_excel(alumnos_carnet)
            elif action == "generar_cuentas_correo":
                return generar_cuentas_correo(alumnos_carnet)
            else:
                return HttpResponseRedirect('../../../../../../')
        else:
            query = request.GET.get('q', '')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')

            carreras = Carrera.objects.all()
            anios = []
            semestres = []
            condiciones = []

            for i in ANIO_CHOICES:
                anios.append(i[0])

            for s in SEMESTRE_CHOICES:
                semestres.append(s[0])

            for c in CONDICION_CHOICES:
                condiciones.append(c[0])

            try:
                periodo = Periodo.objects.get(id=idperiodo)
            except Periodo.DoesNotExist:
                mensaje = "El periodo elegido no es correcto"
                links = "<a href='../../../'>Volver</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

            results = MatriculaCiclo.objects.filter(Periodo=periodo)

            if query1:
                results1 = results.filter(Alumno__Carrera__id=query1)
            else:
                results1 = results

            if query2:
                results2 = results1.filter(Alumno__AnioIngreso=query2)
            else:
                results2 = results1

            if query3:
                results3 = results2.filter(Alumno__Semestre=query3)
            else:
                results3 = results2

            if query4:
                results4 = results3.filter(Estado=query4)
            else:
                results4 = results3

            if query:
                str_q_nombres = "Q(Alumno__Nombres__icontains = "
                str_q_apellidopaterno = "Q(Alumno__ApellidoPaterno__icontains = "
                str_q_apellidomaterno = "Q(Alumno__ApellidoMaterno__icontains = "
                lista_query = query.split(" ")
                g = ""
                for i in lista_query:
                    g = g + "Q(Alumno__Nombres__icontains = \"%s\")|Q(Alumno__ApellidoPaterno__icontains = \"%s\")|Q(Alumno__ApellidoMaterno__icontains = \"%s\")|" % (
                        i, i, i)

                qset = (
                        Q(Alumno__Codigo__icontains=query) |
                        eval(g[:-1])
                )
                results5 = results4.filter(qset).order_by('Alumno__Carrera__Carrera', 'Alumno__ApellidoPaterno',
                                                          'Alumno__ApellidoMaterno', 'Alumno__Nombres')
            else:
                results5 = results4.order_by('Alumno__Carrera__Carrera', 'Alumno__ApellidoPaterno',
                                             'Alumno__ApellidoMaterno', 'Alumno__Nombres')

            n_alumnos = results5.count()
            paginator = Paginator(results5, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Matriculas/Individual/buscar_matriculado.html",
                                      {"results": results, "paginator": paginator, "periodo": periodo, "query": query,
                                       "query1": query1, "query2": query2, "query3": query3, "query4": query4,
                                       "carreras": carreras, "anios": anios, "semestres": semestres,
                                       "condiciones": condiciones, "n_alumnos": n_alumnos, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../../')


def buscar_ingresante_matricular(request, idperiodo):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.add_matriculaciclo'):
        query = request.GET.get('q', '')
        query1 = request.GET.get('c', '')
        query2 = request.GET.get('p', '')
        query3 = request.GET.get('s', '')
        query4 = request.GET.get('m', '')

        carreras = Carrera.objects.all()
        anios = []
        semestres = []
        modalidades = []

        for i in ANIO_CHOICES:
            anios.append(i[0])

        for s in SEMESTRE_CHOICES:
            semestres.append(s[0])

        for m in SEMESTRE_CHOICES:
            modalidades.append(m[0])

        try:
            periodo = Periodo.objects.get(id=idperiodo)
        except Periodo.DoesNotExist:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../matriculacursos/'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        results = Alumno.objects.filter(Egreso=False).exclude(matriculaciclo__Periodo__id=idperiodo)

        if query1:
            results1 = results.filter(Carrera__id=query1)
        else:
            results1 = results

        if query2:
            results2 = results1.filter(AnioIngreso=query2)
        else:
            results2 = results1

        if query3:
            results3 = results2.filter(Semestre=query3)
        else:
            results3 = results2

        if query4:
            results4 = results3.filter(Modalidad=query4)
        else:
            results4 = results3

        if query:
            qset = (
                    Q(Codigo__icontains=query) |
                    Q(Nombres__icontains=query) |
                    Q(ApellidoPaterno__icontains=query) |
                    Q(ApellidoMaterno__icontains=query)
            )
            results5 = results4.filter(qset).order_by('Carrera__Carrera', 'ApellidoPaterno', 'ApellidoMaterno',
                                                      'Nombres')
        else:
            results5 = results4.order_by('Carrera__Carrera', 'ApellidoPaterno', 'ApellidoMaterno', 'Nombres')

        n_alumnos = results5.count()
        paginator = Paginator(results5, 100)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)

        return render_to_response("Matriculas/Individual/buscar_ingresante_matricular.html",
                                  {"results": results, "paginator": paginator, "query": query, "query1": query1,
                                   "query2": query2, "query3": query3, "query4": query4, "carreras": carreras,
                                   "anios": anios, "semestres": semestres, "modalidades": modalidades,
                                   "n_alumnos": n_alumnos, "periodo": periodo, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def agregar_matriculaciclo(request, idperiodo, idalumno):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.add_matriculaciclo'):
        try:
            periodo = Periodo.objects.get(id=idperiodo)
        except Periodo.DoesNotExist:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../matriculacursos/'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        try:
            alumno = Alumno.objects.get(id=idalumno)
        except Alumno.DoesNotExist:
            mensaje = "El ingresante elegido no es correcto"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        existe_matricula = MatriculaCiclo.objects.filter(Periodo=periodo, Alumno=alumno)
        if existe_matricula.count() != 0:
            mensaje = u'El alumno(a) %s ya se encuentra matriculado' % alumno
            links = "<a href='../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        categorias = Categoria.objects.filter(Periodo__id=idperiodo)
        if request.method == 'POST':
            form = MatriculaCicloForm(categorias, request.POST)
            if form.is_valid():
                categoria = form.cleaned_data['Categoria']
                fecha_matricula = form.cleaned_data['FechaMatricula']
                condicion = form.cleaned_data['Condicion']
                fecha_retiro = form.cleaned_data['FechaRetiro']
                observaciones = form.cleaned_data['Observaciones']

                nueva_matricula = MatriculaCiclo(Periodo=periodo, Alumno=alumno, Categoria=categoria,
                                                 FechaMatricula=fecha_matricula, Estado=condicion,
                                                 FechaRetiro=fecha_retiro, Observaciones=observaciones)
                nueva_matricula.save()

            return HttpResponseRedirect('../../../../matriculacursos/individual/periodo/%s/' % periodo.id)
        else:
            form = MatriculaCicloForm(categorias=categorias,
                                      initial={'FechaMatricula': datetime.date.today(), 'Condicion': 'Matriculado'})

        return render_to_response("Matriculas/Individual/agregar_matriculaciclo.html",
                                  {"alumno": alumno, "periodo": periodo, "form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_matriculaciclo(request, idmatriculaciclo):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.change_matriculaciclo'):
        try:
            matriculaciclo = MatriculaCiclo.objects.get(id=idmatriculaciclo)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "La matricula elegida no es correcta"
            links = "<a href='../../matriculacursos/'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        categorias = Categoria.objects.filter(Periodo__id=matriculaciclo.Periodo.id).order_by('Categoria')
        if request.method == 'POST':
            form = MatriculaCicloForm(categorias, request.POST)
            if form.is_valid():
                categoria = form.cleaned_data['Categoria']
                fecha_matricula = form.cleaned_data['FechaMatricula']
                condicion = form.cleaned_data['Condicion']
                fecha_retiro = form.cleaned_data['FechaRetiro']
                observaciones = form.cleaned_data['Observaciones']
                exoneradoidiomas = form.cleaned_data['Exoneradoidiomas']
                activadoproyectoformativo = form.cleaned_data['MatriculaProyectoFormativo']

                matriculaciclo.Categoria = categoria
                matriculaciclo.FechaMatricula = fecha_matricula
                matriculaciclo.Estado = condicion
                matriculaciclo.FechaRetiro = fecha_retiro
                matriculaciclo.Observaciones = observaciones
                matriculaciclo.Exoneradoidiomas = exoneradoidiomas
                matriculaciclo.MatriculaProyectoFormativo = activadoproyectoformativo
                matriculaciclo.save()

                return HttpResponseRedirect('../../matriculacursos/individual/periodo/%s/' % matriculaciclo.Periodo.id)
        else:
            form = MatriculaCicloForm(categorias=categorias, initial={'Categoria': matriculaciclo.Categoria,
                                                                      'FechaMatricula': matriculaciclo.FechaMatricula,
                                                                      'Condicion': matriculaciclo.Estado,
                                                                      'FechaRetiro': matriculaciclo.FechaRetiro,
                                                                      'Exoneradoidiomas': matriculaciclo.Exoneradoidiomas,
                                                                      'MatriculaProyectoFormativo': matriculaciclo.MatriculaProyectoFormativo,
                                                                      'Observaciones': matriculaciclo.Observaciones})

        return render_to_response("Matriculas/Individual/agregar_matriculaciclo.html",
                                  {"alumno": matriculaciclo.Alumno, "periodo": matriculaciclo.Periodo, "form": form,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../'>Volver</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_seguro(request, idmatriculaciclo):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.change_seguro_matriculaciclo'):
        try:
            matriculaciclo = MatriculaCiclo.objects.get(id=idmatriculaciclo)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "La matricula elegida no es correcta"
            links = "<a href='../../../matriculacursos/'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = MatriculaCicloSeguroForm(request.POST)
            if form.is_valid():
                tieneseguro = form.cleaned_data['TieneSeguro']
                copiadni = form.cleaned_data['CopiaDni']
                reciboluzagua = form.cleaned_data['ReciboLuzAgua']

                if tieneseguro is True:
                    copiadni = False
                    reciboluzagua = False
                else:
                    pass

                matriculaciclo.TieneSeguro = tieneseguro
                matriculaciclo.CopiaDni = copiadni
                matriculaciclo.ReciboLuzAgua = reciboluzagua
                matriculaciclo.save()

                return HttpResponseRedirect(
                    '../../../matriculacursos/individual/periodo/%s/' % matriculaciclo.Periodo.id)
        else:
            form = MatriculaCicloSeguroForm(
                initial={'TieneSeguro': matriculaciclo.TieneSeguro, 'CopiaDni': matriculaciclo.CopiaDni,
                         'ReciboLuzAgua': matriculaciclo.ReciboLuzAgua})

        return render_to_response("Matriculas/Individual/agregar_seguro.html",
                                  {"alumno": matriculaciclo.Alumno, "periodo": matriculaciclo.Periodo, "form": form,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../../'>Volver</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def cursos_matriculado(request, idmatriculadociclo):
    if request.user.is_authenticated() and request.user.has_perm(
            'Matriculas.read_matriculacursos') or request.user.has_perm(
        'Matriculas.add_matriculacursos') or request.user.has_perm('Matriculas.change_matriculacursos'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculadociclo)
            cursos = MatriculaCursos.objects.filter(MatriculaCiclo=idmatriculadociclo).order_by('Convalidado',
                                                                                                '-PeriodoCurso__Curso__Ciclo')
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        query = request.GET.get('q', '')
        query1 = request.GET.get('p', '')

        if query and query1:
            cursos = MatriculaCursos.objects.filter(MatriculaCiclo=idmatriculadociclo,
                                                    PeriodoCurso__Curso__Nombre__icontains=query,
                                                    Estado=query1).order_by('Convalidado',
                                                                            '-PeriodoCurso__Curso__Ciclo')
        elif query:
            cursos = MatriculaCursos.objects.filter(MatriculaCiclo=idmatriculadociclo,
                                                    PeriodoCurso__Curso__Nombre__icontains=query)('Convalidado',
                                                                                                  '-PeriodoCurso__Curso__Ciclo')
        elif query1:
            cursos = MatriculaCursos.objects.filter(MatriculaCiclo=idmatriculadociclo, Estado=query1)('Convalidado',
                                                                                                      '-PeriodoCurso__Curso__Ciclo')

        return render_to_response("Matriculas/Individual/cursos_matriculado.html",
                                  {"matriculado": matriculado, "cursos": cursos, "query": query, "query1": query1,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def delete_cursos_matriculado(request, idcursomatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.delete_matriculacursos'):
        cursomatriculado = MatriculaCursos.objects.get(id=idcursomatriculado)
        if request.method == 'POST':
            cursomatriculado.delete()
            html = """<html><head>
                <script language="JavaScript">
                alert("REGISTRO ACTUALIZADO");
                window.opener.document.location.reload();
                self.close();
                </script>
                </head><body></body></html>"""
            return HttpResponse(html)
        return render_to_response("Matriculas/Individual/delete_cursos_matriculado.html",
                                  {"cursomatriculado": cursomatriculado, }, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def add_cursos_matriculado(request, idmatriculadociclo):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.add_matriculacursos'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculadociclo)
            cursos_aprobados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno=matriculado.Alumno, Aprobado=True)
            cursos_registrados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno=matriculado.Alumno)
            cursos_convalidados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno=matriculado.Alumno,
                                                                 Convalidado=True)
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        plan_estudios = Curso.objects.filter(Periodo=matriculado.Periodo, Carrera=matriculado.Alumno.Carrera).order_by(
            'Ciclo', 'Nombre')
        cursos_disponibles = []
        ver_todo = 1
        for cur in plan_estudios:
            if ver_todo == 1:
                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                if periodocurso.count() != 0:
                    for percur in periodocurso:
                        cursos_disponibles.append(percur)
            else:
                n_equi = cur.Equivalencias.count()
                i = 1
                if n_equi != 0:
                    for equi in cur.Equivalencias.all():
                        curso_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=equi,
                                                                        MatriculaCiclo__Alumno=matriculado.Alumno,
                                                                        Aprobado=True)
                        if curso_aprobado.count() == 0:
                            if i == n_equi:
                                n_pre = cur.Prerequisitos.count()
                                j = 1
                                if n_pre != 0:
                                    for pre in cur.Prerequisitos.all():
                                        curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                                            MatriculaCiclo__Alumno=matriculado.Alumno,
                                                                                            Aprobado=True)
                                        if curso_pre_aprobado.count() != 0:
                                            periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                            if periodocurso.count() != 0:
                                                for percur in periodocurso:
                                                    cursos_disponibles.append(percur)
                                            break
                                else:
                                    periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                    if periodocurso.count() != 0:
                                        for percur in periodocurso:
                                            cursos_disponibles.append(percur)
                            else:
                                i += 1
                else:
                    n_pre = cur.Prerequisitos.count()
                    j = 1
                    if n_pre != 0:
                        for pre in cur.Prerequisitos.all():
                            curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                                MatriculaCiclo__Alumno=matriculado.Alumno,
                                                                                Aprobado=True)
                            if curso_pre_aprobado.count() != 0:
                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                if periodocurso.count() != 0:
                                    for percur in periodocurso:
                                        cursos_disponibles.append(percur)
                                break
                    else:
                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                        if periodocurso.count() != 0:
                            for percur in periodocurso:
                                cursos_disponibles.append(percur)

        cursos = cursos_disponibles

        if request.method == 'POST':

            form = MatriculaCursosForm(request.POST)

            if form.is_valid():
                matriculaciclo = form.cleaned_data['MatriculaCiclo']
                periodocursos = form.cleaned_data['PeriodoCurso']
                fechamatricula = form.cleaned_data['FechaMatricula']
                convalidacion = form.cleaned_data['Convalidacion']

                for periodocurso in periodocursos:
                    periodocurso_existe = MatriculaCursos.objects.filter(PeriodoCurso=periodocurso,
                                                                         MatriculaCiclo__id=matriculaciclo)
                    if periodocurso_existe.count() == 0:
                        save_periodocurso = MatriculaCursos(PeriodoCurso=periodocurso, MatriculaCiclo_id=matriculaciclo,
                                                            Convalidado=convalidacion, FechaMatricula=fechamatricula)
                        save_periodocurso.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Matriculas/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = MatriculaCursosForm(
                initial={'MatriculaCiclo': idmatriculadociclo, 'FechaMatricula': datetime.date.today()})

        return render_to_response("Matriculas/Individual/add_cursos_matriculado.html",
                                  {"matriculado": matriculado, "cursos": cursos, "form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_cursos_matriculado(request, matricula_curso_id):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.change_matriculacursos'):
        try:
            matricula_curso = MatriculaCursos.objects.get(id=matricula_curso_id)
        except MatriculaCursos.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = EditMatriculaCursosForm(request.POST)
            if form.is_valid():
                fechamatricula = form.cleaned_data['FechaMatricula']
                convalidacion = form.cleaned_data['Convalidacion']
                resolucion = form.cleaned_data['Resolucion']
                estado = form.cleaned_data['Estado']
                fecharetiro = form.cleaned_data['FechaRetiro']

                matricula_curso.FechaMatricula = fechamatricula
                matricula_curso.Convalidado = convalidacion
                matricula_curso.Resolucion = resolucion
                matricula_curso.Estado = estado
                matricula_curso.FechaRetiro = fecharetiro
                matricula_curso.Aprobado = matricula_curso.aprobado()
                matricula_curso.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Matriculas/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = EditMatriculaCursosForm(
                initial={"FechaMatricula": matricula_curso.FechaMatricula, "Convalidacion": matricula_curso.Convalidado,
                         "Resolucion": matricula_curso.Resolucion, "Estado": matricula_curso.Estado,
                         "FechaRetiro": matricula_curso.FechaRetiro})

        return render_to_response("Matriculas/Individual/editar_cursos_matriculado.html",
                                  {"form": form, "user": request.user, "matricula_curso": matricula_curso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def detalle_curso_matriculado(request, id_matriculadocurso):
    if request.user.is_authenticated():
        try:
            matriculado_curso = MatriculaCursos.objects.get(id=id_matriculadocurso)
        except MatriculaCursos.DoesNotExist:
            mensaje = "El alumno no esta matriculado en el curso o no tiene permiso de entrada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("Matriculas/Individual/detalle_curso_matriculado.html",
                                  {"matriculado_curso": matriculado_curso, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../../')


def exportar_notas_excel(request, idperiodocurso):
    import StringIO
    output = StringIO.StringIO()

    matriculados = MatriculaCursos.objects.filter(PeriodoCurso__id=idperiodocurso, Estado=True,
                                                  Convalidado=False).order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                                              'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                                              'MatriculaCiclo__Alumno__Nombres')
    notas = Nota.objects.filter(SubNotas=False, PeriodoCurso__id=idperiodocurso).order_by('Orden')
    notas1 = Nota.objects.filter(SubNotas=True, PeriodoCurso__id=idperiodocurso).order_by('Orden')
    from xlwt import Workbook
    book = Workbook()
    sheet1 = book.add_sheet('Notas')

    sheet1.write(0, 0, 'Nro')
    sheet1.write(0, 1, 'Codigo')
    sheet1.write(0, 2, 'Estudiante')
    sheet1.write(0, 3, 'Carrera')

    k = 4
    for nota in notas:
        sheet1.write(0, k, nota.Identificador)
        k += 1

    sheet1.write(0, k, 'PF')

    j = 4
    i = 1

    for mat in matriculados:
        sheet1.write(i, 0, i)
        sheet1.write(i, 1, mat.MatriculaCiclo.Alumno.Codigo)
        sheet1.write(i, 2, mat.MatriculaCiclo.Alumno.__unicode__())
        sheet1.write(i, 3, mat.MatriculaCiclo.Alumno.Carrera.Carrera)
        for nota in notas:
            try:
                nota_alumno = NotasAlumno.objects.get(MatriculaCursos=mat, Nota=nota)
                if nota_alumno.Valor == Decimal('0.00'):
                    sheet1.write(i, j, '')
                else:
                    sheet1.write(i, j, Decimal(nota_alumno.Valor))
            except:
                sheet1.write(i, j, '')
            j += 1
        sheet1.write(i, j, mat.Promedio())
        i += 1
        j = 4
    l = i
    for nota in notas:
        sheet1.write(i + 5, 1, '%s:' % nota.Identificador)
        sheet1.write(i + 5, 2, '%s' % nota.Nota)
        i += 1

    for nota in notas1:
        subnotas = Nota.objects.filter(NotaPadre=nota)
        k = 1
        formula = ''
        suma = Decimal('0.00')
        for subnota in subnotas:
            if k == 1:
                formula += '%s = (%s*%s' % (nota.Identificador, subnota.Identificador, nota.Peso)
            else:
                formula += '+ %s*%s' % (subnota.Identificador, nota.Peso)
            k += 1
            suma += Decimal(nota.Peso)
        formula += ')/%s' % str(suma)
        sheet1.write(l + 5, 4, formula)
        l += 1

    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), mimetype='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=notas_excel.xls'
    return response


CREDITOS_CARRERA = {'AT': 24, 'AM': 23, 'IA': 24, 'IC': 24, 'IS': 23}
CREDITOS_CARRERA_ESPECIAL = {'AT': 24, 'AM': 23, 'IA': 24, 'IC': 24, 'IS': 23}
MONTO_CREDITO = Decimal('17.50')
CREDITOS_MAXIMOS = 26


def asistente_add_cursos(request, idmatriculadociclo):
    if request.user.is_authenticated() and request.user.has_perm('Matriculas.add_matriculacursos'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculadociclo)
            cursos_aprobados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno=matriculado.Alumno, Aprobado=True)
            cursos_registrados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno=matriculado.Alumno)
            cursos_convalidados = MatriculaCursos.objects.filter(MatriculaCiclo__Alumno=matriculado.Alumno,
                                                                 Convalidado=True)
            estudiante = matriculado.Alumno
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        periodo = matriculado.Periodo

        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        try:
            matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "Usted aún no ha pagado su derecho de matrícula, debe regularizar sus pagos."
            links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
            return render_to_response("Matriculas/Individual/mensaje.html",
                                      {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                       "user": request.user})

        matriculado_cursos = MatriculaCursos.objects.filter(MatriculaCiclo=matriculaciclo).exclude(Convalidado=True)
        if matriculado_cursos.count() != 0:
            mensaje = "Usted ya se encuentra matriculado en cursos."
            links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
            return render_to_response("Matriculas/Individual/mensaje.html",
                                      {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                       "user": request.user})

        orcos = []

        # pasa saltar validacion de ingles por nueva curricula
        estudiante = matriculaciclo.Alumno
        ciclo_ingreso = estudiante.AnioIngreso + '-' + estudiante.Semestre

        # no es obligatorio que los alumnos del 1ero y 2do ciclo lleven inglés en el instituo de idiomas.
        # CAMBIAR ESTOS DATOS EN CADA MATRICULA!!!
        saltaringles = False
        if ciclo_ingreso == '2018-I' or ciclo_ingreso == '2018-II':
            saltaringles = True

        # validamos que el alumno de segundo ciclo haya pagado el concepto de pago de libro para inglés básico.
        isexonerado = matriculaciclo.Exoneradoidiomas
        if ciclo_ingreso == '2018-I' and isexonerado is False:
            json_string = urllib2.urlopen(settings.URL_IDIOMAS + "/json3/").read()
            pagaron_libro = json.loads(json_string)
            pago_libro = False
            for datoidioma in pagaron_libro:
                if estudiante.Codigo == datoidioma["usuario"]:
                    pago_libro = True
            if pago_libro is False:
                mensaje = "Estimado alumno, recibe nuestro saludo y te informamos que debes acercarte a la " \
                          "Oficina del Instituto de Idiomas ( 4º piso UDL) para adquirir tu material de la " \
                          "asignatura de Inglés Básico y luego podrás registrarte en las asignaturas del " \
                          "semestre 2018-II"
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                return render_to_response("Matriculas/Individual/mensaje.html",
                                          {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                           "user": request.user})

        # proyectos formativos
        curso_formativo_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso__Proyecto_formativo=True,
                                                                  MatriculaCiclo__Alumno=estudiante, Aprobado=True)

        isactivoproyectoformativo = matriculaciclo.MatriculaProyectoFormativo
        if (ciclo_ingreso == '2017-I') and (curso_formativo_aprobado.count() == 0) and (
                isactivoproyectoformativo is False):
            mensaje = "Estimado alumno, recibe nuestro saludo y te informamos que debes acreditar haber " \
                      "llevado Proyectos Formativos. Para mas información acercarte a Oficina de " \
                      "Bienestar Universitario"
            links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
            return render_to_response("Matriculas/Individual/mensaje.html",
                                      {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                       "user": request.user})

        try:
            # Verificar Idiomas
            json_string = urllib2.urlopen(settings.URL_IDIOMAS + "/json2/%s/" % "2018").read()
            datos_idiomas = json.loads(json_string)
            egresadosidiomas = Alumno.objects.using('idiomas').filter(Egresoidiomas=True)
            isexonerado = matriculaciclo.Exoneradoidiomas

            usuarios_idiomas = []

            # Buscamos en egresados de idiomas
            for egresado in egresadosidiomas:
                if estudiante.Codigo == egresado.Codigo:
                    usuarios_idiomas.append(egresado.Codigo)
                    break
            # Buscamos en matriculados en idiomas
            for datoidioma in datos_idiomas:
                if estudiante.Codigo == datoidioma["usuario"]:
                    usuarios_idiomas.append(datoidioma["usuario"])
                    break

            buscar_alumnos = MatriculaCiclo.objects.filter(Periodo=periodo, Alumno__Codigo__in=usuarios_idiomas)

            pago_var = PagoAlumno.objects.filter(MatriculaCiclo__Periodo=periodo,
                                                 MatriculaCiclo__Alumno=estudiante,
                                                 ConceptoPago__Descripcion__icontains='MATRICULA', Estado=True)
            recordatorio = True
            if (buscar_alumnos.count() == 0 and isexonerado is False) and (pago_var.count() == 0):
                mensaje = "Usted aún no se ha registrado en un grupo de Inglés. " \
                          "Tampoco ha pagado su derecho de matrícula, debe regularizar sus pagos."
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                return render_to_response("Matriculas/Individual/mensaje.html",
                                          {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                           "links": mark_safe(links), "user": request.user})
            if (buscar_alumnos.count() == 0) and (saltaringles is False) and (isexonerado is False):
                mensaje = "Usted aún no se ha registrado en un grupo de Inglés."
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                recordatorio = False
                return render_to_response("Matriculas/Individual/mensaje.html",
                                          {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                           "links": mark_safe(links), "user": request.user})
            if pago_var.count() == 0:
                mensaje = "Usted aún no ha pagado su derecho de matrícula, debe regularizar sus pagos."
                links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                return render_to_response("Matriculas/Individual/mensaje.html",
                                          {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                           "links": mark_safe(links), "user": request.user})
            if estudiante.Codigo in orcos:
                mensaje = "Hemos detectado una irregularidad en su historial académico, debe ir donde su " \
                          "coordinador de Carrera o Decano para inscribirse manualmente."
                links = ""
                recordatorio = False
                return render_to_response("Matriculas/Individual/mensaje.html",
                                          {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                           "links": mark_safe(links), "user": request.user})

            pago = pago_var[0]

            creditos_carrera = CREDITOS_CARRERA
            creditos_maximos = 26

            plan_estudios = Curso.objects.filter(Plan_id=estudiante.Plan_id, Periodo=periodo,
                                                 Carrera=estudiante.Carrera, Extracurricular=False,
                                                 Proyecto_formativo=False).order_by('Ciclo', 'Nombre')
            cursos_disponibles = []
            creditos_llevados = 0
            categoria = pago.MatriculaCiclo.Categoria
            creditos_regulares = creditos_carrera['%s' % estudiante.Carrera.Codigo]
            costo_cred = Decimal('17.50')
            for cur in plan_estudios:
                n_equi = cur.Equivalencias.count()
                i = 1
                if n_equi != 0:
                    for equi in cur.Equivalencias.all():
                        curso_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=equi,
                                                                        MatriculaCiclo__Alumno=estudiante,
                                                                        Aprobado=True)
                        if curso_aprobado.count() == 0:
                            if i == n_equi:
                                n_pre = cur.Prerequisitos.count()
                                j = 1
                                if n_pre != 0:
                                    for pre in cur.Prerequisitos.all():
                                        curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                                            MatriculaCiclo__Alumno=estudiante,
                                                                                            Aprobado=True)
                                        if curso_pre_aprobado.count() != 0:
                                            creditos = creditos_llevados + cur.Creditos
                                            if creditos >= creditos_maximos:
                                                pass
                                            else:
                                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                                if periodocurso.count() != 0:
                                                    cursos_disponibles.append(periodocurso[0])
                                                    creditos_llevados += cur.Creditos
                                            break
                                else:
                                    creditos = creditos_llevados + cur.Creditos
                                    if creditos >= creditos_maximos:
                                        pass
                                    else:
                                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                        if periodocurso.count() != 0:
                                            cursos_disponibles.append(periodocurso[0])
                                            creditos_llevados += cur.Creditos
                            else:
                                i += 1
                        else:
                            break
                else:
                    creditos = creditos_llevados + cur.Creditos
                    if creditos >= creditos_maximos:
                        pass
                    else:
                        n_pre = cur.Prerequisitos.count()
                        j = 1
                        if n_pre != 0:
                            for pre in cur.Prerequisitos.all():
                                curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                                    MatriculaCiclo__Alumno=estudiante,
                                                                                    Aprobado=True)
                                if curso_pre_aprobado.count() != 0:
                                    creditos = creditos_llevados + cur.Creditos
                                    if creditos >= creditos_maximos:
                                        pass
                                    else:
                                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                        if periodocurso.count() != 0:
                                            cursos_disponibles.append(periodocurso[0])
                                            creditos_llevados += cur.Creditos
                                    break
                        else:
                            creditos = creditos_llevados + cur.Creditos
                            if creditos >= creditos_maximos:
                                pass
                            else:
                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                if periodocurso.count() != 0:
                                    cursos_disponibles.append(periodocurso[0])
                                    creditos_llevados += cur.Creditos

            cursos = cursos_disponibles
            cursos_ciclo = {}
            for i in range(1, 10):
                contador = 0
                for cur in cursos:
                    if int(cur.Curso.Ciclo) == int(i):
                        contador += 1
                if contador != 0:
                    cursos_ciclo[i] = contador
            print costo_cred
            return render_to_response("Matriculas/Individual/asistente_add_cursos.html",
                                      {"results": cursos, "cursos_ciclo": cursos_ciclo, "estudiante": estudiante,
                                       "creditos": creditos_llevados, "categoria": categoria,
                                       "creditos_regulares": creditos_regulares, "costo_cred": str(costo_cred),
                                       "periodo": periodo, "user": request.user},
                                      context_instance=RequestContext(request))
        except PagoAlumno.DoesNotExist:
            mensaje = "El alumno aún no se ha registrado en un grupo de Inglés."
            # mensaje = "El alumno aún no ha pagado su derecho de matrícula, debe regularizar sus pagos."
            links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
            return render_to_response("Matriculas/Online/mensaje.html",
                                      {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                       "user": request.user})
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


@csrf_exempt
def asistente_registrar_cursos(request, idmatriculadociclo):
    if request.user.is_authenticated():
        try:
            matriculaciclo = MatriculaCiclo.objects.get(id=idmatriculadociclo)

        except Periodo.DoesNotExist:
            mensaje = "Error, No ha pagado su matricula, verifique por favor."
            return HttpResponse(mensaje)

        periodo = matriculaciclo.Periodo
        estudiante = matriculaciclo.Alumno

        vcursos = request.POST['vcursos']
        rsp = request.POST['vcursos'].split('-')
        pps = {}
        creditos_matriculados = 0
        total_electivos = 0

        for r in rsp:
            if r != '':
                try:
                    periodocurso = PeriodoCurso.objects.get(id=r)
                except PeriodoCurso.DoesNotExist:
                    mensaje = "Se encontraron errores al procesar la matricula."
                    return HttpResponse(mensaje)
                # Revisar Electivos
                electivo = periodocurso.Curso.Codigo.split("-")
                if len(electivo) > 1:
                    total_electivos += 1
                creditos_matriculados += int(periodocurso.Curso.Creditos)

                # if total_electivos > 1:
                # mensaje = "No puedes llevar más de un curso electivo, por favor escoge uno solo"
                # return HttpResponse(mensaje)

        if creditos_matriculados > CREDITOS_MAXIMOS:
            mensaje = "ERROR, No puedes llevar mas de %s creditos." % CREDITOS_MAXIMOS
            return HttpResponse(mensaje)

        coordinador = User.objects.get(id=request.user.id)
        log_matricula = '%s,%s,%s %s,%s,%s,%s,%s,%s,' % (
            coordinador, estudiante.Codigo, estudiante.ApellidoPaterno, estudiante.ApellidoMaterno, estudiante.Nombres,
            estudiante.Carrera.Codigo, matriculaciclo.Categoria.Categoria, matriculaciclo.Categoria.Pago,
            creditos_matriculados)
        log_cursos = []
        hoy = datetime.datetime.now()
        fecha_date = hoy.strftime('%Y-%m-%d')
        fecha_hora = hoy.strftime('%H:%M')

        creditos_matriculados = 0
        for r in rsp:
            if r != '':
                try:
                    periodocurso = PeriodoCurso.objects.get(id=r)
                except PeriodoCurso.DoesNotExist:
                    mensaje = "Se encontraron errores al procesar la matricula."
                    return HttpResponse(mensaje)

                nueva_matricula = MatriculaCursos(MatriculaCiclo=matriculaciclo, PeriodoCurso=periodocurso)
                # nueva_matricula_campus = MatriculaCursos(MatriculaCiclo=matriculaciclo, PeriodoCurso=periodocurso)
                nueva_matricula.save()
                # nueva_matricula_campus.save(using="campus")

                log_cursos.append('%s,%s,%s,%s,%s,%s,%s\n' % (
                    periodocurso.Grupo, periodocurso.Curso.Codigo, periodocurso.Curso.Nombre, periodocurso.Curso.Ciclo,
                    periodocurso.Curso.Creditos, fecha_date, fecha_hora))

                creditos_matriculados += int(periodocurso.Curso.Creditos)

        creditos_carrera = CREDITOS_CARRERA
        creditos_regulares = int(creditos_carrera['%s' % estudiante.Carrera.Codigo])
        creditos_adicionales = creditos_matriculados - creditos_regulares
        pension_regular = matriculaciclo.Categoria.Pago
        pension_adicional = MONTO_CREDITO * creditos_adicionales
        pension_total = pension_regular + pension_adicional

        if matriculaciclo.Categoria.Categoria.find('CATEGORIA A') != -1:
            actual_cat = 'CATEGORIA A'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA B') != -1:
            actual_cat = 'CATEGORIA B'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA C') != -1:
            actual_cat = 'CATEGORIA C'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA D') != -1:
            actual_cat = 'CATEGORIA D'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA E') != -1:
            actual_cat = 'CATEGORIA E'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA F') != -1:
            actual_cat = 'CATEGORIA F'
        else:
            actual_cat = 'CATEGORIA A'

        try:
            categoria = Categoria.objects.get(Periodo=periodo, Categoria__icontains=actual_cat, Pago=pension_total)
            matriculaciclo.Categoria = categoria
        except Categoria.DoesNotExist:
            categoria = ''
            pass
        matriculaciclo.FechaMatricula = datetime.date.today()
        matriculaciclo.save()

        from django.utils.encoding import smart_str
        listado = []
        rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
               'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
               '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' '}
        if categoria != '':
            log_matricula += '%s,%s,%s,%s,%s,' % (
                creditos_regulares, creditos_adicionales, pension_adicional, categoria.Categoria, categoria.Pago)
        else:
            log_matricula += '%s,%s,%s,%s,%s,' % (creditos_regulares, creditos_adicionales, pension_adicional, '', '')
        f = open(settings.MEDIA_ROOT + 'matricula_coordinadores_log.csv', 'a+')
        for log in log_cursos:
            linea = replace_all(smart_str(log_matricula + log), rep)
            f.write(unicode(linea))
        f.close()

        mensaje = "Cursos registrados correctamente"
        return HttpResponse(mensaje)
    else:
        return HttpResponseRedirect('../')


def asistente_descargar_ficha(request, idmatriculadociclo):
    if request.user.is_authenticated():
        try:
            matriculaciclo = MatriculaCiclo.objects.get(id=idmatriculadociclo)

        except Periodo.DoesNotExist:
            mensaje = "Error, No ha pagado su matricula, verifique por favor."
            return HttpResponse(mensaje)

        periodo = matriculaciclo.Periodo
        estudiante = matriculaciclo.Alumno

        try:
            matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "Error, No ha pagado su matricula, verifique por favor."
            return HttpResponse(mensaje)

        # lineas para generar la ficha
        from Notas.formatos import ficha_matricula, ficha_matricula2
        from reportlab.pdfgen import canvas
        from reportlab.lib.units import cm

        response = HttpResponse(mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=ficha_matricula.pdf'
        p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))

        try:
            # dibujar ficha inferior
            p = ficha_matricula(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = matriculaciclo.Alumno.Codigo
            alumno = str(matriculaciclo.Alumno)
            carrera = str(matriculaciclo.Alumno.Carrera)
            periodo = str(matriculaciclo.Periodo)
            p.drawString(500, 335, codigo)
            p.drawString(500, 315, periodo)
            p.drawString(100, 315, carrera)
            p.drawString(100, 335, alumno)
            i = 265
            creditos = 0
            for cur in matriculaciclo.matriculacursos_set.all():
                if cur.Convalidado is False and cur.Estado is True:
                    # codigo del curso
                    p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(181, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos y total de creditos
                    p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    i = i - 20
            p.drawString(529, 95, str(creditos))

            # dibujar ficha superior
            w = 420
            p = ficha_matricula2(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = matriculaciclo.Alumno.Codigo
            alumno = str(matriculaciclo.Alumno)
            carrera = str(matriculaciclo.Alumno.Carrera)
            periodo = str(matriculaciclo.Periodo)
            p.drawString(500, 335 + w, codigo)
            p.drawString(500, 315 + w, periodo)
            p.drawString(100, 315 + w, carrera)
            p.drawString(100, 335 + w, alumno)
            i = 265 + w
            creditos = 0
            for cur in matriculaciclo.matriculacursos_set.all():
                if cur.Convalidado is False and cur.Estado is True:
                    # codigo del curso
                    p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(181, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos y total de creditos
                    p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    # i = i - 18
                    i = i - 20
            p.drawString(529, 95 + w, str(creditos))
            p.showPage()
            p.save()
        except:
            p.showPage()
            p.save()
        # fin generar ficha-------------

        return response
    else:
        return HttpResponseRedirect('../')
