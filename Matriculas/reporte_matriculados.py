# -*- coding: utf-8 -*-
# imports para traer variable de entorno django
import os
import sys

import xlwt

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

from django.utils.encoding import smart_str
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from Pagos.models import PagoAlumno
from UDL.settings import MEDIA_ROOT

periodo_id = 12
anio_ingreso = "2014"
semestre = "I"


def generar_lista(periodo_id, anio_ingreso, excluir=False):
    seleccionar_matriculados = MatriculaCiclo.objects.all().exclude(Estado='Retirado').order_by('Periodo')
    # if excluir:
    #     seleccionar_matriculados = MatriculaCiclo.objects.filter(Estado="Matriculado",Periodo__id=periodo_id).exclude(
    #         Alumno__AnioIngreso=anio_ingreso, Alumno__Semestre=semestre)
    # else:
    #     seleccionar_matriculados = MatriculaCiclo.objects.filter(Estado="Matriculado", Periodo__id=periodo_id,
    #                                                              Alumno__AnioIngreso=anio_ingreso)

    lista = []
    linea = []

    for matriculado in seleccionar_matriculados:
        codigo = matriculado.Alumno.Codigo
        nombres_alumno = smart_str(matriculado.Alumno.ApellidoPaterno) + " " + smart_str(
            matriculado.Alumno.ApellidoMaterno) + " " + smart_str(matriculado.Alumno.Nombres)
        # apellidos_alumno = smart_str(matriculado.Alumno.ApellidoPaterno) + " " + smart_str(
        #     matriculado.Alumno.ApellidoMaterno)
        carrera_alumno = smart_str(matriculado.Alumno.Carrera)
        departamento_alumno = smart_str(matriculado.Alumno.Provincia.Departamento.Departamento)
        ingreso = str(matriculado.Alumno.AnioIngreso) + "-" + str(matriculado.Alumno.Semestre)
        periodo = (matriculado.Periodo.__unicode__())
        genero = str(matriculado.Alumno.Sexo)
        categoria = smart_str(matriculado.Categoria.Categoria)
        pago_alumno = PagoAlumno.objects.filter(MatriculaCiclo__id=matriculado.id,
                                                ConceptoPago__Descripcion__icontains="4ta")
        tipo_colegio = matriculado.Alumno.Colegio.Tipo
        nombre_colegio = matriculado.Alumno.Colegio.Colegio
        if pago_alumno.count() > 0:
            monto_pension = pago_alumno[0].Monto()
        else:
            monto_pension = ""

        cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=matriculado.id)
        sum_cred = 0
        for cur in cursos_matriculado:
            if cur.Convalidado == False and cur.Estado == True:
                if cur.PeriodoCurso.Curso.Equivalencia:
                    k_codigo = cur.PeriodoCurso.Curso.Codigo1
                    k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                    k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)
                else:
                    k_codigo = cur.PeriodoCurso.Curso.Codigo
                    k_nombre = cur.PeriodoCurso.Curso.Nombre
                    k_creditos = str(cur.PeriodoCurso.Curso.Creditos)
                sum_cred = sum_cred + int(k_creditos)

        # Verificar si ha pagado matrícula
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="MATRICULA", Estado=True)
        if pagos.count() > 0:
            pago_matricula = "SI"
        else:
            pago_matricula = "NO"

        # Verificar pago pensiones
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="1era", Estado=True)
        if pagos.count() > 0:
            pago_pension_1 = "SI"
        else:
            pago_pension_1 = "NO"
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="2da", Estado=True)
        if pagos.count() > 0:
            pago_pension_2 = "SI"
        else:
            pago_pension_2 = "NO"
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="3era", Estado=True)
        if pagos.count() > 0:
            pago_pension_3 = "SI"
        else:
            pago_pension_3 = "NO"
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="4ta", Estado=True)
        if pagos.count() > 0:
            pago_pension_4 = "SI"
        else:
            pago_pension_4 = "NO"
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="5ta", Estado=True)
        if pagos.count() > 0:
            pago_pension_5 = "SI"
        else:
            pago_pension_5 = "NO"

        # CazaFantasmas
        if codigo.find("F") > 0:
            fantasma = "SI"
        else:
            fantasma = "NO"

        # Verificar si tiene cursos registrados
        cursos = matriculado.matriculacursos_set.all()
        if cursos.count() == 0:
            registra = " "
        else:
            registra = cursos.count()
        #

        linea.extend([codigo, smart_str(nombres_alumno), carrera_alumno, periodo, categoria, sum_cred, ingreso,
                      departamento_alumno, genero, pago_matricula, registra, monto_pension, pago_pension_1,
                      pago_pension_2, pago_pension_3, pago_pension_4, pago_pension_5, fantasma, tipo_colegio,
                      nombre_colegio])
        if linea not in lista:
            lista.append(linea)

        linea = []

    lista.sort(key=lambda x: (x[8], x[9]))

    # Establezco la cabecera
    # NO Pagaron Matrícula y tienen cursos registrados
    # SI Pagaron Matrícula y tienen cursos registrados
    # SI Pagaron Matrícula y NO tienen cursos registrados
    # No Pagaron Matrícula y NO tienen cursos registrados
    rp_1 = 0
    rp_2 = 0
    rp_3 = 0
    rp_4 = 0

    for linea in lista:
        if (linea[8] == "NO") and (type(linea[9]) is int):
            rp_1 += 1
        if (linea[8] == "SI") and (type(linea[9]) is int):
            rp_2 += 1
        if (linea[8] == "SI") and (type(linea[9]) is str):
            rp_3 += 1
        if (linea[8] == "NO") and (type(linea[9]) is str):
            rp_4 += 1

    extra = (rp_1, rp_2, rp_3, rp_4)
    return [lista, extra]


def generar_excel(datos_lista, excluir=False):
    datos = datos_lista[0]
    extra = datos_lista[1]

    ruta_archivo = str(MEDIA_ROOT)

    if excluir:
        nombre_archivo = "reporte_matriculados_antiguos_2014.xls"
    else:
        nombre_archivo = "reporte_matriculados_ingresantes_2014.xls"

    style = xlwt.XFStyle()
    styles = {'default': xlwt.easyxf('font: height 160;', num_format_str='@'),
              'headers': xlwt.easyxf('font: height 160, bold 1;', num_format_str='@'),
              'date': xlwt.easyxf('font: height 160;', num_format_str='dd-mm-yyyy'), }
    book = xlwt.Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 1')

    # Primera parte
    j = 1
    encabezado = ["NO Pagaron Matrícula y tienen cursos registrados",
                  "SI Pagaron Matrícula y tienen cursos registrados",
                  "SI Pagaron Matrícula y NO tienen cursos registrados",
                  "No Pagaron Matrícula y NO tienen cursos registrados"]

    for rowx, row in enumerate(encabezado):
        cell_style = styles['headers']
        sheet.write(rowx + j, 0, row, style=cell_style)
        sheet.write(rowx + j, 3, extra[rowx], style=cell_style)

    i = 7
    flag1 = 0

    for rowx, row in enumerate(datos):
        if flag1 == 0:
            flag1 = 1
            cell_style = styles['headers']
            cell_stylef = styles['date']
            encabezado = ["CODIGO", "NOMBRES", "CARRERA", "MATRÍCULA", "CATEGORIA", "CREDITOS", "INGRESO",
                          "DEPARTAMENTO", "GENERO", "PAGO MATRICULA", "CURSOS REGISTRADOS", "MONTO PENSIÓN",
                          "PAGO PENSIÓN 1", "PAGO PENSIÓN 2", "PAGO PENSIÓN 3", "PAGO PENSIÓN 4", "PAGO PENSIÓN 5",
                          "FANTASMA", "TIPO COLEGIO", "NOMBRE COLEGIO"]
            for c, celda in enumerate(encabezado):
                sheet.write(rowx + i, c, celda, style=cell_style)
            i += 1

        cell_style = styles['default']
        for colx, value in enumerate(row):
            sheet.write(rowx + i, colx, value, style=cell_style)

    full_path = ruta_archivo + nombre_archivo
    book.save(full_path)


lista = generar_lista(periodo_id, anio_ingreso)
generar_excel(lista)
lista = generar_lista(periodo_id, anio_ingreso, excluir=True)
generar_excel(lista, excluir=True)
