# -*- coding: utf-8 -*-
# imports para traer variable de entorno django
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Matriculas.models import MatriculaCiclo

periodo_id = 22
alumnos = open('reporte_matriculados.csv', 'r')

i = 1
for cod in alumnos:
    alumno = cod.split('\n')[0]
    if len(alumno) == 7:
        try:
            mat = MatriculaCiclo.objects.get(Estado="Matriculado", Periodo__id=periodo_id, Alumno__Codigo=alumno).id
            MatriculaCiclo.objects.get(Estado="Matriculado", Periodo__id=periodo_id, Alumno__Codigo=alumno).delete()
        except MatriculaCiclo.DoesNotExist:
            print "No existe"
        print i, alumno, " - ELIMINADO"
        i += 1
