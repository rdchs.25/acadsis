# -*- coding: utf-8 -*-

from decimal import Decimal

from django import forms
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.encoding import smart_str
from django.views.decorators.csrf import csrf_protect
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
# imports reportlab
from reportlab.pdfgen import canvas

from Asistencias.models import Asistencia
from Carreras.models import Carrera
from Cursos.models import Curso
from Cursos.models import PeriodoCurso
from Horarios.models import Horario
from Matriculas.models import MatriculaCiclo, MatriculaCursos
# importar formatos reportlab creados
from Notas.formatos import boleta_notas, boleta_notas1, ficha_matricula, ficha_matricula2, certificado_estudios, \
    constancia_matricula, certificado_estudios_egresado, historial_academico, certificado_estudios_simple
from Notas.models import Nota
from Periodos.models import Periodo
from funciones import response_excel

CICLO_CHOICES = (
    ('', '---------'),
    ('01', '1º'),
    ('02', '2º'),
    ('03', '3º'),
    ('04', '4º'),
    ('05', '5º'),
    ('06', '6º'),
    ('07', '7º'),
    ('08', '8º'),
    ('09', '9º'),
    ('10', '10º'),
)

CONDICION_CHOICES = (
    ('', '---------'),
    ('Matriculado', 'Matriculado'),
    ('Reserva de Matrícula', 'Reserva de Matrícula'),
    ('Retirado', 'Retirado'),
)


class ReporteMatriculaCursosForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=False,
                                     label='Periodo')
    Carrera = forms.ModelChoiceField(queryset=Carrera.objects.all(), widget=forms.Select(), required=False,
                                     label='Carrera')
    Ciclo = forms.ChoiceField(choices=CICLO_CHOICES, required=False, widget=forms.Select(), label='Ciclo')
    Condicion = forms.ChoiceField(choices=CONDICION_CHOICES, required=False, widget=forms.Select(), label='Condición')
    Curso = forms.ModelChoiceField(queryset=Curso.objects.all(), widget=forms.Select(), required=False, label='Curso')

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Cursos/frm_obtener_cursos.js"
              )


@csrf_protect
def frm_reporte_matricula_cursos(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = ReporteMatriculaCursosForm(request.POST)
            if form.is_valid():
                cursos_matriculados = []
                periodo = form.cleaned_data['Periodo']
                carrera = form.cleaned_data['Carrera']
                ciclo = form.cleaned_data['Ciclo']
                curso = form.cleaned_data['Curso']
                condicion = form.cleaned_data['Condicion']

                # periodos = Periodo.objects.all();
                periodos = Periodo.objects.filter(id=periodo.id)
                for valueperiodo in periodos:

                    carreras = None
                    if carrera:
                        carreras = Carrera.objects.filter(id=carrera.id)
                    else:
                        carreras = Carrera.objects.all()

                    for valuecarrera in carreras:
                        cursos = None
                        if curso:
                            cursos = Curso.objects.filter(Extracurricular=False, Carrera=valuecarrera,
                                                          Periodo=valueperiodo, id=curso.id)
                        else:
                            if ciclo:
                                cursos = Curso.objects.filter(Extracurricular=False, Carrera=valuecarrera,
                                                              Periodo=valueperiodo, Ciclo=ciclo).order_by('Ciclo')
                            else:
                                cursos = Curso.objects.filter(Extracurricular=False, Carrera=valuecarrera,
                                                              Periodo=valueperiodo).order_by('Ciclo')
                        for valuecurso in cursos:
                            if condicion:
                                matriculados = MatriculaCursos.objects.filter(MatriculaCiclo__Estado=condicion,
                                                                              MatriculaCiclo__Periodo=valueperiodo,
                                                                              PeriodoCurso__Curso=valuecurso).exclude(
                                    Convalidado=True, FechaRetiro__isnull=False).order_by(
                                    'MatriculaCiclo__Alumno__ApellidoPaterno')
                            else:
                                matriculados = MatriculaCursos.objects.filter(MatriculaCiclo__Periodo=valueperiodo,
                                                                              PeriodoCurso__Curso=valuecurso).exclude(
                                    Convalidado=True, FechaRetiro__isnull=False).order_by(
                                    'MatriculaCiclo__Alumno__ApellidoPaterno')

                            # alumnos aprobados
                            # aprobados = 0
                            for valuematriculados in matriculados:
                                # notas = Nota.objects.filter(PeriodoCurso=valuematriculados.PeriodoCurso,
                                #                             Identificador='PF')
                                # promedio = Decimal("0.00")
                                # for valuenotas in notas:
                                #     try:
                                #         notaalumno = NotasAlumno.objects.get(MatriculaCursos=valuematriculados,
                                #                                              Nota=valuenotas)
                                #         promedio = notaalumno.Valor
                                #         if notaalumno.Valor >= Decimal("11.00"):
                                #             aprobados = aprobados + 1
                                #     except NotasAlumno.DoesNotExist:
                                #         pass
                                lista = []
                                lista.append(valueperiodo.Anio + '-' + valueperiodo.Semestre)
                                lista.append(valuematriculados.MatriculaCiclo.Alumno.Carrera.Carrera)
                                lista.append(valuecarrera.Carrera)
                                if valuecurso.Equivalencia:
                                    lista.append(valuecurso.Codigo1)
                                    lista.append(valuecurso.Ciclo)
                                    lista.append(valuecurso.Equivalencia)
                                else:
                                    lista.append(valuecurso.Codigo)
                                    lista.append(valuecurso.Ciclo)
                                    lista.append(valuecurso.Nombre)

                                lista.append(
                                    valuematriculados.PeriodoCurso.Docente.ApellidoPaterno + ' '
                                    + valuematriculados.PeriodoCurso.Docente.ApellidoMaterno + ' ' +
                                    valuematriculados.PeriodoCurso.Docente.Nombres)

                                horarios = Horario.objects.filter(PeriodoCurso=valuematriculados.PeriodoCurso)
                                cadenahorario = ''
                                for valuehorario in horarios:
                                    cadenaaux = '%s: %s (%s-%s)' % (
                                        valuehorario.Seccion, valuehorario.Dia, valuehorario.HoraInicio,
                                        valuehorario.HoraFin)
                                    cadenahorario = cadenahorario + cadenaaux
                                lista.append(cadenahorario)
                                lista.append(valuematriculados.MatriculaCiclo.Alumno.Codigo)
                                lista.append(valuematriculados.MatriculaCiclo.Categoria.__unicode__())
                                lista.append(valuematriculados.MatriculaCiclo.Alumno.NroDocumento)
                                lista.append(valuematriculados.MatriculaCiclo.Alumno.ApellidoPaterno)
                                lista.append(valuematriculados.MatriculaCiclo.Alumno.ApellidoMaterno)
                                lista.append(valuematriculados.MatriculaCiclo.Alumno.Nombres)
                                semestreingreso = valuematriculados.MatriculaCiclo.Alumno.AnioIngreso + valuematriculados.MatriculaCiclo.Alumno.Semestre
                                lista.append(semestreingreso)
                                # Verificar si ha pagado matrícula
                                pago_matricula = ""
                                pago_pension_1 = ""
                                monto_pension_1 = ""
                                pagos = valuematriculados.MatriculaCiclo.pagoalumno_set.filter(
                                    ConceptoPago__Descripcion__icontains="MATRICULA", Estado=True)
                                if pagos.count() > 0:
                                    pago_matricula = "SI"
                                else:
                                    pago_matricula = "NO"

                                # Verificar pago pensiones
                                pagos = valuematriculados.MatriculaCiclo.pagoalumno_set.filter(
                                    ConceptoPago__Descripcion__icontains="1ra",
                                    Estado=True)
                                if pagos.count() > 0:
                                    pago_pension_1 = "SI"
                                    for pago in pagos:
                                        monto_pension_1 = smart_str(pago.Monto())
                                else:
                                    pago_pension_1 = "NO"
                                    monto_pension_1 = "--"
                                lista.append(pago_matricula)
                                lista.append(pago_pension_1)
                                lista.append(monto_pension_1)
                                cursos_matriculados.append(lista)
                headers = ['Periodo', 'Carrera Alumno', 'Carrera Curso', 'Código Curso', 'Ciclo', 'Curso', 'Docente',
                           'Horario', 'Código Alumno', 'Categoría', 'DNI', 'Apellido Paterno', 'Apellido Materno',
                           'Nombres', 'Ingreso', "Pago Matrícula", "Pago 1era pensión", "Monto pensión"]
                titulo = 'MATRICULADOS SEMESTRE'
                return response_excel(titulo=titulo, heads=headers, registros=cursos_matriculados,
                                      nombre_archivo='matriculados_semestre')
        else:
            form = ReporteMatriculaCursosForm(initial={'Curso': ''})
        return render_to_response("Matriculas/Reportes/frm_reporte_matricula_cursos.html",
                                  {"form": form, "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


def print_ficha_matricula(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=fichas_matricula.pdf'
    # p = canvas.Canvas(response, pagesize = (21*cm, 29.7*cm*.5))
    p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))
    for mat in matriculados:
        # dibujar ficha inferior
        p = ficha_matricula(p)
        p.setFont("Helvetica-Bold", 10)
        codigo = mat[0].MatriculaCiclo.Alumno.Codigo
        alumno = str(mat[0].MatriculaCiclo.Alumno)
        carrera = str(mat[0].MatriculaCiclo.Alumno.Carrera)
        periodo = str(mat[0].MatriculaCiclo.Periodo)
        p.drawString(500, 335, codigo)
        p.drawString(500, 315, periodo)
        p.drawString(100, 315, carrera)
        p.drawString(100, 335, alumno)
        i = 265
        creditos = 0
        for cur in mat:
            if cur.FechaRetiro is not None:
                continue
            if cur.Convalidado is False and cur.Estado is True:
                # codigo del curso
                p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                # grupo horario del curso
                p.drawString(181, i, cur.PeriodoCurso.Grupo)
                # nombre del curso, creditos y total de creditos
                p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                if cur.PeriodoCurso.Curso.Extracurricular is True:
                    p.drawString(529, i, str('---'))
                else:
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                # i = i - 18
                i = i - 20
        p.drawString(529, 95, str(creditos))

        # dibujar ficha superior
        w = 420
        p = ficha_matricula2(p)
        p.setFont("Helvetica-Bold", 10)
        codigo = mat[0].MatriculaCiclo.Alumno.Codigo
        alumno = str(mat[0].MatriculaCiclo.Alumno)
        carrera = str(mat[0].MatriculaCiclo.Alumno.Carrera)
        periodo = str(mat[0].MatriculaCiclo.Periodo)
        p.drawString(500, 335 + w, codigo)
        p.drawString(500, 315 + w, periodo)
        p.drawString(100, 315 + w, carrera)
        p.drawString(100, 335 + w, alumno)
        i = 265 + w
        creditos = 0
        for cur in mat:
            if cur.FechaRetiro is not None:
                continue
            if cur.Convalidado is False and cur.Estado is True:
                # codigo del curso
                p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                # grupo horario del curso
                p.drawString(181, i, cur.PeriodoCurso.Grupo)
                # nombre del curso, creditos y total de creditos
                p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                if cur.PeriodoCurso.Curso.Extracurricular is True:
                    p.drawString(529, i, str('---'))
                else:
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                # i = i - 18
                i = i - 20
        p.drawString(529, 95 + w, str(creditos))

        p.showPage()
        p.save()
    return response


def print_ficha_notas(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=ficha_notas.pdf'
    # p = canvas.Canvas(response, pagesize = (21*cm, 29.7*cm*.5))
    p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))
    for mat in matriculados:
        try:
            # dibujar ficha inferior
            p = boleta_notas(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = mat[0].MatriculaCiclo.Alumno.Codigo
            alumno = str(mat[0].MatriculaCiclo.Alumno)
            carrera = str(mat[0].MatriculaCiclo.Alumno.Carrera)
            periodo = str(mat[0].MatriculaCiclo.Periodo)
            p.drawString(500, 335, codigo)
            p.drawString(500, 315, periodo)
            p.drawString(100, 315, carrera)
            p.drawString(100, 335, alumno)
            i = 265
            j = 1
            creditos = 0
            suma_producto = 0
            creditos_aprobados = 0
            for cur in mat:
                if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
                    # numero
                    if j < 10:
                        p.drawString(50, i, '0' + str(j))
                    else:
                        p.drawString(50, i, str(j))
                    # codigo del curso
                    p.drawString(72, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(122, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos, promedio de curso
                    p.drawString(180, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(462, i, str(cur.PeriodoCurso.Curso.Creditos))
                    p.drawString(514, i, str(cur.Promedio()))
                    # total de creditos
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    # creditos aprobados
                    if cur.aprobado() is True:
                        creditos_aprobados += int(cur.PeriodoCurso.Curso.Creditos)
                    suma_producto += int(cur.PeriodoCurso.Curso.Creditos) * cur.Promedio()
                    i = i - 20
                    j += 1
            # ponderado, total de creditos y creditos aprobados
            ponderado = suma_producto / creditos
            creditos_desaprobados = int(creditos) - int(creditos_aprobados)
            p.drawString(170, 95, str(creditos_aprobados))
            p.drawString(170, 75, str(creditos_desaprobados))
            p.drawString(514, 95, "%.2f" % ponderado)

            # dibujar ficha superior
            w = 420
            p = boleta_notas1(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = mat[0].MatriculaCiclo.Alumno.Codigo
            alumno = str(mat[0].MatriculaCiclo.Alumno)
            carrera = str(mat[0].MatriculaCiclo.Alumno.Carrera)
            periodo = str(mat[0].MatriculaCiclo.Periodo)
            p.drawString(500, 335 + w, codigo)
            p.drawString(500, 315 + w, periodo)
            p.drawString(100, 315 + w, carrera)
            p.drawString(100, 335 + w, alumno)
            i = 265 + w
            j = 1
            creditos = 0
            suma_producto = 0
            creditos_aprobados = 0
            for cur in mat:
                if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
                    # numero
                    if j < 10:
                        p.drawString(50, i, '0' + str(j))
                    else:
                        p.drawString(50, i, str(j))
                    # codigo del curso
                    p.drawString(72, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(122, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos, promedio de curso
                    p.drawString(180, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(462, i, str(cur.PeriodoCurso.Curso.Creditos))
                    p.drawString(514, i, str(cur.Promedio()))
                    # total de creditos
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    # creditos aprobados
                    if cur.aprobado() is True:
                        creditos_aprobados += int(cur.PeriodoCurso.Curso.Creditos)
                    suma_producto += int(cur.PeriodoCurso.Curso.Creditos) * cur.Promedio()
                    i = i - 20
                    j += 1
            # ponderado, total de creditos y creditos aprobados
            ponderado = suma_producto / creditos
            creditos_desaprobados = int(creditos) - int(creditos_aprobados)
            p.drawString(170, 95 + w, str(creditos_aprobados))
            p.drawString(170, 75 + w, str(creditos_desaprobados))
            p.drawString(514, 95 + w, "%.2f" % ponderado)

            p.showPage()
            p.save()
        except:
            p.showPage()
            p.save()
    return response


def print_ficha_notas1(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=ficha_notas.pdf'
    p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm * .5))
    for mat in matriculados:
        try:
            p = boleta_notas(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = mat[0].MatriculaCiclo.Alumno.Codigo
            alumno = str(mat[0].MatriculaCiclo.Alumno)
            carrera = str(mat[0].MatriculaCiclo.Alumno.Carrera)
            periodo = str(mat[0].MatriculaCiclo.Periodo)
            p.drawString(500, 335, codigo)
            p.drawString(500, 315, periodo)
            p.drawString(100, 315, carrera)
            p.drawString(100, 335, alumno)
            i = 265
            j = 1
            creditos = 0
            suma_producto = 0
            creditos_aprobados = 0
            for cur in mat:
                if cur.Convalidado is False and cur.Estado is True:
                    # numero
                    if j < 10:
                        p.drawString(50, i, '0' + str(j))
                    else:
                        p.drawString(50, i, str(j))
                    # codigo del curso
                    p.drawString(72, i, cur.PeriodoCurso.Curso.Codigo1)
                    # grupo horario del curso
                    p.drawString(122, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos, promedio de curso
                    p.drawString(180, i, cur.PeriodoCurso.Curso.Equivalencia)
                    p.drawString(462, i, str(cur.PeriodoCurso.Curso.Creditos))
                    p.drawString(514, i, str(cur.Promedio()))
                    # total de creditos
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    # creditos aprobados
                    if cur.aprobado() is True:
                        creditos_aprobados += int(cur.PeriodoCurso.Curso.Creditos)
                    suma_producto += int(cur.PeriodoCurso.Curso.Creditos) * cur.Promedio()
                    i = i - 20
                    j += 1
            # ponderado, total de creditos y creditos aprobados
            ponderado = suma_producto / creditos
            creditos_desaprobados = int(creditos) - int(creditos_aprobados)
            p.drawString(170, 95, str(creditos_aprobados))
            p.drawString(170, 75, str(creditos_desaprobados))
            p.drawString(514, 95, "%.2f" % ponderado)
            p.showPage()
            p.save()
        except:
            p.showPage()
            p.save()
    return response


def print_certificado_estudios(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=certificado_estudios.pdf'
    p = canvas.Canvas(response, pagesize=A4)
    dict = {'0.00': 'cero', '1.00': 'uno', '2.00': 'dos', '3.00': 'tres', '4.00': 'cuatro', '5.00': 'cinco',
            '6.00': 'seis', '7.00': 'siete', '8.00': 'ocho', '9.00': 'nueve', '10.00': 'diez', '11.00': 'once',
            '12.00': 'doce', '13.00': 'trece', '14.00': 'catorce', '15.00': 'quince', '16.00': 'dieciseis',
            '17.00': 'diecisiete', '18.00': 'dieciocho', '19.00': 'diecinueve', '20.00': 'veinte'}

    for id_mat in matriculados:
        # n viene a ser el contador para el número de matriculados
        n = 1
        try:
            matriculado = MatriculaCiclo.objects.get(id=id_mat)
            codigo = matriculado.Alumno.Codigo
            alumno = matriculado.Alumno
            carrera = str(matriculado.Alumno.Carrera)
        except:
            p.showPage()
            p.save()

        cadena_facultad = ''
        if alumno.Carrera.Codigo == 'IS' or alumno.Carrera.Codigo == 'IC' or alumno.Carrera.Codigo == 'IA':
            cadena_facultad = ' FACULTAD DE CIENCIAS DE INGENIERÍA'
        elif alumno.Carrera.Codigo == 'AM' or alumno.Carrera.Codigo == 'AT':
            cadena_facultad = 'FACULTAD DE CIENCIAS SOCIALES, COMERCIALES Y DERECHO'
            # cadena_facultad = 'FACULTAD DE CIENCIAS DE ADMINISTRACIÓN'

        # Se deben renderizar dos semestres por hoja
        semestres = MatriculaCiclo.objects.filter(Alumno__id=alumno.id).order_by('Periodo__Anio', 'Periodo__Semestre')

        semestres_escritos = 0  # contador de semestres escritos en una hoja
        for semestre in semestres:
            # Crear hoja
            p = certificado_estudios(p)
            p.setFont("Helvetica", 10)
            p.drawString(500, 690, codigo)
            p.drawString(240, 670, carrera)
            p.drawString(70, 690, str(alumno))

            firma = 75
            p.setFont("Helvetica", 9)
            p.drawString(70, firma - 22, cadena_facultad)

            p.setFont("Helvetica", 10)
            i = 560

            periodo_id = semestre.id
            cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=semestre.id)
            if cursos_matriculado.count() != 0:
                print "escribir los cursos de aquel semestre"
                p.setFont("Helvetica-Bold", 9)
                p.drawString(115, i, 'Periodo %s:' % semestre.Periodo)
                p.line(115, i - 5, 185, i - 5)
                i = i - 20
                # Escribir los cursos del semestre
                p.setFont("Helvetica", 9)
                notas = []
                cred_matriculados = 0
                for cur in cursos_matriculado:
                    if cur.Convalidado is False and cur.Estado is True and cur.Promedio() >= Decimal("11.00"):
                        if n < 10:
                            p.drawString(47, i, str(n))
                        elif n < 99:
                            p.drawString(45, i, str(n))
                        else:
                            p.drawString(43, i, str(n))
                        if cur.PeriodoCurso.Curso.Equivalencia:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo1
                            k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)
                        else:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo
                            k_nombre = cur.PeriodoCurso.Curso.Nombre
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos)
                        p.drawString(67, i, k_codigo)
                        # p.drawString(115, i, k_nombre)
                        p.drawString(281, i, str(cur.PeriodoCurso.Periodo))
                        # p.drawString(329, i, k_creditos)
                        # p.drawString(354,i,str(cur.Promedio()).split('.')[0])

                        creditos = Decimal(k_creditos)
                        cred_matriculados += creditos
                        promedio = cur.Promedio()
                        p.drawString(329, i, str(creditos))
                        p.drawString(354, i, str(promedio).split('.')[0])

                        p.drawString(382, i, dict[str(cur.Promedio())])

                        palabras = k_nombre.split(" ")
                        longitud = 0
                        l_nombre = []
                        for caracter in palabras:
                            longitud = len(caracter) + longitud
                            if longitud >= 40:
                                l_nombre.append(k_nombre[:longitud - len(caracter)])
                                l_nombre.append(k_nombre[longitud - len(caracter):])
                            else:
                                longitud += 1
                        if len(l_nombre) == 0:
                            p.drawString(115, i, k_nombre)
                        else:
                            p.drawString(115, i, l_nombre[0])
                            i = i - 15
                            p.drawString(115, i, l_nombre[1])

                        i = i - 20
                        n += 1
                        notas.extend([[creditos, promedio]])
                    elif cur.Convalidado is True and cur.Estado is True and cur.Promedio() >= Decimal("11.00"):
                        if n < 10:
                            p.drawString(47, i, str(n))
                        elif n < 99:
                            p.drawString(45, i, str(n))
                        else:
                            p.drawString(43, i, str(n))
                        p.drawString(67, i, cur.PeriodoCurso.Curso.Codigo)
                        p.drawString(115, i, cur.PeriodoCurso.Curso.Nombre)
                        p.drawString(281, i, str(cur.PeriodoCurso.Periodo))
                        creditos = cur.PeriodoCurso.Curso.Creditos
                        promedio = cur.Promedio()
                        p.drawString(329, i, str(creditos))
                        p.drawString(354, i, str(promedio).split('.')[0])
                        p.drawString(382, i, dict[str(cur.Promedio())])
                        p.drawString(435, i, 'DEC NRO %s' % cur.Resolucion)
                        i = i - 20
                        n += 1
                        notas.extend([[creditos, promedio]])
                    elif cur.Estado is True and cur.Promedio() < Decimal("11.00"):
                        cred_matriculados += cur.PeriodoCurso.Curso.Creditos
                        notas.extend([[cur.PeriodoCurso.Curso.Creditos, cur.Promedio()]])

                sum_creditos = Decimal("0.00")
                sum_promedios = Decimal("0.00")
                cred_aprobados = Decimal("0.00")
                for var in notas:
                    credito = var[0]
                    nota = var[1]
                    if nota >= Decimal("11.00"):
                        cred_aprobados += credito
                    sum_creditos += credito
                    promedio_curso = credito * nota
                    sum_promedios += promedio_curso

                if sum_promedios > 0 and sum_creditos > 0:
                    valor = sum_promedios / sum_creditos
                    pps = Decimal("%.2f" % valor)
                else:
                    pps = ''
                cred_aprobados = str(cred_aprobados).split('.')[0]
                ## Verificar si hay créditos aprobados y desaprobados.
                p.drawString(115, i, "Cred. Matriculados:")
                p.drawString(197, i, str(cred_matriculados))
                p.drawString(320, i, 'P.P.S')
                p.drawString(350, i, str(pps))
                p.drawString(435, i, '')
                i = i - 20
                p.drawString(115, i, "Cred. Aprobados:")
                p.drawString(190, i, str(cred_aprobados))
                i = i - 20

            else:
                print "no escribir nada, pasar al siguiente semestre"
                continue
            p.showPage()
            p.save()
    return response


def print_certificado_estudios_egresado(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=certificado_estudios_egresado.pdf'
    p = canvas.Canvas(response, pagesize=A4)
    dict = {'0.00': 'Cero', '1.00': 'Uno', '2.00': 'Dos', '3.00': 'Tres', '4.00': 'Cuatro', '5.00': 'Cinco',
            '6.00': 'Seis', '7.00': 'Siete', '8.00': 'Ocho', '9.00': 'Nueve', '10.00': 'Diez', '11.00': 'Once',
            '12.00': 'Doce', '13.00': 'Trece', '14.00': 'Catorce', '15.00': 'Quince', '16.00': 'Dieciseis',
            '17.00': 'Diecisiete', '18.00': 'Dieciocho', '19.00': 'Diecinueve', '20.00': 'Veinte'}

    for id_mat in matriculados:
        # n viene a ser el contador para el número de matriculados
        n = 1
        try:
            matriculado = MatriculaCiclo.objects.get(id=id_mat)
            codigo = matriculado.Alumno.Codigo
            alumno = matriculado.Alumno
            carrera = str(matriculado.Alumno.Carrera)
        except:
            p.showPage()
            p.save()

        # Se deben renderizar dos semestres por hoja
        semestres = MatriculaCiclo.objects.filter(Alumno__id=alumno.id).order_by('Periodo__Anio', 'Periodo__Semestre')

        semestres_escritos = 1  # contador de semestres escritos en una hoja
        datos_totales = []
        total_notas = Decimal("0.000")
        total_creditos = Decimal("0.000")
        total_creditos_aprobados = 0
        for semestre in semestres:
            periodo_matricula = semestre.Periodo
            periodo_actual = Periodo.objects.filter(Activo=True)[0]
            cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=semestre.id)
            datos_semestre = []
            if cursos_matriculado.count() != 0:
                notas = []
                cred_matriculados = 0
                # Datos semestre
                datos_semestre.append(semestres_escritos)
                lista_cursos = []
                for cur in cursos_matriculado:
                    lista = []
                    if cur.Convalidado is False and cur.Estado is True and cur.Promedio() >= Decimal(
                            "11.00") and cur.Aprobado is True:
                        if cur.PeriodoCurso.Curso.Equivalencia:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo1
                            k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)

                            # LISTA
                            lista.append(k_codigo)
                            lista.append(k_nombre)
                        else:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo
                            k_nombre = cur.PeriodoCurso.Curso.Nombre
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos)

                            # LISTA
                            lista.append(k_codigo)
                            lista.append(k_nombre)

                        creditos = Decimal(k_creditos)
                        cred_matriculados += creditos
                        promedio = cur.Promedio()

                        n += 1
                        notas.extend([[creditos, promedio]])

                        # LISTA
                        lista.append(str(cur.PeriodoCurso.Periodo))
                        lista.append(str(creditos))
                        lista.append(str(promedio).split('.')[0])
                        lista.append(dict[str(cur.Promedio())])
                        lista.append('')
                        lista.append(cur.PeriodoCurso.Curso.Ciclo)
                        lista_cursos.append(lista)

                    elif cur.Convalidado is True and cur.Estado is True and cur.Promedio() >= Decimal(
                            "11.00") and cur.Aprobado is True:

                        creditos = cur.PeriodoCurso.Curso.Creditos
                        promedio = cur.Promedio()
                        n += 1
                        notas.extend([[creditos, promedio]])

                        lista.append(cur.PeriodoCurso.Curso.Codigo)
                        lista.append(cur.PeriodoCurso.Curso.Nombre)
                        lista.append(str(cur.PeriodoCurso.Periodo))
                        lista.append(str(creditos))
                        lista.append(str(promedio).split('.')[0])
                        lista.append(dict[str(cur.Promedio())])
                        lista.append('DEC %s' % cur.Resolucion)
                        lista.append(cur.PeriodoCurso.Curso.Ciclo)
                        lista_cursos.append(lista)

                    elif cur.Estado is True and cur.Promedio() < Decimal("11.00") and cur.Aprobado is False:
                        cred_matriculados += cur.PeriodoCurso.Curso.Creditos
                        notas.extend([[cur.PeriodoCurso.Curso.Creditos, cur.Promedio()]])
                    # Datos semestre

                datos_semestre.append(lista_cursos)

                sum_creditos = Decimal("0.000")
                sum_promedios = Decimal("0.000")
                cred_aprobados = Decimal("0.000")
                ppa = Decimal("0.000")
                for var in notas:
                    credito = var[0]
                    nota = var[1]
                    if nota >= Decimal("11.00"):
                        cred_aprobados += credito
                        total_creditos_aprobados += credito
                    sum_creditos += credito
                    promedio_curso = credito * nota
                    sum_promedios += promedio_curso
                    total_notas += promedio_curso
                    total_creditos += credito

                if sum_promedios > 0 and sum_creditos > 0:
                    valor = sum_promedios / sum_creditos
                    pps = Decimal("%.3f" % valor)
                    # promedio ponderado acumulado
                    valor2 = total_notas / total_creditos
                    ppa = Decimal("%.3f" % valor2)
                else:
                    pps = ''

                # Datos semestre
                datos_semestre.append(pps)
                datos_semestre.append(ppa)
                datos_semestre.append(total_creditos_aprobados)
                semestres_escritos = semestres_escritos + 1
                datos_totales.append(datos_semestre)

                # ORDENAMOS LOS CURSOS POR CICLO
                nro_semestres = len(datos_totales)
                for y in xrange(0, nro_semestres):
                    lista_cursos = datos_totales[y][1]
                    nro_cursos = len(lista_cursos)
            else:
                print "no escribir nada, pasar al siguiente semestre"
                continue

        # ordenamos la lista por orden de ciclo
        primer_ciclo = []
        notas_primer_ciclo = []
        segundo_ciclo = []
        notas_segundo_ciclo = []
        tercer_ciclo = []
        notas_tercer_ciclo = []
        cuarto_ciclo = []
        notas_cuarto_ciclo = []
        quinto_ciclo = []
        notas_quinto_ciclo = []
        sexto_ciclo = []
        notas_sexto_ciclo = []
        septimo_ciclo = []
        notas_septimo_ciclo = []
        octavo_ciclo = []
        notas_octavo_ciclo = []
        noveno_ciclo = []
        notas_noveno_ciclo = []
        decimo_ciclo = []
        notas_decimo_ciclo = []

        nro_semestres = len(datos_totales)

        for y in xrange(0, nro_semestres):
            lista_cursos = datos_totales[y][1]
            nro_cursos = len(lista_cursos)
            for x in xrange(0, nro_cursos):
                ciclo_curso = lista_cursos[x][7]
                creditos = Decimal(lista_cursos[x][3])
                promedio = Decimal(lista_cursos[x][4])
                if ciclo_curso == '01':
                    primer_ciclo.append(lista_cursos[x])
                    notas_primer_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '02':
                    segundo_ciclo.append(lista_cursos[x])
                    notas_segundo_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '03':
                    tercer_ciclo.append(lista_cursos[x])
                    notas_tercer_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '04':
                    cuarto_ciclo.append(lista_cursos[x])
                    notas_cuarto_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '05':
                    quinto_ciclo.append(lista_cursos[x])
                    notas_quinto_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '06':
                    sexto_ciclo.append(lista_cursos[x])
                    notas_sexto_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '07':
                    septimo_ciclo.append(lista_cursos[x])
                    notas_septimo_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '08':
                    octavo_ciclo.append(lista_cursos[x])
                    notas_octavo_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '09':
                    noveno_ciclo.append(lista_cursos[x])
                    notas_noveno_ciclo.extend([[creditos, promedio]])
                elif ciclo_curso == '10':
                    decimo_ciclo.append(lista_cursos[x])
                    notas_decimo_ciclo.extend([[creditos, promedio]])

        # datos totales
        datos_totales2 = []
        total_notas = Decimal("0.000")
        total_creditos = Decimal("0.000")
        total_creditos_aprobados = 0

        # primer ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_primer_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(1)
        datos_semestre.append(primer_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # segundo ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_segundo_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(2)
        datos_semestre.append(segundo_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # tercer ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_tercer_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(3)
        datos_semestre.append(tercer_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # cuarto ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_cuarto_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(4)
        datos_semestre.append(cuarto_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # quinto ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_quinto_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(5)
        datos_semestre.append(quinto_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # sexto ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_sexto_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(6)
        datos_semestre.append(sexto_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # septimo ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_septimo_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(7)
        datos_semestre.append(septimo_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # octavo ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_octavo_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(8)
        datos_semestre.append(octavo_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # noveno ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_noveno_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(9)
        datos_semestre.append(noveno_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        # decimo ciclo
        sum_creditos = Decimal("0.000")
        sum_promedios = Decimal("0.000")
        cred_aprobados = Decimal("0.000")
        ppa = Decimal("0.000")
        pps = Decimal("0.000")
        for var in notas_decimo_ciclo:
            credito = var[0]
            nota = var[1]
            print str(credito) + "---" + str(nota)
            if nota >= Decimal("11.00"):
                cred_aprobados += credito
                total_creditos_aprobados += credito
            sum_creditos += credito
            promedio_curso = credito * nota
            sum_promedios += promedio_curso
            total_notas += promedio_curso
            total_creditos += credito

        if sum_promedios > 0 and sum_creditos > 0:
            valor = sum_promedios / sum_creditos
            pps = Decimal("%.3f" % valor)
            # promedio ponderado acumulado
            valor2 = total_notas / total_creditos
            ppa = Decimal("%.3f" % valor2)
        else:
            pps = ''

        datos_semestre = []
        datos_semestre.append(10)
        datos_semestre.append(decimo_ciclo)
        datos_semestre.append(pps)
        datos_semestre.append(ppa)
        datos_semestre.append(total_creditos_aprobados)
        datos_totales2.append(datos_semestre)

        p = certificado_estudios_egresado(p, alumno, carrera, codigo, datos_totales2)
        p.showPage()
        p.save()
    return response


def print_certificado_estudios_egresado_2(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=certificado_estudios_egresado.pdf'
    p = canvas.Canvas(response, pagesize=A4)
    dict = {'0.00': 'Cero', '1.00': 'Uno', '2.00': 'Dos', '3.00': 'Tres', '4.00': 'Cuatro', '5.00': 'Cinco',
            '6.00': 'Seis', '7.00': 'Siete', '8.00': 'Ocho', '9.00': 'Nueve', '10.00': 'Diez', '11.00': 'Once',
            '12.00': 'Doce', '13.00': 'Trece', '14.00': 'Catorce', '15.00': 'Quince', '16.00': 'Dieciseis',
            '17.00': 'Diecisiete', '18.00': 'Dieciocho', '19.00': 'Diecinueve', '20.00': 'Veinte'}

    for id_mat in matriculados:
        # n viene a ser el contador para el número de matriculados
        n = 1
        alumno = None
        try:
            matriculado = MatriculaCiclo.objects.get(id=id_mat)
            codigo = matriculado.Alumno.Codigo
            alumno = matriculado.Alumno
            carrera = str(matriculado.Alumno.Carrera)
        except ValueError:
            p.showPage()
            p.save()

        # Se deben renderizar dos semestres por hoja
        semestres = MatriculaCiclo.objects.filter(Alumno__id=alumno.id).order_by('Periodo__Anio', 'Periodo__Semestre')

        semestres_escritos = 1  # contador de semestres escritos en una hoja
        datos_totales = []
        total_notas = Decimal("0.000")
        total_creditos = Decimal("0.000")
        total_creditos_aprobados = 0
        for semestre in semestres:
            periodo_matricula = semestre.Periodo
            periodo_actual = Periodo.objects.filter(Activo=True)[0]
            cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=semestre.id)
            datos_semestre = []
            if cursos_matriculado.count() != 0:
                notas = []
                cred_matriculados = 0
                # Datos semestre
                datos_semestre.append(semestres_escritos)
                lista_cursos = []
                for cur in cursos_matriculado:
                    lista = []
                    if cur.Convalidado is False and cur.Estado is True and cur.Promedio() >= Decimal(
                            "11.00") and cur.Aprobado is True:
                        if cur.PeriodoCurso.Curso.Equivalencia:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo1
                            k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)

                            # LISTA
                            lista.append(k_codigo)
                            lista.append(k_nombre)
                        else:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo
                            k_nombre = cur.PeriodoCurso.Curso.Nombre
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos)

                            # LISTA
                            lista.append(k_codigo)
                            lista.append(k_nombre)

                        creditos = Decimal(k_creditos)
                        cred_matriculados += creditos
                        promedio = cur.Promedio()

                        n += 1
                        notas.extend([[creditos, promedio]])

                        # LISTA
                        lista.append(str(cur.PeriodoCurso.Periodo))
                        lista.append(str(creditos))
                        lista.append(str(promedio).split('.')[0])
                        lista.append(dict[str(cur.Promedio())])
                        lista.append('')
                        lista_cursos.append(lista)

                    elif cur.Convalidado is True and cur.Estado is True and cur.Promedio() >= Decimal(
                            "11.00") and cur.Aprobado is True:

                        creditos = cur.PeriodoCurso.Curso.Creditos
                        promedio = cur.Promedio()
                        n += 1
                        notas.extend([[creditos, promedio]])

                        lista.append(cur.PeriodoCurso.Curso.Codigo)
                        lista.append(cur.PeriodoCurso.Curso.Nombre)
                        lista.append(str(cur.PeriodoCurso.Periodo))
                        lista.append(str(creditos))
                        lista.append(str(promedio).split('.')[0])
                        lista.append(dict[str(cur.Promedio())])
                        lista.append(' (DEC NRO %s' % cur.Resolucion + ')')
                        lista_cursos.append(lista)

                    elif cur.Estado is True and cur.Promedio() < Decimal("11.00") and cur.Aprobado is False:
                        cred_matriculados += cur.PeriodoCurso.Curso.Creditos
                        notas.extend([[cur.PeriodoCurso.Curso.Creditos, cur.Promedio()]])
                    # Datos semestre

                datos_semestre.append(lista_cursos)

                sum_creditos = Decimal("0.000")
                sum_promedios = Decimal("0.000")
                cred_aprobados = Decimal("0.000")
                ppa = Decimal("0.000")
                for var in notas:
                    credito = var[0]
                    nota = var[1]
                    if nota >= Decimal("11.00"):
                        cred_aprobados += credito
                        total_creditos_aprobados += credito
                    sum_creditos += credito
                    promedio_curso = credito * nota
                    sum_promedios += promedio_curso
                    total_notas += promedio_curso
                    total_creditos += credito

                if sum_promedios > 0 and sum_creditos > 0:
                    valor = sum_promedios / sum_creditos
                    pps = Decimal("%.3f" % valor)
                    # promedio ponderado acumulado
                    valor2 = total_notas / total_creditos
                    ppa = Decimal("%.3f" % valor2)
                else:
                    pps = ''

                # Datos semestre
                datos_semestre.append(pps)
                datos_semestre.append(ppa)
                datos_semestre.append(total_creditos_aprobados)
                semestres_escritos = semestres_escritos + 1
                # if periodo_matricula != periodo_actual:
                datos_totales.append(datos_semestre)
            else:
                print "no escribir nada, pasar al siguiente semestre"
                continue
        p = certificado_estudios_egresado(p, alumno, carrera, codigo, datos_totales)
        p.showPage()
        p.save()
    return response


def print_certificado_estudios_simple(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=certificado_estudios_simple.pdf'
    p = canvas.Canvas(response, pagesize=A4)
    dict = {'0.00': 'Cero', '1.00': 'Uno', '2.00': 'Dos', '3.00': 'Tres', '4.00': 'Cuatro', '5.00': 'Cinco',
            '6.00': 'Seis', '7.00': 'Siete', '8.00': 'Ocho', '9.00': 'Nueve', '10.00': 'Diez', '11.00': 'Once',
            '12.00': 'Doce', '13.00': 'Trece', '14.00': 'Catorce', '15.00': 'Quince', '16.00': 'Dieciseis',
            '17.00': 'Diecisiete', '18.00': 'Dieciocho', '19.00': 'Diecinueve', '20.00': 'Veinte'}

    for id_mat in matriculados:
        # n viene a ser el contador para el número de matriculados
        n = 1
        try:
            matriculado = MatriculaCiclo.objects.get(id=id_mat)
            codigo = matriculado.Alumno.Codigo
            alumno = matriculado.Alumno
            carrera = matriculado.Alumno.Carrera.Carrera
        except:
            p.showPage()
            p.save()

        # Se deben renderizar dos semestres por hoja
        semestres = MatriculaCiclo.objects.filter(Alumno__id=alumno.id).order_by('Periodo__Anio', 'Periodo__Semestre')

        semestres_escritos = 1  # contador de semestres escritos en una hoja
        datos_totales = []
        total_notas = Decimal("0.000")
        total_creditos = Decimal("0.000")
        total_creditos_aprobados = 0
        for semestre in semestres:
            periodo_matricula = semestre.Periodo
            periodo_actual = Periodo.objects.filter(Activo=True)[0]
            cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=semestre.id,
                                                                PeriodoCurso__Curso__Extracurricular=False)
            datos_semestre = []
            if cursos_matriculado.count() != 0:
                notas = []
                cred_matriculados = 0
                # Datos semestre
                datos_semestre.append(semestres_escritos)
                lista_cursos = []
                for cur in cursos_matriculado:
                    lista = []
                    if cur.Convalidado is False and cur.Estado is True and cur.Promedio() >= Decimal(
                            "11.00") and cur.Aprobado is True:
                        if cur.PeriodoCurso.Curso.Equivalencia:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo1
                            k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)

                            # LISTA
                            lista.append(k_codigo)
                            lista.append(k_nombre)
                        else:
                            k_codigo = cur.PeriodoCurso.Curso.Codigo
                            k_nombre = cur.PeriodoCurso.Curso.Nombre
                            k_creditos = str(cur.PeriodoCurso.Curso.Creditos)

                            # LISTA
                            lista.append(k_codigo)
                            lista.append(k_nombre)

                        creditos = Decimal(k_creditos)
                        cred_matriculados += creditos
                        promedio = cur.Promedio()

                        n += 1
                        notas.extend([[creditos, promedio]])

                        # LISTA
                        lista.append(str(cur.PeriodoCurso.Periodo))
                        lista.append(str(creditos))
                        lista.append(str(promedio).split('.')[0])
                        lista.append(dict[str(cur.Promedio())])
                        lista.append('')
                        lista_cursos.append(lista)

                    elif cur.Convalidado is True and cur.Estado is True and cur.Promedio() >= Decimal(
                            "11.00") and cur.Aprobado is True:

                        creditos = cur.PeriodoCurso.Curso.Creditos
                        promedio = cur.Promedio()
                        n += 1
                        notas.extend([[creditos, promedio]])

                        lista.append(cur.PeriodoCurso.Curso.Codigo)
                        lista.append(cur.PeriodoCurso.Curso.Nombre)
                        lista.append(str(cur.PeriodoCurso.Periodo))
                        lista.append(str(creditos))
                        lista.append(str(promedio).split('.')[0])
                        lista.append(dict[str(cur.Promedio())])
                        lista.append(' (DEC NRO %s' % cur.Resolucion + ')')
                        lista_cursos.append(lista)

                    elif cur.Estado is True and cur.Promedio() < Decimal("11.00") and cur.Aprobado is False:
                        cred_matriculados += cur.PeriodoCurso.Curso.Creditos
                        notas.extend([[cur.PeriodoCurso.Curso.Creditos, cur.Promedio()]])
                    # Datos semestre

                datos_semestre.append(lista_cursos)

                sum_creditos = Decimal("0.000")
                sum_promedios = Decimal("0.000")
                cred_aprobados = Decimal("0.000")
                for var in notas:
                    credito = var[0]
                    nota = var[1]
                    if nota >= Decimal("11.00"):
                        cred_aprobados += credito
                        total_creditos_aprobados += credito
                    sum_creditos += credito
                    promedio_curso = credito * nota
                    sum_promedios += promedio_curso
                    total_notas += promedio_curso
                    total_creditos += credito

                if sum_promedios > 0 and sum_creditos > 0:
                    valor = sum_promedios / sum_creditos
                    pps = Decimal("%.3f" % valor)
                    # promedio ponderado acumulado
                    valor2 = total_notas / total_creditos
                    ppa = Decimal("%.3f" % valor2)
                else:
                    pps = ''

                # Datos semestre
                datos_semestre.append(pps)
                datos_semestre.append(ppa)
                datos_semestre.append(total_creditos_aprobados)
                semestres_escritos = semestres_escritos + 1
                # if periodo_matricula != periodo_actual:
                datos_semestre.append(periodo_matricula)
                datos_totales.append(datos_semestre)
            else:
                print "no escribir nada, pasar al siguiente semestre"
                continue
        p = certificado_estudios_simple(p, alumno, carrera, codigo, datos_totales)
        p.showPage()
        p.save()
    return response


def print_historial_academico(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=historial_academico.pdf'
    p = canvas.Canvas(response, pagesize=A4)
    dict = {'0.00': 'cero', '1.00': 'uno', '2.00': 'dos', '3.00': 'tres', '4.00': 'cuatro', '5.00': 'cinco',
            '6.00': 'seis', '7.00': 'siete', '8.00': 'ocho', '9.00': 'nueve', '10.00': 'diez', '11.00': 'once',
            '12.00': 'doce', '13.00': 'trece', '14.00': 'catorce', '15.00': 'quince', '16.00': 'dieciseis',
            '17.00': 'diecisiete', '18.00': 'dieciocho', '19.00': 'diecinueve', '20.00': 'veinte'}

    for id_mat in matriculados:
        c = 3
        n = 1
        fin = False
        fin2 = False
        while c <= 12:
            try:
                p = historial_academico(p)
                p.setFont("Helvetica", 10)
                matriculado = MatriculaCiclo.objects.get(id=id_mat)
                codigo = matriculado.Alumno.Codigo
                alumno = str(matriculado.Alumno)
                carrera = str(matriculado.Alumno.Carrera)
                p.drawString(500, 690, codigo)
                p.drawString(240, 670, carrera)
                p.drawString(70, 690, alumno)
                # n = 1
                i = 560
                for ciclo in range(c - 2, c):
                    ciclo = str(ciclo).rjust(2).replace(" ", "0")
                    cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__Estado="Matriculado",
                                                                        MatriculaCiclo__Alumno=matriculado.Alumno,
                                                                        PeriodoCurso__Curso__Ciclo=ciclo)
                    if cursos_matriculado.count() != 0:
                        p.setFont("Helvetica-Bold", 9)
                        p.drawString(115, i, '%sº Ciclo :' % ciclo)
                        p.line(115, i - 5, 160, i - 5)
                        i = i - 20
                        p.setFont("Helvetica", 9)
                        for cur in cursos_matriculado:
                            if cur.Convalidado is False and cur.Estado == True:
                                if n < 10:
                                    p.drawString(47, i, str(n))
                                elif n < 99:
                                    p.drawString(45, i, str(n))
                                else:
                                    p.drawString(43, i, str(n))

                                if cur.PeriodoCurso.Curso.Equivalencia:
                                    k_codigo = cur.PeriodoCurso.Curso.Codigo1
                                    k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                                    k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)
                                else:
                                    k_codigo = cur.PeriodoCurso.Curso.Codigo
                                    k_nombre = cur.PeriodoCurso.Curso.Nombre
                                    k_creditos = str(cur.PeriodoCurso.Curso.Creditos)

                                creditos = Decimal(k_creditos)
                                # cred_matriculados += creditos
                                promedio = cur.Promedio()
                                p.drawString(67, i, k_codigo)
                                p.drawString(115, i, k_nombre)
                                p.drawString(281, i, str(cur.PeriodoCurso.Periodo))
                                p.drawString(329, i, str(creditos))
                                p.drawString(354, i, str(cur.Promedio()).split('.')[0])
                                p.drawString(382, i, dict[str(cur.Promedio())])
                                i = i - 20
                            elif cur.Convalidado is True and cur.Estado == True:
                                if n < 10:
                                    p.drawString(47, i, str(n))
                                elif n < 99:
                                    p.drawString(45, i, str(n))
                                else:
                                    p.drawString(43, i, str(n))
                                p.drawString(67, i, cur.PeriodoCurso.Curso.Codigo)
                                p.drawString(115, i, cur.PeriodoCurso.Curso.Nombre)
                                p.drawString(281, i, str(cur.PeriodoCurso.Periodo))
                                p.drawString(329, i, str(cur.PeriodoCurso.Curso.Creditos))
                                p.drawString(354, i, str(cur.Promedio()).split('.')[0])
                                p.drawString(382, i, dict[str(cur.Promedio())])
                                p.drawString(435, i, 'DEC NRO %s' % cur.Resolucion)
                                i = i - 20
                            n += 1
                        # p.drawString(115,i,'Cred. Matriculados')
                        # p.drawString(329,i,'18')
                        # p.drawString(354,i,'19')
                        # p.drawString(435,i,'')
                        # i = i - 20
                    else:
                        p.setFont("Helvetica-Bold", 9)
                        p.drawString(115, i, '%sº Ciclo :' % ciclo)
                        p.line(115, i - 5, 160, i - 5)
                        i = i - 20
                        fin = True
                        c = 13
                p.showPage()
                p.save()
                c += 2
            except ValueError:
                p.showPage()
                p.save()
    return response


def archivo_promedio_cursos(matriculados):
    cursos_matriculados = []
    for mat in matriculados:
        if mat.count() != 0:
            for cur in mat:
                lista = []
                lista.append(cur.MatriculaCiclo.Alumno.Codigo)
                lista.append(
                    cur.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + cur.MatriculaCiclo.Alumno.ApellidoMaterno)
                lista.append(cur.MatriculaCiclo.Alumno.Nombres)
                lista.append(cur.MatriculaCiclo.Alumno.Carrera.Carrera)
                lista.append(cur.PeriodoCurso.Curso.Codigo)
                lista.append(cur.PeriodoCurso.Curso.Nombre)
                lista.append(cur.PeriodoCurso.Curso.Creditos)
                lista.append(cur.Resolucion)
                lista.append(cur.PeriodoCurso.Grupo)
                lista.append(
                    cur.PeriodoCurso.Docente.ApellidoPaterno + ' ' + cur.PeriodoCurso.Docente.ApellidoMaterno + ' ' + cur.PeriodoCurso.Docente.Nombres)

                if cur.Estado:
                    lista.append("Matriculado")
                else:
                    lista.append("Retirado")

                if cur.Convalidado is True:
                    lista.append('Si')
                else:
                    lista.append('')
                lista.append(cur.Promedio())
                cursos_matriculados.append(lista)
    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Código Curso', 'Curso', 'Créditos', 'Resolucion', 'Grupo',
               'Docente', 'Condición', 'Convalidado', 'Promedio']
    titulo = 'REPORTE DE PROMEDIOS DE CURSOS'
    return response_excel(titulo=titulo, heads=headers, registros=cursos_matriculados, nombre_archivo='promedio_cursos')


def avance_plan_estudios(alumnos):
    import StringIO
    output = StringIO.StringIO()
    from Matriculas.matricula_online import CREDITOS_MAXIMOS
    from xlwt import Workbook, XFStyle, Borders, Pattern, Font
    book = Workbook()
    sheet1 = book.add_sheet('Notas')

    # estilos de celda titulo
    fnt_titulo = Font()
    fnt_titulo.name = 'Arial'
    fnt_titulo.bold = True

    style_titulo = XFStyle()
    style_titulo.font = fnt_titulo

    # estilos de celda etiqueta resumen
    fnt_etiqueta_resumen = Font()
    fnt_etiqueta_resumen.name = 'Arial'
    fnt_etiqueta_resumen.bold = True

    style_etiqueta_resumen = XFStyle()
    style_etiqueta_resumen.font = fnt_etiqueta_resumen

    # estilos de celda datos resumen
    fnt_dato_resumen = Font()
    fnt_dato_resumen.name = 'Arial'

    style_dato_resumen = XFStyle()
    style_dato_resumen.font = fnt_dato_resumen

    # estilos de celda heads
    fnt_heads = Font()
    fnt_heads.name = 'Arial'
    fnt_heads.bold = True
    borders_heads = Borders()
    borders_heads.left = Borders.THIN
    borders_heads.right = Borders.THIN
    borders_heads.top = Borders.THIN
    borders_heads.bottom = Borders.THIN
    pattern_heads = Pattern()
    pattern_heads.pattern = Pattern.SOLID_PATTERN
    pattern_heads.pattern_fore_colour = 0x9ACD32

    style_heads = XFStyle()
    style_heads.font = fnt_heads
    style_heads.borders = borders_heads
    style_heads.pattern = pattern_heads

    # estilos de celda registros

    # registros del estudiante
    fnt_registros = Font()
    fnt_registros.name = 'Arial'

    # cursos no llevados
    fnt_nollevados = Font()
    fnt_nollevados.name = 'Arial'
    fnt_nollevados.colour_index = 0xa

    pattern_nollevados = Pattern()
    pattern_nollevados.pattern = Pattern.SOLID_PATTERN
    pattern_nollevados.pattern_fore_colour = 0x1A

    pattern_nodisponibles = Pattern()
    pattern_nodisponibles.pattern = Pattern.SOLID_PATTERN
    pattern_nodisponibles.pattern_fore_colour = 0x16

    # estilos de celda registros
    fnt_registros = Font()
    fnt_registros.name = 'Arial'
    borders_registros = Borders()
    borders_registros.left = Borders.THIN
    borders_registros.right = Borders.THIN
    borders_registros.top = Borders.THIN
    borders_registros.bottom = Borders.THIN

    # style cursos no llevados
    style_nollevados = XFStyle()
    style_nollevados.font = fnt_registros
    style_nollevados.borders = borders_registros
    style_nollevados.pattern = pattern_nollevados

    # style cursos no llevados
    style_nodisponibles = XFStyle()
    style_nodisponibles.font = fnt_registros
    style_nodisponibles.borders = borders_registros
    style_nodisponibles.pattern = pattern_nodisponibles

    style_registros = XFStyle()
    style_registros.font = fnt_registros
    style_registros.borders = borders_registros

    cursos = []
    for alumno in alumnos:
        anio_ingreso = alumno.Alumno.AnioIngreso
        semestre_ingreso = alumno.Alumno.Semestre
        apellidos = alumno.Alumno.ApellidoPaterno + ' ' + alumno.Alumno.ApellidoMaterno
        nombres = alumno.Alumno.Nombres
        carrera = alumno.Alumno.Carrera.Carrera
        al = '%s %s' % (apellidos, nombres)
        ingreso = '%s-%s' % (anio_ingreso, semestre_ingreso)

        plan_estudios = Curso.objects.filter(Periodo=alumno.Periodo, Extracurricular=False, Proyecto_formativo=False,
                                             Carrera=alumno.Alumno.Carrera).order_by('Ciclo', 'Nombre')
        cursos_disponibles = []
        for cur in plan_estudios:
            n_equi = cur.Equivalencias.count()
            i = 1
            if n_equi != 0:
                for equi in cur.Equivalencias.all():
                    curso_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=equi,
                                                                    MatriculaCiclo__Alumno=alumno.Alumno, Aprobado=True)
                    if curso_aprobado.count() == 0:
                        if i == n_equi:
                            n_pre = cur.Prerequisitos.count()
                            j = 1
                            if n_pre != 0:
                                for pre in cur.Prerequisitos.all():
                                    curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                                        MatriculaCiclo__Alumno=alumno.Alumno,
                                                                                        Aprobado=True)
                                    if curso_pre_aprobado.count() != 0:
                                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                        if periodocurso.count() != 0:
                                            cursos_disponibles.append([periodocurso[0], False, True])
                                        break
                                    else:
                                        if j == n_pre:
                                            periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                            if periodocurso.count() != 0:
                                                cursos_disponibles.append([periodocurso[0], False, False])
                                        else:
                                            j += 1
                            else:
                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                if periodocurso.count() != 0:
                                    cursos_disponibles.append([periodocurso[0], False, True])
                        else:
                            i += 1
                    else:
                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                        if periodocurso.count() != 0:
                            cursos_disponibles.append([curso_aprobado[0], True])
                        break
            else:
                n_pre = cur.Prerequisitos.count()
                j = 1
                if n_pre != 0:
                    for pre in cur.Prerequisitos.all():
                        curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                            MatriculaCiclo__Alumno=alumno.Alumno,
                                                                            Aprobado=True)
                        if curso_pre_aprobado.count() != 0:
                            periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                            if periodocurso.count() != 0:
                                cursos_disponibles.append([periodocurso[0], False, True])
                            break
                        else:
                            if j == n_pre:
                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                if periodocurso.count() != 0:
                                    cursos_disponibles.append([periodocurso[0], False, False])
                            else:
                                j += 1
                else:
                    periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                    if periodocurso.count() != 0:
                        cursos_disponibles.append([periodocurso[0], False, True])

        for cur in cursos_disponibles:
            if cur[1] is True:
                ciclo_curso = cur[0].PeriodoCurso.Curso.Ciclo
                codigo_curso = cur[0].PeriodoCurso.Curso.Codigo
                nombre_curso = cur[0].PeriodoCurso.Curso.Nombre
                creditos_curso = cur[0].PeriodoCurso.Curso.Creditos

                if cur[0].PeriodoCurso.Curso.Prerequisito is not None:
                    prerequisito = cur[0].PeriodoCurso.Curso.Prerequisito.Nombre
                else:
                    prerequisito = 'Ninguno'
                nota_curso = cur[0].Promedio()
                ciclo_estudios = str(cur[0].PeriodoCurso.Periodo)
                cursos.append(
                    [ciclo_curso, codigo_curso, nombre_curso, creditos_curso, prerequisito, nota_curso, ciclo_estudios,
                     ''])
            elif cur[1] is False:
                ciclo_curso = cur[0].Curso.Ciclo
                codigo_curso = cur[0].Curso.Codigo
                nombre_curso = cur[0].Curso.Nombre
                creditos_curso = cur[0].Curso.Creditos
                if cur[0].Curso.Prerequisito is not None:
                    prerequisito = cur[0].Curso.Prerequisito.Nombre
                else:
                    prerequisito = 'Ninguno'
                nota_curso = ''
                ciclo_estudios = str(cur[0].Periodo)
                if cur[2] is True:
                    estado = "Disponible"
                else:
                    estado = "No Disponible"
                cursos.append(
                    [ciclo_curso, codigo_curso, nombre_curso, creditos_curso, prerequisito, nota_curso, ciclo_estudios,
                     estado])

    headers = ['N', 'Ciclo', 'Codigo', 'Nombre Curso', 'Creditos', 'Prerequisito', 'Nota', 'Ciclo Estudios', 'Estado']
    label_resumen = ['Alumno', 'Carrera', 'Semestre Ingreso']
    datos_resumen = [al, carrera, ingreso]
    titulo = 'Historial Curricular'
    registros = cursos

    # escribir el titulo
    sheet1.write(6, 0, titulo, style_titulo)

    row = 8
    col = 0
    # escribir las etiquetas del resumen
    for etiqueta in label_resumen:
        sheet1.write(row, col, etiqueta, style_etiqueta_resumen)
        row += 1

    row = 8
    col = 1
    # escribir los datos del resumen
    for dato in datos_resumen:
        sheet1.write(row, col, dato, style_dato_resumen)
        row += 1

    row += 1
    col = 0
    # escribimos los encabezados
    for head in headers:
        sheet1.write(row, col, head, style_heads)
        col += 1

    row += 1
    col = 1
    n = 1
    # recorremos la lista y escribimos los datos
    creditos_acumulados = 0
    for fila in registros:
        sheet1.write(row, 0, n, style_registros)
        if fila[7] == "Disponible":
            creditos_acumulados += int(fila[3])
        for dato in fila:
            if fila[5] == '':
                if fila[7] == "Disponible":
                    if creditos_acumulados <= int(CREDITOS_MAXIMOS):
                        sheet1.write(row, col, dato, style_nollevados)
                    else:
                        sheet1.write(row, col, dato, style_registros)
                else:
                    sheet1.write(row, col, dato, style_nodisponibles)
            else:
                sheet1.write(row, col, dato, style_registros)
            col += 1
        col = 1
        row += 1
        n += 1

    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=avance_plan_estudios.xls'
    return response


def print_reporte_asistencia(matriculados):
    asistencia_alumnos = []
    for mat in matriculados:
        if mat.count() != 0:
            for cur in mat:
                codigo = cur.MatriculaCiclo.Alumno.Codigo
                apellidos = cur.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + cur.MatriculaCiclo.Alumno.ApellidoMaterno
                nombres = cur.MatriculaCiclo.Alumno.Nombres
                carrera = cur.MatriculaCiclo.Alumno.Carrera.Carrera
                curso = cur.PeriodoCurso.Curso.Nombre
                docente = cur.PeriodoCurso.Docente.ApellidoPaterno + ' ' + cur.PeriodoCurso.Docente.ApellidoMaterno + ' ' + cur.PeriodoCurso.Docente.Nombres
                ciclo = cur.PeriodoCurso.Curso.Ciclo
                asistencias = Asistencia.objects.filter(MatriculaCursos__PeriodoCurso=cur.PeriodoCurso,
                                                        MatriculaCursos__MatriculaCiclo=cur.MatriculaCiclo).order_by(
                    '-Fecha')
                for asi in asistencias:
                    fecha = asi.Fecha
                    dia = asi.Horario.Dia
                    inicio = asi.Horario.HoraInicio.strftime('%H:%M')
                    fin = asi.Horario.HoraFin.strftime('%H:%M')
                    aula = asi.Horario.Seccion
                    estado = asi.Estado
                    asistencia_alumnos.append(
                        [codigo, apellidos, nombres, ciclo, carrera, curso, docente, fecha, dia, inicio, fin, aula,
                         estado])
        else:
            asistencia_alumnos.append(['', '', '', '', '', '', '', '', '', '', '', ''])
    headers = ['Código', 'Apellidos', 'Nombres', 'Ciclo', 'Carrera', 'Curso', 'Docente', 'Fecha', 'Día', 'Inicio',
               'Fin', 'Aula', 'Estado']
    titulo = 'REPORTE DE ASISTENCIAS DE ALUMNOS'
    return response_excel(titulo=titulo, heads=headers, registros=asistencia_alumnos,
                          nombre_archivo='asistencia_alumnos')


def archivo_promedio_ponderado(matriculados):
    ponderado_matriculados = []
    dict = {'0.00': 'cero', '1.00': 'uno', '2.00': 'dos', '3.00': 'tres', '4.00': 'cuatro', '5.00': 'cinco',
            '6.00': 'seis', '7.00': 'siete', '8.00': 'ocho', '9.00': 'nueve', '10.00': 'diez',
            '11.00': 'once', '12.00': 'doce', '13.00': 'trece', '14.00': 'catorce', '15.00': 'quince',
            '16.00': 'dieciseis', '17.00': 'diecisiete', '18.00': 'dieciocho', '19.00': 'diecinueve', '20.00': 'veinte'}
    for mat in matriculados:
        try:
            codigo = mat[0].MatriculaCiclo.Alumno.Codigo
            apellidos = mat[0].MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + mat[
                0].MatriculaCiclo.Alumno.ApellidoMaterno
            nombres = mat[0].MatriculaCiclo.Alumno.Nombres
            carrera = mat[0].MatriculaCiclo.Alumno.Carrera.Carrera
            categoria = mat[0].MatriculaCiclo.Categoria.Categoria + ' - ' + mat[
                0].MatriculaCiclo.Categoria.Moneda + str(mat[0].MatriculaCiclo.Categoria.Pago)
            semestre_actual = mat[0].MatriculaCiclo.Alumno.AnioIngreso + ' - ' + mat[0].MatriculaCiclo.Alumno.Semestre
            colegio = mat[0].MatriculaCiclo.Alumno.Colegio.__unicode__()
            creditos_reporte = 0
            suma_producto = 0
            creditos_aprobados = 0
            for cur in mat:
                if cur.Convalidado is False and cur.Estado is True and cur.PeriodoCurso.Curso.Extracurricular is False:
                    # Antes de calcular el credito primero es necesario
                    # verificar si el curso tiene equivalencia
                    if cur.PeriodoCurso.Curso.Equivalencia:
                        c = cur.PeriodoCurso.Curso.Creditos_equiv
                    else:
                        c = cur.PeriodoCurso.Curso.Creditos
                    # total de creditos
                    creditos_reporte += c
                    # creditos aprobados
                    if cur.aprobado() is True:
                        creditos_aprobados += int(c)
                    suma_producto += int(c) * cur.Promedio()
            # ponderado, total de creditos y creditos aprobados
            ponderado = suma_producto / creditos_reporte
            creditos_desaprobados = int(creditos_reporte) - int(creditos_aprobados)
            # comprobamos que sus pagos hayan sido ha tiempo

            ponderado_matriculados.append(
                [codigo, apellidos, nombres, colegio, carrera, categoria, semestre_actual, creditos_aprobados,
                 creditos_desaprobados, creditos_reporte, Decimal("%.2f" % ponderado)])
        except ValueError:
            ponderado_matriculados.append(['', '', '', '', '', '', '', '', '', '', ''])
    headers = ['Código', 'Apellidos', 'Nombres', 'Colegio', 'Carrera', 'Categoria', 'Semestre Ingreso',
               'Creditos Aprobados', 'Creditos Desaprobados', 'Total Creditos', 'Ponderado']
    titulo = 'REPORTE DE PROMEDIOS PONDERADOS DE ALUMNOS'
    return response_excel(titulo=titulo, heads=headers, registros=ponderado_matriculados,
                          nombre_archivo='ponderado_matriculados')

def print_reporte_notas(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=reporte_notas.pdf'
    p = canvas.Canvas(response, pagesize=A4)

    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    anchoA4 = 595
    altoA4 = 842
    margenLeft = 40
    margenRight = 40

    for mat in matriculados:
        m = 1
        n = 0

        # aqui verificamos que el numero de cursos sea mayor que 4 (utilizar dos hojas)
        if mat.count() > 4:
            r = 2
        else:
            r = 1

        # configurar el reporte de un alumno para que salga en dos hojas
        while m <= r:
            m += 1
            p.setFont("Helvetica", 12)
            p.drawString(205, 800, "EVALUACIÓN Y REGISTRO")
            p.drawString(240, 780, "Reporte de Notas")

            p.setFont("Helvetica-Bold", 9)
            p.drawString(40, 730, "Código :")
            p.drawString(40, 710, "Estudiante :")
            p.drawString(370, 730, "Carrera :")
            p.drawString(370, 710, "Semestre :")

            p.setFont("Helvetica", 9)
            if mat.count() != 0:
                p.drawString(100, 730, mat[0].MatriculaCiclo.Alumno.Codigo)
                p.drawString(100, 710, str(mat[0].MatriculaCiclo.Alumno))
                p.drawString(430, 730, mat[0].MatriculaCiclo.Alumno.Carrera.Carrera)
                p.drawString(430, 710, str(mat[0].MatriculaCiclo.Periodo))

            p.setStrokeColorRGB(0.70588, 0.01176, 0.05098)
            p.line(70, 40, 235, 40)
            p.drawString(75, 25, "Responsable de la Carrera Profesional")

            # lineas de fecha
            p.line(380, 40, 470, 40)
            p.line(410, 43, 415, 53)  # slash 1
            p.line(440, 43, 445, 53)  # slash 2
            # --------------------------
            # Logo
            # --------------------------

            logo = settings.MEDIA_ROOT + 'logoHD.jpg'
            # logo = '/home/proyectos_django/UDL/media/Imagenes/logoHD.png'
            p.drawImage(logo, 10, 765, width=140, height=70)

            i = 640
            j = 625
            p1 = i + 65  # primer punto de la linea vertical
            # los cursos se toman de 4 en 4
            for cur in mat.exclude(Convalidado=True).exclude(Estado=False)[0 + n:4 + n]:
                p.setFont("Helvetica-Bold", 8)
                p.drawString(50, i + 40, "Curso :")
                p.drawString(310, i + 40, "Docente :")
                p.drawString(50, i + 30, "Creditos :")
                p.drawString(310, i + 30, "Ciclo :")

                p.setFont("Helvetica", 8)
                p.drawString(110, i + 40, cur.PeriodoCurso.Curso.Nombre)
                p.drawString(370, i + 40, str(cur.PeriodoCurso.Docente))
                p.drawString(110, i + 30, str(cur.PeriodoCurso.Curso.Creditos))
                p.drawString(370, i + 30, u'%s º' % cur.PeriodoCurso.Curso.Ciclo)

                p.setFont("Helvetica-Bold", 8)
                p.drawString(70, i + 15, "Notas")

                p.setFont("Helvetica", 8)

                j = i

                # se imprimen todas las notas del alumno.
                for nota in cur.Calificaciones().filter(Nota__Nivel='3'):
                    p.drawString(50, i, nota.Nota.Nota + " (%s)" % nota.Nota.Identificador + ":")
                    p.drawString(180, i, str(nota.Valor))
                    i = i - 10

                for nota in cur.Calificaciones().filter(Nota__Nivel='2'):
                    p.drawString(50, i, nota.Nota.Nota + " (%s)" % nota.Nota.Identificador + ":")
                    p.drawString(180, i, str(nota.Valor))
                    i = i - 10

                for nota in cur.Calificaciones().filter(Nota__Nivel='1'):
                    p.drawString(50, i, nota.Nota.Nota + " (%s)" % nota.Nota.Identificador + ":")
                    p.drawString(180, i, str(nota.Valor))
                    i = i - 10

                for nota in cur.Calificaciones().filter(Nota__Nivel='0'):
                    p.drawString(50, i, nota.Nota.Nota + " (%s)" % nota.Nota.Identificador + ":")
                    p.drawString(180, i, str(nota.Valor))
                    i = i - 10

                # impresion de los recuadros
                p.line(40, p1 - 10, 40, i - 10)
                p.line(anchoA4 - margenRight, p1 - 10, anchoA4 - margenRight, i - 10)

                p.line(40, p1 - 10, anchoA4 - margenRight, p1 - 10)
                p.line(40, i - 10, anchoA4 - margenRight, i - 10)

                i = i - 70
                p1 = i + 60

                # imprime las formulas
                notas = Nota.objects.filter(PeriodoCurso=cur.PeriodoCurso, SubNotas=True).order_by('Nivel')
                if notas.count() != 0:
                    p.setFont("Helvetica-Bold", 8)
                    p.drawString(250, j + 15, "Fórmulas")
                    p.setFont("Helvetica", 8)

                for cal1 in notas:
                    subnotas = Nota.objects.filter(NotaPadre=cal1)
                    formula = "%s = (" % cal1.Identificador
                    suma_pesos = 0
                    a = 1
                    for nota in subnotas:
                        if a == 1:
                            formula += "%s*%s" % (nota.Identificador, nota.Peso)
                        else:
                            formula += " + %s*%s" % (nota.Identificador, nota.Peso)
                        suma_pesos += nota.Peso
                        a += 1
                    formula += ")/%s" % suma_pesos
                    p.drawString(220, j, str(formula))
                    j = j - 10

                # imprime las leyendas
                j = j - 20
                if notas.count() != 0:
                    p.setFont("Helvetica-Bold", 8)
                    p.drawString(250, j + 15, "Leyenda")
                    p.setFont("Helvetica", 8)

                for cal1 in notas:
                    subnotas = Nota.objects.filter(NotaPadre=cal1)
                    leyenda = "%s : %s" % (cal1.Identificador, cal1.Nota)
                    p.drawString(220, j, leyenda)
                    j = j - 10

                j = j - 60

            n += 4
            p.showPage()
            p.save()
    return response


def print_reporte_asistencias_pdf(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=reporte_asistencias.pdf'
    # p = canvas.Canvas(response, pagesize = A4)
    # horizontal
    p = canvas.Canvas(response, pagesize=(29.7 * cm, 21 * cm))
    # Por defecto se trabaja en 72dpi 72 ppp: 595x842
    altoA4 = 595
    anchoA4 = 842
    margenLeft = 20
    margenRight = 60

    # recorremos todas las listas de cursos
    for mat in matriculados:
        m = 1  # numero de hoja
        n = 0  # cursos a obtener de mat ,va de cuatro en cuatro

        # aqui verificamos que el numero de cursos sea mayor que 6 (utilizar dos hojas)
        if mat.count() > 6:
            r = 3
        elif mat.count() > 3 and mat.count() < 7:
            r = 2
        else:
            r = 1

        # configurar el reporte de un alumno para que salga en una o dos hojas
        while m <= r:
            m += 1
            # aqui se construye todo el header y footer
            p.setFont("Helvetica", 12)
            p.drawString(330, 540, "EVALUACIÓN Y REGISTRO")
            p.drawString(345, 520, "Reporte de Asistencias")

            p.setFont("Helvetica-Bold", 9)
            p.drawString(40, 500, "Código :")
            p.drawString(40, 480, "Estudiante :")
            p.drawString(370, 500, "Carrera :")
            p.drawString(370, 480, "Semestre :")

            p.setFont("Helvetica", 9)
            if mat.count() != 0:
                p.drawString(100, 500, mat[0].MatriculaCiclo.Alumno.Codigo)
                p.drawString(100, 480, str(mat[0].MatriculaCiclo.Alumno))
                p.drawString(430, 500, mat[0].MatriculaCiclo.Alumno.Carrera.Carrera)
                p.drawString(430, 480, str(mat[0].MatriculaCiclo.Periodo))

            p.setStrokeColorRGB(0.70588, 0.01176, 0.05098)
            p.line(70, 40, 235, 40)
            p.drawString(75, 25, "Responsable de la Carrera Profesional")

            # lineas de fecha
            p.line(380, 40, 470, 40)
            p.line(410, 43, 415, 53)  # slash 1
            p.line(440, 43, 445, 53)  # slash 2
            # --------------------------
            # Logo
            # --------------------------
            logo = settings.MEDIA_ROOT + 'logoHD.jpg'
            # logo = '/home/proyectos_django/UDL/media/Imagenes/logoHD.png'
            p.drawImage(logo, 30, 520, width=140, height=70)
            # c.drawImage(logo,30,520,width=140,height=62)

            i = 420  # altura a la que empezamos a escribir el encabezado de cada curso
            # j = 400

            # los cursos se toman de 3 en 3
            for cur in mat.exclude(Convalidado=True)[0 + n:3 + n]:
                # escribir el encabezado de cada curso
                p.setFont("Helvetica-Bold", 8)
                p.drawString(50, i + 40, "Curso :")
                p.drawString(280, i + 40, "Créditos :")
                p.drawString(380, i + 40, "Horario :")
                p.drawString(50, i + 30, "Docente :")
                p.drawString(280, i + 30, "Ciclo :")

                p.setFont("Helvetica", 8)
                p.drawString(110, i + 40, cur.PeriodoCurso.Curso.Nombre)
                p.drawString(340, i + 40, str(cur.PeriodoCurso.Curso.Creditos))
                p.drawString(110, i + 30, str(cur.PeriodoCurso.Docente))
                p.drawString(340, i + 30, u'%s º' % cur.PeriodoCurso.Curso.Ciclo)

                # recorremos e imprimimos los horarios del curso
                horarios = cur.PeriodoCurso.horario_set.all()
                h = 0
                alumno = cur
                total_asi = 0

                for hor in horarios:
                    p.drawString(420, i + 40 - h, hor.Etiqueta())
                    h += 10

                    # buscamos el alumno que registra mas asistencias en el curso para tomarlo como referencia
                    grupos = Horario.objects.filter(PeriodoCurso__Periodo=hor.PeriodoCurso.Periodo, Dia=hor.Dia,
                                                    HoraInicio=hor.HoraInicio, HoraFin=hor.HoraFin,
                                                    Seccion=hor.Seccion).values('PeriodoCurso__id').distinct()

                    for g in grupos:
                        matriculados = MatriculaCursos.objects.filter(PeriodoCurso__id=g['PeriodoCurso__id'])
                        for mat1 in matriculados:
                            if total_asi > mat1.asistencia_set.all().count():
                                total_asi = mat1.asistencia_set.all().count()
                                alumno = mat1

                # obtenemos todas las fechas en las que se ha tomado asistencia en el curso
                fechas = Asistencia.objects.filter(MatriculaCursos=alumno).order_by('Fecha');

                # imprimirmos el % de asistencias, faltas y tardanzas del alumno
                p.setFont("Helvetica-Bold", 8)
                p.drawString(50, i + 20, "Asistencias :")
                p.drawString(280, i + 20, "Faltas :")
                p.drawString(380, i + 20, "Tardanzas :")

                p.setFont("Helvetica", 8)

                if len(fechas) != 0:
                    # p.drawString(110,i + 20,str((cur.NroAsistencias() * 100) / len(fechas)) + ' %')
                    # p.drawString(340,i + 20,str((cur.NroFaltas() * 100) / len(fechas)) + ' %')
                    # p.drawString(440,i + 20,str((cur.NroTardanzas() * 100) / len(fechas)) + ' %')
                    if cur.PorcentajeAsistencias() > 100:
                        p.drawString(110, i + 20, '100' + ' %')
                    else:
                        p.drawString(110, i + 20, str(cur.PorcentajeAsistencias()) + ' %')
                    p.drawString(340, i + 20, str(cur.PorcentajeFaltas()) + ' %')
                    p.drawString(440, i + 20, str((cur.NroTardanzas() * 100) / len(fechas)) + ' %')
                else:
                    t = cur.NroAsistencias() + cur.NroFaltas() + cur.NroTardanzas()
                    p.drawString(110, i + 20, str(len(fechas)) + ' %')
                    p.drawString(340, i + 20, str(len(fechas)) + ' %')
                    p.drawString(440, i + 20, str(len(fechas)) + ' %')

                p.line(40, i + 10, anchoA4 - margenRight, i + 10)  # linea horizontal superior
                p.line(40, i - 45, anchoA4 - margenRight, i - 45)  # linea horizontal media
                p.line(40, i - 60, anchoA4 - margenRight, i - 60)  # linea horizontal inferior

                p.line(40, i + 10, 40, i - 60)  # linea vertical izquierda
                p.line(anchoA4 - margenRight, i + 10, anchoA4 - margenRight, i - 60)  # linea vertical derecha

                # imprimimos las fechas y el estado de las asistencias del alumno
                o = 0
                q = -50
                for fecha in fechas:
                    p.saveState()
                    p.rotate(90)
                    p.drawString(i - 40, q - o, str(fecha.Fecha))
                    p.restoreState()
                    try:
                        asistencia = Asistencia.objects.get(MatriculaCursos=cur, Fecha=fecha.Fecha,
                                                            Horario__id=fecha.Horario_id)
                        p.drawString(-q + o - 5, i - 55, asistencia.Estado[0])
                    except:
                        p.drawString(-q + o - 5, i - 55, '*')
                    o += 15
                q += 100

                i = i - 130

            n += 3
            p.showPage()
            p.save()
    return response


def datos_matriculados_excel(matriculados):
    listado = []
    for obj in matriculados:
        codigo = obj.Alumno.Codigo
        apellidopat = obj.Alumno.ApellidoPaterno
        apellidomat = obj.Alumno.ApellidoMaterno
        nombres = obj.Alumno.Nombres
        nacimiento = obj.Alumno.Nacimiento
        sexo = obj.Alumno.Sexo
        nrodocumento = obj.Alumno.NroDocumento
        departamento = obj.Alumno.Provincia.Departamento.Departamento
        relacion = obj.Alumno.Parentesco.Parentesco
        provincia = obj.Alumno.Provincia.Provincia
        distrito = obj.Alumno.Distrito.Distrito
        direccion = obj.Alumno.Direccion
        urbanizacion = obj.Alumno.Urbanizacion
        email = obj.Alumno.Email
        telefono = obj.Alumno.Telefono
        celular = obj.Alumno.Celular
        carrera = obj.Alumno.Carrera.__unicode__()
        anioingreso = obj.Alumno.AnioIngreso
        semestre = obj.Alumno.Semestre
        modalidad = obj.Alumno.Modalidad
        colegio = obj.Alumno.Colegio.__unicode__()
        tipo_colegio = obj.Alumno.Colegio.Tipo
        provincia_colegio = obj.Alumno.Colegio.Provincia.Provincia
        distrito_colegio = obj.Alumno.Colegio.Distrito
        egresocolegio = obj.Alumno.AnioEgreso
        apellidos_tutor = obj.Alumno.Parentesco.ApePatTutor + ' ' + obj.Alumno.Parentesco.ApeMatTutor
        nombres_tutor = obj.Alumno.Parentesco.NombresTutor
        dir_tutor = obj.Alumno.Parentesco.DirTutor
        urb_tutor = obj.Alumno.Parentesco.UrbTutor
        tel_tutor = obj.Alumno.Parentesco.TelfTutor
        cel_tutor = obj.Alumno.Parentesco.CelTutor
        dni_tutor = obj.Alumno.Parentesco.DNITutor
        doc_entregados = obj.Alumno.documentos_entregados()
        doc_noentregados = obj.Alumno.documentos_no_entregados()
        tiene_seguro = 'No'
        copia_dni = 'No'
        copia_recibo = 'No'
        anioegresado = "--"
        semestreegresado = "--"
        if obj.TieneSeguro:
            tiene_seguro = 'Si'
        if obj.CopiaDni:
            copia_dni = 'Si'
        if obj.ReciboLuzAgua:
            copia_recibo = 'Si'
        if obj.Alumno.Foto == '':
            foto = 'No'
        else:
            foto = 'Si'
        if obj.Alumno.Egreso:
            egreso = 'Si'
            anioegresado = obj.Alumno.AnioEgresado
            semestreegresado = obj.Alumno.SemestreEgresado
        else:
            egreso = 'No'
        observacion = obj.Observaciones
        estado = obj.Estado
        if obj.Alumno.GeneroCarne:
            carne = 'Si'
        else:
            carne = 'No'

        listado.append(
            [codigo, apellidopat.upper(), apellidomat.upper(), nombres.upper(), nacimiento, sexo, nrodocumento,
             departamento, provincia, distrito, direccion, urbanizacion, email, telefono, celular, carrera, anioingreso,
             semestre, modalidad, colegio, tipo_colegio, provincia_colegio, distrito_colegio, egresocolegio,
             apellidos_tutor, nombres_tutor, dir_tutor, urb_tutor, tel_tutor, cel_tutor, dni_tutor, relacion,
             doc_entregados, doc_noentregados, foto, estado, tiene_seguro, copia_dni, copia_recibo, egreso, observacion,
             carne, anioegresado, semestreegresado])
    headers = ['Código', 'Apellido Paterno', 'Apellido Materno', 'Nombres', 'Nacimiento', 'Sexo', 'Nro Documento',
               'Departamento', 'Provincia', 'Distrito', 'Direccion', 'Urb.', 'Email', 'Telefono', 'Celular', 'Carrera',
               'Anio Ingreso', 'Semestre', 'Modalidad', 'Colegio', 'Tipo Colegio', 'Prov Colegio', 'Distrito Colegio',
               'Egreso Colegio', 'Apellidos Tutor', 'Nombres Tutor', 'Dir. Tutor', 'Urb. Tutor', 'Telef. Tutor',
               'Cel. Tutor', 'DNI Tutor', 'Parentesco', 'Doc Entregados', 'Doc No Entregados', 'Foto', 'Estado',
               '¿Tiene seguro?', 'Copia de DNI', 'Copia de recibo', 'Egreso', 'Observacion', '¿Se generó carne?',
               'Año egresado', "Semestre egresado"]
    titulo = 'REPORTE DE MATRICULADOS'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='reporte_matriculados')


def listado_categorias_excel(matriculados):
    listado = []
    for obj in matriculados:
        codigo = obj.Alumno.Codigo
        apellidos = obj.Alumno.ApellidoPaterno + ' ' + obj.Alumno.ApellidoMaterno
        nombres = obj.Alumno.Nombres
        carrera = obj.Alumno.Carrera.__unicode__()
        anioingreso = obj.Alumno.AnioIngreso
        semestre = obj.Alumno.Semestre
        modalidad = obj.Alumno.Modalidad
        categoria = obj.Categoria.Categoria
        monto = obj.Categoria.Pago
        fecha_matricula = obj.FechaMatricula
        estado = obj.Estado
        fecha_retiro = obj.FechaRetiro
        observaciones = obj.Observaciones

        listado.append(
            [codigo, apellidos, nombres, carrera, anioingreso, semestre, modalidad, categoria, monto, fecha_matricula,
             estado, fecha_retiro, observaciones])
    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Año Ingreso', 'Semestre', 'Modalidad', 'Categoria',
               'Monto', 'Fecha Matrícula', 'Estado', 'Fecha Retiro', 'Observaciones']
    titulo = 'REPORTE DE CATEGORIAS DE MATRICULADOS'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='categorias_matriculados')


def print_historial_categorias(matriculados):
    output_name = "historial_categorias"
    encoding = 'utf8'
    import StringIO
    output = StringIO.StringIO()
    try:
        import xlwt
        from xlwt import Workbook, easyxf
    except ImportError:
        pass
    book = xlwt.Workbook(encoding=encoding)
    # styles
    style_heads = easyxf(
        'font: name Arial,bold True;''borders: left thin, right thin, top thin, bottom thin;''pattern: pattern solid;')
    style_heads.pattern.pattern_fore_colour = 0x9ACD32
    style_registros = easyxf(
        'font: name Arial;''borders: left thin, right thin, top thin, bottom thin;')
    i = 1
    heads = ['Periodo', 'Categoria', 'Monto']
    for id_mat in matriculados:
        codigo = None
        alumno = None
        carrera = None
        matriculado = None
        try:
            matriculado = MatriculaCiclo.objects.get(id=id_mat)
            codigo = matriculado.Alumno.Codigo
            alumno = matriculado.Alumno.__unicode__()
            carrera = str(matriculado.Alumno.Carrera)
        except ValueError:
            pass
        sheet = book.add_sheet('Hoja%s' % i)
        sheet.write(0, 0, 'Codigo:')
        sheet.write(1, 0, 'Apellidos y nombre')
        sheet.write(2, 0, 'Carrera:')
        sheet.write(3, 0, 'Periodo ingreso:')

        sheet.write(0, 1, codigo)
        sheet.write(1, 1, alumno)
        sheet.write(2, 1, carrera)
        sheet.write(3, 1, matriculado.Alumno.AnioIngreso +
                    ' - ' + matriculado.Alumno.Semestre)

        row = 5
        col = 1
        sheet.write(row, 0, 'Num', style_heads)
        for head in heads:
            sheet.write(row, col, head, style_heads)
            col += 1

        row += 1
        col = 0
        semestres = MatriculaCiclo.objects.filter(Alumno__id=matriculado.Alumno.id).order_by('Periodo__Anio',
                                                                                             'Periodo__Semestre')
        contador = 1
        for semestre in semestres:
            sheet.write(row, col, contador, style_registros)
            sheet.write(row, col + 1, semestre.Periodo.__unicode__(), style_registros)
            sheet.write(row, col + 2, semestre.Categoria.Categoria, style_registros)
            sheet.write(row, col + 3, semestre.Categoria.Pago, style_registros)
            contador = contador + 1
            row = row + 1
        i = i + 1

    content_type = 'application/vnd.ms-excel'
    file_ext = 'xls'
    book.save(output)
    output.seek(0)
    response = HttpResponse(content=output.getvalue(), content_type=content_type)
    response['Content-Disposition'] = 'attachment;filename="%s.%s"' % (
        output_name.replace('"', '\"'), file_ext)
    return response


def print_constancia_matricula(matriculados):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=constancia_matricula.pdf'
    # p = canvas.Canvas(response, pagesize = (21*cm, 29.7*cm*.5))
    p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))
    for mat in matriculados:
        # dibujar ficha inferior
        p = constancia_matricula(p, 0)
        p.setFont("Helvetica", 12)
        codigo = mat[0].MatriculaCiclo.Alumno.Codigo
        alumno = mat[0].MatriculaCiclo.Alumno
        alumno = alumno.ApellidoPaterno + " " + alumno.ApellidoMaterno + ", " + alumno.Nombres
        carrera = mat[0].MatriculaCiclo.Alumno.Carrera.Carrera
        periodo = str(mat[0].MatriculaCiclo.Periodo)
        sexo = ''
        identificado = ''
        matriculado = ''
        if str(mat[0].MatriculaCiclo.Alumno.Sexo) == 'M':
            sexo = 'el alumno'
            identificado = 'identificado con el'
            matriculado = ', se encuentra matriculado'
        else:
            sexo = 'la alumna'
            identificado = 'identificada con el'
            matriculado = ', se encuentra matriculada'

        p.drawString(68, 240, sexo)
        p.setFont("Helvetica-Bold", 12)
        p.drawString(125, 240, alumno.upper())
        p.setFont("Helvetica", 12)
        p.drawString(450, 240, identificado)

        # tercerca línea
        p.setFont("Helvetica-Bold", 12)
        p.drawString(78, 225, codigo)
        p.setFont("Helvetica", 12)
        p.drawString(132, 225, matriculado)

        # cuarta linea
        p.setFont("Helvetica-Bold", 12)
        p.drawString(140, 210, carrera.upper())
        p.setFont("Helvetica-Bold", 12)
        p.drawString(450, 210, periodo)
        p.setFont("Helvetica", 12)

        creditos = 0
        for cur in mat:
            if cur.Convalidado is False and cur.Estado is True:
                creditos += cur.PeriodoCurso.Curso.Creditos

        p.setFont("Helvetica-Bold", 12)
        p.drawString(120, 195, str(creditos))

        mydate = mat[0].MatriculaCiclo.FechaMatricula

        mn = {'01': 'Enero', '02': 'Febrero', '03': 'Marzo', '04': 'Abril', '05': 'Mayo',
              '06': 'Junio', '07': 'Julio', '08': 'Agosto', '09': 'Setiembre',
              '10': 'Octubre', '11': 'Noviembre', '12': 'Diciembre'}

        p.setFont("Helvetica", 12)
        p.drawString(350, 140, "Chiclayo,   " + str(mydate.strftime("%d")))
        # c.line(395, 140, 410, 140)
        p.drawString(425, 140, "de   " + str(mn[mydate.strftime("%m")]))
        # c.line(430, 140, 500, 140)
        p.drawString(505, 140, "del  " + str(mydate.strftime("%Y")))
        # c.line(520, 140, 555, 140)

        # dibujar ficha superior
        # w = 420
        p = constancia_matricula(p, 420, True)
        p.setFont("Helvetica-Bold", 10)
        codigo = mat[0].MatriculaCiclo.Alumno.Codigo
        alumno = mat[0].MatriculaCiclo.Alumno
        alumno = alumno.ApellidoPaterno + " " + alumno.ApellidoMaterno + ", " + alumno.Nombres
        carrera = mat[0].MatriculaCiclo.Alumno.Carrera.Carrera
        periodo = str(mat[0].MatriculaCiclo.Periodo)

        p.setFont("Helvetica", 12)
        p.drawString(68, 240 + 420, sexo)
        p.setFont("Helvetica-Bold", 12)
        p.drawString(125, 240 + 420, alumno.upper())
        p.setFont("Helvetica", 12)
        p.drawString(450, 240 + 420, identificado)

        # tercerca línea
        p.setFont("Helvetica-Bold", 12)
        p.drawString(78, 225 + 420, codigo)
        p.setFont("Helvetica", 12)
        p.drawString(132, 225 + 420, matriculado)

        # cuarta linea
        p.setFont("Helvetica-Bold", 12)
        p.drawString(140, 210 + 420, carrera.upper())
        p.setFont("Helvetica-Bold", 12)
        p.drawString(450, 210 + 420, periodo)
        p.setFont("Helvetica", 12)

        p.setFont("Helvetica-Bold", 12)
        p.drawString(120, 195 + 420, str(creditos))

        p.setFont("Helvetica", 12)
        p.drawString(350, 140 + 420, "Chiclayo,   " + str(mydate.strftime("%d")))
        # c.line(395, 140, 410, 140)
        p.drawString(425, 140 + 420, "de   " + str(mn[mydate.strftime("%m")]))
        # c.line(430, 140, 500, 140)
        p.drawString(505, 140 + 420, "del  " + str(mydate.strftime("%Y")))
        # c.line(520, 140, 555, 140)

        p.showPage()
        p.save()
    return response
