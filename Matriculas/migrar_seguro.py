#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

# imports para traer variable de entorno django
from Matriculas.models import MatriculaCiclo

matriculados = MatriculaCiclo.objects.filter(Periodo__id=19)
for matriculado in matriculados:
    alumno = matriculado.Alumno
    try:
        matricula_anterior = MatriculaCiclo.objects.get(Periodo__id=18, Alumno=alumno)
        matriculado.TieneSeguro = matricula_anterior.TieneSeguro
        matriculado.CopiaDni = matricula_anterior.CopiaDni
        matriculado.ReciboLuzAgua = matricula_anterior.ReciboLuzAgua
        matriculado.save()
    except MatriculaCiclo.DoesNotExist:
        print "alumno no encontrado: " + alumno.Codigo
        pass
