# -*- coding: utf-8 -*-
# from Alumnos.widgets import DateTimeWidget
import datetime

from django import forms
from django import template
from django.contrib import admin
# imports para eliminar matricula de cursos
from django.contrib.admin.utils import unquote, get_deleted_objects
from django.core.exceptions import PermissionDenied
from django.db import transaction, router
from django.http import Http404
from django.shortcuts import render_to_response
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_unicode
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_protect

from Categorias.models import Categoria
from Matriculas.models import MatriculaCiclo, MatriculaCursos
# import generar reporte en excel
from funciones import response_excel


# model form para el modelo alumno
class MatriculaCicloAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MatriculaCicloAdminForm, self).__init__(*args, **kwargs)
        try:
            self.fields['FechaMatricula'].initial = datetime.date.today()
            self.fields['selected_cat'].initial = self.instance.Categoria.id
        except:
            pass

    Categoria = forms.ModelChoiceField(queryset=Categoria.objects.filter(Estado=True), widget=forms.Select(),
                                       required=True, label="Categoría")
    # FechaMatricula = forms.DateField(widget = DateTimeWidget,required = True, label = "Fecha de Matrícula")
    # FechaRetiro = forms.DateField(widget = DateTimeWidget,required = False, label = "Fecha de Retiro")
    selected_cat = forms.CharField(widget=forms.HiddenInput, required=False, label="")

    class Meta:
        model = MatriculaCiclo
        fields = '__all__'

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Matriculas/frm_matricula_ciclo.js"
              )


csrf_protect_m = method_decorator(csrf_protect)


class MatriculaCicloAdmin(admin.ModelAdmin):
    list_display = ('Codigo', 'Alumno', 'Periodo', 'Carrera', 'Categoria', 'FechaMatricula', 'Estado', 'Observaciones')
    search_fields = (
        'Alumno__ApellidoPaterno', "Alumno__ApellidoMaterno", 'Alumno__Nombres', 'Alumno__Carrera__Carrera')
    list_filter = ('Periodo', 'Categoria', 'Estado')
    actions = ['listado_excel', 'listado_categorias_excel']
    raw_id_fields = ('Alumno',)
    form = MatriculaCicloAdminForm
    fieldsets = (
        ('Datos de la Matrícula', {
            'fields': ('Periodo', 'Alumno', 'Categoria', 'FechaMatricula', 'Estado', 'FechaRetiro', 'Observaciones',
                       'selected_cat'),
        }),
    )

    @csrf_protect_m
    @transaction.atomic()
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        using = router.db_for_write(self.model)

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, model_count, perms_needed, protected) = get_deleted_objects(
            [obj], opts, request.user, self.admin_site, using)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            self.delete_model(request, obj)

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Matricula Eliminada Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        object_name = force_unicode(opts.verbose_name)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": object_name}
        else:
            title = _("Are you sure?")

        context = {
            "title": title,
            "object_name": object_name,
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": opts,
            "app_label": app_label,
        }
        context.update(extra_context or {})

        return TemplateResponse(request, self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, current_app=self.admin_site.name)

    def listado_excel(modeladmin, request, queryset):
        from Alumnos.views import alumnos_excel
        alumnos = []
        for q in queryset:
            alumnos.append(q.Alumno)
        return alumnos_excel(alumnos)

    listado_excel.short_description = "Listado de matriculados seleccionados en Excel"

    def listado_categorias_excel(modeladmin, request, queryset):
        listado = []
        for obj in queryset:
            codigo = obj.Alumno.Codigo
            apellidos = obj.Alumno.ApellidoPaterno + ' ' + obj.Alumno.ApellidoMaterno
            nombres = obj.Alumno.Nombres
            carrera = obj.Alumno.Carrera.__unicode__()
            anioingreso = obj.Alumno.AnioIngreso
            semestre = obj.Alumno.Semestre
            modalidad = obj.Alumno.Modalidad
            categoria = obj.Categoria.Categoria
            monto = obj.Categoria.Pago
            fecha_matricula = obj.FechaMatricula
            estado = obj.Estado
            fecha_retiro = obj.FechaRetiro

            listado.append([codigo, apellidos, nombres, carrera, anioingreso, semestre, modalidad, categoria, monto,
                            fecha_matricula, estado, fecha_retiro])
        headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Año Ingreso', 'Semestre', 'Modalidad', 'Categoria',
                   'Monto', 'Fecha Matrícula', 'Estado', 'Fecha Retiro']
        titulo = 'Listado de Matriculados UDL'
        return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='matriculados')

    listado_categorias_excel.short_description = "Listado de categorias de alumnos"


admin.site.register(MatriculaCiclo, MatriculaCicloAdmin)


class MatriculaCursosAdmin(admin.ModelAdmin):
    list_display = ('MatriculaCiclo', 'PeriodoCurso', 'FechaMatricula', 'Convalidado', 'Estado', 'FechaRetiro')
    search_fields = ('MatriculaCiclo__Alumno__ApellidoPaterno', "MatriculaCiclo__Alumno__ApellidoMaterno",
                     'MatriculaCiclo__Alumno__Nombres',)
    raw_id_fields = ('MatriculaCiclo', 'PeriodoCurso')

    @csrf_protect_m
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, perms_needed) = get_deleted_objects((obj,), opts, request.user, self.admin_site)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            obj.delete()

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Curso Matriculado Eliminado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        context = {
            "title": _("Are you sure?"),
            "object_name": force_unicode(opts.verbose_name),
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "opts": opts,
            "root_path": self.admin_site.root_path,
            "app_label": app_label,
        }
        context.update(extra_context or {})
        context_instance = template.RequestContext(request, current_app=self.admin_site.name)
        return render_to_response(self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, context_instance=context_instance)


admin.site.register(MatriculaCursos, MatriculaCursosAdmin)
