#!/usr/bin/python
# -*- coding: utf-8 -*-
# imports para traer variable de entorno django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

# imports para generar archivo en excel
from Matriculas.models import MatriculaCiclo
from tempfile import TemporaryFile
from xlwt import Workbook


def faltan_matricular():
    book = Workbook()
    sheet1 = book.add_sheet('Hoja 1')

    # escribimos los encabezados
    sheet1.write(14, 0, 'Nro')
    sheet1.write(14, 1, 'Carrera')
    sheet1.write(14, 2, 'Codigo')
    sheet1.write(14, 3, 'Apellidos')
    sheet1.write(14, 4, 'Nombres')

    i = 15
    matriculados = MatriculaCiclo.objects.filter(Periodo__id=10, Estado='Matriculado')

    # recorremos la consulta y escribimos los resultados
    for mat in matriculados:
        if mat.matriculacursos_set.all().count() == 0:
            sheet1.write(i, 0, i - 14)
            sheet1.write(i, 1, mat.Alumno.Carrera.Carrera)
            sheet1.write(i, 2, mat.Alumno.Codigo)
            sheet1.write(i, 3, mat.Alumno.ApellidoPaterno + " " + mat.Alumno.ApellidoMaterno)
            sheet1.write(i, 4, mat.Alumno.Nombres)
            i += 1
    book.save('faltan_matricular.xls')
    book.save(TemporaryFile())


faltan_matricular()
