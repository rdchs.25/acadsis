# -*- coding: utf-8 -*-
from django.conf.urls import url

from Matriculas.reportes import frm_reporte_matricula_cursos
from Matriculas.views import redireccionar_matriculas, index_matriculas, buscar_alumno_matriculado, \
    buscar_ingresante_matricular, agregar_matriculaciclo, editar_matriculaciclo, cursos_matriculado, \
    delete_cursos_matriculado, add_cursos_matriculado, editar_cursos_matriculado, detalle_curso_matriculado, \
    asistente_add_cursos, asistente_registrar_cursos, asistente_descargar_ficha, editar_seguro

app_name = 'Matriculas'

urlpatterns = [
    url(r'^Matriculas/matriculaciclo/$', redireccionar_matriculas),
    url(r'^Matriculas/matriculacursos/add/$', redireccionar_matriculas),
    url(r'^Matriculas/matriculaciclo/add/$', redireccionar_matriculas),
    url(r'^Matriculas/matriculacursos/$', index_matriculas),
    url(r'^Matriculas/matriculacursos/individual/periodo/(\d+)/$', buscar_alumno_matriculado),
    url(r'^Matriculas/matriculaciclo/add/(\d+)/$', buscar_ingresante_matricular),
    url(r'^Matriculas/matriculaciclo/add/(\d+)/(\d+)/$', agregar_matriculaciclo),
    url(r'^Matriculas/matriculaciclo/(\d+)/$', editar_matriculaciclo),
    url(r'^Matriculas/matriculaciclo/seguro/(\d+)/$', editar_seguro),
    url(r'^Matriculas/matriculacursos/individual/(\d+)/$', cursos_matriculado),
    url(r'^Matriculas/matriculacursos/individual/(\d+)/delete/$', delete_cursos_matriculado),
    url(r'^Matriculas/matriculacursos/addcursos/(\d+)/$', add_cursos_matriculado),
    url(r'^Matriculas/matriculacursos/individual/editar/(\d+)/$', editar_cursos_matriculado),
    url(r'^Matriculas/matriculacursos/individual/detalle/(\d+)/$', detalle_curso_matriculado),
    url(r'^Matriculas/matriculacursos/reportes/ciclo/$', frm_reporte_matricula_cursos),
    url(r'^Matriculas/matriculacursos/asistenteaddcursos/(\d+)/$', asistente_add_cursos),
    url(r'^Matriculas/matriculacursos/asistenteaddcursos/(\d+)/registrar/$', asistente_registrar_cursos),
    url(r'^Matriculas/matriculacursos/asistenteaddcursos/(\d+)/descargar_ficha/$', asistente_descargar_ficha),
]
