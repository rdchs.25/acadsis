# -*- coding: utf-8 -*-
import hashlib

from django import forms
from django.contrib.auth.models import User
from django.forms.utils import ErrorList

from Alumnos.widgets import DateTimeWidget
from Cursos.models import PeriodoCurso
from Docentes.models import MoodleUser
from Matriculas.models import CONDICION_CHOICES
from Periodos.models import Periodo


class MatriculaCicloForm(forms.Form):
    Categoria = forms.ModelChoiceField(queryset=None, widget=forms.Select(), required=True)
    FechaMatricula = forms.DateField(widget=DateTimeWidget)
    Condicion = forms.ChoiceField(choices=CONDICION_CHOICES, required=True)
    FechaRetiro = forms.DateField(widget=DateTimeWidget, required=False)
    Exoneradoidiomas = forms.BooleanField(label="¿Exonerado de idiomas?", required=False)
    MatriculaProyectoFormativo = forms.BooleanField(label="¿Activado proyecto formativo?", required=False)
    Observaciones = forms.CharField(widget=forms.Textarea, required=False)

    def __init__(self, categorias, *args, **kwargs):
        super(MatriculaCicloForm, self).__init__(*args, **kwargs)
        self.fields['Categoria'].queryset = categorias

    class Media:
        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class IndexMatriculaForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Periodo')


class MatriculaCicloSeguroForm(forms.Form):
    TieneSeguro = forms.BooleanField(label="¿Tiene seguro?", required=False)
    CopiaDni = forms.BooleanField(label="Copia de DNI", required=False)
    ReciboLuzAgua = forms.BooleanField(label="Recibo de luz o agua", required=False)

    def __init__(self, *args, **kwargs):
        super(MatriculaCicloSeguroForm, self).__init__(*args, **kwargs)

    class Media:
        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class MatriculaCursosForm(forms.Form):
    MatriculaCiclo = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    PeriodoCurso = forms.ModelMultipleChoiceField(queryset=PeriodoCurso.objects.all(), required=True)
    FechaMatricula = forms.DateField(widget=DateTimeWidget)
    Convalidacion = forms.BooleanField(label="¿Convalidado(s)?", required=False)

    class Media:
        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


# editar curso de matriculado en ciclo académico

class EditMatriculaCursosForm(forms.Form):
    FechaMatricula = forms.DateField(widget=DateTimeWidget)
    Convalidacion = forms.BooleanField(label="¿Convalidado?", required=False)
    Resolucion = forms.CharField(max_length=20, required=False, label="Nº Decreto")
    Estado = forms.BooleanField(label="¿Matriculado?", required=False)
    FechaRetiro = forms.DateField(widget=DateTimeWidget, required=False)

    class Media:
        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class LoginFormMatriculaOnline(forms.Form):
    Usuario = forms.CharField(label="Usuario", required=True)
    Password = forms.CharField(widget=forms.PasswordInput, label="Contraseña", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        user = cleaned_data.get("Usuario")
        passwd = cleaned_data.get("Password")

        if user and passwd:
            # if user == "137029E":
            # return cleaned_data
            try:
                validar_usuario = MoodleUser.objects.using('moodle').get(username=user)
                usuario_udl = User.objects.get(username=user)

                grupos = usuario_udl.groups.all()
                alumno = False
                for grupo in grupos:
                    if grupo.name == "Alumnos UDL":
                        alumno = True
                        break

                if alumno:

                    try:
                        usuario = MoodleUser.objects.using('moodle').get(username=user,
                                                                         password=hashlib.md5(passwd).hexdigest())
                        usuario_udl.set_password(passwd)
                        usuario_udl.save()
                    except MoodleUser.DoesNotExist:
                        msg = u'Usuario Incorrecto'
                        msg1 = u'Contraseña Incorrecta'
                        self._errors["Usuario"] = ErrorList([msg])
                        self._errors["Password"] = ErrorList([msg1])
                        del cleaned_data["Usuario"]
                else:
                    msg = u'No eres estudiante'
                    self._errors["Usuario"] = ErrorList([msg])
                    del cleaned_data["Usuario"]

            except (MoodleUser.DoesNotExist, User.DoesNotExist):
                msg = u'Usuario Incorrecto'
                self._errors["Usuario"] = ErrorList([msg])
                del cleaned_data["Usuario"]

        return cleaned_data
