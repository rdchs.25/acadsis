# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

from Matriculas.models import MatriculaCursos
from Cursos.models import PeriodoCurso

matriculados = MatriculaCursos.objects.filter(PeriodoCurso__Periodo__id=12)
lista = []
for m in matriculados:
    curso = '%s_%s_%s_%s_%s_%s' % (
        m.PeriodoCurso.Periodo.Anio, m.PeriodoCurso.Periodo.Semestre, m.PeriodoCurso.Curso.Carrera.Codigo,
        m.PeriodoCurso.Curso.Ciclo, m.PeriodoCurso.Curso.Codigo, m.PeriodoCurso.Grupo)
    usuario = '%s' % m.MatriculaCiclo.Alumno.Codigo
    lista.append([curso, usuario, 'student'])

cursos = PeriodoCurso.objects.filter(Periodo__id=12)

for c in cursos:
    curso = '%s_%s_%s_%s_%s_%s' % (
        c.Periodo.Anio, c.Periodo.Semestre, c.Curso.Carrera.Codigo, c.Curso.Ciclo, c.Curso.Codigo, c.Grupo)
    usuario = c.Docente.username
    lista.append([curso, usuario, 'editingteacher'])

import MySQLdb

db = MySQLdb.connect(host="mysql.udlcix.edu.pe", user="root", passwd="celad0n065", db="udl")
cursor = db.cursor()

try:
    cursor.execute("""
                       CREATE TABLE IF NOT EXISTS `tb_externa_moodle`
                       (
                           id              int(11) NOT NULL AUTO_INCREMENT,
                           curso           VARCHAR(50) NOT NULL,
                           username        VARCHAR(50) NOT NULL,
                           rol         VARCHAR(50) NOT NULL,
                           PRIMARY KEY (`id`)
                       )
                    """)
except:
    pass

cursor.execute("""
                   TRUNCATE TABLE `tb_externa_moodle`
                """)

for l in lista:
    # escribimos la sentencia sql que efectuara la insercion de registros en la tabla td_externa_moodle
    sql = "INSERT INTO tb_externa_moodle  (curso, username, rol) VALUES ('%s','%s','%s')" % (l[0], l[1], l[2])
    # ejecutamos la consulta
    cursor.execute(sql)
cursor.close()
