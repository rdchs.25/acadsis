# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

from Matriculas.models import MatriculaCiclo

matriculados = MatriculaCiclo.objects.filter(Periodo__id=6, Estado='Matriculado')
c = matriculados.count()
lista = []
for m in matriculados:
    prerequisitos = []
    for curso in m.matriculacursos_set.all():
        if curso.PeriodoCurso.Curso.Prerequisito != '':
            prerequisitos.append(curso.PeriodoCurso.Curso.Prerequisito.Nombre)

    for curso in m.matriculacursos_set.all():
        if curso.PeriodoCurso.Curso.Nombre in prerequisitos:
            print m, curso.PeriodoCurso.Curso.Nombre
            # lista.append([m.Alumno.ApellidoPaterno, m.Alumno.ApellidoMaterno,curso.PeriodoCurso.Curso.Nombre])

# archivo_excel('listado', datos_resumen=[c], heads=['codigo', 'alumno', 'carrera', 'celular', 'telefono'],
#               registros=lista, nombre_archivo='faltan_matricular.xls')
