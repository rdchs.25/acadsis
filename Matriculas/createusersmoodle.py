#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Matriculas.models import MatriculaCiclo


def crearusuarios():
    matriculas = MatriculaCiclo.objects.filter(Periodo_id=25, Alumno__Moodleid=None)
    for matricula in matriculas:
        alumno = matricula.Alumno
        alumno.save()


crearusuarios()
