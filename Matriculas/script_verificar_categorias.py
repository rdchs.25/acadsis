#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

# imports para traer variable de entorno django
import codecs
from Categorias.models import Categoria
from Matriculas.models import MatriculaCiclo
from Alumnos.models import Alumno


def script_matricular():
    file1 = codecs.open("ingenieria_ambiental.csv", 'r', 'utf-8')
    for line in file1:
        line = line.strip('\n')
        linea = line.split(',')
        codigo = linea[0]
        id_categoria = linea[1]

        try:
            alumno = Alumno.objects.get(Codigo=codigo)
        except:
            print "ERROR, no existe alumno con codigo %s" % codigo
            continue

        try:
            obj = MatriculaCiclo.objects.get(Periodo_id=19, Alumno=alumno)
        except:
            print "Error %s" % alumno
            continue
        # print "Categoria %s vs %s" % (obj.Categoria_id, id_categoria)
        if str(obj.Categoria_id) == str(id_categoria):
            pass
            # print "El alumno %s ha sido recategorizado" % codigo
        else:
            categorianueva = Categoria.objects.get(id=id_categoria)
            obj.Categoria_id = id_categoria
            obj.Observaciones = str(
                obj.Observaciones) + "\n18/08/2016: Cambio de  " + obj.Categoria.Categoria + " a " + categorianueva.Categoria + ", correo srta Lucila"
            obj.save()
            print "El alumno %s : %s ha cambiado de categoria, pasa de %s a %s" % (
                codigo, alumno.ApellidoPaterno, obj.Categoria.Categoria, categorianueva.Categoria)

    file1.close()


script_matricular()
