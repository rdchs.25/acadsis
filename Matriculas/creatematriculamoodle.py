#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Matriculas.models import MatriculaCiclo, MatriculaCursos


def crearusuarios():
    try:
        matriculas = MatriculaCiclo.objects.filter(Periodo_id=25, Estado='Matriculado').order_by('Alumno__AnioIngreso')
        for matricula in matriculas:
            matriculacursos = MatriculaCursos.objects.filter(MatriculaCiclo=matricula, Estado=True, Convalidado=False)
            for matriculacurso in matriculacursos:
                print matriculacurso.__unicode__()
                matriculacurso.save()
    except:
        return "error"


crearusuarios()
