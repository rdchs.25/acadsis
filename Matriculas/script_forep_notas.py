#!/usr/bin/python
# -*- coding: utf-8 -*-
# imports para traer variable de entorno django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

# imports para generar archivo en excel
from Matriculas.models import MatriculaCursos
from tempfile import TemporaryFile
from xlwt import Workbook


def forep_notas():
    book = Workbook()
    sheet1 = book.add_sheet('Hoja 1')

    # escribimos los encabezados
    sheet1.write(14, 0, 'Nro')
    sheet1.write(14, 1, 'Carrera')
    sheet1.write(14, 2, 'Codigo')
    sheet1.write(14, 3, 'Apellidos')
    sheet1.write(14, 4, 'Nombres')
    sheet1.write(14, 5, 'Ciclo')
    sheet1.write(14, 6, 'Cod Curso')
    sheet1.write(14, 7, 'Curso')
    sheet1.write(14, 8, 'Nota')

    i = 15
    matriculados = MatriculaCursos.objects.filter(PeriodoCurso__Periodo__id=9, MatriculaCiclo__Estado='Matriculado')
    print matriculados.count()

    # recorremos la consulta y escribimos los resultados
    for mat in matriculados:
        sheet1.write(i, 0, i - 14)
        sheet1.write(i, 1, mat.MatriculaCiclo.Alumno.Carrera.Carrera)
        sheet1.write(i, 2, mat.MatriculaCiclo.Alumno.Codigo)
        sheet1.write(i, 3, mat.MatriculaCiclo.Alumno.ApellidoPaterno + " " + mat.MatriculaCiclo.Alumno.ApellidoMaterno)
        sheet1.write(i, 4, mat.MatriculaCiclo.Alumno.Nombres)
        sheet1.write(i, 5, mat.PeriodoCurso.Curso.Ciclo)
        sheet1.write(i, 6, mat.PeriodoCurso.Curso.Codigo)
        sheet1.write(i, 7, mat.PeriodoCurso.Curso.Nombre)
        sheet1.write(i, 8, mat.Promedio())

        i += 1
    book.save('forep_notas.xls')
    book.save(TemporaryFile())


forep_notas()
