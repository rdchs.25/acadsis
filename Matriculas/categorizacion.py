# -*- coding: utf-8 -*-
import datetime
import os
import smtplib
import sys
from email import encoders

import xlwt
from django.core.wsgi import get_wsgi_application
from email.MIMEBase import MIMEBase
from email.MIMEImage import MIMEImage
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')

os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()
from django.utils.encoding import smart_str
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from decimal import Decimal
from UDL.settings import MEDIA_ROOT

# QUITA TILDES
rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
       'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
       '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' ', '-': ' '}


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


periodo_id = 23
anio_ingreso = "2018"
semestre_ingreso = "I"


def generar_lista(periodo_id, anio_ingreso, excluir=False):
    if excluir:
        seleccionar_matriculados = MatriculaCiclo.objects.filter(Estado="Matriculado", Periodo__id=periodo_id,
                                                                 Alumno__Egreso=False).exclude(
            Alumno__AnioIngreso=anio_ingreso, Alumno__Semestre=semestre_ingreso)
    else:
        seleccionar_matriculados = MatriculaCiclo.objects.filter(Estado="Matriculado", Periodo__id=periodo_id,
                                                                 Alumno__Egreso=False, Alumno__AnioIngreso=anio_ingreso,
                                                                 Alumno__Semestre=semestre_ingreso)

    lista = []
    linea = []

    for matriculado in seleccionar_matriculados:
        codigo = matriculado.Alumno.Codigo
        nombres_alumno = smart_str(matriculado.Alumno.ApellidoPaterno) + " " + smart_str(
            matriculado.Alumno.ApellidoMaterno) + " " + smart_str(matriculado.Alumno.Nombres)
        direccion = matriculado.Alumno.Direccion
        telefono = " / ".join([matriculado.Alumno.Telefono, matriculado.Alumno.Celular])
        email = matriculado.Alumno.Email
        carrera_alumno = smart_str(matriculado.Alumno.Carrera)
        departamento_alumno = smart_str(matriculado.Alumno.Provincia.Departamento.Departamento)
        ingreso = str(matriculado.Alumno.AnioIngreso) + "-" + str(matriculado.Alumno.Semestre)
        genero = str(matriculado.Alumno.Sexo)
        categoria = smart_str(matriculado.Categoria.Categoria)
        monto_pension = smart_str(matriculado.Categoria.pago())
        tipo_colegio = matriculado.Alumno.Colegio.Tipo
        nombre_colegio = matriculado.Alumno.Colegio.Colegio
        estado_asistencia = "HABILITADO"
        cursos_inhabilitados = ""
        # if pago_alumno.count() > 0:
        # monto_pension = pago_alumno[0].Monto()
        # else:
        # monto_pension = ""

        cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=matriculado.id)
        sum_cred = 0
        primero = True
        for cur in cursos_matriculado:
            porcentaje = cur.PorcentajeFaltas()
            if Decimal(porcentaje) >= Decimal("30.00"):
                estado_asistencia = "INHABILITADO"
                if primero is True:
                    cursos_inhabilitados = cursos_inhabilitados + cur.PeriodoCurso.Curso.Nombre + "(" + str(
                        porcentaje) + ")"
                else:
                    cursos_inhabilitados = cursos_inhabilitados + ", " + cur.PeriodoCurso.Curso.Nombre + "(" + str(
                        porcentaje) + ")"
                primero = False
            if cur.Convalidado is False and cur.Estado is True:
                if cur.PeriodoCurso.Curso.Equivalencia:
                    k_codigo = cur.PeriodoCurso.Curso.Codigo1
                    k_nombre = cur.PeriodoCurso.Curso.Equivalencia
                    k_creditos = str(cur.PeriodoCurso.Curso.Creditos_equiv)
                else:
                    k_codigo = cur.PeriodoCurso.Curso.Codigo
                    k_nombre = cur.PeriodoCurso.Curso.Nombre
                    k_creditos = str(cur.PeriodoCurso.Curso.Creditos)
                sum_cred = sum_cred + int(k_creditos)

        foto = ''
        if matriculado.Alumno.Foto != '':
            if os.path.exists(matriculado.Alumno.Foto.path):
                foto = 'SI-SI'
            else:
                foto = 'SI-NO'
        else:
            foto = 'No'

        tipodocumento = matriculado.Alumno.DocIdentidad
        nrodocumento = matriculado.Alumno.NroDocumento

        # Verificar si ha pagado matrícula
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="MATRICULA", Estado=True)
        if pagos.count() > 0:
            pago_matricula = "SI"
        else:
            pago_matricula = "NO"

        # Verificar pago pensiones
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="1ra", Estado=True)
        if pagos.count() > 0:
            pago_pension_1 = "SI"
            for pago in pagos:
                monto_pension_1 = smart_str(pago.Monto())
        else:
            pago_pension_1 = "NO"
            monto_pension_1 = "--"

        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="2da", Estado=True)
        if pagos.count() > 0:
            pago_pension_2 = "SI"
            for pago in pagos:
                monto_pension_2 = smart_str(pago.Monto())
        else:
            pago_pension_2 = "NO"
            monto_pension_2 = "---"

        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="3era", Estado=True)
        if pagos.count() > 0:
            pago_pension_3 = "SI"
        else:
            pago_pension_3 = "NO"
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="4ta", Estado=True)
        if pagos.count() > 0:
            pago_pension_4 = "SI"
        else:
            pago_pension_4 = "NO"
        pagos = matriculado.pagoalumno_set.filter(ConceptoPago__Descripcion__icontains="5ta", Estado=True)
        if pagos.count() > 0:
            pago_pension_5 = "SI"
        else:
            pago_pension_5 = "NO"

        # Verificar si tiene cursos registrados
        cursos = matriculado.matriculacursos_set.all()
        if cursos.count() == 0:
            registra = " "
        else:
            registra = cursos.count()
        #

        linea.extend(
            [codigo, smart_str(nombres_alumno), carrera_alumno, categoria, sum_cred, ingreso, departamento_alumno,
             genero, pago_matricula, registra, monto_pension, pago_pension_1, pago_pension_2, pago_pension_3,
             pago_pension_4, pago_pension_5, tipo_colegio, nombre_colegio, direccion, telefono, email,
             estado_asistencia, cursos_inhabilitados, foto, tipodocumento, nrodocumento])
        if linea not in lista:
            lista.append(linea)

        linea = []

    lista.sort(key=lambda x: (x[8], x[9]))

    # Establezco la cabecera
    # NO Pagaron Matrícula y tienen cursos registrados
    # SI Pagaron Matrícula y tienen cursos registrados
    # SI Pagaron Matrícula y NO tienen cursos registrados
    # No Pagaron Matrícula y NO tienen cursos registrados
    rp_1 = 0
    rp_2 = 0
    rp_3 = 0
    rp_4 = 0

    for linea in lista:
        if (linea[8] == "NO") and (type(linea[9]) is int):
            rp_1 += 1
        if (linea[8] == "SI") and (type(linea[9]) is int):
            rp_2 += 1
        if (linea[8] == "SI") and (type(linea[9]) is str):
            rp_3 += 1
        if (linea[8] == "NO") and (type(linea[9]) is str):
            rp_4 += 1

    extra = (rp_1, rp_2, rp_3, rp_4)
    return [lista, extra]


def generar_excel(datos_lista, excluir=False):
    datos = datos_lista[0]
    extra = datos_lista[1]

    ruta_archivo = str(MEDIA_ROOT)

    if excluir:
        nombre_archivo = "reporte_matriculados_antiguos_" + anio_ingreso + "_" + semestre_ingreso + ".xls"
    else:
        nombre_archivo = "reporte_matriculados_ingresantes_" + anio_ingreso + "_" + semestre_ingreso + ".xls"

    style = xlwt.XFStyle()
    styles = {'default': xlwt.easyxf('font: height 160;', num_format_str='@'),
              'headers': xlwt.easyxf('font: height 160, bold 1;', num_format_str='@'),
              'date': xlwt.easyxf('font: height 160;', num_format_str='dd-mm-yyyy'), }
    book = xlwt.Workbook(encoding='utf8')
    sheet = book.add_sheet('Hoja 1')

    # Primera parte
    j = 1
    encabezado = ["NO Pagaron Matrícula y tienen cursos registrados",
                  "SI Pagaron Matrícula y tienen cursos registrados",
                  "SI Pagaron Matrícula y NO tienen cursos registrados",
                  "No Pagaron Matrícula y NO tienen cursos registrados"]

    for rowx, row in enumerate(encabezado):
        cell_style = styles['headers']
        sheet.write(rowx + j, 0, row, style=cell_style)
        sheet.write(rowx + j, 3, extra[rowx], style=cell_style)

    i = 7
    flag1 = 0

    for rowx, row in enumerate(datos):
        if flag1 == 0:
            flag1 = 1
            cell_style = styles['headers']
            cell_stylef = styles['date']
            encabezado = ["CODIGO", "NOMBRES", "CARRERA", "CATEGORIA", "CREDITOS", "INGRESO", "DEPARTAMENTO", "GENERO",
                          "PAGO MATRICULA", "CURSOS REGISTRADOS", "MONTO PENSIÓN", "PAGO PENSIÓN 1", "PAGO PENSIÓN 2",
                          "PAGO PENSIÓN 3", "PAGO PENSIÓN 4", "PAGO PENSIÓN 5", "TIPO COLEGIO", "NOMBRE COLEGIO",
                          "DIRECCION", "TELEFONO", "EMAIL", "ESTADO ASISTENCIAS", "CURSOS INHABILITADOS", "FOTO",
                          "TIPO DOCUMENTO", "NRO DOCUMENTO"]
            for c, celda in enumerate(encabezado):
                sheet.write(rowx + i, c, celda, style=cell_style)
            i += 1

        cell_style = styles['default']
        for colx, value in enumerate(row):
            sheet.write(rowx + i, colx, value, style=cell_style)

    full_path = ruta_archivo + nombre_archivo
    book.save(full_path)


lista = generar_lista(periodo_id, anio_ingreso)
generar_excel(lista)
lista = generar_lista(periodo_id, anio_ingreso, excluir=True)
generar_excel(lista, excluir=True)


# funcion para enviar correos
def enviar_mail(emisor, passwd, receptor, asunto, html, archivos, path_archivos="/home/udl/acadsis/static/uploads/"):
    # Establecemos conexion con el servidor smtp de gmail
    mailServer = smtplib.SMTP('smtp.gmail.com', 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login(emisor, passwd)

    # Construimos un mensaje Multipart, con un texto y datos adjuntos
    mensaje = MIMEMultipart()
    mensaje['From'] = emisor
    mensaje['To'] = receptor
    mensaje['Subject'] = asunto

    # Adjuntamos el texto
    mensaje.attach(MIMEText(html, _charset="UTF-8"))

    # Adjuntamos la imagenes en caso se encuentre alguna
    for archivo in archivos:
        try:
            f = open(path_archivos + archivo, "rb")
            contenido = MIMEImage(file.read())
            contenido.add_header('Content-Disposition', 'attachment', filename=archivo)
            mensaje.attach(contenido)
        except ValueError:
            pass

    # Adjuntamos los archivos en caso se encuentre alguno
    for archivo in archivos:
        try:
            f = open(path_archivos + archivo, 'rb')
            # contenido = MIMEBase('multipart', 'encrypted')
            contenido = MIMEBase('application', 'vnd.ms-excel')
            contenido.set_payload(f.read())
            f.close()
            encoders.encode_base64(contenido)
            contenido.add_header('Content-Disposition', 'attachment', filename=archivo)
            mensaje.attach(contenido)
        except ValueError:
            pass

    # Enviamos el correo, con los campos emisor, receptor y mensaje
    mailServer.sendmail(emisor, receptor, mensaje.as_string())

    # Cierre de la conexion
    mailServer.close()


fecha = datetime.date.today() - datetime.timedelta(1)
asunto = 'Listado Actual de Matriculados Semestre %s-%s, fecha %s' % (
    anio_ingreso, semestre_ingreso, fecha.strftime("%d %b %Y"))
mensaje = 'Estimado coordinador :\n\nA continuación se le adjunta el reporte diario del numero de estudiantes ' \
          'matriculados en linea en el Semestre %s-%s.\n\nEl reporte de matriculados es generado automáticamente' \
          ' por el Sistema Académico de la UDL (ACADSIS).\n\nEste reporte contiene los matriculados en línea' \
          ' hasta la fecha:\n\nFecha: %s\nLa finalidad del reporte es apoyar a los coordinadores en el control del' \
          ' numero de matriculados en el semestre %s-%s.\n\nCualquier duda, consulta o sugerencia comunicarse' \
          ' con el personal de Central de Cómputo.\n\nAtentamente.\n\nCentral de Cómputo UDL' % (
              anio_ingreso, semestre_ingreso, fecha.strftime("%d %b %Y"), anio_ingreso, semestre_ingreso)

nombre_archivo_antiguo = "reporte_matriculados_antiguos_" + anio_ingreso + "_" + semestre_ingreso + ".xls"
nombre_archivo_nuevo = "reporte_matriculados_ingresantes_" + anio_ingreso + "_" + semestre_ingreso + ".xls"
enviar_mail('acadsis@udl.edu.pe', 'acadsisudl', 'rchaname@udl.edu.pe', asunto, mensaje,
            [nombre_archivo_nuevo, nombre_archivo_antiguo])
