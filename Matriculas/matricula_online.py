# -*- coding: utf-8 -*-
# Avoid shadowing the login() view below.
import datetime
import json
import urllib2
from decimal import Decimal

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect

from Alumnos.actualizacion_datos import FormActualizarDatos
from Alumnos.models import Alumno
from Categorias.models import Categoria
from Cursos.models import Curso, PeriodoCurso
from Matriculas.forms import LoginFormMatriculaOnline
from Matriculas.models import MatriculaCursos, MatriculaCiclo
from Pagos.models import PagoAlumno
from Periodos.models import Periodo

ID_PERIODO_MATRICULA = 25
CREDITOS_CARRERA = {'AT': 24, 'AM': 23, 'IA': 24, 'IC': 24, 'IS': 23}
CREDITOS_CARRERA_ESPECIAL = {'AT': 24, 'AM': 23, 'IA': 24, 'IC': 24, 'IS': 23}
MONTO_CREDITO = Decimal('17.50')
CREDITOS_MAXIMOS = 26


@csrf_protect
def logout_matricula_online(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('../../matricula_enlinea/')
    else:
        return HttpResponseRedirect('../../matricula_enlinea/')


@csrf_protect
@never_cache
def login_matricula_online(request):
    periodo = Periodo.objects.get(id=ID_PERIODO_MATRICULA)

    if request.method == 'POST':
        matriculaactiva = True
        if matriculaactiva is True:
            mensaje = "Matrícula 2018-II ha finalizado."
            links = ""
            return render_to_response("Matriculas/Online/mensaje.html",
                                      {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                       "user": request.user})

        form = LoginFormMatriculaOnline(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['Usuario']
            passwd = form.cleaned_data['Password']
            user = authenticate(username=usuario, password=passwd)
            if user is not None:
                auth_login(request, user)
                var = request.GET.get('q', 'm')
                try:
                    estudiante = Alumno.objects.get(user_ptr=request.user.id)
                except Alumno.DoesNotExist:
                    mensaje = "Usted no es un estudiante, verifique por favor."
                    links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                    return render_to_response("Matriculas/Online/mensaje.html",
                                              {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                               "user": request.user})
                try:
                    matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
                except MatriculaCiclo.DoesNotExist:
                    mensaje = "Usted aún no ha pagado su derecho de matrícula, debe regularizar sus pagos."
                    links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                    return render_to_response("Matriculas/Online/mensaje.html",
                                              {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                               "user": request.user})

                matriculado_cursos = MatriculaCursos.objects.filter(MatriculaCiclo=matriculaciclo).exclude(
                    Convalidado=True)
                if matriculado_cursos.count() != 0:
                    mensaje = "Usted ya se encuentra matriculado en cursos."
                    links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                    return render_to_response("Matriculas/Online/mensaje.html",
                                              {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                               "user": request.user})

                orcos = []

                # pasa saltar validacion de ingles por nueva curricula
                estudiante = matriculaciclo.Alumno
                ciclo_ingreso = estudiante.AnioIngreso + '-' + estudiante.Semestre

                # no es obligatorio que los alumnos del 1ero y 2do ciclo lleven inglés en el instituo de idiomas.
                # CAMBIAR ESTOS DATOS EN CADA MATRICULA!!!
                saltaringles = False
                if ciclo_ingreso == '2018-I' or ciclo_ingreso == '2018-II':
                    saltaringles = True

                # validamos que el alumno de segundo ciclo haya pagado el concepto de pago de libro para inglés básico
                isexonerado = matriculaciclo.Exoneradoidiomas
                if ciclo_ingreso == '2018-I' and isexonerado is False:
                    json_string = urllib2.urlopen(settings.URL_IDIOMAS + "/json3/").read()
                    pagaron_libro = json.loads(json_string)
                    pago_libro = False
                    for datoidioma in pagaron_libro:
                        if estudiante.Codigo == datoidioma["usuario"]:
                            pago_libro = True
                    if pago_libro is False:
                        mensaje = "Estimado alumno, recibe nuestro saludo y te informamos que debes acercarte a la " \
                                  "Oficina del Instituto de Idiomas ( 4º piso UDL) para adquirir tu material de la " \
                                  "asignatura de Inglés Básico y luego podrás registrarte en las asignaturas del " \
                                  "semestre 2018-II"
                        links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                        return render_to_response("Matriculas/Online/mensaje.html",
                                                  {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                                   "user": request.user})

                # proyectos formativos
                curso_formativo_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso__Proyecto_formativo=True,
                                                                          MatriculaCiclo__Alumno=estudiante,
                                                                          Aprobado=True)

                isactivoproyectoformativo = matriculaciclo.MatriculaProyectoFormativo
                if (ciclo_ingreso == '2017-I') and (curso_formativo_aprobado.count() == 0) and (
                        isactivoproyectoformativo is False):
                    mensaje = "Estimado alumno, recibe nuestro saludo y te informamos que debes acreditar haber " \
                              "llevado Proyectos Formativos. Para mas información acercarte a Oficina de " \
                              "Bienestar Universitario"
                    links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                    return render_to_response("Matriculas/Online/mensaje.html",
                                              {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                               "user": request.user})

                try:
                    # Verificar Idiomas
                    json_string = urllib2.urlopen(settings.URL_IDIOMAS + "/json2/%s/" % "2018").read()
                    datos_idiomas = json.loads(json_string)
                    egresadosidiomas = Alumno.objects.using('idiomas').filter(Egresoidiomas=True)
                    isexonerado = matriculaciclo.Exoneradoidiomas

                    usuarios_idiomas = []

                    # Buscamos en egresados de idiomas
                    for egresado in egresadosidiomas:
                        if estudiante.Codigo == egresado.Codigo:
                            usuarios_idiomas.append(egresado.Codigo)
                            break
                    # Buscamos en matriculados en idiomas
                    for datoidioma in datos_idiomas:
                        if estudiante.Codigo == datoidioma["usuario"]:
                            usuarios_idiomas.append(datoidioma["usuario"])
                            break

                    buscar_alumnos = MatriculaCiclo.objects.filter(Periodo=periodo, Alumno__Codigo__in=usuarios_idiomas)

                    pago_var = PagoAlumno.objects.filter(MatriculaCiclo__Periodo__id=ID_PERIODO_MATRICULA,
                                                         MatriculaCiclo__Alumno=estudiante,
                                                         ConceptoPago__Descripcion__icontains='MATRICULA', Estado=True)
                    recordatorio = True
                    if (buscar_alumnos.count() == 0 and isexonerado is False) and (pago_var.count() == 0):
                        mensaje = "Usted aún no se ha registrado en un grupo de Inglés. " \
                                  "Tampoco ha pagado su derecho de matrícula, debe regularizar sus pagos."
                        links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                        return render_to_response("Matriculas/Online/mensaje.html",
                                                  {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                                   "links": mark_safe(links), "user": request.user})
                    if (buscar_alumnos.count() == 0) and (saltaringles is False) and (isexonerado is False):
                        mensaje = "Usted aún no se ha registrado en un grupo de Inglés."
                        links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                        recordatorio = False
                        return render_to_response("Matriculas/Online/mensaje.html",
                                                  {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                                   "links": mark_safe(links), "user": request.user})
                    if pago_var.count() == 0:
                        mensaje = "Usted aún no ha pagado su derecho de matrícula, debe regularizar sus pagos."
                        links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                        return render_to_response("Matriculas/Online/mensaje.html",
                                                  {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                                   "links": mark_safe(links), "user": request.user})
                    if estudiante.Codigo in orcos:
                        mensaje = "Hemos detectado una irregularidad en su historial académico, debe ir donde su " \
                                  "coordinador de Carrera o Decano para inscribirse manualmente."
                        links = ""
                        recordatorio = False
                        return render_to_response("Matriculas/Online/mensaje.html",
                                                  {"mensaje": mensaje, "recordatorio": recordatorio, "periodo": periodo,
                                                   "links": mark_safe(links), "user": request.user})

                    pago = pago_var[0]

                    creditos_carrera = CREDITOS_CARRERA
                    creditos_carrera_especial = CREDITOS_CARRERA_ESPECIAL
                    creditos_maximos = 26

                    plan_estudios = Curso.objects.filter(Periodo__id=ID_PERIODO_MATRICULA, Carrera=estudiante.Carrera,
                                                         Extracurricular=False, Proyecto_formativo=False).order_by(
                        'Ciclo', 'Nombre')
                    cursos_disponibles = []
                    creditos_llevados = 0
                    categoria = pago.MatriculaCiclo.Categoria
                    creditos_regulares = creditos_carrera['%s' % estudiante.Carrera.Codigo]
                    creditos_regulares_especial = creditos_carrera_especial['%s' % estudiante.Carrera.Codigo]
                    costo_cred = Decimal('17.50')
                    for cur in plan_estudios:
                        n_equi = cur.Equivalencias.count()
                        i = 1
                        if n_equi != 0:
                            for equi in cur.Equivalencias.all():
                                curso_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=equi,
                                                                                MatriculaCiclo__Alumno=estudiante,
                                                                                Aprobado=True)
                                if curso_aprobado.count() == 0:
                                    if i == n_equi:
                                        n_pre = cur.Prerequisitos.count()
                                        j = 1
                                        if n_pre != 0:
                                            for pre in cur.Prerequisitos.all():
                                                curso_pre_aprobado = MatriculaCursos.objects.filter(
                                                    PeriodoCurso__Curso=pre, MatriculaCiclo__Alumno=estudiante,
                                                    Aprobado=True)
                                                if curso_pre_aprobado.count() != 0:
                                                    creditos = creditos_llevados + cur.Creditos
                                                    if creditos >= creditos_maximos:
                                                        pass
                                                    else:
                                                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                                        if periodocurso.count() != 0:
                                                            cursos_disponibles.append(periodocurso[0])
                                                            creditos_llevados += cur.Creditos
                                                    break
                                        else:
                                            creditos = creditos_llevados + cur.Creditos
                                            if creditos >= creditos_maximos:
                                                pass
                                            else:
                                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                                if periodocurso.count() != 0:
                                                    cursos_disponibles.append(periodocurso[0])
                                                    creditos_llevados += cur.Creditos
                                    else:
                                        i += 1
                                else:
                                    break
                        else:
                            creditos = creditos_llevados + cur.Creditos
                            if creditos >= creditos_maximos:
                                pass
                            else:
                                # if cur.Prerequisito == None:
                                # periodocurso = PeriodoCurso.objects.filter(Curso = cur)
                                # if periodocurso.count() != 0:
                                # cursos_disponibles.append(periodocurso[0])
                                # creditos_llevados += cur.Creditos
                                # else:
                                # curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso =
                                # cur.Prerequisito, MatriculaCiclo__Alumno = estudiante, Aprobado = True)
                                # if curso_pre_aprobado.count() != 0:
                                # periodocurso = PeriodoCurso.objects.filter(Curso = cur)
                                # if periodocurso.count() != 0:
                                # cursos_disponibles.append(periodocurso[0])
                                # creditos_llevados += cur.Creditos

                                n_pre = cur.Prerequisitos.count()
                                j = 1
                                if n_pre != 0:
                                    for pre in cur.Prerequisitos.all():
                                        curso_pre_aprobado = MatriculaCursos.objects.filter(PeriodoCurso__Curso=pre,
                                                                                            MatriculaCiclo__Alumno=
                                                                                            estudiante,
                                                                                            Aprobado=True)
                                        if curso_pre_aprobado.count() != 0:
                                            creditos = creditos_llevados + cur.Creditos
                                            if creditos >= creditos_maximos:
                                                pass
                                            else:
                                                periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                                if periodocurso.count() != 0:
                                                    cursos_disponibles.append(periodocurso[0])
                                                    creditos_llevados += cur.Creditos
                                            break
                                else:
                                    creditos = creditos_llevados + cur.Creditos
                                    if creditos >= creditos_maximos:
                                        pass
                                    else:
                                        periodocurso = PeriodoCurso.objects.filter(Curso=cur)
                                        if periodocurso.count() != 0:
                                            cursos_disponibles.append(periodocurso[0])
                                            creditos_llevados += cur.Creditos

                    cursos = cursos_disponibles
                    cursos_ciclo = {}
                    for i in range(1, 8):
                        contador = 0
                        for cur in cursos:
                            if int(cur.Curso.Ciclo) == int(i):
                                contador += 1
                        if contador != 0:
                            cursos_ciclo[i] = contador
                    form = FormActualizarDatos()
                    return render_to_response("Matriculas/Online/profile_matricula_online.html",
                                              {"results": cursos, "cursos_ciclo": cursos_ciclo,
                                               "estudiante": estudiante, "creditos": creditos_llevados,
                                               "categoria": categoria, "creditos_regulares": creditos_regulares,
                                               "creditos_regulares_especial": creditos_regulares_especial,
                                               "costo_cred": costo_cred, "periodo": periodo, "var": var, "form": form,
                                               "user": request.user}, context_instance=RequestContext(request))
                except PagoAlumno.DoesNotExist:
                    mensaje = "Usted aún no se ha registrado en un grupo de Inglés."
                    # mensaje = "Usted aún no ha pagado su derecho de matrícula, debe regularizar sus pagos."
                    links = "<a class='btn btn-primary btn-large' href=''>Intente nuevamente</a>"
                    return render_to_response("Matriculas/Online/mensaje.html",
                                              {"mensaje": mensaje, "periodo": periodo, "links": mark_safe(links),
                                               "user": request.user})
    else:
        form = LoginFormMatriculaOnline()

    return render_to_response("Matriculas/Online/login.html", {"form": form, "periodo": periodo},
                              context_instance=RequestContext(request))


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


@csrf_exempt
def registrar_matricula_online(request):
    if request.user.is_authenticated():
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO_MATRICULA)
        except Periodo.DoesNotExist:
            mensaje = "Error, No existe el semestre, verifique por favor."
            return HttpResponse(mensaje)

        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            mensaje = "Error, Usted no es un estudiante, verifique por favor."
            return HttpResponse(mensaje)

        try:
            matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "Error, No ha pagado su matricula, verifique por favor."
            return HttpResponse(mensaje)

        '''Inicio actualizacion de datos David'''
        rspdatos_alumnos = request.POST['vdatosalumno'].split('/')
        if len(rspdatos_alumnos) != 0:
            estudiante.ApellidoPaterno = rspdatos_alumnos[0]
            estudiante.Nombres = rspdatos_alumnos[1]
            estudiante.LugarNacimiento = rspdatos_alumnos[2]
            estudiante.Direccion = rspdatos_alumnos[3]
            estudiante.Celular = rspdatos_alumnos[4]
            estudiante.ApellidoMaterno = rspdatos_alumnos[5]
            estudiante.Nacimiento = rspdatos_alumnos[6]
            estudiante.NroDocumento = rspdatos_alumnos[7]
            estudiante.Telefono = rspdatos_alumnos[8]
            estudiante.Email = rspdatos_alumnos[9]

            estudiante.save()

        '''Fin actualizacion de datos David'''

        vcursos = request.POST['vcursos']
        rsp = request.POST['vcursos'].split('-')
        pps = {}
        creditos_matriculados = 0
        total_electivos = 0

        ciclo = ''
        cursos_mismo_ciclo = True
        contador = 1

        for r in rsp:
            if r != '':
                try:
                    periodocurso = PeriodoCurso.objects.get(id=r)
                    if contador == 1:
                        ciclo = periodocurso.Curso.Ciclo
                    else:
                        if ciclo != str(periodocurso.Curso.Ciclo):
                            cursos_mismo_ciclo = False
                    contador = contador + 1
                except PeriodoCurso.DoesNotExist:
                    mensaje = "Se encontraron errores al procesar la matricula."
                    return HttpResponse(mensaje)
                # Revisar Electivos
                electivo = periodocurso.Curso.Codigo.split("-")
                if len(electivo) > 1:
                    total_electivos += 1
                creditos_matriculados += int(periodocurso.Curso.Creditos)

                # if total_electivos > 1:
                # mensaje = "No puedes llevar más de un curso electivo, por favor escoge uno solo"
                # return HttpResponse(mensaje)

        if creditos_matriculados > CREDITOS_MAXIMOS:
            mensaje = "ERROR, No puedes llevar mas de %s creditos." % CREDITOS_MAXIMOS
            return HttpResponse(mensaje)

        log_matricula = '%s,%s %s,%s,%s,%s,%s,%s,' % (
            estudiante.Codigo, estudiante.ApellidoPaterno, estudiante.ApellidoMaterno, estudiante.Nombres,
            estudiante.Carrera.Codigo, matriculaciclo.Categoria.Categoria, matriculaciclo.Categoria.Pago,
            creditos_matriculados)
        log_cursos = []
        hoy = datetime.datetime.now()
        fecha_date = hoy.strftime('%Y-%m-%d')
        fecha_hora = hoy.strftime('%H:%M')

        creditos_matriculados = 0
        for r in rsp:
            if r != '':
                try:
                    periodocurso = PeriodoCurso.objects.get(id=r)
                except PeriodoCurso.DoesNotExist:
                    mensaje = "Se encontraron errores al procesar la matricula."
                    return HttpResponse(mensaje)

                nueva_matricula = MatriculaCursos(MatriculaCiclo=matriculaciclo, PeriodoCurso=periodocurso)
                nueva_matricula.save()
                # nueva_matricula_campus = MatriculaCursos(MatriculaCiclo=matriculaciclo, PeriodoCurso=periodocurso)
                # nueva_matricula_campus.save(using="campus")

                log_cursos.append('%s,%s,%s,%s,%s,%s,%s\n' % (
                    periodocurso.Grupo, periodocurso.Curso.Codigo, periodocurso.Curso.Nombre, periodocurso.Curso.Ciclo,
                    periodocurso.Curso.Creditos, fecha_date, fecha_hora))

                creditos_matriculados += int(periodocurso.Curso.Creditos)

        creditos_carrera = CREDITOS_CARRERA
        creditos_carrera_especial = CREDITOS_CARRERA_ESPECIAL

        creditos_regulares_especial = int(creditos_carrera_especial['%s' % estudiante.Carrera.Codigo])
        creditos_regulares = int(creditos_carrera['%s' % estudiante.Carrera.Codigo])

        if cursos_mismo_ciclo is True and ciclo == '09':
            creditos_regulares = creditos_regulares_especial

        creditos_adicionales = creditos_matriculados - creditos_regulares
        pension_regular = matriculaciclo.Categoria.Pago
        pension_adicional = MONTO_CREDITO * creditos_adicionales
        pension_total = pension_regular + pension_adicional

        if matriculaciclo.Categoria.Categoria.find('CATEGORIA A') != -1:
            actual_cat = 'CATEGORIA A'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA B') != -1:
            actual_cat = 'CATEGORIA B'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA C') != -1:
            actual_cat = 'CATEGORIA C'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA D') != -1:
            actual_cat = 'CATEGORIA D'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA E') != -1:
            actual_cat = 'CATEGORIA E'
        elif matriculaciclo.Categoria.Categoria.find('CATEGORIA F') != -1:
            actual_cat = 'CATEGORIA F'
        else:
            actual_cat = 'CATEGORIA A'

        try:
            categoria = Categoria.objects.get(Periodo=periodo, Categoria__icontains=actual_cat, Pago=pension_total)
            matriculaciclo.Categoria = categoria
        except Categoria.DoesNotExist:
            categoria = ''
            pass
        matriculaciclo.FechaMatricula = datetime.date.today()
        matriculaciclo.save()

        from django.utils.encoding import smart_str
        listado = []
        rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'Ñ': 'N', 'Á': 'A',
               'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ',
               '!': ' ', '¿': ' ', '?': ' ', '^': ' ', '"': ' '}
        if categoria != '':
            log_matricula += '%s,%s,%s,%s,%s,' % (
                creditos_regulares, creditos_adicionales, pension_adicional, categoria.Categoria, categoria.Pago)
        else:
            log_matricula += '%s,%s,%s,%s,%s,' % (creditos_regulares, creditos_adicionales, pension_adicional, '', '')
        f = open(settings.MEDIA_ROOT + 'matricula_online_log.csv', 'a+')
        for log in log_cursos:
            linea = replace_all(smart_str(log_matricula + log), rep)
            f.write(unicode(linea, "utf-8"))
        f.close()

        mensaje = "Cursos registrados correctamente"
        return HttpResponse(mensaje)
    else:
        return HttpResponseRedirect('../')


@csrf_exempt
def enviar_ficha_email(request):
    if request.user.is_authenticated():
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO_MATRICULA)
        except Periodo.DoesNotExist:
            mensaje = "Error, No existe el semestre, verifique por favor."
            return HttpResponse(mensaje)

        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            mensaje = "Error, Usted no es un estudiante, verifique por favor."
            return HttpResponse(mensaje)

        try:
            matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "Error, No ha pagado su matricula, verifique por favor."
            return HttpResponse(mensaje)

        email = request.POST['email']
        from funciones import enviar_mail
        asunto = "Ficha de Matricula UDL semestre %s" % periodo
        mensaje = "Estimado(a): %s\n\nEl presente correo es para adjuntarte la ficha de matricula de cursos en el " \
                  "semestre %s.\n\nUniversidad de Lambayeque\nSabiduria " \
                  "para llegar lejos.\nCalle Tacna 065 - Chiclayo\nTelf : (51)(074)-208836\nwww.edu.pe" % (
                      estudiante, periodo)

        # lineas para generar la ficha
        from Notas.formatos import ficha_matricula, ficha_matricula2
        from reportlab.pdfgen import canvas
        from reportlab.lib.units import cm

        now = datetime.datetime.now().strftime("%d_%m_%y_%H_%M_%S")
        ruta_archivo = settings.MEDIA_ROOT + 'archivos_pdf/'
        nombre_archivo = u'ficha_matricula_%s.pdf' % now
        p = canvas.Canvas(ruta_archivo + nombre_archivo, pagesize=(21 * cm, 29.7 * cm))

        try:
            # dibujar ficha inferior
            p = ficha_matricula(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = matriculaciclo.Alumno.Codigo
            alumno = str(matriculaciclo.Alumno)
            carrera = str(matriculaciclo.Alumno.Carrera)
            periodo = str(matriculaciclo.Periodo)
            p.drawString(500, 335, codigo)
            p.drawString(500, 315, periodo)
            p.drawString(100, 315, carrera)
            p.drawString(100, 335, alumno)
            i = 265
            creditos = 0
            for cur in matriculaciclo.matriculacursos_set.all():
                if cur.Convalidado is False and cur.Estado is True:
                    # codigo del curso
                    p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(181, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos y total de creditos
                    p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    i = i - 20
            p.drawString(529, 95, str(creditos))

            # dibujar ficha superior
            w = 420
            p = ficha_matricula2(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = matriculaciclo.Alumno.Codigo
            alumno = str(matriculaciclo.Alumno)
            carrera = str(matriculaciclo.Alumno.Carrera)
            periodo = str(matriculaciclo.Periodo)
            p.drawString(500, 335 + w, codigo)
            p.drawString(500, 315 + w, periodo)
            p.drawString(100, 315 + w, carrera)
            p.drawString(100, 335 + w, alumno)
            i = 265 + w
            creditos = 0
            for cur in matriculaciclo.matriculacursos_set.all():
                if cur.Convalidado is False and cur.Estado is True:
                    # codigo del curso
                    p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(181, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos y total de creditos
                    p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    # i = i - 18
                    i = i - 20
            p.drawString(529, 95 + w, str(creditos))
            p.showPage()
            p.save()
        except:
            p.showPage()
            p.save()
        # fin generar ficha-------------

        envio_correo = enviar_mail('matriculaonline@udl.edu.pe', 'udlmatriculaonline', [email], asunto, mensaje, [nombre_archivo],
                                   ruta_archivo)
        # envio_correo = True
        if envio_correo is True:
            mensaje = "Ficha enviada correctamente"
            estudiante.Email = email
            estudiante.save()
        else:
            mensaje = "Error enviando ficha"
        return HttpResponse(mensaje)
    else:
        return HttpResponseRedirect('../')


def descargar_ficha_online(request):
    if request.user.is_authenticated():
        try:
            periodo = Periodo.objects.get(id=ID_PERIODO_MATRICULA)
        except Periodo.DoesNotExist:
            mensaje = "Error, No existe el semestre, verifique por favor."
            return HttpResponse(mensaje)

        try:
            estudiante = Alumno.objects.get(user_ptr=request.user.id)
        except Alumno.DoesNotExist:
            mensaje = "Error, Usted no es un estudiante, verifique por favor."
            return HttpResponse(mensaje)

        try:
            matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo, Alumno=estudiante)
        except MatriculaCiclo.DoesNotExist:
            mensaje = "Error, No ha pagado su matricula, verifique por favor."
            return HttpResponse(mensaje)

        # lineas para generar la ficha
        from Notas.formatos import ficha_matricula, ficha_matricula2
        from reportlab.pdfgen import canvas
        from reportlab.lib.units import cm

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=ficha_matricula.pdf'
        p = canvas.Canvas(response, pagesize=(21 * cm, 29.7 * cm))

        try:
            # dibujar ficha inferior
            p = ficha_matricula(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = matriculaciclo.Alumno.Codigo
            alumno = str(matriculaciclo.Alumno)
            carrera = str(matriculaciclo.Alumno.Carrera)
            periodo = str(matriculaciclo.Periodo)
            p.drawString(500, 335, codigo)
            p.drawString(500, 315, periodo)
            p.drawString(100, 315, carrera)
            p.drawString(100, 335, alumno)
            i = 265
            creditos = 0
            for cur in matriculaciclo.matriculacursos_set.all():
                if cur.Convalidado is False and cur.Estado is True:
                    # codigo del curso
                    p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(181, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos y total de creditos
                    p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    i = i - 20
            p.drawString(529, 95, str(creditos))

            # dibujar ficha superior
            w = 420
            p = ficha_matricula2(p)
            p.setFont("Helvetica-Bold", 10)
            codigo = matriculaciclo.Alumno.Codigo
            alumno = str(matriculaciclo.Alumno)
            carrera = str(matriculaciclo.Alumno.Carrera)
            periodo = str(matriculaciclo.Periodo)
            p.drawString(500, 335 + w, codigo)
            p.drawString(500, 315 + w, periodo)
            p.drawString(100, 315 + w, carrera)
            p.drawString(100, 335 + w, alumno)
            i = 265 + w
            creditos = 0
            for cur in matriculaciclo.matriculacursos_set.all():
                if cur.Convalidado is False and cur.Estado is True:
                    # codigo del curso
                    p.drawString(80, i, cur.PeriodoCurso.Curso.Codigo)
                    # grupo horario del curso
                    p.drawString(181, i, cur.PeriodoCurso.Grupo)
                    # nombre del curso, creditos y total de creditos
                    p.drawString(234, i, cur.PeriodoCurso.Curso.Nombre)
                    p.drawString(529, i, str(cur.PeriodoCurso.Curso.Creditos))
                    creditos += cur.PeriodoCurso.Curso.Creditos
                    # i = i - 18
                    i = i - 20
            p.drawString(529, 95 + w, str(creditos))
            p.showPage()
            p.save()
        except:
            p.showPage()
            p.save()
        # fin generar ficha-------------

        return response
    else:
        return HttpResponseRedirect('../')
