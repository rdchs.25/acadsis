#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'

# imports para traer variable de entorno django
from Matriculas.models import MatriculaCiclo, MatriculaCursos

matriculados = MatriculaCiclo.objects.filter(
    Periodo__id=16, Estado='Matriculado')
i = 0
CREDITOS_CARRERA = {'AT': 21, 'AM': 22, 'IA': 22, 'IC': 21, 'IS': 20}
for matriculado in matriculados:
    matriculascurso = MatriculaCursos.objects.filter(
        MatriculaCiclo=matriculado)

    if matriculascurso.count() == 0:
        continue

    estudiante = matriculado.Alumno
    creditos_regulares = CREDITOS_CARRERA['%s' % estudiante.Carrera.Codigo]

    sumacreditos = 0
    ciclo_curso = None
    varios_ciclos = False
    for x in matriculascurso:
        periodocurso = x.PeriodoCurso
        if ciclo_curso is None:
            ciclo_curso = periodocurso.Curso.Ciclo
        else:
            if ciclo_curso != periodocurso.Curso.Ciclo:
                varios_ciclos = True
        sumacreditos = sumacreditos + periodocurso.Curso.Creditos
    if varios_ciclos and (sumacreditos > creditos_regulares):
        diferencia = sumacreditos - creditos_regulares
        print str(estudiante.Codigo) + ' excede: ' + str(diferencia) + ' categoria: ' + str(
            matriculado.Categoria.Categoria)
