# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Alumnos.models import Alumno
from Categorias.forms import AsignarCategoriaGrupoAlumnoForm, IndexAsignarCategoriaAlumnosForm, IndexCategoriaForm
from Matriculas.models import MatriculaCiclo
from Periodos.models import Periodo


@csrf_protect
def index_categorias(request):
    if request.user.is_authenticated() and request.user.has_perm(
            'Categorias.change_categoria') or request.user.has_perm('Categorias.add_categoria'):
        if request.method == 'POST':
            form = IndexCategoriaForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?Periodo__id__exact=" + str(periodo.id))
        else:
            form = IndexCategoriaForm()
        return render_to_response('Categorias/index.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def index_asignarcategoria_grupoalumnos(request):
    if request.user.is_authenticated() and request.user.has_perm(
            'Categorias.change_categoria') or request.user.has_perm('Categorias.add_categoria'):
        if request.method == 'POST':
            form = IndexAsignarCategoriaAlumnosForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../" + str(periodo.id))

        else:
            form = IndexAsignarCategoriaAlumnosForm()
        return render_to_response('Categorias/index.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def asignarcategoria_grupoalumnos(request, periodo_id):
    if request.user.is_authenticated() and request.user.has_perm('Categorias.add_categoria'):
        try:
            periodo = Periodo.objects.get(id=periodo_id)
        except Periodo.DoesNotExist:
            print ("Periodo no existe")

        if request.method == 'POST':
            form = AsignarCategoriaGrupoAlumnoForm(request.POST, request.FILES)
            if form.is_valid():
                alumnosnoencontrados = ""

                periodo_origen = form.cleaned_data['PeriodoOrigen']
                periodo_destino = form.cleaned_data['PeriodoDestino']
                actualizar = form.cleaned_data['Actualizar']
                archivo = form.cleaned_data['Archivo']
                categoria = form.cleaned_data['Categoria']
                if periodo_origen:
                    contador = 0
                    matriculaciclo_alumno = MatriculaCiclo.objects.filter(Periodo=periodo_origen.id,
                                                                          Estado='Matriculado')

                    for objmatriculaciclo in matriculaciclo_alumno:
                        alumno_id = objmatriculaciclo.Alumno_id
                        categoria_id = categoria.id

                        try:
                            MatriculaCiclo.objects.get(Periodo=periodo_destino, Alumno_id=alumno_id)
                            continue
                        except MatriculaCiclo.DoesNotExist:
                            nueva_matriculaciclo = MatriculaCiclo(Periodo_id=periodo_destino, Alumno_id=alumno_id,
                                                                  Categoria_id=categoria_id)
                            nueva_matriculaciclo.save()
                        contador += 1
                    print ("Registros Ingresados %s" % contador)
                else:
                    contador = 0
                    for obj in archivo:
                        line = obj.strip('\n')
                        linea = line.split(';')
                        codigo_alumno = linea[0].strip()
                        categoria_id = linea[1].strip()
                        if actualizar:
                            try:
                                matriculaciclo = MatriculaCiclo.objects.get(Periodo=periodo_destino,
                                                                            Alumno__Codigo=codigo_alumno)
                                matriculaciclo.Categoria_id = categoria_id
                                matriculaciclo.save()
                            except MatriculaCiclo.DoesNotExist:
                                try:
                                    alumno_id = Alumno.objects.get(Codigo=codigo_alumno).id
                                except Alumno.DoesNotExist:
                                    alumno_id = 0
                                    alumnosnoencontrados += "%s|" % (
                                        str(codigo_alumno))
                                    continue
                                if alumno_id != 0:
                                    nueva_matriculaciclo = MatriculaCiclo(Periodo_id=periodo_destino,
                                                                          Alumno_id=alumno_id,
                                                                          Categoria_id=categoria_id)
                                    nueva_matriculaciclo.save()
                                    contador += 1
                        else:
                            try:
                                MatriculaCiclo.objects.get(Periodo=periodo_destino, Alumno__Codigo=codigo_alumno)
                                continue
                            except MatriculaCiclo.DoesNotExist:
                                try:
                                    alumno_id = Alumno.objects.get(Codigo=codigo_alumno).id
                                except Alumno.DoesNotExist:
                                    alumno_id = 0
                                    alumnosnoencontrados += "%s|" % (
                                        str(codigo_alumno))
                                    continue
                                if alumno_id != 0:
                                    nueva_matriculaciclo = MatriculaCiclo(Periodo_id=periodo_destino,
                                                                          Alumno_id=alumno_id,
                                                                          Categoria_id=categoria_id)
                                    nueva_matriculaciclo.save()
                            contador += 1
                mensaje = "Reporte Asignación de Categoría --> Alumnos"
                estadistica = "Código(s):  " + alumnosnoencontrados
                links = "<a href=''>Volver</a>"
                return render_to_response("Categorias/mensaje.html",
                                          {"mensaje": mensaje, "estadistica": estadistica, "links": mark_safe(links),
                                           "user": request.user})

        else:
            form = AsignarCategoriaGrupoAlumnoForm(initial={'PeriodoDestino': periodo_id})
            form.fields['PeriodoOrigen'].queryset = Periodo.objects.exclude(pk=periodo_id)

        return render_to_response("Categorias/Grupal/form_asignarcategoria_grupoalumnos.html",
                                  {"form": form, "periodo": periodo, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        print ("No hay permisos")
