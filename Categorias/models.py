# -*- coding: utf-8 -*-
from decimal import Decimal

from django.db import models

from Periodos.models import Periodo

MONEDA_CHOICES = (
    ('S/.', 'Nuevo Sol'),
    ('US$', 'Dólar'),
    ('€', 'Euro'),
)


class Categoria(models.Model):
    CONDICION_CHOICES = (
        ('Periodo', 'Periodo'),
        ('Archivo', 'Archivo'),
    )
    Periodo = models.ForeignKey(Periodo)
    Categoria = models.CharField("Categoría", max_length=50)
    Moneda = models.CharField("Moneda", max_length=4, choices=MONEDA_CHOICES)
    Pago = models.DecimalField(max_digits=6, decimal_places=2, default='0.00',
                               help_text="Ingrese el monto de pago de la pensión")
    Estado = models.BooleanField("Activada", default=True)
    Observaciones = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u'%s - %s %s - %s' % (self.Categoria, self.Moneda, str(self.Pago), self.Periodo)

    def categoria(self):
        return u'%s' % self.Categoria

    def pago(self):
        return Decimal(self.Pago)

    class Meta:
        verbose_name = "Categoría de Pago"
        verbose_name_plural = "Categorías de Pago"
