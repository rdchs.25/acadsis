# -*- coding: utf-8 -*-
from django.conf.urls import url

from Categorias.views import index_categorias, index_asignarcategoria_grupoalumnos, asignarcategoria_grupoalumnos
from Matriculas.views import obtener_categorias

app_name = 'Categorias'

urlpatterns = [
    url(r'^Categorias/categoria/elegirperiodo/$', index_categorias),
    url(r'^Categorias/asignarcategoriaalumnos/elegirperiodo/$', index_asignarcategoria_grupoalumnos),
    url(r'^Categorias/asignarcategoriaalumnos/(\d+)/$', asignarcategoria_grupoalumnos),
    url(r'^Categorias/asignarcategoriaalumnos/obtener_categorias/(?P<periodo_id>\d*)/$', obtener_categorias,
        name='obtener_categorias'),
]
