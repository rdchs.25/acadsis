# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Categoria', models.CharField(max_length=50, verbose_name=b'Categor\xc3\xada')),
                ('Moneda', models.CharField(max_length=4, verbose_name=b'Moneda', choices=[(b'S/.', b'Nuevo Sol'), (b'US$', b'D\xc3\xb3lar'), (b'\xe2\x82\xac', b'Euro')])),
                ('Pago', models.DecimalField(default=b'0.00', help_text=b'Ingrese el monto de pago de la pensi\xc3\xb3n', max_digits=6, decimal_places=2)),
                ('Estado', models.BooleanField(default=True, verbose_name=b'Activada')),
                ('Observaciones', models.TextField(null=True, blank=True)),
                ('Periodo', models.ForeignKey(to='Periodos.Periodo')),
            ],
            options={
                'verbose_name': 'Categor\xeda de Pago',
                'verbose_name_plural': 'Categor\xedas de Pago',
            },
        ),
    ]
