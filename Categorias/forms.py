# -*- coding: utf-8 -*-
from django import forms
from django.forms.utils import ErrorList

from Categorias.models import Categoria
from Periodos.models import Periodo


class IndexCategoriaForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class IndexAsignarCategoriaAlumnosForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.filter(Activo=True).order_by('-id'),
                                     widget=forms.Select(),
                                     required=True, label='Período')


class AsignarCategoriaGrupoAlumnoForm(forms.Form):
    PeriodoDestino = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    Condicion = forms.ChoiceField(choices=Categoria.CONDICION_CHOICES)
    PeriodoOrigen = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(),
                                           required=False, label='Periodo Origen',
                                           empty_label="Seleccione un Periodo")
    Categoria = forms.ModelChoiceField(queryset=Categoria.objects.all().order_by('Categoria'), widget=forms.Select(),
                                       required=False, empty_label="Seleccione una categoría")
    Archivo = forms.FileField(help_text='Cargar archivo en formato csv, con los codigos de los estudiantes',
                              required=False)
    Actualizar = forms.BooleanField(required=False, initial=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        periodo_origen = cleaned_data.get("PeriodoOrigen")
        periodo_destino = cleaned_data.get("PeriodoDestino")
        archivo = cleaned_data.get("Archivo")

        if periodo_origen and archivo and periodo_destino:

            for line in archivo:
                line = line.strip('\n')
                try:
                    alumno = Periodo.objects.get(Periodo=periodo_destino)
                except Periodo.DoesNotExist:
                    msg = "El periodo %s, no pertenece al semestre elegido, verificar por favor" % line
                    self._errors["Archivo"] = ErrorList([msg])
                    del cleaned_data["Archivo"]
        return cleaned_data

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Categoria/frm_asignarcategoria_grupoalumnos.js"
              )
