# -*- coding: utf-8 -*-
from django.contrib import admin

from Categorias.models import Categoria


class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('Categoria', 'pago', 'Periodo', 'Observaciones')
    list_filter = ('Periodo',)


admin.site.register(Categoria, CategoriaAdmin)
