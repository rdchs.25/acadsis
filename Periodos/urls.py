# -*- coding: utf-8 -*-
from django.conf.urls import url

from Periodos.views import listado_periodo_curso, index_configurar_curso, index_periodo_curso, \
    agregar_grupo_periodocurso, editar_grupo_periodocurso

app_name = 'Periodos'

urlpatterns = [
    url(r'^Cursos/periodocurso/(\d+)/$', listado_periodo_curso),
    url(r'^Cursos/periodocurso/add/$', index_configurar_curso),
    url(r'^Cursos/periodocurso/configurar/$', index_configurar_curso),
    url(r'^Cursos/periodocurso/(\d+)/(\d+)/$', index_periodo_curso),
    url(r'^Cursos/periodocurso/agregar/(\d+)/(\d+)/$', agregar_grupo_periodocurso),
    url(r'^Cursos/periodocurso/editar/(\d+)/$', editar_grupo_periodocurso),
]
