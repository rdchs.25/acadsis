# -*- coding: utf-8 -*-
from django.contrib import admin
# imports para eliminar periodocurso
from django.contrib.admin.utils import unquote, get_deleted_objects
from django.core.exceptions import PermissionDenied
from django.db import transaction, router
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_unicode
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_protect

from Cursos.models import PeriodoCurso
from Periodos.models import Periodo

csrf_protect_m = method_decorator(csrf_protect)

# imports para xlwt
import datetime
from django.db.models.query import QuerySet, ValuesQuerySet

# imports reportlab
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from decimal import Decimal

# importar formatos reportlab creados
from Notas.formatos import acta_notas


class PeriodoAdmin(admin.ModelAdmin):
    list_display = ('Anio', 'Semestre', 'Inicio', 'Fin', 'Activo')
    list_filter = ('Activo',)


admin.site.register(Periodo, PeriodoAdmin)


class PeriodoCursoAdmin(admin.ModelAdmin):
    list_display = ('Curso', 'Grupo', 'Docente', 'CarreraCurso')
    search_fields = ('Curso__Nombre', 'Docente__ApellidoPaterno', 'Docente__ApellidoMaterno', 'Docente__Nombres',
                     'Curso__Carrera__Carrera')
    list_filter = ('Periodo',)
    actions = ['actas_periodocurso', 'matriculados_grupo_excel']

    def actas_periodocurso(self, request, queryset):
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=actas.pdf'
        p = canvas.Canvas(response, pagesize=A4)
        for grupo in queryset:
            m = 0
            o = grupo.matriculacursos_set.all().count()
            j = 1
            for m in range(0, o, 21):
                matriculados = grupo.matriculacursos_set.order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                                  'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                                  'MatriculaCiclo__Alumno__Nombres')[0 + m:21 + m]
                p = acta_notas(p)
                p.setFont("Helvetica-Bold", 10)
                p.drawString(100, 710, grupo.Curso.Nombre)
                p.drawString(100, 690, str(grupo.Curso.Creditos))
                p.drawString(100, 670, grupo.Curso.Carrera.Carrera)
                p.drawString(100, 650, str(grupo.Docente))

                p.drawString(430, 710, str(grupo.Curso.Codigo))
                p.drawString(430, 690, grupo.Grupo)
                p.drawString(430, 670, str(grupo.Periodo))

                diccionario = {'0.00': 'cero', '1.00': 'uno', '2.00': 'dos', '3.00': 'tres', '4.00': 'cuatro', '5.00': 'cinco',
                        '6.00': 'seis', '7.00': 'siete', '8.00': 'ocho', '9.00': 'nueve', '10.00': 'diez',
                        '11.00': 'once', '12.00': 'doce', '13.00': 'trece', '14.00': 'catorce', '15.00': 'quince',
                        '16.00': 'dieciseis', '17.00': 'diecisiete', '18.00': 'dieciocho', '19.00': 'diecinueve',
                        '20.00': 'veinte'}
                i = 565
                for mat in matriculados:
                    if j < 10:
                        p.drawString(50, i, '0' + str(j))
                    else:
                        p.drawString(50, i, str(j))
                    p.drawString(73, i, mat.MatriculaCiclo.Alumno.Codigo)
                    p.drawString(143, i, str(mat.MatriculaCiclo.Alumno))
                    if mat.Promedio() < Decimal('10.00'):
                        p.drawString(423, i, '0' + str(mat.Promedio()).split('.')[0])
                    else:
                        p.drawString(423, i, str(mat.Promedio()).split('.')[0])
                    p.drawString(493, i, diccionario[str(mat.Promedio())])
                    i = i - 20
                    j += 1
                p.showPage()
                p.save()
        return response

    actas_periodocurso.short_description = "Emitir Acta de Notas"

    def matriculados_grupo_excel(self, request, queryset):

        output_name = "matriculados"
        encoding = 'utf8'
        import StringIO
        output = StringIO.StringIO()
        try:
            import xlwt
        except ImportError:
            # xlwt doesn't exist; fall back to csv
            pass

        book = xlwt.Workbook(encoding=encoding)
        i = 0
        for dat in queryset:
            i = i + 1
            if dat.matriculacursos_set.all().count() != 0:
                data = dat.matriculacursos_set.order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                        'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                        'MatriculaCiclo__Alumno__Nombres').values(
                    'MatriculaCiclo__Alumno__Codigo', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                    'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres')
                headers = ['MatriculaCiclo__Alumno__Codigo', 'MatriculaCiclo__Alumno__ApellidoPaterno',
                           'MatriculaCiclo__Alumno__ApellidoMaterno', 'MatriculaCiclo__Alumno__Nombres']
                titulos = ['Código', 'Apellido Paterno', 'Apellido Materno', 'Nombres']

                # Make sure we've got the right type of data to work with
                valid_data = False
                if isinstance(data, ValuesQuerySet):
                    matriculados = list(data)
                elif isinstance(data, QuerySet):
                    matriculados = list(data.values())
                if hasattr(data, '__getitem__'):
                    if isinstance(data[0], dict):
                        if headers is None:
                            headers = data[0].keys()
                        data = [[row[col] for col in headers] for row in data]
                        if titulos is None:
                            data.insert(0, headers)
                        else:
                            data.insert(0, titulos)
                    if hasattr(data[0], '__getitem__'):
                        valid_data = True
                assert valid_data is True, "ExcelResponse requires a sequence of sequences"

                sheet = book.add_sheet('Hoja%s' % i)
                styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
                          'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
                          'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
                          'default': xlwt.Style.default_style}

                for rowx, row in enumerate(data):
                    for colx, value in enumerate(row):
                        if isinstance(value, datetime.datetime):
                            cell_style = styles['datetime']
                        elif isinstance(value, datetime.date):
                            cell_style = styles['date']
                        elif isinstance(value, datetime.time):
                            cell_style = styles['time']
                        else:
                            cell_style = styles['default']
                        sheet.write(rowx + 6, colx, value, style=cell_style)

                sheet.write(0, 0, 'Codigo Curso:', style=cell_style)
                sheet.write(1, 0, 'Curso', style=cell_style)
                sheet.write(2, 0, 'Grupo Horario:', style=cell_style)
                sheet.write(3, 0, 'Docente:', style=cell_style)

                sheet.write(0, 1, dat.Curso.Codigo, style=cell_style)
                sheet.write(1, 1, dat.Curso.Nombre, style=cell_style)
                sheet.write(2, 1, dat.Grupo, style=cell_style)
                sheet.write(3, 1,
                            dat.Docente.Nombres + ' ' + dat.Docente.ApellidoPaterno + ' ' + dat.Docente.ApellidoMaterno,
                            style=cell_style)

                content_type = 'application/vnd.ms-excel'
                file_ext = 'xls'
            else:
                mensaje = "El curso %s no tiene matriculados" % dat
                links = "<a href=''>Volver</a>"
                return render_to_response("Periodos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        book.save(output)
        output.seek(0)
        response = HttpResponse(content=output.getvalue(), content_type=content_type)
        response['Content-Disposition'] = 'attachment;filename="%s.%s"' % (output_name.replace('"', '\"'), file_ext)
        return response

    matriculados_grupo_excel.short_description = "Listado de alumnos matriculados en el grupo seleccionado"

    @csrf_protect_m
    @transaction.atomic()
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        using = router.db_for_write(self.model)

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, model_count, perms_needed, protected) = get_deleted_objects(
            [obj], opts, request.user, self.admin_site, using)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            self.delete_model(request, obj)

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Grupo Horario Eliminado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        object_name = force_unicode(opts.verbose_name)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": object_name}
        else:
            title = _("Are you sure?")

        context = {
            "title": title,
            "object_name": object_name,
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": opts,
            "app_label": app_label,
        }
        context.update(extra_context or {})

        return TemplateResponse(request, self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, current_app=self.admin_site.name)


admin.site.register(PeriodoCurso, PeriodoCursoAdmin)
