# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
from UDL.moodle_webservice import moodle_webservices

ANIO_CHOICES = (
    ('2010', '2010'),
    ('2011', '2011'),
    ('2012', '2012'),
    ('2013', '2013'),
    ('2014', '2014'),
    ('2015', '2015'),
    ('2016', '2016'),
    ('2017', '2017'),
    ('2018', '2018'),
    ('2019', '2019'),
    ('2020', '2020'),
    ('2021', '2021'),
)

SEMESTRE_CHOICES = (
    ('I', 'I'),
    ('II', 'II'),
    ('III', 'III'),
)


class Periodo(models.Model):
    Anio = models.CharField("Año", max_length=4, choices=ANIO_CHOICES)
    Semestre = models.CharField(max_length=3, choices=SEMESTRE_CHOICES)
    Inicio = models.DateField()
    Fin = models.DateField()
    Activo = models.BooleanField("Activo Intranet", default=False)
    Moodleid = models.PositiveIntegerField("Moodle id", editable=False, null=True)

    def save(self, **kwargs):
        nombrecategoria = '%s - %s' % (self.Anio, self.Semestre)
        if self.Moodleid is None:
            datos_categoria = {
                'categories[0][name]': nombrecategoria,
            }
            rest_datos_cateogria = moodle_webservices("core_course_create_categories", datos_categoria)
            self.Moodleid = rest_datos_cateogria[0]['id']
        else:
            datos_categoria = {
                'categories[0][name]': nombrecategoria,
                'categories[0][id]': self.Moodleid,
            }
            moodle_webservices("core_course_update_categories", datos_categoria)

        return super(Periodo, self).save()

    def __unicode__(self):
        return u'%s - %s' % (self.Anio, self.Semestre)

    class Meta:
        verbose_name = "Período UDL"
        verbose_name_plural = "Períodos UDL"
