# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Cursos.models import Curso
from Cursos.models import PeriodoCurso
from Periodos.forms import IndexPeriodoForm, GrupoPeriodoCursoForm
from Periodos.models import Periodo


def index(request):
    return HttpResponseRedirect('/docente/login/')


def listado_periodo_curso(request, periodocurso_id):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.read_periodocurso') or request.user.has_perm(
            'Cursos.add_periodocurso') or request.user.has_perm('Cursos.change_periodocurso'):
        try:
            periodocurso = PeriodoCurso.objects.get(id=periodocurso_id)
        except PeriodoCurso.DoesNotExist:
            raise Http404

        periodo = periodocurso.Periodo
        curso = periodocurso.Curso
        return HttpResponseRedirect('../' + str(periodo.id) + '/' + str(curso.id) + '/')
    else:
        return HttpResponseRedirect('../../../../')


@csrf_protect
def index_configurar_curso(request):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.read_periodocurso') or request.user.has_perm(
            'Cursos.add_periodocurso') or request.user.has_perm('Cursos.change_periodocurso'):
        if request.method == 'POST':
            form = IndexPeriodoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                curso = form.cleaned_data['Curso']
                return HttpResponseRedirect('../' + str(periodo.id) + '/' + str(curso.id) + '/')
        else:
            form = IndexPeriodoForm()
        return render_to_response("Periodos/index.html", {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


def index_periodo_curso(request, periodo_id, curso_id):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.read_periodocurso') or request.user.has_perm(
            'Cursos.add_periodocurso') or request.user.has_perm('Cursos.change_periodocurso'):
        cursos = PeriodoCurso.objects.filter(Periodo__id=periodo_id, Curso__id=curso_id)
        n_grupos = cursos.count()
        if n_grupos == 0:
            try:
                periodo = Periodo.objects.get(id=periodo_id)
                curso = Curso.objects.get(id=curso_id)
            except (Periodo.DoesNotExist, Curso.DoesNotExist):
                raise Http404
        else:
            existe_curso = PeriodoCurso.objects.filter(Periodo__id=periodo_id, Curso__id=curso_id)[0]
            periodo = existe_curso.Periodo
            curso = existe_curso.Curso

        paginator = Paginator(cursos, 10)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)

        return render_to_response("Periodos/periodo_curso.html",
                                  {"user": request.user, "periodo": periodo, "curso": curso, "cursos": results,
                                   "n_grupos": n_grupos}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def agregar_grupo_periodocurso(request, periodo_id, curso_id):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.add_periodocurso'):
        try:
            periodo = Periodo.objects.get(id=periodo_id)
            curso = Curso.objects.get(id=curso_id)
        except (Periodo.DoesNotExist, Curso.DoesNotExist):
            raise Http404
        if request.method == 'POST':
            form = GrupoPeriodoCursoForm(request.POST)
            if form.is_valid():
                grupo = form.cleaned_data['Grupo']
                docente = form.cleaned_data['Docente']
                observaciones = form.cleaned_data['Observaciones']

                grabar_periodocurso = PeriodoCurso(Periodo_id=periodo_id, Curso_id=curso_id, Grupo=grupo,
                                                   Docente=docente, Observaciones=observaciones)
                grabar_periodocurso.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = GrupoPeriodoCursoForm(initial={"Periodo1": periodo_id, "Curso1": curso_id})

        return render_to_response("Periodos/agregar_grupo_periodocurso.html",
                                  {"form": form, "user": request.user, "periodo": periodo, "curso": curso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../../../'>Inicio</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_grupo_periodocurso(request, periodo_curso_id):
    if request.user.is_authenticated() and request.user.has_perm('Cursos.change_periodocurso'):
        try:
            periodo_curso = PeriodoCurso.objects.get(id=periodo_curso_id)
        except PeriodoCurso.DoesNotExist:
            raise Http404

        if request.method == 'POST':
            form = GrupoPeriodoCursoForm(request.POST)
            if form.is_valid():
                grupo = form.cleaned_data['Grupo']
                docente = form.cleaned_data['Docente']
                observaciones = form.cleaned_data['Observaciones']

                periodo_curso.Grupo = grupo
                periodo_curso.Docente = docente
                periodo_curso.Observaciones = observaciones
                periodo_curso.save()

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        else:
            form = GrupoPeriodoCursoForm(
                initial={"Periodo1": periodo_curso.Periodo.id, "Curso1": periodo_curso.Curso.id,
                         "Grupo": periodo_curso.Grupo, "Docente": periodo_curso.Docente.id,
                         "Observaciones": periodo_curso.Observaciones})

        return render_to_response("Periodos/editar_grupo_periodocurso.html",
                                  {"form": form, "user": request.user, "periodo_curso": periodo_curso},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../../'>Inicio</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
