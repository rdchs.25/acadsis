# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Periodo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Anio', models.CharField(max_length=4, verbose_name=b'A\xc3\xb1o', choices=[(b'2010', b'2010'), (b'2011', b'2011'), (b'2012', b'2012'), (b'2013', b'2013'), (b'2014', b'2014'), (b'2015', b'2015'), (b'2016', b'2016'), (b'2017', b'2017'), (b'2018', b'2018'), (b'2019', b'2019'), (b'2020', b'2020'), (b'2021', b'2021')])),
                ('Semestre', models.CharField(max_length=3, choices=[(b'I', b'I'), (b'II', b'II'), (b'III', b'III')])),
                ('Inicio', models.DateField()),
                ('Fin', models.DateField()),
                ('Activo', models.BooleanField(default=False, verbose_name=b'Activo Intranet')),
            ],
            options={
                'verbose_name': 'Per\xedodo UDL',
                'verbose_name_plural': 'Per\xedodos UDL',
            },
        ),
    ]
