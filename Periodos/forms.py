# -*- coding: utf-8 -*-
from django import forms

from Carreras.models import Carrera
from Cursos.models import Curso, CICLO_CHOICES, PeriodoCurso
from Docentes.models import Docente
from Periodos.models import Periodo


class IndexPeriodoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True,
                                     label='Período')
    Carrera = forms.ModelChoiceField(queryset=Carrera.objects.all(), widget=forms.Select(), required=True,
                                     label='Carrera')
    Ciclo = forms.ChoiceField(choices=CICLO_CHOICES, required=True, label="Ciclo")
    Curso = forms.ModelChoiceField(queryset=Curso.objects.all(), widget=forms.Select(), required=True, label='Curso')

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Periodos/frm_index_periodos.js"
              )


class GrupoPeriodoCursoForm(forms.ModelForm):
    Grupo = forms.CharField(min_length=3, max_length=3, required=True, label="Grupo Horario")
    Docente = forms.ModelChoiceField(
        queryset=Docente.objects.filter(Estado=True).order_by('ApellidoPaterno', 'ApellidoMaterno', 'Nombres'),
        widget=forms.Select(), required=True, label='Docente')
    Observaciones = forms.CharField(widget=forms.Textarea, required=False, label="Comentario")
    Periodo1 = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    Curso1 = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    class Meta:
        model = PeriodoCurso
        exclude = ('Periodo', 'Curso', 'Aprobacion', 'EditarAsistencias', 'EditarFormula', 'EditarNotas',)
