# Create your views here.
from django.core import serializers
from django.http import HttpResponse

from Ubicacion.models import Provincia, Distrito


def provincia(request, departamento_id):
    if departamento_id != "":
        return HttpResponse(serializers.serialize('json', Provincia.objects.filter(Departamento=departamento_id),
                                                  fields=('pk', 'Provincia')))


def ubigeo(request, provincia_id):
    if provincia_id != "":
        return HttpResponse(serializers.serialize('json', Distrito.objects.filter(Provincia=provincia_id),
                                                  fields=('pk', 'Distrito')))
