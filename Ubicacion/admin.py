from django.contrib import admin

from Ubicacion.models import Departamento, Provincia, Distrito

admin.site.register(Departamento)
admin.site.register(Provincia)
