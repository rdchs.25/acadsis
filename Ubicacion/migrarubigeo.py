#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

# imports para traer variable de entorno django
import codecs
from Ubicacion.models import Departamento, Provincia, Distrito


def script_ubigeo():
    file1 = codecs.open("extras/plantilla.csv", 'r', 'utf-8')
    contador = 0
    for line in file1:
        line = line.strip('\n')
        linea = line.split(',')
        codigoubigeo = linea[0]
        departamento = linea[1]
        provincia = linea[2]
        distrito = linea[3]
        departamentoacadsis = None
        try:
            departamentoacadsis = Departamento.objects.get(Departamento=departamento.capitalize())
        except Departamento.DoesNotExist:
            print ("no existe en base de datos ACADSIS el departamento: " + departamento)
            break
        provinciaacadsis = None
        try:
            provinciaacadsis = Provincia.objects.get(Provincia=provincia.capitalize(), Departamento=departamentoacadsis)
        except Provincia.DoesNotExist:
            print ("no existe en base de datos ACADSIS la provincia: " + provincia)
            break
        print (distrito)
        try:
            distritoacadsis = Distrito()
            distritoacadsis.Distrito = distrito
            distritoacadsis.Codigoubigeo = codigoubigeo
            distritoacadsis.Provincia = provinciaacadsis
            distritoacadsis.save()
        except ValueError:
            print ("error registrando el distrito: " + distrito)
        contador = contador + 1

    file1.close()


script_ubigeo()
