# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import Tramitedocumento.models
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Pagos', '0006_pagoalumno_usado'),
        ('Autoridades', '0002_tipo_carrera'),
    ]

    operations = [
        migrations.CreateModel(
            name='Asunto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=100, verbose_name=b'Asunto')),
                ('Descripcion', models.TextField(help_text=b'Redacta el asunto', null=True, blank=True)),
                ('Fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('Ultima_edicion', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='HistorialDocumento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Estado', models.CharField(default=b'tramite', max_length=20, choices=[(b'tramite', b'En tr\xc3\xa1mite'), (b'aceptado', b'Aceptado'), (b'rechazado', b'Rechazado'), (b'cancelado', b'Cancelado'), (b'extraviado', b'Extraviado'), (b'archivado', b'Archivado')])),
                ('Descripcion', models.TextField(null=True, blank=True)),
                ('Atendido', models.BooleanField(default=False, verbose_name=b'Atendido')),
                ('Fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('Ultima_edicion', models.DateTimeField(auto_now=True)),
                ('Archivo', models.FileField(upload_to=Tramitedocumento.models.obtenerpath_historial, verbose_name=b'Archivo')),
                ('Destinatario', models.ForeignKey(related_name='Destino', blank=True, to='Autoridades.Autoridad', null=True)),
                ('Remitente', models.ForeignKey(related_name='Remite', blank=True, to='Autoridades.Autoridad', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TipoDocumento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=50, verbose_name=b'Tipo de Documento')),
                ('Descripcion', models.TextField(help_text=b'Redacta el tipo del documento', null=True, blank=True)),
                ('Fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('Ultima_edicion', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='TramiteDocumento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Codigo', models.CharField(max_length=50, editable=False)),
                ('NroDocumento', models.CharField(max_length=20, null=True, blank=True)),
                ('FechaIngreso', models.DateField(default=django.utils.timezone.now, verbose_name=b'Fecha de Ingreso')),
                ('FechaDocumento', models.DateField(default=django.utils.timezone.now, verbose_name=b'Fecha de Documento')),
                ('Atendido', models.BooleanField(default=False, verbose_name=b'Atendido', editable=False)),
                ('IsAdministrativo', models.BooleanField(default=False, verbose_name=b'Administrativo', editable=False)),
                ('Egresado', models.BooleanField(default=False, verbose_name=b'Egresado', editable=False)),
                ('Descripcion', models.TextField(help_text=b'Redacta una descripci\xc3\xb3n', null=True, blank=True)),
                ('Fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('Ultima_edicion', models.DateTimeField(auto_now=True)),
                ('Archivo', models.FileField(upload_to=Tramitedocumento.models.obtenerpath, verbose_name=b'Archivo')),
                ('Administrativo', models.ForeignKey(verbose_name=b'Administrativo', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('Asunto', models.ForeignKey(to='Tramitedocumento.Asunto')),
                ('PagoAlumno', models.ForeignKey(verbose_name=b'Concepto', blank=True, to='Pagos.PagoAlumno', null=True)),
                ('TipoDocumento', models.ForeignKey(verbose_name=b'Tipo Doc.', to='Tramitedocumento.TipoDocumento')),
            ],
        ),
        migrations.AddField(
            model_name='historialdocumento',
            name='TramiteDocumento',
            field=models.ForeignKey(to='Tramitedocumento.TramiteDocumento'),
        ),
    ]
