# -*- coding: utf-8 -*-

from django.conf.urls import url

from Tramitedocumento.views import index_tramites, buscar_alumno_matriculado, agregar_documento, \
    tramites_alumno_periodo, editar_documento, profile, derivar_documento, json_autoridades, verificar_destinatario, \
    historial_documento,atender_documento,ver_documento

app_name = 'Tramitedocumento'

urlpatterns = [
    url(r'^Tramitedocumento/documento/$', index_tramites),
    url(r'^Tramitedocumento/documento/periodo/(\d+)/$', buscar_alumno_matriculado),
    url(r'^Tramitedocumento/documento/add/(\d+)/(\d+)/$', agregar_documento),
    url(r'^Tramitedocumento/documento/(\d+)/$', editar_documento),
    url(r'^Tramitedocumento/documento/ver/(\d+)/(\d+)/$', tramites_alumno_periodo),
    url(r'^Tramitedocumento/documento/profile/$', profile),
    url(r'^Tramitedocumento/documento/profile/derivar/(\d+)/$', derivar_documento),
    url(r'^Tramitedocumento/documento/profile/json_autoridades/$', json_autoridades),
    url(r'^Tramitedocumento/documento/profile/verificar_destinatario/$', verificar_destinatario),
    url(r'^Tramitedocumento/documento/profile/historial/(\d+)/$', historial_documento),
    url(r'^Tramitedocumento/documento/profile/atender/(\d+)/$', atender_documento),
    url(r'^Tramitedocumento/documento/profile/ver_documento/(\d+)/$', ver_documento),

]
