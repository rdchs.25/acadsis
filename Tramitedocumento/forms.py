# -*- coding: utf-8 -*-

import datetime

from django import forms
from django.forms.utils import ErrorList

from Alumnos.widgets import DateTimeWidget
from Autoridades.models import Autoridad
from Tramitedocumento.models import TipoDocumento, Asunto


class DocumentoForm(forms.Form):
    NroDocumento = forms.CharField(max_length=20, required=True)
    TipoDocumento = forms.ModelChoiceField(queryset=TipoDocumento.objects.all(), label='Tipo Documento')
    Asunto = forms.ModelChoiceField(queryset=Asunto.objects.all(), label='Asunto')
    Destinatario = forms.ModelChoiceField(queryset=Autoridad.objects.all(), widget=forms.Select(), required=True,
                                          label='Destinatario')
    FechaIngreso = forms.DateField(widget=DateTimeWidget, initial=datetime.date.today, label='Fecha Ingreso')
    FechaDocumento = forms.DateField(widget=DateTimeWidget, initial=datetime.date.today, label='Fecha Documento')
    Descripcion = forms.CharField(widget=forms.Textarea, required=False)
    PagoAlumno = forms.ModelChoiceField(queryset=None, widget=forms.Select(), required=True, label='Concepto Pago')
    Archivo = forms.FileField()


    def __init__(self, conceptos, *args, **kwargs):
        super(DocumentoForm, self).__init__(*args, **kwargs)
        self.fields['PagoAlumno'].queryset = conceptos

    class Media:
        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class HistorialDocumentoForm(forms.Form):
    Descripcion = forms.CharField(widget=forms.Textarea, required=False)
    Archivo = forms.FileField()


