from django.contrib import admin

from Tramitedocumento.models import Asunto, TipoDocumento, TramiteDocumento

# Register your models here.
admin.site.register(Asunto)
admin.site.register(TipoDocumento)
admin.site.register(TramiteDocumento)
