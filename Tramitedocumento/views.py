# -*- coding: utf-8 -*-
import datetime

from django.core import serializers
# imports para la paginacion
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Alumnos.models import Alumno
from Autoridades.models import Autoridad
from Carreras.models import Carrera
from Matriculas.forms import IndexMatriculaForm
from Matriculas.models import CONDICION_CHOICES
from Matriculas.models import MatriculaCiclo, MatriculaCursos
from Pagos.models import PagoAlumno
from Periodos.models import ANIO_CHOICES, SEMESTRE_CHOICES
from Periodos.models import Periodo
from Tramitedocumento.forms import DocumentoForm, HistorialDocumentoForm
from Tramitedocumento.models import TramiteDocumento, HistorialDocumento, TipoDocumento, Asunto


@csrf_protect
def index_tramites(request):
    if request.user.is_authenticated() and request.user.has_perm(
            'Tramitedocumento.read_documento') or request.user.has_perm(
        'Tramitedocumento.change_documento') or request.user.has_perm('Tramitedocumento.add_documento'):
        if request.method == 'POST':
            form = IndexMatriculaForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("periodo/" + str(periodo.id) + "/")
        else:
            form = IndexMatriculaForm()
        return render_to_response('Tramitedocumento/index.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def buscar_alumno_matriculado(request, idperiodo):
    if request.user.is_authenticated() and request.user.has_perm(
            'Tramitedocumento.read_documento') or request.user.has_perm(
        'Tramitedocumento.add_documento') or request.user.has_perm('Tramitedocumento.change_documento'):
        if request.method == 'POST':
            action = request.POST['action']
            all = request.POST['select_across']
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')

            if all == '0':
                matriculados_ciclo = request.POST.getlist('_selected_action')
            else:
                matriculados_ciclo = []
                ids_mat = MatriculaCiclo.objects.filter(Periodo__id=idperiodo).order_by('Alumno__Carrera__Carrera',
                                                                                        'Alumno__ApellidoPaterno',
                                                                                        'Alumno__ApellidoMaterno',
                                                                                        'Alumno__Nombres')
                if query1:
                    ids_mat1 = ids_mat.filter(Alumno__Carrera__id=query1)
                else:
                    ids_mat1 = ids_mat

                if query2:
                    ids_mat2 = ids_mat1.filter(Alumno__AnioIngreso=query2)
                else:
                    ids_mat2 = ids_mat1

                if query3:
                    ids_mat3 = ids_mat2.filter(Alumno__Semestre=query3)
                else:
                    ids_mat3 = ids_mat2

                if query4:
                    ids_mat4 = ids_mat3.filter(Estado=query4)
                else:
                    ids_mat4 = ids_mat3

                for id_mat in ids_mat4:
                    matriculados_ciclo.append(id_mat.id)
            alumnos_carnet = []
            matriculados = []

            for mat in matriculados_ciclo:
                alumno = MatriculaCiclo.objects.get(id=mat)
                alumnos_carnet.append(alumno)

            for mat in matriculados_ciclo:
                cursos_matriculado = MatriculaCursos.objects.filter(MatriculaCiclo__id=mat)
                if cursos_matriculado.count() > 0:
                    matriculados.append(cursos_matriculado)
        else:

            query = request.GET.get('q', '')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')

            carreras = Carrera.objects.all()
            anios = []
            semestres = []
            condiciones = []

            for i in ANIO_CHOICES:
                anios.append(i[0])

            for s in SEMESTRE_CHOICES:
                semestres.append(s[0])

            for c in CONDICION_CHOICES:
                condiciones.append(c[0])

            try:
                periodo = Periodo.objects.get(id=idperiodo)
            except Periodo.DoesNotExist:
                mensaje = "El periodo elegido no es correcto"
                links = "<a href='../../../'>Volver</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

            results = MatriculaCiclo.objects.filter(Periodo=periodo)

            if query1:
                results1 = results.filter(Alumno__Carrera__id=query1)
            else:
                results1 = results

            if query2:
                results2 = results1.filter(Alumno__AnioIngreso=query2)
            else:
                results2 = results1

            if query3:
                results3 = results2.filter(Alumno__Semestre=query3)
            else:
                results3 = results2

            if query4:
                results4 = results3.filter(Estado=query4)
            else:
                results4 = results3

            if query:
                str_q_nombres = "Q(Alumno__Nombres__icontains = "
                str_q_apellidopaterno = "Q(Alumno__ApellidoPaterno__icontains = "
                str_q_apellidomaterno = "Q(Alumno__ApellidoMaterno__icontains = "
                lista_query = query.split(" ")
                g = ""
                for i in lista_query:
                    g = g + "Q(Alumno__Nombres__icontains = \"%s\")|Q(Alumno__ApellidoPaterno__icontains = \"%s\")|Q(Alumno__ApellidoMaterno__icontains = \"%s\")|" % (
                        i, i, i)

                qset = (
                        Q(Alumno__Codigo__icontains=query) |
                        eval(g[:-1])
                )
                results5 = results4.filter(qset).order_by('Alumno__Carrera__Carrera', 'Alumno__ApellidoPaterno',
                                                          'Alumno__ApellidoMaterno', 'Alumno__Nombres')
            else:
                results5 = results4.order_by('Alumno__Carrera__Carrera', 'Alumno__ApellidoPaterno',
                                             'Alumno__ApellidoMaterno', 'Alumno__Nombres')

            n_alumnos = results5.count()
            paginator = Paginator(results5, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Tramitedocumento/Individual/buscar_matriculado.html",
                                      {"results": results, "paginator": paginator, "periodo": periodo, "query": query,
                                       "query1": query1, "query2": query2, "query3": query3, "query4": query4,
                                       "carreras": carreras, "anios": anios, "semestres": semestres,
                                       "condiciones": condiciones, "n_alumnos": n_alumnos, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../../')


@csrf_protect
def agregar_documento(request, idperiodo, idalumno):
    if request.user.is_authenticated() and request.user.has_perm('Tramitedocumento.add_documento'):
        try:
            periodo = Periodo.objects.get(id=idperiodo)
        except Periodo.DoesNotExist:
            mensaje = "El periodo elegido no es correcto"
            links = "<a href='../../../documento/'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        try:
            alumno = Alumno.objects.get(id=idalumno)
        except Alumno.DoesNotExist:
            mensaje = "El ingresante elegido no es correcto"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        conceptos = PagoAlumno.objects.filter(MatriculaCiclo__Alumno__id=idalumno, MatriculaCiclo__Periodo_id=idperiodo,
                                              Usado=False,
                                              Estado=True)

        if request.method == 'POST':
            form = DocumentoForm(conceptos, request.POST, request.FILES)
            if form.is_valid():
                codigo = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
                nrodocumento = form.cleaned_data['NroDocumento']
                fechaingreso = form.cleaned_data['FechaIngreso']
                fechadocumento = form.cleaned_data['FechaDocumento']
                descripcion = form.cleaned_data['Descripcion']
                archivo = form.cleaned_data['Archivo']
                asunto_id = form.cleaned_data['Asunto']
                pagoalumno_id = form.cleaned_data['PagoAlumno']
                tipodocumento_id = form.cleaned_data['TipoDocumento']
                destinatario = form.cleaned_data['Destinatario']

                nuevo_tramite = TramiteDocumento(Codigo=codigo, NroDocumento=nrodocumento, FechaIngreso=fechaingreso,
                                                 FechaDocumento=fechadocumento, Descripcion=descripcion,
                                                 Archivo=archivo, Asunto=asunto_id, PagoAlumno=pagoalumno_id,
                                                 TipoDocumento=tipodocumento_id)
                nuevo_tramite.save()

                nuevo_historial = HistorialDocumento(Descripcion=descripcion, TramiteDocumento_id=int(nuevo_tramite.id),
                                                     Destinatario=destinatario, Archivo=archivo)
                nuevo_historial.save()

                return HttpResponseRedirect('../../../../documento/periodo/%s/' % periodo.id)
        else:
            form = DocumentoForm(conceptos=conceptos)

        return render_to_response("Tramitedocumento/Individual/agregar_documento.html",
                                  {"alumno": alumno, "periodo": periodo, "form": form, "user": request.user},
                                  context_instance=RequestContext(request))

    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def tramites_alumno_periodo(request, idperiodo, idalumno):
    if request.user.is_authenticated() and request.user.has_perm('Tramitedocumento.add_documento'):
        try:
            matriculado = MatriculaCiclo.objects.get(Periodo_id=idperiodo, Alumno_id=idalumno)
            documentos = TramiteDocumento.objects.filter(PagoAlumno__MatriculaCiclo_id=matriculado.id)
            periodos = Periodo.objects.all()

        except MatriculaCiclo.DoesNotExist:
            raise Http404

        query = request.GET.get('q', '')
        query1 = request.GET.get('p', '')

        if query and query1:
            if query1 != "-1":
                documentos = TramiteDocumento.objects.filter(NroDocumento__icontains=query,
                                                             PagoAlumno__MatriculaCiclo__Alumno__id=idalumno,
                                                             PagoAlumno__MatriculaCiclo__Periodo__id=query1).order_by(
                    'NroDocumento')
            else:
                documentos = TramiteDocumento.objects.filter(NroDocumento__icontains=query,
                                                             PagoAlumno__MatriculaCiclo__Alumno__id=idalumno).order_by(
                    'NroDocumento')
        elif query:
            documentos = TramiteDocumento.objects.filter(PagoAlumno__MatriculaCiclo__id=matriculado.id,
                                                         NroDocumento__icontains=query,
                                                         PagoAlumno__MatriculaCiclo__Alumno__id=idalumno)(
                'NroDocumento')
        elif query1 and query1 != "-1":
            documentos = TramiteDocumento.objects.filter(PagoAlumno__MatriculaCiclo__Periodo__id=query1,
                                                         PagoAlumno__MatriculaCiclo__Alumno__id=idalumno).order_by(
                'NroDocumento')

        elif query1 == "-1":
            documentos = TramiteDocumento.objects.filter(PagoAlumno__MatriculaCiclo__Alumno__id=idalumno).order_by(
                'NroDocumento')

        return render_to_response("Tramitedocumento/Individual/tramites_alumno_periodo.html",
                                  {"matriculado": matriculado, "periodos": periodos,
                                   "documentos": documentos,
                                   "query": query, "query1": query1,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def editar_documento(request, idtramitedocumento):
    if request.user.is_authenticated() and request.user.has_perm('Tramitedocumento.change_documento'):
        try:
            tramitedocumento = TramiteDocumento.objects.get(id=idtramitedocumento)
            periodo = Periodo.objects.get(id=tramitedocumento.PagoAlumno.MatriculaCiclo.Periodo.id)
            historialdocumento = HistorialDocumento.objects.get(TramiteDocumento_id=idtramitedocumento, Remitente=None)

        except TramiteDocumento.DoesNotExist:
            mensaje = "El documento elegido no es correcta"
            links = "<a href='../../matriculacursos/'>Volver</a>"
            return render_to_response("Matriculas/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        matriculaciclo = tramitedocumento.PagoAlumno.MatriculaCiclo
        conceptos = PagoAlumno.objects.filter(
            MatriculaCiclo__Alumno__id=matriculaciclo.Alumno.id,
            MatriculaCiclo__Periodo_id=matriculaciclo.Periodo.id, Usado=False,
            Estado=True)
        if request.method == 'POST':
            form = DocumentoForm(conceptos, request.POST, request.FILES)
            if form.is_valid():
                tramitedocumento.NroDocumento = form.cleaned_data['NroDocumento']
                tramitedocumento.FechaIngreso = form.cleaned_data['FechaIngreso']
                tramitedocumento.FechaDocumento = form.cleaned_data['FechaDocumento']
                tramitedocumento.Asunto = form.cleaned_data['Asunto']
                tramitedocumento.PagoAlumno = form.cleaned_data['PagoAlumno']
                tramitedocumento.TipoDocumento = form.cleaned_data['TipoDocumento']
                tramitedocumento.Descripcion = form.cleaned_data['Descripcion']
                tramitedocumento.Archivo = form.cleaned_data['Archivo']
                tramitedocumento.save()

                historialdocumento.Destinatario = form.cleaned_data['Destinatario']
                historialdocumento.Descripcion = form.cleaned_data['Descripcion']
                historialdocumento.Archivo = form.cleaned_data['Archivo']
                historialdocumento.save()

                return HttpResponseRedirect('../../documento/ver/%s/%s' % (periodo.id, matriculaciclo.Alumno.id))

        else:
            form = DocumentoForm(conceptos=conceptos,
                                 initial={'NroDocumento': tramitedocumento.NroDocumento,
                                          'TipoDocumento': tramitedocumento.TipoDocumento,
                                          'Asunto': tramitedocumento.Asunto,
                                          'PagoAlumno': tramitedocumento.PagoAlumno,
                                          'FechaIngreso': tramitedocumento.FechaIngreso,
                                          'FechaDocumento': tramitedocumento.FechaDocumento,
                                          'Descripcion': tramitedocumento.Descripcion,
                                          'Archivo': tramitedocumento.Archivo,
                                          'Destinatario': historialdocumento.Destinatario})

        return render_to_response("Tramitedocumento/Individual/edit_documento.html",
                                  {"alumno": matriculaciclo.Alumno, "periodo": periodo,
                                   "form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../../../'>Volver</a>"
        return render_to_response("Matriculas/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def profile(request):
    if request.user.is_authenticated():

        verifica_post = False
        documentos = request.GET.get('documentos', 'derivados')
        emisor = request.GET.get('emisor', '')
        codigo = request.GET.get('codigo', '')
        fecha = request.GET.get('fecha', '')
        docs = []
        tipo_docs = TipoDocumento.objects.all().order_by('Nombre')
        asuntos = Asunto.objects.all().order_by('Nombre')
        hoy = datetime.date.today().strftime('%d/%m/%Y')
        destino = None
        remite = None
        if documentos == 'derivados':
            results = HistorialDocumento.objects.filter(Destinatario=request.user)

            if emisor:
                results1 = results.filter(Remitente__Apellidos__icontains=emisor)
            else:
                results1 = results

            if codigo:
                results2 = results1.filter(TramiteDocumento__Codigo__icontains=codigo)
            else:
                results2 = results1

            if fecha:
                fecha_dia = int(fecha.split('/')[0])
                fecha_mes = int(fecha.split('/')[1])
                fecha_anio = int(fecha.split('/')[2])
                results3 = results2.filter(
                    TramiteDocumento__FechaIngreso=datetime.date(fecha_anio, fecha_mes, fecha_dia))
            else:
                results3 = results2

            for resultado in results3.order_by('-TramiteDocumento__FechaIngreso'):
                historial = HistorialDocumento.objects.filter(TramiteDocumento=resultado.TramiteDocumento,
                                                              Remitente=request.user,
                                                              Atendido=False)
                if historial.count() != 0:
                    if not [resultado.TramiteDocumento, True, False] in docs:
                        docs.append([resultado.TramiteDocumento, True, False])
                else:
                    if not [resultado.TramiteDocumento, False, False] in docs:
                        docs.append([resultado.TramiteDocumento, False, False])
        elif documentos == 'enviados':
            results = HistorialDocumento.objects.filter(Remitente=request.user)

            if emisor:
                results1 = results.filter(Remitente__Apellidos__icontains=emisor)
            else:
                results1 = results

            if codigo:
                results2 = results1.filter(TramiteDocumento__Codigo__icontains=codigo)
            else:
                results2 = results1

            if fecha:
                fecha_dia = int(fecha.split('/')[0])
                fecha_mes = int(fecha.split('/')[1])
                fecha_anio = int(fecha.split('/')[2])
                results3 = results2.filter(
                    TramiteDocumento__FechaIngreso=datetime.date(fecha_anio, fecha_mes, fecha_dia))
            else:
                results3 = results2

            for resultado in results3.order_by('-TramiteDocumento__FechaIngreso'):
                docs.append([resultado, False, True])

        n_documentos = len(docs)
        paginator = Paginator(docs, 100)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        # If page request (9999) is out of range, deliver last page of results.
        try:
            results = paginator.page(page)
        except (EmptyPage, InvalidPage):
            results = paginator.page(paginator.num_pages)

        return render_to_response("Tramitedocumento/Individual/profile.html",
                                  {"user": request.user, "results": results, "paginator": paginator,
                                   "n_documentos": n_documentos, "documentos": documentos, "emisor": emisor,
                                   "codigo": codigo, "fecha": fecha, "tipo_docs": tipo_docs, "asuntos": asuntos,
                                   "hoy": hoy, "verifica_post": verifica_post, "remite": remite, "destino": destino})
    else:
        return HttpResponseRedirect('/')


def derivar_documento(request, id_documento):
    if request.user.is_authenticated():
        try:
            documento = TramiteDocumento.objects.get(id=id_documento)
        except:
            HttpResponse('El documento no existe')

        if request.method == "POST":
            form = HistorialDocumentoForm(request.POST, request.FILES)
            if form.is_valid():
                destinatario = Autoridad.objects.get(Abreviatura=request.POST['input_destinatario'])
                descripcion = form.cleaned_data['Descripcion']
                archivo = form.cleaned_data['Archivo']
                usuario = int(request.user.id)
                derivar = HistorialDocumento(TramiteDocumento_id=int(documento.id), Remitente_id=usuario,
                                             Destinatario_id=int(destinatario.id), Descripcion=descripcion,
                                             Archivo=archivo)
                derivar.save()
                return HttpResponseRedirect('../../../profile/')
        else:
            form = HistorialDocumentoForm()

        return render_to_response("Tramitedocumento/Individual/derivar_documento.html",
                                  {"form": form, "user": request.user, "documento": documento},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/')


def json_autoridades(request):
    if request.user.is_authenticated():
        query = request.GET.get('term', '')
        qset = (
                Q(Nombres__icontains=query) |
                Q(Apellidos__icontains=query)
        )
        autoridades = Autoridad.objects.filter(qset).order_by('Apellidos', 'Nombres').exclude(
            id=request.user.id)
        return HttpResponse(serializers.serialize('json', autoridades, fields=(
            'pk', "Apellidos", "Nombres", "Abreviatura")))
    else:
        return HttpResponseRedirect('/')


def verificar_destinatario(request):
    if request.user.is_authenticated():
        query = request.GET.get('term', '')
        usuarios = Autoridad.objects.filter(Abreviatura=query)
        return HttpResponse(serializers.serialize('json', usuarios, fields=(
            'pk', 'Abreviatura', "Apellidos", "Nombres")))
    else:
        return HttpResponseRedirect('/')


def historial_documento(request, id_documento):
    if request.user.is_authenticated():
        try:
            documento = TramiteDocumento.objects.get(id=id_documento)
            historial = documento.historialdocumento_set.all().order_by('Fecha_creacion')
        except:
            HttpResponse('El documento no existe')
        return render_to_response("Tramitedocumento/Individual/historial_documento.html",
                                  {"user": request.user, "documento": documento, "historial": historial},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/')


def atender_documento(request, id_documento):
    if request.user.is_authenticated():
        try:
            documento = TramiteDocumento.objects.get(id=id_documento)
        except:
            HttpResponse('El documento no existe')

        if request.method == "POST":
            estado = request.POST['Estado']
            comentario = request.POST['Comentario']
            usuario = int(request.user.id)
            atender = HistorialDocumento(TramiteDocumento_id=int(documento.id), Remitente_id=usuario,
                                         Descripcion=comentario, Estado=estado, Atendido=True)
            atender.save()
            documento.Atendido = True
            documento.save()
            return HttpResponseRedirect('../../../profile/')
        else:
            return render_to_response("Tramitedocumento/Individual/atender_documento.html",
                                      {"user": request.user, "documento": documento},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/')


def ver_documento(request, id_documento):
    if request.user.is_authenticated():
        try:
            documento = TramiteDocumento.objects.get(id=id_documento)
        except:
            HttpResponse('El documento no existe')
        return render_to_response("Tramitedocumento/Individual/ver_documento.html",
                                  {"user": request.user, "documento": documento},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/')
