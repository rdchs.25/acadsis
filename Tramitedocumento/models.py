# -*- coding: utf-8 -*-
import datetime

import django.utils.timezone
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from Autoridades.models import Autoridad
from Pagos.models import PagoAlumno

# Create your models here.

ESTADO_CHOICES = (
    ('tramite', 'En trámite'),
    ('aceptado', 'Aceptado'),
    ('rechazado', 'Rechazado'),
    ('cancelado', 'Cancelado'),
    ('extraviado', 'Extraviado'),
    ('archivado', 'Archivado'),
)


class Asunto(models.Model):
    Nombre = models.CharField(max_length=100, verbose_name="Asunto")
    Descripcion = models.TextField(help_text='Redacta el asunto', null=True, blank=True)
    Fecha_creacion = models.DateTimeField(auto_now_add=True)
    Ultima_edicion = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % (self.Nombre)


class TipoDocumento(models.Model):
    Nombre = models.CharField(max_length=50, verbose_name="Tipo de Documento")
    Descripcion = models.TextField(help_text='Redacta el tipo del documento', null=True, blank=True)
    Fecha_creacion = models.DateTimeField(auto_now_add=True)
    Ultima_edicion = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % (self.Nombre)


def obtenerpath(instance, filename):
    ext = filename.split('.')[-1]
    ahora = datetime.datetime.now()
    return u'tramitedocumentos/%s/%s.%s' % (instance.PagoAlumno.MatriculaCiclo.Alumno.username,
                                            ahora.strftime('%Y-%m-%d-%H-%M-%S'), ext)


class TramiteDocumento(models.Model):
    Codigo = models.CharField(max_length=50, editable=False)
    NroDocumento = models.CharField(max_length=20, null=True, blank=True)
    FechaIngreso = models.DateField("Fecha de Ingreso", default=django.utils.timezone.now)
    FechaDocumento = models.DateField("Fecha de Documento", default=django.utils.timezone.now)
    Atendido = models.BooleanField("Atendido", default=False, editable=False)
    IsAdministrativo = models.BooleanField("Administrativo", default=False, editable=False)
    Egresado = models.BooleanField("Egresado", default=False, editable=False)
    Descripcion = models.TextField(help_text='Redacta una descripción', null=True, blank=True)
    Fecha_creacion = models.DateTimeField(auto_now_add=True, editable=False)
    Ultima_edicion = models.DateTimeField(auto_now=True, editable=False)
    TipoDocumento = models.ForeignKey(TipoDocumento, verbose_name='Tipo Doc.')
    Asunto = models.ForeignKey(Asunto)
    PagoAlumno = models.ForeignKey(PagoAlumno, verbose_name="Concepto", null=True, blank=True)
    Administrativo = models.ForeignKey(User, verbose_name="Administrativo", null=True, blank=True)
    Archivo = models.FileField(upload_to=obtenerpath, verbose_name="Archivo")

    def __unicode__(self):
        return u'%s - %s' % (
            self.PagoAlumno.MatriculaCiclo.Alumno, self.Asunto)

    def archivo(self):
        if self.Archivo == '' or self.Archivo is None:
            return 'No'
        else:
            return '<a href="%s%s" target="_blank" >Si</a>' % (settings.MEDIA_URL, self.Archivo.name)

    archivo.allow_tags = True

    def atendido(self):
        for historial in self.historialdocumento_set.all():
            if historial.Atendido == True:
                self.Atendido = True
                self.save()
                return True
        return False

    def estado(self):
        for historial in self.historialdocumento_set.all():
            if historial.Atendido == True:
                return historial.Estado
        return 'tramite'


def obtenerpath_historial(instance, filename):
    ext = filename.split('.')[-1]
    ahora = datetime.datetime.now()
    return u'tramitedocumentos/%s/historial/%s.%s' % (
        instance.TramiteDocumento.PagoAlumno.MatriculaCiclo.Alumno.username,
        ahora.strftime('%Y-%m-%d-%H-%M-%S'), ext)


class HistorialDocumento(models.Model):
    Estado = models.CharField(max_length=20, choices=ESTADO_CHOICES, default='tramite')
    Descripcion = models.TextField(blank=True, null=True)
    Atendido = models.BooleanField("Atendido", default=False)
    TramiteDocumento = models.ForeignKey(TramiteDocumento)
    Remitente = models.ForeignKey(Autoridad, related_name='Remite', null=True, blank=True)
    Destinatario = models.ForeignKey(Autoridad, related_name='Destino', null=True, blank=True)
    Fecha_creacion = models.DateTimeField(auto_now_add=True, editable=False)
    Ultima_edicion = models.DateTimeField(auto_now=True, editable=False)
    Archivo = models.FileField(upload_to=obtenerpath_historial, verbose_name="Archivo")

    def Emisor(self):
        return u'%s' % self.Emisor

    def FechaDocumento(self):
        return u'%s' % self.TramiteDocumento.FechaIngreso.strftime('%Y-%m-%d')

    def archivo(self):
        if self.TramiteDocumento.Archivo == '' or self.TramiteDocumento.Archivo == None:
            return 'No'
        else:
            return '<a href="%s%s" target="_blank" >Si</a>' % (settings.MEDIA_URL, self.Archivo.name)

    archivo.allow_tags = True

    def __unicode__(self):
        return u'%s - %s - %s' % (self.TramiteDocumento, self.Remitente, self.Destinatario)
