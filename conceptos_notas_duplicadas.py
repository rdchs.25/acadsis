# -*- coding: utf-8 -*-
from Cursos.models import PeriodoCurso
from Matriculas.models import MatriculaCursos
from Notas.models import Nota, NotasAlumno

# periodo_id = 1459
periodo_id = 12

# periodo_cursos = PeriodoCurso.objects.filter(id=periodo_id)
periodo_cursos = PeriodoCurso.objects.filter(Periodo__id=periodo_id)
for per in periodo_cursos:
    notas = Nota.objects.filter(PeriodoCurso=per).order_by('id')
    mats = MatriculaCursos.objects.filter(PeriodoCurso=per,
                                          Estado=True,
                                          Convalidado=False).order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                                      'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                                      'MatriculaCiclo__Alumno__Nombres')
    for mat in mats:
        ident1 = []
        if notas.count() != 0:
            for cal1 in notas:
                if cal1.Nota in ident1:
                    pass
                else:
                    if cal1.Nivel == "0":
                        ultima_nota = cal1.Nota
                        subnotas1 = Nota.objects.filter(NotaPadre=cal1)

                        nota = NotasAlumno.objects.filter(Nota=cal1, MatriculaCursos=mat)
                        if nota.count() > 1:
                            print "sub1: ", nota.count(), " -- ", nota[1].id, " ", nota

                    else:
                        subnotas2 = Nota.objects.filter(NotaPadre=cal1)
                        if subnotas2.count() == 0:
                            nota = NotasAlumno.objects.filter(Nota=cal1, MatriculaCursos=mat)
                            if nota.count() > 1:
                                print "sub2:", nota.count(), " // ", nota[1].id, " ", nota
                            # print "****"
