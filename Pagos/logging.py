from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_unicode


def log_addition(id_user, object, message):
    """
    Log that an object has been successfully added.

    The default implementation creates an admin LogEntry object.
    """

    LogEntry.objects.log_action(
        user_id=id_user,
        content_type_id=ContentType.objects.get_for_model(object).pk,
        object_id=object.pk,
        object_repr=force_unicode(object),
        action_flag=ADDITION,
        change_message=message
    )


def log_change(id_user, object, message):
    """
    Log that an object has been successfully changed.

    The default implementation creates an admin LogEntry object.
    """
    LogEntry.objects.log_action(
        user_id=id_user,
        content_type_id=ContentType.objects.get_for_model(object).pk,
        object_id=object.pk,
        object_repr=force_unicode(object),
        action_flag=CHANGE,
        change_message=message
    )


def log_deletion(id_user, object, message):
    """
    Log that an object has been successfully deleted. Note that since the
    object is deleted, it might no longer be safe to call *any* methods
    on the object, hence this method getting object_repr.

    The default implementation creates an admin LogEntry object.
    """
    LogEntry.objects.log_action(
        user_id=id_user,
        content_type_id=ContentType.objects.get_for_model(object).pk,
        object_id=object.pk,
        object_repr=force_unicode(object),
        action_flag=DELETION,
        change_message=message
    )
