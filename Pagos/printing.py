# -*- coding: utf-8 -*-

from decimal import Decimal

from django import forms
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from reportlab.lib.units import cm
# imports reportlab
from reportlab.pdfgen import canvas

from Configuraciones.models import TipoComprobante
from Pagos.models import DetallePago, Comprobante
# from Matriculas.reportes import ArchivoExcel
from funciones import response_excel

REPORTE_CHOICES = (
    ('Excel', 'Excel'),
    ('PDF', 'PDF'),
)


# metodo para imprimir comprobantes
def imprimir_comprobantes(request, id_comp='', id_comps=[], serie='', corre_inicio='', corre_fin=''):
    # Creamos el archivo pdf
    response = HttpResponse(content_type='application/pdf')
    p = canvas.Canvas(response, pagesize=(13 * cm, 14 * cm))

    comps = []

    if id_comps != []:
        for id_comp in id_comps:
            try:
                comp = Comprobante.objects.get(id=id_comp)
                detalle = DetallePago.objects.filter(Comprobante=comp)
                fechapago = DetallePago.objects.filter(Comprobante=comp)[0]
                comps.append([comp, detalle, fechapago])
            except (DetallePago.DoesNotExist, Comprobante.DoesNotExist):
                raise Http404, "El Comprobante no se pudo generar, idcomp %s" % id_comp
    else:
        if serie != '' and corre_inicio != '' and corre_fin != '':
            for correlativo in range(corre_inicio, corre_fin + 1):
                try:
                    tipo = TipoComprobante.objects.get(Serie=serie)
                    comp = Comprobante.objects.get(TipoComprobante=tipo,
                                                   Correlativo=str(correlativo).rjust(7).replace(" ", "0"))
                    detalle = DetallePago.objects.filter(Comprobante=comp)
                    fechapago = DetallePago.objects.filter(Comprobante=comp)[0]
                    comps.append([comp, detalle, fechapago])
                except (DetallePago.DoesNotExist, Comprobante.DoesNotExist):
                    raise Http404, "El Comprobante no se pudo generar,Correlativo %s - %s" % (serie, correlativo)
        else:
            try:
                comp = Comprobante.objects.get(id=id_comp)
                detalle = DetallePago.objects.filter(Comprobante=comp)
                fechapago = DetallePago.objects.filter(Comprobante=comp)[0]
                comps.append([comp, detalle, fechapago])
            except (DetallePago.DoesNotExist, Comprobante.DoesNotExist):
                raise Http404, "El Comprobante no se pudo generar, idcomp %s" % id_comp

    n_comps = len(comps)
    i = 1
    for c in comps:
        comp = c[0]
        detalle = c[1]
        fechapago = c[2]

        corre = str(comp.TipoComprobante.Serie) + " - " + str(comp.Correlativo)
        corre_name = str(comp.TipoComprobante.Serie) + str(comp.Correlativo)

        if i == 1:
            comp_inicio = corre_name
            comp_fin = corre_name
        elif i == n_comps:
            comp_fin = corre_name
        i += 1

        fechacobro = fechapago.FechaPago
        dia = str(fechacobro.day).rjust(2).replace(" ", "0")
        mes = str(fechacobro.month).rjust(2).replace(" ", "0")
        anio = str(fechacobro.year)
        alumno = detalle[0]

        p.setFont("Helvetica-Bold", 10)
        p.drawString(120, 388, corre)
        p.drawString(40, 315, str(alumno.PagoAlumno.MatriculaCiclo.Alumno))
        p.drawString(40, 300, str(alumno.PagoAlumno.MatriculaCiclo.Alumno.Carrera))
        p.drawString(220, 310, dia)
        p.drawString(240, 310, mes)
        p.drawString(260, 310, anio)
        p.drawString(55, 285, alumno.PagoAlumno.MatriculaCiclo.Alumno.Direccion)
        p.drawString(240, 290, alumno.PagoAlumno.MatriculaCiclo.Alumno.Codigo)

        i = 242
        total = Decimal("0.00")
        matricu = 0

        for det in detalle:
            if det.Mora is not True:
                desc = det.PagoAlumno.ConceptoPago.Descripcion.lower()

                if desc.find('matricula') == -1:
                    if desc.find(u'matrícula') == -1:
                        print ("entro aqui")
                        p.drawString(60, i, det.PagoAlumno.ConceptoPago.Descripcion)
                    else:
                        p.drawString(60, i, 'MATRICULA SEMESTRE %s' % det.PagoAlumno.ConceptoPago.Periodo)
                        matricu = 1
                else:
                    p.drawString(60, i, 'MATRICULA SEMESTRE %s' % det.PagoAlumno.ConceptoPago.Periodo)
                    matricu = 1

                # imprimir descuento para los que tienen categoria
                normal = det.PagoAlumno.ConceptoPago.MontoPago
                if matricu == 1:
                    resta = 0
                else:
                    if det.PagoAlumno.IgnorarCategoria is True:
                        resta = 0
                    else:
                        if desc.find('pension') != -1 or desc.find(u'pensión') != -1:
                            print ("es pension")
                            descuento = det.PagoAlumno.MatriculaCiclo.Categoria.Pago
                            resta = normal - descuento
                        else:
                            print ("no es matricula entra aqui")
                            resta = 0

                if resta >= Decimal("0.00") and desc.find('pension') != -1 and det.PagoAlumno.IgnorarCategoria is False:
                    p.drawString(60, i - 22, det.PagoAlumno.MatriculaCiclo.Categoria.Categoria[:11])
                elif resta >= Decimal("0.00") and desc.find(
                        u'pensión') != -1 and det.PagoAlumno.IgnorarCategoria is False:
                    p.drawString(60, i - 22, det.PagoAlumno.MatriculaCiclo.Categoria.Categoria[:11])
                elif resta <= Decimal("0.00") and desc.find(
                        'pension') != -1 and det.PagoAlumno.IgnorarCategoria is False:
                    p.drawString(60, i - 22, 'Creditos adicionales')
                elif resta <= Decimal("0.00") and desc.find(
                        u'pensión') != -1 and det.PagoAlumno.IgnorarCategoria is False:
                    p.drawString(60, i - 22, 'Creditos adicionales')

                # p.drawString(222,i,str(det.PagoAlumno.ConceptoPago.MontoPago))
                # p.drawString(254,i,str(det.PagoAlumno.ConceptoPago.MontoPago))
                p.drawString(222, i, str(det.MontoDeuda + resta))
                p.drawString(254, i, str(det.MontoDeuda + resta))

                if desc.find('pension') != -1 or desc.find(u'pensión') != -1:
                    if det.PagoAlumno.IgnorarCategoria is False:
                        if resta >= Decimal("0.00"):
                            p.drawString(222, i - 22, '-' + str(resta))
                            p.drawString(254, i - 22, '-' + str(resta))
                        else:
                            p.drawString(222, i - 22, '+' + str(resta * -1))
                            p.drawString(254, i - 22, '+' + str(resta * -1))

                if resta >= Decimal("0.00") and desc.find('pension') != -1:
                    i = i - 44
                elif resta >= Decimal("0.00") and desc.find(u'pensión') != -1:
                    i = i - 44
                else:
                    i = i - 22

                if det.MontoDeuda != det.MontoCancelado:
                    if det.MontoCancelado == Decimal('0.00'):
                        total = total + det.MontoDeuda
                    else:
                        total = total + det.MontoCancelado
                else:
                    total = total + det.MontoDeuda
            else:
                desc = "Mora " + det.PagoAlumno.ConceptoPago.Descripcion
                p.drawString(60, i, desc)
                p.drawString(222, i, str(det.MontoCancelado))
                p.drawString(254, i, str(det.MontoCancelado))
                total = total + det.MontoCancelado
                i = i - 22

            p.drawString(253, 20, str(total))

        p.showPage()
        p.save()

    archivo = str(comp_inicio) + '_' + str(comp_fin)
    nombre_archivo = "attachment; filename=%s.pdf" % archivo
    response['Content-Disposition'] = nombre_archivo

    return response


from django.forms.utils import ErrorList


class RangoComprobantesForm(forms.Form):
    TipoComprobante = forms.ModelChoiceField(queryset=TipoComprobante.objects.filter(Estado=True),
                                             widget=forms.Select(), required=True, label="Comprobante")
    Inicio = forms.CharField(max_length=7, min_length=7, label="Inicio", required=True)
    Fin = forms.CharField(max_length=7, min_length=7, label="Fin", required=True)
    TipoReporte = forms.ChoiceField(choices=REPORTE_CHOICES, required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        comp = cleaned_data.get("TipoComprobante")
        inicio = cleaned_data.get("Inicio")
        fin = cleaned_data.get("Fin")
        tipo_reporte = cleaned_data.get("TipoReporte")

        if comp and inicio and fin and tipo_reporte:
            # Only do something if both fields are valid so far.
            existe_inicio = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=inicio)
            existe_fin = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=fin)

            if existe_inicio.count() == 0:
                msg = u"El Correlativo %s no existe." % inicio
                self._errors["Inicio"] = ErrorList([msg])
                del cleaned_data["Inicio"]

            elif existe_fin.count() == 0:
                msg = u"El Correlativo %s no existe." % fin
                self._errors["Fin"] = ErrorList([msg])
                del cleaned_data["Fin"]

            elif existe_inicio.count() != 0 and existe_fin.count() != 0:
                resta = int(fin) - int(inicio)
                if resta < 0:
                    msg = u"El Correlativo Inicio debe ser menor que el Correlativo Fin."
                    self._errors["Inicio"] = ErrorList([msg])
                    del cleaned_data["Inicio"]
                else:
                    for i in range(int(inicio), int(fin) + 1):
                        corre = str(i).rjust(7).replace(" ", "0")
                        existe = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=corre)
                        existe_detalle = DetallePago.objects.filter(Comprobante__Correlativo=corre)

                        if existe.count() == 0:
                            msg = u"El Correlativo intermedio %s no existe,cambie el rango." % corre
                            self._errors["Inicio"] = ErrorList([msg])
                            del cleaned_data["Inicio"]
                            break

                        if existe_detalle.count() == 0:
                            msg = u"El Correlativo intermedio %s no posee detalle de pago,cambie el rango." % corre
                            self._errors["Inicio"] = ErrorList([msg])
                            del cleaned_data["Inicio"]
                            break
        # Always return the full collection of cleaned data.
        return cleaned_data


@csrf_protect
def print_comprobantes(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = RangoComprobantesForm(request.POST)
            if form.is_valid():
                tipo = form.cleaned_data['TipoComprobante']
                inicio = form.cleaned_data['Inicio']
                fin = form.cleaned_data['Fin']
                tipo_reporte = form.cleaned_data['TipoReporte']
                corre_inicio = int(inicio)
                corre_fin = int(fin)

                if tipo_reporte == 'PDF':
                    return imprimir_comprobantes(request, id_comp='', id_comps=[], serie=tipo.Serie,
                                                 corre_inicio=corre_inicio, corre_fin=corre_fin)
                elif tipo_reporte == 'Excel':
                    i = 1
                    comprobantes = []
                    for correlativo in range(corre_inicio, corre_fin + 1):
                        idcomp = Comprobante.objects.get(TipoComprobante=tipo,
                                                         Correlativo=str(correlativo).rjust(7).replace(" ", "0"))

                        try:
                            detalle = DetallePago.objects.get(Comprobante__TipoComprobante=tipo,
                                                              Comprobante__Correlativo=str(correlativo).rjust(
                                                                  7).replace(" ", "0"))

                            tipo_comprobante = detalle.Comprobante.TipoComprobante.Tipo
                            comprobante = "%s - %s" % (
                                detalle.Comprobante.TipoComprobante.Serie.Numero, detalle.Comprobante.Correlativo)
                            lugar_pago = detalle.Comprobante.LugarPago
                            monto_deuda = detalle.MontoDeuda
                            monto_cancelado = detalle.MontoCancelado
                            pago_restante = detalle.PagoRestante
                            fecha_pago = detalle.FechaPago
                            if detalle.Mora == True:
                                mora = 'Si'
                            else:
                                mora = 'No'
                            if detalle.Activado == True:
                                activo = 'Si'
                            else:
                                activo = 'No'

                            comprobantes.append(
                                [tipo_comprobante, comprobante, lugar_pago, monto_deuda, monto_cancelado, pago_restante,
                                 fecha_pago, mora, activo])
                            i += 1
                        except DetallePago.DoesNotExist:
                            pass
                    headers = ['Comprobante', 'Correlativo', 'Lugar Pago', 'Monto Deuda', 'Monto Cancelado', 'Debe',
                               'Fecha Pago', 'Es mora', 'Activo']
                    titulo = 'Rango de Comprobantes de pago'
                    return response_excel(titulo=titulo, heads=headers, registros=comprobantes,
                                          nombre_archivo='rango_comprobantes')
        else:
            form = RangoComprobantesForm(initial={'TipoReporte': 'PDF'})

        return render_to_response("Pagos/Comprobante/form_print_comprobantes.html",
                                  {"form": form, "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='admin/'>Inicio</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
