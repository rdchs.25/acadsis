# -*- coding: utf-8 -*-
import csv
import datetime
from StringIO import StringIO
from copy import deepcopy
from decimal import Decimal
from itertools import ifilter
from operator import itemgetter
from zipfile import ZipFile

from django import forms
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.encoding import smart_str
from django.views.decorators.csrf import csrf_protect

from Alumnos.widgets import DateTimeWidget
from Pagos.cuentas_asientos import *
from Pagos.models import DetallePago


def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text


rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u',
       'Ñ': 'N', 'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U',
       'ä': 'a', 'ë': 'e', 'ï': 'i', 'ö': 'o', 'ü': 'u',
       'Ä': 'A', 'Ë': 'E', 'Ï': 'I', 'Ö': 'O', 'Ü': 'U',
       '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ', '!': ' ', '¿': ' ',
       '?': ' ', '^': ' ', '"': ' ', '-': ' ', '_': ' '}


def convertir_decimal(v):
    if (v % 1) != 0:
        d = 1 - (v % 1)
    else:
        d = 0
    r = v + (d * 2)
    return r


class GenerarSiscontForm(forms.Form):
    Correlativo_inicio = forms.DecimalField(required=True, max_digits=7, decimal_places=0, label="Boleta de Inicio")
    Correlativo_fin = forms.DecimalField(required=True, max_digits=7, decimal_places=0, label="Boleta de Fin")
    Fecha_generacion = forms.DateField(widget=DateTimeWidget, required=True, label="Fecha de Pago/Generación")
    Voucher = forms.IntegerField(required=True, min_value=1, initial=1, label="Comenzar desde el voucher Nº")

    class Media:
        js = ("custom/js/jquery.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {"all": ("custom/calendario_admin/src/css/jscal2.css",
                       "custom/calendario_admin/src/css/border-radius.css",
                       "custom/calendario_admin/src/css/reduce-spacing.css",
                       "custom/calendario_admin/src/css/steel/steel.css",)
               }


@csrf_protect
def generar_siscont(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = GenerarSiscontForm(request.POST)
            if form.is_valid():
                comprobante_comienzo = int(form.cleaned_data['Correlativo_inicio'])
                comprobante_final = int(form.cleaned_data['Correlativo_fin'])
                fechapago = form.cleaned_data['Fecha_generacion']

                total_comprobantes = (comprobante_final - comprobante_comienzo) + 1
                # Configuraciones Generales
                correlativo = comprobante_comienzo
                fecha_comprobante = fechapago.strftime('%d/%m/%y')  # fecha_comprobante
                tipo_comprobante = "02"  # 02: Ventas
                tipo_documento = "03"  # 03: Boletas
                moneda = "S"  # Nuevos Soles
                tipo_cambio = str("02.98").ljust(10).replace(" ", "0")

                # fecha_vencimiento = " ".rjust(8)
                centro_costo = " ".rjust(10)
                flujo_efectivo = " ".rjust(4)
                presupuesto = " ".rjust(10)

                ruc = " ".rjust(11).replace(" ", "0")
                tipo = " ".rjust(1).replace(" ", "0")
                tipo_doc_identidad = " ".rjust(1)
                medio_pagos = " ".rjust(3)

                lista = []
                reporte = []  # para emitir un reporte con las boletas que no se migren
                i = 0

                for i in range(total_comprobantes):
                    num_boleta = str(correlativo + i).rjust(7).replace(" ", "0")
                    # correlativo = correlativo + 1
                    # Pagos sin descuento
                    # lista_pagos1 = []        # Pagos Sin Descuento
                    lista_pagos2 = []  # Pagos Con Descuento

                    pagos_condesc1 = DetallePago.objects.filter(Comprobante__Correlativo=num_boleta,
                                                                PagoAlumno__ConceptoPago__Descripcion__icontains="Pensión",
                                                                Comprobante__TipoComprobante__id=2,
                                                                Mora=False).exclude(
                        PagoAlumno__MatriculaCiclo__Categoria__Categoria__icontains="Beca").order_by('FechaPago')

                    mora_condesc1 = DetallePago.objects.filter(Comprobante__Correlativo=num_boleta,
                                                               PagoAlumno__ConceptoPago__Descripcion__icontains="Pensión",
                                                               Comprobante__TipoComprobante__id=2,
                                                               Mora=True).exclude(
                        PagoAlumno__MatriculaCiclo__Categoria__Categoria__icontains="Beca").order_by('FechaPago')

                    pagos_condesc2 = DetallePago.objects.filter(Comprobante__Correlativo=num_boleta,
                                                                PagoAlumno__ConceptoPago__Descripcion__istartswith="Matricula",
                                                                Comprobante__TipoComprobante__id=2,
                                                                Mora=False).exclude(
                        PagoAlumno__MatriculaCiclo__Categoria__Categoria__icontains="INTEGRAL").order_by('FechaPago')

                    pagos_anr = DetallePago.objects.filter(Comprobante__Correlativo=num_boleta,
                                                           PagoAlumno__ConceptoPago__Descripcion__icontains="Valorado",
                                                           Comprobante__TipoComprobante__id=2,
                                                           Mora=False).order_by('FechaPago')

                    lista_pagos2.extend([pagos_condesc1, mora_condesc1, pagos_condesc2, pagos_anr])

                    # Validar Boletas
                    f = len(lista_pagos2[0]) + len(lista_pagos2[1]) + len(lista_pagos2[2]) + len(lista_pagos2[3])
                    # Si la suma da 0 significa que ambas listas están vacías y que la boleta no se migrará
                    if f == 0:
                        estado = "ERROR"
                        motivo = u'No existe el comprobante'
                        # detalle_pago = DetallePago.objects.filter(Comprobante__Correlativo = num_boleta)
                        # if detalle_pago.count() == 0:
                        # motivo = u'No existe el comprobante'
                        # else:
                        # motivo = u'Desconocido'
                        # f_beca = detalle_pago[0].PagoAlumno.MatriculaCiclo.categoria().upper().find("Beca")
                        # if f_beca != -1:
                        # motivo = u'Alumno becado'
                    else:
                        estado = "OK"
                        motivo = ""
                    contenido = {"boleta": num_boleta, "estado": unicode(estado), "motivo": motivo}
                    reporte.append(contenido)

                    for index, pagos_condesc in enumerate(lista_pagos2):
                        for pago in pagos_condesc:
                            try:
                                # Inicializar valores
                                tipo_libro = " ".rjust(1)
                                monto_neto_1 = " ".rjust(12).replace(" ", "0")
                                monto_neto_2 = " ".rjust(12).replace(" ", "0")
                                monto_neto_3 = " ".rjust(12).replace(" ", "0")
                                monto_neto_4 = " ".rjust(12).replace(" ", "0")
                                monto_igv = " ".rjust(12).replace(" ", "0")
                                #
                                diferencia = Decimal(str(pago.PagoAlumno.ConceptoPago.MontoPago)) - Decimal(
                                    str(pago.PagoAlumno.MatriculaCiclo.Categoria.Pago))
                                # En caso la diferencia sea menor a 0 significa que el monto a pagar supera al del concepto.
                                # Lo que hago es ajustar los montos a tres cuentas contables (eliminando la 74110) y también pongo a 0 la columna de "otros_conceptos".
                                if diferencia < 0:
                                    # Matricula
                                    if (index == 2) or (index == 3):
                                        monto_pago = str(pago.PagoAlumno.ConceptoPago.MontoPago).rjust(12).replace(" ",
                                                                                                                   "0")
                                    else:
                                        monto_pago = str(pago.PagoAlumno.MatriculaCiclo.Categoria.Pago).rjust(
                                            12).replace(" ", "0")
                                    beca = "".rjust(12).replace(" ", "0")
                                    pension = monto_pago
                                else:
                                    monto_pago = str(pago.PagoAlumno.ConceptoPago.MontoPago).rjust(12).replace(" ", "0")
                                    beca = str(diferencia).rjust(12).replace(" ", "0")
                                    pension = str(pago.PagoAlumno.MatriculaCiclo.Categoria.Pago).rjust(12).replace(" ",
                                                                                                                   "0")

                                # Le agrego un 0 adelante de la serie de las boletas para que contenga 3 ceros adelante del número,
                                # así me evito tocar la Base de Datos (por ahora)
                                nd = "%s-%s".replace(" ", "").ljust(20)[0:20] % (
                                pago.Comprobante.TipoComprobante.Serie.Numero, pago.Comprobante.Correlativo)
                                num_documento = str("0" + nd).ljust(20)[0:20]
                                num_boleta = str(pago.Comprobante.Correlativo)

                                try:
                                    fecha_comprobante = datetime.date.strftime(pago.Comprobante.FechaEmision,
                                                                               '%d/%m/%y')
                                except:
                                    print "-"
                                fecha_documento = fecha_comprobante
                                fecha_vencimiento = fecha_comprobante

                                ape1 = smart_str(pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoPaterno)
                                ape2 = smart_str(pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoMaterno)
                                nomb = smart_str(pago.PagoAlumno.MatriculaCiclo.Alumno.Nombres)
                                apellido_paterno = replace_all(ape1, rep)
                                apellido_materno = replace_all(ape2, rep)
                                nombre = replace_all(nomb, rep)

                                razon_social = str(
                                    str(apellido_paterno) + " " + str(apellido_materno) + " " + str(nombre)).ljust(40)[
                                               0:40]
                                datos_alumno = str(apellido_paterno).ljust(20)[0:20] + str(apellido_materno).ljust(20)[
                                                                                       0:20] + str(nombre).ljust(20)[
                                                                                               0:20]
                                codigo_cliente = str(pago.PagoAlumno.MatriculaCiclo.Alumno.Codigo).rjust(11)

                                g = smart_str(pago.PagoAlumno.ConceptoPago.Descripcion)
                                glosa = replace_all(g, rep).ljust(30)[0:30]
                                glosa_nula = glosa

                                alumno_carrera = pago.PagoAlumno.MatriculaCiclo.Alumno.Carrera.Codigo
                                alumno_categoria = pago.PagoAlumno.MatriculaCiclo.Categoria.Categoria

                                otros_conceptos = ""

                                # Parte para completar los montos
                                pensiones = ["1era", "2da", "3era", "4ta", "5ta"]
                                # if (glosa.split(" ")[0] in pensiones) and (cuenta_contable[:6] == "121210"):
                                b_monto_nuevo = False
                                if glosa.split(" ")[0] in pensiones:
                                    # print glosa, " // ", Decimal(monto_cuenta), "*", num_documento
                                    b_monto = Decimal(pension)
                                    b_residuo = b_monto % 50

                                    # if b_monto > Decimal("350.00"):
                                    # print glosa, " // ", Decimal(monto_cuenta), "*", num_documento

                                    if (b_residuo != Decimal("0.00")) and (b_monto <= Decimal("350.00")):
                                        ramen = Decimal("50.00") - b_residuo
                                        b_monto_nuevo = b_monto + ramen
                                        # print glosa, " // ", b_monto, "*", b_monto_nuevo, "--", num_documento

                                if alumno_categoria[10:11] in ["A", "B", "C", "D", "E", "F"] and index == 0:
                                    if b_monto_nuevo:
                                        cuentas = deepcopy(ifilter(
                                            lambda x: x['carrera'] == alumno_carrera and x['configuracion'][
                                                'tipo'] == 'pension' and x['configuracion']['monto'] == int(
                                                b_monto_nuevo), dict_pensiones).next())
                                    # cuentas = deepcopy(ifilter(lambda x: x['carrera'] == alumno_carrera and x['configuracion']['tipo']=='pension' and x['configuracion']['categoria'] == alumno_categoria[10:11], dict_pensiones).next())
                                    # if b_monto_nuevo:
                                    ## obtener el 121210
                                    # temp_cuenta = False
                                    # for temp_cuenta in cuentas["cuentas"]:
                                    # if temp_cuenta[:6] == '121210':
                                    # break
                                    # b_cuentas = deepcopy(ifilter(lambda x: x['carrera'] == alumno_carrera and x['configuracion']['tipo']=='pension' and x['configuracion']['monto'] == int(b_monto_nuevo) , dict_pensiones).next())
                                    # print cuentas
                                    # b_cuenta_contable = False
                                    # for b_cuenta_contable in b_cuentas["cuentas"]:
                                    # if b_cuenta_contable[:6] == '121210':
                                    # break
                                    # cuentas["cuentas"][b_cuenta_contable] = cuentas["cuentas"].pop(temp_cuenta)
                                    # print cuentas
                                    # print "-------"
                                    else:
                                        cuentas = deepcopy(ifilter(
                                            lambda x: x['carrera'] == alumno_carrera and x['configuracion'][
                                                'tipo'] == 'pension' and x['configuracion'][
                                                          'categoria'] == alumno_categoria[10:11],
                                            dict_pensiones).next())
                                elif index == 1:
                                    cuentas = deepcopy(ifilter(
                                        lambda x: x['carrera'] == alumno_carrera and x['configuracion'][
                                            'tipo'] == 'mora', dict_pensiones).next())
                                elif index == 2:
                                    cuentas = deepcopy(ifilter(
                                        lambda x: x['carrera'] == alumno_carrera and x['configuracion'][
                                            'tipo'] == 'matricula', dict_pensiones).next())
                                elif index == 3:
                                    cuentas = deepcopy(
                                        ifilter(lambda x: x['configuracion']['tipo'] == 'documentovalorado',
                                                dict_pensiones).next())
                                else:
                                    continue

                                for cuenta_contable in cuentas['cuentas']:
                                    otros_conceptos = ""
                                    debe_haber = cuentas['cuentas'][cuenta_contable]
                                    tipo_libro = " ".rjust(1)
                                    monto_cuenta = monto_pago
                                    glosa = replace_all(g, rep).ljust(30)[0:30]
                                    glosa_nula = glosa
                                    monto_neto_1 = " ".rjust(12).replace(" ", "0")
                                    monto_neto_2 = " ".rjust(12).replace(" ", "0")
                                    monto_neto_3 = " ".rjust(12).replace(" ", "0")
                                    monto_neto_4 = " ".rjust(12).replace(" ", "0")
                                    monto_igv = " ".rjust(12).replace(" ", "0")

                                    if cuenta_contable == '40111':
                                        tipo_libro = "V"
                                        monto_cuenta = str(Decimal('0.00')).rjust(12).replace(" ", "0")
                                        monto_neto_2 = monto_pago
                                        if diferencia < 0:
                                            otros_conceptos = ""
                                        else:
                                            otros_conceptos = "-" + str(convertir_decimal(diferencia)).rjust(
                                                11).replace(" ", "0")
                                        if index == 1:
                                            monto_neto_2 = str(pago.MontoCancelado).rjust(12).replace(" ", "0")
                                            glosa = str("Mora " + glosa).ljust(30)[0:30]
                                            fecha_comprobante = datetime.date.strftime(pago.FechaPago, '%d/%m/%y')
                                            fecha_documento = fecha_comprobante
                                            fecha_vencimiento = fecha_comprobante
                                            otros_conceptos = ""
                                        elif index == 2:
                                            glosa = "MATRICULA SEMESTRE " + str(
                                                pago.PagoAlumno.ConceptoPago.Periodo).ljust(11)[0:11]
                                            otros_conceptos = ""
                                        elif index == 3:
                                            otros_conceptos = ""
                                    elif cuenta_contable[:6] == '121210':
                                        monto_cuenta = pension
                                        if index == 1:
                                            monto_cuenta = str(pago.MontoDeuda).rjust(12).replace(" ", "0")
                                            glosa = str("Mora " + glosa).ljust(30)[0:30]
                                            fecha_comprobante = datetime.date.strftime(pago.FechaPago, '%d/%m/%y')
                                            fecha_documento = fecha_comprobante
                                            fecha_vencimiento = fecha_comprobante
                                            otros_conceptos = ""
                                        elif index == 2:
                                            # para el caso de matrículas"
                                            monto_cuenta = monto_pago
                                            glosa = "MATRICULA SEMESTRE " + str(
                                                pago.PagoAlumno.ConceptoPago.Periodo).ljust(11)[0:11]
                                            otros_conceptos = ""
                                    elif cuenta_contable[:6] == '121211':
                                        monto_cuenta = monto_pago
                                        otros_conceptos = ""
                                    elif cuenta_contable[:4] == '7041':
                                        monto_cuenta = monto_pago
                                        if index == 1:
                                            monto_cuenta = str(pago.MontoCancelado).rjust(12).replace(" ", "0")
                                            glosa = str("Mora " + glosa).ljust(30)[0:30]
                                            fecha_comprobante = datetime.date.strftime(pago.FechaPago, '%d/%m/%y')
                                            fecha_documento = fecha_comprobante
                                            fecha_vencimiento = fecha_comprobante
                                            otros_conceptos = ""
                                        elif index == 2:
                                            # para el caso de matrículas"
                                            glosa = "MATRICULA SEMESTRE " + str(
                                                pago.PagoAlumno.ConceptoPago.Periodo).ljust(11)[0:11]
                                            otros_conceptos = ""
                                        elif index == 3:
                                            otros_conceptos = ""
                                    elif cuenta_contable[:5] == '74110':
                                        if diferencia < 0:
                                            continue
                                        else:
                                            monto_cuenta = beca
                                            glosa = str(alumno_categoria).ljust(30)[0:30]

                                    texto = fecha_comprobante + cuenta_contable.ljust(10) + monto_cuenta + debe_haber + \
                                            moneda + tipo_cambio + tipo_documento + num_documento + fecha_vencimiento + codigo_cliente + \
                                            centro_costo + flujo_efectivo + presupuesto + tipo_libro + \
                                            fecha_documento + \
                                            monto_neto_1 + monto_neto_2 + monto_neto_3 + monto_neto_4 + monto_igv + ruc + tipo + \
                                            razon_social + glosa + \
                                            tipo_doc_identidad + medio_pagos + \
                                            datos_alumno + otros_conceptos

                                    contenido = {"boleta": num_boleta, "cadena": texto}
                                    lista.append(contenido)
                            except:
                                f = ""
                                # num_boleta = str(correlativo + i).rjust(7).replace(" ", "0")
                                # print num_boleta

                # Creamos un archivo de texto plano
                nombre_archivo = "ayu_mayu" + ".zip"
                response = HttpResponse(content_type='application/zip')
                # response = HttpResponse(content_type='text/plain')
                response['Content-Disposition'] = 'attachment; filename=%s' % nombre_archivo

                # Ordeno los items de la lista
                lista_ordenada = sorted(lista, key=itemgetter('boleta'))
                nueva_lista = []

                voucher = form.cleaned_data['Voucher']
                c = 0
                comparar = []
                for o in lista_ordenada:
                    c += 1
                    linea = o['cadena']
                    entero = str(linea)[51:56]
                    comparar.append(entero)
                    if c > 1:
                        anterior = comparar[-2]
                        if anterior != entero:
                            voucher += 1

                    num_comprobante = str(voucher).rjust(4).replace(" ", "0")
                    n_linea = tipo_comprobante + num_comprobante + str(linea)

                    nueva_lista.append(n_linea)

                negativos = []
                fechas = []
                mes_seleccionado = fechapago.strftime('%d/%m/%y')[3:5]
                # Genero en memoria el archivo MSISCONT
                in_memory = StringIO()
                writer = csv.writer(in_memory)
                for l in nueva_lista:
                    writer.writerow([l])
                    # En esta sección no debería haber negativos, así que busco por si existe alguno
                    if l[24:33].find("-") > 0:
                        negativos.append(l)
                    # Tampoco debería haber un mes que no corresponde al seleccionado
                    meses_registro = l[9:11], l[117:119]
                    if meses_registro[0] == meses_registro[1]:
                        if meses_registro[0] != mes_seleccionado:
                            fechas.append(l)
                    else:
                        fechas.append(l)

                in_memory.seek(0)
                file_txt = in_memory.read()
                in_memory.close()

                reporte = sorted(reporte, key=itemgetter('boleta'))
                # Genero en memoria el reporte
                rango_boletas = str(comprobante_comienzo) + " - " + str(comprobante_final)
                in_memory = StringIO()
                writer = csv.writer(in_memory)

                cadena = "Rango de Boletas: " + rango_boletas
                writer.writerow([smart_str(cadena)])
                writer.writerow('')
                cadena = "Boleta".ljust(7) + " | " + "Estado" + " | " + "Motivo".ljust(50) + " | "
                writer.writerow([smart_str(cadena)])
                for l in reporte:
                    cadena = l['boleta'] + " | " + l['estado'].ljust(6) + " | " + l['motivo'].ljust(50) + " | "
                    writer.writerow([smart_str(cadena)])

                if len(negativos) > 0:
                    writer.writerow('')
                    writer.writerow(["A verificar pago de pensión"])
                    for l in negativos:
                        writer.writerow([smart_str(l)])
                if len(fechas) > 0:
                    writer.writerow('')
                    writer.writerow(["A verificar fechas"])
                    for l in fechas:
                        writer.writerow([smart_str(l)])
                in_memory.seek(0)
                reporte = in_memory.read()
                in_memory.close()

                # Crear ZIP
                in_memory = StringIO()
                zip = ZipFile(in_memory, 'a')
                zip.writestr('MSISCONT.txt', file_txt)
                zip.writestr('reporte.txt', reporte)
                for file in zip.filelist:
                    file.create_system = 0
                zip.close()
                in_memory.seek(0)
                response.write(in_memory.read())
                in_memory.close()
                return response

        else:
            form = GenerarSiscontForm()
        return render_to_response('Pagos/form_siscont.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponse("<h2>Permiso Denegado</h2><a href='javascript:window.close()'>Cerrar</a>")
