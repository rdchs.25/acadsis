# -*- coding: utf-8 -*-
from decimal import Decimal

from django import forms
from django.forms.utils import ErrorList

from Alumnos.models import Alumno
from Alumnos.widgets import DateTimeWidget
from Configuraciones.models import TipoComprobante
from Matriculas.models import MatriculaCiclo
from Pagos.models import ConceptoPago, Comprobante, DetallePago, PagoAlumno
from Pagos.models import LUGAR_CHOICES
from Periodos.models import Periodo


def info_empresa():
    cod_sucursal = "305"
    moneda = "0"
    # cuenta_recaudadora = "1853001", cuenta antigua
    cuenta_recaudadora = "1916966"
    comb = cod_sucursal + moneda + cuenta_recaudadora
    return comb


class AsignarConceptosGrupoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True,
                                     label='Periodo')
    ConceptoPago = forms.ModelChoiceField(queryset=ConceptoPago.objects.filter(Unico=True), widget=forms.Select(),
                                          required=True, label='Concepto')
    Archivo = forms.FileField(help_text='Cargar archivo en formato csv, con los codigos de los estudiantes')

    def clean(self):
        cleaned_data = self.cleaned_data
        periodo = cleaned_data.get("Periodo")
        conceptopago = cleaned_data.get("ConceptoPago")
        archivo = cleaned_data.get("Archivo")

        if periodo and archivo and conceptopago:
            # if archivo.content_type == 'text/csv':
            #   msg = u"Debe ingresar un archivo csv"
            #   self._errors["Archivo"] = ErrorList([msg])
            #   del cleaned_data["Archivo"]
            # else:
            for line in archivo:
                line = line.strip('\n')
                try:
                    MatriculaCiclo.objects.get(Periodo=periodo, Alumno__Codigo=line)
                except MatriculaCiclo.DoesNotExist:
                    msg = "El estudiante con codigo %s, no pertenece al semestre elegido, verificar por favor" % line
                    self._errors["Archivo"] = ErrorList([msg])
                    del cleaned_data["Archivo"]
        return cleaned_data

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Pagos/frm_asignarconceptos_grupo.js"
              )


class GenerarBoletasBancoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True, label="Año")
    ConceptoPago = forms.ModelChoiceField(queryset=ConceptoPago.objects.filter(Unico=True), widget=forms.Select(),
                                          required=True, label="Concepto de Pago")
    Correlativo = forms.CharField(min_length=7, max_length=7, required=False, label="Correlativo de Inicio")
    FechaPago = forms.DateField(widget=DateTimeWidget, required=False, label="Fecha de Generación")

    def clean(self):
        cleaned_data = self.cleaned_data
        corre = cleaned_data.get("Correlativo")
        fechapago = cleaned_data.get("FechaPago")

        if corre == "":
            msg = u"Cuando el pago es en banco debes ingresar un correlativo de inicio de generación de boletas."
            self._errors["Correlativo"] = ErrorList([msg])
            del cleaned_data["Correlativo"]
        else:
            # Only do something if both fields are valid so far.
            existe = Comprobante.objects.filter(TipoComprobante__PagoBanco=True, Correlativo=corre)
            if existe.count() != 0:
                msg = u"El Correlativo ya existe."
                self._errors["Correlativo"] = ErrorList([msg])
                del cleaned_data["Correlativo"]

        if fechapago is None:
            msg = u"Cuando el pago es en banco debes ingresar una fecha de generación de boletas."
            self._errors["FechaPago"] = ErrorList([msg])
            del cleaned_data["FechaPago"]

        # Always return the full collection of cleaned data.
        return cleaned_data

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Pagos/frm_generarboletas_banco.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class IndexDescuentoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class IndexPagoAlumnoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Periodo')


class DetallePagoForm(forms.Form):
    TipoComprobante = forms.ModelChoiceField(queryset=TipoComprobante.objects.filter(Estado=True),
                                             widget=forms.Select(), required=True, label="Comprobante")
    Correlativo = forms.CharField(max_length=7, min_length=7, label="Nº Correlativo", required=True)
    FechaPago = forms.DateField(widget=DateTimeWidget(), required=True, label="Fecha Pago")
    LugarPago = forms.ChoiceField(choices=LUGAR_CHOICES, required=True)
    MontoPagar = forms.DecimalField(max_digits=6, decimal_places=2, label="Monto Pagar", required=True)
    selected_tipo = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    selected_corre = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    selected_detalle = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        comp = cleaned_data.get("TipoComprobante")
        corre = cleaned_data.get("Correlativo")
        lugar_pago = cleaned_data.get("LugarPago")
        monto_pagar = cleaned_data.get("MontoPagar")
        fecha_pago = cleaned_data.get("FechaPago")
        selected_tipo = cleaned_data.get("selected_tipo")
        selected_corre = cleaned_data.get("selected_corre")
        selected_detalle = cleaned_data.get("selected_detalle")

        if comp and corre and selected_tipo and selected_corre and selected_detalle and lugar_pago and monto_pagar:
            # Only do something if both fields are valid so far.
            existe = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=corre)
            existe_original = Comprobante.objects.filter(TipoComprobante__Serie__Numero=selected_tipo,
                                                         Correlativo=selected_corre)

            if existe_original.count() == 0:
                msg = u"Verifique que el Comprobante a editar existe en el sistema."
                self._errors["Correlativo"] = ErrorList([msg])
                del cleaned_data["Correlativo"]
            else:
                if existe.count() != 0:
                    if existe[0] != existe_original[0]:
                        msg = u"El Correlativo ya existe."
                        self._errors["Correlativo"] = ErrorList([msg])
                        del cleaned_data["Correlativo"]

            try:
                detalle = DetallePago.objects.get(id=selected_detalle)

                if detalle.Mora is False:

                    detalle_pagados = DetallePago.objects.filter(PagoAlumno=detalle.PagoAlumno, Mora=False,
                                                                 Activado=True).exclude(id=detalle.id)

                    detalle_moras = DetallePago.objects.filter(PagoAlumno=detalle.PagoAlumno, Mora=True, Activado=True)

                    suma_pagos = Decimal('0.00')

                    reps = {'S/.': '', 'US$': '', '€': '', ' ': ''}
                    monto_total = str(detalle.PagoAlumno.Monto())
                    for i, j in reps.items():
                        monto_total = monto_total.replace(i, j)

                    for d in detalle_pagados:
                        suma_pagos += d.MontoCancelado

                    monto_deuda = Decimal(monto_total) - Decimal(suma_pagos)

                    if Decimal(monto_pagar) > Decimal(monto_deuda):
                        msg = u'El monto a pagar debe ser menor o igual a S/. %s' % str(monto_deuda)
                        self._errors["MontoPagar"] = ErrorList([msg])
                        del cleaned_data["MontoPagar"]

                    vence = detalle.PagoAlumno.ConceptoPago.FechaVencimiento
                    if fecha_pago < vence and detalle_moras.count() != 0:
                        msg = u'Ya existe mora cobrada, la fecha debe ser mayor a %s' % str(vence.strftime("%Y-%m-%d"))
                        self._errors["FechaPago"] = ErrorList([msg])
                        del cleaned_data["FechaPago"]

                    if Decimal(monto_pagar) != Decimal(monto_deuda) and detalle_moras.count() != 0:
                        msg = u'Ya existe mora cobrada, el monto debe ser igual a S./ %s' % str(monto_deuda)
                        self._errors["MontoPagar"] = ErrorList([msg])
                        del cleaned_data["MontoPagar"]

            except ValueError:
                msg = u"Verifique que el Detalle de Pago a editar existe en el sistema."
                self._errors["selected_detalle"] = ErrorList([msg])
                del cleaned_data["selected_detalle"]

        # Always return the full collection of cleaned data.
        return cleaned_data


class ConceptosForm(forms.Form):
    Matriculado = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    ConceptoPago = forms.ModelMultipleChoiceField(queryset=ConceptoPago.objects.all().order_by('Descripcion'),
                                                  required=True)
    IgnorarCategoria = forms.BooleanField(label="¿Ignorar Categoría?", required=False)


class CobrarConceptosForm(forms.Form):
    Matriculado = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    ConceptoPago = forms.ModelMultipleChoiceField(queryset=None, required=True)

    def __init__(self, conceptos, *args, **kwargs):
        super(CobrarConceptosForm, self).__init__(*args, **kwargs)
        self.fields['ConceptoPago'].queryset = conceptos


class FacturarConceptosForm(forms.Form):
    Matriculado = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    TipoComprobante = forms.ModelChoiceField(queryset=TipoComprobante.objects.filter(Estado=True),
                                             widget=forms.Select(), required=True)
    Glosa = forms.CharField(max_length=50, required=False)
    FechaPago = forms.DateField(widget=DateTimeWidget, required=True)
    Correlativo = forms.CharField(min_length=7, max_length=7, required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        comp = cleaned_data.get("TipoComprobante")
        corre = cleaned_data.get("Correlativo")

        if comp and corre:
            # Only do something if both fields are valid so far.
            existe = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=corre)
            if existe.count() != 0:
                msg = u"El Correlativo ya existe."
                self._errors["Correlativo"] = ErrorList([msg])

                del cleaned_data["Correlativo"]

        # Always return the full collection of cleaned data.
        return cleaned_data

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class FacturarMorasForm(forms.Form):
    Matriculado = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    TipoComprobante = forms.ModelChoiceField(queryset=TipoComprobante.objects.filter(Estado=True),
                                             widget=forms.Select(), required=True)
    Glosa = forms.CharField(max_length=50, required=False)
    FechaPago = forms.DateField(widget=DateTimeWidget, required=True)
    Correlativo = forms.CharField(min_length=7, max_length=7, required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        comp = cleaned_data.get("TipoComprobante")
        corre = cleaned_data.get("Correlativo")

        if comp and corre:
            # Only do something if both fields are valid so far.
            existe = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=corre)
            if existe.count() != 0:
                msg = u"El Correlativo ya existe."
                self._errors["Correlativo"] = ErrorList([msg])

                del cleaned_data["Correlativo"]

        # Always return the full collection of cleaned data.
        return cleaned_data

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


# form para el modelo Comprobante
class ComprobanteAdminForm(forms.Form):
    TipoComprobante = forms.ModelChoiceField(queryset=TipoComprobante.objects.filter(Estado=True),
                                             widget=forms.Select(), required=True, label="Comprobante")
    Correlativo = forms.CharField(max_length=7, min_length=7, label="Nº Correlativo", required=True)
    FechaPago = forms.DateField(widget=DateTimeWidget(), required=True, label="Fecha Pago")
    selected_tipo = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    selected_corre = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js")

        css = {"all": ("custom/calendario_admin/src/css/jscal2.css",
                       "custom/calendario_admin/src/css/border-radius.css",
                       "custom/calendario_admin/src/css/reduce-spacing.css",
                       "custom/calendario_admin/src/css/steel/steel.css",)
               }

    def clean(self):
        cleaned_data = self.cleaned_data
        comp = cleaned_data.get("TipoComprobante")
        corre = cleaned_data.get("Correlativo")
        selected_tipo = cleaned_data.get("selected_tipo")
        selected_corre = cleaned_data.get("selected_corre")

        if comp and corre and selected_tipo and selected_corre:
            # Only do something if both fields are valid so far.
            existe = Comprobante.objects.filter(TipoComprobante=comp, Correlativo=corre)
            existe_original = Comprobante.objects.filter(TipoComprobante__Serie__Numero=selected_tipo,
                                                         Correlativo=selected_corre)

            if existe_original.count() == 0:
                msg = u"Verifique que el Comprobante a editar existe en el sistema."
                self._errors["Correlativo"] = ErrorList([msg])
                del cleaned_data["Correlativo"]
            else:
                if existe.count() != 0:
                    if existe[0] != existe_original[0]:
                        msg = u"El Correlativo ya existe."
                        self._errors["Correlativo"] = ErrorList([msg])
                        del cleaned_data["Correlativo"]
        return cleaned_data


# Prorroga
class ProrrogaForm(forms.Form):
    Prorroga = forms.BooleanField(label="¿Prorroga?", required=False)
    FechaProrroga = forms.DateField(widget=DateTimeWidget(), required=False, label="Fecha Prorroga")
    selected_pagoalumno = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        fechaprorroga = cleaned_data.get("FechaProrroga")
        prorroga = cleaned_data.get("Prorroga")

        print (fechaprorroga, prorroga)
        if prorroga and fechaprorroga is None:
            msg = u"Ingrese Fecha Prorroga."
            self._errors["FechaProrroga"] = ErrorList([msg])
            del cleaned_data["FechaProrroga"]
        return cleaned_data


class ExcesoForm(forms.Form):
    Monto = forms.DecimalField(label="Monto de exceso", required=False)
    selected_pagoalumno = forms.CharField(widget=forms.HiddenInput, required=True, label="")

    class Media:
        def __init__(self):
            pass

        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data


class IndexConceptoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all().order_by('-id'), widget=forms.Select(),
                                     required=True, label='Período')


class CobrarMorasForm(forms.Form):
    Matriculado = forms.CharField(widget=forms.HiddenInput, required=True, label="")
    ConceptoPago = forms.ModelMultipleChoiceField(queryset=None, required=True)

    def __init__(self, conceptos, *args, **kwargs):
        super(CobrarMorasForm, self).__init__(*args, **kwargs)
        self.fields['ConceptoPago'].queryset = conceptos


# formulario para generar el archivo al banco
class ArchivoBancoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(
    ), widget=forms.Select(), required=True, label='Período')
    ConceptoPago = forms.ModelMultipleChoiceField(queryset=ConceptoPago.objects.filter(
        PagoBanco=True), required=True, label='Concepto')
    FechaCreacion = forms.DateField(
        widget=DateTimeWidget, required=True, label="Fecha de Generación")

    class Media:
        def __init__(self):
            pass

        js = ("custom/js/jquery.js",
              "custom/js/Pagos/Banco/frm_generar_archivo.js",
              "custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


class ArchivoForm(forms.Form):
    Archivo = forms.FileField()
    TipoComp = forms.ModelChoiceField(queryset=TipoComprobante.objects.filter(
        Estado=True), widget=forms.Select(), required=True, label="Comprobante")
    Correlativo = forms.CharField(
        max_length=7, min_length=7, label="Correlativo Inicio", required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        archivo = cleaned_data.get("Archivo")
        tipo_comp = cleaned_data.get("TipoComp")
        corre_inicio = cleaned_data.get("Correlativo")

        if tipo_comp and corre_inicio and archivo:
            comprobante = Comprobante.objects.filter(
                TipoComprobante=tipo_comp, Correlativo=str(corre_inicio).rjust(7).replace(" ", "0"))
            if comprobante.count() != 0:
                msg = u"El comprobante %s ya existe, ingrese otro " % comprobante[
                    0].Correlativo
                self._errors["Correlativo"] = ErrorList([msg])

                del cleaned_data["Correlativo"]

            if archivo.content_type != 'text/plain':
                msg = u"Debe ingresar un archivo en texto plano"
                self._errors["Archivo"] = ErrorList([msg])
                del cleaned_data["Archivo"]
            else:
                i = 1
                total = Decimal("0.00")
                validacion = True
                for line in archivo:
                    linea = str(line)
                    identificacion = str(linea[:2])
                    if identificacion == "DD" and i != 1:
                        detalle = str(linea[2:13])
                        if detalle != info_empresa():
                            msg = u"La cuenta del registro posee errores - linea %s" % i
                            self._errors["Archivo"] = ErrorList([msg])
                            del cleaned_data["Archivo"]
                            validacion = False
                            break

                        codigo = str(line[20:27])

                        try:
                            alu = Alumno.objects.get(Codigo=codigo)
                        except Alumno.DoesNotExist:
                            msg = u"El codigo del alumno no existe - linea %s" % i
                            self._errors["Archivo"] = ErrorList([msg])
                            del cleaned_data["Archivo"]
                            validacion = False
                            break

                        retorno1_id_pago = int(linea[27:37])

                        try:
                            pago = PagoAlumno.objects.get(id=retorno1_id_pago)
                        except PagoAlumno.DoesNotExist:
                            msg = u"La información de retorno posee errores, el pago del alumno no existe - linea %s" % i
                            self._errors["Archivo"] = ErrorList([msg])
                            del cleaned_data["Archivo"]
                            validacion = False
                            break

                        retorno2_desc = linea[37:47]

                        # solo tomo la serie no el correlativo
                        retorno3_comp = linea[47:50]
                        # solo tomo el correlativo no la serie
                        retorno4_comp = linea[50:57]
                        # Debido a que en algunas ocaciones el archivo contiene un error (contiene un espacio
                        # donde debería haber un cero)
                        # la siguiente línea agrega un cero en el lugar
                        # correcto.
                        try:
                            retorno5_comp = str(int(retorno4_comp)).rjust(
                                7).replace(" ", "0")
                        except ValueError:
                            retorno5_comp = 0

                        if int(retorno5_comp) != 0:
                            try:
                                comp = Comprobante.objects.get(
                                    TipoComprobante__Serie__Numero=retorno3_comp, Correlativo=retorno5_comp)
                            except Comprobante.DoesNotExist:
                                msg = u"El comprobante no existe - linea %s" % i
                                self._errors["Archivo"] = ErrorList([msg])
                                del cleaned_data["Archivo"]
                                validacion = False
                                break
                        else:
                            comp = Comprobante.objects.filter(TipoComprobante=tipo_comp, Correlativo=str(
                                corre_inicio).rjust(7).replace(" ", "0"))
                            if comp.count() != 0:
                                msg = u"El comprobante ya existe - comp N %s" % str(
                                    corre_inicio).rjust(7).replace(" ", "0")
                                self._errors["Archivo"] = ErrorList([msg])
                                del cleaned_data["Archivo"]
                                validacion = False
                                break
                            corre_inicio = int(corre_inicio) + 1

                        pago_anio = linea[57:61]
                        pago_mes = linea[61:63]
                        pago_dia = linea[63:65]

                        vence_anio = linea[65:69]
                        vence_mes = linea[69:71]
                        vence_dia = linea[71:73]

                        monto_pagado = linea[73:86] + "." + linea[86:88]
                        monto_mora = linea[88:101] + "." + linea[101:103]
                        monto_total_pagado = linea[
                                             103:116] + "." + linea[116:118]

                        total = total + Decimal(monto_total_pagado)

                    elif identificacion == "CC" and i == 1:
                        cuenta = linea[2:14]
                        recaudo_anio = linea[14:18]
                        recaudo_mes = linea[18:20]
                        recaudo_dia = linea[20:22]
                        total_registros = linea[22:31]
                        monto_total = linea[31:44] + "." + linea[44:46]
                    else:
                        msg = u"El contenido del archivo presenta errores"
                        self._errors["Archivo"] = ErrorList([msg])
                        del cleaned_data["Archivo"]
                        validacion = False
                        break
                    i += 1

                if validacion is True:
                    if total != Decimal(monto_total):
                        msg = u"El monto total no coincide (%s frente a %s)" % (
                            total, monto_total)
                        self._errors["Archivo"] = ErrorList([msg])
                        del cleaned_data["Archivo"]
                        return cleaned_data

                    if int(total_registros) != int(i - 2):
                        msg = u"El número de registros no coincide (%s frente a %s)" % (
                            int(total_registros), int(i - 2))
                        self._errors["Archivo"] = ErrorList([msg])
                        del cleaned_data["Archivo"]
                        return cleaned_data

        return cleaned_data
