# -*- coding: utf-8 -*-
import datetime

from django.core import serializers
from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Configuraciones.models import TipoComprobante
from Matriculas.models import MatriculaCiclo
from Pagos.forms import AsignarConceptosGrupoForm, GenerarBoletasBancoForm
from Pagos.models import ConceptoPago, PagoAlumno, DetallePago, Comprobante


# noinspection PyUnusedLocal
def concepto(request, periodo_id, pagobanco):
    if periodo_id != "":
        consulta = ConceptoPago.objects.filter(Periodo__id=periodo_id, PagoBanco=pagobanco)
        if consulta:
            return HttpResponse(serializers.serialize('json', consulta, fields=('pk', 'Descripcion')))


# noinspection PyUnusedLocal
def obtener_conceptos(request, periodo_id):
    if periodo_id != "":
        consulta = ConceptoPago.objects.filter(Periodo__id=periodo_id).order_by('Descripcion')
        if consulta:
            return HttpResponse(serializers.serialize('json', consulta, fields=('pk', 'Descripcion')))


@csrf_protect
def form_asignarconceptos_grupo(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = AsignarConceptosGrupoForm(request.POST, request.FILES)
            if form.is_valid():
                conceptopago = form.cleaned_data['ConceptoPago']
                periodo = form.cleaned_data['Periodo']
                archivo = form.cleaned_data['Archivo']

                for codigo_alumno in archivo:
                    codigo_alumno = codigo_alumno.strip('\n')
                    mat = MatriculaCiclo.objects.get(Periodo=periodo, Alumno__Codigo=codigo_alumno)
                    pago_existe = PagoAlumno.objects.filter(ConceptoPago=conceptopago, MatriculaCiclo=mat)
                    if pago_existe.count() == 0:
                        save_pago = PagoAlumno(ConceptoPago=conceptopago, MatriculaCiclo=mat)
                        save_pago.save()

                mensaje = "Conceptos de Pago asignados Correctamente"
                links = "<a href=''>Volver</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = AsignarConceptosGrupoForm()

        return render_to_response("Pagos/Grupal/form_asignarconceptos_grupo.html", {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../'>Volver</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def form_generarboletas_banco(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_comprobante') and request.user.has_perm(
            'Pagos.add_detallepago') and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = GenerarBoletasBancoForm(request.POST)
            if form.is_valid():
                anio = form.cleaned_data['Periodo']
                correlativo = form.cleaned_data['Correlativo']
                conceptopago = form.cleaned_data['ConceptoPago']
                fechapago = form.cleaned_data['FechaPago']

                siguiente = int(correlativo)
                pagantes = PagoAlumno.objects.filter(MatriculaCiclo__Periodo=anio, MatriculaCiclo__Estado='Matriculado',
                                                     Estado=False, ConceptoPago=conceptopago).order_by(
                    '-MatriculaCiclo__Alumno__user_ptr')

                if pagantes.count() != 0:
                    for mat in pagantes:
                        # solo asignamos boleta si no tiene ninguna asignada, el conteo de objetos
                        # DetallePago que referencian este PagoAlumno debe ser 0
                        if mat.detallepago_set.count() == 0:
                            # verificamos que el pago no tenga detalle
                            # obtenemos el tipo de comprobante asociado con el pago en banco
                            try:
                                comp = TipoComprobante.objects.filter(PagoBanco=True, Estado=True)[0]
                            except Comprobante.DoesNotExist:
                                mensaje = "No existe un tipo de comprobante destinado para pagos en banco"
                                links = "<a href=''>Volver</a>"
                                return render_to_response("Pagos/mensaje.html",
                                                          {"mensaje": mensaje, "links": mark_safe(links),
                                                           "user": request.user})

                            # creamos el comprobante con el correlativo siguiente
                            comp1 = None
                            try:
                                comp1 = Comprobante(Correlativo=str(siguiente).rjust(7).replace(" ", "0"),
                                                    TipoComprobante=comp, Glosa="", LugarPago="Banco",
                                                    FechaEmision=fechapago)
                                comp1.save()
                            except IntegrityError:
                                mensaje = "Fin de la asignacion,El comprobante %s %s - %s ya existe" % (
                                    comp1.TipoComprobante.Tipo, comp1.TipoComprobante.Serie,
                                    str(siguiente).rjust(7).replace(" ", "0"))
                                links = "<a href=''>Volver</a>"
                                return render_to_response("Pagos/mensaje.html",
                                                          {"mensaje": mensaje, "links": mark_safe(links),
                                                           "user": request.user})

                            # creamos el detalle del pago con el correlativo creado y el concepto asignado
                            registro = DetallePago(Mora=0, MontoDeuda=mat.DeudaMonto(), MontoCancelado='0.00',
                                                   PagoRestante=mat.DeudaMonto(), PagoAlumno=mat, Comprobante=comp1,
                                                   FechaPago=fechapago)
                            registro.save()

                            # siguiente correlativo
                            siguiente = siguiente + 1
                else:
                    mensaje = "No existe ningun alumno con este Concepto de Pago,verifique por favor"
                    links = "<a href=''>Volver</a>"
                    return render_to_response("Pagos/mensaje.html",
                                              {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

                mensaje = "Boletas de pago asignados Correctamente"
                links = "<a href=''>Volver</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            comp = Comprobante.objects.all().order_by('-id')
            if comp.count() != 0:
                corre = int(comp[0].Correlativo) + 1
                siguiente = str(corre).rjust(7).replace(" ", "0")
                form = GenerarBoletasBancoForm(initial={'Correlativo': siguiente, 'FechaPago': datetime.date.today()})
            else:
                form = GenerarBoletasBancoForm(initial={'FechaPago': datetime.date.today()})

        return render_to_response("Pagos/Banco/form_generarboletas_banco.html", {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')
