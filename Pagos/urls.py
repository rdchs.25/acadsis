# -*- coding: utf-8 -*-
from django.conf.urls import url

from Pagos.Banco import form_archivo_banco, upload_archivo_banco, generar_archivo_banco_auto
from Pagos.printing import imprimir_comprobantes, print_comprobantes
from Pagos.reportes import form_pagosdiarios, form_deudasconcepto, form_pagosconcepto, form_cuentascobrar, \
    form_cuentascobrar2
from Pagos.siscont import generar_siscont
from Pagos.views import index_descuentos, index_conceptos
from Pagos.views import index_pagos, form_pago_individual, buscar_matriculado, pagos_matriculado, \
    detalle_pago_matriculado, editar_detallepago, eliminar_detallepago, add_conceptos_matriculado, \
    index_cobrar_matriculado, add_conceptos_cobrar, save_facturar_conceptos, add_moras_cobrar, save_facturar_moras, \
    detalle_comprobante, modificar_comprobante, prorroga_pago_matriculado, exceso_pago_matriculado
from Pagos.views2 import form_generarboletas_banco, form_asignarconceptos_grupo

app_name = 'Pagos'

urlpatterns = [
    url(r'^Pagos/pagoalumno/$', index_pagos),
    url(r'^Pagos/pagoalumno/add/$', index_pagos),
    url(r'^Pagos/pagoalumno/periodo/$', form_pago_individual),
    url(r'^Pagos/pagoalumno/periodo/(\d+)/$', buscar_matriculado),
    url(r'^Pagos/pagoalumno/individual/(\d+)/$', pagos_matriculado),
    url(r'^Pagos/pagoalumno/individual/prorroga/(\d+)/$', prorroga_pago_matriculado),
    url(r'^Pagos/pagoalumno/individual/exceso/(\d+)/$', exceso_pago_matriculado),
    url(r'^Pagos/pagoalumno/individual/detalle/(\d+)/$', detalle_pago_matriculado),
    url(r'^Pagos/pagoalumno/individual/detalle/editar/(\d+)/$', editar_detallepago),
    url(r'^Pagos/pagoalumno/individual/detalle/eliminar/(\d+)/$', eliminar_detallepago),
    url(r'^Pagos/pagoalumno/addconceptos/(\d+)/$', add_conceptos_matriculado),
    url(r'^Pagos/pagoalumno/cobrarmatriculado/(\d+)/$', index_cobrar_matriculado),
    url(r'^Pagos/pagoalumno/cobrarmatriculado/conceptos/(\d+)/$', add_conceptos_cobrar),
    url(r'^Pagos/pagoalumno/cobrarmatriculado/conceptos/(\d+)/save/$', save_facturar_conceptos),
    url(r'^Pagos/pagoalumno/cobrarmatriculado/moras/(\d+)/$', add_moras_cobrar),
    url(r'^Pagos/pagoalumno/cobrarmatriculado/moras/(\d+)/save$', save_facturar_moras),
    url(r'^Pagos/comprobante/(\d+)/$', detalle_comprobante),
    url(r'^Pagos/comprobante/modificar/(\d+)/$', modificar_comprobante),
    url(r'^Pagos/comprobante/imprimir/(\d+)/$', imprimir_comprobantes),
    url(r'^Pagos/pagoalumno/comprobantes/imprimir/$', print_comprobantes),
    url(r'^Pagos/pagoalumno/generar_archivo_banco/$', form_archivo_banco),
    url(r'^Pagos/pagoalumno/generar_archivo_banco/auto/$', generar_archivo_banco_auto),
    url(r'^Pagos/pagoalumno/upload_archivo_banco/$', upload_archivo_banco),
    url(r'^Pagos/pagoalumno/boletasbanco/$', form_generarboletas_banco),
    url(r'^Pagos/pagoalumno/asignarconceptosgrupo/$', form_asignarconceptos_grupo),
    url(r'^Pagos/pagoalumno/reportepagos/pagosdiarios/$', form_pagosdiarios),
    url(r'^Pagos/pagoalumno/reportepagos/deudasconcepto/$', form_deudasconcepto),
    url(r'^Pagos/pagoalumno/reportepagos/pagosconcepto/$', form_pagosconcepto),
    url(r'^Pagos/pagoalumno/reportepagos/cuentascobrar/$', form_cuentascobrar),
    url(r'^Pagos/pagoalumno/reportepagos/cuentascobrar2/$', form_cuentascobrar2),
    url(r'^Pagos/pagoalumno/siscont/$', generar_siscont),
    # urls para aplicacion Conceptos de Pago
    url(r'^Pagos/conceptopago/elegirperiodo/$', index_conceptos),
    url(r'^Pagos/descuentopago/elegirperiodo/$', index_descuentos),
]
