# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin
# imports para eliminar pagoalumno
from django.contrib.admin.utils import unquote, get_deleted_objects
from django.core.exceptions import PermissionDenied
from django.db import transaction, router
from django.http import Http404
from django.shortcuts import render_to_response
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_unicode
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_protect

from Categorias.models import Categoria
from Pagos.models import PagoAlumno, ConceptoPago, DescuentoPago, Comprobante
from Periodos.models import Periodo

csrf_protect_m = method_decorator(csrf_protect)

CONCEPTO_CHOICES = (
    ('Pension', 'Pensión'),
    ('Matricula', 'Matrícula'),
    ('Otro', 'Otro'),
)

MES_CHOICES = (
    ('Enero', 'Enero'),
    ('Febrero', 'Febrero'),
    ('Marzo', 'Marzo'),
    ('Abril', 'Abril'),
    ('Mayo', 'Mayo'),
    ('Junio', 'Junio'),
    ('Julio', 'Julio'),
    ('Agosto', 'Agosto'),
    ('Setiembre', 'Setiembre'),
    ('Octubre', 'Octubre'),
    ('Noviembre', 'Noviembre'),
    ('Diciembre', 'Diciembre'),
)


class ConceptoPagoAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ConceptoPagoAdminForm, self).__init__(*args, **kwargs)
        try:
            self.fields['FechaVencimiento'].initial = self.instance.FechaVencimiento
        except:
            pass

    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True)
    Tipo = forms.ChoiceField(choices=CONCEPTO_CHOICES, required=False)
    Mes = forms.ChoiceField(choices=MES_CHOICES, required=False)
    Descuentos = forms.ModelMultipleChoiceField(queryset=DescuentoPago.objects.all(), required=False)

    # FechaVencimiento = forms.DateField(widget = DateTimeWidget,label = "Vence Pago")

    class Meta:
        model = ConceptoPago
        fields = '__all__'

    class Media:

        js = ("custom/js/jquery.js",
              "custom/js/Pagos/frmconcepto.js"
              )


class ConceptoPagoAdmin(admin.ModelAdmin):
    form = ConceptoPagoAdminForm
    search_fields = ('Descripcion',)
    list_display = ('Descripcion', 'Monto', 'TipoMora', 'MontoMora', 'Vence', 'PagoBanco', 'Unico')
    list_filter = ('Periodo',)
    list_editable = ('PagoBanco',)
    fieldsets = (
        ('Concepto de Pago', {
            'fields': ('Periodo', ('Tipo', 'Mes', 'Descripcion'), ('MontoPago', 'Moneda'),
                       ('TipoMora', 'MontoMora', 'FechaVencimiento'), 'Descuentos', 'PagoBanco', 'Unico'),
        }),
    )

    def queryset(self, request):
        """
        Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view.
        """
        id_periodo = request.GET.get('p', '')
        qs = self.model._default_manager.get_query_set()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or ()  # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        if id_periodo != '':
            return qs.filter(Periodo__id=id_periodo)
        else:
            return qs


admin.site.register(ConceptoPago, ConceptoPagoAdmin)


# DescuentoPago
class DescuentoPagoAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(DescuentoPagoAdminForm, self).__init__(*args, **kwargs)
        try:
            self.fields['FechaLimite'].initial = self.instance.FechaLimite
        except:
            pass

    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True)
    Categoria = forms.ModelMultipleChoiceField(queryset=Categoria.objects.filter(Periodo_id=25), required=False)

    class Meta:
        model = DescuentoPago
        fields = '__all__'


class DescuentoPagoAdmin(admin.ModelAdmin):
    form = DescuentoPagoAdminForm
    search_fields = ('Descripcion',)
    list_display = ('Descripcion', 'MontoDescuento', 'FechaLimite')
    list_filter = ('Periodo',)
    fieldsets = (
        ('Descuento de Pago', {
            'fields': ('Descripcion', 'Periodo', 'Categoria', 'MontoDescuento', 'FechaLimite'),
        }),
    )

    def queryset(self, request):
        """
        Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view.
        """
        id_periodo = request.GET.get('p', '')
        qs = self.model._default_manager.get_query_set()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or ()  # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        if id_periodo != '':
            return qs.filter(Periodo__id=id_periodo)
        else:
            return qs


admin.site.register(DescuentoPago, DescuentoPagoAdmin)


# Fin de DescuentoPago


class PagoAlumnoAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    raw_id_fields = ('MatriculaCiclo', 'ConceptoPago',)

    @csrf_protect_m
    @transaction.atomic()
    def delete_view(self, request, object_id, extra_context=None):
        "The 'delete' admin view for this model."
        opts = self.model._meta
        app_label = opts.app_label

        obj = self.get_object(request, unquote(object_id))

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            raise Http404(_('%(name)s object with primary key %(key)r does not exist.') % {
                'name': force_unicode(opts.verbose_name), 'key': escape(object_id)})

        using = router.db_for_write(self.model)

        # Populate deleted_objects, a data structure of all related objects that
        # will also be deleted.
        (deleted_objects, model_count, perms_needed, protected) = get_deleted_objects(
            [obj], opts, request.user, self.admin_site, using)

        if request.POST:  # The user has already confirmed the deletion.
            if perms_needed:
                raise PermissionDenied
            obj_display = force_unicode(obj)
            self.log_deletion(request, obj, obj_display)
            self.delete_model(request, obj)

            self.message_user(request, _('The %(name)s "%(obj)s" was deleted successfully.') % {
                'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj_display)})

            mensaje = "Pago Eliminado Correctamente"
            links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": self})

        object_name = force_unicode(opts.verbose_name)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": object_name}
        else:
            title = _("Are you sure?")

        context = {
            "title": title,
            "object_name": object_name,
            "object": obj,
            "deleted_objects": deleted_objects,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": opts,
            "app_label": app_label,
        }
        context.update(extra_context or {})

        return TemplateResponse(request, self.delete_confirmation_template or [
            "admin/%s/%s/delete_confirmation.html" % (app_label, opts.object_name.lower()),
            "admin/%s/delete_confirmation.html" % app_label,
            "admin/delete_confirmation.html"
        ], context, current_app=self.admin_site.name)


admin.site.register(PagoAlumno, PagoAlumnoAdmin)


class ComprobanteAdmin(admin.ModelAdmin):
    list_display = ('Correlativo', 'TipoComprobante', 'LugarPago', 'Glosa',)
    list_filter = ('TipoComprobante', 'LugarPago',)
    search_fields = ('Correlativo',)
    list_per_page = 100


admin.site.register(Comprobante, ComprobanteAdmin)
