# -*- coding: utf-8 -*-
import datetime
import json
import urllib2
from copy import deepcopy
from decimal import Decimal
from itertools import ifilter

from django import forms
from django.conf import settings
from django.db.models import Q
from django.forms.utils import ErrorList
from django.forms.widgets import CheckboxSelectMultiple
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.encoding import smart_str
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Alumnos.models import Alumno
from Alumnos.widgets import DateTimeWidget
from Carreras.models import Carrera
from Matriculas.models import MatriculaCursos, MatriculaCiclo, CONDICION_CHOICES
from Pagos.models import PagoAlumno, DetallePago, ConceptoPago
from Periodos.models import Periodo
from funciones import response_excel, color_excel, response_excelpago2

LUGAR_PAGO_CHOICES = (
    ('Caja', 'Caja'),
    ('Banco', 'Banco'),
    ('Banco2', 'Banco2'),
    ('Depósito', 'Depósito'),
    ('Planilla', 'Planilla'),
)


def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


class PagosDiariosForm(forms.Form):
    LugarPago = forms.MultipleChoiceField(help_text='Banco2 es la cuenta corriente recaudadora', required=True,
                                          widget=CheckboxSelectMultiple, choices=LUGAR_PAGO_CHOICES)
    Desde = forms.DateField(widget=DateTimeWidget(), required=True, label="Desde")
    Hasta = forms.DateField(widget=DateTimeWidget(), required=True, label="Hasta")
    Archivo = forms.FileField(required=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        archivo = cleaned_data.get("Archivo")
        desde = cleaned_data.get("Desde")
        hasta = cleaned_data.get("Hasta")

        print archivo
        if archivo:
            if archivo.content_type != 'text/plain':
                msg = u"Debe ingresar un archivo en texto plano CDPG"
                self._errors["Archivo"] = ErrorList([msg])
                del cleaned_data["Archivo"]
            else:
                i = 1
                total = Decimal("0.00")
                for line in archivo:
                    linea = str(line)
                    identificacion = str(linea[:2])

                    if identificacion == "CC" and i == 1:
                        cuenta = linea[2:14]
                        recaudo_anio = linea[14:18]
                        recaudo_mes = linea[18:20]
                        recaudo_dia = linea[20:22]
                        total_registros = linea[22:31]
                        monto_total = Decimal(linea[31:44] + "." + linea[44:46])

                        # Verifico la fecha
                        date_cdgp = recaudo_anio + "-" + recaudo_mes + "-" + recaudo_dia
                        if desde.strftime('%Y-%m-%d') != hasta.strftime('%Y-%m-%d'):
                            msg = u"La fecha debe ser la misma en ambos"
                            self._errors["Archivo"] = ErrorList([msg])
                        else:
                            if desde.strftime('%Y-%m-%d') != date_cdgp:
                                msg = u"El archivo CDPG es de una fecha distinta"
                                self._errors["Archivo"] = ErrorList([msg])
        return cleaned_data

    class Media:
        js = ("custom/calendario_admin/src/js/jscal2.js",
              "custom/calendario_admin/src/js/lang/es.js"
              )

        css = {
            "all": ("custom/calendario_admin/src/css/jscal2.css",
                    "custom/calendario_admin/src/css/border-radius.css",
                    "custom/calendario_admin/src/css/reduce-spacing.css",
                    "custom/calendario_admin/src/css/steel/steel.css",)
        }


@csrf_protect
def form_pagosdiarios(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = PagosDiariosForm(request.POST, request.FILES)
            if form.is_valid():
                total = 0
                lugarpago = form.cleaned_data['LugarPago']
                desde = form.cleaned_data['Desde']
                hasta = form.cleaned_data['Hasta']
                archivo = form.cleaned_data["Archivo"]

                lugares = ''
                pagos = DetallePago.objects.filter(FechaPago__range=(desde, hasta)).exclude(
                    MontoCancelado=Decimal('0.00'))

                qset = Q(Comprobante__LugarPago=lugarpago[0])
                lugares += lugarpago[0] + ''
                for lugar in lugarpago[1:]:
                    lugares += lugar + ','
                    qset = qset | Q(Comprobante__LugarPago=lugar)

                pagos = pagos.filter(qset).order_by('FechaPago', 'PagoAlumno__MatriculaCiclo__Alumno')

                total = 0
                pagos_diarios = []
                anio = str(desde.year)
                desde = str(desde.strftime("%d-%m-%Y"))
                hasta = str(hasta.strftime("%d-%m-%Y"))

                # Enlace Sistema de Idiomas
                if archivo:
                    json_string = urllib2.urlopen(settings.URL_IDIOMAS + "/json/%s/" % "2017").read()
                    datos_idiomas = json.loads(json_string)

                for pago in pagos:
                    codigo = pago.PagoAlumno.MatriculaCiclo.Alumno.Codigo
                    apellidos_alumno = pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoMaterno
                    nombres_alumno = pago.PagoAlumno.MatriculaCiclo.Alumno.Nombres
                    carrera = pago.PagoAlumno.MatriculaCiclo.Alumno.Carrera.Carrera
                    if pago.Mora == True:
                        concepto = 'Mora ' + pago.PagoAlumno.ConceptoPago.Descripcion
                    else:
                        concepto = pago.PagoAlumno.ConceptoPago.Descripcion
                    monto_pagar = pago.MontoDeuda
                    monto_cancelado = pago.MontoCancelado
                    comprobante = pago.Comprobante.TipoComprobante.Serie.Numero + '-' + pago.Comprobante.Correlativo
                    fecha = pago.FechaPago
                    lugar = pago.Comprobante.LugarPago
                    deuda = pago.PagoRestante

                    monto = pago.MontoCancelado
                    total = total + Decimal(monto)

                    # ProcesarCDPG
                    if archivo:
                        i = 1
                        total_cdpg = Decimal("0.00")
                        monto_ingles = Decimal("0.00")

                        for line in archivo:
                            if i == 1:
                                i += 1
                                continue

                            codigo_cdpg = line[20:27]
                            concepto_cdpg = line[37:46]
                            if codigo != codigo_cdpg:
                                continue
                            if concepto[:3].lower() != concepto_cdpg[:3].lower():
                                continue

                            monto_pagado_cdpg = line[73:86] + "." + line[86:88]
                            # monto_mora_cdpg = line[88:101] + "." + line[101:103]
                            # monto_total_pagado_cdpg = line[103:116] + "." + line[116:118]
                            # total_cdpg = total + Decimal(monto_total_pagado_cdpg)

                            if Decimal(monto_pagado_cdpg) >= Decimal(monto_cancelado):
                                monto_ingles = Decimal(monto_pagado_cdpg) - monto_cancelado
                            else:
                                monto_ingles = "ERROR"
                                # print "Codigo: ", codigo, "CDPG: ", codigo_cdpg
                                # print "Concepto CDPG: ", concepto_cdpg[:3].lower(), "Monto Pagado: ", monto_pagado_cdpg, "Monto Mora: ", monto_mora_cdpg, "Monto Total: ", monto_total_pagado_cdpg
                                # print " "
                                # print "Concepto: ", concepto[:3].lower(), "Monto a Pagar: ", monto_pagar, "Monto Cancelado: ", monto_cancelado, "Deuda: ", pago.PagoRestante
                                # print "-----------"
                        try:
                            datos = deepcopy(ifilter(lambda x: x['usuario'] == codigo, datos_idiomas).next())
                        except:
                            pagos_diarios.append(
                                [codigo, apellidos_alumno, nombres_alumno, carrera, concepto, monto_pagar,
                                 monto_cancelado, deuda, comprobante, fecha, lugar, monto_ingles,
                                 "Error, usuario no encontrado en el sistema de Idiomas"])
                            continue

                        grupo_ingles = datos["grupo"]
                        categoria_ingles = datos["categoria"]

                        concepto_ingles = " "
                        monto2_ingles = " "
                        pago_ingles = " "

                        # pagos_diarios.append([codigo, apellidos_alumno, nombres_alumno, carrera, concepto, monto_pagar, monto_cancelado, deuda, comprobante, fecha, lugar, monto_ingles, grupo_ingles, categoria_ingles, concepto_ingles, monto2_ingles, pago_ingles])
                        for j, pagos_idiomas in enumerate(datos["pagos"]):
                            concepto_ingles = pagos_idiomas["concepto"]
                            monto2_ingles = pagos_idiomas["monto"]
                            if pagos_idiomas["estado"]:
                                pago_ingles = "Pagado"
                            else:
                                pago_ingles = "Deuda"
                            if j == 0:
                                pagos_diarios.append(
                                    [codigo, apellidos_alumno, nombres_alumno, carrera, concepto, monto_pagar,
                                     monto_cancelado, deuda, comprobante, fecha, lugar, monto_ingles, grupo_ingles,
                                     categoria_ingles, concepto_ingles, monto2_ingles, pago_ingles])
                            else:
                                pagos_diarios.append(
                                    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                     concepto_ingles, monto2_ingles, pago_ingles])

                    else:
                        pagos_diarios.append(
                            [codigo, apellidos_alumno, nombres_alumno, carrera, concepto, monto_pagar, monto_cancelado,
                             deuda, comprobante, fecha, lugar])

                titulo = 'REPORTE DE PAGOS DIARIOS'
                if archivo:
                    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Concepto', 'Monto de pago', 'Monto Pagado',
                               'Deuda', 'Comprobante', 'Fecha', 'Lugar', 'Monto Pagado CDPG', "Grupo Inglés",
                               "Categoria", 'Concepto', 'Monto a Pagar', 'Estado']
                    return color_excel(titulo=titulo, heads=headers, registros=pagos_diarios,
                                       label_resumen=['LUGAR DE PAGO', 'DESDE', 'HASTA', 'TOTAL RECAUDADO'],
                                       datos_resumen=[lugares, desde, hasta, total], nombre_archivo='pagos_diarios')
                else:
                    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Concepto', 'Monto de pago', 'Monto Pagado',
                               'Deuda', 'Comprobante', 'Fecha', 'Lugar']
                    return response_excel(titulo=titulo, heads=headers, registros=pagos_diarios,
                                          label_resumen=['LUGAR DE PAGO', 'DESDE', 'HASTA', 'TOTAL RECAUDADO'],
                                          datos_resumen=[lugares, desde, hasta, total], nombre_archivo='pagos_diarios')
        else:
            form = PagosDiariosForm(initial={'Desde': datetime.date.today(), 'Hasta': datetime.date.today()})

        return render_to_response("Pagos/Reportes/form_reporte_pagosdiarios.html", {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


class DeudasConceptoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True,
                                     label='Periodo')
    ConceptoPago = forms.ModelChoiceField(queryset=ConceptoPago.objects.all().order_by('Descripcion'),
                                          widget=forms.Select(), required=True, label='Concepto')
    Condicion = forms.ChoiceField(choices=CONDICION_CHOICES)

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Pagos/Reportes/frmdeudasconcepto.js"
              )


@csrf_protect
def form_deudasconcepto(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = DeudasConceptoForm(request.POST)
            if form.is_valid():
                total = 0
                total_deuda = 0
                total_mora = 0
                hoy = datetime.date.today().strftime("%d-%m-%Y")
                conceptopago = form.cleaned_data['ConceptoPago']
                condicion = form.cleaned_data['Condicion']
                deudas = PagoAlumno.objects.filter(MatriculaCiclo__Estado=condicion, ConceptoPago=conceptopago,
                                                   Estado=False).order_by('MatriculaCiclo__Alumno__ApellidoPaterno',
                                                                          'MatriculaCiclo__Alumno__ApellidoMaterno',
                                                                          'MatriculaCiclo__Alumno__Nombres')

                deudas_concepto = []
                for deuda in deudas:
                    codigo = deuda.MatriculaCiclo.Alumno.Codigo
                    apellidos_alumno = deuda.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + deuda.MatriculaCiclo.Alumno.ApellidoMaterno
                    nombres_alumno = deuda.MatriculaCiclo.Alumno.Nombres
                    carrera = deuda.MatriculaCiclo.Alumno.Carrera.Carrera
                    telefono_alumno = deuda.MatriculaCiclo.Alumno.Telefono
                    celular_alumno = deuda.MatriculaCiclo.Alumno.Celular
                    provincia_alumno = deuda.MatriculaCiclo.Alumno.Provincia.Provincia
                    direccion_alumno = deuda.MatriculaCiclo.Alumno.Direccion

                    try:
                        detalle = deuda.detallepago_set.filter(Mora=False)[0]
                        comprobante = str(detalle.Comprobante.TipoComprobante.Serie) + ' - ' + str(
                            detalle.Comprobante.Correlativo)
                    except:
                        comprobante = ''

                    monto = deuda.Monto()

                    deuda_monto = deuda.DeudaMonto()
                    monto_pagado = Decimal(monto) - Decimal(deuda_monto)

                    mora = deuda.Mora()

                    deuda_mora = deuda.DeudaMora()
                    if deuda.ConceptoPago.TipoMora == 'Ninguna':
                        vence = ''
                    else:
                        vence = deuda.ConceptoPago.FechaVencimiento.strftime("%d-%m-%Y")
                    deudas_concepto.append(
                        [codigo, apellidos_alumno, nombres_alumno, carrera, monto, monto_pagado, deuda_monto,
                         comprobante, vence, mora, deuda_mora, telefono_alumno, celular_alumno, provincia_alumno,
                         direccion_alumno])
                    total_deuda = total_deuda + Decimal(deuda.DeudaMonto())
                    total_mora = total_mora + Decimal(deuda.DeudaMora())
                    total = total + Decimal(deuda.DeudaMonto()) + Decimal(deuda.DeudaMora())

            headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Monto de pago', 'Monto Pagado', 'Deuda',
                       'Comprobante', 'Vence', 'Monto de Mora', 'Deuda de Mora', 'Teléfono', 'Celular', 'Provincia',
                       'Dirección']
            titulo = 'REPORTE DE DEUDAS POR CONCEPTO'
            return response_excel(titulo=titulo, heads=headers, registros=deudas_concepto,
                                  label_resumen=['DEUDAS DE', 'FECHA', 'DEUDA ', 'MORA', 'TOTAL'],
                                  datos_resumen=[conceptopago.Descripcion, hoy, total_deuda, total_mora, total],
                                  nombre_archivo='deudas_concepto')
        else:
            form = DeudasConceptoForm(initial={'Condicion': 'Matriculado'})

        return render_to_response("Pagos/Reportes/form_reporte_deudasconcepto.html",
                                  {"form": form, "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


class PagosConceptoForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True,
                                     label='Periodo')
    ConceptoPago = forms.ModelMultipleChoiceField(queryset=ConceptoPago.objects.all(), required=True, label='Concepto')
    Condicion = forms.ChoiceField(choices=CONDICION_CHOICES)

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Pagos/Reportes/frmpagosconcepto.js"
              )


@csrf_protect
def form_pagosconcepto(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = PagosConceptoForm(request.POST)
            if form.is_valid():
                total = 0
                conceptos_pago = form.cleaned_data['ConceptoPago']
                condicion = form.cleaned_data['Condicion']

                conceptos = ''
                pagos = DetallePago.objects.filter(PagoAlumno__MatriculaCiclo__Estado=condicion).exclude(
                    MontoCancelado=Decimal('0.00'))

                qset = Q(PagoAlumno__ConceptoPago=conceptos_pago[0])
                conceptos += conceptos_pago[0].Descripcion + ','
                for concepto in conceptos_pago[1:]:
                    conceptos += concepto.Descripcion + ','
                    qset = qset | Q(PagoAlumno__ConceptoPago=concepto)

                pagos = pagos.filter(qset).order_by('PagoAlumno__MatriculaCiclo__Alumno', 'PagoAlumno__ConceptoPago')

                total = 0
                pagos_diarios = []

                for pago in pagos:
                    codigo = pago.PagoAlumno.MatriculaCiclo.Alumno.Codigo
                    apellidos_alumno = pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoPaterno + ' ' + pago.PagoAlumno.MatriculaCiclo.Alumno.ApellidoMaterno
                    nombres_alumno = pago.PagoAlumno.MatriculaCiclo.Alumno.Nombres
                    carrera = pago.PagoAlumno.MatriculaCiclo.Alumno.Carrera.Carrera
                    categoria = smart_str(pago.PagoAlumno.MatriculaCiclo.Categoria.Categoria)
                    monto_categoria = smart_str(pago.PagoAlumno.MatriculaCiclo.Categoria.Pago)
                    if pago.Mora == True:
                        concepto = 'Mora ' + pago.PagoAlumno.ConceptoPago.Descripcion
                    else:
                        concepto = pago.PagoAlumno.ConceptoPago.Descripcion
                    monto_pagar = pago.MontoDeuda
                    monto_cancelado = pago.MontoCancelado
                    comprobante = pago.Comprobante.TipoComprobante.Serie.Numero + '-' + pago.Comprobante.Correlativo
                    fecha = pago.FechaPago
                    lugar = pago.Comprobante.LugarPago
                    deuda = pago.PagoRestante
                    monto = pago.MontoCancelado
                    total = total + Decimal(monto)

                    periodoingreso = pago.PagoAlumno.MatriculaCiclo.Alumno.AnioIngreso + ' - ' + pago.PagoAlumno.MatriculaCiclo.Alumno.Semestre

                    pagos_diarios.append(
                        [codigo, apellidos_alumno, nombres_alumno, carrera, periodoingreso, categoria, monto_categoria,
                         concepto, monto_pagar, monto_cancelado, deuda, comprobante, fecha, lugar])

                headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Periodo de Ingreso', 'Categoria',
                           'Monto de Categoría', 'Concepto', 'Monto de pago', 'Monto Pagado', 'Deuda', 'Comprobante',
                           'Fecha', 'Lugar']
                titulo = 'REPORTE DE PAGOS'
                return response_excel(titulo=titulo, heads=headers, registros=pagos_diarios,
                                      label_resumen=['CONCEPTOS', 'TOTAL RECAUDADO'], datos_resumen=[conceptos, total],
                                      nombre_archivo='pagos_concepto')
        else:
            form = PagosConceptoForm(initial={'Condicion': 'Matriculado'})

        return render_to_response("Pagos/Reportes/form_reporte_pagosconcepto.html",
                                  {"form": form, "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


class CuentasCobrarForm(forms.Form):
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(), widget=forms.Select(), required=True,
                                     label='Período')
    ConceptoPago = forms.ModelMultipleChoiceField(queryset=ConceptoPago.objects.all(), required=True, label='Conceptos')
    Condicion = forms.ChoiceField(choices=CONDICION_CHOICES)

    class Media:
        js = ("custom/js/jquery.js",
              "custom/js/Pagos/Reportes/frmcuentascobrar.js"
              )


@csrf_protect
def form_cuentascobrar(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = CuentasCobrarForm(request.POST)
            if form.is_valid():
                conceptos = form.cleaned_data['ConceptoPago']
                periodo = form.cleaned_data['Periodo']
                condicion = form.cleaned_data['Condicion']

                headers = ['Código', 'Estudiante', 'Categoria', 'Carrera', 'Dirección', 'Departamento', 'Provincia',
                           'Distrito', 'Email', 'Teléfono', 'Celular', 'Periodo Ingreso', 'Foto', '¿Tiene cursos?',
                           '¿Estudio 2017-II?']
                for concepto in conceptos:
                    headers.append(concepto.Descripcion)
                    # headers.append("Mora")

                headers.append('Total')
                # headers.append('Total Mora')

                listado = []
                matriculados = MatriculaCiclo.objects.filter(Estado=condicion, Periodo=periodo)
                for mat in matriculados:
                    lista = []
                    lista.append(mat.Alumno.Codigo)
                    lista.append(mat.Alumno.__unicode__())
                    lista.append(mat.Categoria.__unicode__())
                    lista.append(mat.Alumno.Carrera.Carrera)
                    lista.append(mat.Alumno.Direccion)
                    lista.append(mat.Alumno.Provincia.Departamento.Departamento)
                    lista.append(mat.Alumno.Provincia.Provincia)
                    lista.append(mat.Alumno.Distrito.Distrito)
                    lista.append(mat.Alumno.Email)
                    lista.append(mat.Alumno.Telefono)
                    lista.append(mat.Alumno.Celular)
                    lista.append(mat.Alumno.AnioIngreso + ' - ' + mat.Alumno.Semestre)
                    if mat.Alumno.Foto != '':
                        lista.append('Si')
                    else:
                        lista.append('No')

                    cursos_matriculados = MatriculaCursos.objects.filter(MatriculaCiclo=mat)
                    tiene_cursos = 'No'
                    for curso_mat in cursos_matriculados:
                        tiene_cursos = 'Si'
                        break
                    lista.append(tiene_cursos)
                    # estudiaron 2017-II
                    matricula2017ii = MatriculaCiclo.objects.filter(Estado="Matriculado", Alumno=mat.Alumno, Periodo=22)
                    estudio = "NO"
                    if matricula2017ii.count() > 0:
                        estudio = "SI"
                    lista.append(estudio)
                    suma = Decimal('0.00')
                    sumamora = Decimal('0.00')
                    listar = True
                    for concepto in conceptos:
                        try:
                            pago = PagoAlumno.objects.get(MatriculaCiclo=mat, ConceptoPago=concepto)
                            if pago.Estado == True:
                                lista.append('')
                            else:
                                monto_p = pago.DeudaMonto()
                                lista.append(monto_p)
                                suma += monto_p
                            # lista.append(pago.DeudaMora())
                            sumamora += pago.DeudaMora()
                        except:
                            lista.append('NA')
                            # lista.append('0.00')

                    lista.append(suma)
                    '''
                    if suma == 0:
                        listar = False

                    if listar==False:
                        continue
                    '''
                    # lista.append(sumamora)

                    listado.append(lista)

                titulo = 'RESUMEN DE COBRANZA UDL PERIODO %s' % periodo
                return response_excel(titulo=titulo, heads=headers, registros=listado,
                                      nombre_archivo='resumen_cobranza')
        else:
            form = CuentasCobrarForm()

        return render_to_response("Pagos/Reportes/form_reporte_cuentascobrar.html",
                                  {"form": form, "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def archivo_pagos_deudas_alumno(alumnos):
    listado = []
    for alumno3 in alumnos:
        pagos = DetallePago.objects.filter(PagoAlumno__MatriculaCiclo__Alumno__id=alumno3).order_by(
            '-PagoAlumno__ConceptoPago__Periodo', 'PagoAlumno__Estado', '-FechaPago')
        if pagos.count() != 0:
            alumno = pagos[0].PagoAlumno.MatriculaCiclo.Alumno
            for pago in pagos:
                lista = []
                lista.append(alumno.Codigo)
                lista.append(alumno.ApellidoPaterno + ' ' + alumno.ApellidoMaterno)
                lista.append(alumno.Nombres)
                lista.append(alumno.Carrera.Carrera)
                lista.append(alumno.AnioIngreso + ' - ' + alumno.Semestre)
                lista.append(pago.PagoAlumno.ConceptoPago.Periodo.__unicode__())
                if pago.Mora == True:
                    lista.append('Mora ' + pago.PagoAlumno.ConceptoPago.Descripcion)
                else:
                    lista.append(pago.PagoAlumno.ConceptoPago.Descripcion)
                lista.append(pago.MontoDeuda)
                lista.append(pago.MontoCancelado)
                lista.append(pago.PagoRestante)
                lista.append(pago.Comprobante.TipoComprobante.Serie.Numero + '-' + pago.Comprobante.Correlativo)
                lista.append(pago.FechaPago)
                lista.append(pago.Comprobante.LugarPago)
                if pago.PagoAlumno.Estado == True or pago.PagoRestante == Decimal('0.00'):
                    lista.append('Cancelado')
                else:
                    lista.append('Pendiente')
                listado.append(lista)

        pagoalumno = PagoAlumno.objects.filter(MatriculaCiclo__Alumno__id=alumno3, Estado=False).order_by(
            '-ConceptoPago__Periodo')
        if pagoalumno.count() != 0:
            alumno1 = pagoalumno[0].MatriculaCiclo.Alumno
            for pago in pagoalumno:
                if pago.detallepago_set.count() == 0:
                    lista = []
                    lista.append(alumno1.Codigo)
                    lista.append(alumno1.ApellidoPaterno + ' ' + alumno1.ApellidoMaterno)
                    lista.append(alumno1.Nombres)
                    lista.append(alumno1.Carrera.Carrera)
                    lista.append(alumno1.AnioIngreso + ' - ' + alumno1.Semestre)
                    lista.append(pago.ConceptoPago.Periodo.__unicode__())
                    lista.append(pago.ConceptoPago.Descripcion)
                    lista.append(pago.DeudaMonto())
                    lista.append(Decimal('0.00'))
                    lista.append(pago.DeudaMonto())
                    lista.append('')
                    lista.append('')
                    lista.append('')
                    lista.append('Pendiente')
                    listado.append(lista)

    headers = ['Código', 'Apellidos', 'Nombres', 'Carrera', 'Ingreso', 'Semestre', 'Concepto', 'Monto de Pago',
               'Monto Pagado', 'Pago Restante', 'Comprobante', 'Fecha', 'Lugar', 'Estado']
    titulo = 'RESUMEN DE PAGOS Y DEUDAS'
    return response_excel(titulo=titulo, heads=headers, registros=listado, nombre_archivo='resumen_cobranza')


# EXPERIMENTAL

class CuentasCobrarForm2(forms.Form):
    Carrera = forms.ModelChoiceField(queryset=Carrera.objects.all(
    ), widget=forms.Select(), required=True, label='Carrera')
    Periodo = forms.ModelChoiceField(queryset=Periodo.objects.all(
    ), widget=forms.Select(), required=True, label='Periodo')
    ConceptoPago = forms.ModelMultipleChoiceField(
        queryset=ConceptoPago.objects.all(), required=True, label='Conceptos')
    Condicion = forms.ChoiceField(choices=CONDICION_CHOICES)

    class Media:
        js = ('custom/js/jquery.js', 'custom/js/Pagos/Reportes/frmcuentascobrar2.js')
        # js = ("custom/js/jquery.js",)


@csrf_protect
def form_cuentascobrar2(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = CuentasCobrarForm2(request.POST)
            if form.is_valid():

                carrera = form.cleaned_data['Carrera']
                conceptos = form.cleaned_data['ConceptoPago']
                periodo = str(form.cleaned_data['Periodo'])
                condicion = form.cleaned_data['Condicion']

                headers = ['Código', 'Estudiante', 'Carrera', 'Dirección', 'Departamento',
                           'Provincia', 'Distrito', 'Email', 'Teléfono', 'Celular', 'Ingreso', 'Foto']
                for concepto in conceptos:
                    headers.append(concepto.Descripcion)
                    headers.append('Fecha de Pago')
                    headers.append('Comprobante')

                headers.append('Total Cancelado')
                headers.append('Total Deuda')

                listado = []
                # matriculados = MatriculaCiclo.objects.filter(Estado=condicion,Alumno__AnioIngreso=periodo[:4],Alumno__Carrera=carrera)
                alumnos = Alumno.objects.filter(AnioIngreso=periodo[:4], Semestre=periodo[-2:].strip(), Carrera=carrera)

                for alu in alumnos:
                    lista = []
                    lista.append(alu.Codigo)
                    lista.append(alu.__unicode__())
                    lista.append(alu.Carrera.Carrera)
                    lista.append(alu.Direccion)
                    lista.append(alu.Provincia.Departamento.Departamento)
                    lista.append(alu.Provincia.Provincia)
                    lista.append(alu.Distrito.Distrito)
                    lista.append(alu.Email)
                    lista.append(alu.Telefono)
                    lista.append(alu.Celular)
                    lista.append(alu.AnioIngreso + ' - ' + alu.Semestre)
                    if alu.Foto != '':
                        lista.append('Si')
                    else:
                        lista.append('No')

                    suma = Decimal('0.00')
                    suma1 = Decimal('0.00')
                    sumamora = Decimal('0.00')
                    listar = True

                    for concepto in conceptos:
                        try:
                            pago = PagoAlumno.objects.get(MatriculaCiclo__Alumno=alu, ConceptoPago=concepto)
                            # pago = PagoAlumno.objects.get(ConceptoPago = concepto, MatriculaCiclo.Alumno = mat)
                            if pago.Estado == True:
                                # lista.append('')
                                detalles = DetallePago.objects.get(PagoAlumno=pago)
                                MontoCancelado = detalles.MontoCancelado
                                FechaPago = detalles.FechaPago
                                NumComprobante = detalles.Comprobante.Correlativo
                                MontoSoles = "S/ " + str(MontoCancelado)
                                suma1 += MontoCancelado
                                lista.append(MontoSoles)
                                lista.append(FechaPago)
                                lista.append(NumComprobante)
                            else:
                                # monto_p = pago.DeudaMonto()
                                # lista.append(monto_p)
                                # suma += monto_p
                                try:
                                    detalles = DetallePago.objects.get(PagoAlumno=pago)
                                    NumComprobante = detalles.Comprobante.Correlativo
                                    monto_p = pago.DeudaMonto()
                                    MontoSoles = "S/ " + str(monto_p)
                                    lista.append(MontoSoles)
                                    suma += monto_p
                                    lista.append('Debe cuota')
                                    lista.append(NumComprobante)
                                except DetallePago.DoesNotExist:
                                    monto_p = pago.DeudaMonto()
                                    MontoSoles = "S/ " + str(monto_p)
                                    lista.append(MontoSoles)
                                    suma += monto_p
                                    lista.append('Debe cuota')
                                    lista.append('Sin Comprobante')
                            # lista.append(pago.DeudaMora())
                            sumamora += pago.DeudaMora()
                        except:
                            # lista.append('NA')
                            # lista.append('0.00')
                            lista.append('NA')
                            lista.append('NA')
                            lista.append('NA')

                    lista.append(suma1)
                    lista.append(suma)
                    listado.append(lista)

                titulo = 'RESUMEN DE COBRANZA UDL PERIODO %s' % periodo
                return response_excel(titulo=titulo, heads=headers, registros=listado,
                                      nombre_archivo='resumen_cobranza')
        else:
            form = CuentasCobrarForm()

        return render_to_response("Pagos/Reportes/form_reporte_cuentascobrar2.html",
                                  {"form": form, "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


# titulo = 'RESUMEN DE COBRANZA'
#                print (listado)
#                return color_excel_condicional(titulo=titulo, heads=headers, registros=listado, nombre_archivo='resumen_cobranza')
#                print('pasamos')
#        else:
#            form = CuentasCobrarForm2()
#
#        return render_to_response("Pagos/Reportes/form_reporte_cuentascobrar2.html", {"form": form, "user": request.user}, context_instance=RequestContext(request))
#    else:
#        mensaje = "Permiso Denegado"
#        links = "<a href='javascript:window.close()'>Cerrar</a>"
#        return render_to_response("Pagos/mensaje.html", {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

def archivo_resumen_pagos_alumno(alumno):
    alumno_id = alumno[0]
    alumno = Alumno.objects.get(id=alumno_id)
    alumno_nombres = alumno.__unicode__()
    carrera = alumno.Carrera.__unicode__()
    matriculas = MatriculaCiclo.objects.filter(Alumno__Codigo=alumno.Codigo).order_by('Periodo')
    datos_matriculas = matriculas.values_list("Periodo__Anio", "Periodo__Semestre", "Categoria__Categoria")
    datos_pagos = []
    i = 1
    for matricula in matriculas:
        alumno_categoria = matricula.Categoria.Categoria
        alumno_monto_pension = matricula.Categoria.Pago

        alumno_pago_pensiones = matricula.pagoalumno_set.all().order_by('ConceptoPago__Descripcion')
        datos_pagos.append(alumno_pago_pensiones)

        i += 1

    headers = ["Código", "Nombres", "Carrera", "Matrículas", "Categoría"]
    data = [alumno.Codigo, alumno_nombres, carrera, datos_matriculas, datos_pagos]

    return response_excelpago2(data, headers=headers, nombre_archivo='reporte_un_alumno', alumnopar=alumno.Codigo)
