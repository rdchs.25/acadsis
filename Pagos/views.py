# -*- coding: utf-8 -*-
import datetime
import json
from decimal import Decimal, DecimalException

from django.conf import settings
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.encoding import smart_str
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from Carreras.models import Carrera
from Matriculas.models import CONDICION_CHOICES
from Matriculas.models import MatriculaCiclo
from Pagos.forms import IndexDescuentoForm, IndexPagoAlumnoForm, DetallePagoForm, ConceptosForm, CobrarConceptosForm, \
    FacturarConceptosForm, FacturarMorasForm, ComprobanteAdminForm, ProrrogaForm, ExcesoForm, IndexConceptoForm, \
    CobrarMorasForm
from Pagos.logging import log_change, log_deletion
from Pagos.models import ConceptoPago, PagoAlumno, DetallePago, Comprobante, Mora
from Pagos.reportes import archivo_pagos_deudas_alumno, archivo_resumen_pagos_alumno
from Periodos.models import ANIO_CHOICES, SEMESTRE_CHOICES
from Periodos.models import Periodo




try:
    # For Python 3.0 and later
    # noinspection PyCompatibility
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    # noinspection PyCompatibility
    from urllib2 import urlopen


@csrf_protect
def index_conceptos(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.change_conceptopago') or request.user.has_perm(
            'Pagos.add_conceptopago'):
        if request.method == 'POST':
            form = IndexConceptoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?Periodo__id__exact=" + str(periodo.id))
        else:
            form = IndexConceptoForm()
        return render_to_response('Pagos/Conceptos/index_conceptopago.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def index_descuentos(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.change_conceptopago') or request.user.has_perm(
            'Pagos.add_conceptopago'):
        if request.method == 'POST':
            form = IndexDescuentoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect("../?Periodo__id__exact=" + str(periodo.id))
        else:
            form = IndexDescuentoForm()
        return render_to_response('Pagos/Descuentos/index_descuentopago.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


def index_pagos(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):
        return render_to_response('Pagos/index.html', {"user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../')


@csrf_protect
def form_pago_individual(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):
        if request.method == 'POST':
            form = IndexPagoAlumnoForm(request.POST)
            if form.is_valid():
                periodo = form.cleaned_data['Periodo']
                return HttpResponseRedirect(str(periodo.id) + "/")
        else:
            form = IndexPagoAlumnoForm()

        return render_to_response('Pagos/Individual/index_pago_individual.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


@csrf_protect
def buscar_matriculado(request, idperiodo):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):
        if request.method == 'POST':
            action = request.POST['action']
            todo = request.POST['select_across']
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')
            alumnos = []
            if todo == '0':
                matriculados_ciclo = request.POST.getlist('_selected_action')
                for mat in matriculados_ciclo:
                    try:
                        matriculado = MatriculaCiclo.objects.get(id=mat)
                        alumnos.append(matriculado.Alumno.id)
                    except ValueError:
                        pass
            else:
                matriculados_ciclo = []
                ids_mat = MatriculaCiclo.objects.filter(Periodo__id=idperiodo)
                if query1:
                    ids_mat1 = ids_mat.filter(Alumno__Carrera__id=query1)
                else:
                    ids_mat1 = ids_mat

                if query2:
                    ids_mat2 = ids_mat1.filter(Alumno__AnioIngreso=query2)
                else:
                    ids_mat2 = ids_mat1

                if query3:
                    ids_mat3 = ids_mat2.filter(Alumno__Semestre=query3)
                else:
                    ids_mat3 = ids_mat2

                if query4:
                    ids_mat4 = ids_mat3.filter(Estado=query4)
                else:
                    ids_mat4 = ids_mat3

                for id_mat in ids_mat4:
                    matriculados_ciclo.append(id_mat.id)
                    alumnos.append(id_mat.Alumno.id)

            if action == "print_pagos_deudas_selected":
                return archivo_pagos_deudas_alumno(alumnos)
            elif action == "print_resumen_pagos_selected":
                if len(alumnos) == 1:
                    return archivo_resumen_pagos_alumno(alumnos)
                elif len(alumnos) > 1:
                    return HttpResponse("Para éste reporte Solo puede seleccionar un alumno")
            else:
                return HttpResponseRedirect('../../../../../')
        else:
            query = request.GET.get('q', '')
            query1 = request.GET.get('c', '')
            query2 = request.GET.get('p', '')
            query3 = request.GET.get('s', '')
            query4 = request.GET.get('e', '')

            carreras = Carrera.objects.all()
            anios = []
            semestres = []
            condiciones = []

            for i in ANIO_CHOICES:
                anios.append(i[0])

            for s in SEMESTRE_CHOICES:
                semestres.append(s[0])

            for c in CONDICION_CHOICES:
                condiciones.append(c[0])

            try:
                periodo = Periodo.objects.get(id=idperiodo)
            except (Periodo.DoesNotExist, ConceptoPago.DoesNotExist):
                mensaje = "El periodo elegido no es correcto"
                links = "<a href=''>Volver</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

            results = MatriculaCiclo.objects.filter(Periodo=periodo)

            if query1:
                results1 = results.filter(Alumno__Carrera__id=query1)
            else:
                results1 = results

            if query2:
                results2 = results1.filter(Alumno__AnioIngreso=query2)
            else:
                results2 = results1

            if query3:
                results3 = results2.filter(Alumno__Semestre=query3)
            else:
                results3 = results2

            if query4:
                results4 = results3.filter(Estado=query4)
            else:
                results4 = results3

            if query:
                qset = (
                        Q(Alumno__Codigo__icontains=query) |
                        Q(Alumno__Nombres__icontains=query) |
                        Q(Alumno__ApellidoPaterno__icontains=query) |
                        Q(Alumno__ApellidoMaterno__icontains=query)
                )
                results5 = results4.filter(qset).order_by('Alumno__Carrera__Carrera', 'Alumno__ApellidoPaterno',
                                                          'Alumno__ApellidoMaterno', 'Alumno__Nombres')
            else:
                results5 = results4.order_by('Alumno__Carrera__Carrera', 'Alumno__ApellidoPaterno',
                                             'Alumno__ApellidoMaterno', 'Alumno__Nombres')

            n_alumnos = results5.count()
            paginator = Paginator(results5, 100)
            # Make sure page request is an int. If not, deliver first page.
            try:
                page = int(request.GET.get('page', '1'))
            except ValueError:
                page = 1
            # If page request (9999) is out of range, deliver last page of results.
            try:
                results = paginator.page(page)
            except (EmptyPage, InvalidPage):
                results = paginator.page(paginator.num_pages)

            return render_to_response("Pagos/Individual/buscar_matriculado.html",
                                      {"results": results, "paginator": paginator, "periodo": periodo, "query": query,
                                       "query1": query1, "query2": query2, "query3": query3, "query4": query4,
                                       "carreras": carreras, "anios": anios, "semestres": semestres,
                                       'condiciones': condiciones, "n_alumnos": n_alumnos, "user": request.user},
                                      context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


@csrf_protect
def pagos_matriculado(request, idmatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
            pagos = PagoAlumno.objects.filter(MatriculaCiclo=idmatriculado)
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        query = request.GET.get('q', '')
        query1 = request.GET.get('p', '')

        if query and query1:
            pagos = PagoAlumno.objects.filter(MatriculaCiclo=idmatriculado, ConceptoPago__Descripcion__icontains=query,
                                              Estado=query1)
        elif query:
            pagos = PagoAlumno.objects.filter(MatriculaCiclo=idmatriculado, ConceptoPago__Descripcion__icontains=query)
        elif query1:
            pagos = PagoAlumno.objects.filter(MatriculaCiclo=idmatriculado, Estado=query1)

        '''Inicio deudas Idiomas '''
        json_string = urlopen("%s/jsondeudaporanio/%s/%s/" % (settings.URL_IDIOMAS,
                                                              str(matriculado.Alumno.Codigo),
                                                              matriculado.Periodo.Anio)).read()
        deudas_idiomas = json.loads(json_string)

        '''Fin deudas Idiomas '''
        return render_to_response("Pagos/Individual/pagos_matriculado.html",
                                  {"matriculado": matriculado, "pagos": pagos, "deudas_idiomas": deudas_idiomas,
                                   "query": query, "query1": query1,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


def detalle_pago_matriculado(request, idpagomatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):
        pagoalumno = PagoAlumno.objects.get(id=idpagomatriculado)
        detalle_pago = DetallePago.objects.filter(PagoAlumno__id=idpagomatriculado).order_by('-id')
        if detalle_pago.count() == 0:
            detalles = []
        else:
            detalles = detalle_pago

        return render_to_response("Pagos/Individual/detalle_pago_matriculado.html",
                                  {"pagoalumno": pagoalumno, "detalles_pago": detalles, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def editar_detallepago(request, id_detallepago):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.change_detallepago'):
        try:
            detalle_pago = DetallePago.objects.get(id=id_detallepago)
            comprobante = Comprobante.objects.get(id=detalle_pago.Comprobante.id)
        except (DetallePago.DoesNotExist, Comprobante.DoesNotExist):
            mensaje = "El detalle de pago elegido es incorrecto"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = DetallePagoForm(request.POST)
            if form.is_valid():
                comp = form.cleaned_data['TipoComprobante']
                corre = form.cleaned_data['Correlativo']
                lugar_pago = form.cleaned_data['LugarPago']
                monto_pagar = form.cleaned_data['MontoPagar']
                fecha_pago = form.cleaned_data['FechaPago']

                comprobante.TipoComprobante = comp
                comprobante.Correlativo = corre
                comprobante.LugarPago = lugar_pago
                detalle_pago.MontoCancelado = monto_pagar
                detalle_pago.FechaPago = fecha_pago

                if detalle_pago.Mora is False:
                    detalle_pagados = DetallePago.objects.filter(PagoAlumno=detalle_pago.PagoAlumno, Mora=False,
                                                                 Activado=True).exclude(id=detalle_pago.id).order_by(
                        'id')

                    reps = {'S/.': '', 'US$': '', '€': '', ' ': ''}
                    monto_total = str(detalle_pago.PagoAlumno.Monto())
                    for i, j in reps.items():
                        monto_total = monto_total.replace(i, j)

                    monto_restante = Decimal(detalle_pago.MontoDeuda) - Decimal(monto_pagar)
                    detalle_pago.PagoRestante = monto_restante

                    for d in detalle_pagados:
                        if d.id > detalle_pago.id:
                            d.MontoDeuda = monto_restante
                            d.PagoRestante = Decimal(monto_restante) - d.MontoCancelado
                            d.save()
                            monto_restante = d.PagoRestante

                    msg_detalle = u'Monto Deuda: %s, Monto Cancelado: %s,Pago Resta: %s, Fecha Pago: %s' % (
                        str(monto_pagar), str(monto_pagar), str(monto_restante), str(fecha_pago.strftime("%Y-%m-%d")))

                    log_change(request.user.id, detalle_pago, msg_detalle)
                    detalle_pago.save()
                    comprobante.save()

                    vence = detalle_pago.PagoAlumno.ConceptoPago.FechaVencimiento
                    reps = {'S/.': '', 'US$': '', '€': '', ' ': ''}
                    mora = str(detalle_pago.PagoAlumno.Mora())
                    for i, j in reps.items():
                        mora = mora.replace(i, j)

                    if Decimal(detalle_pago.PagoAlumno.DeudaMonto()) == Decimal('0.00') and Decimal(mora) == Decimal(
                            '0.00') and vence < fecha_pago:
                        detalle_pago.PagoAlumno.Estado = False
                        detalle_pago.PagoAlumno.save()

                    detalle_mora = DetallePago.objects.filter(PagoAlumno__id=detalle_pago.PagoAlumno.id, Mora=True,
                                                              Activado=True)
                    mora_diaria = detalle_pago.PagoAlumno.ConceptoPago.MontoMora

                    if vence < detalle_pago.FechaPago and detalle_mora.count() != 0:
                        diferencia = detalle_pago.FechaPago - vence
                        valor_mora = diferencia.days * mora_diaria
                        det = DetallePago.objects.get(id=detalle_mora[0].id)
                        det.MontoCancelado = Decimal(valor_mora)
                        det.PagoRestante = Decimal('0.00')
                        det.MontoDeuda = Decimal(valor_mora)
                        det.save()
                        if detalle_pago.PagoAlumno.mora_set.all().count() != 0:
                            for mora in detalle_pago.PagoAlumno.mora_set.all():
                                mora.Monto = Decimal(valor_mora)
                                mora.Estado = True
                                mora.Exonera = False
                                mora.save()
                        else:
                            crear_mora = Mora(Monto=Decimal(valor_mora), Estado=True, Exonera=False,
                                              PagoAlumno=detalle_pago.PagoAlumno)
                            crear_mora.save()

                    detalle_pago.PagoAlumno.Mora()
                    if Decimal(detalle_pago.PagoAlumno.DeudaMonto()) == Decimal('0.00') and Decimal(
                            detalle_pago.PagoAlumno.DeudaMora()) == Decimal('0.00'):
                        detalle_pago.PagoAlumno.Estado = True
                        detalle_pago.PagoAlumno.save()
                    else:
                        detalle_pago.PagoAlumno.Estado = False
                        detalle_pago.PagoAlumno.save()
                else:
                    detalle_pago.MontoDeuda = monto_pagar
                    msg_detalle = u'Monto Deuda: %s, Monto Cancelado: %s,Fecha Pago: %s' % (
                        str(monto_pagar), str(monto_pagar), str(fecha_pago.strftime("%Y-%m-%d")))

                    msg_comprobante = u'Tipo Comp: %s, Correlativo: %s,Lugar Pago: %s' % (
                        str(comp), str(corre), str(lugar_pago))

                    log_change(request.user.id, detalle_pago, msg_detalle)
                    log_change(request.user.id, comprobante, msg_comprobante)
                    detalle_pago.save()
                    comprobante.save()
                    detalle_pago.PagoAlumno.Mora()

                return HttpResponseRedirect('../../%s/' % detalle_pago.PagoAlumno.id)
        else:
            form = DetallePagoForm(
                initial={'TipoComprobante': comprobante.TipoComprobante.id, 'Correlativo': comprobante.Correlativo,
                         'LugarPago': comprobante.LugarPago, 'FechaPago': detalle_pago.FechaPago,
                         'MontoPagar': detalle_pago.MontoCancelado,
                         'selected_tipo': comprobante.TipoComprobante.Serie.Numero,
                         'selected_corre': comprobante.Correlativo, 'selected_detalle': detalle_pago.id})

        return render_to_response("Pagos/Individual/editar_detallepago.html",
                                  {"detalle_pago": detalle_pago, "form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../'>Volver</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def eliminar_detallepago(request, id_detallepago):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.delete_detallepago'):
        try:
            detalle_pago = DetallePago.objects.get(id=id_detallepago)
            comprobante = Comprobante.objects.get(id=detalle_pago.Comprobante.id)
        except (DetallePago.DoesNotExist, Comprobante.DoesNotExist):
            mensaje = "El detalle de pago elegido es incorrecto"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        detalles = DetallePago.objects.filter(PagoAlumno=detalle_pago.PagoAlumno, Mora=False, Activado=True).order_by(
            '-id')

        detalles_mora = DetallePago.objects.filter(PagoAlumno=detalle_pago.PagoAlumno, Mora=True, Activado=True)

        i = 1
        eliminar = None
        for detalle in detalles:
            if i == 1 and detalle.id == detalle_pago.id:
                eliminar = True
                break
            else:
                eliminar = False
            i += 1

        if request.method == 'POST':
            estado = request.POST['post']
            id_pagoalumno = str(detalle_pago.PagoAlumno.id)
            if estado == 'yes':
                msg_comprobante = u'Tipo Comp: %s, Correlativo: %s,Lugar Pago: %s' % (
                    str(comprobante.TipoComprobante), str(comprobante.Correlativo), str(comprobante.LugarPago))

                msg_detalle = u'Monto Deuda: %s, Monto Cancelado: %s,Fecha Pago: %s, Comprobante: %s' % (
                    str(detalle_pago.MontoDeuda), str(detalle_pago.MontoCancelado), str(detalle_pago.FechaPago),
                    str(comprobante))

                log_deletion(request.user.id, comprobante, msg_comprobante)
                comprobante.delete()

                detalle_pago.PagoAlumno.Estado = False
                detalle_pago.PagoAlumno.save()

                if detalle_pago.Mora is True:
                    moras = Mora.objects.filter(PagoAlumno__id=id_pagoalumno)
                    for mora in moras:
                        mora.Estado = False
                        mora.Exonera = False
                        mora.save()

                log_deletion(request.user.id, detalle_pago, msg_detalle)
                detalle_pago.delete()
            return HttpResponseRedirect('../../%s/' % id_pagoalumno)

        if detalle_pago.Mora is True:
            return render_to_response("Pagos/Individual/eliminar_detallepago.html",
                                      {"detalle_pago": detalle_pago, "comprobante": comprobante, "user": request.user},
                                      context_instance=RequestContext(request))
        elif eliminar is True and detalles_mora.count() == 0:
            return render_to_response("Pagos/Individual/eliminar_detallepago.html",
                                      {"detalle_pago": detalle_pago, "comprobante": comprobante, "user": request.user},
                                      context_instance=RequestContext(request))
        else:
            if detalles_mora.count() == 0:
                mensaje = "El detalle no se puede eliminar, debe eliminar el ultimo detalle cobrado."
            else:
                mensaje = "Existe mora cobrada, por tanto no se puede eliminar el registro"
            links = "<a href='../../%s/'>Volver &rsaquo</a>" % detalle_pago.PagoAlumno.id
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='../'>Volver</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def add_conceptos_matriculado(request, idmatriculado):

    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
            pagos_registrados = PagoAlumno.objects.filter(MatriculaCiclo__id=idmatriculado)
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        periodo = matriculado.Periodo

        conceptos2 = ConceptoPago.objects.filter(Periodo=periodo, Unico=True).order_by('Descripcion')
        conceptos3 = ConceptoPago.objects.filter(Periodo=periodo, Unico=False).order_by('Descripcion')
        conceptos4 = ConceptoPago.objects.filter(Periodo=periodo).order_by('Descripcion')
        conceptos = []

        n_pagos_registrados = pagos_registrados.count()
        i = 1
        if n_pagos_registrados != 0:
            for concepto in conceptos2:
                for pago_registrado in pagos_registrados:
                    if concepto.id == pago_registrado.ConceptoPago.id:
                        i = 1
                        break
                    else:
                        if i == n_pagos_registrados:
                            conceptos.append(concepto)
                            i = 1
                        else:
                            i = i + 1
            for c in conceptos3:
                conceptos.append(c)
        else:
            conceptos = conceptos4

        if request.method == 'POST':
            form = ConceptosForm(request.POST)
            if form.is_valid():
                alumno = form.cleaned_data['Matriculado']
                conceptos = form.cleaned_data['ConceptoPago']
                categoria = form.cleaned_data['IgnorarCategoria']

                for concepto in conceptos:
                    if concepto.Unico is True:
                        pago_existe = PagoAlumno.objects.filter(ConceptoPago=concepto, MatriculaCiclo__id=alumno)
                        if pago_existe.count() == 0:
                            save_pago = PagoAlumno(ConceptoPago_id=concepto.id, MatriculaCiclo_id=alumno,
                                                   IgnorarCategoria=categoria)
                            save_pago.save(usuario = request.user.id)
                    else:
                        save_pago = PagoAlumno(ConceptoPago_id=concepto.id, MatriculaCiclo_id=alumno,
                                               IgnorarCategoria=categoria)
                        save_pago.save(usuario=request.user.id)

                mensaje = "Datos Guardados Correctamente"
                links = "<a href='javascript:opener.location.reload();window.close()'>Cerrar</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = ConceptosForm(initial={'Matriculado': idmatriculado})

        return render_to_response("Pagos/Individual/add_conceptos_matriculado.html",
                                  {"matriculado": matriculado, "conceptos": conceptos, "form": form,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def index_cobrar_matriculado(request, idmatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_detallepago') and request.user.has_perm(
            'Pagos.add_comprobante'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response("Pagos/Individual/index_cobrar_matriculado.html",
                                  {"matriculado": matriculado, "user": request.user})
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def add_conceptos_cobrar(request, idmatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_detallepago') and request.user.has_perm(
            'Pagos.add_comprobante'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
            conceptos_pendientes = PagoAlumno.objects.filter(MatriculaCiclo__id=idmatriculado, Estado=False)
            pagos_pendientes = []
            for concepto_pendiente in conceptos_pendientes:
                if concepto_pendiente.DeudaMonto() != Decimal("0.00"):
                    pagos_pendientes.append(concepto_pendiente)
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = CobrarConceptosForm(conceptos_pendientes, request.POST)
            if form.is_valid():
                alumno = form.cleaned_data['Matriculado']
                conceptos = form.cleaned_data['ConceptoPago']
                form1 = FacturarConceptosForm(initial={'Matriculado': alumno, 'FechaPago': datetime.date.today()})
                return render_to_response("Pagos/Individual/facturar_conceptos_cobrar.html",
                                          {"matriculado": matriculado, "pagos": conceptos, "user": request.user,
                                           "form": form1}, context_instance=RequestContext(request))
        else:
            form = CobrarConceptosForm(conceptos=conceptos_pendientes, initial={'Matriculado': idmatriculado})

        return render_to_response("Pagos/Individual/add_conceptos_cobrar.html",
                                  {"matriculado": matriculado, "pagos_pendientes": pagos_pendientes, "form": form,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close()'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def validar_decimales(value, montocobrar):
    value = smart_str(value).strip()
    montocobrar = smart_str(montocobrar).strip()
    try:
        value = Decimal(value)
        montocobrar = Decimal(montocobrar)
    except DecimalException:
        return "Ingrese un Monto Correcto"

    sign, digittuple, exponent = value.as_tuple()
    decimals = abs(exponent)

    if value < 0:
        return "Valor menor o igual a cero"
    if decimals > 2:
        return "Ingrese solo dos decimales"
    if value > montocobrar:
        return "Valor mayor del monto a cobrar"
    return None


@csrf_protect
def save_facturar_conceptos(request, idmatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_detallepago') and request.user.has_perm(
            'Pagos.add_comprobante'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
        except MatriculaCiclo.DoesNotExist:
            raise Http404
        montos_pago = None
        pagos_pendientes = []
        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close()'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        errores = []

        if request.method == 'POST':
            codigos_pago = request.POST.getlist('pago_id')
            montos_pago = request.POST.getlist('a_pagar')
            montos_deuda = []
            montos_deuda_mora = []
            pagos_pendientes = []

            for codigo_pago in codigos_pago:
                try:
                    pagos = PagoAlumno.objects.get(id=codigo_pago)
                    pagos_pendientes.append(pagos)
                    montos_deuda.append(pagos.DeudaMonto())
                    montos_deuda_mora.append(pagos.DeudaMora())
                except PagoAlumno.DoesNotExist:
                    raise Http404

            for (monto_pago, monto_deuda) in zip(montos_pago, montos_deuda):
                error = validar_decimales(monto_pago, monto_deuda)
                if error is not None:
                    errores.append(error)

            form = FacturarConceptosForm(request.POST)
            if form.is_valid() and errores == []:
                comprobante = form.cleaned_data['TipoComprobante']
                correlativo = form.cleaned_data['Correlativo']
                glosa = form.cleaned_data['Glosa']
                fechapago = form.cleaned_data['FechaPago']

                try:
                    comp = Comprobante(Correlativo=correlativo, TipoComprobante=comprobante, Glosa=glosa,
                                       LugarPago="Caja", FechaEmision=fechapago)
                    comp.save()
                except IntegrityError:
                    raise Http404("El Comprobante %s %s - %s ya existe" % (
                        comprobante.Tipo, comprobante.Serie.Numero, correlativo))

                for (codigo_pago, monto_pago, monto_deuda, monto_deuda_mora) in zip(codigos_pago, montos_pago,
                                                                                    montos_deuda, montos_deuda_mora):
                    try:
                        monto_deuda = Decimal(monto_deuda)
                        monto_pago = Decimal(monto_pago)
                    except DecimalException:
                        mensaje = "Los montos de pago son incorrectos"
                        links = "<a href='javascript:window.close()'>Cerrar</a>"
                        return render_to_response("Pagos/mensaje.html",
                                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
                    pago_restante = monto_deuda - monto_pago
                    registro = DetallePago(Mora=0, MontoDeuda=monto_deuda, MontoCancelado=monto_pago,
                                           PagoRestante=pago_restante, FechaPago=fechapago, PagoAlumno_id=codigo_pago,
                                           Comprobante=comp)
                    registro.save()

                    if Decimal(pago_restante) == Decimal("0.00") and Decimal(monto_deuda_mora) == Decimal("0.00"):
                        pago_alumno = PagoAlumno.objects.get(id=codigo_pago)
                        pago_alumno.Estado = True
                        pago_alumno.save()

                mensaje = "<h1>Datos Guardados Correctamente</h1><br/><a href='../../../../../../" \
                          "Pagos/comprobante/imprimir/" + str(comp.id) + "/'>Imprimir Comprobante</a><br/>"
                links = "<a href='javascript:opener.location.reload();window.close();'>Cerrar</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mark_safe(mensaje), "links": mark_safe(links),
                                           "user": request.user})
        else:
            form = FacturarConceptosForm(initial={'Matriculado': idmatriculado})

        return render_to_response("Pagos/Individual/facturar_conceptos_cobrar.html",
                                  {"matriculado": matriculado, "pagos": pagos_pendientes, "form": form,
                                   "user": request.user, "errores": errores, "montos_pago": montos_pago},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close();'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


# clases y metodos para cobrar y facturar moras de pago pendientes


@csrf_protect
def add_moras_cobrar(request, idmatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_detallepago') and request.user.has_perm(
            'Pagos.add_comprobante') and request.user.has_perm('Pagos.add_mora'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
            conceptos_pendientes = PagoAlumno.objects.filter(MatriculaCiclo__id=idmatriculado, Estado=0)
            pagos_pendientes = []
            for concepto_pendiente in conceptos_pendientes:
                if concepto_pendiente.DeudaMora() != Decimal("0.00") and Decimal(
                        concepto_pendiente.DeudaMonto()) == Decimal('0.00'):
                    pagos_pendientes.append(concepto_pendiente)
        except MatriculaCiclo.DoesNotExist:
            raise Http404

        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close();'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = CobrarMorasForm(conceptos_pendientes, request.POST)
            if form.is_valid():
                alumno = form.cleaned_data['Matriculado']
                conceptos = form.cleaned_data['ConceptoPago']
                form1 = FacturarMorasForm(initial={'Matriculado': alumno, 'FechaPago': datetime.date.today()})
                return render_to_response("Pagos/Individual/Moras/facturar_moras_cobrar.html",
                                          {"matriculado": matriculado, "pagos": conceptos, "user": request.user,
                                           "form": form1}, context_instance=RequestContext(request))
        else:
            form = CobrarMorasForm(conceptos=conceptos_pendientes, initial={'Matriculado': idmatriculado})

        return render_to_response("Pagos/Individual/Moras/add_moras_cobrar.html",
                                  {"matriculado": matriculado, "pagos_pendientes": pagos_pendientes, "form": form,
                                   "user": request.user}, context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close();'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


@csrf_protect
def save_facturar_moras(request, idmatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_detallepago') and request.user.has_perm(
            'Pagos.add_comprobante') and request.user.has_perm('Pagos.add_mora'):
        try:
            matriculado = MatriculaCiclo.objects.get(id=idmatriculado)
        except MatriculaCiclo.DoesNotExist:
            raise Http404
        pagos_pendientes = []
        montos_pago = None
        if matriculado.Estado == u'Reserva de Matrícula' or matriculado.Estado == 'Retirado':
            mensaje = "Permiso Denegado, Matrícula Desactivada"
            links = "<a href='javascript:window.close();'>Cerrar</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        errores = []

        if request.method == 'POST':
            codigos_pago = request.POST.getlist('pago_id')
            montos_pago = request.POST.getlist('a_pagar')
            montos_deuda = []
            montos_deuda_mora = []
            pagos_pendientes = []

            for codigo_pago in codigos_pago:
                try:
                    pagos = PagoAlumno.objects.get(id=codigo_pago)
                    pagos_pendientes.append(pagos)
                    montos_deuda.append(pagos.DeudaMonto())
                    montos_deuda_mora.append(pagos.DeudaMora())
                except PagoAlumno.DoesNotExist:
                    raise Http404

            for (monto_pago, monto_deuda_mora) in zip(montos_pago, montos_deuda_mora):
                error = validar_decimales(monto_pago, monto_deuda_mora)
                if error is not None:
                    errores.append(error)

            form = FacturarMorasForm(request.POST)
            if form.is_valid() and errores == []:
                comprobante = form.cleaned_data['TipoComprobante']
                correlativo = form.cleaned_data['Correlativo']
                glosa = form.cleaned_data['Glosa']
                fechapago = form.cleaned_data['FechaPago']

                try:
                    comp = Comprobante(Correlativo=correlativo, TipoComprobante=comprobante, Glosa=glosa,
                                       LugarPago="Caja", FechaEmision=fechapago)
                    comp.save()
                except IntegrityError:
                    raise Http404("El Comprobante %s %s - %s ya existe" % (
                        comprobante.Tipo, comprobante.Serie.Numero, correlativo))

                for (codigo_pago, monto_pago, monto_deuda, monto_deuda_mora) in zip(codigos_pago, montos_pago,
                                                                                    montos_deuda, montos_deuda_mora):
                    try:
                        monto_deuda_mora = Decimal(monto_deuda_mora)
                        monto_pago = Decimal(monto_pago)
                    except DecimalException:
                        mensaje = "Los montos de pago son incorrectos"
                        links = "<a href='javascript:window.close()'>Cerrar</a>"
                        return render_to_response("Pagos/mensaje.html",
                                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
                    pago_restante = monto_deuda_mora - monto_pago
                    registro = DetallePago(Mora=True, MontoDeuda=monto_deuda_mora, MontoCancelado=monto_pago,
                                           PagoRestante=pago_restante, FechaPago=fechapago, PagoAlumno_id=codigo_pago,
                                           Comprobante=comp)
                    registro.save()

                    registro_mora = Mora.objects.get(PagoAlumno__id=codigo_pago)
                    registro_mora.Monto = monto_pago
                    registro_mora.Estado = True
                    registro_mora.save()

                    if Decimal(pago_restante) == Decimal('0.00') and Decimal(monto_deuda) == Decimal("0.00"):
                        pago_alumno = PagoAlumno.objects.get(id=codigo_pago)
                        pago_alumno.Estado = True
                        pago_alumno.save()

                mensaje = "<h1>Datos Guardados Correctamente</h1><br/><a href='../../../../../" \
                          "Pagos/comprobante/imprimir/" + str(comp.id) + "/'>Imprimir Comprobante</a><br/>"
                links = "<a href='javascript:opener.location.reload();window.close();'>Cerrar</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mark_safe(mensaje), "links": mark_safe(links),
                                           "user": request.user})
        else:
            form = FacturarMorasForm(initial={'Matriculado': idmatriculado})

        return render_to_response("Pagos/Individual/Moras/facturar_moras_cobrar.html",
                                  {"matriculado": matriculado, "pagos": pagos_pendientes, "form": form,
                                   "user": request.user, "errores": errores, "montos_pago": montos_pago},
                                  context_instance=RequestContext(request))
    else:
        mensaje = "Permiso Denegado"
        links = "<a href='javascript:window.close();'>Cerrar</a>"
        return render_to_response("Pagos/mensaje.html",
                                  {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})


def detalle_comprobante(request, idcomp):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        try:
            comprobante = Comprobante.objects.get(id=idcomp)
            detallepago = DetallePago.objects.filter(Comprobante__id=idcomp).order_by('-FechaPago')
            detalles = detallepago
            alumno = detalles[0]
        except Comprobante.DoesNotExist:
            mensaje = "El comprobante no existe o no tiene un detalle de pago,verifique por favor"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        return render_to_response('Pagos/Comprobante/detalle_comprobante.html',
                                  {"detalles": detalles, "comprobante": comprobante, "alumno": alumno,
                                   "user": request.user})
    else:
        return HttpResponseRedirect('../../../../')


@csrf_protect
def modificar_comprobante(request, idcomprobante):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.change_comprobante'):
        comp = None
        try:
            comp = Comprobante.objects.get(id=idcomprobante)
            detalles_pago = comp.detallepago_set.all()
            fecha_original = detalles_pago[0].FechaPago
        except ValueError:
            mensaje = "El comprobante no existe o no tiene un detalle de pago,verifique por favor"
            links = "<a href='../../" + str(comp.id) + "/'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = ComprobanteAdminForm(request.POST)
            if form.is_valid():
                tipo = form.cleaned_data['TipoComprobante']
                corre = form.cleaned_data['Correlativo']
                fecha = form.cleaned_data['FechaPago']

                comp.Correlativo = corre
                comp.TipoComprobante = tipo
                comp.save()

                if detalles_pago.count() != 0:
                    for det in detalles_pago:
                        det.FechaPago = fecha
                        det.save()

                mensaje = "Comprobante Modificado"
                links = "<a href='../../" + str(comp.id) + "/'>Volver</a>"
                return render_to_response("Pagos/mensaje.html",
                                          {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = ComprobanteAdminForm(
                initial={'TipoComprobante': comp.TipoComprobante.id, 'Correlativo': comp.Correlativo,
                         'selected_tipo': comp.TipoComprobante.Serie.Numero, 'selected_corre': comp.Correlativo,
                         'FechaPago': fecha_original})

        return render_to_response("Pagos/Comprobante/modificar_comprobante.html",
                                  {"form": form, "user": request.user, 'comp': comp},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../../')


def prorroga_pago_matriculado(request, idpagomatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):

        try:
            pago_alumno = PagoAlumno.objects.get(id=idpagomatriculado)
        except PagoAlumno.DoesNotExist:
            mensaje = "El detalle de pago elegido es incorrecto"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = ProrrogaForm(request.POST)
            if form.is_valid():
                prorroga = form.cleaned_data['Prorroga']
                fechaprorroga = form.cleaned_data['FechaProrroga']
                pago_alumno_id = form.cleaned_data['selected_pagoalumno']

                try:
                    pago_alumno = PagoAlumno.objects.get(id=pago_alumno_id)
                    pago_alumno.Prorroga = prorroga
                    pago_alumno.FechaProrroga = fechaprorroga
                    pago_alumno.save()

                    return HttpResponseRedirect('../../%s/' % pago_alumno.MatriculaCiclo_id)
                except PagoAlumno.DoesNotExist:
                    mensaje = "El Pago elegido es incorrecto"
                    links = "<a href='../'>Volver</a>"
                    return render_to_response("Pagos/mensaje.html",
                                              {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = ProrrogaForm(initial={'selected_pagoalumno': idpagomatriculado, "Prorroga": pago_alumno.Prorroga,
                                         "FechaProrroga": pago_alumno.FechaProrroga})
        return render_to_response("Pagos/Individual/prorroga_detallepago.html",
                                  {"form": form, "pago_alumno": pago_alumno, "user": request.user},
                                  context_instance=RequestContext(request))


def exceso_pago_matriculado(request, idpagomatriculado):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.read_pagoalumno') or request.user.has_perm(
            'Pagos.add_pagoalumno') or request.user.has_perm('Pagos.change_pagoalumno'):

        try:
            pago_alumno = PagoAlumno.objects.get(id=idpagomatriculado)
        except PagoAlumno.DoesNotExist:
            mensaje = "El detalle de pago elegido es incorrecto"
            links = "<a href='../'>Volver</a>"
            return render_to_response("Pagos/mensaje.html",
                                      {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})

        if request.method == 'POST':
            form = ExcesoForm(request.POST)
            if form.is_valid():
                monto = form.cleaned_data['Monto']
                pago_alumno_id = form.cleaned_data['selected_pagoalumno']

                try:
                    pago_alumno = PagoAlumno.objects.get(id=pago_alumno_id)
                    pago_alumno.Exceso = monto
                    pago_alumno.save()

                    return HttpResponseRedirect('../../%s/' % pago_alumno.MatriculaCiclo_id)
                except PagoAlumno.DoesNotExist:
                    mensaje = "El Pago elegido es incorrecto"
                    links = "<a href='../'>Volver</a>"
                    return render_to_response("Pagos/mensaje.html",
                                              {"mensaje": mensaje, "links": mark_safe(links), "user": request.user})
        else:
            form = ExcesoForm(initial={'selected_pagoalumno': idpagomatriculado, "Monto": pago_alumno.Exceso})
        return render_to_response("Pagos/Individual/exceso_detallepago.html",
                                  {"form": form, "pago_alumno": pago_alumno, "user": request.user},
                                  context_instance=RequestContext(request))
