# -*- coding: utf-8 -*-
import csv
import datetime
from decimal import Decimal

from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from Pagos.forms import ArchivoBancoForm, ArchivoForm
from Pagos.models import PagoAlumno, DetallePago, ConceptoPago, Comprobante, Mora, BankFile, DetailBankFile

# Declaro variables generales
rep = {'ñ': 'n', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u',
       'Ñ': 'N', 'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U',
       'ä': 'a', 'ë': 'e', 'ï': 'i', 'ö': 'o', 'ü': 'u',
       'Ä': 'A', 'Ë': 'E', 'Ï': 'I', 'Ö': 'O', 'Ü': 'U',
       '*': ' ', 'º': ' ', '¬': ' ', '¡': ' ', '!': ' ', '¿': ' ',
       '?': ' ', '^': ' ', '"': ' ', '-': ' ', '_': ' '}
mn = {'Enero': '01', 'Febrero': '02', 'Marzo': '03', 'Abril': '04', 'Mayo': '05',
      'Junio': '06', 'Julio': '07', 'Agosto': '08', 'Setiembre': '09',
      'Octubre': '10', 'Noviembre': '11', 'Diciembre': '12'}


def info_empresa():
    cod_sucursal = "305"
    moneda = "0"
    # cuenta_recaudadora = "1853001", cuenta antigua
    cuenta_recaudadora = "1916966"
    comb = cod_sucursal + moneda + cuenta_recaudadora
    return comb


def nombre_empresa():
    tipo_validacion = "C"
    nombreempresa = "UNIVERSIDAD DE LAMBAYEQUE"
    tn = tipo_validacion + nombreempresa
    return tn


def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)

    return text


@csrf_protect
def form_archivo_banco(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        if request.method == 'POST':
            form = ArchivoBancoForm(request.POST)
            if form.is_valid():
                fecha = form.cleaned_data['FechaCreacion']
                periodo = form.cleaned_data['Periodo']
                conceptos = form.cleaned_data['ConceptoPago']
                return generar_archivo_banco(fecha, periodo, conceptos)
        else:
            form = ArchivoBancoForm(
                initial={'FechaCreacion': datetime.date.today()})

        return render_to_response("Pagos/Banco/form_archivo_banco.html", {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('../../../../')


def generar_archivo_banco(fecha, periodo, conceptos):
    lista = []
    sumatoria_pago = Decimal("0.00")
    tf = 0
    i = 1
    # Variables del ENCABEZADO (Comienzo)
    inf = info_empresa()
    nomb = str(nombre_empresa()).ljust(41)
    fecha_de_generacion = str(fecha.strftime("%Y%m%d"))
    # Variables del ENCABEZADO (FIN)

    for concepto in conceptos:
        # Variables del DETALLE Y DEL TOTAL(Comienzo)
        cobranza = PagoAlumno.objects.filter(MatriculaCiclo__Estado='Matriculado', Estado=0,
                                             ConceptoPago__PagoBanco=1, ConceptoPago__Periodo=periodo,
                                             ConceptoPago=concepto)
        cont = 9999999
        for cobrar in cobranza:
            i += 1
            postulante = str(cobrar.MatriculaCiclo.Alumno)
            fecha_emision_cuota = str(cobrar.FechaCreacion.strftime("%Y%m%d"))
            fecha_vencimiento = str(
                cobrar.ConceptoPago.FechaVencimiento.strftime("%Y%m%d"))
            codigo_alumno = cobrar.MatriculaCiclo.Alumno.Codigo
            monto_mora = cobrar.ConceptoPago.MontoMora
            monto_cuota = cobrar.DeudaMonto()

            concepto = cobrar.ConceptoPago.Descripcion.split(" ")[0][0:3].encode(
                'utf8') + cobrar.ConceptoPago.Descripcion.split(" ")[1].encode('utf8')
            desc = replace_all(concepto, rep)

            mc = str(monto_cuota).split(".")[
                     0] + str(monto_cuota).split(".")[1]
            # mora = str(monto_mora).split(".")[0] + str(monto_mora).split(".")[1]
            mora = '0'
            nombre = replace_all(postulante, rep)
            id_pagoalumno = str(cobrar.id)
            detalles = DetallePago.objects.filter(Activado=1, PagoAlumno__id=cobrar.id,
                                                  Comprobante__TipoComprobante__Estado=1,
                                                  Comprobante__TipoComprobante__PagoBanco=1).order_by('FechaPago')

            if detalles.count() != 0:
                for var in detalles:
                    id_comprobante = str(var.Comprobante.id)
                    num_comprobante = str(
                        var.Comprobante.TipoComprobante.Serie) + str(var.Comprobante.Correlativo)
            else:
                # num_comprobante = "999" + str(cont)
                # cont = int(cont) - 1
                num_comprobante = ""

            detalle = str(codigo_alumno[0:13]).rjust(14) + str(nombre.upper()[0:39]).ljust(40) + id_pagoalumno.ljust(
                10) + str(desc.upper()[0:9]).ljust(
                10) + num_comprobante.ljust(10) + fecha_emision_cuota + fecha_vencimiento + str(mc).rjust(15).replace(
                " ", "0") + mora.rjust(15).replace(" ", "0") + "A".rjust(10)
            lista.append(detalle)
            sumatoria_pago = Decimal(monto_cuota) + sumatoria_pago
    # Variables del DETALLE Y DEL TOTAL(FIN)

    # Creamos un archivo de texto plano
    nombre_archivo = "CREP" + ".txt"
    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % nombre_archivo
    writer = csv.writer(response)
    for num in lista:
        tf += 1
    total_filas = str(tf).rjust(9).replace(" ", "0")
    sm = str(sumatoria_pago)
    sm2 = sm.split(".")[0] + sm.split(".")[1]
    suma_pagos = str(sm2).rjust(15).replace(" ", "0")

    A = 'A'
    writer.writerow(
        ["CC" + inf + nomb + fecha_de_generacion + total_filas + suma_pagos + A])
    for num in lista:
        writer.writerow(["DD" + inf + num])
    return response


def handle_uploaded_file(f, tipo_comp, corre_inicio):
    fecha = datetime.date.today()
    nombre_archivo = f.name
    ext_archivo = str(nombre_archivo).lower()[-3:]
    ruta_archivo = str(settings.MEDIA_ROOT) + "ArchivosBanco"
    # ruta_archivo = str(settings.MEDIA_ROOT) + "/ArchivosBanco"
    nombre_txt = str(fecha) + ".txt"
    con_detalle = []
    sin_detalle = []
    moras = []

    if ext_archivo == "txt":
        destination = open('%s/%s' % (ruta_archivo, nombre_txt), 'wb+')

        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()

        archivo = open('%s/%s' % (ruta_archivo, nombre_txt), 'rb')
        ids_moras = []
        cdpg_text = open('%s/%s' %
                         (ruta_archivo, nombre_txt), 'rb').readlines()
        for line in archivo:
            linea = str(line)
            identificacion = str(linea[:2])

            if identificacion == "CC":
                recaudo_anio = int(linea[14:18])
                recaudo_mes = int(linea[18:20])
                recaudo_dia = int(linea[20:22])

                thebankfile = BankFile(cdpg_data=cdpg_text,
                                       cdpg_cuenta=linea[2:14],
                                       cdpg_codigo_interno=linea[46:50],
                                       cdpg_fecha=datetime.date(
                                           recaudo_anio, recaudo_mes, recaudo_dia),
                                       cdpg_registros=int(linea[22:31]),
                                       cdpg_monto_total=Decimal(
                                           linea[31:44] + "." + linea[44:46]),
                                       cdpg_codigo_teletransfer=linea[50:56])
                thebankfile.save()
                continue

            if identificacion == "DD":
                detalle = linea[2:13]
                codigo = str(linea[20:27])
                retorno1_id_pago = int(linea[27:37])
                retorno2_desc = linea[37:47]

                # solo tomo la serie no el correlativo
                retorno3_comp = linea[47:50]
                # solo tomo el correlativo no la serie
                retorno4_comp = linea[50:57]
                # Debido a que en algunas ocaciones el archivo contiene un error (contiene un espacio donde debería haber un cero)
                # la siguiente línea agrega un cero en el lugar correcto.
                try:
                    retorno4_comp = str(int(retorno4_comp)).rjust(
                        7).replace(" ", "0")
                except ValueError:
                    retorno4_comp = str(" ").rjust(7).replace(" ", "0")

                pago_anio = int(linea[57:61])
                pago_mes = int(linea[61:63])
                pago_dia = int(linea[63:65])

                vence_anio = int(linea[65:69])
                vence_mes = int(linea[69:71])
                vence_dia = int(linea[71:73])

                # descomentar linea de abajo
                # monto_pagado = linea[73:86] + "." + linea[86:88]
                monto_mora = linea[88:101] + "." + linea[101:103]
                monto_total_pagado = linea[103:116] + "." + linea[116:118]

                # Grabar en BD
                hora_1 = int(linea[168:170])
                hora_2 = int(linea[170:172])
                hora_3 = int(linea[172:174])
                DetailBankFile(cabecera_id=thebankfile.id,
                               codigo=codigo,
                               retorno=linea[27:57],
                               fecha_pago=datetime.date(
                                   pago_anio, pago_mes, pago_dia),
                               fecha_vencimiento=datetime.date(
                                   vence_anio, vence_mes, vence_dia),
                               monto_pagado=Decimal(
                                   linea[73:86] + "." + linea[86:88]),
                               mora_pagada=Decimal(monto_mora),
                               monto_total=Decimal(monto_total_pagado),
                               sucursal=linea[118:124],
                               num_operacion=linea[124:130],
                               referencia=linea[130:152],
                               terminal=linea[152:156],
                               hora_atencion=datetime.time(hora_1, hora_2, hora_3)).save()
                #

                pagoalumno = PagoAlumno.objects.get(id=retorno1_id_pago)
                print pagoalumno.id
                # Lineas agregadas para el asunto de la matrícula 2017 - II acerca del pago extra por Aniversario
                # Automatización del proceso de generacion de boletas en el sistema
                conceptopago_aux = ConceptoPago.objects.get(id=pagoalumno.ConceptoPago_id)
                print conceptopago_aux.MontoPago
                print monto_total_pagado
                if (conceptopago_aux.id == 201 or conceptopago_aux.id == 209 or conceptopago_aux.id == 210) and Decimal(
                        monto_total_pagado) > conceptopago_aux.MontoPago:
                    print "aquí"
                    pagoalumno2 = PagoAlumno.objects.get(MatriculaCiclo_id=pagoalumno.MatriculaCiclo_id,
                                                         ConceptoPago_id=207)
                    print pagoalumno2.id
                    obtener_detalle_pago_lista = DetallePago.objects.filter(
                        PagoAlumno__id=pagoalumno2.id, Mora=False, Activado=True).order_by('-id')

                    if obtener_detalle_pago_lista.count() == 0:
                        print "control2"
                        nuevo_comprobante = Comprobante(Correlativo=str(corre_inicio).rjust(7).replace(
                            " ", "0"), TipoComprobante=tipo_comp, LugarPago="Banco",
                            FechaEmision=datetime.date(pago_anio, pago_mes, pago_dia))
                        nuevo_comprobante.save()

                        cobrar_detalle_pago = DetallePago(MontoDeuda=pagoalumno2.DeudaMonto(),
                                                          MontoCancelado=Decimal(
                                                              pagoalumno2.ConceptoPago.MontoPago), PagoRestante=0.00,
                                                          FechaPago=datetime.date(pago_anio, pago_mes, pago_dia),
                                                          PagoAlumno=pagoalumno2, Comprobante=nuevo_comprobante)
                        cobrar_detalle_pago.save()
                        corre_inicio = int(corre_inicio) + 1
                        sin_detalle.append(cobrar_detalle_pago)
                        # pago = PagoAlumno.objects.get(id=retorno1_id_pago)
                        pago = PagoAlumno.objects.get(MatriculaCiclo_id=pagoalumno.MatriculaCiclo_id,
                                                      ConceptoPago_id=207)
                        print pago.id
                        # if Decimal(resta) == Decimal('0.00') and Decimal(monto_mora) == Decimal('0.00'):
                        pago.Estado = True
                        pago.save()
                        print "control"

                # quitar linea de abajo
                monto_pagado = Decimal(pagoalumno.DeudaMonto())
                resta = Decimal(pagoalumno.DeudaMonto()) - \
                        Decimal(monto_pagado)

                obtener_detalle_pago_lista = DetallePago.objects.filter(
                    PagoAlumno__id=retorno1_id_pago, Mora=False, Activado=True).order_by('-id')

                if obtener_detalle_pago_lista.count() != 0:
                    obtener_detalle_pago = obtener_detalle_pago_lista[0]
                    if obtener_detalle_pago.PagoRestante != Decimal("0.00"):
                        obtener_detalle_pago.PagoRestante = resta
                        obtener_detalle_pago.MontoCancelado = Decimal(
                            monto_pagado)
                        obtener_detalle_pago.FechaPago = datetime.date(
                            pago_anio, pago_mes, pago_dia)
                        obtener_detalle_pago.save()
                    con_detalle.append(obtener_detalle_pago)
                else:
                    nuevo_comprobante = Comprobante(Correlativo=str(corre_inicio).rjust(7).replace(
                        " ", "0"), TipoComprobante=tipo_comp, LugarPago="Banco",
                        FechaEmision=datetime.date(pago_anio, pago_mes, pago_dia))
                    nuevo_comprobante.save()

                    cobrar_detalle_pago = DetallePago(MontoDeuda=pagoalumno.DeudaMonto(), MontoCancelado=Decimal(
                        monto_pagado), PagoRestante=resta, FechaPago=datetime.date(pago_anio, pago_mes, pago_dia),
                                                      PagoAlumno=pagoalumno, Comprobante=nuevo_comprobante)
                    cobrar_detalle_pago.save()
                    corre_inicio = int(corre_inicio) + 1
                    sin_detalle.append(cobrar_detalle_pago)
                pago = PagoAlumno.objects.get(id=retorno1_id_pago)
                if Decimal(resta) == Decimal('0.00') and Decimal(monto_mora) == Decimal('0.00'):
                    pago.Estado = True
                    pago.save()

                # Primero debo verificar que no existan registros de mora del alumno, en caso de que exista alguno
                # actualizo la data, caso contrario creo un nuevo registro.
                mora = Mora.objects.filter(PagoAlumno__id=retorno1_id_pago)
                if mora.count() == 0 and Decimal(monto_mora) != Decimal("0.00"):
                    # Crear registros
                    crear_mora = Mora(Monto=monto_mora,
                                      PagoAlumno_id=retorno1_id_pago)
                    crear_mora.save()
                    ids_moras.append([retorno1_id_pago, monto_mora])
                else:
                    # Actualizar registros existentes
                    if Decimal(monto_mora) != Decimal("0.00"):
                        update_mora = Mora.objects.get(
                            PagoAlumno__id=retorno1_id_pago)
                        update_mora.Monto = monto_mora
                        update_mora.Estado = False
                        update_mora.save()
                        ids_moras.append([retorno1_id_pago, monto_mora])

                        # codigo generado solo para aquellos que han pagado su matricula cachimbos
                        # se matricula automaticamente en los cursos de 1er ciclo de su carrera
                        # if pago.MatriculaCiclo.Alumno.AnioIngreso == '2014' and pago.MatriculaCiclo.Alumno.Semestre == 'I':
                        # cursos = PeriodoCurso.objects.filter(Curso__Carrera = pago.MatriculaCiclo.Alumno.Carrera, Curso__Ciclo = '1',Periodo = pago.MatriculaCiclo.Periodo)
                        # cursos_mat = MatriculaCursos.objects.filter(MatriculaCiclo = pago.MatriculaCiclo)
                        # if cursos_mat.count() == 0:
                        # for curso in cursos:
                        # nueva_mat = MatriculaCursos(MatriculaCiclo = pago.MatriculaCiclo,PeriodoCurso = curso)
                        # nueva_mat.save()

        for id_mora in ids_moras:
            pagoalumno = PagoAlumno.objects.get(id=id_mora[0])
            if pagoalumno.Estado == False:
                nuevo_comprobante = Comprobante(Correlativo=str(corre_inicio).rjust(7).replace(
                    " ", "0"), TipoComprobante=tipo_comp, LugarPago="Caja", FechaEmision=datetime.date.today())
                nuevo_comprobante.save()

                cobrar_detalle_pago = DetallePago(Mora=True, MontoDeuda=Decimal(id_mora[1]), MontoCancelado=Decimal(
                    id_mora[1]), PagoRestante=Decimal('0.00'), FechaPago=datetime.date.today(), PagoAlumno=pagoalumno,
                                                  Comprobante=nuevo_comprobante)
                cobrar_detalle_pago.save()
                pagoalumno.Estado = True
                pagoalumno.save()
                corre_inicio = int(corre_inicio) + 1
                moras.append(cobrar_detalle_pago)
        return True, sin_detalle, con_detalle, moras
    else:
        return False, sin_detalle, con_detalle, moras


@csrf_protect
def upload_archivo_banco(request):
    if request.user.is_authenticated() and request.user.has_perm('Pagos.add_pagoalumno'):
        text = " "
        if request.method == 'POST':
            form = ArchivoForm(request.POST, request.FILES)
            if form.is_valid():
                f = request.FILES['Archivo']
                tipo_comp = form.cleaned_data['TipoComp']
                corre_inicio = form.cleaned_data['Correlativo']
                estado, nuevos_detalles, detalles, moras = handle_uploaded_file(
                    f, tipo_comp, corre_inicio)
                if estado is True:
                    return render_to_response("Pagos/Banco/reporte_pagosbanco.html",
                                              {"nuevos_detalles": nuevos_detalles, "detalles": detalles, "moras": moras,
                                               "user": request.user}, context_instance=RequestContext(request))
        else:
            form = ArchivoForm()

        return render_to_response('Pagos/Banco/form_upload_archivo.html', {"form": form, "user": request.user},
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect("../../../../")


# metodo que genera el archivo banco con un solo clic


def generar_archivo_banco_auto(request):
    fecha = datetime.date.today()
    lista = []
    sumatoria_pago = Decimal("0.00")
    tf = 0
    i = 1
    # Variables del ENCABEZADO (Comienzo)
    inf = info_empresa()
    nomb = str(nombre_empresa()).ljust(41)
    fecha_de_generacion = str(fecha.strftime("%Y%m%d"))
    # Variables del ENCABEZADO (FIN)
    conceptos = ConceptoPago.objects.filter(PagoBanco=True)

    for concepto in conceptos:
        id_concepto = concepto.id
        cobranza = PagoAlumno.objects.filter(
            MatriculaCiclo__Estado='Matriculado', Estado=False, ConceptoPago=concepto)
        cont = 9999999
        for cobrar in cobranza:
            i += 1
            postulante = str(cobrar.MatriculaCiclo.Alumno)
            fecha_emision_cuota = str(cobrar.FechaCreacion.strftime("%Y%m%d"))
            fecha_vencimiento = str(cobrar.ConceptoPago.FechaVencimiento.strftime("%Y%m%d"))
            codigo_alumno = cobrar.MatriculaCiclo.Alumno.Codigo
            monto_mora = cobrar.ConceptoPago.MontoMora
            monto_cuota = cobrar.DeudaMonto()
            cachimbo = False
            ingreso = cobrar.MatriculaCiclo.Alumno.AnioIngreso + \
                      u' - ' + cobrar.MatriculaCiclo.Alumno.Semestre
            semestre_concepto = cobrar.ConceptoPago.Periodo.__unicode__()
            if ingreso == semestre_concepto:
                cachimbo = True

            m_t = {132: "20150331", 134: "20150329",
                   136: "20150330", 145: "20150328", 139: "20150427"}
            if id_concepto in m_t:
                fecha_vencimiento = m_t[id_concepto]
                print codigo_alumno, cachimbo

            m_t = {151: "20150829", 152: "20150828"}
            if id_concepto in m_t:
                fecha_vencimiento = m_t[id_concepto]
                print codigo_alumno, cachimbo

            m_t = {163: "20210331", 164: "20210330",
                   166: "20210329", 167: "20210328"}
            if id_concepto in m_t:
                fecha_vencimiento = m_t[id_concepto]
                print codigo_alumno, cachimbo

            m_t = {177: "20161231", 185: "20161230"}
            if id_concepto in m_t:
                fecha_vencimiento = m_t[id_concepto]
                print codigo_alumno, cachimbo

            m_t = {187: "20170330", 193: "20170401", 194: "20170402"}
            if id_concepto in m_t:
                fecha_vencimiento = m_t[id_concepto]
                print codigo_alumno, cachimbo

            cadena = cobrar.ConceptoPago.Descripcion.split(" ")
            concepto = ""
            if len(cadena) > 1:
                concepto = cobrar.ConceptoPago.Descripcion.split(" ")[0][0:3].encode('utf8') + \
                           cobrar.ConceptoPago.Descripcion.split(" ")[1].encode('utf8')
            else:
                concepto = cobrar.ConceptoPago.Descripcion.split(" ")[0].encode('utf8')
            desc = replace_all(concepto, rep)

            mc = str(monto_cuota).split(".")[
                     0] + str(monto_cuota).split(".")[1]
            mora = '0'

            nombre = replace_all(postulante, rep)
            id_pagoalumno = str(cobrar.id)
            detalles = DetallePago.objects.filter(Activado=1, PagoAlumno__id=cobrar.id,
                                                  Comprobante__TipoComprobante__Estado=1,
                                                  Comprobante__TipoComprobante__PagoBanco=1).order_by('FechaPago')

            if detalles.count() != 0:
                for var in detalles:
                    id_comprobante = str(var.Comprobante.id)
                    num_comprobante = str(
                        var.Comprobante.TipoComprobante.Serie) + str(var.Comprobante.Correlativo)
            else:
                # num_comprobante = "999" + str(cont)
                # cont = int(cont) - 1
                num_comprobante = ""

            detalle = str(codigo_alumno[0:13]).rjust(14) + str(nombre.upper()[0:39]).ljust(40) + id_pagoalumno.ljust(
                10) + str(desc.upper()[0:9]).ljust(
                10) + num_comprobante.ljust(10) + fecha_emision_cuota + fecha_vencimiento + str(mc).rjust(15).replace(
                " ", "0") + mora.rjust(15).replace(" ", "0") + "A".rjust(10)
            lista.append(detalle)
            sumatoria_pago = Decimal(monto_cuota) + sumatoria_pago
    # Variables del DETALLE Y DEL TOTAL(FIN)

    # Creamos un archivo de texto plano
    nombre_archivo = "CREP" + ".txt"
    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % nombre_archivo
    writer = csv.writer(response)
    for num in lista:
        tf += 1
    total_filas = str(tf).rjust(9).replace(" ", "0")
    sm = str(sumatoria_pago)
    sm2 = sm.split(".")[0] + sm.split(".")[1]
    suma_pagos = str(sm2).rjust(15).replace(" ", "0")

    A = 'R'
    writer.writerow(
        ["CC" + inf + nomb + fecha_de_generacion + total_filas + suma_pagos + A])
    for num in lista:
        writer.writerow(["DD" + inf + num])
    return response
