# -*- coding: utf-8 -*-
import datetime
from decimal import Decimal

from django.db import models
from django.conf import settings

from Categorias.models import Categoria
from Configuraciones.models import Serie, TipoComprobante
from Matriculas.models import MatriculaCiclo
from Periodos.models import Periodo
from UDL.facturacion_webservice import facturacion_webservices

MORA_CHOICES = (
    ('Ninguna', 'Ninguna'),
    ('Diaria', 'Diaria'),
    ('Semanal', 'Semanal'),
    ('Mensual', 'Mensual'),
)

MONEDA_CHOICES = (
    ('S/.', 'Nuevo Sol'),
    ('US$', 'Dólar'),
    ('€', 'Euro'),
)

LUGAR_CHOICES = (
    ('Caja', 'Caja'),
    ('Banco', 'Banco'),
    ('Banco2', 'Banco2'),
    ('Depósito', 'Depósito'),
    ('Planilla', 'Planilla'),
)


class DescuentoPago(models.Model):
    Periodo = models.ForeignKey(Periodo, verbose_name="Período", null=True, blank=True)
    Categoria = models.ManyToManyField(Categoria)
    Descripcion = models.CharField("Descripción", max_length=24)
    FechaLimite = models.PositiveIntegerField(verbose_name="Días antes")
    MontoDescuento = models.DecimalField("Descuento", max_digits=6, decimal_places=2)

    def __unicode__(self):
        return u'%s %s' % (self.Descripcion, self.Periodo)

    class Meta:
        verbose_name = "Descuento de pago"
        verbose_name_plural = "Descuentos de pago"


class ConceptoPago(models.Model):
    Periodo = models.ForeignKey(Periodo, verbose_name="Período")
    Descripcion = models.CharField("Descripción", max_length=100)
    MontoPago = models.DecimalField("Monto", max_digits=8, decimal_places=2)
    Moneda = models.CharField(max_length=4, choices=MONEDA_CHOICES, default="Nuevo Sol")
    TipoMora = models.CharField("Tipo Mora ", max_length=20, choices=MORA_CHOICES)
    MontoMora = models.DecimalField("Monto Mora", max_digits=4, decimal_places=2)
    FechaVencimiento = models.DateField(verbose_name="Vence Pago")
    Descuentos = models.ManyToManyField(DescuentoPago, verbose_name="Descuentos", blank=True)
    PagoBanco = models.BooleanField("¿Se paga en el Banco?", default=False)
    Unico = models.BooleanField("¿Es único?", default=True)

    def __unicode__(self):
        return u'%s' % self.Descripcion

    def Monto(self):
        # return u'%s %s' % (self.Moneda,self.MontoPago)
        return Decimal(self.MontoPago)

    def Vence(self):
        if self.TipoMora == "Ninguna":
            valor = ""
            return valor
        else:
            return self.FechaVencimiento

    Vence.short_description = 'Vence Pago'

    class Meta:
        verbose_name = "Concepto de Pago"
        verbose_name_plural = "Conceptos de Pago"


class PagoAlumno(models.Model):
    ConceptoPago = models.ForeignKey(ConceptoPago, verbose_name="Concepto de Pago")
    MatriculaCiclo = models.ForeignKey(MatriculaCiclo, verbose_name="Alumno")
    Estado = models.BooleanField("¿Cancelado?", default=False, editable=False)
    IgnorarCategoria = models.BooleanField("¿Ignorar Categoría?", default=False, editable=False)
    FechaCreacion = models.DateTimeField(editable=False, auto_now_add=True)
    Prorroga = models.BooleanField("¿Prórroga?", default=False, editable=False)
    FechaProrroga = models.DateField("Fecha prórroga", default=None, null=True, blank=True)
    Exceso = models.DecimalField("Exceso", max_digits=8, decimal_places=2, default=None, null=True)
    Usado = models.BooleanField(default=False)

    def monto_facturacion(self):
        if "Pension" in self.ConceptoPago.Descripcion.encode('utf-8'):
            return self.MatriculaCiclo.Categoria.Pago
        else:
            return self.ConceptoPago.MontoPago

    def facturacion_json_pago(self, usuario):
        datos_for_facturacion = {
            "cPerJurCodigo": "1000003456",
            "cPerAluCodigo": str(self.MatriculaCiclo.Alumno.WbServiceId),
            "nPerEstCodigo": 1,
            "nImporte": float(self.monto_facturacion()),
            "dFecVencimiento": str(self.ConceptoPago.FechaVencimiento),
            "nPeriodo": 1,
            "cPerUsuRegistra": str(usuario),
            "nArtMasCodigo": 2,
            "cNombreProceso": self.ConceptoPago.Descripcion.encode('utf-8'),
            "nCodigoReferencia": 0,
            "cObservacion": "",
        }
        return datos_for_facturacion

    def save(self, **kwargs):
        if settings.ACTIVAR_SERVICIOS_FACTURACION == 1:
            usuario = None
            if kwargs is not None and kwargs['usuario'] is not None:
                usuario = kwargs['usuario']
            try:
                if usuario is not None:
                    json_pago = self.facturacion_json_pago(usuario)
                    print(json_pago)
                    print ("GenerarCargo")
                    id_fac = facturacion_webservices("GenerarCargo", json_pago)
                    print(id_fac)
            except:
                print('error')
        return super(PagoAlumno, self).save()


    def __unicode__(self):
        return u'%s - %s' % (self.MatriculaCiclo.Alumno, self.ConceptoPago.Descripcion)

    def EstadoProrroga(self):
        if self.FechaProrroga:
            if self.FechaProrroga >= datetime.date.today():
                return True
            else:
                return False
        else:
            return False

    def Monto(self):
        totaldescuento = 0
        montoapagar = 0.00
        descuentos = self.ConceptoPago.Descuentos.all()
        for descuento in descuentos:
            categorias_con_descuento = descuento.Categoria.all()
            if self.MatriculaCiclo.Categoria in categorias_con_descuento:
                if ((self.ConceptoPago.FechaVencimiento - datetime.timedelta(
                        days=descuento.FechaLimite)) >= datetime.date.today()):
                    totaldescuento += descuento.MontoDescuento

        if self.IgnorarCategoria is True:
            detalles = self.detallepago_set.filter(Mora=False).order_by('id')
            if detalles.count() == 0:
                # return u'%s %s' % (self.ConceptoPago.Moneda,self.ConceptoPago.MontoPago)
                montoapagar = Decimal(self.ConceptoPago.MontoPago - totaldescuento)
            else:
                monto_deuda = detalles[0].MontoDeuda
                # return u'%s %s' % (self.ConceptoPago.Moneda, monto_deuda)
                montoapagar = Decimal(monto_deuda - totaldescuento)

        else:
            desc = self.ConceptoPago.Descripcion.lower()
            encontrar = desc.find(u'pensión')
            encontrar1 = desc.find(u'pension')

            # detalles = self.detallepago_set.filter(Mora = False).order_by('-FechaPago')
            detalles = self.detallepago_set.filter(Mora=False).order_by('id')
            if detalles.count() == 0:
                if encontrar == -1:
                    if encontrar1 == -1:
                        montoapagar = Decimal(self.ConceptoPago.MontoPago - totaldescuento)
                    else:
                        montoapagar = (self.MatriculaCiclo.Categoria.pago() - totaldescuento)
                else:
                    montoapagar = (self.MatriculaCiclo.Categoria.pago() - totaldescuento)
            else:
                monto_deuda = detalles[0].MontoDeuda
                # return u'%s %s' % (self.ConceptoPago.Moneda, monto_deuda)
                montoapagar = Decimal(monto_deuda)
        if self.Exceso is not None:
            montoapagar = montoapagar + self.Exceso
        return montoapagar

    def DeudaMonto(self):
        detalles = self.detallepago_set.filter(Mora=False, Activado=True)
        reps = {'S/.': '', 'US$': '', '€': '', ' ': ''}
        # monto_total = str(self.Monto())
        monto_total = self.Monto()
        # for i,j in reps.iteritems():
        #    monto_total = monto_total.replace(i,j)

        if detalles.count() == 0:
            return monto_total
        else:
            suma_pagos_hechos = Decimal('0.00')
            for detalle in detalles:
                suma_pagos_hechos += Decimal(detalle.MontoCancelado)

            deuda = Decimal(monto_total) - Decimal(suma_pagos_hechos)
            if deuda != Decimal('0.00'):
                self.Estado = False
                self.save(usuario=None)
            return Decimal(deuda)

    def PagoMatricula(self):
        # Obtengo la matricula del alumno
        from Pagos.models import PagoAlumno, DetallePago

        try:
            q = PagoAlumno.objects.get(ConceptoPago__Descripcion__startswith="Matr", MatriculaCiclo=self.MatriculaCiclo)
            # Ahora debo saber si el alumno ya pagó su matrícula (ya sea que haya pagado todo o una parte)
            m = "no"
            monto = str(q.Monto()).replace("S/. ", "")
            if Decimal(q.DeudaMonto()) < Decimal(monto):
                # Ahora me falta saber la fecha en qué pagó dicha matrícula
                try:
                    detalles = DetallePago.objects.get(Mora=False, PagoAlumno__id=q.id, FechaPago__lte='2011-08-31')
                    m = str(detalles.FechaPago)
                except:
                    m = "no"

        except PagoAlumno.DoesNotExist:
            m = "no"

        return m

    def fechapagomatricula(self):
        # Obtengo la matricula del alumno
        from Pagos.models import PagoAlumno, DetallePago

        try:
            q = PagoAlumno.objects.get(ConceptoPago__Descripcion__icontains="Matrícula",
                                       MatriculaCiclo=self.MatriculaCiclo)
            # Ahora debo saber si el alumno ya pagó su matrícula (ya sea que haya pagado todo o una parte)
            m = "no"
            monto = str(q.Monto()).replace("S/. ", "")
            if Decimal(q.DeudaMonto()) < Decimal(monto):
                # Ahora me falta saber la fecha en qué pagó dicha matrícula
                try:
                    detalles = DetallePago.objects.filter(Mora=False, PagoAlumno__id=q.id).order_by('FechaPago')
                    m = str(detalles[0].FechaPago)
                except ValueError:
                    m = "no"

        except (PagoAlumno.DoesNotExist, PagoAlumno.MultipleObjectsReturned):
            print ("ERROR" + self.MatriculaCiclo.Alumno.Codigo)
            m = "no"

        return m

    def Mora(self):
        if self.MatriculaCiclo.Estado == 'Matriculado':
            if self.ConceptoPago.Vence() != "":
                hoy = datetime.date.today()
                vence = self.ConceptoPago.FechaVencimiento
                monto_mora = self.ConceptoPago.MontoMora

                if self.detallepago_set.filter(Mora=True, Activado=True).count() == 0:
                    if self.detallepago_set.filter(Mora=False, Activado=True).count() != 0:
                        detalle = self.detallepago_set.filter(Mora=False, Activado=True).order_by('-id')[0]
                        if Decimal(self.DeudaMonto()) == Decimal('0.00'):
                            fecha_actual = detalle.FechaPago
                        else:
                            fecha_actual = hoy
                    else:
                        fecha_actual = hoy

                    if vence < fecha_actual and self.Estado is False:
                        diferencia = fecha_actual - vence
                        valor_mora = diferencia.days * monto_mora
                        if self.mora_set.all().count() == 0:
                            crear_mora = Mora(Monto=Decimal(valor_mora), Exonera=False, Estado=False, PagoAlumno=self)
                            crear_mora.save()
                            # return u'%s %s' % ('S/.',crear_mora.Monto)
                            return Decimal(crear_mora.Monto)
                        else:
                            mora = self.mora_set.all()[0]
                            if mora.Exonera is False:
                                mora.Monto = Decimal(valor_mora)
                                mora.save()
                                # return u'%s %s' % ('S/.',mora.Monto)
                                return Decimal(mora.Monto)
                            else:
                                # return u'%s %s' % ('S/.','0.00')
                                return Decimal('0.00')
                    else:
                        if self.mora_set.all().count() != 0:
                            for mora in self.mora_set.all():
                                mora.delete()
                        # return u'%s %s' % ('S/.','0.00')
                        return Decimal('0.00')
                else:
                    detalle_mora = self.detallepago_set.filter(Mora=True, Activado=True)[0]
                    if self.mora_set.all().count() == 0:
                        crear_mora = Mora(Monto=detalle_mora.MontoCancelado, Exonera=False, Estado=True,
                                          PagoAlumno=self)
                        crear_mora.save()
                        # return u'%s %s' % ('S/.',crear_mora.Monto)
                        return Decimal(crear_mora.Monto)
                    else:
                        mora = self.mora_set.all()[0]
                        mora.Monto = detalle_mora.MontoCancelado
                        mora.Estado = True
                        mora.save()
                        # return u'%s %s' % ('S/.',mora.Monto)
                        return Decimal(mora.Monto)
            else:
                detalle_mora = self.detallepago_set.filter(Mora=True, Activado=True)
                if detalle_mora.count() == 0:
                    if self.mora_set.all().count() != 0:
                        for mora in self.mora_set.all():
                            mora.delete()
                # return u'%s %s' % ('S/.','0.00')
                return Decimal('0.00')
        else:
            if self.mora_set.all().count() == 0:
                # return u'%s %s' % ('S/.','0.00')
                return Decimal('0.00')
            else:
                mora = self.mora_set.all()[0]
            # return u'%s %s' % ('S/.',mora.Monto)
            return Decimal(mora.Monto)

    def DeudaMora(self):
        if self.mora_set.all().count() == 0:
            cero = Decimal("0.00")
            return Decimal(cero)
        else:
            mora = self.mora_set.all()[0]
            if mora.Exonera is True or mora.Estado is True:
                cero = Decimal("0.00")
                return cero
            else:
                return Decimal(mora.Monto)

    def TotalMora(self):
        if self.mora_set.all().count() == 0:
            cero = Decimal("0.00")
            return cero
        else:
            return Decimal(self.mora_set.all()[0].Monto)

    def ComprobanteMora(self):
        detalle = self.detallepago_set.filter(Mora=True)
        if detalle.count() == 0:
            return ""
        else:
            return detalle[0].Comprobante.Correlativo

    class Meta:
        verbose_name = "Asignación de Pagos"
        verbose_name_plural = "Asignación de Pagos"
        permissions = (("read_pagoalumno", "Observar Pagos de Alumnos UDL"), ("update_prorroga", "Editar prorroga"),
                       ("update_exceso", "Editar exceso"),)


class Comprobante(models.Model):
    Correlativo = models.CharField("Correlativo", max_length=7)
    TipoComprobante = models.ForeignKey(TipoComprobante)
    Glosa = models.CharField("Glosa", max_length=100)
    LugarPago = models.CharField("Lugar de Pago", max_length=10, choices=LUGAR_CHOICES)
    FechaEmision = models.DateField(blank=True, null=True)

    def __unicode__(self):
        return u'%s %s - %s' % (self.TipoComprobante.Tipo, self.TipoComprobante.Serie, self.Correlativo)

    def ConceptosPagados(self):
        detalle = self.detallepago_set.all()
        conceptos = []
        for det in detalle:
            conceptos.append(det)
        return conceptos

    class Meta:
        verbose_name = "Comprobante de Pago"
        verbose_name_plural = "Comprobantes de Pago"
        unique_together = (("Correlativo", "TipoComprobante"),)


class DetallePago(models.Model):
    Mora = models.BooleanField("¿Mora?", default=False)
    MontoDeuda = models.DecimalField("Deuda", max_digits=6, decimal_places=2)
    MontoCancelado = models.DecimalField("Cancela", max_digits=6, decimal_places=2)
    PagoRestante = models.DecimalField("Resta", max_digits=6, decimal_places=2)
    FechaPago = models.DateField()
    PagoAlumno = models.ForeignKey(PagoAlumno, verbose_name="Concepto")
    Comprobante = models.ForeignKey(Comprobante, verbose_name="Comprobante")
    Activado = models.BooleanField("¿Activada?", default=True, editable=False)
    def __unicode__(self):
        return u'%s - %s' % (self.PagoAlumno, self.MontoCancelado)

    class Meta:
        verbose_name = "Pago de Alumnos UDL"
        verbose_name_plural = "Pagos de Alumnos UDL"


class Mora(models.Model):
    Monto = models.DecimalField("Monto", max_digits=6, decimal_places=2)
    Exonera = models.BooleanField("¿Exonerada?", default=False)
    Estado = models.BooleanField("¿Cancelada?", default=False)
    PagoAlumno = models.ForeignKey(PagoAlumno, verbose_name="Concepto")

    def __unicode__(self):
        return u'%s' % self.Monto

    class Meta:
        verbose_name = "Mora"
        verbose_name_plural = "Moras"


class BankFile(models.Model):
    cdpg_data = models.TextField()
    cdpg_cuenta = models.CharField(max_length=24)
    cdpg_codigo_interno = models.CharField(max_length=10)
    cdpg_fecha = models.DateField()
    cdpg_registros = models.PositiveIntegerField()
    cdpg_monto_total = models.DecimalField(max_digits=15, decimal_places=2)
    cdpg_codigo_teletransfer = models.CharField(max_length=10)
    date_upload = models.DateTimeField(auto_now_add=True)

    def duplicados(self):
        datos = BankFile.objects.filter(cdpg_fecha=self.cdpg_fecha)
        if datos.count() == 1:
            return None
        else:
            return datos.count()


class DetailBankFile(models.Model):
    cabecera = models.ForeignKey(BankFile)
    codigo = models.CharField(max_length=14)
    retorno = models.CharField(max_length=30)
    # dato_adicional = models.CharField(max_length=5)
    fecha_pago = models.DateField()
    fecha_vencimiento = models.DateField()
    monto_pagado = models.DecimalField(max_digits=15, decimal_places=2)
    mora_pagada = models.DecimalField(max_digits=15, decimal_places=2)
    monto_total = models.DecimalField(max_digits=15, decimal_places=2)
    sucursal = models.PositiveIntegerField()
    num_operacion = models.PositiveIntegerField()
    referencia = models.CharField(max_length=22)
    terminal = models.CharField(max_length=4, blank=True, null=True)
    medio_atencion = models.CharField(max_length=12, blank=True, null=True)
    hora_atencion = models.TimeField()
    num_cheque = models.PositiveIntegerField(blank=True, null=True)
    codigo_banco = models.PositiveSmallIntegerField(blank=True, null=True)
