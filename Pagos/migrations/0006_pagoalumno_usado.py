# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Pagos', '0005_auto_20180611_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagoalumno',
            name='Usado',
            field=models.BooleanField(default=False),
        ),
    ]
