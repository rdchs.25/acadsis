# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Pagos', '0003_pagoalumno_exceso'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagoalumno',
            name='Exceso',
            field=models.DecimalField(default=None, null=True, verbose_name=b'Exceso', max_digits=8, decimal_places=2),
        ),
    ]
