# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Pagos', '0002_auto_20180306_1913'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagoalumno',
            name='Exceso',
            field=models.DecimalField(default=None, null=True, verbose_name=b'Exceso', max_digits=8, decimal_places=4),
        ),
    ]
