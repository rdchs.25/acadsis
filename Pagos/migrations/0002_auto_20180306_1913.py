# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Pagos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conceptopago',
            name='MontoPago',
            field=models.DecimalField(verbose_name=b'Monto', max_digits=8, decimal_places=2),
        ),
    ]
