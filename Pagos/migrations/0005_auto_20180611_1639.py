# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Pagos', '0004_auto_20180611_0840'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pagoalumno',
            options={'verbose_name': 'Asignaci\xf3n de Pagos', 'verbose_name_plural': 'Asignaci\xf3n de Pagos', 'permissions': (('read_pagoalumno', 'Observar Pagos de Alumnos UDL'), ('update_prorroga', 'Editar prorroga'), ('update_exceso', 'Editar exceso'))},
        ),
    ]
