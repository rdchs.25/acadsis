# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Categorias', '0001_initial'),
        ('Matriculas', '0001_initial'),
        ('Configuraciones', '0001_initial'),
        ('Periodos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cdpg_data', models.TextField()),
                ('cdpg_cuenta', models.CharField(max_length=24)),
                ('cdpg_codigo_interno', models.CharField(max_length=10)),
                ('cdpg_fecha', models.DateField()),
                ('cdpg_registros', models.PositiveIntegerField()),
                ('cdpg_monto_total', models.DecimalField(max_digits=15, decimal_places=2)),
                ('cdpg_codigo_teletransfer', models.CharField(max_length=10)),
                ('date_upload', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Comprobante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Correlativo', models.CharField(max_length=7, verbose_name=b'Correlativo')),
                ('Glosa', models.CharField(max_length=100, verbose_name=b'Glosa')),
                ('LugarPago', models.CharField(max_length=10, verbose_name=b'Lugar de Pago', choices=[(b'Caja', b'Caja'), (b'Banco', b'Banco'), (b'Banco2', b'Banco2'), (b'Dep\xc3\xb3sito', b'Dep\xc3\xb3sito'), (b'Planilla', b'Planilla')])),
                ('FechaEmision', models.DateField(null=True, blank=True)),
                ('TipoComprobante', models.ForeignKey(to='Configuraciones.TipoComprobante')),
            ],
            options={
                'verbose_name': 'Comprobante de Pago',
                'verbose_name_plural': 'Comprobantes de Pago',
            },
        ),
        migrations.CreateModel(
            name='ConceptoPago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Descripcion', models.CharField(max_length=100, verbose_name=b'Descripci\xc3\xb3n')),
                ('MontoPago', models.DecimalField(verbose_name=b'Monto', max_digits=6, decimal_places=2)),
                ('Moneda', models.CharField(default=b'Nuevo Sol', max_length=4, choices=[(b'S/.', b'Nuevo Sol'), (b'US$', b'D\xc3\xb3lar'), (b'\xe2\x82\xac', b'Euro')])),
                ('TipoMora', models.CharField(max_length=20, verbose_name=b'Tipo Mora ', choices=[(b'Ninguna', b'Ninguna'), (b'Diaria', b'Diaria'), (b'Semanal', b'Semanal'), (b'Mensual', b'Mensual')])),
                ('MontoMora', models.DecimalField(verbose_name=b'Monto Mora', max_digits=4, decimal_places=2)),
                ('FechaVencimiento', models.DateField(verbose_name=b'Vence Pago')),
                ('PagoBanco', models.BooleanField(default=False, verbose_name=b'\xc2\xbfSe paga en el Banco?')),
                ('Unico', models.BooleanField(default=True, verbose_name=b'\xc2\xbfEs \xc3\xbanico?')),
            ],
            options={
                'verbose_name': 'Concepto de Pago',
                'verbose_name_plural': 'Conceptos de Pago',
            },
        ),
        migrations.CreateModel(
            name='DescuentoPago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Descripcion', models.CharField(max_length=24, verbose_name=b'Descripci\xc3\xb3n')),
                ('FechaLimite', models.PositiveIntegerField(verbose_name=b'D\xc3\xadas antes')),
                ('MontoDescuento', models.DecimalField(verbose_name=b'Descuento', max_digits=6, decimal_places=2)),
                ('Categoria', models.ManyToManyField(to='Categorias.Categoria')),
                ('Periodo', models.ForeignKey(verbose_name=b'Per\xc3\xadodo', blank=True, to='Periodos.Periodo', null=True)),
            ],
            options={
                'verbose_name': 'Descuento de pago',
                'verbose_name_plural': 'Descuentos de pago',
            },
        ),
        migrations.CreateModel(
            name='DetailBankFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.CharField(max_length=14)),
                ('retorno', models.CharField(max_length=30)),
                ('fecha_pago', models.DateField()),
                ('fecha_vencimiento', models.DateField()),
                ('monto_pagado', models.DecimalField(max_digits=15, decimal_places=2)),
                ('mora_pagada', models.DecimalField(max_digits=15, decimal_places=2)),
                ('monto_total', models.DecimalField(max_digits=15, decimal_places=2)),
                ('sucursal', models.PositiveIntegerField()),
                ('num_operacion', models.PositiveIntegerField()),
                ('referencia', models.CharField(max_length=22)),
                ('terminal', models.CharField(max_length=4, null=True, blank=True)),
                ('medio_atencion', models.CharField(max_length=12, null=True, blank=True)),
                ('hora_atencion', models.TimeField()),
                ('num_cheque', models.PositiveIntegerField(null=True, blank=True)),
                ('codigo_banco', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('cabecera', models.ForeignKey(to='Pagos.BankFile')),
            ],
        ),
        migrations.CreateModel(
            name='DetallePago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Mora', models.BooleanField(default=False, verbose_name=b'\xc2\xbfMora?')),
                ('MontoDeuda', models.DecimalField(verbose_name=b'Deuda', max_digits=6, decimal_places=2)),
                ('MontoCancelado', models.DecimalField(verbose_name=b'Cancela', max_digits=6, decimal_places=2)),
                ('PagoRestante', models.DecimalField(verbose_name=b'Resta', max_digits=6, decimal_places=2)),
                ('FechaPago', models.DateField()),
                ('Activado', models.BooleanField(default=True, verbose_name=b'\xc2\xbfActivada?', editable=False)),
                ('Comprobante', models.ForeignKey(verbose_name=b'Comprobante', to='Pagos.Comprobante')),
            ],
            options={
                'verbose_name': 'Pago de Alumnos UDL',
                'verbose_name_plural': 'Pagos de Alumnos UDL',
            },
        ),
        migrations.CreateModel(
            name='Mora',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Monto', models.DecimalField(verbose_name=b'Monto', max_digits=6, decimal_places=2)),
                ('Exonera', models.BooleanField(default=False, verbose_name=b'\xc2\xbfExonerada?')),
                ('Estado', models.BooleanField(default=False, verbose_name=b'\xc2\xbfCancelada?')),
            ],
            options={
                'verbose_name': 'Mora',
                'verbose_name_plural': 'Moras',
            },
        ),
        migrations.CreateModel(
            name='PagoAlumno',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Estado', models.BooleanField(default=False, verbose_name=b'\xc2\xbfCancelado?', editable=False)),
                ('IgnorarCategoria', models.BooleanField(default=False, verbose_name=b'\xc2\xbfIgnorar Categor\xc3\xada?', editable=False)),
                ('FechaCreacion', models.DateTimeField(auto_now_add=True)),
                ('Prorroga', models.BooleanField(default=False, verbose_name=b'\xc2\xbfPr\xc3\xb3rroga?', editable=False)),
                ('FechaProrroga', models.DateField(default=None, null=True, verbose_name=b'Fecha pr\xc3\xb3rroga', blank=True)),
                ('ConceptoPago', models.ForeignKey(verbose_name=b'Concepto de Pago', to='Pagos.ConceptoPago')),
                ('MatriculaCiclo', models.ForeignKey(verbose_name=b'Alumno', to='Matriculas.MatriculaCiclo')),
            ],
            options={
                'verbose_name': 'Asignaci\xf3n de Pagos',
                'verbose_name_plural': 'Asignaci\xf3n de Pagos',
                'permissions': (('read_pagoalumno', 'Observar Pagos de Alumnos UDL'), ('update_prorroga', 'Editar prorroga')),
            },
        ),
        migrations.AddField(
            model_name='mora',
            name='PagoAlumno',
            field=models.ForeignKey(verbose_name=b'Concepto', to='Pagos.PagoAlumno'),
        ),
        migrations.AddField(
            model_name='detallepago',
            name='PagoAlumno',
            field=models.ForeignKey(verbose_name=b'Concepto', to='Pagos.PagoAlumno'),
        ),
        migrations.AddField(
            model_name='conceptopago',
            name='Descuentos',
            field=models.ManyToManyField(to='Pagos.DescuentoPago', verbose_name=b'Descuentos', blank=True),
        ),
        migrations.AddField(
            model_name='conceptopago',
            name='Periodo',
            field=models.ForeignKey(verbose_name=b'Per\xc3\xadodo', to='Periodos.Periodo'),
        ),
        migrations.AlterUniqueTogether(
            name='comprobante',
            unique_together=set([('Correlativo', 'TipoComprobante')]),
        ),
    ]
