#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
os.environ['DJANGO_SETTINGS_MODULE'] = 'UDL.settings'
application = get_wsgi_application()

from Pagos.models import DetallePago

from UDL.settings import MEDIA_ROOT

# imports para traer variable de entorno django
import codecs


def script_matricular():
    file1 = codecs.open("extras/migracion.csv", 'r', 'ANSI')
    i = 1
    nombre_archivo = "migracion" + ".csv"
    ruta_archivo = str(MEDIA_ROOT)
    destination = csv.writer(open('%s/%s' % (ruta_archivo, nombre_archivo), 'wb+'))
    for line in file1:
        line = line.strip('\n')
        linea = line.split('|')
        if i == 1:
            i = i + 1
            continue
        serie = linea[1]
        numero = linea[2]
        alumno = linea[3]
        concepto = linea[4]
        if serie == "1":
            try:
                detallepago = DetallePago.objects.filter(Comprobante__Correlativo=numero.replace("0", ""))
            except DetallePago.DoesNotExist:
                destination.writerow([numero, concepto])


script_matricular()
